<h3 style="font-size:18px;">No Class/Camps Bookings Report</h3>
<?php
if ($gval[2] == 1) { ?>    
    <form method="POST" action="thinline.php?id=51:4:2::::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A">
        <table style="background: #efefef; width:50%;" cellpadding="5" border="0">
            <tr>
                <td><strong>Select Term to Check Against</strong></td>
                
            </tr>
            <tr>
                <td width="10">
                    <?php
                    $seasonArray = array("Spring","Summer","Autumn","Winter");
                    ?>
                    <select name="termtime" style="padding:3px;font-size:14px;">
                        <option value="">Select One</option>
                        <option value="any">Any</option>
                        <?php
                        foreach ($seasonArray AS $season) { ?>
                            <option value="<?php echo $season;?>"><?php echo $season;?></option>
                        <?php
                        } ?>
                    </select>
                    <select name="year" style="padding:3px;font-size:14px;">
                        <option value="">Select One</option>
                        <option value="any">Any</option>
                        <?php
                        $startYear = 2014;
                        $endYear = date("Y") + 5;
                        $currentYear = date("Y");
                        for ($x = $startYear; $x<=$endYear; $x++) {?>
                            <option <?php echo $selected;?> value="<?php echo $x;?>"><?php echo $x;?></option>
                        <?php
                        } ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan=2>
                    <input type="submit" name="rebooklist" value="Get Rebook List" style="padding:5px;font-size:14px;">
                </td>
            </tr>
        </table>
    </form>
    <br>
<?php
} elseif ($gval[2] == 2) {
    
    if ((!isset($_POST['termtime'])) ||  (!isset($_POST['year']))) {
        die("Something has gone wrong, please rerun the report.");
    }
    
    // define master in this case is kids
    $sql = "SELECT hashid, childsname, birthday FROM `udf_63538FE6EF330C13A05A3ED7E599D5F7` WHERE issaved = 1 ".$addsqldepartments;
    //echo $sql."<br>";
    $res = mysql_query($sql);
    $counter = 0;
    $outputArray = array();
    while ($row = mysql_fetch_array($res)) {
        
        $sql_term = ($_POST['termtime'] != "any") ? " AND termtime = '".mysql_real_escape_string($_POST['termtime'])."' " : "" ;
        $sql_year = ($_POST['year'] != "any") ? " AND year = '".mysql_real_escape_string($_POST['year'])."' " : "" ;
        
        // Now we check if they have a booking in the current term
        $sqli = "SELECT id FROM `udf_2BB232C0B13C774965EF8558F0FBD615`
                    WHERE issaved = 1
                    AND childsname = '".mysql_real_escape_string($row['childsname'])."'
                    ".$sql_term.$sql_year."
                ".$addsqldepartments.";";
        //echo $sqli."<br />";
        $resi = mysql_query($sqli);
        $rowi = mysql_fetch_array($resi);
        
        if ($rowi['id'] == "") {
            
            $outputArray[$counter]['childhashid'] = $row['hashid'];
            $outputArray[$counter]['childsname'] = $row['childsname'];
            $outputArray[$counter]['birthday'] = $row['birthday'];
            
            // Get parent hashid
            $sqlj = "SELECT `child_record` FROM multilink
                        WHERE `parent_table` = '63538FE6EF330C13A05A3ED7E599D5F7'
                        AND `parent_record` = '".mysql_real_escape_string($row['hashid'])."'
                        AND `child_table` = '45C48CCE2E2D7FBDEA1AFC51C7C6AD26';";
            //echo $sqlj."<br>";
            $resj = mysql_query($sqlj);
            $rowj = mysql_fetch_array($resj);
            
            // Get guardian details
            $sqlc = "SELECT hashid, fullname, mobile, email, marketingcommunications FROM `udf_45C48CCE2E2D7FBDEA1AFC51C7C6AD26`
                    WHERE hashid = '".mysql_real_escape_string($rowj['child_record'])."'
                    AND issaved = 1 ".$addsqldepartments." LIMIT 1";
            //echo $sqlc."<br>";
            $resc = mysql_query($sqlc);
            $rowc = mysql_fetch_array($resc);
            
            $outputArray[$counter]['guardiansname'] = $rowc['fullname'];
            $outputArray[$counter]['guardianhashid'] = $rowc['hashid'];
            $outputArray[$counter]['guardiansmobile'] = $rowc['mobile'];
            $outputArray[$counter]['guardiansemail'] = $rowc['email'];
            $outputArray[$counter]['marketingcommunications'] = $rowc['marketingcommunications'];
            
            $counter++;
            
        } 
    }
    
    $sqlc = "SELECT * FROM udf_45C48CCE2E2D7FBDEA1AFC51C7C6AD26 WHERE issaved = 1 ".$addsqldepartments;
    //echo $sqlc;
    $resc = mysql_query($sqlc);
    while ($rowc = mysql_fetch_array($resc)) {   
        $sqli = "SELECT `child_record` FROM multilink WHERE `parent_table` = '45C48CCE2E2D7FBDEA1AFC51C7C6AD26'
                AND `parent_record` = '".$rowc['hashid']."'
                AND `child_table` = '63538FE6EF330C13A05A3ED7E599D5F7';";
        $resci = mysql_query($sqli);
        $rowci = mysql_fetch_array($resci);
        
        if (!$rowci['child_record']) {
            //echo $sqli."<br />";
            //echo "Nothing for me<br>";
            
            $outputArray[$counter]['guardiansname'] = $rowc['fullname'];
            $outputArray[$counter]['guardianhashid'] = $rowc['hashid'];
            $outputArray[$counter]['guardiansmobile'] = $rowc['mobile'];
            $outputArray[$counter]['guardiansemail'] = $rowc['email'];
            $outputArray[$counter]['marketingcommunications'] = $rowc['marketingcommunications'];
            
            $counter++;
            
        }
    }
    
    /*
    echo "<pre>";
    print_r($outputArray);
    echo "</pre>";*/
    ?>

    <a href="dashboards/no-bookings-csv-v2.php?season=<?php echo $_POST['termtime'];?>&year=<?php echo $_POST['year'];?>">Export to CSV</a><br><br>
    <table border="0" width="100%" style="font-family:Arial;font-size: 14px;">
        <tr>
            <td class="ls_top"><strong>#</strong></td>
            <td class="ls_top"><strong>Guardian Name</strong></td>
            <td class="ls_top"><strong>Phone</strong></td>
            <td class="ls_top"><strong>Email</strong></td>
            <td class="ls_top"><strong>Opt In/Out</strong></td>
            <td class="ls_top"><strong>Child's Name</strong></td>
            <td class="ls_top"><strong>DOB</strong></td>
        </tr>
        
        <?php
        
        if (count($outputArray) < 1) {
            include("includes/ls.php"); ?>
            <tr>
                <td colspan="5" class="ls_<?php echo $ls;?>" style="font-size:14px;">No data to compare</td>
            </tr>
        <?php
        } else {
            $zecount = 1;
            foreach($outputArray AS $rebook) {
                include("includes/ls.php"); ?>
                <tr>
                    <td class="ls_<?php echo $ls;?>" style="font-size:14px;" align="center"><?php echo ($zecount);?></td>
                    <td class="ls_<?php echo $ls;?>" style="font-size:14px;"><?php echo $rebook['guardiansname'];?></td>
                    <td class="ls_<?php echo $ls;?>" style="font-size:14px;">
                        <?php
                            echo $rebook['guardiansmobile'];
                            //echo ($rebook['landline'] != "") ? ", ".$rebook['landline'] : "";
                        ?>
                    </td>
                    <td class="ls_<?php echo $ls;?>" style="font-size:14px;"><a target="_playball_nobook" href="thinline.php?id=2:<?php echo $rebook['guardianHashId'];?>:::::::::::::::::::A87FF679A2F3E71D9181A67B7542122C:45C48CCE2E2D7FBDEA1AFC51C7C6AD26:2::"><?php echo $rebook['guardiansemail'];?></a></td>
                    <td class="ls_<?php echo $ls;?>" style="font-size:14px;"><?php echo $rebook['marketingcommunications'];?></td>
                    <td class="ls_<?php echo $ls;?>" style="font-size:14px;"><a target="_rebookreport" href="thinline.php?id=2:<?php echo $rebook['childhashid'];?>:::::::::::::::::::8F85517967795EEEF66C225F7883BDCB:63538FE6EF330C13A05A3ED7E599D5F7:2::"><?php echo $rebook['childsname'];?></a></td>
                    <td class="ls_<?php echo $ls;?>" style="font-size:14px;"><?php echo     $rebook['birthday'];?></td>
                </tr>
            <?php
            $zecount++;
            }
            
        } ?>
    </table>
    <br>
    <a href="dashboards/no-bookings-csv-v2.php?season=<?php echo $_POST['termtime'];?>&year=<?php echo $_POST['year'];?>">Export to CSV</a><br><br>
<?php
}?>