<h3 style="font-size:18px;">Outstanding Confirmations</h3><table width="100%">
    <tr>
        <td class="ls_top"><strong>Venue ID</td>
        <td class="ls_top"><strong>Franchise Owner</td>
        <td class="ls_top"><strong>Venue Name</td>
        <td class="ls_top"><strong>Address</td>
        <td class="ls_top"><strong>Country</td>
        <td class="ls_top"><strong>Days Since Approval</td>
        <td class="ls_top"><strong>Actions</td>
    </tr>
<?php
if (($_SESSION['fullname'] != "Patrick McCarthy") && ($_SESSION['fullname'] != "Jill Jacobsen")) {
   $addZeSQL = " AND udf.country = '".mysql_real_escape_string($_SESSION['countrymanager']['country'])."' ";
}
$sql = "SELECT recordid, hashid, department, createdby, venuename, address, citytown, zippostcode,
                stateprovincecounty, mapcode, udf.country, u.username, u.email, approveddate, DATEDIFF(CURDATE(), approveddate) AS dayssinceapproval
            FROM `udf_5737034557EF5B8C02C0E46513B98F90` udf, users u 
            WHERE udf.department = u.departments AND issaved = 1 AND receivedconfirmation = 'No'
            ".$addZeSQL;
//echo $sql."<br />";
$res = mysql_query($sql);
$num_rows = mysql_num_rows($res);
if ($num_rows > 0) {
    while ($row = mysql_fetch_array($res)) {
        include("includes/ls.php");
        $overdueDate = strtotime("+60 days", strtotime($row['approveddate'])); ?>
        <tr>
            <td class='ls_<?php echo $ls;?>' style="font-size:14px;"><?php echo $row['recordid'];?></td>
            <td class='ls_<?php echo $ls;?>' style="font-size:14px;"><?php echo $row['username']." (".$row['email'].")";?></td>
            <td class='ls_<?php echo $ls;?>' style="font-size:14px;"><?php echo $row['venuename'];?></td>
            <td class='ls_<?php echo $ls;?>' style="font-size:14px;"><?php echo $row['address'].", ".$row['citytown'].", ".$row['zippostcode'].", ".$row['stateprovincecounty'];?></td>
            <td class='ls_<?php echo $ls;?>' style="font-size:14px;"><?php echo $row['country'];?></td>
            <td class='ls_<?php echo $ls;?>' style="font-size:14px;">
                <?php
                $fontcolor = ($row['dayssinceapproval'] <= 60) ? "green" : "red";
                echo "<font style='color:".$fontcolor.";font-size:12px;font-weight:bold'>".$row['dayssinceapproval']."</font>";?>
            </td>
            <td class='ls_<?php echo $ls;?>' style="font-size:14px;">
                <?php echo "<a href='mailto:".$row['email']."&subject=Venue confirmation outstanding for ".$row['venuename']."&body=Please note that a written confirmation was required for ".$row['venuename']." by ".date("d F Y",$overdueDate).".'>Email Late Notice</a>";?> | 
                <a href="dashboards/venue-confirmation-received.php?hashid=<?php echo $row['hashid'];?>&email=<?php echo $row['email'];?>&username=<?php echo $row['username'];?>">Confirmation Received</a>
            </td>
        </tr>
    <?php
    }
} else {
    echo "<td colspan=7 class='ls_off' style='font-size:14px;'>No records pending approval</td>";
} ?>
</table>