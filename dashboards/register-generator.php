<?php
if (($gval[1] == 1) && ($gval[2] == 1)) { ?>
    <h3 style="font-size:18px;">Register Generator</h3>
    <form method="POST" action="thinline.php?id=51:1:3::::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A">
        <table style="background: #efefef; width:100%;" cellpadding="5" border="0">
            <tr>
                <td colspan="2"><h3>Search for Class</h3></td>
            </tr>
            <tr>
                <td>
                    <select style="font-size:14px;padding:3px;" name="searchoption">
                        <option>Contains</option>
                        <option>Starts With</option>
                    </select>
                    <input type="text" name="regitersearch" style="padding:5px;width:350px"></td>
            </tr>
            <tr>
                <td>
                    <?php
                    $seasonArray = array("Spring","Summer","Autumn","Winter");
                    ?>
                    <select name="season" style="padding:3px;font-size:14px;">
                        <option value="Any">Any Term</option>
                        <?php
                        foreach ($seasonArray AS $season) { ?>
                            <option value="<?php echo $season;?>"><?php echo $season;?></option>
                        <?php
                        } ?>
                    </select>
                    <select name="year" style="padding:3px;font-size:14px;">
                        <option value="Any">Any Year</option>
                        <?php
                        $startYear = 2014;
                        $endYear = date("Y") + 5;
                        $currentYear = date("Y");
                        for ($x = $startYear; $x<=$endYear; $x++) {
                            ?>
                            <option <?php echo $selected;?> value="<?php echo $x;?>"><?php echo $x;?></option>
                        <?php
                        } ?>
                    </select>
                    <input style="font-size:14px;padding:5px;" type="submit" value="Find" />
                </td>
            </tr>
        </table>
    </form>
    <br>
    <form method="POST" action="thinline.php?id=51:1:2::::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A" name="standardcssformlist" id="standardcssformlist">
    <table border="0" style="background: #efefef; width:100%;" cellpadding="5" >
    <tr>
        <td colspan="2"><h3>Select by Class</h3></td>
    </tr>
    <tr>
        <td class="smscell">
            <?php
            $sql = "SELECT hashid, classname, recordid FROM `udf_2B8A61594B1F4C4DB0902A8A395CED93` WHERE issaved = 1 ".$addsqldepartments." ORDER BY id DESC";
            //echo $sql;
            ?>
            <select name="class" style="font-size:12px;padding:3px;">
                <?php
                $result = mysql_query($sql);
                while ($row = mysql_fetch_array($result)) { ?>
                    <option value="<?php echo trim($row['hashid']);?>"><?php echo $row['classname']." (".$row['recordid'];?>)</option>
                <?php
                } ?>
            </select>
        </td>
    </tr>
    <tr>
        <td colspan="<?php echo $colspan;?>">
            <input style="font-size:14px;padding:5px;" type="submit" value="Generate Class List" />
        </td>
    </tr>
    </table>
    </form>
    
<?php
} elseif (($gval[1] == 1) && ($gval[2] == 2)) {
    function findage($dob) {
        $localtime = getdate();
        $today = $localtime['year']."-".$localtime['mon']."-".$localtime['mday'];
        
        $ts1 = strtotime($today);
        $ts2 = strtotime($dob);
        
        $year1 = date('Y', $ts1);
        $year2 = date('Y', $ts2);
        
        $month1 = date('m', $ts1);
        $month2 = date('m', $ts2);
        
        $diff = (($year1 - $year2) * 12) + ($month1 - $month2);
        $years = floor($diff/12)."<br>";
        $months = 12 * (($diff/12) - floor($diff/12));
        
        return "$years years $months months";
    }

    // To get all related record ids from a parent
    // Usage = generateRecList(<parentmenuid>,<resultsmenuid>);
    function generateRecList($parentmenuid, $childmenuid) {

        $reclist = array();
        // Get the recordids for the linked table
        $sql = "SELECT recordid FROM  `relations_values`
                WHERE parent_recordid =  '".mysql_escape_string($parentmenuid)."'
                AND matrix_id = 390
                AND location =  '".mysql_escape_string($childmenuid)."';";
        //echo $sql."<br><br>";
        $res = mysql_query($sql);
        while ($row = mysql_fetch_array($res)) {
            array_push($reclist,$row['recordid']);
        }
        return ($reclist);
    }

    // Pull linked record data for the results of generateRecList
    // Usage pullRecData(<hashids from generateRecList>, <parentmenuid>, <columns to display>)
    function pullRecData($hashid, $parentmenuid, $columns) {

        $recdata = array();
        // Get the recordids for the linked table
        $sql = "SELECT ".mysql_escape_string($columns)." FROM  `udf_".mysql_escape_string($parentmenuid)."`
                WHERE hashid =  '".mysql_escape_string($hashid)."' LIMIT 1;";
        //echo $sql."<br><br>";
        $res = mysql_query($sql);
        $row = mysql_fetch_array($res);
        $columnsarr = explode(",",$columns);
        $recdata['parenthashid'] = $hashid;
        foreach ($columnsarr AS $cols) {
            $recdata[$cols] .= $row[$cols];
        }
        
        return ($recdata);
    }

    function fromOnetoOne($parentrecordid, $childmatrixid, $childmenuid, $columns) {

        $recdata = array();
        $sql = "SELECT rv.parent_recordid AS recordid
                FROM udf_definitions udf, relations_matrix rx, relations_values rv, menusub_tabs mst
                WHERE rx.id = ".mysql_escape_string($childmatrixid)."
                AND rx.id = rv.matrix_id
                AND rx.udf_columnid = udf.hashid
                AND rv.recordid = '".mysql_escape_string($parentrecordid)."'
                AND rx.parenttable = mst.hashid AND rx.registrantid = 1 LIMIT 1";
        //echo $sql."<br>";
        $res = mysql_query($sql);
        $row = mysql_fetch_array($res);
        $childrecordid = $row['recordid'];

        $sql = "SELECT ".mysql_escape_string($columns)."
                FROM udf_".mysql_escape_string($childmenuid)."
                WHERE hashid = '".mysql_escape_string($childrecordid)."' LIMIT 1";
        //echo $sql."<br>";
        $res = mysql_query($sql);
        $row = mysql_fetch_array($res);
        $columnsarr = explode(",",$columns);
        foreach ($columnsarr AS $cols) {
            $recdata[$cols] .= $row[$cols];
        }
        return ($recdata);

    }

    function oneToMany($parenthashid,$menuid,$childmenuid,$columns) {

        $primarr = array();
        $recdata = array();

        $sql = "SELECT `child_record` FROM multilink
                WHERE `parent_table` = '".mysql_escape_string($menuid)."'
                AND `parent_record` = '".mysql_escape_string($parenthashid)."'
                AND `child_table` = '".mysql_escape_string($childmenuid)."';";
        //echo $sql."<br>";
        $res = mysql_query($sql);
        $counter = 0;
        while ($row = mysql_fetch_array($res)) {
            $sqli = "SELECT ".mysql_escape_string($columns)." FROM udf_".mysql_escape_string($childmenuid)."
                    WHERE hashid = '".mysql_escape_string($row['child_record'])."' LIMIT 1;";
            //echo $sqli."<br>";
            $resi = mysql_query($sqli);
            $rowi = mysql_fetch_array($resi);
            $columnsarr = explode(",",$columns);
            foreach ($columnsarr AS $cols) {
                $recdata[$cols] = $rowi[$cols];
            }
            $primarr[$counter] = $recdata;
            $counter++;

        }
        return ($primarr);
    }

    // Start with the primary search - In our case finidng all bookings for a specific class - This produces a hashid list
    // in an array.
    
    $_POST['class'] = (isset($_POST['class'])) ? $_POST['class'] : $gval[3];
    
    $hashrecordidarr = generateRecList($_POST['class'],'2BB232C0B13C774965EF8558F0FBD615');?>

    <?php
    $counter = 0;
    // run through the array to either out the hashids found or any related
    // linked fields and continually add them to the final array
    
    foreach ($hashrecordidarr AS $val) {

            $recdata[$counter] = pullRecData($val, '2BB232C0B13C774965EF8558F0FBD615', 'paid,bookingtype,campbookingdate,notes,photosallowed,registrationfee,createddate,guardiansname,guardiansemail,guardiansmobile');
            
            if (($recdata[$counter]['bookingtype'] == 'Duplicate') || ($recdata[$counter]['bookingtype'] == '')) {
                unset($recdata[$counter]);
                continue;
            }
            
            
            ///////// Get kid information (or 1 to 1 relationship)  //////////
            $cols = 'hashid,childsname,birthday,notesmedical,tshirt,leftrighthanded';
            $kidrecordidarr = fromOnetoOne($val,'364','63538FE6EF330C13A05A3ED7E599D5F7',$cols);
            $colsarr = explode(",",$cols);
            foreach ($colsarr AS $newcols) {
                    $recdata[$counter][$newcols] .= $kidrecordidarr[$newcols];
            }

            ///////// Get Guardian information  (many to many relationship) multilink-output.php //////////
            $cols = 'fullname AS guardiansname,mobile AS guardiansmobile,landline,email AS guardiansemail';
            $guardianredordidarr = oneToMany($kidrecordidarr['hashid'],'63538FE6EF330C13A05A3ED7E599D5F7','45C48CCE2E2D7FBDEA1AFC51C7C6AD26',$cols);
            $gcounter = 0;
            foreach ($guardianredordidarr AS $garrays) {
                $colsarr = explode(",",$cols);
                foreach ($colsarr AS $newcols) {
                    $recdata[$counter]['guardians'][$gcounter][$newcols] .= $garrays[$newcols];
                }
                $gcounter++;
            }
            
            //echo "<pre>";
            //print_r($recdata);

            ///////// Get class information (or 1 to 1 relationship)  //////////
            $cols = 'classname,venue,coach,agegroup';
            $classrecordidarr = fromOnetoOne($val,'390','2B8A61594B1F4C4DB0902A8A395CED93',$cols);
            $colsarr = explode(",",$cols);
            foreach ($colsarr AS $newcols) {
                    $recdata[$counter][$newcols] .= $classrecordidarr[$newcols];
            }

            // Dont touch counter
            $counter++;
    }

    if ($counter > 0) { // Have a booking

        // Lets check if this is a camps or class
        $sql = "SELECT type, payoptions FROM `udf_2B8A61594B1F4C4DB0902A8A395CED93` WHERE `hashid` = '".$_POST['class']."' LIMIT 1";
        //echo $sql."<br />";
        $res = mysql_query($sql);
        $row = mysql_fetch_array($res);
        $campType = $row['type'];
        $payOptions = $row['payoptions'];
        
        // Special for sessions
        $sessionarr = array();
        $sql = "SELECT session_date, session_start_time, session_end_time
                 FROM `class_sessions` WHERE parent_hashid = '".$_POST['class']."' AND location = '2B8A61594B1F4C4DB0902A8A395CED93' ORDER BY session_date ASC";
        //echo $sql."<br>";
        $res = mysql_query($sql);
        $sshcounter = 0;
        while ($row = mysql_fetch_array($res)) {
            $sessionarr[$sshcounter]['session_date'] = $row['session_date'];
            $sessionarr[$sshcounter]['session_start_time'] = $row['session_start_time'];
            $sessionarr[$sshcounter]['session_end_time'] = $row['session_end_time'];
            $sshcounter++;
        } ?>
        <!--<pre><?php //print_r($recdata); ?></pre>
        <pre><?php //print_r($sessionarr); ?></pre>-->
        Class Name: <?php echo $recdata[0]['classname']; ?><br>
        Age Group: <?php echo $recdata[0]['agegroup']; ?><br>
        Venue: <?php echo $recdata[0]['venue']; ?><br>
        Coach: <?php echo $recdata[0]['coach']; ?><br><br>
        
        <?php
        if (($campType == "Camps") && ($payOptions != "Pay Full Camp/Class Fee")) {
            // Type is camp offer filter option on day
            echo "<font style='font-size:14px;'><strong><u>Select Camp Date to Print</u></strong><br /><br />";
            $sqli = "SELECT session_date, session_start_time, session_end_time
                 FROM `class_sessions` WHERE parent_hashid = '".$_POST['class']."' AND location = '2B8A61594B1F4C4DB0902A8A395CED93' ORDER BY session_date ASC";
            //echo $sql."<br>";
            $resi = mysql_query($sqli);
            $sshcounter = 0;
            while ($rowi = mysql_fetch_array($resi)) {
               echo date("d M Y", strtotime($rowi['session_date']))." - ";
               echo "<a href=\"dashboards/register-to-csv.php?id=".$_POST['class'].":2B8A61594B1F4C4DB0902A8A395CED93:".$rowi['session_date']."\">Export to CSV - Single Date Column</a> &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; ";
               echo "<a href=\"dashboards/register-to-csv-signed.php?id=".$_POST['class'].":2B8A61594B1F4C4DB0902A8A395CED93:".$rowi['session_date']."\">Export to CSV - Sign In / Sign Out Columns</a><br>";
            }
            echo '</font>';
        } else { ?>
        
            <table border="0" width="100%" style="font-family:Arial;font-size: 12px;">
                <tr>
                    
                    <td class="ls_top">Child Name</td>
                    <td class="ls_top">Guardian Name</td>
                    <td class="ls_top">Number</td>
                    <td class="ls_top">Email</td>
                    <td class="ls_top">DOB</td>
                    <td class="ls_top">Notes (Medical)</td>
                    <td class="ls_top">Notes (Booking)</td>
                    <td class="ls_top">Paid</td>
                    <td class="ls_top">Photos Allowed</td>
                    <td class="ls_top">Booking</td>
                    <td class="ls_top">L/R Hand</td>
                    <td class="ls_top">T-Shirt</td>
                    <td class="ls_top">Registration<br>Fee</td>
                    
                    <?php
                    $numsessions = count($sessionarr);
                    for($w = 0; $w < $numsessions; $w++) {
                        echo "<td  class='ls_top'>".date("d-M",strtotime($sessionarr[$w]['session_date']))."</td>";
                    } ?>
                </tr>
            <?php
            
                $numrecords = count($recdata);
                $y = 0;
                for($x = 0; $x < $numrecords; $x++) {
                    if ($recdata[$x]['childsname'] != "") {
                        include("includes/ls.php");
                        echo "<tr>";
                            echo "<td class='lsi_".$ls."'>".$recdata[$x]['childsname']."</td>";
                            echo "<td class='lsi_".$ls."'>";
                            if ($recdata[$x]['guardians'][$y]['fullname'] != "") {
                               echo $recdata[$x]['guardians'][$y]['fullname'];
                            } else {
                                echo $recdata[$x]['guardiansname'];
                            }
                            echo "</td>";
                            echo "<td class='lsi_".$ls."'>";
                            if ($recdata[$x]['guardians'][$y]['mobile'] != "") {
                                echo $recdata[$x]['guardians'][$y]['mobile'] != "" ? $recdata[$x]['guardians'][$y]['mobile'].", " : "";
                            } else {
                                echo $recdata[$x]['guardiansmobile'];
                            }
                            echo $recdata[$x]['guardians'][$y]['landline'] != "" ? " / ".$recdata[$x]['guardians'][$y]['landline']."" : "";
                            echo "</td>";
                            echo "<td class='lsi_".$ls."'>";
                            if ($recdata[$x]['guardians'][$y]['mobile'] != "") {
                                echo $recdata[$x]['guardians'][$y]['email'];
                            } else {
                                echo $recdata[$x]['guardiansemail'];
                            }
                            echo "</td>";
                            echo "<td class='lsi_".$ls."'>".date("d-m-Y",strtotime($recdata[$x]['birthday']))."</td>"; //put date in the dd-mm-yyyy format
                            echo "<td class='lsi_".$ls."'>".$recdata[$x]['notesmedical']."</td>";
                            $string = $recdata[$x]['notes'];
                            $prefix = "VendorTxCode:";
                            $index = strpos($string, $prefix) + strlen($prefix);
                            $found_str = strpos($string, $prefix);
                            if ($found_str > 0) {
                                $result = substr($string, 0, $index);
                            } else {
                                $result = $recdata[$x]['notes'];
                            }
                            $remove_text = array("VendorTxCode:","Booking Notes","No notes","No Discount Applied");
                            echo "<td class='lsi_".$ls."'>".str_replace($remove_text,"", $result)."</td>";
                            echo "<td class='lsi_".$ls."'>".$recdata[$x]['paid']."</td>";
                            $photosAllowed = ($recdata[$x]['photosallowed']=="checkboxon") ? "Yes" : "No";
                            echo "<td class='lsi_".$ls."'>".$photosAllowed."</td>";
                            echo "<td class='lsi_".$ls."'>".$recdata[$x]['bookingtype']." (".date("d M H:i",strtotime($recdata[$x]['createddate'])).")</td>";
                            $handed = ($recdata[$x]['leftrighthanded'] == "Right Handed") ? "RH" : "LH";
                            echo "<td class='lsi_".$ls."'>".$handed."</td>";
                            echo "<td class='lsi_".$ls."'>".$recdata[$x]['tshirt']."</td>";
                            echo "<td class='lsi_".$ls."'>".$recdata[$x]['registrationfee']."</td>";
                            
                            for($w = 0; $w < $numsessions; $w++) {
                                echo "<td class='lsi_".$ls."'>&nbsp;</td>";
                            }
                        echo "</tr>";
                        unset($line,$value);
                    }
                    
                }
                echo "<br>";
                $data = str_replace( "\r" , "" , $data );
    
            ?>
            </table>
            <br>
            <h3>Export Register Options</h3>
            <a href="dashboards/register-to-csv.php?id=<?php echo $_POST['class'];?>:2B8A61594B1F4C4DB0902A8A395CED93">Export to CSV - Single Date Column</a><br>
            <a href="dashboards/register-to-csv-with-booking-date.php?id=<?php echo $_POST['class'];?>:2B8A61594B1F4C4DB0902A8A395CED93">Export to CSV - With Camp Booking Date</a><br>
            <a href="dashboards/register-to-csv-signed.php?id=<?php echo $_POST['class'];?>:2B8A61594B1F4C4DB0902A8A395CED93">Export to CSV - Sign In / Sign Out Columns</a><br>
            <a href="dashboards/register-to-weeks-remaining.php?id=<?php echo $_POST['class'];?>:2B8A61594B1F4C4DB0902A8A395CED93">Export to CSV - Weeks Remaining</a><br>
            <a href="dashboards/register-to-weeks-remaining-signed.php?id=<?php echo $_POST['class'];?>:2B8A61594B1F4C4DB0902A8A395CED93">Export to CSV - Weeks Remaining - Sign In / Sign Out Columns</a>
<?php
        }
    } else {
        echo "There are no bookings for this class";
    }
} elseif (($gval[1] == 1) && ($gval[2] == 3)) {
    
    if ($_POST['searchoption'] == "Starts With") {
        $searchString = strtolower($_POST['regitersearch']."%");
    } elseif ($_POST['searchoption'] == "Contains") {
        $searchString = strtolower("%".$_POST['regitersearch']."%");
    }
    
    if ($_POST['season'] != "Any") {
        $addsqlseason = "AND termtime = '".mysql_real_escape_string($_POST['season'])."' ";
        $addTxtSeason = "&season=".$_POST['season'];
    }
    
    if ($_POST['year'] != "Any") {
        $addsqlyear = "AND year = '".mysql_real_escape_string($_POST['year'])."' ";
        $addTxtYear = "&year=".$_POST['year'];
    }
    
    $sqli = "SELECT hashid, classname, recordid, coach FROM udf_2B8A61594B1F4C4DB0902A8A395CED93
                WHERE LOWER(classname) LIKE '".mysql_real_escape_string($searchString)."'
                ".$addsqlseason."
                ".$addsqlyear."
                ".$addsqldepartments."
                AND issaved = 1 ORDER BY recordid DESC;";
    //echo $sqli."<br>";
    $resi = mysql_query($sqli); ?>
    
    <h3 style="font-size:18px;">Search Results</h2>
    <table style="width:100%;" cellpadding="5" border="0">
    <tr>    
        <td class="ls_top"><strong>Record ID</strong></td>
        <td class="ls_top"><strong>Class Name</strong></td>
        <td class="ls_top"><strong>Coach</strong></td>
        <td class="ls_top"><strong>Action</strong></td>
    </tr>
    
    <?php
    $counter = 0;
    while ($rowi = mysql_fetch_array($resi)) {
        include("includes/ls.php");
        echo "<tr>";
        echo "<td style='font-size:14px;' class='ls_".$ls."' width='10%'>".$rowi['recordid']."</td>";
        echo "<td style='font-size:14px;' class='ls_".$ls."'>".$rowi['classname']."</td>";
        echo "<td style='font-size:14px;' class='ls_".$ls."'>".$rowi['coach']."</td>";
        echo "<td style='font-size:14px;' class='ls_".$ls."'><a href='thinline.php?id=51:1:2:".$rowi['hashid'].":::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A".$addTxtSeason.$addTxtYear."'>Generate Register</a></td>";
        echo "</tr>";
        $counter++;
    }
    
    if ($counter == 0) {
        echo "<tr>";
        echo "<td style='font-size:14px;' >No results found</td>";
        echo "</tr>";   
    }
    
    ?>
    
    </table>
    <br>
<?php
} ?>
