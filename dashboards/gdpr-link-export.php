<?php
include_once("..//_dbaccessdetails.php");
$filename = $rootpath.'document_store/temp_files/'.strtotime('now').'.csv';

if ($_SESSION['securityarrdept'][0] != "") {
	$deptcounter = 0;
	foreach ($_SESSION['securityarrdept'] AS $deptval) {
		if ($deptcounter == 0) {
			$deptoperator = " AND (";
		} else {
			$deptoperator = " OR ";
		}
		$addsqldepartments .= $deptoperator." FIND_IN_SET('".$deptval."', department)";
		$deptcounter++;
	}
	$addsqldepartments .= ")";
}

// Special for sessions
$sql = "SELECT recordid, hashid, fullname, email, marketingcommunications FROM udf_45C48CCE2E2D7FBDEA1AFC51C7C6AD26 WHERE issaved = 1 ".$addsqldepartments." ORDER BY fullname ASC";
//echo $sql."<br>";
//die();
$res = mysql_query($sql);

// Create CSV header
$header = "Full Name, Email, Preferences Link, Opt In, ID,";

while ($row = mysql_fetch_array($res)) {

	$value = '"' . $row['fullname'] . '"' . ",";
	$value .= '"' . $row['email'] . '"' . ",";
	$value .= '"https://www.playballkids.com/contact-preferences.php?id=' . $row['hashid'] . '"' . ",";
	$value .= '"' . $row['marketingcommunications'] . '"' . ",";
	$value .= '"' . $row['recordid'] . '"' . ",";
    
    $data .= trim( $value ) . "\n";
    unset($value);
}

$data = str_replace( "\r" , "" , $data );

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=gdpr_contact_preferences_".date("Y-m-d H:i:s").".csv");
header("Pragma: no-cache");
header("Expires: 0");
print "$header\n$data"; ?>
