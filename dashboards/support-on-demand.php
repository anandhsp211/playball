<?php
if ($_POST['action'] != "pay") { ?>
    <form method="POST" name="standardcssformlist" id="standardcssformlist">
    <table border="0" width="30%" cellpadding="5">
        <tr>
            <td style="background: #f2f2f2" align="center"><strong>Support Package</strong></td>
            <td style="background: #f2f2f2" align="center"><strong>Time</strong></td>
        </tr>
        <?php
        //echo $_SESSION['franchisecountry'];
        if ($_SESSION['franchisecountry'] == "United Kingdom") {
            $creditarray = array(30=>"25.00", 60=>"45.00", 90=>"65.00", 120=>"90.00", 240=>"175.00");
            $currency = "&pound;";
        } elseif ($_SESSION['franchisecountry'] == "South Africa") {
            $creditarray = array(30=>"450.00", 60=>"900.00", 90=>"1300.00", 120=>"1800.00", 240=>"3500.00");
            $currency = "R";
        } elseif (($_SESSION['franchisecountry'] == "Ireland") || ($_SESSION['franchisecountry'] == "Switzerland") || ($_SESSION['franchisecountry'] == "The Netherlands")) {
            $creditarray = array(30=>"30.00", 60=>"55.00", 90=>"80.00", 120=>"110.00", 240=>"215.00");
            $currency = "&euro;";
        } else {
            $creditarray = array(30=>"37.50", 60=>"67.50", 90=>"100.00", 120=>"135.00", 240=>"265.00");
            $currency = "$";
        }
        
        foreach ($creditarray AS $time=>$cost) {
            if ($time == 60) {
                $checked = "checked";
            } ?>
            <tr>
                <td style="background: #f9f9f9" >Personal Support - <?php echo $time;?> minutes @ <?php echo $currency." ".$cost;?></td>
                <td style="background: #f9f9f9" align="center"><input <?php echo $checked; ?> type="radio" name="supportcost" value="<?php echo $time."||".$cost;?>" /></td>
            </tr>
        <?php
        unset($checked);
        } ?>
        <tr>
            <td><br><input type="submit" name="buytexts" value="Buy with PayPal"></td>
        </tr>
    </table>
    <input type="hidden" name="action" value="pay">
    </form>
<?php
} else {
    $package = explode("||",$_POST['supportcost']); ?>
    <form action="https://www.paypal.com/cgi-bin/webscr?return=http://<?php echo $domainname;?>/backoffice/thank-you.php" method="post">
    <table border="0" width="30%" cellpadding="5">
        <tr>
            <td style="background: #f2f2f2" align="center"><strong>Support Package - Time</strong></td>
            <td style="background: #f2f2f2" align="center"><strong>Cost</strong></td>
        </tr>
        <tr>
            <td style="background: #f9f9f9" align="right"><?php echo $package[0];?> minutes</td>
            <td style="background: #f9f9f9" align="right"><?php echo $currency."".$package[1];?></td>
        </tr>
    </table>
    <?php include_once("includes/paypal-cert-support.php");?>
    <br><input type="submit" name="makepurchase" value="Confirm Purchase">
    </form>
<?php
} ?>