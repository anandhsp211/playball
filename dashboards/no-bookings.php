<h3 style="font-size:18px;">No Bookings Report</h3>
<a href="dashboards/no-bookings-csv.php">Export to CSV</a><br><br>
<table width="100%">
    <tr>    
            <td class="ls_top"><strong>Guardian Name</strong></td>
            
            <td class="ls_top"><strong>Email</strong></td>
            <td class="ls_top"><strong>Phone</strong></td>
            <td class="ls_top"><strong>Child's Name</strong></td>
            <td class="ls_top"><strong>Gender</strong></td>
            <td class="ls_top"><strong>DOB</strong></td>
        </tr>
<?php
// define master in this case is kids
$sql = "SELECT recordid, hashid, childsname, gender, birthday FROM `udf_63538FE6EF330C13A05A3ED7E599D5F7` WHERE issaved = 1 ".$addsqldepartments;
//echo $sql."<br>";
$res = mysql_query($sql);
while ($row = mysql_fetch_array($res)) {
    
    $sqli = "SELECT rv.recordid, udf.classtype FROM relations_values rv
                INNER JOIN udf_2BB232C0B13C774965EF8558F0FBD615 udf ON udf.hashid = rv.recordid
                WHERE location = '2BB232C0B13C774965EF8558F0FBD615' 
                AND matrix_id = 364 AND parent_recordid = '".$row['hashid']."'
                AND udf.classtype LIKE 'Class' LIMIT 1";    
    //echo $sqli."<br>";
    $resi = mysql_query($sqli);
    $rowi = mysql_fetch_array($resi);
    if ($rowi['recordid'] == "") {
        include("includes/ls.php");
        
        $sqlp = "SELECT `child_record` FROM multilink
                    WHERE `parent_table` = '63538FE6EF330C13A05A3ED7E599D5F7'
                    AND `parent_record` = '".$row['hashid']."'
                    AND `child_table` = '45C48CCE2E2D7FBDEA1AFC51C7C6AD26'";
        //echo $sqlp."<br>";
        $resp = mysql_query($sqlp);
        $rowp = mysql_fetch_array($resp);
        
        $sqll = "SELECT id, hashid, fullname, mobile, email, issaved FROM `udf_45C48CCE2E2D7FBDEA1AFC51C7C6AD26`
                WHERE hashid = '".$rowp['child_record']."' ".$addsqldepartments;
        //echo $sqll."<br>";
        $resl = mysql_query($sqll);
        $rowl = mysql_fetch_array($resl);
        
        if ($rowl['issaved'] == "-1") {
            // Noticed an anomoly where some people are not set to saved.  Bit of a hack fix.
            //$sqlu = "UPDATE `udf_45C48CCE2E2D7FBDEA1AFC51C7C6AD26` SET `issaved` = '1' WHERE `id` = ".$rowl['id']." LIMIT 1;";
            //mysql_query($sqlu);
            $isdeleted = 1;
        }
        
        // Now that we have the parent we need to find all associated siblings
        $sqln = "SELECT `child_record` FROM multilink
                WHERE `parent_table` = '45C48CCE2E2D7FBDEA1AFC51C7C6AD26'
                AND `child_table` = '63538FE6EF330C13A05A3ED7E599D5F7'
                AND `parent_record` = '".$rowl['hashid']."' AND child_record != '".$row['hashid']."'; ";
        //echo $sqln."<br><br>";
        $resn = mysql_query($sqln);
        $num_rows = mysql_num_rows($resn);
        if ($num_rows >= 1) {
            $checker = 0;
            while ($rown = mysql_fetch_array($resn)) {
                // Lets check if they have more kids and if the other kids have a booking.  If they do then exlcude
                $sqlz = "SELECT recordid, menutabid FROM relations_values
                            WHERE location = '2BB232C0B13C774965EF8558F0FBD615' AND matrix_id = 364
                            AND parent_recordid = '".$rown['child_record']."' LIMIT 1";
                //echo $sqlz."<br><br>";
                $resz = mysql_query($sqlz);
                $rowz = mysql_fetch_array($resz);
                
                if ($rowz['recordid'] != "") {
                    $checker = 1;
                }
                
            }
            
        }
        if ($checker == 0) { ?>
            <tr>
                <td class="ls_<?php echo $ls ?>_big">
                    <?php echo $rowl['fullname'];?>
                    <?php
                    if ($isdeleted == 1) {
                        echo "<br /><font style='font-size:11px;color:red;'>This record has been deleleted.  Restore the Guardian or delete the Kid to remove them from this report.</font>";
                    }
                    ?>
                </td>
                <td class="ls_<?php echo $ls ?>_big"><a target="_playball_nobook" href="thinline.php?id=2:<?php echo $rowl['hashid'];?>:::::::::::::::::::A87FF679A2F3E71D9181A67B7542122C:45C48CCE2E2D7FBDEA1AFC51C7C6AD26:2::"><?php echo $rowl['email'];?></a></td>
                <td class="ls_<?php echo $ls ?>_big"><?php echo $rowl['mobile'];?></td>
                <td class="ls_<?php echo $ls ?>_big"><a target="_playball_nobook" href="thinline.php?id=2:<?php echo $row['hashid'];?>:::::::::::::::::::8F85517967795EEEF66C225F7883BDCB:63538FE6EF330C13A05A3ED7E599D5F7:2::"><?php echo $row['childsname'];?></a></td>
                <td class="ls_<?php echo $ls ?>_big"><?php echo $row['gender'];?></td>
                <td class="ls_<?php echo $ls ?>_big"><?php echo $row['birthday'];?></td>
            </tr>
    <?php
        }
        unset($changeissaved, $checker, $isdeleted);
    }
} ?>
</table>
<br><a href="dashboards/no-bookings-csv.php">Export to CSV</a><br>