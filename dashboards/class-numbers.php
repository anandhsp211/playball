<?php
if ($gval[2] == 1) { ?>
    <h3 style="font-size:18px;">Class/Camp Numbers Report</h3>
    <form method="POST" action="thinline.php?id=51:6:2::::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A">
        <table style="background: #efefef; width:100%;" cellpadding="5" border="0">
            <tr>
                <td colspan="2"><h3>Select Term</h3></td>
            </tr>
            <tr>
                <td>
                    <?php
                    $seasonArray = array("Spring","Summer","Autumn");
                    ?>
                    <select name="season" style="padding:3px;font-size:14px;">
                        <option value="">Select One</option>
                        <?php
                        foreach ($seasonArray AS $season) { ?>
                            <option value="<?php echo $season;?>"><?php echo $season;?></option>
                        <?php
                        } ?>
                    </select>
                    <select name="year" style="padding:3px;font-size:14px;">
                        <?php
                        $startYear = 2014;
                        $endYear = date("Y") + 5;
                        $currentYear = date("Y");
                        for ($x = $startYear; $x<=$endYear; $x++) {
                            $selected = ($x == date("Y")) ? "selected" : "" ;
                            ?>
                            <option <?php echo $selected;?> value="<?php echo $x;?>"><?php echo $x;?></option>
                        <?php
                        } ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan=2>
                    <input type="submit" name="rebooklist" value="Get Class/Camp Numbers List" style="padding:5px;font-size:14px;">
                </td>
            </tr>
        </table>
    </form>
    <br>
<?php
} elseif ($gval[2] == 2) {?>
    <h3 style="font-size:18px;">Class/Camp Numbers for <?php echo $_POST['season']." ".$_POST['year'];?></h2>
    <a href="dashboards/class-numbers-csv.php?season=<?php echo $_POST['season'];?>&year=<?php echo $_POST['year'];?>">Export to CSV</a><br><br>
    <table  border="0" width="100%" style="font-family:Arial;font-size: 14px;">
        <tr>
            <td class="ls_top"><strong>Class ID</td>
            <td class="ls_top"><strong>Class Name</td>
            <td class="ls_top"><strong>Type</td>
            <td class="ls_top"><strong>Day</td>
            <td class="ls_top"><strong>Places</td>
            <td class="ls_top"><strong>Bookings</td>
            <td class="ls_top"><strong>Places Remaining</td>
            <td class="ls_top"><strong>Waiting List</td>
        </tr>
    <?php
    $sql = "SELECT recordid, classname, type, places, classidentifier FROM `udf_2B8A61594B1F4C4DB0902A8A395CED93`
                WHERE `termtime` = '".mysql_real_escape_string($_POST['season'])."'
                AND `year` = '".mysql_real_escape_string($_POST['year'])."' ".$addsqldepartments." AND issaved = 1
                ORDER BY classname  ASC";
    //echo $sql."<br>";
    $res = mysql_query($sql);
    while ($row = mysql_fetch_array($res)) {
        include("includes/ls.php");
        
            if ($row['type'] == "Camps") {
                
                // Get the total numbers
                $sqli = "SELECT COUNT(id) AS numberKids, campbookingdate FROM `udf_2BB232C0B13C774965EF8558F0FBD615`
                            WHERE issaved = 1
                            AND `termtime` = '".mysql_real_escape_string($_POST['season'])."'
                            AND `year` = '".mysql_real_escape_string($_POST['year'])."'
                            AND classname = '".mysql_real_escape_string($row['classname'])."'
                            ".$addsqldepartments."
                            AND bookingtype = 'Booking'
                            GROUP BY campbookingdate; ";
                //echo $sqli."<br>";
                $resi = mysql_query($sqli);
                while ($rowi = mysql_fetch_array($resi)) {
                    
                    echo "<tr>";
                
                    echo "<td class='ls_".$ls."' style='font-size:14px;'>".$row['recordid']."</td>";
                    echo "<td class='ls_".$ls."' style='font-size:14px;'>".$row['classname']."</td>";
                    echo "<td class='ls_".$ls."' style='font-size:14px;'>".$row['type']."</td>";
                    echo "<td class='ls_".$ls."' style='font-size:14px;'>".$rowi['campbookingdate']."</td>";
                    echo "<td class='ls_".$ls."' style='font-size:14px;'>".$row['places']."</td>";
                    
                    echo "<td class='ls_".$ls."' style='font-size:14px;'>".$rowi['numberKids']."</td>";
                    echo "<td class='ls_".$ls."' style='font-size:14px;'>".($row['places']-$rowi['numberKids'])."</td>";
                
                
                
                    // Get the total numbers
                    $sqlii = "SELECT COUNT(id) AS numberKids FROM `udf_2BB232C0B13C774965EF8558F0FBD615`
                                WHERE issaved = 1
                                AND `termtime` = '".mysql_real_escape_string($_POST['season'])."'
                                AND `year` = '".mysql_real_escape_string($_POST['year'])."'
                                AND classname = '".mysql_real_escape_string($row['classname'])."'
                                ".$addsqldepartments."
                                AND bookingtype = 'Waiting List'
                                GROUP BY campbookingdate; ";
                    //echo $sqli."<br>";
                    $resii = mysql_query($sqlii);
                    $rowii = mysql_fetch_array($resii);
                    echo "<td class='ls_".$ls."' style='font-size:14px;'>".$rowii['numberKids']."</td>";
                    
                    echo "</tr>";
                    
                    
                }
                
            } else {
                
                echo "<tr>";
                
                echo "<td class='ls_".$ls."' style='font-size:14px;'>".$row['recordid']."</td>";
                echo "<td class='ls_".$ls."' style='font-size:14px;'>".$row['classname']."</td>";
                echo "<td class='ls_".$ls."' style='font-size:14px;'>".$row['type']."</td>";
                echo "<td class='ls_".$ls."' style='font-size:14px;'></td>";
                echo "<td class='ls_".$ls."' style='font-size:14px;'>".$row['places']."</td>";
            
                // Get the total numbers
                $sqli = "SELECT COUNT(id) AS numberKids FROM `udf_2BB232C0B13C774965EF8558F0FBD615`
                            WHERE issaved = 1
                            AND `termtime` = '".mysql_real_escape_string($_POST['season'])."'
                            AND `year` = '".mysql_real_escape_string($_POST['year'])."'
                            AND classname = '".mysql_real_escape_string($row['classname'])."'
                            ".$addsqldepartments."
                            AND bookingtype = 'Booking'; ";
                //echo $sqli."<br>";
                $resi = mysql_query($sqli);
                $rowi = mysql_fetch_array($resi);
                
                echo "<td class='ls_".$ls."' style='font-size:14px;'>".$rowi['numberKids']."</td>";
                echo "<td class='ls_".$ls."' style='font-size:14px;'>".($row['places']-$rowi['numberKids'])."</td>";
                
                // Get the total numbers
                $sqli = "SELECT COUNT(id) AS numberKids FROM `udf_2BB232C0B13C774965EF8558F0FBD615`
                            WHERE issaved = 1
                            AND `termtime` = '".mysql_real_escape_string($_POST['season'])."'
                            AND `year` = '".mysql_real_escape_string($_POST['year'])."'
                            AND classname = '".mysql_real_escape_string($row['classname'])."'
                            ".$addsqldepartments."
                            AND bookingtype = 'Waiting List'; ";
                //echo $sqli."<br>";
                $resi = mysql_query($sqli);
                $rowi = mysql_fetch_array($resi);
                echo "<td class='ls_".$ls."' style='font-size:14px;'>".$rowi['numberKids']."</td>";
                
                echo "</tr>";
                
            }
        
        
    } ?>
    </table>
    <a href="dashboards/class-numbers-csv.php?season=<?php echo $_POST['season'];?>&year=<?php echo $_POST['year'];?>">Export to CSV</a><br><br>
<?php
} ?>