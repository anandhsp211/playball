<h3 style="font-size:18px;">Venue Status Change</h3>
<?php
if($gval[3] == 1) {
    $filterrec = 'class="filterhighlight"';
	$addeda = "ed";
    $orderby = " ORDER BY recordid DESC ";
} elseif ($gval[3] == 2) {
    $filteralpha = 'class="filterhighlight"';
	$addedb = "ed";
    $orderby = " ORDER BY venuename ASC ";
}
?>
<a href="/thinline.php?id=51:11:1:1:::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A" <?php echo $filteralpha;?>>Sort<?php echo $addedb;?> Alphabetical&nbsp;</a> | 
<a href="/thinline.php?id=51:11:1:2:::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A" <?php echo $filterrec;?>>Sort<?php echo $addeda;?> by Record ID&nbsp;</a>
<br /><br />
<?php
if (($_SESSION['fullname'] != "Patrick McCarthy") && ($_SESSION['fullname'] != "Jill Jacobsen")) {
   $addZeSQL = " AND udf.country = '".mysql_real_escape_string($_SESSION['countrymanager']['country'])."' ";
}
$sql = "SELECT recordid, hashid, department, createdby, venuename, address, citytown, zippostcode, stateprovincecounty, mapcode, udf.country
				FROM `udf_5737034557EF5B8C02C0E46513B98F90` udf
				WHERE issaved = 1
				".$addZeSQL."
				".mysql_real_escape_string($orderby)."";
//echo $sql."<br>";
$res = mysql_query($sql); ?>
<form method="POST">
<table width="100%">
    <tr>
        <td class="ls_on" style="font-size:14px;">
            Venue List:<br />
            <select name="venuehashid" style="font-size:14px;padding:3px;">
                <option value="">Select One</option>
                <?php
                while ($row = mysql_fetch_array($res)) {
                    echo "<option value='".$row['hashid']."'>".$row['recordid']." - ".$row['venuename']."</option>";
                }
                ?>
            </select>
            <br /><br />
            New Status:<br />
            <select name="venuestatus" style="font-size:14px;padding:3px;">
                <option value="">Select One</option>
                <option value="Approved">Approved</option>
                <option value="New Venue">New Venue</option>
                <option value="Not Approved - Duplicate Venue">Not Approved - Duplicate Venue</option>
                <option value="Not Approved - Exceeded Maximum Venues">Not Approved - Exceeded Maximum Venues</option>
                <option value="Pending Approval">Pending Approval</option>
            </select>
        </td>
    </tr>
</table>
<br /><br />
<input type="submit" name="changevenuestatus" value="Change Venue Status" onclick="return confirm('Are you sure you want to change this venues status?')" style="font-size:14px;padding:3px;" />
</form>