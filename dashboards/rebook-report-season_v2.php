<?php
if ($gval[2] == 1) { ?>
    <h3 style="font-size:18px;">Term Rebook Report</h3>
    <form method="POST" action="thinline.php?id=51:5:2::::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A">
        <table style="background: #efefef; width:50%;" cellpadding="5" border="0">
            <tr>
                <td><strong>Select Current Term</strong></td>
                <td><strong>Check Options</strong></td>
            </tr>
            <tr>
                <td width="10">
                    <?php
                    $seasonArray = array("Spring","Summer","Autumn");
                    ?>
                    <select name="season" style="padding:3px;font-size:14px;">
                        <option value="">Select One</option>
                        <?php
                        foreach ($seasonArray AS $season) { ?>
                            <option value="<?php echo $season;?>"><?php echo $season;?></option>
                        <?php
                        } ?>
                    </select>
                    <select name="year" style="padding:3px;font-size:14px;">
                        <?php
                        $startYear = 2014;
                        $endYear = date("Y") + 5;
                        $currentYear = date("Y");
                        for ($x = $startYear; $x<=$endYear; $x++) {
                            $selected = ($x == date("Y")) ? "selected" : "" ;
                            ?>
                            <option <?php echo $selected;?> value="<?php echo $x;?>"><?php echo $x;?></option>
                        <?php
                        } ?>
                    </select>
                </td>
                <td>
                    <select name="options" style="padding:3px;font-size:14px;">
                        <option value="0">Check against last term only</option>
                       <!--<option value="1">Check against all previous terms</option>-->
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan=2>
                    <input type="submit" name="rebooklist" value="Get Rebook List" style="padding:5px;font-size:14px;">
                </td>
            </tr>
        </table>
    </form>
    <br>
<?php
} elseif ($gval[2] == 2) {
    
    function getPreviousTerm ($term, $year) {
        if ($term == "Spring") {
            $previousTerm = "Autumn";
            $previousYear = ($year-1);
            return array($previousTerm,$previousYear);
        } elseif ($term == "Autumn") {
            $previousTerm = "Summer";
            $previousYear = $year;
            return array($previousTerm,$previousYear);
        } elseif ($term == "Summer") {
            $previousTerm = "Spring";
            $previousYear = $year;
            return array($previousTerm,$previousYear);
        }
    }
    
    // If options equals only last term then add some sql;
    if ($_POST['options'] == "0") {
        $checkAgainstTerm = getPreviousTerm($_POST['season'], $_POST['year']);
        $addOptionsSQL = "AND (termtime = '".mysql_real_escape_string($checkAgainstTerm[0])."' AND year = '".$checkAgainstTerm[1]."')";
    } 
    
    // get a list of all the kids
    //Get previous terms
    $previousTerm = getPreviousTerm($_POST['season'], $_POST['year']);
    $guardianHashid = array();
    $sql = "SELECT * FROM `udf_2BB232C0B13C774965EF8558F0FBD615`
            WHERE issaved = 1
            AND classtype = 'Class'
            AND termtime = '".mysql_real_escape_string($previousTerm[0])."'
            AND year = '".mysql_real_escape_string($previousTerm[1])."'
            ".$addsqldepartments." ";
    //echo $sql."<br>";
    $res = mysql_query($sql);
    $outputArray = array();
    $counter = 0;
    while ($row = mysql_fetch_array($res)) {
        
        $sqli = "SELECT id FROM `udf_2BB232C0B13C774965EF8558F0FBD615`
                WHERE issaved = 1
                AND childsname = '".mysql_real_escape_string($row['childsname'])."'
                AND termtime = '".mysql_real_escape_string($_POST['season'])."'
                AND year = '".mysql_real_escape_string($_POST['year'])."'
                ".$addsqldepartments." LIMIT 1";
        //echo $sqli."<br>";
        $resi = mysql_query($sqli);
        $rowi = mysql_fetch_array($resi);
        
        if ($rowi['id'] != "") {
            //echo "Have new term booking<br>";
            // If they have a new booking we dont want to show them for another kid
            // as per task 128 - so we get the guardian id and dont add them unless they have zero bookings
            $sqlc = "SELECT hashid FROM `udf_45C48CCE2E2D7FBDEA1AFC51C7C6AD26`
                    WHERE fullname = '".mysql_real_escape_string($row['guardiansname'])."'
                    AND issaved = 1 ".$addsqldepartments." LIMIT 1";
            //echo $sqlc."<br>";
            $resc = mysql_query($sqlc);
            $rowc = mysql_fetch_array($resc);

            $guardianHashid[$rowc['hashid']] = $rowc['hashid'];
            
        } else {
            //echo "Have no new term booking<br>";
            
            $sqlc = "SELECT hashid, marketingcommunications FROM `udf_45C48CCE2E2D7FBDEA1AFC51C7C6AD26`
                    WHERE fullname = '".mysql_real_escape_string($row['guardiansname'])."'
                    AND issaved = 1 ".$addsqldepartments." LIMIT 1";
            //echo $sqlc."<br>";
            $resc = mysql_query($sqlc);
            $rowc = mysql_fetch_array($resc);
            
            $outputArray[$counter]['guardiansname'] = $row['guardiansname'];
            $outputArray[$counter]['guardianHashId'] = $rowc['hashid'];
            $outputArray[$counter]['guardiansmobile'] = $row['guardiansmobile'];
            $outputArray[$counter]['guardiansemail'] = $row['guardiansemail'];
            $outputArray[$counter]['marketingcommunications'] = $rowc['marketingcommunications'];
            $outputArray[$counter]['bookingtype'] = $row['bookingtype'];
            $outputArray[$counter]['childsname'] = $row['childsname'];
            
            $sqlc = "SELECT hashid, birthday FROM `udf_63538FE6EF330C13A05A3ED7E599D5F7`
                    WHERE childsname = '".mysql_real_escape_string($row['childsname'])."'
                    AND issaved = 1 ".$addsqldepartments." LIMIT 1";
            //echo $sql."<br>";
            $resc = mysql_query($sqlc);
            $rowc = mysql_fetch_array($resc);
            
            $outputArray[$counter]['childhashid'] = $rowc['hashid'];
            $outputArray[$counter]['birthday'] = $rowc['birthday'];
            $outputArray[$counter]['classname'] = $row['classname'];
            $outputArray[$counter]['coach'] = $row['coach'];
            
            $counter++;
            
        }
        
       
    }

    /*echo "<pre>";
    print_r($outputArray);
    echo "</pre>";
    die();*/
    
    ?>
    
    <h3 style="font-size:18px;">Term Rebook Report for <?php echo $_POST['season']." ".$_POST['year'];?></h2>
    <a href="dashboards/rebook-report-season-csv.php?season=<?php echo $_POST['season'];?>&year=<?php echo $_POST['year'];?>">Export to CSV</a><br><br>
    <table border="0" width="100%" style="font-family:Arial;font-size: 14px;">
        <tr>
            <td class="ls_top"><strong>#</strong></td>
            <td class="ls_top"><strong>Guardian Name</strong></td>
            <td class="ls_top"><strong>Phone</strong></td>
            <td class="ls_top"><strong>Email</strong></td>
            <td class="ls_top"><strong>Opt In/Out</strong></td>
            <td class="ls_top"><strong>Child's Name</strong></td>
            <td class="ls_top"><strong>DOB</strong></td>
            <td class="ls_top"><strong>Class</strong></td>
            <td class="ls_top"><strong>Booking Type</strong></td>
            <td class="ls_top"><strong>Coach</strong></td>
        </tr>
        
        <?php
        
        if (count($outputArray) < 1) {
            include("includes/ls.php"); ?>
            <tr>
                <td colspan="5" class="ls_<?php echo $ls;?>" style="font-size:14px;">No data to compare</td>
            </tr>
        <?php
        } else {
            $zecount = 1;
            foreach($outputArray AS $rebook) {
                include("includes/ls.php");
                
                if ($guardianHashid[$rebook['guardianHashId']] != $rebook['guardianHashId']) {
                
                    ?>
                    <tr>
                        <td align="center"><?php echo ($zecount);?></td>
                        <td class="ls_<?php echo $ls;?>" style="font-size:14px;"><a target="_playball_nobook" href="thinline.php?id=2:<?php echo $rebook['guardianHashId'];?>:::::::::::::::::::A87FF679A2F3E71D9181A67B7542122C:45C48CCE2E2D7FBDEA1AFC51C7C6AD26:2::"><?php echo $rebook['guardiansname'];?></a></td>
                        <td class="ls_<?php echo $ls;?>" style="font-size:14px;">
                            <?php
                                echo $rebook['guardiansmobile'];
                                //echo ($rebook['landline'] != "") ? ", ".$rebook['landline'] : "";
                            ?>
                        </td>
                        <td class="ls_<?php echo $ls;?>" style="font-size:14px;"><?php echo $rebook['guardiansemail'];?></td>
                        <td class="ls_<?php echo $ls;?>" style="font-size:14px;"><?php echo $rebook['marketingcommunications'];?></td>
                        <td class="ls_<?php echo $ls;?>" style="font-size:14px;"><a target="_rebookreport" href="thinline.php?id=2:<?php echo $rebook['childhashid'];?>:::::::::::::::::::8F85517967795EEEF66C225F7883BDCB:63538FE6EF330C13A05A3ED7E599D5F7:2::"><?php echo $rebook['childsname'];?></a></td>
                        <td class="ls_<?php echo $ls;?>" style="font-size:14px;"><?php echo     $rebook['birthday'];?></td>
                        <td class="ls_<?php echo $ls;?>" style="font-size:14px;"><?php echo $rebook['classname'];?></td>
                        <td class="ls_<?php echo $ls;?>" style="font-size:14px;"><?php echo $rebook['bookingtype'];?></td>
                        <td class="ls_<?php echo $ls;?>" style="font-size:14px;"><?php echo $rebook['coach'];?></td>
                    </tr>
                    <?php
                    $zecount++;
                }
            }
            
        } ?>
    </table>
    <br>
    <a href="dashboards/rebook-report-season-csv.php?season=<?php echo $_POST['season'];?>&year=<?php echo $_POST['year'];?>">Export to CSV</a><br><br>
<?php
}?>