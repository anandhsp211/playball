<?php
if ($gval[2] == 1) {
    
    function createBookingArray ($classidentifier, $recordid, $newValue) {
        
        if (isset($newValue)) {
            $addSQL = " AND recordid = ".mysql_real_escape_string($newValue);
        } else {
            $addSQL = " AND recordid != ".mysql_real_escape_string($recordid)." AND recordid < ".mysql_real_escape_string($recordid);
        }
        
        $sql = "SELECT recordid, hashid FROM `udf_2B8A61594B1F4C4DB0902A8A395CED93`
            WHERE `classidentifier` = '".mysql_real_escape_string(str_replace("||",":",$classidentifier))."'
            ".$addSQL."
            AND issaved = 1
            ORDER BY `id` DESC";
        //echo $sql."<br>";
        $res = mysql_query($sql);
        $row = mysql_fetch_array($res);
        $hashid = $row['hashid'];
        $recordid = $row['recordid'];
        
        $sql = "SELECT recordid FROM relations_values
                WHERE location = '2BB232C0B13C774965EF8558F0FBD615' AND matrix_id = 390
                AND parent_recordid = '".$hashid."' AND registrantid =".RID;
        //echo $sql."<br>";
        $recordArray = array();
        $res = mysql_query($sql);
        while ($row = mysql_fetch_array($res)) {
            
            // switch booking id for kid id
            $sqli = "SELECT rv.parent_recordid AS recordid 
                        FROM udf_definitions udf, relations_matrix rx, relations_values rv, menusub_tabs mst 
                        WHERE rx.id = 364 AND rx.id = rv.matrix_id 
                        AND rx.udf_columnid = udf.hashid 
                        AND rv.recordid = '".$row['recordid']."' 
                        AND rx.parenttable = mst.hashid 
                        AND rx.registrantid = 1 
                        ORDER BY rv.id DESC LIMIT 1";
            //echo $sqli."<br>";
            $resi = mysql_query($sqli);
            $rowi = mysql_fetch_array($resi);
            
            array_push($recordArray,$rowi['recordid']);
            
        }
        
        return array ($recordArray, $recordid);
    }
    
    $compareAgainst = createBookingArray($gval[4],$gval[3]);
    
    /*echo "<pre>";
    print_r($compareAgainst);
    echo "</pre>";*/
    
    $newCamp = createBookingArray($gval[4],$compareAgainst[1],$gval[3]);
    
    /*echo "<pre>";
    print_r($newCamp);
    echo "</pre>";*/
    
    foreach ($newCamp[0] AS $bookedKid) {
        $pos = array_search($bookedKid, $compareAgainst[0]);
        unset($compareAgainst[0][$pos]);
        
    }
    
    // Now we need to get the parent record information
    $compareAgainst = array_unique($compareAgainst[0]);
    $outputArray = array();
    $counter = 0;
    foreach ($compareAgainst AS $kidHashid) {
        
        // Get Kids details
        $sqlk = "SELECT childsname, birthday FROM `udf_63538FE6EF330C13A05A3ED7E599D5F7` WHERE hashid = '".$kidHashid."' AND issaved = 1;";
        //echo $sqlk."<br>";
        $resk = mysql_query($sqlk);
        $rowk = mysql_fetch_array($resk);
        $outputArray[$counter]['childsName'] = ucwords($rowk['childsname']);
        $outputArray[$counter]['birthday'] = $rowk['birthday'];
        
        $sql = "SELECT `child_record` FROM multilink
                WHERE `parent_table` = '63538FE6EF330C13A05A3ED7E599D5F7'
                AND `parent_record` = '".$kidHashid."' AND `child_table` = '45C48CCE2E2D7FBDEA1AFC51C7C6AD26' LIMIT 1;";
        //echo $sql."<br>";
        $res = mysql_query($sql);
        $row = mysql_fetch_array($res);
        $parentRecordid = $row['child_record'];
        
        $sql = " SELECT recordid, fullname, mobile, landline, email FROM udf_45C48CCE2E2D7FBDEA1AFC51C7C6AD26
                    WHERE (hashid = '".$parentRecordid."') ".$addsqldepartments." AND issaved = 1 ORDER BY ID ASC";
        //echo $sql."<br>";
        $res = mysql_query($sql);
        $row = mysql_fetch_array($res);
        
        $outputArray[$counter]['parentRecordid'] = $row['recordid'];
        $outputArray[$counter]['fullname'] = ucwords($row['fullname']);
        $outputArray[$counter]['mobile'] = $row['mobile'];
        $outputArray[$counter]['landline'] = $row['landline'];
        $outputArray[$counter]['email'] = $row['email'];
        
        $counter++;
        
    } ?>
    <h3 style="font-size:18px;">Rebook Report</h2>
    <table border="0" width="100%" style="font-family:Arial;font-size: 14px;">
        <tr>    
            <td class="ls_top"><strong>Guardian Name</strong></td>
            <td class="ls_top"><strong>Phone</strong></td>
            <td class="ls_top"><strong>Email</strong></td>
            <td class="ls_top"><strong>Child's Name</strong></td>
            <td class="ls_top"><strong>DOB</strong></td>
        </tr>
        
        <?php
        
        if (count($outputArray) < 1) {
            include("includes/ls.php"); ?>
            <tr>
                <td colspan="5" class="ls_<?php echo $ls;?>" style="font-size:14px;">No data to compare</td>
            </tr>
        <?php
        } else {
        
            foreach($outputArray AS $rebook) {
                include("includes/ls.php"); ?>
                <tr>
                    <td class="ls_<?php echo $ls;?>" style="font-size:14px;"><?php echo $rebook['fullname'];?></td>
                    <td class="ls_<?php echo $ls;?>" style="font-size:14px;">
                        <?php
                            echo $rebook['mobile'];
                            echo ($rebook['landline'] != "") ? ", ".$rebook['landline'] : "";
                        ?>
                    </td>
                    <td class="ls_<?php echo $ls;?>" style="font-size:14px;"><?php echo $rebook['email'];?></td>
                    <td class="ls_<?php echo $ls;?>" style="font-size:14px;"><?php echo $rebook['childsName'];?></td>
                    <td class="ls_<?php echo $ls;?>" style="font-size:14px;"><?php echo $rebook['birthday'];?></td>
                </tr>
            <?php
            }
            
        } ?>
    </table>
    <br>
<?php    
} ?>