<?php
include_once("..//_globalconnect.php");
$filename = $rootpath.'document_store/temp_files/'.strtotime('now').'.csv';

function getPreviousTerm ($term, $year) {
    if ($term == "Spring") {
        $previousTerm = "Autumn";
        $previousYear = ($year-1);
        return array($previousTerm,$previousYear);
    } elseif ($term == "Autumn") {
        $previousTerm = "Summer";
        $previousYear = $year;
        return array($previousTerm,$previousYear);
    } elseif ($term == "Summer") {
        $previousTerm = "Spring";
        $previousYear = $year;
        return array($previousTerm,$previousYear);
    }
}
    
$previousTerm = getPreviousTerm($_GET['season'], $_GET['year']);;
$sql = "SELECT * FROM `udf_2BB232C0B13C774965EF8558F0FBD615`
        WHERE issaved = 1
        AND classtype = 'Class'
        AND termtime = '".mysql_real_escape_string($previousTerm[0])."'
        AND year = '".mysql_real_escape_string($previousTerm[1])."'
        ".$addsqldepartments." ";
//echo $sql."<br>";
$res = mysql_query($sql);
$outputArray = array();
$counter = 0;
while ($row = mysql_fetch_array($res)) {
    
    $sqli = "SELECT id FROM `udf_2BB232C0B13C774965EF8558F0FBD615`
            WHERE issaved = 1
            AND childsname = '".mysql_real_escape_string($row['childsname'])."'
            AND termtime = '".mysql_real_escape_string($_GET['season'])."'
            AND year = '".mysql_real_escape_string($_GET['year'])."'
            ".$addsqldepartments." LIMIT 1";
    //echo $sqli."<br>";
    $resi = mysql_query($sqli);
    $rowi = mysql_fetch_array($resi);
    
    if ($rowi['id'] != "") {
        //echo "Have new term booking<br>";
    } else {
        //echo "Have no new term booking<br>";
        
        $sqlc = "SELECT hashid, marketingcommunications FROM `udf_45C48CCE2E2D7FBDEA1AFC51C7C6AD26`
                WHERE fullname = '".mysql_real_escape_string($row['guardiansname'])."'
                AND issaved = 1 ".$addsqldepartments." LIMIT 1";
        //echo $sqlc."<br>";
        $resc = mysql_query($sqlc);
        $rowc = mysql_fetch_array($resc);
        
        $outputArray[$counter]['guardiansname'] = $row['guardiansname'];
        $outputArray[$counter]['guardiansmobile'] = $row['guardiansmobile'];
        $outputArray[$counter]['guardiansemail'] = $row['guardiansemail'];
        $outputArray[$counter]['marketingcommunications'] = $rowc['marketingcommunications'];
        $outputArray[$counter]['childsname'] = $row['childsname'];
        $outputArray[$counter]['bookingtype'] = $row['bookingtype'];
        
        $sqlc = "SELECT hashid, birthday FROM `udf_63538FE6EF330C13A05A3ED7E599D5F7`
                WHERE childsname = '".mysql_real_escape_string($row['childsname'])."'
                AND issaved = 1 ".$addsqldepartments." LIMIT 1";
        //echo $sql."<br>";
        $resc = mysql_query($sqlc);
        $rowc = mysql_fetch_array($resc);
        
        $outputArray[$counter]['birthday'] = $rowc['birthday'];
        $outputArray[$counter]['classname'] = $row['classname'];
        $outputArray[$counter]['coach'] = $row['coach'];
        
        $counter++;
    }
} 
        
$header = "Rebook Report: " .$_GET['season']." ".$_GET['year']."\n";
$header .= "Guardian Name,Number,Email,Opt In/Out, Child Name,DOB,Class Name,Booking Type,Coach";
    
foreach($outputArray AS $rebook) {
    
    $phoneNumbers = ($rebook['landline'] != "") ? ", ".$rebook['landline'] : "";
    $value .= '"' . $rebook['guardiansname'] . '"' . ",";
    $value .= '"' . $rebook['guardiansmobile'] . $phoneNumbers . '"' . ",";
    $value .= '"' . $rebook['guardiansemail'] . '"' . ",";
    $value .= '"' . $rebook['marketingcommunications'] . '"' . ",";
    $value .= '"' . $rebook['childsname'] . '"' . ",";
    $value .= '"' . $rebook['birthday'] . '"' . ",";
    $value .= '"' . $rebook['classname'] . '"' . ",";
    $value .= '"' . $rebook['bookingtype'] . '"' . ",";
    $value .= '"' . $rebook['coach'] . '"' . ",";
    
    $data .= trim( $value ) . "\n";
    unset($value);

}
$data = str_replace( "\r" , "" , $data );

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=rebookreport_export_".date("Y-m-d H:i:s").".csv");
header("Pragma: no-cache");
header("Expires: 0");
print "$header\n$data"; ?>