<?php

if ($_SESSION['franchisecountry'] == "United Kingdom") {
	$costpertext = .10;
	$currency = "&pound;";
	$currencyCode = "GBP";
} elseif ($_SESSION['franchisecountry'] == "South Africa") {
	$costpertext = .50;
	$currency = "R";
	$currencyCode = "ZAR";
} elseif (($_SESSION['franchisecountry'] == "Ireland") || ($_SESSION['franchisecountry'] == "Switzerland") || ($_SESSION['franchisecountry'] == "The Netherlands")) {
	$costpertext = .10;
	$currency = "&euro;";
	$currencyCode = "EUR";
} else {
	$costpertext = .10;
	$currency = "$";
	$currencyCode = "USD";
}
?>

<style>
    .smscell {
        background:#e9e9e9;
        font-size:14px;
        padding:15px;
    }
    .smscellsmallhead {
        background:#e9e9e9;
        font-size:14px;
        padding:5px;
    }

    .smscellsmall {
        background:#f4f4f4;
        font-size:12px;
        padding:2px;
    }
</style>
<script type="text/javascript">
	checked=false;
	function checkedAll (standardcssformlist) {
		var aa = document.getElementById('standardcssformlist');
		if (checked == false) {
			checked = true
		} else {
			checked = false
		}
		for (var i =0; i < aa.elements.length; i++) {
			aa.elements[i].checked = checked;
		}
	} </script>

<?php
$arr = array("+"," ");
$colspan = 5; ?>

<table >
    <?php
    if ($gval[1] == "") { ?>
			<h3 style="font-size:18px;">SMS Broadcast Management</h3>
            <table style="width:100%;font-size:14px;" cellpadding="5" border="0">
				<tr>
					<td class="ls_on_big">
						<a href="thinline.php?id=50:1:::::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A">Select By Class</a><br />
						<font style="font-size:12px;">Select a specific class and then generate a list of all the guardians of the chiuldren attending that class.</font>
					</td>
				</tr>
				<tr>
					<td class="ls_on_big">
						<a href="thinline.php?id=50:2:::::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A">Select By Guardian</a><br />
						<font style="font-size:12px;">Select multiple guardians.</font>
					</td>
				</tr>
				<tr>
					<td class="ls_on_big">
						<a href="thinline.php?id=50:3:::::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A">Select Age Group</a><br />
						<font style="font-size:12px;">Select an age group and generate a list of guardians with children in that group.</font>
					</td>
				</tr>
				<tr>
					<td class="ls_on_big">
						<a href="thinline.php?id=50:4:::::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A" style='background:#0088CC;padding:2px;color:white'>Buy SMS Credit</a><br />
						<font style="font-size:12px;">Add more SMS credit to your account.</font>
					</td>
				</tr>	
            </table>
    <?php
    } ?>
    <tr>
        <td colspan="<?php echo $colspan;?>">&nbsp;</td>
    </tr>
    <?php
    if ($gval[1] == 1) {
        if ($_POST['action'] == "sendbroadcastmessage") {
			if ($txterror != 1) { ?>
				<tr>
					<td colspan="<?php echo $colspan;?>"><img align="left" src="images/icons/24_shadow/check.png" /> &nbsp;&nbsp;Messages sent successfully!</td>
				</tr>
			<?php
			} else { ?>
			<tr>
					<td colspan="<?php echo $colspan;?>"><img align="left" src="images/icons/24_shadow/stop.png" /> &nbsp;&nbsp;Send failed, please buy more credit!</td>
				</tr>
			<?php
			}
        } elseif ($_POST['action'] == "fromclass") {
           // print_r($_POST['classes']);
            // First we get the bookings
            $ids = join(',',$_POST['classes']);
            $sql = "SELECT recordid FROM relations_values WHERE location = '2BB232C0B13C774965EF8558F0FBD615' AND parent_recordid IN ($ids)";
            //echo "<br />".$sql."<br />";
            $result = mysql_query($sql);
            $kidrelarray = array();
            while ($row = mysql_fetch_array($result)) {
                // Now we need to get a list of the kids and their guardians
                array_push($kidrelarray, "'".$row['recordid']."'");
            }
           // print_r($kidrelarray);
            $ids = join(',',$kidrelarray);
            // Get all the kids names
            $sql = "SELECT rv.parent_recordid AS recordid
                         FROM udf_definitions udf, relations_matrix rx, relations_values rv, menusub_tabs mst WHERE rx.id = 364
                         AND rx.id = rv.matrix_id AND rx.udf_columnid = udf.hashid
                         AND rv.recordid IN (".$ids.")
                         AND rx.parenttable = mst.hashid AND rx.registrantid = 1 ORDER BY rv.id DESC";
            //echo $sql."<br />";
            $result = mysql_query($sql);
            $guardrelarray = array();
            while ($row = mysql_fetch_array($result)) {
                array_push($guardrelarray, "'".$row['recordid']."'");
            }
            //print_r($guardrelarray);
            // Now we get the guardians details
            $ids = join(',',$guardrelarray);
            $sql = "SELECT `child_record` FROM multilink
                    WHERE `parent_table` = '63538FE6EF330C13A05A3ED7E599D5F7'
                    AND `child_table` = '45C48CCE2E2D7FBDEA1AFC51C7C6AD26'
                    AND `parent_record` IN ($ids) ;";
            //echo "<br />".$sql."<br />";
            $result = mysql_query($sql);
            $phonenum = array();
            while ($row = mysql_fetch_array($result)) {
                $phonenum[$row['child_record']] = "'".$row['child_record']."'";
            }
            /*echo "<pre>";
            print_r($phonenum);
            echo "</pre>";*/
            $ids = join(',',$phonenum);
            $sql = "SELECT hashid, fullname, mobile FROM udf_45C48CCE2E2D7FBDEA1AFC51C7C6AD26
                    WHERE mobile != '' AND hashid IN ($ids) ".$addsqldepartments." ORDER BY fullname ASC";
            //echo "<br />".$sql."<br />";
            $result = mysql_query($sql); ?>
	    <form method="POST" name="standardcssformlist" id="standardcssformlist">
            <tr>
                <td class="smscell" colspan="3" width="680">
                    <SCRIPT LANGUAGE="JavaScript">
                    <!-- Begin
                    function textCounter(field,cntfield,maxlimit) {
                        if (field.value.length > maxlimit) // if too long...trim it!
                                field.value = field.value.substring(0, maxlimit);
                                // otherwise, update 'characters left' counter
                        else
                                cntfield.value = maxlimit - field.value.length;
                    }

		    function textcheck() {
			if (document.standardcssformlist.messagebody.value == "") {
			    alert( "Enter some text." );
			} else  {
			    document.standardcssformlist.submit();
			}
		    }

                    //  End -->
                    </script>
                    <textarea placeholder="Enter your text message here and choose the recipients below." style="font-family:Arial;font-size:14px;width:100%;height:200px" name="messagebody" onKeyDown="textCounter(document.standardcssformlist.messagebody,document.standardcssformlist.remLen1,160)" onKeyUp="textCounter(document.standardcssformlist.messagebody,document.standardcssformlist.remLen1,160)"></textarea><br>
                </td>
            </tr>
            <tr>
                <td class="smscell">Characters Left: <input readonly type="text" name="remLen1" size="3" maxlength="3" value="160" style="font-family:Arial;font-size:14px"></td>
                <td  class="smscell"colspan="2" align="right">
                    <input onclick="javascript: textcheck(); return false;" style="font-size:14px;padding:5px;" type="submit" value="Send Message" />
                    <input type="hidden" name="action" value="sendbroadcastmessage" />
                </td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>


		<tr>
		    <td class="smscell">Select Guardians:</td>
		    <td class="smscell">
			<select multiple name="sessionelement[]" style="font-size:12px;padding:3px;width:600px; height:300px;">
            <?php
            while ($row = mysql_fetch_array($result)) {
				if ($_SESSION['franchisecountry'] == "United Kingdom") {
					echo $mobnumber = "+".preg_replace('/^07/','447',str_replace($arr, "", $row['mobile']));
				} elseif ($_SESSION['franchisecountry'] == "The Netherlands") {
					echo $mobnumber = "+".preg_replace('/^06/','316',str_replace($arr, "", $row['mobile']));
				} elseif ($_SESSION['franchisecountry'] == "South Africa") {
					echo $mobnumber = "+".preg_replace('/^07/','277',str_replace($arr, "", $row['mobile']));
					echo $mobnumber = "+".preg_replace('/^08/','278',str_replace($arr, "", $row['mobile']));
				} elseif ($_SESSION['franchisecountry'] == "Ireland") {
					echo $mobnumber = "+".preg_replace('/^08/','3538',str_replace($arr, "", $row['mobile']));
				} elseif ($_SESSION['franchisecountry'] == "Switzerland") {
					echo $mobnumber = "+".preg_replace('/^07/','417',str_replace($arr, "", $row['mobile']));
				} elseif ($_SESSION['franchisecountry'] == "Botswana") {
					echo $mobnumber = "+".preg_replace('/^07/','2677',str_replace($arr, "", $row['mobile']));
				} elseif ($_SESSION['franchisecountry'] == "Zimbabwe") {
					echo $mobnumber = "+".preg_replace('/^07/','2637',str_replace($arr, "", $row['mobile']));
				} elseif ($_SESSION['franchisecountry'] == "Singapore") {
					echo $mobnumber = "+".preg_replace('/^08/','608',str_replace($arr, "", $row['mobile']));
				} elseif ($_SESSION['franchisecountry'] == "Singapore") {
					echo $mobnumber = "+".preg_replace('/^08/','608',str_replace($arr, "", $row['mobile']));
				} else {
					echo $mobnumber = "+".preg_replace('/^08/','608',str_replace($arr, "", $row['mobile']));
				} ?>
                <option selected value="<?php echo $row['hashid']."||".$mobnumber;?>"><?php echo $row['fullname']." (".$mobnumber;?>)</option>
        <?php
            } ?>	</select>
			<input type="hidden" name="action" value="sendbroadcastmessage" />
		    </td>
		</tr>
	    </form>
	<?php
        } else { ?>
	<form method="POST" name="standardcssformlist" id="standardcssformlist">
	<tr>
	    <td colspan="2"><h3>Select by Class</h3></td>
	</tr>
	<tr>
	    <td class="smscell">Select Class:</td>
	    <td class="smscell">
		<?php
		$sql = "SELECT hashid, classname, recordid FROM `udf_2B8A61594B1F4C4DB0902A8A395CED93` WHERE issaved = 1 ".$addsqldepartments." ORDER BY id DESC";
		//echo $sql;
		?>
		<select multiple name="classes[]" style="font-size:12px;padding:3px;width:600px; height:300px;">
		    <?php
		    $result = mysql_query($sql);
		    while ($row = mysql_fetch_array($result)) { ?>
			<option value="'<?php echo $row['hashid'];?>'"><?php echo $row['recordid']." - ".$row['classname']." ";?></option>
		    <?php
		    } ?>
		</select>
	    </td>
	</tr>
	<tr>
	    <td colspan="<?php echo $colspan;?>">
		<br /><input style="font-size:14px;padding:5px;" type="submit" value="Generate Contact List" />
		<input type="hidden" name="action" value="fromclass" />
	    </td>
	</tr>
	</form>
    <?php
        }
    } elseif ($gval[1] == 2) {
	if ($_POST['action'] == "sendbroadcastmessage") {
	    if ($txterror != 1) { ?>
            <tr>
                <td colspan="<?php echo $colspan;?>"><img align="left" src="images/icons/24_shadow/check.png" /> &nbsp;&nbsp;Messages sent successfully!</td>
            </tr>
        <?php
	    } else { ?>
	    <tr>
                <td colspan="<?php echo $colspan;?>"><img align="left" src="images/icons/24_shadow/stop.png" /> &nbsp;&nbsp;Send failed, please buy more credit!</td>
            </tr>
	    <?php
	    }
	} else { ?>
	    <form method="POST" name="standardcssformlist" id="standardcssformlist">
	    <tr>
		<td colspan="2"><h3>Select by Guardian</h3></td>
	    </tr>
	    <tr>
		<td class="smscell" colspan="2">
		    <SCRIPT LANGUAGE="JavaScript">
			<!-- Begin
			function textCounter(field,cntfield,maxlimit) {
			    if (field.value.length > maxlimit) // if too long...trim it!
				    field.value = field.value.substring(0, maxlimit);
				    // otherwise, update 'characters left' counter
			    else
				    cntfield.value = maxlimit - field.value.length;
			}

			function textcheck() {
			    if (document.standardcssformlist.messagebody.value == "") {
				alert( "Enter some text." );
			    } else {
				    document.standardcssformlist.submit();
			    }
			}

			//  End -->
			</script>
			<textarea placeholder="Enter your text message here and choose the recipients below." style="font-family:Arial;font-size:14px;width:100%;height:200px" name="messagebody" onKeyDown="textCounter(document.standardcssformlist.messagebody,document.standardcssformlist.remLen1,160)" onKeyUp="textCounter(document.standardcssformlist.messagebody,document.standardcssformlist.remLen1,160)"></textarea><br>
		</td>
	    </tr>
	    <tr>
		<td class="smscell">Characters Left: <input readonly type="text" name="remLen1" size="3" maxlength="3" value="160" style="font-family:Arial;font-size:14px"></td>
		<td  class="smscell"colspan="2" align="right">
		    <input onclick="javascript: textcheck(); return false;" style="font-size:14px;padding:5px;" type="submit" value="Send Message" />
		    <input type="hidden" name="action" value="fromguardian" />
		</td>
	    </tr>
	    <tr>
		<td colspan="2">&nbsp;</td>
	    </tr>
	    <tr>
		<td class="smscell">Select Guardians:</td>
		<td class="smscell">
		    <?php
		    $sql = "SELECT hashid, fullname, mobile, recordid FROM `udf_45C48CCE2E2D7FBDEA1AFC51C7C6AD26`
			    WHERE issaved = 1 AND mobile != '' ".$addsqldepartments."
			    ORDER BY fullname, id ASC";
		    //echo $sql;
		    ?>
		    <select multiple name="sessionelement[]" style="font-size:12px;padding:3px;width:600px; height:300px;">
			<?php
			$result = mysql_query($sql);
			while ($row = mysql_fetch_array($result)) {
			    echo $mobnumber = "+".preg_replace('/^07/','447',str_replace($arr, "", $row['mobile'])); ?>
			    <option selected value="<?php echo $row['hashid']."||".$mobnumber;?>"><?php echo $row['fullname']." (".$mobnumber;?>)</option>
			<?php
			} ?>
		    </select>
		    <input type="hidden" name="action" value="sendbroadcastmessage" />
		</td>
	    </tr>
	    </form>
    <?php
	}
    }  elseif ($gval[1] == 3) {
	if ($_POST['action'] == "sendbroadcastmessage") { ?>

            <tr>
                <td colspan="<?php echo $colspan;?>"><img align="left" src="images/icons/24_shadow/check.png" /> &nbsp;&nbsp;Messages sent successfully!</td>
            </tr>
        <?php
	} elseif ($_POST['action'] == "fromage") {
	    // First find all the kids in the age group
	    $agerange = $_POST['agegroup'];
	    foreach ($agerange AS $age) {
		$agestr .= $age.",";
	    }
	    $agerange = explode(",", substr($agestr,0,-1));
	    $agerange= array_unique($agerange);
	    sort($agerange);
	    // get a list of kids
	    $sql = "SELECT * , DATEDIFF( CURRENT_DATE, birthday ) /365 AS ageInYears FROM  `udf_63538FE6EF330C13A05A3ED7E599D5F7` WHERE NOT ISNULL(id) ".$addsqldepartments." ";
	    //echo $sql;
	    $result = mysql_query($sql);
	    $kidarray = array();
	    while ($row = mysql_fetch_array($result)) {
		if (in_array(floor($row['ageInYears']), $agerange)) {
		    array_push($kidarray, "'".$row['hashid']."'");
		}
	    }
	    //print_r($kidarray);

	    // Grab the list of guardian ids
	    $ids = join(',',$kidarray);
            $sql = "SELECT `child_record` FROM multilink
                    WHERE `parent_table` = '63538FE6EF330C13A05A3ED7E599D5F7'
                    AND `child_table` = '45C48CCE2E2D7FBDEA1AFC51C7C6AD26'
                    AND `parent_record` IN ($ids) ;";
            //echo "<br />".$sql."<br />";
            $result = mysql_query($sql);
            $phonenum = array();
            while ($row = mysql_fetch_array($result)) {
                $phonenum[$row['child_record']] = "'".$row['child_record']."'";
            }
	    //print_r($phonenum);

	    // Grab the phone numbers
	    $ids = join(',',$phonenum);
            $sql = "SELECT hashid, fullname, mobile FROM udf_45C48CCE2E2D7FBDEA1AFC51C7C6AD26
                    WHERE mobile != '' AND hashid IN ($ids) ORDER BY fullname ASC";
            //echo "<br />".$sql."<br />";
            $result = mysql_query($sql);
	    ?>
	    <form method="POST" name="standardcssformlist" id="standardcssformlist">
	    <tr>
                <td class="smscell" colspan="3" width="680">
                    <SCRIPT LANGUAGE="JavaScript">
                    <!-- Begin
                    function textCounter(field,cntfield,maxlimit) {
                        if (field.value.length > maxlimit) // if too long...trim it!
                                field.value = field.value.substring(0, maxlimit);
                                // otherwise, update 'characters left' counter
                        else
                                cntfield.value = maxlimit - field.value.length;
                    }

		    function textcheck() {
			if (document.standardcssformlist.messagebody.value == "") {
			    alert( "Enter some text." );
			} else  {
			    document.standardcssformlist.submit();
			}
		    }

                    //  End -->
                    </script>
                    <textarea placeholder="Enter your text message here and choose the recipients below." style="font-family:Arial;font-size:14px;width:100%;height:200px" name="messagebody" onKeyDown="textCounter(document.standardcssformlist.messagebody,document.standardcssformlist.remLen1,160)" onKeyUp="textCounter(document.standardcssformlist.messagebody,document.standardcssformlist.remLen1,160)"></textarea><br>
                </td>
            </tr>
            <tr>
                <td class="smscell">Characters Left: <input readonly type="text" name="remLen1" size="3" maxlength="3" value="160" style="font-family:Arial;font-size:14px"></td>
                <td  class="smscell"colspan="2" align="right">
                    <input onclick="javascript: textcheck(); return false;" style="font-size:14px;padding:5px;" type="submit" value="Send Message" />
                    <input type="hidden" name="action" value="sendbroadcastmessage" />
                </td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>


		<tr>
		    <td class="smscell">Select Guardians:</td>
		    <td class="smscell">
			<select multiple name="sessionelement[]" style="font-size:12px;padding:3px;width:600px; height:300px;">

            <?php
            while ($row = mysql_fetch_array($result)) {
		echo $mobnumber = "+".preg_replace('/^07/','447',str_replace($arr, "", $row['mobile'])); ?>
                <option selected value="<?php echo $row['hashid']."||".$mobnumber;?>"><?php echo $row['fullname']." (".$mobnumber;?>)</option>
        <?php
            } ?>	</select>
			<input type="hidden" name="action" value="sendbroadcastmessage" />
		    </td>
		</tr>
	    </form>
	<?php
	} else { ?>
	<form method="POST" name="standardcssformlist" id="standardcssformlist">
	<tr>
	    <td colspan="2"><h3>Select by Class</h3></td>
	</tr>
	<tr>
	    <td class="smscell">Select Age Group:</td>
	    <td class="smscell">
		<select multiple name="agegroup[]" style="font-size:12px;padding:3px;width:600px; height:300px;">
		    <?php
		    $agearray = array("Two Can Do" => "2,3", "Watch Me at Three" => "3,4", "Dinkies" => "4,5", "Preps" => "5,6", "Players" => "6,7", "Pros" => "7,8");
		    foreach ($agearray AS $age => $id) { ?>
			<option value="<?php echo $id; ?>"><?php echo $age;?></option>
		    <?php
		    } ?>
		</select>
	    </td>
	</tr>
	<tr>
	    <td colspan="<?php echo $colspan;?>">
		<br /><input style="font-size:14px;padding:5px;" type="submit" value="Generate Contact List" />
		<input type="hidden" name="action" value="fromage" />
	    </td>
	</tr>
	</form>
    <?php
	}
    } elseif ($gval[1] == 4) { ?>
	<tr>
	    <td><strong>Buy More Messages</strong></td>
	</tr>
	<?php
	if (!isset($_POST['buytexts'])) {
	    ?>
	    <tr>
		<td width="50%">
		    <br>
		    <form method="POST" name="standardcssformlist" id="standardcssformlist">
		    <table border="0" width="100%" cellpadding="5">
			<tr>
			    <td style="background: #f2f2f2" align="center"><strong># Messages</strong></td>
			    <td style="background: #f2f2f2" align="center"><strong>Cost</strong></td>
			    <td style="background: #f2f2f2" align="center"><strong>Action</strong></td>
			</tr>
			<?php

			$creditarray = array(50, 100, 250, 500, 1000, 5000, 10000);
			
			foreach ($creditarray AS $val) {
				
				$newcostpertext = $val * $costpertext;
				
			    if ($val == 500) {
				$checked = "checked";
			    } ?>
			    <tr>
				<td style="background: #f9f9f9" align="right"><?php echo number_format($val,0);?></td>
				<td style="background: #f9f9f9" align="right"><?php echo $currency." ".number_format($newcostpertext,2);?></td>
				<td style="background: #f9f9f9" align="center"><input <?php echo $checked; ?> type="radio" name="smscredit" value="<?php echo $val;?>" /></td>
			    </tr>
			<?php
			unset($checked);
			} ?>
		    </table>
		</td>
	    </tr>
	    <tr>
		<td>
			<!--<br><input type="submit" name="buytexts" style="font-size:14px;padding:3px;" value="Buy with PayPal">-->
			Please email <a href="mailto: patrick@mediatomcat.net">patrick@mediatomcat.net</a> with the quantity required. 
		</td>
	    </tr>
	    </form>
    <?php
	} else { ?>
	    <tr>
		<td width="50%">
		    <br>
		    <form action="https://www.paypal.com/cgi-bin/webscr?return=http://<?php echo $domainname;?>/backoffice/thank-you.php" method="post">
		    <table border="0" width="100%" cellpadding="5">
			<tr>
			    <td style="background: #f2f2f2" align="center"><strong># Messages</strong></td>
			    <td style="background: #f2f2f2" align="center"><strong>Cost</strong></td>
			</tr>
			<tr>
			    <td style="background: #f9f9f9" align="right"><?php echo number_format($_POST['smscredit'],0);?></td>
			    <?php $totalcost = ($_POST['smscredit']*$costpertext);?>
			    <td style="background: #f9f9f9" align="right">&pound; <?php echo number_format($totalcost,2);?></td>
			</tr>
		    </table>
		    <?php
			include_once("includes/paypal-cert-crm.php");?>
		</td>
	    </tr>
	    <tr>
		<td><br><input type="submit" name="makepurchase" style="font-size:14px;padding:3px;" value="Confirm Purchase"></td>
	    </tr>
		</form>
    <?php
	}
    } ?>
</table>
