<?php

if ((!isset($_GET['season'])) ||  (!isset($_GET['year']))) {
    die("Something has gone wrong, please rerun the report.");
}

include_once("..//_globalconnect.php");
// define master in this case is kids


$sql = "SELECT hashid, childsname, birthday FROM `udf_63538FE6EF330C13A05A3ED7E599D5F7` WHERE issaved = 1 ".$addsqldepartments;
//echo $sql."<br>";
$res = mysql_query($sql);
$counter = 0;
$outputArray = array();
while ($row = mysql_fetch_array($res)) {
    
    $sql_term = ($_GET['season'] != "any") ? " AND termtime = '".mysql_real_escape_string($_GET['season'])."' " : "" ;
    $sql_year = ($_GET['year'] != "any") ? " AND year = '".mysql_real_escape_string($_GET['year'])."' " : "" ;
    
    // Now we check if they have a booking in the current term
    $sqli = "SELECT id FROM `udf_2BB232C0B13C774965EF8558F0FBD615`
                WHERE issaved = 1
                AND childsname = '".mysql_real_escape_string($row['childsname'])."'
                ".$sql_term.$sql_year."
            ".$addsqldepartments.";";
    //echo $sqli."<br />";
    $resi = mysql_query($sqli);
    $rowi = mysql_fetch_array($resi);
    
    if ($rowi['id'] == "") {
        
        $outputArray[$counter]['childhashid'] = $row['hashid'];
        $outputArray[$counter]['childsname'] = $row['childsname'];
        $outputArray[$counter]['birthday'] = $row['birthday'];
        
        // Get parent hashid
        $sqlj = "SELECT `child_record` FROM multilink
                    WHERE `parent_table` = '63538FE6EF330C13A05A3ED7E599D5F7'
                    AND `parent_record` = '".mysql_real_escape_string($row['hashid'])."'
                    AND `child_table` = '45C48CCE2E2D7FBDEA1AFC51C7C6AD26';";
        //echo $sqlj."<br>";
        $resj = mysql_query($sqlj);
        $rowj = mysql_fetch_array($resj);
        
        // Get guardian details
        $sqlc = "SELECT hashid, fullname, mobile, email, marketingcommunications FROM `udf_45C48CCE2E2D7FBDEA1AFC51C7C6AD26`
                WHERE hashid = '".mysql_real_escape_string($rowj['child_record'])."'
                AND issaved = 1 ".$addsqldepartments." LIMIT 1";
        //echo $sqlc."<br>";
        $resc = mysql_query($sqlc);
        $rowc = mysql_fetch_array($resc);
        
        $outputArray[$counter]['guardiansname'] = $rowc['fullname'];
        $outputArray[$counter]['guardianhashid'] = $rowc['hashid'];
        $outputArray[$counter]['guardiansmobile'] = $rowc['mobile'];
        $outputArray[$counter]['guardiansemail'] = $rowc['email'];
        $outputArray[$counter]['marketingcommunications'] = $rowc['marketingcommunications'];
        
        $counter++;
        
    } 
}

$sqlc = "SELECT * FROM udf_45C48CCE2E2D7FBDEA1AFC51C7C6AD26 WHERE issaved = 1 ".$addsqldepartments;
//echo $sql."<br>";
$resc = mysql_query($sqlc);
while ($rowc = mysql_fetch_array($resc)) {   
    $sqli = "SELECT `child_record` FROM multilink WHERE `parent_table` = '45C48CCE2E2D7FBDEA1AFC51C7C6AD26'
            AND `parent_record` = '".$rowc['hashid']."'
            AND `child_table` = '63538FE6EF330C13A05A3ED7E599D5F7';";
    $resci = mysql_query($sqli);
    $rowci = mysql_fetch_array($resci);
    
    if (!$rowci['child_record']) {
        //echo $sqli."<br />";
        //echo "Nothing for me<br>";
        
        $outputArray[$counter]['guardiansname'] = $rowc['fullname'];
        $outputArray[$counter]['guardianhashid'] = $rowc['hashid'];
        $outputArray[$counter]['guardiansmobile'] = $rowc['mobile'];
        $outputArray[$counter]['guardiansemail'] = $rowc['email'];
        $outputArray[$counter]['marketingcommunications'] = $rowc['marketingcommunications'];
        
        $counter++;
        
    }
}

$header = "No Class/Camps Bookings Report: " .$_GET['season']." ".$_GET['year']."\n";
$header .= "Guardian Name,Number,Email,Opt In/Out,Child Name,DOB";
    
foreach($outputArray AS $rebook) {
    
    $phoneNumbers = ($rebook['landline'] != "") ? ", ".$rebook['landline'] : "";
    $value .= '"' . $rebook['guardiansname'] . '"' . ",";
    $value .= '"' . $rebook['guardiansmobile'] . $phoneNumbers . '"' . ",";
    $value .= '"' . $rebook['guardiansemail'] . '"' . ",";
    $value .= '"' . $rebook['marketingcommunications'] . '"' . ",";
    $value .= '"' . $rebook['childsname'] . '"' . ",";
    $value .= '"' . $rebook['birthday'] . '"' . ",";
    
    $data .= trim( $value ) . "\n";
    unset($value);

}
$data = str_replace( "\r" , "" , $data );

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=rebookreport_export_".date("Y-m-d H:i:s").".csv");
header("Pragma: no-cache");
header("Expires: 0");
print "$header\n$data";

?>