<?php

// approval code in thinline.php

if (($gval[1] == 9) && ($gval[2] == 1)) { ?>
    <h3 style="font-size:18px;">Venue Approval Manager</h3>
    <table border="0" width="100%" style="font-family:Arial;font-size: 14px;">
        <tr>
            <td class="ls_top"><strong>Venue ID</td>
            <td class="ls_top"><strong>Franchise Owner</td>
			<td class="ls_top"><strong># Approved Venues</td>
            <td class="ls_top"><strong>Venue Name</td>
            <td class="ls_top"><strong>Address</td>
            <td class="ls_top"><strong>Country</td>
            <td class="ls_top"><strong>Actions</td>
        </tr>
        <?php
		if (($_SESSION['fullname'] != "Patrick McCarthy") && ($_SESSION['fullname'] != "Jill Jacobsen") && ($_SESSION['fullname'] != "Playball Master Administrator")) {
			$addZeSQL = " AND udf.country = '".mysql_real_escape_string($_SESSION['countrymanager']['country'])."' ";
		}
        $sql = "SELECT recordid, hashid, department, createdby, venuename, address, citytown, zippostcode, stateprovincecounty, mapcode, udf.country, u.username, u.email
				FROM `udf_5737034557EF5B8C02C0E46513B98F90` udf, users u 
				WHERE udf.department = u.departments
				".$addZeSQL."
				AND issaved = 1
				AND status = 'Pending Approval'
				GROUP BY department
				ORDER BY createddate ASC";
        //echo $sql."<br>";
        $res = mysql_query($sql);
		$num_rows = mysql_num_rows($res);
		$departmentArr = array();
		if ($num_rows > 0) {
			while ($row = mysql_fetch_array($res)) {
				include("includes/ls.php"); ?>
				<tr>
					<td class='ls_<?php echo $ls;?>' style="font-size:14px;"><?php echo $row['recordid'];?></td>
					<td class='ls_<?php echo $ls;?>' style="font-size:14px;"><?php echo $row['username']; ?></td>
					<td class='ls_<?php echo $ls;?>' style="font-size:14px;">
						<?php
						// Need to count the number of venues they have registered already
						if (!array_key_exists($row['department'], $departmentArr)) {
							$sqlNum = "SELECT id FROM `udf_5737034557EF5B8C02C0E46513B98F90` WHERE issaved = 1 AND status = 'Approved' AND department = ".$row['department'].";";
							//echo $sqlNum."<br>";
							$resNum = mysql_query($sqlNum);
							$numVenuyes = mysql_num_rows($resNum);
							
							// Does not exist so we push it inot the array.
							$departmentArr[$row['department']] = $numVenuyes;
						}
						
						$fontcolor =  ($departmentArr[$row['department']] >= 12) ? "red" : "green" ;
						echo "<font style='font-weight:bold;color:".$fontcolor."'>";
						echo $departmentArr[$row['department']];
						echo "</font>";
						unset($numVenuyes);
						?>
					</td>
					<td class='ls_<?php echo $ls;?>' style="font-size:14px;"><?php echo $row['venuename'];?></td>
					<td class='ls_<?php echo $ls;?>' style="font-size:14px;"><?php echo $row['address'].", ".$row['citytown'].", ".$row['zippostcode'].", ".$row['stateprovincecounty'];?></td>
					<td class='ls_<?php echo $ls;?>' style="font-size:14px;"><?php echo $row['country'];?></td>
					<td class='ls_<?php echo $ls;?>' style="font-size:14px;"> 
						<a href="thinline.php?id=51:9:2:<?php echo $row['hashid'];?>:<?php echo $row['email'];?>:<?php echo $row['username'];?>:::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A">Check Details</a> |
						<a href="thinline.php?id=51:9:3:<?php echo $row['department'];?>:<?php echo $row['username'];?>::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A">View Franchise Venue List</a>
					</td>
				</tr>
			<?php
			}
		} else {
			echo "<td colspan=7 class='ls_off' style='font-size:14px;'>No records pending approval</td>";
		} ?>
    </table>
<?php
} elseif (($gval[1] == 9) && ($gval[2] == 2)) {
	
	require(dirname(__FILE__) . '/../includes/class.damerauLevenshtein.php');
	
	// Get the location details for this record.
	$sqlVen = "SELECT * FROM `udf_5737034557EF5B8C02C0E46513B98F90`
				  WHERE hashid = '".mysql_real_escape_string($gval[3])."' AND issaved = 1 LIMIT 1";
	//echo $sqlVen."<br>";
	$resven = mysql_query($sqlVen);
	$rowven = mysql_fetch_array($resven);
	
	$sourceAddress = array("latitude"=>$rowven['latitude'],"longitude"=>$rowven['longitude'],"country"=>$rowven['country'], "zip"=>$rowven['zippostcode'],"state"=>$rowven['stateprovincecounty'],"town"=>$rowven['citytown'],"address"=>$rowven['address'], "venue"=>$rowven['venuename'],"username"=>$gval[5],"emailaddress"=>$gval[4],"venueid"=>$rowven['recordid']);
	$venueType = $rowven['venuetype'];
		
	// Get everything in that country and state
	$sql =  "SELECT * FROM `udf_5737034557EF5B8C02C0E46513B98F90`
				WHERE country = '".mysql_real_escape_string($rowven['country'])."'
				AND stateprovincecounty LIKE '%".mysql_real_escape_string($rowven['stateprovincecounty'])."%'
				AND citytown LIKE '%".mysql_real_escape_string($rowven['citytown'])."%'
				AND hashid != '".mysql_real_escape_string($gval[3])."' AND issaved = 1
				AND status = 'Approved'";
	//echo $sql."<br />";
	$res = mysql_query($sql);
	$results = array();
	$counter = 0;
	while ($row = mysql_fetch_array($res)) {
		$results[$counter]['id'] = $row['id'];
		$results[$counter]['department'] = $row['department'];
		$results[$counter]['venue'] = $row['venuename'];
		$results[$counter]['address'] = $row['address'];
		$results[$counter]['town'] = $row['citytown'];
		$results[$counter]['state'] = $row['stateprovincecounty'];
		$results[$counter]['country'] = $row['country'];
		$results[$counter]['zip'] = $row['zippostcode'];
		$results[$counter]['venuetype'] = $row['venuetype'];
		$results[$counter]['levscore'] = -1;
		$counter++;
	}
	
	$input = $rowven['venuename'];
	$shortest = -1;
	
	$counter = 0;
	foreach ($results as $words) {
	
		// First we check if the address is eaxct match
		if (($sourceAddress['address'] == $words['address']) && ($sourceAddress['town'] == $words['town'])) {
		  if ($sourceAddress['venue'] == $words['venue']) {
			$results[$counter]['levscore'] = "Venue &amp; address exact match";
		  } else {
			$results[$counter]['levscore'] = "Address and town exact match";
		  }
		} else {
		  
		  // Otherwise do a check on the venue name and report accordingly
		  $word = $words['venue'];
		  $dl = new DamerauLevenshtein($input, $word, 1,6,6,1);
		  $lev = $dl->getSimilarity();
		  //echo $lev."<br>";
		  
		  if ($lev >= 70) {
			  unset($results[$counter]);
		  } else {
			  // Add lev score to the array
			  $results[$counter]['levscore'] = "Venue name &amp; address similar";
		  }
		}
		
		$counter++;
		
	}
	
	// Count the number of array entries
	$numSimilarVenues = count($results);
	// Reset the index
	$results = array_values($results);
	// Sort by lev score
	usort($results, function($a, $b) {
		return $a['levscore'] - $b['levscore'];
	});
	
	function returnMatch ($distance) {
		
		if ($distance == 0) {
			$message = "<font color='green'>Exact Match Venue</font>";
		} else {
			$message = $distance;
		}
		return $message;
	}
	
	?>
	
	<table border="0" width="100%">
	<tr>
		<td colspan="2" style="padding: 10px;">
		  <table width="100%" cellpadding="20" cellspacing="10">
		  <tr>
			  <td colspan="4">
				  <?php
				  if ($numSimilarVenues > 0) {
					  $ifare = ($numSimilarVenues <= 1) ? array("is","venue") : array("are","venues");
					  echo "There ".$ifare[0]." <font style='font-weight:bold;font-size:18px;'>".$numSimilarVenues."</font> existing ".$ifare[1]." that ".$ifare[0]." similar to the venue being proposed.
						  <br />";
				  }
				  ?>
			  </td>
		  </tr>
		  
		  <?php
			if ($numSimilarVenues > 0) {
				$counter = 0;
				foreach ($results As $res) {
					$address = $res['venue'].",".$res['town'].",".$res['state'].",".$res['country'];
					//echo $address."<br />";
					$latLong = getLatLongFromPostcode($address);
					//print_r($latLong); 
					?>
					<tr>
						<td valign="top" style="background: #f1f1f1">
						  <strong>Proposed</strong><br />Venue Type: <?php echo $venueType;?><hr>
						<?php
						  echo "<strong>".$sourceAddress['venue']."</strong><br>";
						  echo $sourceAddress['address'].", ";
						  echo $sourceAddress['town'].", ";
						  echo $sourceAddress['state'].", ";
						  echo $sourceAddress['zip'].", ";
						  echo $sourceAddress['country'];
						  echo "<br><br>Reason: <font color='red'>".$res['levscore']."</font>";
						?>
						<br /><br />
						<div id="map_primary_<?php echo $counter;?>"></div>
						</td>
						<td style="background: #f1f1f1">
						  <strong>Existing</strong><hr>
						  <?php
						  echo "<strong>".$res['venue']."</strong><br />";
						  echo $res['address'].", ".$res['town'].", ".$res['state'].", ".$res['zip'].", ".$res['country'];
						  echo "<br><br>Reason: <font color='red'>".$res['levscore']."</font>"; ?>
						  
						  <br /><br />
						  <div id="map_<?php echo $counter;?>"></div>
						  <br /><br />
						  <?php
						  $sql = "SELECT username, email FROM users WHERE departments = '".$res['department']."' ORDER BY id ASC LIMIT 1";
						  //echo $sql;
						  $res = mysql_query($sql);
						  $row = mysql_fetch_array($res);
						  
						  echo "Franchise Owner: <strong>".$row['username']."</strong><br>";
						  echo "Email Address: <a href='mailto:".$row['email']."'>".$row['email']."</a>";
						  
						  ?>
						</td>
						<!--<td>
							<textarea style="width:400px;height:150px;"><iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" width="600" height="450" src="https://maps.google.com/maps?hl=en&q=<?php echo $address;?>&ie=UTF8&t=roadmap&z=10&iwloc=B&output=embed"><div></div></iframe></textarea>
						</td>-->
						
					</tr>
					<style>
					#map_<?php echo $counter;?> {
						height: 250px;
						width: 90%;
						padding:23px;
					}
					
					#map_primary_<?php echo $counter;?> {
						height: 250px;
						width: 90%;
						padding:23px;
					}
					</style>
				<?php
					$counter++;
				}
			} else { ?>
				
				<td valign="top" style="background: #f1f1f1">
					<strong>Proposed</strong><br />Venue Type: <?php echo $venueType;?><hr>
					<?php
						echo "<strong>".$sourceAddress['venue']."</strong><br>";
						echo $sourceAddress['address'].", ";
						echo $sourceAddress['town'].", ";
						echo $sourceAddress['state'].", ";
						echo $sourceAddress['zip'].", ";
						echo $sourceAddress['country'];
						echo "<br><br>Reason: <font color='green'>No similar venues found</font>";
					?>
					<br /><br />
					<div id="map_primary"></div>
				</td>
				
				<style>
					#map_primary {
						height: 250px;
						width: 95%;
						padding:23px;
					}
				</style>
			<?php
			} ?>
		  
		  <script>
			  var map;
			  function initMap() {
				  <?php
					if ($numSimilarVenues > 0) {
						$counter = 0;
						foreach ($results AS $res) {
							$address = $res['address'].", ".$res['town'].",".$res['state'].",".$res['country'];
							$latLong = getLatLongFromPostcode($address); ?>
							map = new google.maps.Map(document.getElementById('map_<?php echo $counter;?>'), {
								center: {lat: <?php echo $latLong[0];?>, lng: <?php echo $latLong[1];?>},
								zoom: 15
							});
							
							var marker = new google.maps.Marker({
								position: {lat: <?php echo $latLong[0];?>, lng: <?php echo $latLong[1];?>},
								map: map,
								title: '<?php echo mysql_real_escape_string($res['venue']);?>'
							});
							
							// Add circle overlay and bind to marker
							var circle = new google.maps.Circle({
								map: map,
								radius: 400,    // 10 miles in metres
								strokeColor: '#FF0000',
								strokeOpacity: 0.4,
								strokeWeight: 2,
								fillColor: '#FF0000',
								fillOpacity: 0.2
							});
							circle.bindTo('center', marker, 'position');
							
							map = new google.maps.Map(document.getElementById('map_primary_<?php echo $counter;?>'), {
								center: {lat: <?php echo $sourceAddress['latitude'];?>, lng: <?php echo $sourceAddress['longitude'];?>},
								zoom: 15
							});
							
							var marker = new google.maps.Marker({
								position: {lat: <?php echo $sourceAddress['latitude'];?>, lng: <?php echo $sourceAddress['longitude'];?>},
								map: map,
								title: '<?php echo mysql_real_escape_string($sourceAddress['venue']);?>'
							});
							
							// Add circle overlay and bind to marker
							var circle = new google.maps.Circle({
								map: map,
								radius: 400,    // 10 miles in metres
								strokeColor: '#FF0000',
								strokeOpacity: 0.4,
								strokeWeight: 2,
								fillColor: '#FF0000',
								fillOpacity: 0.2
							});
							circle.bindTo('center', marker, 'position');
							
						<?php
							$counter++;
						}
					} else { ?>
					
						map = new google.maps.Map(document.getElementById('map_primary'), {
								center: {lat: <?php echo $sourceAddress['latitude'];?>, lng: <?php echo $sourceAddress['longitude'];?>},
								zoom: 15
							});
							
							var marker = new google.maps.Marker({
								position: {lat: <?php echo $sourceAddress['latitude'];?>, lng: <?php echo $sourceAddress['longitude'];?>},
								map: map,
								title: '<?php echo mysql_real_escape_string($sourceAddress['venue']);?>'
							});
							
							// Add circle overlay and bind to marker
							var circle = new google.maps.Circle({
								map: map,
								radius: 400,    // 10 miles in metres
								strokeColor: '#FF0000',
								strokeOpacity: 0.4,
								strokeWeight: 2,
								fillColor: '#FF0000',
								fillOpacity: 0.2
							});
							circle.bindTo('center', marker, 'position');
					<?php
					} ?>
				  
				  
				  
			  }
		  </script>
		  <tr>
			  <td colspan="3">
				<form method="POST">
				<strong>Add note for Franchise Owner (not required)</strong><br />
				<font style="font-size: 12px;">When you submit your decision, an email will be sent to the <br />franchise owner.  This note will be includes in this email.</font><br /><br />
				<textarea name="approvalnote" style="width: 550px; height: 100px;"></textarea>
				<br /><br />
				If you have a question about this submission, you can email <?php echo $gval[5];?> at <a href="mailto: <?php echo $gval[4];?>"><?php echo $gval[4];?></a>.
				<br /><br />
					<input style="font-size:14px;padding:3px;" type="submit" name="approval" value="Approved" /> <br /><br />
					<strong>Rejected - Select Reason - if applicable</strong><br /><br />
					<select name="rejectionreason" style="font-size:14px;padding:3px;">
						<option value="Exceeded Maximum Venues">Exceeded Maximum Venues</option>
						<option value="Duplicate Venue">Duplicate Venue</option>
						<option value="Other">Other - Add Note</option>
					</select><br /><br />
					<input style="font-size:14px;padding:3px;" type="submit" name="approval" value="Reject" /> <br /><br />
					<input type="hidden" name="hashid" value="<?php echo $gval[3];?>" />
					<input type="hidden" name="venuemanager" value="1" />
					<?php $_SESSION['venuedetails'] = $sourceAddress;?>
				</form>
			  </td>
		  </tr>
		  </table>
		  <br /><br />
		  <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $googleMapsAPIKey;?>&callback=initMap" async defer></script>
		</td>
	</tr>
	</table>
<?php
} elseif (($gval[1] == 9) && ($gval[2] == 3)) { ?>
	<h3 style="font-size:18px;">Franchise Venue List</h3>
	Franchise Owner: <strong><?php echo $gval[4];?></strong><br /><br />
    <table  border="0" width="100%" style="font-family:Arial;font-size: 14px;">
        <tr>
            <td class="ls_top"><strong>Venue ID</td>
            <td class="ls_top"><strong>Venue Name</td>
            <td class="ls_top"><strong>Address</td>
			<td class="ls_top"><strong>Map</td>
        </tr>
        <?php
        $sql = "SELECT recordid, venuename, address, citytown, zippostcode, stateprovincecounty, mapcode, country
				FROM `udf_5737034557EF5B8C02C0E46513B98F90`
				WHERE issaved = 1
				AND department = '".mysql_real_escape_string($gval[3])."'
				AND status = 'Approved'
				ORDER BY venuename ASC";
        //echo $sql."<br>";
        $res = mysql_query($sql);
		while ($row = mysql_fetch_array($res)) {
			include("includes/ls.php"); ?>
			<tr>
				<td class='ls_<?php echo $ls;?>' style="font-size:14px;"><?php echo $row['recordid'];?></td>
				<td class='ls_<?php echo $ls;?>' style="font-size:14px;"><?php echo $row['venuename'];?></td>
				<td class='ls_<?php echo $ls;?>' style="font-size:14px;"><?php echo $row['address'].", ".$row['citytown'].", ".$row['stateprovincecounty'].", ".$row['zippostcode'].", ".$row['country'];?></td>
				<?php
				$mapCode = $row['mapcode'];
				$mapCode = str_replace('height="450"','height="250"',$mapCode);
				$mapCode = str_replace('width="600"','width="95%"',$mapCode);
				?>
				<td class='ls_<?php echo $ls;?>' style="font-size:14px;width:40%" align="center"><?php echo $mapCode;?></td>
			</tr>		
		<?php
		}?>
	</table>
<?php
} ?>