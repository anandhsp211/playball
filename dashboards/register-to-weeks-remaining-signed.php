<?php
include_once("..//_dbaccessdetails.php");
$filename = $rootpath.'document_store/temp_files/'.strtotime('now').'.csv';

function findage($dob) {
    $localtime = getdate();
        $today = $localtime['year']."-".$localtime['mon']."-".$localtime['mday'];
        
        $ts1 = strtotime($today);
        $ts2 = strtotime($dob);
        
        $year1 = date('Y', $ts1);
        $year2 = date('Y', $ts2);
        
        $month1 = date('m', $ts1);
        $month2 = date('m', $ts2);
        
        $diff = (($year1 - $year2) * 12) + ($month1 - $month2);
        $years = floor($diff/12)."<br>";
        $months = 12 * (($diff/12) - floor($diff/12));
        
        return "$years years $months months";
}

// To get all related record ids from a parent
// Usage = generateRecList(<parentmenuid>,<resultsmenuid>);
function generateRecList($parentmenuid, $childmenuid) {

    $reclist = array();
    // Get the recordids for the linked table
    $sql = "SELECT recordid FROM  `relations_values`
            WHERE parent_recordid =  '".mysql_escape_string($parentmenuid)."'
            AND matrix_id = 390
            AND location =  '".mysql_escape_string($childmenuid)."';";
    //echo $sql."<br><br>";
    $res = mysql_query($sql);
    while ($row = mysql_fetch_array($res)) {
        array_push($reclist,$row['recordid']);
    }
    return ($reclist);
}

// Pull linked record data for the results of generateRecList
// Usage pullRecData(<hashids from generateRecList>, <parentmenuid>, <columns to display>)
function pullRecData($hashid, $parentmenuid, $columns, $filterDate) {

    //$recdata = array();
    // Get the recordids for the linked table
    
    if ($filterDate != "") {
        $addsql = " AND campbookingdate = '".$filterDate."'";
        $loadExcluder = 1;
    } else {
        $loadExcluder = 0;
    }
    
    $sql = "SELECT ".mysql_escape_string($columns)." FROM  `udf_".mysql_escape_string($parentmenuid)."`
            WHERE hashid =  '".mysql_escape_string($hashid)."' ".$addsql." LIMIT 1;";
    //echo $sql."<br><br>";
    $res = mysql_query($sql);
    $row = mysql_fetch_array($res);
    
    $columnsarr = explode(",",$columns);
    
    foreach ($columnsarr AS $cols) {
        if ($loadExcluder == 1) {
            if ($row['campbookingdate'] != "") {
                $recdata['parenthashid'] = $hashid;
                $recdata[$cols] .= $row[$cols];
            }
        } else {
            $recdata['parenthashid'] = $hashid;
            $recdata[$cols] .= $row[$cols];
        }
    }
    
    return ($recdata);
}

function fromOnetoOne($parentrecordid, $childmatrixid, $childmenuid, $columns) {

    $recdata = array();
    $sql = "SELECT rv.parent_recordid AS recordid
            FROM udf_definitions udf, relations_matrix rx, relations_values rv, menusub_tabs mst
            WHERE rx.id = ".mysql_escape_string($childmatrixid)."
            AND rx.id = rv.matrix_id
            AND rx.udf_columnid = udf.hashid
            AND rv.recordid = '".mysql_escape_string($parentrecordid)."'
            AND rx.parenttable = mst.hashid AND rx.registrantid = 1 LIMIT 1";
    //echo $sql."<br>";
    $res = mysql_query($sql);
    $row = mysql_fetch_array($res);
    $childrecordid = $row['recordid'];

    $sql = "SELECT ".mysql_escape_string($columns)."
            FROM udf_".mysql_escape_string($childmenuid)."
            WHERE hashid = '".mysql_escape_string($childrecordid)."' LIMIT 1";
    //echo $sql."<br>";
    $res = mysql_query($sql);
    $row = mysql_fetch_array($res);
    $columnsarr = explode(",",$columns);
    foreach ($columnsarr AS $cols) {
        $recdata[$cols] .= $row[$cols];
    }
    return ($recdata);

}

function oneToMany($parenthashid,$menuid,$childmenuid,$columns) {

    $primarr = array();
    $recdata = array();

    $sql = "SELECT `child_record` FROM multilink
            WHERE `parent_table` = '".mysql_escape_string($menuid)."'
            AND `parent_record` = '".mysql_escape_string($parenthashid)."'
            AND `child_table` = '".mysql_escape_string($childmenuid)."';";
    //echo $sql."<br>";
    $res = mysql_query($sql);
    $counter = 0;
    while ($row = mysql_fetch_array($res)) {
        $sqli = "SELECT ".mysql_escape_string($columns)." FROM udf_".mysql_escape_string($childmenuid)."
                WHERE hashid = '".mysql_escape_string($row['child_record'])."' LIMIT 1;";
        //echo $sqli."<br>";
        $resi = mysql_query($sqli);
        $rowi = mysql_fetch_array($resi);
        $columnsarr = explode(",",$columns);
        foreach ($columnsarr AS $cols) {
            $recdata[$cols] = $rowi[$cols];
        }
        $primarr[$counter] = $recdata;
        $counter++;

    }
    return ($primarr);
}

// Start with the primary search - In our case finidng all bookings for a specific class - This produces a hashid list
// in an array.
$gval = explode(":",$_GET['id']);
$hashrecordidarr = generateRecList($gval[0],'2BB232C0B13C774965EF8558F0FBD615');  ?>

<?php
$counter = 0;
// run through the array to either out the hashids found or any related
// linked fields and continually add them to the final array
foreach ($hashrecordidarr AS $val) {

        $recdata[$counter] = pullRecData($val, '2BB232C0B13C774965EF8558F0FBD615', 'paid,bookingtype,campbookingdate,notes,photosallowed,registrationfee', $gval[2]);
        
        if (!empty($recdata[$counter])) {
            ///////// Get kid information (or 1 to 1 relationship)  //////////
            $cols = 'hashid,childsname,birthday,notesmedical,tshirt,leftrighthanded';
            $kidrecordidarr = fromOnetoOne($val,'364','63538FE6EF330C13A05A3ED7E599D5F7',$cols);
            $colsarr = explode(",",$cols);
            foreach ($colsarr AS $newcols) {
                    $recdata[$counter][$newcols] .= $kidrecordidarr[$newcols];
            }
    
            ///////// Get Guardian information  (many to many relationship) multilink-output.php //////////
            $cols = 'fullname,mobile,landline,email';
            $guardianredordidarr = oneToMany($kidrecordidarr['hashid'],'63538FE6EF330C13A05A3ED7E599D5F7','45C48CCE2E2D7FBDEA1AFC51C7C6AD26',$cols);
            $gcounter = 0;
            foreach ($guardianredordidarr AS $garrays) {
                $colsarr = explode(",",$cols);
                foreach ($colsarr AS $newcols) {
                    $recdata[$counter]['guardians'][$gcounter][$newcols] .= $garrays[$newcols];
                }
                $gcounter++;
            }
    
            ///////// Get class information (or 1 to 1 relationship)  //////////
            $cols = 'classname,venue,coach,agegroup';
            $classrecordidarr = fromOnetoOne($val,'390','2B8A61594B1F4C4DB0902A8A395CED93',$cols);
            $colsarr = explode(",",$cols);
            foreach ($colsarr AS $newcols) {
                    $recdata[$counter][$newcols] .= $classrecordidarr[$newcols];
            }
    
            // Dont touch counter
            $counter++;
        }
}

// Special for sessions
$sessionarr = array();
$sql = "SELECT session_date, session_start_time, session_end_time
         FROM `class_sessions` WHERE parent_hashid = '".$gval[0]."' AND location = '2B8A61594B1F4C4DB0902A8A395CED93' ORDER BY session_date ASC";
//echo $sqlc."<br>";
$res = mysql_query($sql);
$sshcounter = 0;
while ($row = mysql_fetch_array($res)) {
    $sessionarr[$sshcounter]['session_date'] = $row['session_date'];
    $sessionarr[$sshcounter]['session_start_time'] = $row['session_start_time'];
    $sessionarr[$sshcounter]['session_end_time'] = $row['session_end_time'];
    $sshcounter++;
}

// Create CSV header
$header = "Class Name: " . $recdata[0]['classname']."\n";
if ($gval[2] != "") {
    $header .= "Camp Date: " . date("d M Y",strtotime($gval[2]))."\n"; 
}
$header .= "Age Group: " . $recdata[0]['agegroup']."\n";
$header .= "Venue: " . $recdata[0]['venue']."\n";
$header .= "Coach: " . $recdata[0]['coach']."\n\n";

$header .= "Child Name,Guardian Name,Number,Email,DOB,Notes (Med),Notes (Booking),Paid,Photos Allowed,Booking,L/R Handed,T-Shirt,Registration Fee,";
$numsessions = count($sessionarr);
$countHeaderEntries = 0;
if ($gval[2] != "") {
        $header .= date("d M Y",strtotime($gval[2])).",";
        $header .= date("d M Y",strtotime($gval[2])).",";
} else {
    for($w = 0; $w < $numsessions; $w++) {
        $sessiondate = strtotime($sessionarr[$w]['session_date']);
        if ($sessiondate >= time()) {
            $header .= date("d-M",strtotime($sessionarr[$w]['session_date'])).",";
            $header .= date("d-M",strtotime($sessionarr[$w]['session_date'])).",";
            $countHeaderEntries++;
        }
    }
}

$header .= "\n , , , , , , , , , , , , ,";
if ($gval[2] != "") {
    $header .= "Sign In,";
    $header .= "Sign Out,";
} else {
    for($w = 0; $w < $numsessions; $w++) {
        $header .= "Sign In,";
        $header .= "Sign Out,";
        if ($w == ($countHeaderEntries-1)) {
            break;
        }
    }
}

$numrecords = count($recdata);
$y = 0;
for($x = 0; $x < $numrecords; $x++) {

    $value .= '"' . $recdata[$x]['childsname'] . '"' . ",";
    $value .= '"' . $recdata[$x]['guardians'][$y]['fullname'] . '"' . ",";
    $guardianstr .= $recdata[$x]['guardians'][$y]['mobile'] != "" ? $recdata[$x]['guardians'][$y]['mobile'].", " : "";
    $guardianstr .= $recdata[$x]['guardians'][$y]['landline'] != "" ? $recdata[$x]['guardians'][$y]['landline']."" : "";
    $value .= '"' . $guardianstr . '"' . ",";
    $value .= '"' . $recdata[$x]['guardians'][$y]['email'] . '"' . ",";
    $value .= '"' . $recdata[$x]['birthday'] . '"' . ",";
    $value .= '"' . $recdata[$x]['notesmedical'] . '"' . ",";
    $value .= '"' . " ". $recdata[$x]['notes'] . '"' . ",";
    $value .= '"' . $recdata[$x]['paid'] . '"' . ",";
    $photosAllowed = ($recdata[$x]['photosallowed']=="checkboxon") ? "Yes" : "No";
    $value .= '"' . $photosAllowed . '"' . ",";
    $value .= '"' . $recdata[$x]['bookingtype'] . '"' . ",";
    $handed = ($recdata[$x]['leftrighthanded'] == "Right Handed") ? "RH" : "LH";
    $value .= '"' . $handed . '"' . ",";
    $value .= '"' . $recdata[$x]['tshirt'] . '"' . ",";
    $value .= '"' . $recdata[$x]['registrationfee'] . '"' . ",";
    
    if ($gval[2] != "") {
        $value .= '""' . ",";
    } else {
        for($w = 0; $w < $numsessions; $w++) {
            $value .= '""' . ",";
        }
    }

    $line .= $value;
    $data .= trim( $value ) . "\n";
    unset($line,$value,$guardianstr);
}
$data = str_replace( "\r" , "" , $data );
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=register_export_".date("Y-m-d H:i:s").".csv");
header("Pragma: no-cache");
header("Expires: 0");
print "$header\n$data"; ?>