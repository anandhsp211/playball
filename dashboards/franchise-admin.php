<?php
$tab_select = mysql_real_escape_string($_GET['tab']); ?>
<?php
if ($gval[2] == 1) {
    $search = mysql_real_escape_string($_GET['search']);
    $tab_select = mysql_real_escape_string($_GET['tab']);
    $country = mysql_real_escape_string($_GET['country']);
//    var_dump($country);
    $addsql = '';
    if ($_GET['country'] != "" && isset($tab_select) && $tab_select == 2) {
        $addsql = " AND country = '" . mysql_real_escape_string($_GET['country']) . "' ";
    }
    if (isset($search) && $search && $search != 'all') {
        if ($search == 'business_address') {
            $val = ', businessaddressline1, businessaddressline2';
        } else if ($search == 'home_address') {
            $val = ', addressline1, addressline2';
        } else if ($search == 'insurance_certificate') {
            $val = ', insurancephysicalcopy, insuranceexpirydate';
        } else if ($search == 'criminal_check') {
            $val = ', criminalphysicalcopy, criminalexpirydate';
        } else if ($search == 'first_aid') {
            $val = ', firstaidphysicalcopy, firstaidexpirydate';
        } else if ($search == 'child_protection') {
            $val = ', childphysicalcopy, childexpirydate';
        } else if ($search == 'debit_order') {
            $val = ', debitphysicalcopy, debitpaymentdate';
        } else if ($search == 'franchise_contract') {
            $val = ', franchisephysicalcopy, franchiseexpirydate';
        }
        $admin_sql = "SELECT id, franchiseownera, franchiseownerb, provencecounty $val $addsql from udf_1728EFBDA81692282BA642AAFD57BE3A WHERE issaved = 1";
    } else {
        $admin_sql = "SELECT * from udf_1728EFBDA81692282BA642AAFD57BE3A WHERE issaved = 1 $addsql";
    }
//    var_dump($admin_sql);
//    die;
    $admin_res = mysql_query($admin_sql);
    $admin_list = array();
    $franchise_list = array();
    $admin_check = 1;
    while ($admin_row = mysql_fetch_assoc($admin_res, MYSQL_ASSOC)) {
        array_push($admin_list, $admin_row);
        $admin_check++;
    }
    if ($tab_select == 1) {
        $addSQLs = ($_GET['country'] != "") ? "WHERE u.country ='" . mysql_real_escape_string($_GET['country']) . "'" : "";
    }
    $franchise_sql = "select count(case when schoolstatus = 'Other' then 1 else null end) as OtherCount, count(case when schoolstatus = 'School' then 1 else null end) as SchoolCount, count(case when schoolstatus = '' then 1 else null end) as CountEmpty, count(case when schoolstatus = 'Other' AND status = 'Approved' then 1 else null end) as OtherCountActive, count(case when schoolstatus = 'School' AND status = 'Approved' then 1 else null end) as SchoolCountActive,
        u.username,
        ho.franchiseownera,
        ho.id,
        ho.franchiseownerb, 
        ho.franchiseownerb,
        ho.provencecounty,
        ho.franchiseareaa FROM 
		`udf_1728efbda81692282ba642aafd57be3a` as ho
        LEFT JOIN users as u
        ON u.departments = ho.department
		LEFT JOIN `udf_5737034557EF5B8C02C0E46513B98F90` as udf_venue		
        ON udf_venue.department = u.departments
        " . $addSQLs . " 
        AND ho.department = udf_venue.department 
        AND udf_venue.issaved = 1 
        AND ho.issaved = 1
        AND udf_venue.status = 'Approved' 
        AND u.isactive = 1 
        GROUP BY departments 
        ORDER BY ho.id ASC";
    $franchise = mysql_query($franchise_sql);
    $franchise_check = 1;
    if($tab_select == 1){
        while ($franchise_row = mysql_fetch_assoc($franchise, MYSQL_ASSOC)) {
            array_push($franchise_list, $franchise_row);
            $franchise_check++;
        }
    }
    $tab = $_GET['tab'];
    $excel = $_GET['excel'];
}
?>
<div class="tab tab_pos">
    <a href="thinline.php?id=51:17:1:1:::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A&tab=1" id="franchise_list" class="tablinks" onclick="openCity(event, 'franchise')">Franchise List</a>
    <a href="thinline.php?id=51:17:1:1:::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A&tab=2" id="admin_list" class="tablinks" onclick="openCity(event, 'admin')">Admin List</a >
    <?php
    $config['base_url'] = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
    $config['base_url'] .= "://" . $_SERVER['HTTP_HOST'];
    $config['base_url'] .= str_replace(basename($_SERVER['SCRIPT_NAME']), "", $_SERVER['SCRIPT_NAME']);
    $rootpath = $config['base_url'];
    $excel = str_replace($rootpath.'thinline.php', '/franchise_to_csv.php', $actual_link.'&excel=1');
    ?>
    <a id="fran_sec" class="image_right" href="<?php echo $excel ?>">
        <img src="images/excel.gif" title="Export to CSV" border=0/>
    </a>
</div>
<div id="franchise" class="tabcontent">
    <br/>
    <?php
    $numc = count($registeredCountries);
    $count = 1;
    foreach ($registeredCountries AS $r) {
        echo "<a href=/thinline.php?id=51:17:1:1:::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A&tab=1&country=" . str_replace(" ", "%20", $r) . ">" . $r . "</a>";
        echo ($count < $numc) ? " | " : "";
        $count++;
    } ?>
    <br/>
    <br/>
    <table id="franchise_table" border="0" width="100%" style="font-family:Arial;font-size: 12px;">
        <tr>
            <td class="ls_top"><strong>ID</strong></td>
            <td class="ls_top"><strong>Franchise Name</strong></td>
            <td class="ls_top"><strong>Franchise Owner A</strong></td>
            <td class="ls_top"><strong>Franchise Owner B</strong></td>
            <td class="ls_top"><strong>Province/Country/State</strong></td>
            <td class="ls_top"><strong>Franchise Area</strong></td>
            <td class="ls_top"><strong>Venue</strong></td>
            <td class="ls_top"><strong>School</strong></td>

        </tr>
        <?php
        $ls = '';
        include("includes/ls.php");

        foreach ($franchise_list as $key => $value) {
            $venue = $value['OtherCount'] + $value['CountEmpty'];
            $venue_active = $value['OtherCountActive'];
            $SchoolCount = $value['SchoolCount'];
            $SchoolCountActive = $value['SchoolCountActive'];
            if ($venue && $venue_active) {
                if ($venue > $venue_active) {
                    $venue_class = 'color_red';
                } else {
                    $venue_class = 'color_green';
                }
                if ($venue_class == 'color_green') {
                    $venue_other_empty_col = "color:green;font-weight:bold";
                    $venue_active_col = "color:green;font-weight:bold";
                } else {
                    $venue_other_empty_col = "color:red;font-weight:bold";
                    $venue_active_col = "color:red;font-weight:bold";
                }
            }
            if ($SchoolCount && $SchoolCountActive) {
                if ($SchoolCount > $SchoolCountActive) {
                    $school_class = 'color_red';
                } else {
                    $school_class = 'color_green';
                }

                if ($school_class == "color_green") {
                    $SchoolCount_col = "color:green;font-weight:bold";
                    $SchoolCountActive_col = "color:green;font-weight:bold";
                } else {
                    $SchoolCount_col = "color:red;font-weight:bold";
                    $SchoolCountActive_col = "color:red;font-weight:bold";
                }
            }
            ?>
            <tr>
                <td style='font-size:14px;text-align:center;'
                    class="ls_<?php echo $ls ?>"><?php echo trim($value['id']); ?></td>
                <td style='font-size:14px;text-align:center;'
                    class="ls_<?php echo $ls ?>"><?php echo trim($value['username']); ?></td>
                <td style='font-size:14px;text-align:center;'
                    class="ls_<?php echo $ls ?>"><?php echo trim($value['franchiseownera']); ?></td>
                <td style='font-size:14px;text-align:center;'
                    class="ls_<?php echo $ls ?>"><?php echo trim($value['franchiseownerb']); ?></td>
                <td style='font-size:14px;text-align:center;'
                    class="ls_<?php echo $ls ?>"><?php echo trim($value['provencecounty']); ?></td>
                <td style='font-size:14px;text-align:center;'
                    class="ls_<?php echo $ls ?>"><?php echo trim($value['franchiseareaa']); ?></td>
                <td style='font-size:14px;text-align:center;' class="ls_<?php echo $ls ?>">
                    <span style="<?php echo $venue_active_col; ?>"><?php echo preg_replace('/\s+/', '', $value['OtherCountActive']); ?></span>/<span
                            style="<?php echo $venue_other_empty_col; ?>"><?php echo preg_replace('/\s+/', '', $value['OtherCount'] + $value['CountEmpty']); ?></span>
                </td>
                <td style='font-size:14px;text-align:center;' class="ls_<?php echo $ls ?>">
                    <span style="<?php echo $SchoolCountActive_col; ?>"><?php echo preg_replace('/\s+/', '', $value['SchoolCountActive']); ?></span>/<span
                            style="<?php echo $SchoolCount_col; ?>"><?php echo preg_replace('/\s+/', '', $value['SchoolCount']); ?></span>
                </td>
            </tr>
        <?php } ?>

    </table>
</div>
<div id="admin" class="tabcontent">

    <br/>
    <?php
    $numc = count($registeredCountries);
    $count = 1;
    foreach ($registeredCountries AS $r) {
        echo "<a href=/thinline.php?id=51:17:1:1:::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A&tab=2&country=" . str_replace(" ", "%20", $r) . ">" . $r . "</a>";
        echo ($count < $numc) ? " | " : "";
        $count++;
    } ?>
    <br/>
    <br/>

    <div class="search_filter">
        <ul>
            <li>
                <a href="/thinline.php?id=51:17:1:1:::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A&search=home_address&tab=2">Home
                    Address</a>
            </li>
            <li>
                <a href="/thinline.php?id=51:17:1:1:::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A&search=business_address&tab=2">Business
                    Address</a>
            </li>
            <li>
                <a href="/thinline.php?id=51:17:1:1:::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A&search=insurance_certificate&tab=2">Insurance
                    Certificate</a>
            </li>
            <li>
                <a href="/thinline.php?id=51:17:1:1:::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A&search=criminal_check&tab=2">Criminal
                    Check</a>
            </li>
            <li>
                <a href="/thinline.php?id=51:17:1:1:::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A&search=first_aid&tab=2">First
                    Aid</a>
            </li>
            <li>
                <a href="/thinline.php?id=51:17:1:1:::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A&search=child_protection&tab=2">Child
                    Protection</a>
            </li>
            <li>
                <a href="/thinline.php?id=51:17:1:1:::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A&search=debit_order&tab=2">Debit
                    Order</a>
            </li>
            <li>
                <a href="/thinline.php?id=51:17:1:1:::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A&search=franchise_contract&tab=2">Franchise
                    Contract</a>
            </li>
            <li>
                <a href="/thinline.php?id=51:17:1:1:::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A&search=insurance_certificate&tab=2">Insurance
                    Certificate</a>
            </li>
            <li>
                <a href="/thinline.php?id=51:17:1:1:::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A&search=all&tab=2">All</a>
            </li>
        </ul>
    </div>

    <table id="admin_table" border="0" width="100%" style="font-family:Arial;font-size: 12px;">
        <?php foreach ($admin_list as $key => $f) {
            $search = $_GET['search'];
            if (isset($search) && $search == 'all') {
                $selc = true;
            } else if (isset($search) && $search != 'all') {
                $selc = false;
            } else {
                $selc = true;
            }
            //var_dump($key);
            $classcounter = '';
            if ($classcounter == 1) {
                $ls = "on";
                $classcounter = 2;
            } else {
                $ls = "off";
                $classcounter = 1;
            }
            ?>
            <?php // var_dump($f); die;?>
            <?php if ($key == 0) {
                ?>
                <tr>
                    <?php if ($selc) { ?>
                        <td class="ls_top"><strong>Complete</strong></td>
                    <?php } ?>

                    <?php if ($f['franchiseownera']) { ?>
                        <td class="ls_top"><strong>Franchise Owner A</strong></td>
                    <?php } ?>

                    <?php if ($f['franchiseownerb']) { ?>
                        <td class="ls_top"><strong>Franchise Owner B</strong></td>
                    <?php } ?>

                    <?php if ($f['provencecounty']) { ?>
                        <td class="ls_top"><strong>Province/Country/State</strong></td>
                    <?php } ?>

                    <?php if ($f['addressline1'] && $f['addressline2']) { ?>
                        <td class="ls_top"><strong>Home Address</strong></td>
                    <?php } ?>

                    <?php if ($f['businessaddressline1'] && $f['businessaddressline2']) { ?>
                        <td class="ls_top"><strong>Business Address</strong></td>
                    <?php } ?>

                    <?php if ($f['insurancephysicalcopy'] && $f['insuranceexpirydate']) { ?>
                        <td class="ls_top"><strong>Insurance Certificate</strong></td>
                    <?php } ?>

                    <?php if ($f['criminalphysicalcopy'] && $f['criminalexpirydate']) { ?>
                        <td class="ls_top"><strong>Criminal Check</strong></td>
                    <?php } ?>

                    <?php if ($f['firstaidphysicalcopy'] && $f['firstaidexpirydate']) { ?>
                        <td class="ls_top"><strong>First Aid Certificate</strong></td>
                    <?php } ?>

                    <?php if ($f['childphysicalcopy'] && $f['childexpirydate']) { ?>
                        <td class="ls_top"><strong>Child Protection</strong></td>
                    <?php } ?>

                    <?php if ($f['debitphysicalcopy'] && $f['debitpaymentdate']) { ?>
                        <td class="ls_top"><strong>Debit Order</strong></td>
                    <?php } ?>

                    <?php if ($f['franchisephysicalcopy'] && $f['franchiseexpirydate']) { ?>
                        <td class="ls_top"><strong>Franchise Contract</strong></td>
                    <?php } ?>

                    <?php if ($f['coacha'] && $f['coachb'] && $f['coachc'] && $f['coachd']) { ?>
                        <td class="ls_top"><strong>Coaches</strong></td>
                    <?php } ?>
                </tr>
            <?php } ?>
            <?php

            if ($f['insuranceexpirydate'] == '0000-00-00') {
                $f['insuranceexpirydate'] = '';
            } else if ($f['criminalexpirydate'] == '0000-00-00') {
                $f['criminalexpirydate'] = '';
            } else if ($f['firstaidexpirydate'] == '0000-00-00') {
                $f['firstaidexpirydate'] = '';
            } else if ($f['childexpirydate'] == '0000-00-00') {
                $f['childexpirydate'] = '';
            } else if ($f['debitpaymentdate'] == '0000-00-00') {
                $f['debitpaymentdate'] = '';
            }
            ?>
            <tr>
                <?php
                if ($key >= 0) {
                    //var_dump($key);
                    $date_check = '';
                    $date_global = '';

                    $date_check1 = dateTimeCheck($f['insuranceexpirydate']);
                    $date_check2 = dateTimeCheck($f['criminalexpirydate']);
                    $date_check3 = dateTimeCheck($f['firstaidexpirydate']);
                    $date_check4 = dateTimeCheck($f['childexpirydate']);
                    $date_check5 = dateTimeCheck($f['debitpaymentdate']);
                    $date_check6 = dateTimeCheck($f['franchiseexpirydate']);

                    if ($date_check1 == true &&
                        $date_check2 == true &&
                        $date_check3 == true &&
                        $date_check4 == true &&
                        $date_check5 == true &&
                        $date_check6 == true) {
                        $date_global = true;
                    } else if ($date_check1 == false &&
                        $date_check2 == false &&
                        $date_check3 == false &&
                        $date_check4 == false &&
                        $date_check5 == false &&
                        $date_check6 == false) {
                        $date_global = false;
                    } else {
                        $date_global = true;
                    }

//                    var_dump($date_global);

                    if ($date_check1 === 'no_date') {
                        $date_check = '';
                        $date_check = 'no_date';
                    } else if ($date_check2 === 'no_date') {
                        $date_check = '';
                        $date_check = 'no_date';
                    } else if ($date_check3 === 'no_date') {
                        $date_check = '';
                        $date_check = 'no_date';
                    } else if ($date_check4 === 'no_date') {
                        $date_check = '';
                        $date_check = 'no_date';
                    } else if ($date_check5 === 'no_date') {
                        $date_check = '';
                        $date_check = 'no_date';
                    } else if ($date_check6 === 'no_date') {
                        $date_check = '';
                        $date_check = 'no_date';
                    }
                }

                //var_dump($f['provencecounty']);

                if ($selc &&
                    !empty(trim($f['franchiseownera'])) &&
                    !empty(trim($f['franchiseownerb'])) &&
                    !empty(trim($f['provencecounty'])) &&
                    !empty(trim($f['addressline1'])) &&
                    !empty(trim($f['addressline2'])) &&
                    !empty(trim($f['businessaddressline1'])) &&
                    !empty(trim($f['businessaddressline2'])) &&
                    !empty(trim($f['insurancephysicalcopy'])) &&
                    !empty(trim($f['criminalphysicalcopy'])) &&
                    !empty(trim($f['firstaidphysicalcopy'])) &&
                    !empty(trim($f['childphysicalcopy'])) &&
                    !empty(trim($f['debitphysicalcopy'])) &&
                    !empty(trim($f['franchisephysicalcopy'])) &&
                    !empty(trim($f['coacha'])) &&
                    !empty(trim($f['coachb'])) &&
                    !empty(trim($f['coachc'])) &&
                    !empty(trim($f['coachd'])) &&
                    $date_global == true && $date_check != 'no_date') { ?>
                    <td style='font-size:14px;text-align:center;' class="ls_<?php echo $ls ?>">
                        <img class="img_wrng" alt="" src="/images/warning.png">
                    </td>
                <?php } else if ($selc &&
                    !empty(trim($f['franchiseownera'])) &&
                    !empty(trim($f['franchiseownerb'])) &&
                    !empty(trim($f['provencecounty'])) &&
                    !empty(trim($f['addressline1'])) &&
                    !empty(trim($f['addressline2'])) &&
                    !empty(trim($f['businessaddressline1'])) &&
                    !empty(trim($f['businessaddressline2'])) &&
                    !empty(trim($f['insurancephysicalcopy'])) &&
                    !empty(trim($f['criminalphysicalcopy'])) &&
                    !empty(trim($f['firstaidphysicalcopy'])) &&
                    !empty(trim($f['childphysicalcopy'])) &&
                    !empty(trim($f['debitphysicalcopy'])) &&
                    !empty(trim($f['franchisephysicalcopy'])) &&
                    !empty(trim($f['coacha'])) &&
                    !empty(trim($f['coachb'])) &&
                    !empty(trim($f['coachc'])) &&
                    !empty(trim($f['coachd'])) &&
                    $date_global == false && $date_check != 'no_date') { ?>
                    <td style='font-size:14px;text-align:center;' class="ls_<?php echo $ls ?>">
                        <img alt="" src="/images/check2.png">
                    </td>
                <?php } else if ($selc != false && $date_check == 'no_date') { ?>
                    <td style='font-size:14px;text-align:center;' class="ls_<?php echo $ls ?>">
                        <img alt="" src="/images/close.png">
                    </td>
                <?php } else if ($selc != false) { ?>
                    <td style='font-size:14px;text-align:center;' class="ls_<?php echo $ls ?>">
                        <img alt="" src="/images/close.png">
                    </td>
                <?php } ?>

                <td style='font-size:14px;text-align:center;'
                    class="ls_<?php echo $ls ?>"><?php echo trim($f['franchiseownera']); ?></td>

                <td style='font-size:14px;text-align:center;'
                    class="ls_<?php echo $ls ?>"><?php echo trim($f['franchiseownerb']); ?></td>

                <td style='font-size:14px;text-align:center;'
                    class="ls_<?php echo $ls ?>"><?php echo trim($f['provencecounty']); ?></td>

                <?php if ($f['addressline1'] && $f['addressline2']) { ?>
                    <td style='font-size:14px;text-align:center;' class="ls_<?php echo $ls ?>">
                        <img alt="" src="/images/check2.png">
                    </td>
                <?php } else if (!$search || $search == 'all') { ?>
                    <td style='font-size:14px;text-align:center;' class="ls_<?php echo $ls ?>">
                        <img alt="" src="/images/close.png">
                    </td>
                <?php } ?>

                <?php if ($f['businessaddressline1'] && $f['businessaddressline2']) { ?>
                    <td style='font-size:14px;text-align:center;' class="ls_<?php echo $ls ?>">
                        <img alt="" src="/images/check2.png">
                    </td>
                <?php } else if (!$search || $search == 'all') { ?>
                    <td style='font-size:14px;text-align:center;' class="ls_<?php echo $ls ?>">
                        <img alt="" src="/images/close.png">
                    </td>
                <?php } ?>

                <?php

                if (!empty(trim($f['insurancephysicalcopy'])) && !empty(trim($f['insuranceexpirydate']))) {
                    $date_check = dateTimeCheck($f['insuranceexpirydate']);
                    if ($date_check == true) { ?>
                        <td style='font-size:14px;text-align:center;' class="ls_<?php echo $ls ?>">
                            <img class="img_wrng" alt="" src="/images/warning.png">
                        </td>
                    <?php } else { ?>
                        <?php if (!empty(trim($f['insurancephysicalcopy'])) && !empty(trim($f['insuranceexpirydate']))) { ?>
                            <td style='font-size:14px;text-align:center;' class="ls_<?php echo $ls ?>">
                                <img alt="" src="/images/check2.png">
                            </td>
                        <?php } else if (!$search || $search == 'all') { ?>
                            <td style='font-size:14px;text-align:center;' class="ls_<?php echo $ls ?>">
                                <img alt="" src="/images/close.png">
                            </td>
                        <?php } ?>
                    <?php }

                } else if (!$search || $search == 'all') { ?>
                    <td style='font-size:14px;text-align:center;' class="ls_<?php echo $ls ?>">
                        <img alt="" src="/images/close.png">
                    </td>
                <?php } ?>

                <?php
                if (!empty(trim($f['criminalphysicalcopy'])) && !empty(trim($f['criminalexpirydate']))) {
                    $date_check = dateTimeCheck($f['criminalexpirydate']);
                    if ($date_check == true) { ?>
                        <td style='font-size:14px;text-align:center;' class="ls_<?php echo $ls ?>">
                            <img class="img_wrng" alt="" src="/images/warning.png">
                        </td>
                    <?php } else { ?>
                        <?php if (!empty(trim($f['criminalphysicalcopy'])) && !empty(trim($f['criminalexpirydate']))) { ?>
                            <td style='font-size:14px;text-align:center;' class="ls_<?php echo $ls ?>">
                                <img alt="" src="/images/check2.png">
                            </td>
                        <?php } else if (!$search || $search == 'all') { ?>
                            <td style='font-size:14px;text-align:center;' class="ls_<?php echo $ls ?>">
                                <img alt="" src="/images/close.png">
                            </td>
                        <?php } ?>
                    <?php }
                } else if (!$search || $search == 'all') { ?>
                    <td style='font-size:14px;text-align:center;' class="ls_<?php echo $ls ?>">
                        <img alt="" src="/images/close.png">
                    </td>
                <?php } ?>

                <?php
                if (!empty(trim($f['firstaidphysicalcopy'])) && !empty(trim($f['firstaidexpirydate']))) {
                    $date_check = dateTimeCheck($f['firstaidexpirydate']);
                    if ($date_check == true) { ?>
                        <td style='font-size:14px;text-align:center;' class="ls_<?php echo $ls ?>">
                            <img class="img_wrng" alt="" src="/images/warning.png">
                        </td>
                    <?php } else { ?>
                        <?php if (!empty(trim($f['firstaidphysicalcopy'])) && !empty(trim($f['firstaidexpirydate']))) { ?>
                            <td style='font-size:14px;text-align:center;' class="ls_<?php echo $ls ?>">
                                <img alt="" src="/images/check2.png">
                            </td>
                        <?php } else if (!$search || $search == 'all') { ?>
                            <td style='font-size:14px;text-align:center;' class="ls_<?php echo $ls ?>">
                                <img alt="" src="/images/close.png">
                            </td>
                        <?php } ?>
                    <?php }
                } else if (!$search || $search == 'all') { ?>
                    <td style='font-size:14px;text-align:center;' class="ls_<?php echo $ls ?>">
                        <img alt="" src="/images/close.png">
                    </td>
                <?php } ?>

                <?php
                if (!empty(trim($f['childphysicalcopy'])) && !empty(trim($f['childexpirydate']))) {
                    $date_check = dateTimeCheck($f['childexpirydate']);
                    if ($date_check == true) { ?>
                        <td style='font-size:14px;text-align:center;' class="ls_<?php echo $ls ?>">
                            <img class="img_wrng" alt="" src="/images/warning.png">
                        </td>
                    <?php } else { ?>
                        <?php if (!empty(trim($f['childphysicalcopy'])) && !empty(trim($f['childexpirydate']))) { ?>
                            <td style='font-size:14px;text-align:center;' class="ls_<?php echo $ls ?>">
                                <img alt="" src="/images/check2.png">
                            </td>
                        <?php } else if (!$search || $search == 'all') { ?>
                            <td style='font-size:14px;text-align:center;' class="ls_<?php echo $ls ?>">
                                <img alt="" src="/images/close.png">
                            </td>
                        <?php } ?>
                    <?php }
                } else if (!$search || $search == 'all') {
                    ?>
                    <td style='font-size:14px;text-align:center;' class="ls_<?php echo $ls ?>">
                        <img alt="" src="/images/close.png">
                    </td>
                <?php } ?>

                <?php
                if (!empty(trim($f['debitphysicalcopy'])) && !empty(trim($f['debitpaymentdate']))) {
                    $date_check = dateTimeCheck($f['debitpaymentdate']);
                    if ($date_check == true) { ?>
                        <td style='font-size:14px;text-align:center;' class="ls_<?php echo $ls ?>">
                            <img class="img_wrng" alt="" src="/images/warning.png">
                        </td>
                    <?php } else { ?>
                        <?php if (!empty(trim($f['debitphysicalcopy'])) && !empty(trim($f['debitpaymentdate']))) { ?>
                            <td style='font-size:14px;text-align:center;' class="ls_<?php echo $ls ?>">
                                <img alt="" src="/images/check2.png">
                            </td>
                        <?php } else if (!$search || $search == 'all') { ?>
                            <td style='font-size:14px;text-align:center;' class="ls_<?php echo $ls ?>">
                                <img alt="" src="/images/close.png">
                            </td>
                        <?php } ?>
                    <?php }
                } else if (!$search || $search == 'all') { ?>
                    <td style='font-size:14px;text-align:center;' class="ls_<?php echo $ls ?>">
                        <img alt="" src="/images/close.png">
                    </td>
                <?php } ?>

                <?php
                if (!empty(trim($f['franchisephysicalcopy'])) && !empty(trim($f['franchiseexpirydate']))) {
                    $date_check = dateTimeCheck($f['franchiseexpirydate']);
                    if ($date_check == true) { ?>
                        <td style='font-size:14px;text-align:center;' class="ls_<?php echo $ls ?>">
                            <img class="img_wrng" alt="" src="/images/warning.png">
                        </td>
                    <?php } else { ?>
                        <?php if (!empty(trim($f['franchisephysicalcopy'])) && !empty(trim($f['franchiseexpirydate']))) { ?>
                            <td style='font-size:14px;text-align:center;' class="ls_<?php echo $ls ?>">
                                <img alt="" src="/images/check2.png">
                            </td>
                        <?php } else if (!$search || $search == 'all') { ?>
                            <td style='font-size:14px;text-align:center;' class="ls_<?php echo $ls ?>">
                                <img alt="" src="/images/close.png">
                            </td>
                        <?php } ?>
                    <?php }
                } else if (!$search || $search == 'all') { ?>
                    <td style='font-size:14px;text-align:center;' class="ls_<?php echo $ls ?>">
                        <img alt="" src="/images/close.png">
                    </td>
                <?php } ?>

                <?php if (!empty(trim($f['coacha'])) && !empty(trim($f['coachb'])) && !empty(trim($f['coachc'])) && !empty(trim($f['coachd']))) { ?>
                    <td style='font-size:14px;text-align:center;' class="ls_<?php echo $ls ?>">
                        <img alt="" src="/images/check2.png">
                    </td>
                <?php } else if (!$search || $search == 'all') { ?>
                    <td style='font-size:14px;text-align:center;' class="ls_<?php echo $ls ?>">
                        <img alt="" src="/images/close.png">
                    </td>
                <?php } ?>
            </tr>
        <?php } ?>
    </table>

</div>
<?php
function dateTimeCheck($givenDate)
{
    if ($givenDate) {
        $date = new DateTime($givenDate);
        $result = $date->format('Y-m-d');
        if ($result) {
            $today_date = date('Y-m-d');
            if ($today_date > $result) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    } else {
        return 'no_date';
    }
}

?>
<style>
    body {
        font-family: Arial;
    }

    .search_filter {
        width: 100%
    }

    .search_filter ul li {
        display: inline-block;
        color: #000fff;
        border-right: 1px solid #000fff;
        text-decoration: underline;
        padding-left: 1px;
        padding-right: 6px;
        cursor: pointer;
        margin-right: 0px;
    }

    .search_filter ul li:last-child {
        border: none;
    }

    /* Style the tab */
    .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons inside the tab */
    .tab button, .tab a{
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    /* Change background color of buttons on hover */
    .tab button:hover, .tab a:hover {
        background-color: #ddd;
    }

    /* Create an active/current tablink class */
    .tab button.active, .tab a.active {
        background-color: #ccc;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }
</style>
<script type="text/javascript">
    <?php
    if ($tab_select) {
        $val = $tab_select . ';';
    } else {
        $val = "'';";
    }?>
    var val = <?php echo $val; ?>
    if (val == 1) {
        franchise_list();
    } else if (val == 2) {
        admin_list();
    } else {
        franchise_list();
    }

    function openCity(evt, divName) {
        if(divName == 'franchise'){
            document.getElementById('franchise').style.display = "block";
            document.getElementById('admin').style.display = "none";
        }else{
            document.getElementById('franchise').style.display = "none";
            document.getElementById('admin').style.display = "block";
        }
    }

    function franchise_list() {
        var element = document.getElementById("franchise_list");
        element.classList.add("active");
        var franchise_element = document.getElementById("franchise");
        franchise_element.style.display = 'block';
        var admin_element = document.getElementById("admin");
        admin_element.style.display = 'none';
    }

    function admin_list() {
        var element = document.getElementById("admin_list");
        element.classList.add("active");
        var admin_element = document.getElementById("admin");
        admin_element.style.display = 'block';
        var franchise_element = document.getElementById("franchise");
        franchise_element.style.display = 'none';
    }

</script>