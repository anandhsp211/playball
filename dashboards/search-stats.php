<?php

$sql = "SELECT searcharray FROM searchvalues WHERE (MONTH(datecreated) = '".date('m')."' AND YEAR(datecreated) = '".date('Y')."')";
//echo $sql."<br>";
$resi = mysql_query($sql);
$num_rows = mysql_num_rows($resi);

$sql = "SELECT searcharray FROM searchvalues WHERE (MONTH(datecreated) = '".date('m')."' AND YEAR(datecreated) = '".date('Y')."') ORDER BY RAND() LIMIT 5000";
//echo $sql."<br>";
$resi = mysql_query($sql);
$num_rows2 = mysql_num_rows($resi);
?>
<h3 style="font-size:18px;padding:0;margin:0;">Post Code Search Stats for <?php echo date("F")." ".date("Y");?></h3>
<font style="font-size:12px;">Every post code or town search on the website is captured and this is a snapshot of a dataset of <strong><?php echo $num_rows2;?></strong> searches out of a total of <strong><?php echo $num_rows;?></strong> searches for this month.<br />
This gives an indication of the most commonly searched post codes and towns on the playballkids.com website.</font>
<br /><br />
<table >
    <tr>
        <td class="ls_top"><strong>Row</strong></td>
        <td class="ls_top"><strong>Post Code</strong></td>
        <td class="ls_top"><strong>Searches</strong></td>
    </tr>
<?php
while ($row = mysql_fetch_array($resi)) {
    $data = unserialize($row['searcharray']);
    if ((strtoupper($data['postcode']) != "SW1A 1AA") && (strtoupper($data['postcode']) != "SW1A1AA") && ($data['postcode'] != "")) {
        $postcode[$data['postcode']] = (1 + $postcode[$data['postcode']]);
    }
}

arsort($postcode);
$counter = 1;
foreach ($postcode AS $key => $val) {
    include("includes/ls.php");
    echo "<tr>";
    echo "<td class='ls_".$ls."_big'>".$counter."</td>";
    echo "<td class='ls_".$ls."_big'>".$key."</td>";
    echo "<td class='ls_".$ls."_big' align='right'>".$val."</td>";
    echo "<tr>";
    $counter++;
} ?>
</table>