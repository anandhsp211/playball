<?php

if ($gval[1] == "") {
    ?>
    <table border=0 width="100%">
    <tr>
        <td valign="top">
            <h3 style="font-size:18px;">Venue Management</h3>
            <table style="width:100%;font-size:14px;" cellpadding="5" border="0">
                <?php
                if ($_SESSION['countrymanager']['status'] == "Yes") {
                    if ($_GET['venuemsg'] == "success") {
                        echo "<span class='greenboard'>Venue status updated successfully</span><br /><br />";
                    } ?>
                    <tr>
                        <td class="ls_on_big">
                            <a href="thinline.php?id=51:9:1::::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A">Approval
                                Manager</a><br/>
                            <font style="font-size:12px;">Shows a list of venues proposed by franchise owners for
                                country managers to approve.</font>
                        </td>
                    </tr>
                    <tr>
                        <td class="ls_off_big">
                            <a href="thinline.php?id=51:10:1::::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A">Outstanding
                                Confirmations</a><br/>
                            <font style="font-size:12px;">Shows a list of venues where written confirmation is still
                                outstanding.</font>
                        </td>
                    </tr>
                    <tr>
                        <td class="ls_on_big">
                            <a href="thinline.php?id=51:12:1:1:::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A">Venue
                                List</a><br/>
                            <font style="font-size:12px;">List of venues with country filter</font>
                        </td>
                    </tr>
                    <tr>
                        <td class="ls_off_big">
                            <a href="thinline.php?id=51:11:1:1:::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A">Venue
                                Status Change</a><br/>
                            <font style="font-size:12px;">If you need to change or reset a venue status then use this
                                tool.</font>
                        </td>
                    </tr>
                    <tr>
                        <td class="ls_on_big">
                            <a href="thinline.php?id=51:13:1:1:::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A">Franchise
                                Setup Status</a><br/>
                            <font style="font-size:12px;">Identifies which franchises have completed the Franchise
                                Settings and added venues</font>
                        </td>
                    </tr>
                    <?php
                } ?>
                <tr>
                    <td class="ls_off_big">
                        <a href="/dashboards/venue-search.php">Venue Search</a><br/>
                        <font style="font-size:12px;">Mobile friendly, quick search active and marketing venues in your
                            country.</font>
                        <?php
                        if (($_SESSION['userid'] == 74) || ($_SESSION['userid'] == 1)) {
                            echo "<br>";
                            $numc = count($registeredCountries);
                            $c = 1;
                            $registeredCountries = ($_SESSION['userid'] == 74) ? $registeredCountries = array("United States") : $registeredCountries;
                            foreach ($registeredCountries AS $countruy) {
                                echo "<a href='/dashboards/venue-search.php?id=" . $franchiseID . "&country=" . $countruy . "' style='font-size:11px;'>" . $countruy . "</a>";
                                echo ($numc > $c) ? " | " : "";
                                $c++;
                            }
                        } ?>
                    </td>
                </tr>
                <tr>
                    <td class="ls_on_big">
                        <a href="/dashboards/download-venues.php?id=<?php echo $franchiseID; ?>">Download Venue List</a><br/>
                        <font style="font-size:12px;">Download a complete list of venues for your franchise
                            country.</font>
                        <?php
                        if (($_SESSION['userid'] == 74) || ($_SESSION['userid'] == 1)) {
                            echo "<br>";
                            $numc = count($registeredCountries);
                            $c = 1;
                            foreach ($registeredCountries AS $countruy) {
                                echo "<a href='/dashboards/download-venues.php?id=" . $franchiseID . "&country=" . $countruy . "' style='font-size:11px;'>" . $countruy . "</a>";
                                echo ($numc > $c) ? " | " : "";
                                $c++;
                            }
                        } ?>
                    </td>
                </tr>
            </table>
        </td>
        <td style="width:10px;"></td>
        <td valign="top">
            <h3 style="font-size:18px;">Reports Menu</h3>
            <table style="width:100%;font-size:14px;" cellpadding="5" border="0">
                <tr>
                    <td class="ls_on_big">
                        <a href="thinline.php?id=51:1:1::::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A">Generate a
                            Register</a><br>
                        <font style="font-size:12px;">Generate a register for a class or camp.</font>
                    </td>
                </tr>
                <tr>
                    <td class="ls_off_big">
                        <a href="thinline.php?id=51:6:1::::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A">Class/Camp
                            Booking Numbers</a><br>
                        <font style="font-size:12px;">A list of all your camps and classes by season showing the numbers
                            ok kids attending.</font>
                    </td>
                </tr>
                <tr>
                    <td class="ls_on_big">
                        <a href="dashboards/kids-marketing-lists.php">Marketing Lists by Age</a><br>
                        <font style="font-size:12px;">A list of all your kids with their age and guardians.</font>
                    </td>
                </tr>
                <tr>
                    <td class="ls_off_big">
                        <a href="thinline.php?id=51:4:1::::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A">No
                            Class/Camps Bookings</a><br>
                        <font style="font-size:12px;">A list of kids and guardians that have have not made a booking for
                            a camp or a class for the current term.</font>
                    </td>
                </tr>
                <!--<tr>
                    <td class="ls_off_big">
                        <a href="thinline.php?id=51:7:1::::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A">No Current Term Bookings</a><br>
                        <font style="font-size:12px;">A list of kids and guardians that have not booked for the current term.</font>
                    </td>
                </tr>-->
                <tr>
                    <td class="ls_on_big">
                        <a href="thinline.php?id=51:5:1::::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A">Seasonal
                            Rebook Report</a><br>
                        <font style="font-size:12px;">Select the current term and the report will check who has a
                            booking in the previous term, but has not rebooked for the current term.</font>
                    </td>
                </tr>
                <?php
                if (ISMASTER == 1) { ?>
                    <tr>
                        <td class="ls_off_big">
                            <a href="thinline.php?id=51:14:1::::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A">Kids
                                Report Manager</a><br>
                            <font style="font-size:12px;">Add and remove elements to kids reports for different
                                programme stages.</font>
                        </td>
                    </tr>
                    <?php
                } ?>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="top">
            <br/>
            <h3 style="font-size:18px;">Tools Menu</h3>
            <table style="width:100%;font-size:14px;" cellpadding="5" border="0">
                <tr>
                    <td class="ls_on_big">
                        <a rel="lyteframe" name="lyteframe" rev=" width: 785px; height: 485px; scrolling: yes;"
                           href="reset-priorities.php?id=CB463625FC9DDE2D82207E15BDE1B674">Reset Guardian
                            Priority</a><br/>
                        <font style="font-size:12px;">Use this tool to reset Guardian priority before releasing classes
                            at the start of a new term.</font>
                    </td>
                </tr>
                <?php
                if ($franchiseID == 1) { ?>
                    <tr>
                        <td class="ls_off_big">
                            <a href="thinline.php?id=51:8:1::::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A"">Post
                            Code &amp; Town Searches</a><br/>
                            <font style="font-size:12px;">See a snapshot of the volume and types of searches from the
                                playballkids.com website.</font>
                        </td>
                    </tr>
                    <?php
                } ?>
            </table>
        </td>
        <td style="width:10px;"></td>
        <td valign="top">
            <br/>
            <h3 style="font-size:18px;">Miscellaneous</h3>
            <table style="width:100%;font-size:14px;" cellpadding="5" border="0">
                <tr>
                    <td class="ls_on_big">
                        <a target="_playball" href="http://equipment.playballkids.com/">Playball Equipment Shop - Helen
                            Simpson (USA)</a><br/>
                        <font style="font-size:12px;">An online store where you can purchase Playball branded
                            equipment.</font>
                    </td>
                </tr>
                <tr>
                    <td class="ls_off_big">
                        <a target="_playball" href="http://apparel.playballkids.com/">Playball Apparel Shop - Glenda
                            Edelson (USA)</a><br/>
                        <font style="font-size:12px;">An online store where you can purchase Playball branded
                            apparel.</font>
                    </td>
                </tr>
                <tr>
                    <td class="ls_on_big">
                        <a target="_playball" href="http://backoffice.playballkids.com/filemanager/">Marketing
                            Documentation</a><br/>
                        <font style="font-size:12px;">The Playball marketing discs in UK and US English format.</font>
                    </td>
                </tr>
                <tr>
                    <td class="ls_on_big">
                        <a href="/dashboards/gdpr-link-export.php">GDPR Opt In Links</a><br/>
                        <font style="font-size:12px;">This report will export a list of customers by name, email address
                            and communications preference opt in link.</font>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <?php
        if (isset($_SESSION['ismaster']) && $_SESSION['ismaster'] == 1) { ?>
        <tr>
            <td valign="top">
                <h3 style="font-size:18px;">Head Office Admin Page</h3>
                <table style="width:100%;font-size:14px;" cellpadding="5" border="0">
                    <tr>
                        <td class="ls_on_big">
                            <a href="thinline.php?id=51:17:1:1:::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A&tab=1">Head Office Manager</a><br/>
                            <font style="font-size:12px;">Shows a list of venues proposed by franchise owners for
                                country managers to approve.</font>
                        </td>
                    </tr>
                    <!--<tr>
                        <td class="ls_on_big">
                            <a href="thinline.php?id=51:12:1:1:::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A">Venue
                                List</a><br/>
                            <font style="font-size:12px;">List of venues with country filter</font>
                        </td>
                    </tr>-->
                </table>
            </td>
        </tr>
        </table>
        <?php
    } ?>
    <?php

} elseif ($gval[1] == "1") {
    include_once("register-generator.php");
} elseif ($gval[1] == "2") {
    include_once("outstanding-payments-report.php");
} elseif ($gval[1] == "3") {
    include_once("rebook-reports.php");
} elseif ($gval[1] == "4") {
    include_once("no-bookings-v2.php");
} elseif ($gval[1] == "5") {
    include_once("rebook-report-season_v2.php");
} elseif ($gval[1] == "6") {
    include_once("class-numbers.php");
} elseif ($gval[1] == "7") {
    include_once("no-bookings-current-term.php");
} elseif ($gval[1] == "8") {
    include_once("search-stats.php");
} elseif ($gval[1] == "9") {
    include_once("venue-manager.php");
} elseif ($gval[1] == "10") {
    include_once("venue-confirmations.php");
} elseif ($gval[1] == "11") {
    include_once("venue-status-change.php");
} elseif ($gval[1] == "12") {
    include_once("venue-list.php");
} elseif ($gval[1] == "13") {
    include_once("franchise-setup.php");
} elseif ($gval[1] == "14") {
    include_once $_SERVER['DOCUMENT_ROOT'] . '/reports/reports-list.php';
} elseif ($gval[1] == "15") {
    include_once $_SERVER['DOCUMENT_ROOT'] . '/reports/reports-run.php';
} elseif ($gval[1] == "16") {
    include_once $_SERVER['DOCUMENT_ROOT'] . '/reports/reports-edit-report.php';
} elseif ($gval[1] == "17") {
    include_once("franchise-admin.php");
} ?>
