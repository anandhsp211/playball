<?php
include_once("..//_globalconnect.php");
$filename = $rootpath.'document_store/temp_files/'.strtotime('now').'.csv';

// Create CSV header
$header = "No Bookings Report";
$header .= "Guardian Name,Email,Phone,Childs Name,Gender,DOB,";

// define master in this case is kids
$sql = "SELECT recordid, hashid, childsname, gender, birthday FROM `udf_63538FE6EF330C13A05A3ED7E599D5F7` WHERE issaved = 1 ".$addsqldepartments;
//echo $sql."<br>";
$res = mysql_query($sql);
while ($row = mysql_fetch_array($res)) {
    
    $sqli = "SELECT rv.recordid, udf.classtype FROM relations_values rv
                INNER JOIN udf_2BB232C0B13C774965EF8558F0FBD615 udf ON udf.hashid = rv.recordid
                WHERE location = '2BB232C0B13C774965EF8558F0FBD615' 
                AND matrix_id = 364 AND parent_recordid = '".$row['hashid']."'
                AND udf.classtype LIKE 'Class' LIMIT 1";    
    //echo $sqli."<br>";
    $resi = mysql_query($sqli);
    $rowi = mysql_fetch_array($resi);
    if ($rowi['recordid'] == "") {
        include("includes/ls.php");
        
        $sqlp = "SELECT `child_record` FROM multilink
                    WHERE `parent_table` = '63538FE6EF330C13A05A3ED7E599D5F7'
                    AND `parent_record` = '".$row['hashid']."'
                    AND `child_table` = '45C48CCE2E2D7FBDEA1AFC51C7C6AD26'";
        //echo $sqlp."<br>";
        $resp = mysql_query($sqlp);
        $rowp = mysql_fetch_array($resp);
        
        $sqll = "SELECT id, hashid, fullname, mobile, email, issaved FROM `udf_45C48CCE2E2D7FBDEA1AFC51C7C6AD26`
                WHERE hashid = '".$rowp['child_record']."' ".$addsqldepartments;
        //echo $sqll."<br>";
        $resl = mysql_query($sqll);
        $rowl = mysql_fetch_array($resl);
        
        if ($rowl['issaved'] == "-1") {
            // Noticed an anomoly where some people are not set to saved.  Bit of a hack fix.
            //$sqlu = "UPDATE `udf_45C48CCE2E2D7FBDEA1AFC51C7C6AD26` SET `issaved` = '1' WHERE `id` = ".$rowl['id']." LIMIT 1;";
            //mysql_query($sqlu);
            $isdeleted = 1;
        }
        
        // Now that we have the parent we need to find all associated siblings
        $sqln = "SELECT `child_record` FROM multilink
                WHERE `parent_table` = '45C48CCE2E2D7FBDEA1AFC51C7C6AD26'
                AND `child_table` = '63538FE6EF330C13A05A3ED7E599D5F7'
                AND `parent_record` = '".$rowl['hashid']."' AND child_record != '".$row['hashid']."'; ";
        //echo $sqln."<br><br>";
        $resn = mysql_query($sqln);
        $num_rows = mysql_num_rows($resn);
        if ($num_rows >= 1) {
            $checker = 0;
            while ($rown = mysql_fetch_array($resn)) {
                // Lets check if they have more kids and if the other kids have a booking.  If they do then exlcude
                $sqlz = "SELECT recordid, menutabid FROM relations_values
                            WHERE location = '2BB232C0B13C774965EF8558F0FBD615' AND matrix_id = 364
                            AND parent_recordid = '".$rown['child_record']."' LIMIT 1";
                //echo $sqlz."<br><br>";
                $resz = mysql_query($sqlz);
                $rowz = mysql_fetch_array($resz);
                
                if ($rowz['recordid'] != "") {
                    $checker = 1;
                }
                
            }
            
        }
        if ($checker == 0) {
    
    
            $phoneNumbers = ($rebook['landline'] != "") ? ", ".$rebook['landline'] : "";
            
            $value .= '"' . $rowl['fullname'] . '"' . ",";
            $value .= '"' . $rowl['email'] . '"' . ",";
            $value .= '"' . $rowl['mobile'] . $phoneNumbers . '"' . ",";
            $value .= '"' . $row['childsname'] . '"' . ",";
            $value .= '"' . $row['gender'] . '"' . ",";
            $value .= '"' . $row['birthday'] . '"' . ",";
            
            $data .= trim( $value ) . "\n";
            unset($value);
        }
        
        unset($changeissaved, $checker, $isdeleted);
    }
}

$data = str_replace( "\r" , "" , $data );

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=nobookings_export_".date("Y-m-d H:i:s").".csv");
header("Pragma: no-cache");
header("Expires: 0");
print "$header\n$data";

?>