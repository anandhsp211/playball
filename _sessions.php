<?php
/// Delete all obsolete sessions
$maxlifetime = 86400;
$time_limit_stamp = mktime()-$maxlifetime;
//echo date("Y-m-d H:i:s", $time_limit_stamp);
$query = "DELETE FROM sessions WHERE last_updated < '".date("Y-m-d H:i:s", $time_limit_stamp)."'";
//echo $query;
mysql_query($query);


// Keep an activity session running
$query = "SELECT session_id, userid FROM sessions WHERE session_id = '".session_id()."'";
//echo $query;
$result = mysql_query($query);
$row = mysql_fetch_array($result);

//echo $row['session_id'];

if (($row['session_id'] == "")) {
	$query = "INSERT INTO sessions (session_id, userid, last_updated, ipaddress) VALUES (
		  '".session_id()."',
		   '".$_SESSION['userid']."',
		  '".$sGMTMySqlString."',
		  '".$_SERVER['REMOTE_ADDR']."'
		  )";
	mysql_query($query);
	//echo $query; 
	$_SESSION['username'] = "";
	$_SESSION['userid'] = "";
	unset($_SESSION['username']);
	$_SESSION['registrantid'] = "";
	unset($_SESSION['userid']);
	
	if ($_SESSION['registrantid'] == "") {
		$_SESSION['loggedin'] = 5;
		header("Location: login.php?session=expired");
	}
	
} else {
	// First check if session has expired
	$query = "SELECT fieldvalue FROM custom_settings WHERE registrantid = ".$_SESSION['registrantid']." AND fieldname = 'sessiontimeout' LIMIT 1";
	//echo $query;
	$result = mysql_query($query);
	$row = mysql_fetch_array($result);
	
	$queryGetLast = "SELECT last_updated FROM sessions WHERE session_id = '".session_id()."'";
	$resultGetLast = mysql_query($queryGetLast);
	$rowGetLast = mysql_fetch_array($resultGetLast);
	
	$last_activity = $rowGetLast['last_updated'];
	$time_since_last_activity = time() - strtotime($last_activity);
	//echo $time_since_last_activity;
	
	
	if (($time_since_last_activity > $row['fieldvalue']) || ($_SESSION['username'] == "")) {
		if ($_SESSION['username'] == "") {
			$sql = "INSERT INTO `session_expiry_reason` (`uid` ,`reason`)
				VALUES ('".UID."',  'No username session');";
			//echo $sql;
			mysql_query($sql);
		} elseif (($time_since_last_activity > $row['fieldvalue'])) {
			$sql = "INSERT INTO `session_expiry_reason` (`uid` ,`reason`)
				VALUES ('".UID."',  'Activity timeout: ".$time_since_last_activity.":".$row['fieldvalue']."');";
			mysql_query($sql);
		}
		$_SESSION['loggedin'] = 5;
		
		$sql = "INSERT INTO `session_expiry_reason` (`uid` ,`reason`)
			VALUES ('".UID."',  'Unknown Reason');";
		//echo $sql;
		mysql_query($sql);
		
		header("Location: login.php?session=expired");
	} else {
 		$query = "UPDATE sessions SET last_updated='".$sGMTMySqlString."' WHERE session_id='".session_id()."'";
		//echo $query;
 		mysql_query($query) Or Die ("Cannot update entry!");
	}
}?>