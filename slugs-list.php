
<script language="JavaScript">
function submitform() {
    if (document.adddept.slug.value == '') {
        alert('You must enter a slug.');
    } else {
        document.adddept.submit();
    }
}
</script>


<table width="100%" cellpadding="5" cellspacing="1" border="0">
<tr>
    <td colspan="2">
        <table width="100%" cellpadding="2" cellspacing="2" border="0">
            <tr>
                <td colspan="2">
                    <img src="images/icons/pin_yellow_24.png" align="left">
                    &nbsp;<strong><font style="font-size:150%;font-family:Trebuchet MS">Slug List</font></strong>
                    <br><br>
                </td>
            </tr>
            <tr>
                <td class="ls_top"><strong>Franchise</strong></td>
                <td class="ls_top"><strong>Slug</strong></td>
                <td class="ls_top"><strong>Action</strong></td>
            </tr>
            <?php
            $query = "SELECT s.id, d.department, s.slug FROM slugs s, departments d 
                        WHERE s.franchiseid = d.id ORDER BY department";
            //echo $query."<br>";
            $result = mysql_query($query);
            while ($row = mysql_fetch_array($result)) {
                include 'includes/ls.php';?>
            <tr>
                <td class="ls_<?php echo $ls;?>"><?php echo $row['department'];?></td>
                <td class="ls_<?php echo $ls;?>">/<?php echo $row['slug'];?></td>
                <td class="ls_<?php echo $ls;?>"><a onclick="return confirm('Are you sure you want to delete this slug?')" href="/settings.php?id=x49&slugid=<?php echo $row['id'];?>">Delete</a></td>
            </tr>
            <?php
            } ?>
        </table>
    </td>
</tr>
</table>
	