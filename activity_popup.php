<?php include_once("_globalconnect.php"); 
if ($_POST['activity_action'] == "Dismiss") {
	$reccounter = count($_POST['acthandle']);
	if ($reccounter > 0) {
		foreach ($_POST['acthandle'] as &$activityhashid) {
			$queryup = "UPDATE activities SET reminderdisabled = 1 WHERE hashid = '".$activityhashid."' AND registrantid = ".RID;
			mysql_query($queryup);
		}
	}
	header("Location: activity_popup.php?id=::2");
}

if ($_POST['activity_action'] == "Snooze") {
	$reccounter = count($_POST['acthandle']);
	if ($reccounter > 0) {
		foreach ($_POST['acthandle'] as &$activityhashid) {
  			$queryup = "UPDATE activities SET snoozestartpoint = '".$sGMTMySqlString."',remindersnooze = '".$_POST['remindertime']."' WHERE hashid = '".$activityhashid."' AND registrantid = ".RID;
			// echo $queryup;
			mysql_query($queryup) or die ('Cannot Snooze');
		}
	}
	header("Location: activity_popup.php?id=::2");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "includes/xhtml1-transitional.dtd">
<html>
<head>
	<title>Alarm - Thinline Software</title>
	<style type="text/css" media="all">@import "css/style.css";</style>
</head>

<body>
<?php 
// lets get the items that are due
$late_nums = count($late_hashid_array);
//echo $late_nums;
$late_basic_counter = 1;
$sql = "SELECT hashid, subject, datestart FROM activities WHERE registrantid = ".RID;
foreach ($late_hashid_array as &$thelasthashes) {
if ($late_basic_counter == 1) {
	$sql .= " AND (hashid = '".$thelasthashes."'";
} else {
	$sql .= " OR hashid = '".$thelasthashes."'";
}
if ($late_basic_counter == $late_nums) {
	$sql .= ")";
}
	$late_basic_counter++;
}
$sql .= " ORDER BY datestart ASC";
//echo $sql;
$res = mysql_query($sql);
if (($late_nums <= 0) || ($late_nums == NULL)) {?>
	<script language="Javascript">
		window.close();
		top.opener.location.reload();
	</script>
<?php 
}
?>
<table width="98%">
<tr>
	<td width="35"><img src="images/icons/24_shadow/box_tall.png"></td>
	<td><h1>Overdue Activities</h1></td>
</tr>
</table><br>
<form method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?>?id=::2">
<script>
	function select_deselectAll (chkVal, idVal) 
{ 
    var frm = document.forms[0];
    // Loop through all elements
    for (i=0; i<frm.length; i++) 
    {
        // Look for our Header Template's Checkbox
        if (idVal.indexOf ('CheckAll') != -1) 
        {
            // Check if main checkbox is checked, then select or deselect datagrid checkboxes 
            if(chkVal == true) 
            {
                frm.elements[i].checked = true;
            } 
            else 
            {
                frm.elements[i].checked = false;
            }
            // Work here with the Item Template's multiple checkboxes
        } 
        else if (idVal.indexOf ('DeleteThis') != -1) 
        {
            // Check if any of the checkboxes are not checked, and then uncheck top select all checkbox
            if(frm.elements[i].checked == false) 
            {
                frm.elements[1].checked = false; //Uncheck main select all checkbox
            }
        }
    }

}
</script>
<table class="note" width="98%">
<tr>
	<td class="ls_top" align="center"><strong>Subject</strong></td>
	<td class="ls_top" align="center"><strong>Due</strong></td>
	<td class="ls_top" align="center"><input type="checkbox" ID="CheckAll" OnClick="javascript: return select_deselectAll (this.checked, this.id);"></td>
</tr>
<?php 
$br = new Browser;
if ($br->Name == "MSIE") {
	$win_opener = "opener";
} else {
	$win_opener = "popup_alert";
}
// We need to get the activities menutab id
while ($row = mysql_fetch_array($res)) { 
	include("includes/ls.php");?>
	<tr>
		<td class="ls_<?php echo $ls ?>"><a href="#" onclick="window.opener.document.location.href='thinline.php?id=::5:::<?php echo $row['hashid'] ?>:::::::::::::::<?php echo ACTID ?>'; window.<?php echo $win_opener; ?>.focus();"><?php echo $row['subject'] ?></a></td>
		<td align="center" class="ls_<?php echo $ls ?>"><?php echo date("d-M-Y H:i",strtotime($row['datestart'])) ?></td>
		<td align="center" class="ls_<?php echo $ls ?>"><input type="checkbox" name="acthandle[]" value="<?php echo $row['hashid'] ?>"></td>
	</tr>
<?php
}
 ?>
</table>
<br>
<table border="0" class="note" width="98%">
<tr>
	<td width="100">
		<?php
		$alarmarray = array("5 minutes", "10 minutes", "30 minutes", "1 hour", "2 hours", "4 hours", "8 hours", "1 day", "2 days", "1 week", "2 weeks");?>
		<select name="remindertime" style="font-family:Arial;font-size:14px;padding:2px">
			<?php
			foreach ($alarmarray as $i => $value) {?>
			<option <?php echo $thisisselected;?> value="<?php echo $value ?>"><?php echo $value ?></option>
		<?php
			} ?>
		</select>	
	</td>
	<td><input type="submit" name="activity_action" value="Snooze"></td>
	<td align="right"><input type="submit" value="Dismiss" name="activity_action"></td>
</tr>
</table>
</form>
</body>
</html>
