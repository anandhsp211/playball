<?php 
include_once("_globalconnect.php");

if ((($_POST['name'] == "") || ($_POST['code'] == "")) && (($_POST['updatesave'] == 1) || ($_POST['updatesave'] == 2))) {
	$error_message = "Please complete all required fields";
} else {
	if ($_POST['updatesave'] == 1) {
		$sql = "SELECT code FROM transaction_types WHERE code = '".$_POST['code']."' AND md5(id) != '".$_POST['hashid']."'";
		// echo $sql;
		$res = mysql_query($sql);
		$row = mysql_fetch_array($res);
		if (!$row['code']) {
			$query = "UPDATE `transaction_types` SET `codetype` = '".$_POST['codetype']."',
					`code` = '".$_POST['code']."',
					`name` = '".$_POST['name']."',
					`description` = '".$_POST['description']."' WHERE md5(id) = '".$_POST['hashid']."' LIMIT 1";
			// echo $query."<br><br>";
			mysql_query($query) Or Die ("Cannot submit entry!");
			$onload = 'onload="window.parent.location = window.parent.location;self.close();return false;"';
		} else {
			$error_message = "Code already in use, please choose another code.";
		}
	} elseif ($_POST['updatesave'] == 2) {
		$sql = "SELECT code FROM transaction_types WHERE code = '".$_POST['code']."' AND md5(id) != '".$_POST['hashid']."'";
		// echo $sql;
		$res = mysql_query($sql);
		$row = mysql_fetch_array($res);
		if (!$row['code']) {
			$query = "INSERT INTO `transaction_types` ( `codetype` , `code` , `name` , `description` ) 
						VALUES ( '".$_POST['codetype']."', '".$_POST['code']."', '".$_POST['name']."', '".$_POST['description']."' );";
			// echo $query."<br><br>";
			mysql_query($query) Or Die ("Cannot submit entry!");
			$onload = 'onload="window.parent.location = window.parent.location;self.close();return false;"';
		} else {
			$error_message = "Code already in use, please choose another code.";
		}
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "includes/xhtml1-transitional.dtd">
<html>
<head>
	<title>Link Records</title>
	<?php 
	if (ERES > 1024) { ?>
		<style type="text/css" media="all">@import "css/style.css";</style>
	<?php 
	} else { ?>
		<style type="text/css" media="all">@import "css/style_small.css";</style>
	<?php
	}?>		
</head>

<body <?php echo $onload ?>>
<form method="POST" name="accounts_manager" action="<?php echo $_SERVER['PHP_SELF'] ?>?id=<?php echo ":".$gval[1].":".$gval[2].":".$gval[3].":".$gval[4].":".$gval[5].":".$gval[6].":".$gval[7].":".$gval[8].":".$gval[9].":".$gval[10].":".$gval[11].":".$gval[12].":".$gval[13].":".$gval[14].":".$gval[15].":".$gval[16].":".$gval[17].":".$gval[18].":".$gval[19].":".$gval[20].":".$gval[21].":".$gval[22].":".$gval[23].":".$gval[24].":".$gval[25]; ?>">
	<table cellspacing="0" cellpadding="0" border="0" width="97%">
	<tr>
		<td><img src="images/<?php echo $_SESSION['franchisedata']['logo'];?>" alt="" / align="center" border="0"></td>
	</tr>
	</table><br>
	<table cellspacing="0" cellpadding="0" border="0" width="98%">
	<tr>
		<td valign="top" align="right" rowspan="2" width="2%">&nbsp;</td>
		<td valign="top" align="right" width="5%">
			<img src="images/icons/24_shadow/text_code_colored.png"><img src="images/spacer.gif" height="1" width="10">
		</td>
		<td valign="top" height="5" width="60%">
			<strong><font style="font-size:130%;font-family:Trebuchet MS">Add/Change Transaction Type Details</font></strong>
		</td>
		
		<td align="right" valign="top" height="5" >
		<div id="menu_bar">
		<script language="JavaScript">
		function submitform() {
			document.accounts_manager.submit();
		}
		</script>
			<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td><img src="images/menu/menu_01.gif" width="13" height="25" alt=""></td>
				<td><a href="javascript: submitform()"><img src="images/menu/save.gif" width="23" height="25" alt="Update &amp; Close" border="0"></a></td>
				<td><img src="images/menu/menu_08.gif" width="8" height="25" alt=""></td>
				<td><a href="thinline.php?id=<?php echo $revfull ?>"><img src="images/refresh.gif" border="0" alt="Refresh this page."></a></td>
				<td><img src="images/menu/menu_11.gif" width="9" height="25" alt=""></td>
				<td><img src="images/menu/menu_13.gif" width="18" height="25" alt=""></td>
			</tr>
		</table>
		</div>
		</td>
		<td rowspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td width="10"><img src="images/spacer.gif" width="25"></td>
		<td valign="top" colspan="4">
		<?php 
		if ($gval[0] == 1) {
			$sql = "SELECT * FROM transaction_types WHERE md5(id) = '".$gval[1]."'";
			//echo $sql;
			$res = mysql_query($sql);
			$row = mysql_fetch_array($res) or die ("Cannot retrieve details");
		} ?>
			<table width="98%" border="0" cellpadding="2" cellspacing="2">
			<?php 
			if ($error_message == "") { ?>
			<tr>
				<td colspan="2"><img src="images/spacer.gif" width="45"></td>
			</tr>
			<?php 
			}
			if ($error_message != "") { ?>
				<tr>
					<td colspan="2"><img src="images/spacer.gif" width="25"></td>
				</tr>
				<tr>
					<td colspan="2" class="messageboard"><?php echo $error_message; ?></td>
				</tr>
				<tr>
				<td colspan="2"><img src="images/spacer.gif" width="15"></td>
			</tr>
			<?php 
			} ?>
			<tr>
				<td class="ls_on">Code Type: <font color="red">*</font></td>
				<td class="ls_on">
					<select class="standardselect" name="codetype">
						<?php 
						if ($_POST['updatesave'] == 1) {
							$selectedvalue = $_POST['codetype'];
						} else {
							$selectedvalue = $row['codetype'];
						}
						$likesrcharray = array("Turnover", "Cost of Sale", "Expenditure", "Fixed Asset", "Current Asset/Liability", "Capital Reserves"); 
						foreach ($likesrcharray AS $likeval) { 
							if ($likeval == $selectedvalue) { $likeselected = "selected";  }?>
							<option <?php echo $likeselected; ?> value="<?php echo $likeval; ?>"><?php echo $likeval ?> </option>
						<?php 
							unset($likeselected);
						} ?>
					</select>
				</td>
			</tr>
			<tr>
				<td class="ls_on">Name: <font color="red">*</font></td>
				<?php 
				if (($_POST['updatesave'] == 1) || ($_POST['updatesave'] == 2)) {
					$selectedvalue_2 = $_POST['name'];
				} else {
					$selectedvalue_2 = $row['name'];
				}?>
				<td class="ls_on"><input name="name" type="text" class="standardfield" value="<?php echo $selectedvalue_2 ?>"></td>
			</tr>
			<tr>
				<td class="ls_on">Code: <font color="red">*</font></td>
				<?php 
				if (($_POST['updatesave'] == 1) || ($_POST['updatesave'] == 2)) {
					$selectedvalue_3 = $_POST['code'];
				} else {
					$selectedvalue_3 = $row['code'];
				}?>
				<td class="ls_on"><input name="code" type="text" class="standardfield_short" value="<?php echo $selectedvalue_3 ?>"></td>
			</tr>
			<tr>
				<td class="ls_on">Description:</td>
				<?php 
				if (($_POST['updatesave'] == 1) || ($_POST['updatesave'] == 2)) {
					$selectedvalue_4 = $_POST['description'];
				} else {
					$selectedvalue_4 = $row['description'];
				}?>
				<td class="ls_on">
					<textarea name="description" style="font-family:Verdana;font-size:12px;width:70%;height:125px"><?php echo $selectedvalue_4  ?></textarea>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
<input type="hidden" name="hashid" value="<?php echo $gval[1] ?>">
<?php  
if ($gval[0] == 1) { ?>
	<input type="hidden" name="updatesave" value="1">
<?php } else { ?>
	<input type="hidden" name="updatesave" value="2">
<?php 
} ?>
</form>
</body>
</html>