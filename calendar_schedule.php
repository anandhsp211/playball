<?php 
include_once("_globalconnect.php"); ?>
<html>
<head>
	<title>Thinline Software - Calendar Schedule View</title>
<style type="text/css" media="all">@import "css/style_schedule.css";</style>
</head>
<body>
<?php 
///////////////////////////////////////////////////////////////////////
//// Better checki how many peopel will be included in the display ////
///////////////////////////////////////////////////////////////////////


$thisyear = date("Y", time());
echo "<table border=\"0\" cellpadding=\"0\" cellspacing=\"2\" height=\"90%\">\n";
//showMonth(8,2007,1);
showMonth($gval[0],$thisyear,1);
//showMonth(10,2007,3);

//showHours(8,2007,1);
showHours($gval[0],$thisyear,1);
//showHours(10,2007,3);

if ($_SESSION['includepeep'] == "") {
	$query_indi = "SELECT id FROM users WHERE id = ".$_SESSION['userid'];
} else {
	$sqlbuild = explode(":", $_SESSION['includepeep']);
	foreach ($sqlbuild as $item) {
		if ($mecasa != 1) {
			$sqloutput = " AND idhash='".$item."'";
			$mecasa = 1;
		} else {
			$sqloutput .= " OR idhash='".$item."'";
		}
	}
	$query_indi = "SELECT id FROM users WHERE registrantid = ".$_SESSION['registrantid']." AND isactive = 1 ".$sqloutput." ORDER BY firstname ASC";
	//echo $query_indi;
}

$result_indi = mysql_query($query_indi);
while ($row_indi = mysql_fetch_array($result_indi)) {
	//showPeep(8,2007,1,$row_indi['id']);
	showPeep($gval[0],$thisyear,1,$row_indi['id']);
	//showPeep(10,2007,3,$row_indi['id']);
}

function showMonth($month, $year,$prntsession) {
	$gval = explode(":",$_GET['id']);
	if ($gval[0] == "") {
		$zeromonth = $month;
	} else {
		$zeromonth = $gval[0];
	}
    $date = mktime(12, 0, 0, $month, 1, $year);
    $daysInMonth = date("t", $date);
    // calculate the position of the first day in the calendar (sunday = 1st column, etc)
    $offset = date("w", $date);
    $rows = 1;
	//if ($prntsession == 1) { 
	if ($gval[1] == 1) {
		$xxscolspan = "48";
	} else {
		$xxscolspan = "24";
	}
	echo "<tr class=\"sched_dates\">";
	for ($day = 1; $day <= $daysInMonth; $day++) {
		echo "<td class=\"multibackground\" colspan=\"".$xxscolspan."\" align=\"left\">";
		echo '<table cellspacing="1" cellpadding="0" border="0">';
		echo '<tr>';
			echo "<td class=\"sched_dates\">&nbsp;<a href=\"calendar_schedule.php?id=".$gval[0].":1#todaysdate\">Show All Day</a> | <a href=\"calendar_schedule.php?id=".$gval[0].":2#todaysdate\">Show Working Day</a></td>";
			echo "<td><img src=\"images/spacer.gif\" height=1 width=75>";
			if ($gval[1] == "") {
				$allorworking = 2;
			} else {
				$allorworking = $gval[1];
			}
			echo '<a href="calendar_schedule.php?id='.($gval[0]-1).':'.$allorworking.':'.$gval[2].'"><img border="0" src="images/calendar/cal_left.png"></a></td>';
			echo "<td class=\"sched_dates\"><a href=calendar_schedule.php?id=".date("m", time()).":".$allorworking.":".$gval[2]."#todaysdate>This Month</td>";
			echo '<td><a href="calendar_schedule.php?id='.($gval[0]+1).':'.$allorworking.':'.$gval[2].'"><img src="images/calendar/cal_right.png" border="0"></a></td>';
		echo '</tr>';
		echo '</table>';
		echo "</td>\n";
	}
	echo "</tr>";
	echo "<tr class=\"sched_dates\">"; //}
    for ($day = 1; $day <= $daysInMonth; $day++) {
		if ($day < 10) { $addme = 0; } else { $addme = ""; }
		if ($_SESSION['anamecntr'] == 1) { 
			$anmedrop = '<a name="#todaysdate"></a>';
			$_SESSION['anamecntr'] = 2;
		}
		if ( (date("Y-m-".$addme.$day."", $date)) == (date("Y-m-d", time())) ) {
			$_SESSION['anamecntr'] = 1;
		}
		echo "<td colspan=\"".$xxscolspan."\" align=\"left\" class=\"mheader\">&nbsp;".$anmedrop."<strong>".$day." ".date("F Y", $date)."</strong></td>\n";
		$anmedrop = "";
	}
	if ($prntsession == 3) { echo '</tr>'; }
}
function showHours($month, $year,$prntsession) {
	$gval = explode(":",$_GET['id']);
	if ($gval[0] == "") {
		$zeromonth = $month;
	} else {
		$zeromonth = $gval[0];
	}
    $date = mktime(12, 0, 0, $month, 1, $year);
    $daysInMonth = date("t", $date);
    // calculate the position of the first day in the calendar (sunday = 1st column, etc)
    $offset = date("w", $date);
    $rows = 1;
	if ($prntsession == 1) { echo "<tr class=\"sched_dates\">"; }
    for ($day = 1; $day <= $daysInMonth; $day++) {
		if ($gval[1] == 1) {
			$timearray = array(
			"00:00", "01:00",  "02:00",  "03:00",  "04:00", "05:00", "06:00", "07:00", "08:00",  "09:00",
			"10:00","11:00", "12:00", "13:00","14:00","15:00", "16:00","17:00", "18:00", "19:00", "20:00", 
			"21:00","22:00", "23:00");
		} else {
			$timearray = array("07:00","08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00");
		}
		foreach ($timearray as $i => $value) {
			echo "<td colspan=2 class=\"mweeks\">$value<br></td>\n";
		}
		
	}
	if ($prntsession == 3) { echo '</tr>'; }
	
}

function showPeep($month, $year,$prntsession,$caluid) {
	$gval = explode(":",$_GET['id']);
	if ($gval[0] == "") {
		$zeromonth = $month;
	} else {
		$zeromonth = $gval[0];
	}
    $date = mktime(12, 0, 0, $month, 1, $year);
    $daysInMonth = date("t", $date);
    // calculate the position of the first day in the calendar (sunday = 1st column, etc)
    $offset = date("w", $date);
    $rows = 1;
//	if ($prntsession == 1) { 
	echo "<tr class=\"sched_dates\">"; //}
	for ($day = 1; $day <= $daysInMonth; $day++) {
		if ($gval[1] == 1) {
			$timearray = array(
			"00:00", "01:00",  "02:00",  "03:00",  "04:00", "05:00", "06:00", "07:00", "08:00",  "09:00",
			"10:00","11:00", "12:00", "13:00","14:00","15:00", "16:00","17:00", "18:00", "19:00", "20:00", 
			"21:00","22:00", "23:00");
		} else {
			$timearray = array("07:00","08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00");
		}
		foreach ($timearray as $i => $value) {
				$hourvalue = explode(":",$value);
				$date = mktime($hourvalue[0], 0, 0, $month, $day, $year);
				$date_2 = $date+3600;
				$datestring = date("Y-m-d H:i:s", $date);
				$query = "SELECT id,datestart,dateend,showtimeas FROM activities WHERE (datestart  >= '".date("Y-m-d H:i:s", $date). "' AND datestart < '".date("Y-m-d H:i:s", $date_2). "') AND (userid = ".$caluid." OR allocatedto = ".$caluid.") AND addtocal='on' ORDER BY id DESC";
				// echo $query."<br>";
				$result = mysql_query($query);
				while ($row = mysql_fetch_array($result)) {
					$datstart = strtotime($row['datestart']);
					$datend = strtotime($row['dateend']);
					$datlen = ($datend-$datstart);
					$_SESSION['bgcolor'] = $row['showtimeas'];
					//echo $_SESSION['bgcolor']."<br>";
					if ($_SESSION['datelen'] > $datlen) {
						$datlen = $_SESSION['datelen'];
					}
				}
				$query6 = "SELECT id FROM activities WHERE (datestart  >= '".date("Y-m-d H:i:s", $date). "' AND datestart < '".date("Y-m-d H:i:s", $date_2). "') AND (userid = ".$caluid." OR allocatedto = ".$caluid.") AND addtocal='on' ORDER BY id DESC";
				//echo $query;
				$result6 = mysql_query($query6);
				$row6 = mysql_fetch_array($result6);
				
				if ($row6) {
					if (($_SESSION['datelen'] == 1800) && ($live_2 != "")) {
						$live = "_l";
						$_SESSION['datelen'] = ($_SESSION['datelen'] - 1800);
						$firstrow2 = 15;
						$bgcolorsql = "SELECT fieldvalue2 FROM custom_settings WHERE fieldname = 'showtimeas' AND registrantid = ".$_SESSION['registrantid']." AND fieldvalue = '".$_SESSION['bgcolor']."'";
						$ressql = mysql_query($bgcolorsql);
						$rowsql = mysql_fetch_array($ressql);
						$bgcolor = $rowsql['fieldvalue2'];
					}
					if ((date("i",$datstart) != 30) && ($datlen == 1800)) {
						$live = "_l";
						$_SESSION['datelen'] = ($datlen - 1800);
						$firstrow = 1;
						$bgcolorsql = "SELECT fieldvalue2 FROM custom_settings WHERE fieldname = 'showtimeas' AND registrantid = ".$_SESSION['registrantid']." AND fieldvalue = '".$_SESSION['bgcolor']."'";
						$ressql = mysql_query($bgcolorsql);
						$rowsql = mysql_fetch_array($ressql);
						$bgcolor = $rowsql['fieldvalue2'];
					} elseif ((date("i",$datstart) != 30) && ($datlen > 1800)) {
						$live = "_l";
						$live_2 = "_l";
						$_SESSION['datelen'] = ($datlen - 3600);
						$firstrow = 1;
						$bgcolorsql = "SELECT fieldvalue2 FROM custom_settings WHERE fieldname = 'showtimeas' AND registrantid = ".$_SESSION['registrantid']." AND fieldvalue = '".$_SESSION['bgcolor']."'";
						$ressql = mysql_query($bgcolorsql);
						$rowsql = mysql_fetch_array($ressql);
						$bgcolor = $rowsql['fieldvalue2'];
					} else {
						$live_2 = "_l";
						$_SESSION['datelen'] = ($datlen - 1800);
						$firstrow = 1;
						$bgcolorsql = "SELECT fieldvalue2 FROM custom_settings WHERE fieldname = 'showtimeas' AND registrantid = ".$_SESSION['registrantid']." AND fieldvalue = '".$_SESSION['bgcolor']."'";
						$ressql = mysql_query($bgcolorsql);
						$rowsql = mysql_fetch_array($ressql);
						$bgcolor = $rowsql['fieldvalue2'];
					}
				} else {
					$live = "";
					$live_2 = "";
				}
					
				if (($_SESSION['datelen'] > 1800) && ($firstrow != 1) ) {
					$live = "_l";
					$live_2 = "_l";
					$_SESSION['datelen'] = ($_SESSION['datelen'] - 3600);
					$thmin = 1;
					$bgcolorsql = "SELECT fieldvalue2 FROM custom_settings WHERE fieldname = 'showtimeas' AND registrantid = ".$_SESSION['registrantid']." AND fieldvalue = '".$_SESSION['bgcolor']."'";
					$ressql = mysql_query($bgcolorsql);
					$rowsql = mysql_fetch_array($ressql);
					$bgcolor = $rowsql['fieldvalue2'];
				} elseif (($_SESSION['datelen'] == 1800) && ($firstrow != 1) && ($thmin == 1) ) {
					$live = "_l";
					$_SESSION['datelen'] = ($_SESSION['datelen'] - 1800);
					$bgcolorsql = "SELECT fieldvalue2 FROM custom_settings WHERE fieldname = 'showtimeas' AND registrantid = ".$_SESSION['registrantid']." AND fieldvalue = '".$_SESSION['bgcolor']."'";
					$ressql = mysql_query($bgcolorsql);
					$rowsql = mysql_fetch_array($ressql);
					$bgcolor = $rowsql['fieldvalue2'];
				}
				
				if (($value == "07:00") && ($gval[1] == 2) ) {
					$_SESSION['datelen'] = ($_SESSION['datelen'] - 43200);
					if ($_SESSION['datelen'] <= 0) {
						$live = "";
						$live_2 = "";
					}
				}
				
				if ($bgcolor != "") {
					$printbgcolor = "bgcolor=\"".$bgcolor."\"";
				}
				
				echo "<td ".$printbgcolor." class=\"md".$live."\">&nbsp;";
					//echo <font color=white>$value."&nbsp;<br>";
					//echo date("i",$datstart)."&nbsp;<br>";
					//echo $datlen." ->".$firstrow."- ". $_SESSION['datelen']."&nbsp;<br>";
				echo "</td>\n";
				echo "<td ".$printbgcolor." class=\"md".$live_2."\">&nbsp;";
					//echo <font color=white>$value."&nbsp;<br>";
					//echo date("i",$datstart)."&nbsp;<br>";
					//echo $datlen." ->".$firstrow."- ". $_SESSION['datelen'];
				echo "</td>\n";
				
			$firstrow++;
			$live = "";
			$live_2 = "";
		}
	}
	//if ($prntsession == 3) { 
	echo '</tr>'; //}
}

echo "</table>\n"; 
$_SESSION['anamecntr'] = 0;

?>
</body>
</html>
