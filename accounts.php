<br>
<?php 
if (($gval[0] == 6) && ($gval[2] == 1)) {
	unset($_SESSION['accounts_receive_search']);
	header("Location: ".$_SERVER['PHP_SELF']."?id=2:".$gval[1].":".$gval[2].":".$gval[3].":".$gval[4].":". $gval[5].":".$gval[6].":".$gval[7].":".$gval[8].":".$gval[9].":".$gval[10].":".$gval[11].":".$gval[12].":".$gval[13].":".$gval[14].":".$gval[15].":".$gval[16].":".$gval[17].":".$gval[18].":".$gval[19].":".$gval[20].":".$gval[21].":".$gval[22].":".$gval[23].":".$gval[24].":".$gval[25]);
} elseif (($gval[0] == 6) && ($gval[2] == 2)) { 
	unset($_SESSION['accounts_pay_search']);
	header("Location: ".$_SERVER['PHP_SELF']."?id=3:".$gval[1].":".$gval[2].":".$gval[3].":".$gval[4].":". $gval[5].":".$gval[6].":".$gval[7].":".$gval[8].":".$gval[9].":".$gval[10].":".$gval[11].":".$gval[12].":".$gval[13].":".$gval[14].":".$gval[15].":".$gval[16].":".$gval[17].":".$gval[18].":".$gval[19].":".$gval[20].":".$gval[21].":".$gval[22].":".$gval[23].":".$gval[24].":".$gval[25]);
} ?>
<table border="0" width="98%" cellpadding="0" cellspacing="3">
<tr>
	<td width="3" rowspan="2">
		<img src="/images/spacer.gif" height="1" width="10" border="0">
	</td>
	<td valign="top">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td width="3"><img src="/images/spacer.gif" height="3" width="5" border="0"></td>
		</tr>
		<tr>
		<?php 
		// get section label and header 
		$sql = "SELECT label FROM menu_tabs WHERE registrantid = ".RID." AND hashid = '".$gval[20]."'"; 
		// echo $sql;
		$res = mysql_query($sql);
		$row = mysql_fetch_array($res); ?>
			<td width="3%" align="left"><img src="/images/icons/24_shadow/chart.png" border=0></td>
			<td width="80%" align="left">&nbsp;<strong><font style="font-size:140%;font-family:Trebuchet MS"><?php echo $row['label']; ?></font></strong> </td>
			<td width="15%" align="right">&nbsp;</td>
			<td width="1%">&nbsp;</td>
		</tr>
		</table><br>
	</td>
</tr>
<tr>
	<td colspan="2">
		<table border="0" width="99%">
		<?php 
		// gval[0] 1/2/3 reserved for section headers
		if ($gval[0] == 1) {
			// $gval[0] = 1 is bank accounts setup ?>
			<tr>
				<td colspan="9" class="ls_top"><strong>Bank Accounts</strong></td>
			</tr>
			<tr>
				<td colspan="9">
					<br style="line-height:5px">
					&nbsp;<a href="banks.php?id=2" <?php echo LFD ?>><font class="note">Add New Bank Account</font></a><br><br>
				</td>
			</tr>
			<tr align="center">
				<td class="ls_top">&nbsp;</td>
				<td class="ls_top"><strong>Account Name</strong></td>
				<td class="ls_top"><strong>Starting Date</strong></td>
				<td class="ls_top"><strong>Opening Balance</strong></td>
				<td class="ls_top"><strong>Current Balance</strong></td>
				<td class="ls_top"><strong>Default Payment<br>Method for Invoices</strong></td>
				<td class="ls_top"><strong>Default Payment<br>Method for Purchases</strong></td>
				<td class="ls_top"><strong>Nominal Code</strong></td>
				<td class="ls_top"><strong>Actions</strong></td>
			</tr>
			<?php 
			$sql = "SELECT * FROM bank_accounts WHERE registrantid =".RID." ORDER BY defaultaccount DESC"; 
			// echo $sql;
			$res = mysql_query($sql);
			while ($row = mysql_fetch_array($res)) { 
				include("includes/ls.php");?>
			<tr>
				<td class="ls_<?php echo $ls ?>" width="1%" align="center"><img src="/images/icons/16_shadow/office-building.png"></td>
				<td class="ls_<?php echo $ls ?>"><?php echo $row['accountname'] ?></td>
				<td class="ls_<?php echo $ls ?>">
					<?php 
					$startingmonth = explode("-",substr($row['starting_date'],0,11));
					$startdatefinal = $startingmonth[2]."-".convertDate($startingmonth[1])."-".$startingmonth[0];
					echo $startdatefinal ?>
				</td>
				<td align="right" class="ls_<?php echo $ls ?>"><?php echo number_format($row['opening_balance'],2) ?></td>
				<td class="ls_<?php echo $ls ?>">-</td>
				<td class="ls_<?php echo $ls ?>">
					<?php 
					$sql_2 = "SELECT method_name FROM  payment_methods WHERE id = ".$row['payment_methods_default_invoices']." "; 
					// echo $sql;
					$res_2 = mysql_query($sql_2);
					$row_2 = mysql_fetch_array($res_2);
					echo $row_2['method_name'] ?>
				</td>
				<td class="ls_<?php echo $ls ?>">
					<?php 
					$sql_2 = "SELECT method_name FROM  payment_methods WHERE id = ".$row['payment_methods_default_purchases']." "; 
					// echo $sql;
					$res_2 = mysql_query($sql_2);
					$row_2 = mysql_fetch_array($res_2);
					echo $row_2['method_name'] ?>
				</td>
				<td class="ls_<?php echo $ls ?>"><?php echo $row['nominal_code'] ?></td>
				<td align="center" class="ls_<?php echo $ls ?>"><a href="banks.php?id=1:<?php echo md5($row['id']) ?>" <?php echo LFD ?>><font class="note">Edit</font></a></td>
			</tr>
			<?php 
			} ?>
		<?php 
		} elseif ( ($gval[0] == 2) || ($gval[0] == "") ) { 
			// $gval[0] = 2 is bank receive money ?>
			<?php 
			$sql_sec = "SELECT mt.label, mst.hashid FROM menu_tabs mt, menusub_tabs mst 
					WHERE mst.componentabtype = 1 
					AND mst.menu_tabhashid = mt.hashid
					AND mst.registrantid = ".RID."
					ORDER BY mt.sortorder ASC"; 
			// echo $sql_sec."<br><br>";
			$res_sec = mysql_query($sql_sec);
			while ($row_sec = mysql_fetch_array($res_sec)) {
				$primarylocation = $row_sec['hashid'];
				$getallrecs = "SELECT rmx.parenttable, rv.matrix_id, rv.parent_recordid ,udf.recordid, udf.tlsrecidinvoiceddate, udf.tlsinvoiceterms, 
							DATE_ADD(udf.tlsrecidinvoiceddate,INTERVAL udf.tlsinvoiceterms DAY) as duedate, 
							DATEDIFF(DATE_ADD(udf.tlsrecidinvoiceddate,INTERVAL udf.tlsinvoiceterms DAY),
							CURRENT_DATE()) AS tildue, 
							SUM(s1.salesprice * quantity) as WithoutTax, 
							SUM(ROUND(
								(s1.salesprice * quantity) * ((taxcode/100)+1),2))-IFNULL((
									SELECT SUM(amountdebtors) FROM payments pay WHERE pay.parenttable = rmx.parenttable 
									AND pay.parent_recordid = rv.parent_recordid
									AND pay.recordid = udf.recordid
									),0) as WithTax 
							FROM udf_".$primarylocation." udf 
							INNER JOIN sales_1 s1 ON udf.hashid = s1.recordid 
							INNER JOIN relations_values rv ON udf.hashid = rv.recordid 
							INNER JOIN relations_matrix rmx ON rmx.id = rv.matrix_id 
							AND udf.tlsrecidinvoiceispaid = 0
							AND udf.issaved = 1
							AND rv.location = '".$primarylocation."' 
							WHERE udf.tlsrecidinvoiced = 1 GROUP BY udf.recordid ORDER BY udf.recordid ASC "; 
				// echo $getallrecs."<br><br>";
				$res = mysql_query($getallrecs);
				while ($row = mysql_fetch_array($res)) { 
					// Now we know the parent table and parent record id
					if ($row['parenttable'] == "1679091C5A880FAF6FB5E6087EB1B2DC") {
						$getlabels = "SELECT recordid, companyname, termsdays FROM udf_1679091C5A880FAF6FB5E6087EB1B2DC WHERE hashid = '".$row['parent_recordid']."' LIMIT 1";
						$res_2 = mysql_query($getlabels);
						$row_2 = mysql_fetch_array($res_2);
						$insertstring .= "('".$row_2['recordid']."','".$row['parenttable']."','".$row['parent_recordid']."','".$row_2['companyname']."','".$primarylocation."','".$row_2['termsdays']."','".$row['tildue']."','".$row['WithTax']."')";
					} elseif ($row['parenttable'] == "45C48CCE2E2D7FBDEA1AFC51C7C6AD26") {
						$getlabels = "SELECT recordid, firstname, lastname FROM udf_45C48CCE2E2D7FBDEA1AFC51C7C6AD26 WHERE hashid = '".$row['parent_recordid']."' LIMIT 1";
						$res_2 = mysql_query($getlabels);
						$row_2 = mysql_fetch_array($res_2);
						$theoutputname = $row_2['firstname']." ".$row_2['lastname'];;
						$insertstring .= "('".$row_2['recordid']."','".$row['parenttable']."','".$row['parent_recordid']."','".$theoutputname."','".$primarylocation."','".$row_2['termsdays']."','".$row['tildue']."','".$row['WithTax']."')";
					} 
					
					$insertstring .= ",";
					
				} 
			}
			
			$insertstring = substr($insertstring, 0, -1); 
			
			$ceatetemp = "CREATE TEMPORARY TABLE `accounts_receivemoney_temp` (
									`recordid` VARCHAR( 100 ) NOT NULL ,
									`parenttable` VARCHAR( 350 ) NOT NULL ,
									`parent_recordid` VARCHAR( 350 ) NOT NULL ,
									`customername` VARCHAR( 350 ) NOT NULL ,
									`location` VARCHAR( 350 ) NOT NULL ,
									`terms` INT NULL ,
									`tildue` INT NULL ,
									`incltax` DECIMAL( 10, 2 ) NOT NULL 
						) ENGINE = MYISAM ; ";
			mysql_query($ceatetemp);
			
			// insert the values into the temporary table
			$insertempdata = "INSERT INTO `accounts_receivemoney_temp` ( `recordid` , `parenttable`, `parent_recordid` , `customername` , `location` , `terms`, `tildue` ,  `incltax` ) 
							  VALUES ".$insertstring.""; 
			// echo $insertempdata."<br>";
			mysql_query($insertempdata); 
			
			// Lets create the full output table so we are able to sort and search properly
			$ceatetemp = "CREATE TEMPORARY TABLE `accounts_receivemoney_structured_temp` (
									`parenttable` VARCHAR( 350 ) NOT NULL ,
									`parent_recordid` VARCHAR( 350 ) NOT NULL ,
									`customername` VARCHAR( 350 ) NOT NULL ,
									`location` VARCHAR( 350 ) NOT NULL ,
									`totals` DECIMAL( 10, 2 )  NULL ,
									`totalcurrent` DECIMAL( 10, 2 )  NULL ,
									`totaloverdue` DECIMAL( 10, 2 )  NULL ,
									`thirtydays` DECIMAL( 10, 2 )  NULL ,
									`sixtydays` DECIMAL( 10, 2 )  NULL ,
									`ninetydays` DECIMAL( 10, 2 )  NULL ,
									`overninetydays` DECIMAL( 10, 2 )  NULL 
						) ENGINE = MYISAM ; ";
			mysql_query($ceatetemp);
			
			$parenttableexcl = array('45C48CCE2E2D7FBDEA1AFC51C7C6AD26','1679091C5A880FAF6FB5E6087EB1B2DC');
			foreach ($parenttableexcl AS &$parenttableexclusive) {
			
				$sql = "SELECT customername, parenttable, parent_recordid, location FROM accounts_receivemoney_temp WHERE parenttable = '".$parenttableexclusive."' GROUP BY parent_recordid ORDER BY customername";
				$query_pages = $sql;
				$res = mysql_query($sql);
				// echo $sql;
				while ($row = mysql_fetch_array($res)) { 
					// Now lets the values within certain aged criteria 
					$getcrit = "SELECT SUM(incltax) AS inctax FROM accounts_receivemoney_temp WHERE parent_recordid = '".$row['parent_recordid']."' AND parenttable = '".$parenttableexclusive."'"; 
					//echo $getcrit."<br>";
					$resgetcrit = mysql_query($getcrit);
					$rowgetcrit = mysql_fetch_array($resgetcrit);
						$grandtotal = $grandtotal + $rowgetcrit['inctax'];
						$grandtotalcust = $rowgetcrit['inctax'];
					// Now lets the values within certain aged criteria 
					$getcrit = "SELECT SUM(incltax) AS inctax FROM accounts_receivemoney_temp WHERE tildue >= 0 AND parent_recordid = '".$row['parent_recordid']."' AND parenttable = '".$parenttableexclusive."'"; 
					//echo $getcrit."<br>";
					$resgetcrit = mysql_query($getcrit);
					$rowgetcrit = mysql_fetch_array($resgetcrit); 
						$current = $current + $rowgetcrit['inctax'];
						$currentcust = $rowgetcrit['inctax'];
					// Now lets the values within certain aged criteria 
					$getcrit = "SELECT SUM(incltax) AS inctax FROM accounts_receivemoney_temp WHERE tildue < 0 AND parent_recordid = '".$row['parent_recordid']."' AND parenttable = '".$parenttableexclusive."'"; 
					//echo $getcrit."<br>";
					$resgetcrit = mysql_query($getcrit);
					$rowgetcrit = mysql_fetch_array($resgetcrit); 
						$totaloverdue = $totaloverdue + $rowgetcrit['inctax'];
						$totaloverduecust = $rowgetcrit['inctax'];
					// Now lets the values within certain aged criteria 
					$getcrit = "SELECT SUM(incltax) AS inctax FROM accounts_receivemoney_temp WHERE (tildue < 0 AND tildue >= -30) AND parent_recordid = '".$row['parent_recordid']."' AND parenttable = '".$parenttableexclusive."'"; 
					//echo $getcrit."<br>";
					$resgetcrit = mysql_query($getcrit);
					$rowgetcrit = mysql_fetch_array($resgetcrit);
						$thirtydays = $thirtydays + $rowgetcrit['inctax'];
						$thirtydayscust = $rowgetcrit['inctax'];
					// Now lets the values within certain aged criteria 
					$getcrit = "SELECT SUM(incltax) AS inctax FROM accounts_receivemoney_temp WHERE (tildue < -30 AND tildue >= -60) AND parent_recordid = '".$row['parent_recordid']."' AND parenttable = '".$parenttableexclusive."'"; 
					//echo $getcrit."<br>";
					$resgetcrit = mysql_query($getcrit);
					$rowgetcrit = mysql_fetch_array($resgetcrit); 
						$sixtydays = $sixtydays + $rowgetcrit['inctax'];
						$sixtydayscust = $rowgetcrit['inctax'];
					// Now lets the values within certain aged criteria 
					$getcrit = "SELECT SUM(incltax) AS inctax FROM accounts_receivemoney_temp WHERE (tildue < -60 AND tildue >= -90) AND parent_recordid = '".$row['parent_recordid']."' AND parenttable = '".$parenttableexclusive."'"; 
					//echo $getcrit."<br>";
					$resgetcrit = mysql_query($getcrit);
					$rowgetcrit = mysql_fetch_array($resgetcrit); 
						$ninetydays = $ninetydays + $rowgetcrit['inctax'];
						$ninetydayscust = $rowgetcrit['inctax'];
					// Now lets the values within certain aged criteria 
					$getcrit = "SELECT SUM(incltax) AS inctax FROM accounts_receivemoney_temp WHERE tildue < -90 AND parent_recordid = '".$row['parent_recordid']."' AND parenttable = '".$parenttableexclusive."'"; 
					//echo $getcrit."<br>";
					$resgetcrit = mysql_query($getcrit);
					$rowgetcrit = mysql_fetch_array($resgetcrit);
						$overninetydays = $overninetydays + $rowgetcrit['inctax'];
						$overninetydayscust = $rowgetcrit['inctax'];
					
					// insert the values into the temporary table
					$insertempdata = "INSERT INTO `accounts_receivemoney_structured_temp` ( `parenttable`, `parent_recordid` , `customername` , `location`, `totals`, `totalcurrent`,`totaloverdue`,`thirtydays`,`sixtydays`,`ninetydays`,`overninetydays` ) 
								  	VALUES (";
					$insertempdata .= "'".$row['parenttable']."','".$row['parent_recordid']."','".$row['customername']."','".$row['location']."','".$grandtotalcust."', '".$currentcust."','".$totaloverduecust."','".$thirtydayscust."','".$sixtydayscust."','".$ninetydayscust."','".$overninetydayscust."'	";
					$insertempdata .= ")";
									
					//	echo $insertempdata."<br><br>";
					mysql_query($insertempdata) or die("Cannot Insert 44558"); 
					
					unset($grandtotalcust,$currentcust,$totaloverduecust,$thirtydayscust,$sixtydayscust,$ninetydayscust,$overninetydayscust);
				} 
			}?>
			<tr>
				<td colspan="8" class="ls_top"><strong>Receive Money</strong></td>
			</tr>
			<tr align="center">
				<td width="14%" class="ls_top"><strong>Total Owed</strong></td>
				<td width="14%" class="ls_top"><strong>Current</strong></td>
				<td width="14%" class="ls_top"><strong>Overdue</strong></td>
				<td width="14%" class="ls_top"><strong>30 Days</strong></td>
				<td width="14%" class="ls_top"><strong>60 Days</strong></td>
				<td width="14%" class="ls_top"><strong>90 Days</strong></td>
				<td width="16%" class="ls_top"><strong>Over 90 Days</strong></td>
			</tr>
			<tr align="right">
				<td class="ls_off"><?php echo number_format($grandtotal,2); ?></td>
				<td class="ls_off"><?php echo number_format($current,2); ?></td>
				<td class="ls_off"><?php echo number_format($totaloverdue,2); ?></td>
				<td class="ls_off"><?php echo number_format($thirtydays,2); ?></td>
				<td class="ls_off"><?php echo number_format($sixtydays,2); ?></td>
				<td class="ls_off"><?php echo number_format($ninetydays,2); ?></td>
				<td class="ls_off"><?php echo number_format($overninetydays,2); ?></td>
			</tr>
			</table><br>
			<table border="0" width="99%" cellpadding="3" cellspacing="2">
			<?php 
			$addsearchstr = "acccounts_search"; 
			if ($_COOKIE[$addsearchstr] != "on") {
				$displayslide = 'style="display:none;"';
			} else {
				$displayslide2 = 'style="display:none;"';
			} ?>
			<tr>
				<td width="20%">
					<font class="note">
					<span id="ShowSearchOption" <?php echo $displayslide2; ?>><a href="#" onclick="javascript: slideLockOn('ShowSearchOption','ShowCloseSearch','<?php echo $addsearchstr; ?>'); Effect.SlideDown('slidedown_demo', { duration: 0.5, queue: {position: 'end', scope: 'slidedown_demo', limit: 2} }); return false;">Show Search Fields</a></span><span id="ShowCloseSearch" <?php echo $displayslide ?>><a href="#" onclick="javascript: slideLockOff('ShowCloseSearch','ShowSearchOption','<?php echo $addsearchstr; ?>'); Effect.SlideUp('slidedown_demo', { duration: 0.5, queue: {position: 'end', scope: 'slidedown_demo', limit: 2} }); return false;">Hide Search Fields</a></span>
					| <a href="thinline.php?id=6::1::::::::::::::::::<?php echo $gval[20] ?>"><font class="note">Remove Filters</font></a>
					</font>
				</td>
				<td width="15">&nbsp;</td>
				<td>
				<?php 
				if (($_SESSION['accounts_receive_search'] == 1) || ($_POST['postsearch'] == 1)) { ?>
					<table>
					<tr>
						<td width="100" align="center" class="messageboard">Filter Loaded</td>
					</tr>
					</table>
					<?php 
				} ?>
				</td>
				<td align="right"><?php include_once("includes/record_gen.php"); ?></td>
			</tr>
			</table>
			<form method="POST" action="thinline.php?id=2::::::::::::::::::::<?php echo $gval[20] ?>">
			<div id="slidedown_demo" <?php echo $displayslide; ?>>
			<div>
			<table width="99%" border="0" cellspacing="2" cellpadding="1">
			<tr>
				<td>
					<table border="0" cellspacing="2" cellpadding="1">
					<tr>
						<td align="center" class="ls_on">
						<?php 
							if (($_SESSION['accounts_receive_search'] == 1) && ($_POST['postsearch'] != 1)) {
								$query_array = "SELECT searchresults FROM accounts_list_results_arrays WHERE sessionid = '".session_id()."' LIMIT 1";
								// echo $query_array;
								$result_array = mysql_query($query_array);
								$row_array = mysql_fetch_array($result_array);
								$results = explode("||",$row_array['searchresults']);
								$customernamesrchval = $results[0];
								$checkerval = $results[1];
							} else {
								$customernamesrchval = $_POST['customername'];
								$checkerval = $_POST['likesearchtype'];
							} ?>
							<input type="text" class="standardfield" name="customername" value="<?php echo $customernamesrchval; ?>">
						</td>
						<td align="center" class="ls_on">
							<select name="likesearchtype">
							<?php 
								$likesrcharray = array(3 => "Contains", 1 => "Starts With", 2 => "Exact"); 
								foreach ($likesrcharray as $likevalid => $likeval) { 
									if ($likevalid == $checkerval) { $likeselected = "selected";  }?>
									<option <?php echo $likeselected; ?> value="<?php echo $likevalid; ?>"><?php echo $likeval ?> </option>
								<?php 
									unset($likeselected);
								} ?>
							</select>
						</td>
						<td align="center" class="ls_on">
							<input type="submit" style="font-family:Tahoma;font-size:11px;width:50px" value=" Find ">
						</td>
						<td width="15%">&nbsp;</td>
						<td align="right">
							<font class="note">To receive a payment, simply click the contact <strong>Name</strong>.</font>
						</td>
					</tr>
					</table>	
				</td>
			</tr>
			</table>
			<input type="hidden" name="postsearch" value="1">
			</form>
			</div>
			<div class="clear"></div>
			</div>
			<table width="100%" border="0" cellspacing="2" cellpadding="2">
			<?php 
			$num_rows = 8;
			$location = $gval[1];
			$tablecolumns = "Name,Total,Total Current,Total Overdue,30 Days,60 Days, 90 Days, Over 90 Days";
			$columnstr = "customername,totals,totalcurrent,totaloverdue,thirtydays,sixtydays,ninetydays,overninetydays";
			$tablecolumns = explode(",", $tablecolumns);
			$columnstr = explode(",", $columnstr);
			$trcounter = 1;
			$getprimary = 1;
			$columnstrcount = 0;
			foreach($tablecolumns as &$value) {
				if ($trcounter == 1) { echo "<tr>\n";}
				if ($getprimary == 1) {	$primarysortlabel = columnnames($value); }
				echo "<td class=\"ls_top\" align='center'>
				<table border=0><tr><td><strong>".$value."</strong>&nbsp;</td>";
				if ($gval[18] == "ASC") {
					echo '<td><a href="thinline.php?id='.$gval[0].':'.$gval[1].':'.$gval[2].':::::'.$gval[7].':'.$gval[8].':'.$gval[9].':'.$gval[10].':'.$gval[11].':'.$gval[12].':'.$gval[13].':'.$gval[14].':'.$gval[15].':'.$gval[16].':'.$gval[17].':DESC:'.$columnstr[$columnstrcount].':'.$gval[20].':'.$gval[21].':'.$gval[22].':'.$gval[23].':'.$gval[24].':'.$gval[25].'">';
					if ($gval[19] == $columnstr[$columnstrcount]) {
						echo "<img src=\"/images/icons/sort_asc.gif\" border=\"0\"></a></td>";
					} else {
						echo "<img src=\"/images/icons/sort_asc_g.gif\" border=\"0\"></a></td>";
					}
				} else {
					echo '<td><a href="thinline.php?id='.$gval[0].':'.$gval[1].':'.$gval[2].':::::'.$gval[7].':'.$gval[8].':'.$gval[9].':'.$gval[10].':'.$gval[11].':'.$gval[12].':'.$gval[13].':'.$gval[14].':'.$gval[15].':'.$gval[16].':'.$gval[17].':ASC:'.$columnstr[$columnstrcount].':'.$gval[20].':'.$gval[21].':'.$gval[22].':'.$gval[23].':'.$gval[24].':'.$gval[25].'">';
					if ($gval[19] == $columnstr[$columnstrcount]) {
						echo "<img src=\"/images/icons/sort_desc.gif\" border=\"0\"></a></td>";
					} else {
						echo "<img src=\"/images/icons/sort_asc_g.gif\" border=\"0\"></a></td>";
					}
				}
				echo "</tr></table></td>\n";
				if ($trcounter == $num_rows) { 
					echo "<td colspan=\"2\">&nbsp;</td>\n";
					echo "</tr>\n"; $trcounter = 0; 
				}
				$trcounter++;
				$getprimary++;
				$columnstrcount++;
			}
			
			/// Searchoing for results
			
			if (($_POST['postsearch'] == 1) || ($_SESSION['accounts_receive_search'] == NULL)) {
				if ($_POST['postsearch'] == 1) {
					$_SESSION['accounts_receive_search'] = 1;
				}
				// Searching on customer name
				// Lets add the subject search fields
				if ($_POST['likesearchtype'] == 3) {
					$addsql = " WHERE customername LIKE '%".$_POST['customername']."%' ";
				} elseif ($_POST['likesearchtype'] == 2) {
					$addsql = " WHERE customername = '".$_POST['customername']."' ";
				} elseif ($_POST['likesearchtype'] == 1) {
					$addsql = " WHERE customername LIKE '".$_POST['customername']."%' ";
				}
				
				$sql = "SELECT * FROM accounts_receivemoney_structured_temp ".$addsql."";
				$sqllistarray = $sql;
				$query_pages = $sql;
				if ($gval[18] == "") {
					if ($gval[9] == "") {
						$sql .= " ORDER BY customername ASC ";
						//$sqllistarray = $sql;
						$sql .= "LIMIT 0, ".$size_of_group;
					} else {
						$sql .= " ORDER BY customername ASC ";
						//$sqllistarray = $sql;
						$sql .= " LIMIT ".$gval[9].", ".$size_of_group."";
					}
				} else {
					if ($gval[9] == "") {
						$gval[9] = 0;
					} 
					$sql .= " ORDER BY ".$gval[19]." ".$gval[18]."";
					$sql .= " LIMIT ".$gval[9].", ".$size_of_group."";
				}
				
				$querylra = "DELETE FROM accounts_list_results_arrays WHERE sessionid = '".session_id()."'";
				mysql_query($querylra) Or Die ("Cannot submit list array!");
				
				$querylra = "INSERT INTO accounts_list_results_arrays (sessionid, searchresults, datecreated) VALUES (
				  						'".session_id()."',
				  						'".addslashes($_POST['customername'])."||".$_POST['likesearchtype']."',
				 						'".$sGMTMySqlString."'
				  						)";
				mysql_query($querylra) Or Die ("Cannot submit list array!");
				
			} else {
				$checklistx = "SELECT list_array FROM accounts_list_arrays WHERE sessionid = '".session_id()."'";
				$reslistx = mysql_query($checklistx);
				$rowlistx = mysql_fetch_array($reslistx);
				$sql = $rowlistx['list_array'];
				$sqllistarray = $sql;
				if ($gval[9] == "") { 
					$gval[9] = 0; 
				} 
				if ($gval[18] != "") {
					$sql .= " ORDER BY ".$gval[19]." ".$gval[18]."";
				} else {
					$sql .= " ORDER BY customername ASC";
				}
				$sql .= " LIMIT ".$gval[9].", ".$size_of_group."";
				$query_pages = $rowlistx['list_array'];
			}
			
			$res = mysql_query($sql);
			// echo $sql;
			while ($row = mysql_fetch_array($res)) { 
				include("includes/ls.php"); ?>
				<tr>
					<td class="ls_<?php echo $ls ?>" ><a href="accounts_receive.php?id=1:<?php echo $row['parenttable'] ?>:<?php echo $row['parent_recordid'] ?>:<?php echo $row['location'] ?>" <?php echo LFD ?>><?php echo $row['customername'] ?></a></td>
					<td align="right" class="ls_<?php echo $ls ?>"><?php echo number_format($row['totals'],2) ?></td>
					<td align="right" class="ls_<?php echo $ls ?>">
						<?php 
							if ($row['totalcurrent'] > 0) {
								echo number_format($row['totalcurrent'],2);
							}  ?>&nbsp;
					</td>
					<td align="right" class="ls_<?php echo $ls ?>">
						<font color="red">
						<?php 
						if ($row['totaloverdue'] > 0) {
							echo number_format($row['totaloverdue'],2) ;
						}?>
						</font>&nbsp;
					</td>
					<td align="right" class="ls_<?php echo $ls ?>">
						<font color="red">
						<?php 
						if ($row['thirtydays'] > 0) {
							echo number_format($row['thirtydays'],2);
						} ?>
						</font>&nbsp;
					</td>
					<td align="right" class="ls_<?php echo $ls ?>">
						<font color="red">
						<?php 
						if ($row['sixtydays'] > 0) {
							echo number_format($row['sixtydays'],2);
						} ?>
						</font>&nbsp;
					</td>
					<td align="right" class="ls_<?php echo $ls ?>">
						<font color="red">
						<?php 
						if ($row['ninetydays'] > 0) {
							echo number_format($row['ninetydays'],2);
						} ?>
						</font>&nbsp;
					</td>
					<td align="right" class="ls_<?php echo $ls ?>">
						<font color="red">
						<?php 
						if ($row['overninetydays'] > 0) {
							echo number_format($row['overninetydays'],2);
						} ?>
						</font>&nbsp;
					</td>
				</tr>
			<?php 
			} ?>
			</table>
			<table width="100%" border="0" cellspacing="2" cellpadding="1">
			<tr>
				<td colspan="6"><br><?php include("includes/pages_next_prev.php"); ?></td>
			</tr>
			</table><br>
			
		<?php 
			/////////////////////////////////////////////////////////////////////////////////////////////
			// Ok we need to have this method to be able scroll through inidividual pages of records... /
			/////////////////////////////////////////////////////////////////////////////////////////////
							
			// Lets check if we need to do an insert or update
			$checklistx = "SELECT sessionid FROM accounts_list_arrays WHERE sessionid = '".session_id()."'";
			$reslistx = mysql_query($checklistx);
			$rowlistx = mysql_fetch_array($reslistx);
			if (!$rowlistx['sessionid']) {
				$queryaddact = "INSERT INTO accounts_list_arrays (sessionid, list_array, datecreated) VALUES (
								'".session_id()."',
								'".addslashes($sqllistarray)."',
								'".$sGMTMySqlString."'
								)";
				mysql_query($queryaddact) Or Die ("Cannot submit list array!");
			} else {
				$queryaddact = "UPDATE accounts_list_arrays SET 
								list_array ='".addslashes($sqllistarray)."'
								WHERE sessionid = '".session_id()."'
								";
								// echo $query."<br>";
				mysql_query($queryaddact) Or Die ("Cannot updatelist array!");
			}
			unset($listcounterer,$num_rowslist,$buildliststring);
			/////////////////////////////////////////////////////////////////////////////////////////////
		} elseif ($gval[0] == 3) { 
			// $gval[0] = 3 is bank pay money ?>
			<?php 
			$sql_sec = "SELECT mt.label, mst.hashid FROM menu_tabs mt, menusub_tabs mst 
					WHERE mst.componentabtype = 2 
					AND mst.menu_tabhashid = mt.hashid
					AND mst.registrantid = ".RID."
					ORDER BY mt.sortorder ASC"; 
			// echo $sql_sec."<br><br>";
			$res_sec = mysql_query($sql_sec);
			while ($row_sec = mysql_fetch_array($res_sec)) {
				$primarylocation = $row_sec['hashid'];
				$getallrecs = "SELECT rmx.parenttable, rv.matrix_id, rv.parent_recordid ,udf.recordid, udf.tlsrecidinvoiceddate, udf.tlsinvoiceterms, 
								DATE_ADD(udf.tlsrecidinvoiceddate,INTERVAL udf.tlsinvoiceterms DAY) as duedate, 
								DATEDIFF(DATE_ADD(udf.tlsrecidinvoiceddate,INTERVAL udf.tlsinvoiceterms DAY),
								CURRENT_DATE()) AS tildue, 
								SUM(p1.salesprice * quantity) as WithoutTax, 
								SUM(ROUND(
									(p1.salesprice * quantity) * ((taxcode/100)+1),2))-IFNULL((
										SELECT SUM(amountcreditors) FROM payments pay WHERE pay.parenttable = rmx.parenttable 
										AND pay.parent_recordid = rv.parent_recordid
										AND pay.recordid = udf.recordid
										),0) as WithTax 
								FROM udf_".$primarylocation." udf 
								INNER JOIN purchases_1 p1 ON udf.hashid = p1.recordid 
								INNER JOIN relations_values rv ON udf.hashid = rv.recordid 
								INNER JOIN relations_matrix rmx ON rmx.id = rv.matrix_id 
								AND udf.tlsrecidinvoiceispaid = 0
								AND udf.issaved = 1
								AND rv.location = '".$primarylocation."' 
								WHERE udf.tlsrecidinvoiced = 1 GROUP BY udf.recordid ORDER BY udf.recordid ASC "; 
				// echo $getallrecs."<br><br>";
				$res = mysql_query($getallrecs);
				while ($row = mysql_fetch_array($res)) { 
					// Now we know the parent table and parent record id
					if ($row['parenttable'] == "1679091C5A880FAF6FB5E6087EB1B2DC") {
						$getlabels = "SELECT recordid, companyname, termsdays FROM udf_1679091C5A880FAF6FB5E6087EB1B2DC WHERE hashid = '".$row['parent_recordid']."' LIMIT 1";
						$res_2 = mysql_query($getlabels);
						$row_2 = mysql_fetch_array($res_2);
						$insertstring .= "('".$row_2['recordid']."','".$row['parenttable']."','".$row['parent_recordid']."','".$row_2['companyname']."','".$primarylocation."','".$row_2['termsdays']."','".$row['tildue']."','".$row['WithTax']."')";
					} elseif ($row['parenttable'] == "45C48CCE2E2D7FBDEA1AFC51C7C6AD26") {
						$getlabels = "SELECT recordid, firstname, lastname FROM udf_45C48CCE2E2D7FBDEA1AFC51C7C6AD26 WHERE hashid = '".$row['parent_recordid']."' LIMIT 1";
						$res_2 = mysql_query($getlabels);
						$row_2 = mysql_fetch_array($res_2);
						$theoutputname = $row_2['firstname']." ".$row_2['lastname'];;
						$insertstring .= "('".$row_2['recordid']."','".$row['parenttable']."','".$row['parent_recordid']."','".$theoutputname."','".$primarylocation."','".$row_2['termsdays']."','".$row['tildue']."','".$row['WithTax']."')";
					} 
				
					
					$insertstring .= ",";
				} 
			}
			
			$insertstring = substr($insertstring, 0, -1); 
			
			$ceatetemp = "CREATE TEMPORARY TABLE `accounts_receivemoney_temp` (
									`recordid` VARCHAR( 100 ) NOT NULL ,
									`parenttable` VARCHAR( 350 ) NOT NULL ,
									`parent_recordid` VARCHAR( 350 ) NOT NULL ,
									`customername` VARCHAR( 350 ) NOT NULL ,
									`location` VARCHAR( 350 ) NOT NULL ,
									`terms` INT NULL ,
									`tildue` INT NULL ,
									`incltax` DECIMAL( 10, 2 ) NOT NULL 
						) ENGINE = MYISAM ; ";
			mysql_query($ceatetemp);
			
			// insert the values into the temporary table
			$insertempdata = "INSERT INTO `accounts_receivemoney_temp` ( `recordid` , `parenttable`, `parent_recordid` , `customername` , `location` , `terms`, `tildue` ,  `incltax` ) 
							  VALUES ".$insertstring.""; 
			// echo $insertempdata."<br>";
			mysql_query($insertempdata); 
			
			// Lets create the full output table so we are able to sort and search properly
			$ceatetemp = "CREATE TEMPORARY TABLE `accounts_receivemoney_structured_temp` (
									`parenttable` VARCHAR( 350 ) NOT NULL ,
									`parent_recordid` VARCHAR( 350 ) NOT NULL ,
									`customername` VARCHAR( 350 ) NOT NULL ,
									`location` VARCHAR( 350 ) NOT NULL ,
									`totals` DECIMAL( 10, 2 )  NULL ,
									`totalcurrent` DECIMAL( 10, 2 )  NULL ,
									`totaloverdue` DECIMAL( 10, 2 )  NULL ,
									`thirtydays` DECIMAL( 10, 2 )  NULL ,
									`sixtydays` DECIMAL( 10, 2 )  NULL ,
									`ninetydays` DECIMAL( 10, 2 )  NULL ,
									`overninetydays` DECIMAL( 10, 2 )  NULL 
						) ENGINE = MYISAM ; ";
			mysql_query($ceatetemp);
			
			$parenttableexcl = array('45C48CCE2E2D7FBDEA1AFC51C7C6AD26','1679091C5A880FAF6FB5E6087EB1B2DC');
			foreach ($parenttableexcl AS &$parenttableexclusive) {
			
				$sql = "SELECT customername, parenttable, parent_recordid, location FROM accounts_receivemoney_temp WHERE parenttable = '".$parenttableexclusive."' GROUP BY parent_recordid ORDER BY customername";
				$query_pages = $sql;
				$res = mysql_query($sql);
				// echo $sql;
				while ($row = mysql_fetch_array($res)) { 
					// Now lets the values within certain aged criteria 
					$getcrit = "SELECT SUM(incltax) AS inctax FROM accounts_receivemoney_temp WHERE parent_recordid = '".$row['parent_recordid']."' AND parenttable = '".$parenttableexclusive."'"; 
					//echo $getcrit."<br>";
					$resgetcrit = mysql_query($getcrit);
					$rowgetcrit = mysql_fetch_array($resgetcrit);
						$grandtotal = $grandtotal + $rowgetcrit['inctax'];
						$grandtotalcust = $rowgetcrit['inctax'];
					// Now lets the values within certain aged criteria 
					$getcrit = "SELECT SUM(incltax) AS inctax FROM accounts_receivemoney_temp WHERE tildue >= 0 AND parent_recordid = '".$row['parent_recordid']."' AND parenttable = '".$parenttableexclusive."'"; 
					//echo $getcrit."<br>";
					$resgetcrit = mysql_query($getcrit);
					$rowgetcrit = mysql_fetch_array($resgetcrit); 
						$current = $current + $rowgetcrit['inctax'];
						$currentcust = $rowgetcrit['inctax'];
					// Now lets the values within certain aged criteria 
					$getcrit = "SELECT SUM(incltax) AS inctax FROM accounts_receivemoney_temp WHERE tildue < 0 AND parent_recordid = '".$row['parent_recordid']."' AND parenttable = '".$parenttableexclusive."'"; 
					//echo $getcrit."<br>";
					$resgetcrit = mysql_query($getcrit);
					$rowgetcrit = mysql_fetch_array($resgetcrit); 
						$totaloverdue = $totaloverdue + $rowgetcrit['inctax'];
						$totaloverduecust = $rowgetcrit['inctax'];
					// Now lets the values within certain aged criteria 
					$getcrit = "SELECT SUM(incltax) AS inctax FROM accounts_receivemoney_temp WHERE (tildue < 0 AND tildue >= -30) AND parent_recordid = '".$row['parent_recordid']."' AND parenttable = '".$parenttableexclusive."'"; 
					//echo $getcrit."<br>";
					$resgetcrit = mysql_query($getcrit);
					$rowgetcrit = mysql_fetch_array($resgetcrit);
						$thirtydays = $thirtydays + $rowgetcrit['inctax'];
						$thirtydayscust = $rowgetcrit['inctax'];
					// Now lets the values within certain aged criteria 
					$getcrit = "SELECT SUM(incltax) AS inctax FROM accounts_receivemoney_temp WHERE (tildue < -30 AND tildue >= -60) AND parent_recordid = '".$row['parent_recordid']."' AND parenttable = '".$parenttableexclusive."'"; 
					//echo $getcrit."<br>";
					$resgetcrit = mysql_query($getcrit);
					$rowgetcrit = mysql_fetch_array($resgetcrit); 
						$sixtydays = $sixtydays + $rowgetcrit['inctax'];
						$sixtydayscust = $rowgetcrit['inctax'];
					// Now lets the values within certain aged criteria 
					$getcrit = "SELECT SUM(incltax) AS inctax FROM accounts_receivemoney_temp WHERE (tildue < -60 AND tildue >= -90) AND parent_recordid = '".$row['parent_recordid']."' AND parenttable = '".$parenttableexclusive."'"; 
					//echo $getcrit."<br>";
					$resgetcrit = mysql_query($getcrit);
					$rowgetcrit = mysql_fetch_array($resgetcrit); 
						$ninetydays = $ninetydays + $rowgetcrit['inctax'];
						$ninetydayscust = $rowgetcrit['inctax'];
					// Now lets the values within certain aged criteria 
					$getcrit = "SELECT SUM(incltax) AS inctax FROM accounts_receivemoney_temp WHERE tildue < -90 AND parent_recordid = '".$row['parent_recordid']."' AND parenttable = '".$parenttableexclusive."'"; 
					//echo $getcrit."<br>";
					$resgetcrit = mysql_query($getcrit);
					$rowgetcrit = mysql_fetch_array($resgetcrit);
						$overninetydays = $overninetydays + $rowgetcrit['inctax'];
						$overninetydayscust = $rowgetcrit['inctax'];
					
					// insert the values into the temporary table
					$insertempdata = "INSERT INTO `accounts_receivemoney_structured_temp` ( `parenttable`, `parent_recordid` , `customername`, `location` , `totals`, `totalcurrent`,`totaloverdue`,`thirtydays`,`sixtydays`,`ninetydays`,`overninetydays` ) 
								  	VALUES (";
					$insertempdata .= "'".$row['parenttable']."','".$row['parent_recordid']."','".$row['customername']."','".$row['location']."','".$grandtotalcust."', '".$currentcust."','".$totaloverduecust."','".$thirtydayscust."','".$sixtydayscust."','".$ninetydayscust."','".$overninetydayscust."'	";
					$insertempdata .= ")";
									
						// echo $insertempdata."<br><br>";
					mysql_query($insertempdata) or die("Cannot Insert"); 
					
					unset($grandtotalcust,$currentcust,$totaloverduecust,$thirtydayscust,$sixtydayscust,$ninetydayscust,$overninetydayscust);
				} 
			}?>
			<tr>
				<td colspan="8" class="ls_top"><strong>Pay Money</strong></td>
			</tr>
			<tr align="center">
				<td width="14%" class="ls_top"><strong>Total Owed</strong></td>
				<td width="14%" class="ls_top"><strong>Current</strong></td>
				<td width="14%" class="ls_top"><strong>Overdue</strong></td>
				<td width="14%" class="ls_top"><strong>30 Days</strong></td>
				<td width="14%" class="ls_top"><strong>60 Days</strong></td>
				<td width="14%" class="ls_top"><strong>90 Days</strong></td>
				<td width="16%" class="ls_top"><strong>Over 90 Days</strong></td>
			</tr>
			<tr align="right">
				<td class="ls_off"><?php echo number_format($grandtotal,2); ?></td>
				<td class="ls_off"><?php echo number_format($current,2); ?></td>
				<td class="ls_off"><?php echo number_format($totaloverdue,2); ?></td>
				<td class="ls_off"><?php echo number_format($thirtydays,2); ?></td>
				<td class="ls_off"><?php echo number_format($sixtydays,2); ?></td>
				<td class="ls_off"><?php echo number_format($ninetydays,2); ?></td>
				<td class="ls_off"><?php echo number_format($overninetydays,2); ?></td>
			</tr>
			</table><br>
			<table border="0" width="99%" cellpadding="3" cellspacing="2">
			<?php 
			$addsearchstr = "acccounts_search"; 
			if ($_COOKIE[$addsearchstr] != "on") {
				$displayslide = 'style="display:none;"';
			} else {
				$displayslide2 = 'style="display:none;"';
			} ?>
			<tr>
				<td width="20%">
					<font class="note">
					<span id="ShowSearchOption" <?php echo $displayslide2; ?>><a href="#" onclick="javascript: slideLockOn('ShowSearchOption','ShowCloseSearch','<?php echo $addsearchstr; ?>'); Effect.SlideDown('slidedown_demo', { duration: 0.5, queue: {position: 'end', scope: 'slidedown_demo', limit: 2} }); return false;">Show Search Fields</a></span><span id="ShowCloseSearch" <?php echo $displayslide ?>><a href="#" onclick="javascript: slideLockOff('ShowCloseSearch','ShowSearchOption','<?php echo $addsearchstr; ?>'); Effect.SlideUp('slidedown_demo', { duration: 0.5, queue: {position: 'end', scope: 'slidedown_demo', limit: 2} }); return false;">Hide Search Fields</a></span>
					| <a href="thinline.php?id=6::1::::::::::::::::::<?php echo $gval[20] ?>"><font class="note">Remove Filters</font></a>
					</font>
				</td>
				<td width="15">&nbsp;</td>
				<td>
				<?php 
				if (($_SESSION['accounts_receive_search'] == 1) || ($_POST['postsearch'] == 1)) { ?>
					<table>
					<tr>
						<td width="100" align="center" class="messageboard">Filter Loaded</td>
					</tr>
					</table>
					<?php 
				} ?>
				</td>
				<td align="right"><?php include_once("includes/record_gen.php"); ?></td>
			</tr>
			</table>
			<form method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?>?id=2::::::::::::::::::::<?php echo $gval[20] ?>">
			<div id="slidedown_demo" <?php echo $displayslide; ?>>
			<div>
			<table width="99%" border="0" cellspacing="2" cellpadding="1">
			<tr>
				<td>
					<table border="0" cellspacing="2" cellpadding="1">
					<tr>
						<td align="center" class="ls_on">
						<?php 
							if (($_SESSION['accounts_receive_search'] == 1) && ($_POST['postsearch'] != 1)) {
								$query_array = "SELECT searchresults FROM accounts_list_results_arrays WHERE sessionid = '".session_id()."' LIMIT 1";
								// echo $query_array;
								$result_array = mysql_query($query_array);
								$row_array = mysql_fetch_array($result_array);
								$results = explode("||",$row_array['searchresults']);
								$customernamesrchval = $results[0];
								$checkerval = $results[1];
							} else {
								$customernamesrchval = $_POST['customername'];
								$checkerval = $_POST['likesearchtype'];
							} ?>
							<input type="text" class="standardfield" name="customername" value="<?php echo $customernamesrchval; ?>">
						</td>
						<td align="center" class="ls_on">
							<select name="likesearchtype">
							<?php 
								$likesrcharray = array(3 => "Contains", 1 => "Starts With", 2 => "Exact"); 
								foreach ($likesrcharray as $likevalid => $likeval) { 
									if ($likevalid == $checkerval) { $likeselected = "selected";  }?>
									<option <?php echo $likeselected; ?> value="<?php echo $likevalid; ?>"><?php echo $likeval ?> </option>
								<?php 
									unset($likeselected);
								} ?>
							</select>
						</td>
						<td align="center" class="ls_on">
							<input type="submit" style="font-family:Tahoma;font-size:11px;width:50px" value=" Find ">
						</td>
						<td width="15%">&nbsp;</td>
						<td align="right">
							<font class="note">To make a payment, simply click the contact <strong>Name</strong>.</font>
						</td>
					</tr>
					</table>	
				</td>
			</tr>
			</table>
			<input type="hidden" name="postsearch" value="1">
			</form>
			</div>
			<div class="clear"></div>
			</div>
			<table width="100%" border="0" cellspacing="2" cellpadding="2">
			<?php 
			$num_rows = 8;
			$location = $gval[1];
			$tablecolumns = "Name,Total,Total Current,Total Overdue,30 Days,60 Days, 90 Days, Over 90 Days";
			$columnstr = "customername,totals,totalcurrent,totaloverdue,thirtydays,sixtydays,ninetydays,overninetydays";
			$tablecolumns = explode(",", $tablecolumns);
			$columnstr = explode(",", $columnstr);
			$trcounter = 1;
			$getprimary = 1;
			$columnstrcount = 0;
			foreach($tablecolumns as &$value) {
				if ($trcounter == 1) { echo "<tr>\n";}
				if ($getprimary == 1) {	$primarysortlabel = columnnames($value); }
				echo "<td class=\"ls_top\" align='center'>
				<table border=0><tr><td><strong>".$value."</strong>&nbsp;</td>";
				if ($gval[18] == "ASC") {
					echo '<td><a href="thinline.php?id='.$gval[0].':'.$gval[1].':'.$gval[2].':::::'.$gval[7].':'.$gval[8].':'.$gval[9].':'.$gval[10].':'.$gval[11].':'.$gval[12].':'.$gval[13].':'.$gval[14].':'.$gval[15].':'.$gval[16].':'.$gval[17].':DESC:'.$columnstr[$columnstrcount].':'.$gval[20].':'.$gval[21].':'.$gval[22].':'.$gval[23].':'.$gval[24].':'.$gval[25].'">';
					if ($gval[19] == $columnstr[$columnstrcount]) {
						echo "<img src=\"/images/icons/sort_asc.gif\" border=\"0\"></a></td>";
					} else {
						echo "<img src=\"/images/icons/sort_asc_g.gif\" border=\"0\"></a></td>";
					}
				} else {
					echo '<td><a href="thinline.php?id='.$gval[0].':'.$gval[1].':'.$gval[2].':::::'.$gval[7].':'.$gval[8].':'.$gval[9].':'.$gval[10].':'.$gval[11].':'.$gval[12].':'.$gval[13].':'.$gval[14].':'.$gval[15].':'.$gval[16].':'.$gval[17].':ASC:'.$columnstr[$columnstrcount].':'.$gval[20].':'.$gval[21].':'.$gval[22].':'.$gval[23].':'.$gval[24].':'.$gval[25].'">';
					if ($gval[19] == $columnstr[$columnstrcount]) {
						echo "<img src=\"/images/icons/sort_desc.gif\" border=\"0\"></a></td>";
					} else {
						echo "<img src=\"/images/icons/sort_asc_g.gif\" border=\"0\"></a></td>";
					}
				}
				echo "</tr></table></td>\n";
				if ($trcounter == $num_rows) { 
					echo "<td colspan=\"2\">&nbsp;</td>\n";
					echo "</tr>\n"; $trcounter = 0; 
				}
				$trcounter++;
				$getprimary++;
				$columnstrcount++;
			}
			
			/// Searchoing for results
			
			if (($_POST['postsearch'] == 1) || ($_SESSION['accounts_receive_search'] == NULL)) {
				if ($_POST['postsearch'] == 1) {
					$_SESSION['accounts_receive_search'] = 1;
				}
				// Searching on customer name
				// Lets add the subject search fields
				if ($_POST['likesearchtype'] == 3) {
					$addsql = " WHERE customername LIKE '%".$_POST['customername']."%' ";
				} elseif ($_POST['likesearchtype'] == 2) {
					$addsql = " WHERE customername = '".$_POST['customername']."' ";
				} elseif ($_POST['likesearchtype'] == 1) {
					$addsql = " WHERE customername LIKE '".$_POST['customername']."%' ";
				}
				
				$sql = "SELECT * FROM accounts_receivemoney_structured_temp ".$addsql."";
				$sqllistarray = $sql;
				$query_pages = $sql;
				if ($gval[18] == "") {
					if ($gval[9] == "") {
						$sql .= " ORDER BY customername ASC ";
						//$sqllistarray = $sql;
						$sql .= "LIMIT 0, ".$size_of_group;
					} else {
						$sql .= " ORDER BY customername ASC ";
						//$sqllistarray = $sql;
						$sql .= " LIMIT ".$gval[9].", ".$size_of_group."";
					}
				} else {
					if ($gval[9] == "") {
						$gval[9] = 0;
					} 
					$sql .= " ORDER BY ".$gval[19]." ".$gval[18]."";
					$sql .= " LIMIT ".$gval[9].", ".$size_of_group."";
				}
				
				$querylra = "DELETE FROM accounts_list_results_arrays WHERE sessionid = '".session_id()."'";
				mysql_query($querylra) Or Die ("Cannot submit list array!");
				
				$querylra = "INSERT INTO accounts_list_results_arrays (sessionid, searchresults, datecreated) VALUES (
				  						'".session_id()."',
				  						'".addslashes($_POST['customername'])."||".$_POST['likesearchtype']."',
				 						'".$sGMTMySqlString."'
				  						)";
				mysql_query($querylra) Or Die ("Cannot submit list array!");
				
			} else {
				$checklistx = "SELECT list_array FROM accounts_list_arrays WHERE sessionid = '".session_id()."'";
				$reslistx = mysql_query($checklistx);
				$rowlistx = mysql_fetch_array($reslistx);
				$sql = $rowlistx['list_array'];
				$sqllistarray = $sql;
				if ($gval[9] == "") { 
					$gval[9] = 0; 
				} 
				if ($gval[18] != "") {
					$sql .= " ORDER BY ".$gval[19]." ".$gval[18]."";
				} else {
					$sql .= " ORDER BY customername ASC";
				}
				$sql .= " LIMIT ".$gval[9].", ".$size_of_group."";
				$query_pages = $rowlistx['list_array'];
			}
			
			$res = mysql_query($sql);
			// echo $sql;
			while ($row = mysql_fetch_array($res)) { 
				include("includes/ls.php"); ?>
				<tr>
					<td class="ls_<?php echo $ls ?>" ><a href="accounts_pay.php?id=1:<?php echo $row['parenttable'] ?>:<?php echo $row['parent_recordid'] ?>:<?php echo $row['location'] ?>" <?php echo LFD ?>><?php echo $row['customername'] ?></a></td>
					<td align="right" class="ls_<?php echo $ls ?>"><?php echo number_format($row['totals'],2) ?></td>
					<td align="right" class="ls_<?php echo $ls ?>">
						<?php 
							if ($row['totalcurrent'] > 0) {
								echo number_format($row['totalcurrent'],2);
							}  ?>&nbsp;
					</td>
					<td align="right" class="ls_<?php echo $ls ?>">
						<font color="red">
						<?php 
						if ($row['totaloverdue'] > 0) {
							echo number_format($row['totaloverdue'],2) ;
						}?>
						</font>&nbsp;
					</td>
					<td align="right" class="ls_<?php echo $ls ?>">
						<font color="red">
						<?php 
						if ($row['thirtydays'] > 0) {
							echo number_format($row['thirtydays'],2);
						} ?>
						</font>&nbsp;
					</td>
					<td align="right" class="ls_<?php echo $ls ?>">
						<font color="red">
						<?php 
						if ($row['sixtydays'] > 0) {
							echo number_format($row['sixtydays'],2);
						} ?>
						</font>&nbsp;
					</td>
					<td align="right" class="ls_<?php echo $ls ?>">
						<font color="red">
						<?php 
						if ($row['ninetydays'] > 0) {
							echo number_format($row['ninetydays'],2);
						} ?>
						</font>&nbsp;
					</td>
					<td align="right" class="ls_<?php echo $ls ?>">
						<font color="red">
						<?php 
						if ($row['overninetydays'] > 0) {
							echo number_format($row['overninetydays'],2);
						} ?>
						</font>&nbsp;
					</td>
				</tr>
			<?php 
			} ?>
			</table>
			<table width="100%" border="0" cellspacing="2" cellpadding="1">
			<tr>
				<td colspan="6"><br><?php include("includes/pages_next_prev.php"); ?></td>
			</tr>
			</table><br>
			
		<?php 
			/////////////////////////////////////////////////////////////////////////////////////////////
			// Ok we need to have this method to be able scroll through inidividual pages of records... /
			/////////////////////////////////////////////////////////////////////////////////////////////
							
			// Lets check if we need to do an insert or update
			$checklistx = "SELECT sessionid FROM accounts_list_arrays WHERE sessionid = '".session_id()."'";
			$reslistx = mysql_query($checklistx);
			$rowlistx = mysql_fetch_array($reslistx);
			if (!$rowlistx['sessionid']) {
				$queryaddact = "INSERT INTO accounts_list_arrays (sessionid, list_array, datecreated) VALUES (
								'".session_id()."',
								'".addslashes($sqllistarray)."',
								'".$sGMTMySqlString."'
								)";
				mysql_query($queryaddact) Or Die ("Cannot submit list array!");
			} else {
				$queryaddact = "UPDATE accounts_list_arrays SET 
								list_array ='".addslashes($sqllistarray)."'
								WHERE sessionid = '".session_id()."'
								";
								// echo $query."<br>";
				mysql_query($queryaddact) Or Die ("Cannot updatelist array!");
			}
			unset($listcounterer,$num_rowslist,$buildliststring);
			/////////////////////////////////////////////////////////////////////////////////////////////
		} elseif ($gval[0] == 4) { 
			// $gval[0] = 3 is bank pay money ?>
			<tr>
				<td colspan="5" class="ls_top"><strong>Transaction Types</strong></td>
			</tr>
			<tr>
				<td colspan="8">
					<br style="line-height:5px">
					&nbsp;<a href="accounts_codes.php?id=2" <?php echo LFD ?>><font class="note">Add New Code</font></a><br><br>
				</td>
			</tr>
			<tr align="center">
				<td width="3%" class="ls_top">&nbsp;</td>
				<td width="23%" class="ls_top"><strong>Type</strong></td>
				<td width="15%" class="ls_top"><strong>Code</strong></td>
				<td class="ls_top"><strong>Name</strong></td>
				<td width="10%" class="ls_top"><strong>Actions</strong></td>
			</tr>
			<?php 
			$sql = "SELECT * FROM transaction_types ORDER BY name ASC";
			$res = mysql_query($sql);
			while ($row = mysql_fetch_array($res)) { 
				include("includes/ls.php"); ?>
				<tr>
					<td align="center" class="ls_<?php echo $ls ?>"><img src="/images/icons/16_shadow/text_code_colored.png"></td>
					<td class="ls_<?php echo $ls ?>"><?php echo $row['codetype'] ?>&nbsp;</td>
					<td class="ls_<?php echo $ls ?>"><?php echo $row['code'] ?></td>
					<td class="ls_<?php echo $ls ?>"><?php echo $row['name'] ?></td>
					<td align="center" class="ls_<?php echo $ls ?>"><a href="accounts_codes.php?id=1:<?php echo md5($row['id'])?>" <?php echo LFD ?>><font class="note">Edit</font></a></td>
				</tr>
			<?php 
			}
		} ?>
		</table>
	</td>
</tr>
</table><br><br>
