<?php
include_once("_globalconnect.php");
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Mon, 26 Jul 2050 05:00:00 GMT");
$imageStatus = array();
if (isset($_POST['fieldtype']) && isset($_POST['value']) && isset($_POST['gval_21']) && isset($_POST['gval_1']) && isset($_FILES["image"])) { // Image Uploading
    //var_dump( $_FILES["image"]['name']);
    $gval[21] = trim($_POST['gval_21']);
    $filename = $_SESSION['documentstorepath'] . $gval[21];
    $dir = $_SESSION['documentstorepath'] . $gval[21];
    if (is_dir($dir) === false && !file_exists($filename)) {
        mkdir($dir, 0777, true);
        sleep(1);
    }
    $target_path = $_SESSION['documentstorepath'] . $gval[21] . "/";
    $target_path = $target_path . str_replace(' ', '_', basename($_FILES['image']['name']));
    if ($_POST['fieldtype'] == 17) {
        $image_name = 'Image ';
    } else {
        $image_name = 'Document ';
    }
    if (!file_exists($target_path)) {
        $checkfilesize = filesize($_FILES['image']['tmp_name']);
        $file_name = basename($_FILES['image']['name']);
        $extension = strtolower(substr($file_name, -3));
        if ($checkfilesize < 5000000) {
            if (($extension != ".js") && ($extension != "js") && ($extension != "exe")) {
                if (move_uploaded_file($_FILES['image']['tmp_name'], $target_path)) {
                    chmod($target_path, 0775);
                    $imageStatus['message'] = $image_name . 'uploaded successfully';
                    $imageStatus['image_name'] = $_FILES['image']['name'];
                    $imageStatus['image_path'] = $target_path;
                } else {
                    $imageStatus['message'] = 'There was an error uploading the file, please try again!';
                    $imageStatus['image_name'] = '';
                }
            } else {
                $imageStatus['message'] = "Due to security restrictions we cannot allow file with the extension <strong>" . $extension . "</strong> to be uploaded.";
                $imageStatus['image_name'] = '';
            }
        } else {
            $imageStatus['message'] = "This file is too big to download.";
            $imageStatus['image_name'] = '';
        }
    } else {
//        $imageStatus['message'] = "File already exists. Please change the filename.";
        $imageStatus['image_name'] = $_FILES['image']['name'];
        $imageStatus['message'] = $image_name . 'uploaded successfully';
        $imageStatus['image_path'] = $target_path;
    }
    echo json_encode($imageStatus);
} else {
    $imageStatus['image_name'] = $_FILES['image']['name'];
    $imageStatus['message'] = 'There is some problem in uploading the data';
    $imageStatus['image_path'] = $target_path;
    echo json_encode($imageStatus);
}
?>