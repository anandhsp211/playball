<?php 
include_once("_globalconnect.php");
if ($_POST['recordid'] != "") {
    foreach ($_POST['itemspaid'] AS $item) {
        $paidDate = explode("-",$_POST['paidDate']);
        $paidDate = $paidDate[2]."-".convertDate($paidDate[1])."-".$paidDate[0];
        $sql = "UPDATE `udf_2BB232C0B13C774965EF8558F0FBD615` SET
                `paid` = 'Yes',
                `paymentreceiveddate` = '".mysql_real_escape_string($paidDate)."'
                WHERE `hashid` = '".mysql_real_escape_string($item)."' LIMIT 1;";
        //echo $sql."<br>";
        mysql_query($sql);
    }
    
	
    $_SESSION['itemsToEmail'] = $_POST['itemspaid'];
    
    header("Location: send-payment-received-multi.php");
    exit();
}
?>
<html>
<head>
	<title>Set Bookings Paid</title>
	<?php 
	if (ERES > 1024) { ?>
		<style type="text/css" media="all">@import "css/style.css";</style>
	<?php 
	} else { ?>
		<style type="text/css" media="all">@import "css/style_small.css";</style>
	<?php
	}?>
    <script type='text/javascript' src='js/calendar_pop.js'></script>
    <style type="text/css" media="all">@import "css/calendar_pop.css";</style>		
</head>

<body <?php echo $onload ?>>
<?php $getvalues = "?id=".$revfull; ?>
<form method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?><?php echo $getvalues; ?>" name="activity_manager">
	<table cellspacing="0" cellpadding="0" border="0" width="97%">
	<tr>
		<td><img src="images/<?php echo $_SESSION['franchisedata']['logo'];?>" alt="" / align="center" border="0"></td>
		<td align="right"></td>
	</tr>
	</table><br>
	<table cellspacing="0" cellpadding="1" border="0" width="95%">
	<tr>
		<td height="5" colspan="5"><img src="images/spacer.gif" height="5" width="1"></td>
	</tr>
	<tr>
		<td width="5%" rowspan="2">&nbsp;</td>
		<td valign="top" width="5%">
			<img src="images/icons/24_shadow/calendar.png"><img src="images/spacer.gif" height="1" width="10">
		</td>
		<td valign="top" height="5" width="60%">
			<strong><font style="font-size:120%;font-family:Trebuchet MS">Set Bookings as Paid</font></strong>
		</td>
		<td align="right" valign="top" height="5" >
		<div id="menu_bar">
		<script language="JavaScript">
            
        function submitform() {
            
            var paidItems = document.getElementsByName('itemspaid[]');					   
            var paid = 0;
            for(var i = 0; i < paidItems.length; ++i) {
                if(paidItems[i].checked) {
                   paid = 1;
                }
            }
         
            if (paid == 0) {
                // Do nothing
                alert("Please select at least item to set as paid.");
                return false;
            }
            
			document.activity_manager.submit();
            
		}
		</script>
		<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td><img src="images/menu/menu_01.gif" width="13" height="25" alt=""></td>
				<td><a href="javascript: submitform()"><img src="images/menu/save.gif" width="23" height="25" alt="Save Payments" border="0"></a></td>
				<td><img src="images/menu/menu_08.gif" width="8" height="25" alt=""></td>
				<td><a href="<?php echo $_SERVER['PHP_SELF']?>?id=<?php echo $revfull ?>"><img src="images/refresh.gif" border="0" alt="Refresh this page."></a></td>
				<td><img src="images/menu/menu_11.gif" width="9" height="25" alt=""></td>
				<td><img src="images/menu/menu_13.gif" width="18" height="25" alt=""></td>
			</tr>
		</table>
		</div>
		</td>
		<td rowspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td valign="top" colspan="4">
            <br>
			<table width="100%">
                <tr>
                    <td colspan="6">
                        Set paid date as:
                        <input style="width:105px;" type="text" name="paidDate" id="paidDate" readonly="" value="<?php echo date("d-M-Y");?>">
                        <img src="images/icons/calendar.png" id="setDate" style="cursor: pointer;" title="Date selector">
                        <script language="javascript">Calendar.setup({ inputField:"paidDate",button:"setDate",align:"Ll00", ifFormat:"%d-%b-%Y"  });</script>
                        <br /><br />
                    </td>
                </tr>
                <tr style="text-align:center">
                    <td class="ls_top"><strong>Child</strong></td>
                    <td class="ls_top"><strong>Name</strong></td>
                    <td class="ls_top"><strong>Class Type</strong></td>
                    <td class="ls_top"><strong>Term</strong></td>
                    <td class="ls_top"><strong>Year</strong></td>
                    <td class="ls_top" align="center"><strong>Select<br />Bookings</strong></td>
                </tr>
                <?php
                $sql = $_SESSION['setPaid'];
                $sql .= " AND paid = 'No' AND bookingtype = 'booking';";
                // echo $sql;
                $res = mysql_query($sql);
				while ($row = mysql_fetch_array($res)) {
                    include("includes/ls_sub.php"); ?>
                    <tr>
                        <td class="ls_<?php echo $lsi;?>"><?php echo $row['childsname'];?></td>
                        <td class="ls_<?php echo $lsi;?>"><?php echo $row['classname'];?></td>
                        <td class="ls_<?php echo $lsi;?>"><?php echo $row['classtype'];?></td>
                        <td class="ls_<?php echo $lsi;?>"><?php echo $row['termtime'];?></td>
                        <td class="ls_<?php echo $lsi;?>"><?php echo $row['year'];?></td>
                        <td class="ls_<?php echo $lsi;?>" align="center">
                            <input type="checkbox" name="itemspaid[]" id="itemspaid" value="<?php echo $row['hashid'];?>" />
                        </td>
                    </tr>
                <?php
                } ?>
            </table>
		</td>
	</tr>
	</table>
	<input type="hidden" name="recordid" value="<?php echo $_GET['id'];?>">
	</form>
</body>
</html>
