<?php
include_once("_globalconnect.php");
if(isset($gval[21]) && $gval[21] == '5737034557EF5B8C02C0E46513B98F90'){
    $filename = 'Franchise_list';
    $headers = "Id, Venue name, Address, City town, Zip code, State province, Country, Venue type, School status, Status,";
    $sql = "SELECT venue.id,
                venue.venuename, 
                venue.address, 
                venue.citytown, 
                venue.stateprovincecounty, 
                venue.venuetype,
                venue.schoolstatus,
                venue.status
               FROM  `udf_" . mysql_real_escape_string(strtolower($gval[21])) . "` as venue
               LEFT JOIN `udf_1728efbda81692282ba642aafd57be3a` as ho
               ON venue.department = ho.department
               LEFT JOIN departments as dept
               ON dept.id = ho.department
               WHERE venue.status = 'Approved'
               AND dept.id = ho.department
               AND venue.department = ho.department
               AND venue.issaved = 1";


    $res = mysql_query($sql);
    $check = 1;
    $row_array = array();
    while ($row = mysql_fetch_assoc($res, MYSQL_ASSOC)) {
        array_push($row_array, $row);
        $check++;
    }

    foreach ($row_array as $value) {
        $line = '';
        $value = '"' . $value['id'] . '"' . ","
            . '"' . $value['venuename'] . '"' . ","
            . '"' . $value['address'] . '"' . ","
            . '"' . $value['citytown'] . '"' . ","
            . '"' . $value['zippostcode'] . '"' . ","
            . '"' . $value['stateprovincecounty'] . '"' . ","
            . '"' . $value['country'] . '"' . ","
            . '"' . $value['venuetype'] . '"' . ","
            . '"' . $value['schoolstatus'] . '"' . ","
            . '"' . $value['status'] . '"' . ",";
        $line .= $value;
        $data .= trim($line) . "\n";
    }
    $data = str_replace("\r", "", $data);

}else if(isset($gval[21]) && $gval[21] == '81448138F5F163CCDBA4ACC69819F280'){
    $filename = 'Coach_list';
    $headers = "Id,Coach name,Franchise owner A,Franchise owner B,Franchise area A,Franchise area B,Franchise area C,Franchise area D,Main franchise contact,Primary phone,Primary email,Secondary email,Secondary phone,From text message mobile number,Address line 1,Address line 2,City,Provence county,Country,Postcode,Criminal reference number,Criminal start date,Criminal expiry date,Automated update service,First aid company name,First aid reference number,First aid start date,First aid expiry date,Child protection company name,Child protection reference number, Child protection start date, Child protection expiry date, Non disclosure reference, Non disclosure start date,Coach contract start date,Coach contract expiry date,Status";
    $sql = "SELECT coach.id,
                coach.coachname, 
                coach.franchiseownera, 
                coach.franchiseownerb, 
                coach.franchiseareaa, 
                coach.franchiseareab,
                coach.franchiseareac,
                coach.franchisearead,
                coach.mainfranchisecontact,
                coach.primaryphone,
                coach.primaryemail,
                coach.secondaryemail,
                coach.secondaryphone,
                coach.fromtextmessagemobilenumber,
                coach.addressline1,
                coach.addressline2,
                coach.city,
                coach.provencecounty,
                coach.country,
                coach.postcodezipcode,
                coach.criminalreferencenumber,
                coach.criminalstartdate,
                coach.criminalexpirydate,
                coach.automatedupdateservice,
                coach.firstaidcompanyname,
                coach.firstaidreferencenumber,
                coach.firstaidstartdate,
                coach.firstaidexpirydate,
                coach.childprotectioncompanyname,
                coach.childprotectionreferencenumber,
                coach.childprotectionstartdate,
                coach.childprotectionexpirydate,
                coach.nondisclosurereference,
                coach.nondisclosurestartdate,
                coach.coachcontractstartdate,
                coach.coachcontractexpirydate,
                coach.isactive
               FROM  `udf_" . mysql_real_escape_string(strtolower($gval[21])) . "` as coach
               LEFT JOIN `udf_1728efbda81692282ba642aafd57be3a` as ho
               ON coach.department = ho.department
               LEFT JOIN departments as dept
               ON coach.id = ho.department
               WHERE coach.isactive = 'Yes'
               AND coach.issaved = 1";
    $res = mysql_query($sql);
    $check = 1;
    $row_array = array();
    while ($row = mysql_fetch_assoc($res, MYSQL_ASSOC)) {
        array_push($row_array, $row);
        $check++;
    }
    foreach ($row_array as $value) {
        $line = '';
        $value = '"' . $value['id'] . '"' . ","
            . '"' . trim($value['coachname']) . '"' . ","
            . '"' . trim($value['franchiseownera']) . '"' . ","
            . '"' . trim($value['franchiseownerb']) . '"' . ","
            . '"' . trim($value['franchiseareaa']). '"' . ","
            . '"' . trim($value['franchiseareab']) . '"' . ","
            . '"' . trim($value['franchiseareac']) . '"' . ","
            . '"' . trim($value['franchisearead']) . '"' . ","
            . '"' . trim($value['mainfranchisecontact']) . '"' . ","
            . '"' . trim($value['primaryphone']). '"' . ","
            . '"' . trim($value['primaryemail']) . '"' . ","
            . '"' . trim($value['secondaryemail']) . '"' . ","
            . '"' . trim($value['secondaryphone']) . '"' . ","
            . '"' . trim($value['fromtextmessagemobilenumber']) . '"' . ","
            . '"' . trim($value['addressline1']) . '"' . ","
            . '"' . trim($value['addressline2']) . '"' . ","
            . '"' . trim($value['city']) . '"' . ","
            . '"' . trim($value['provencecounty']) . '"' . ","
            . '"' . trim($value['country']) . '"' . ","
            . '"' . trim($value['postcodezipcode']) . '"' . ","
            . '"' . trim($value['criminalreferencenumber']) . '"' . ","
            . '"' . trim($value['criminalstartdate']) . '"' . ","
            . '"' . trim($value['criminalexpirydate']) . '"' . ","
            . '"' . trim($value['automatedupdateservice']) . '"' . ","
            . '"' . trim($value['firstaidcompanyname']) . '"' . ","
            . '"' . trim($value['firstaidreferencenumber']) . '"' . ","
            . '"' . trim($value['firstaidstartdate']) . '"' . ","
            . '"' . trim($value['firstaidexpirydate']) . '"' . ","
            . '"' . trim($value['childprotectioncompanyname']) . '"' . ","
            . '"' . trim($value['childprotectionreferencenumber']) . '"' . ","
            . '"' . trim($value['childprotectionstartdate']) . '"' . ","
            . '"' . trim($value['childprotectionexpirydate']) . '"' . ","
            . '"' . trim($value['nondisclosurereference']) . '"' . ","
            . '"' . trim($value['nondisclosurestartdate']) . '"' . ","
            . '"' . trim($value['coachcontractstartdate']) . '"' . ","
            . '"' . trim($value['coachcontractexpirydate']) . '"' . ","
            . '"' . trim($value['isactive']) . '"' . ",";
        $line .= $value;
        $data .= trim($line) . "\n";
    }
}
$data = str_replace("\r", "", $data);
if ($data == "") {
    $data = "\n(0) Records Found!\n";
}
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=" . $filename . "_export_" . date("Y-m-d H:i:s") . ".csv");
header("Pragma: no-cache");
header("Expires: 0");
print "$headers\n$data";