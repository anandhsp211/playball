<br style="line-height:7px;">
<script language="JavaScript">
    function submitform() {
        if (document.adddept.department.value == '') {
            alert('You must enter a department name.');
        } else {
            document.adddept.submit();
        }
    }
</script>
<form method="POST" name="adddept" action="settings.php?id=83">
    <table width="100%" cellpadding="5" cellspacing="1" border="0">
        <tr>
            <td colspan="2" width="97%">
                <div id="menu_bar">
                    <table id="Table_01" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="3%" align="left"><img src="images/icons/id_cards.png" border=0></td>
                            <td width="45%" align="left">&nbsp;<strong><font
                                            style="font-size:150%;font-family:Trebuchet MS">Add/Edit
                                        Franchise</font></strong></td>
                            <?php if ($nomoreusersallowed != 1) { ?>
                                <td width="70%">&nbsp;</td>
                                <td><img src="images/menu/menu_01.gif" width="13" height="25" alt=""></td>
                                <td><a href="thinline.php?id=67"><img src="images/menu/new.gif" width="18" height="25"
                                                                      border="0" alt="New User"></a></td>
                                <td><img src="images/menu/edit_g.gif" width="18" height="25" border="0"
                                         alt="Edit Contact"></td>
                                <!--<td><a href="search.php"><img src="images/menu/open.gif" width="24" height="25" border="0" alt="Open Contact"></a></td>-->
                                <td><a href="javascript: submitform()"><img src="images/menu/save.gif" width="23"
                                                                            height="25" alt="Save Contact"
                                                                            border="0"></a></td>
                                <td><img src="images/menu/menu_08.gif" width="8" height="25" alt=""></td>
                                <td><a href="thinline.php?id=<?php echo $fullidvalues ?>"><img src="images/refresh.gif"
                                                                                               border="0"
                                                                                               alt="Refresh this page."></a>
                                </td>
                                <td><img src="images/menu/menu_11.gif" width="9" height="25" alt=""></td>
                                <td><img src="images/menu/menu_13.gif" width="18" height="25" alt=""></td>
                            <?php } else { ?>
                                <td class="messageboard" align="center"><strong>Note:</strong> You have used allocation
                                    of users for this subscription. Please delete a user or contact us to purchase more
                                    users.
                                </td>
                            <?php } ?>
                        </tr>
                    </table>

                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table width="100%" cellpadding="2" cellspacing="2" border="0">
                    <tr>
                        <td valign="top" width="40%" class="ls_top">
                            <table width="100%" border="0" cellpadding="0" cellspacing="3">
                                <tr>
                                    <td width="30%">
                                        <script type='text/javascript' src='js/department_exists.js'></script>
                                        Franchise Name: &nbsp;<font color="red" size="3">*</font><br>
                                        <?php
                                        if ($gval[0] == 85) {
                                            $sql = "SELECT * FROM departments WHERE id = " . $gval[1] . " LIMIT 1";
                                            $resgetvals = mysql_query($sql);
                                            $row = mysql_fetch_array($resgetvals);
                                            $deptcountry = $row['country'];
                                            $deptval = "value='" . $row['department'] . "'";
                                            $psprocesslogintype = 12;
//                                            var_dump($sql);
                                        } else {
                                            $psprocesslogintype = 11;
                                        }
                                        ?>
                                        <input type="text" onkeyup="showHint(this.value,7,1);" name="department"
                                               class="standardfield" style="width:450px;" <?php echo $deptval; ?>><br>
                                        <span style="color:red;" id="txtHint"></span>
                                        <?php
                                        if ($errormsg != NULL) {
                                            echo '<font class="note" style="color:red">' . $errormsg . '</font>';
                                        } ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <br/>Franchise Country:<br/>

                                        <select name="country" class="standardfield">
                                            <option value="">Select One</option>
                                            <?php
                                            foreach ($registeredCountries AS $c) {
                                                $selected = ($c == $deptcountry) ? "selected " : ""; ?>
                                                <option
                                                    <?php echo $selected; ?>value="<?php echo $c; ?>"><?php echo $c; ?></option>
                                                <?php
                                            } ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <br/>
                                        Schools Permitted:<br/>
                                        <input type="text" name="schoolspermitted"
                                               value="<?php echo $row['schools_permitted']; ?>"/> <br/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <br/>
                                        Other Venues Permitted:<br/>
                                        <input type="text" name="otherspermitted"
                                               value="<?php echo $row['other_permitted']; ?>"/><br/>
                                        <font style="font-size:12px;">This applies to the UK only.</font>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input type="hidden" name="processlogintype" value="<?php echo $psprocesslogintype; ?>">
    <input type="hidden" name="departmentid" value="<?php echo $gval[1]; ?>">
</form>