<?php
// Get address details
$sqlVen = "SELECT venuename, address, citytown, stateprovincecounty, country, zippostcode  FROM `udf_5737034557EF5B8C02C0E46513B98F90` WHERE hashid = '".mysql_real_escape_string($gval[1])."' AND issaved = 1 LIMIT 1";
//echo $sqlVen."<br>";
$resven = mysql_query($sqlVen);
$rowven = mysql_fetch_array($resven);

$address = mysql_real_escape_string($rowven['address']).", ";
$address .= mysql_real_escape_string($rowven['citytown']).", ";
$address .= mysql_real_escape_string($rowven['stateprovincecounty']).", ";
$address .= mysql_real_escape_string($rowven['country']).", ";
$address .= mysql_real_escape_string($rowven['zippostcode']);

// Get lat and lon and update
$latlongVenueVals = getLatLongFromPostcode($address);
//echo "Latitude: ".$latlongVenueVals[0]."<br />";
//echo "Longitude: ".$latlongVenueVals[1]."<br />";
$address2 = $address;
$address = str_replace(" ","%20",$address);
$venuename = $rowven['venuename'];
$mapcode = '<iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" width="425" height="350" src="https://maps.google.com/maps?hl=en&q='.$address.'&ie=UTF8&t=roadmap&z=16&iwloc=B&output=embed"><div></div></iframe>';

// Lets change the status to pending approval

// if there are no dups, just approve the venue.  Otherwise send for approval
$isDupped = base64_decode($_GET['dups']);

if ($isDupped == 0) {
	if ($rowven['country'] == "Germany") {
		$applyStatus = "Approved";
	} else {
		$applyStatus = "Pending Approval";	
	}
	//} else {
	//	$applyStatus = "Approved";
	//}
} else {
	$applyStatus = "Pending Approval";
}

$sqlUpdateVen = "UPDATE `udf_5737034557EF5B8C02C0E46513B98F90`
                    SET `status` = '".$applyStatus."',
                    latitude = '".mysql_real_escape_string($latlongVenueVals[0])."',
                    longitude = '".mysql_real_escape_string($latlongVenueVals[1])."',
                    mapcode = '".mysql_real_escape_string($mapcode)."'
                    
                    WHERE `hashid` = '".mysql_real_escape_string($gval[1])."';";
//echo $sqlUpdateVen."<br />";
mysql_query($sqlUpdateVen);

include_once 'email-server-details.php';

// mail to all franchise managers for this country
$franchiseManagers = $_SESSION['franchiseManagers'];
foreach ($franchiseManagers AS $fran) {
	$mail->addAddress($fran);
}
//$mail->addAddress("playballhq@gmail.com");
//$mail->addAddress("patrick@mediatomcat.net");
//$mail->addBCC($_SESSION['foemailaddress']);
$mail->setFrom($rowemail['fromaddress'],$rowemail['fromaddress']);
$mail->addReplyTo($rowemail['replyaddress']);

if ($applyStatus == "Approved") {
	
	$mail->Subject = 'Venue Automatically Approved';
	$msg = "A venue has been automatically approved by the system, because it had no duplicates.<br><br>";
	$msg .= $_SESSION['fullname']."<br />";
	$msg .= $venuename."-".$address2;
	$mail->Body = $msg;
	
} else {

	$mail->Subject = 'Venue approval required';
	$mail->Body = "Please log into the Playball backoffice CRM and validate a newly proposed venue.<br><br> <a href='http://backoffice.playballkids.com/login.php'>Click here to login to Backoffice</a>";
	
}
if (!$mail->Send()) {
	//echo "Mailer Error: " . $mail->ErrorInfo;
} else {
	//echo "Message has been sent";
} ?>