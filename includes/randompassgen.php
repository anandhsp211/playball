<?php
	class G
	{
		var $wc;
		var $w;
		var $l;
		var $minl;
		var $maxl;

		function G($min, $max, $chararray=NULL)
		{
			if($chararray == NULL)
			{
				$this->wc = array();
				for($i=48; $i<58; $i++)
				{
					array_push($this->wc, chr($i)); // 0-9
				}
				for($i=65; $i<91; $i++)
				{
					array_push($this->wc, chr($i)); // A-Z
				}    
				for($i=97; $i<122; $i++)
				{
					array_push($this->wc, chr($i)); // a-z
				}    
				shuffle($this->wc);
			}
			else
			{ $this->wc = $chararray; }

			$this->minl = $min;
			$this->maxl = $max;
		}


		function setl()
		{ $this->l = rand($this->minl, $this->maxl); }

		function setMin($min)
		{ $this->minl = $min; }

		function setMax($max)
		{ $this->maxl = $max; }

		function getw()
		{
			$this->w = NULL; 
			$this->setl();

			for($i=0; $i<$this->l; $i++)
			{
				$charnum = rand(0, count($this->wc));
				$this->w .= $this->wc[$charnum];
			}

			return $this->w; 
		}

		function getwww()
		{
			return (htmlentities($this->getw()));
		}
	}
?>