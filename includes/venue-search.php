<?php
require(dirname(__FILE__) . '/class.damerauLevenshtein.php');
// Get the location details for this record.
$sqlVen = "SELECT venuename, address, country, stateprovincecounty, citytown, zippostcode
              FROM `udf_5737034557EF5B8C02C0E46513B98F90`
              WHERE hashid = '".mysql_real_escape_string($gval[1])."' AND issaved = 1 LIMIT 1";
//echo $sqlVen."<br>";
$resven = mysql_query($sqlVen);
$rowven = mysql_fetch_array($resven);

$sourceAddress = array("country"=>$rowven['country'], "state"=>$rowven['stateprovincecounty'],"zip"=>$rowven['zippostcode'],"town"=>$rowven['citytown'],"address"=>$rowven['address'], "venue"=>$rowven['venuename']);

// Get everything in that country and state
$sql =  "SELECT * FROM `udf_5737034557EF5B8C02C0E46513B98F90`
            WHERE country = '".mysql_real_escape_string($rowven['country'])."'
            AND stateprovincecounty LIKE '%".mysql_real_escape_string($rowven['stateprovincecounty'])."%'
            AND citytown LIKE '%".mysql_real_escape_string($rowven['citytown'])."%'
            AND hashid != '".mysql_real_escape_string($gval[1])."' AND issaved = 1
            AND status = 'Approved'";
//echo $sql."<br />";
$res = mysql_query($sql);
$results = array();
$counter = 0;
while ($row = mysql_fetch_array($res)) {
    $results[$counter]['id'] = $row['id'];
    $results[$counter]['venue'] = $row['venuename'];
    $results[$counter]['address'] = $row['address'];
    $results[$counter]['town'] = $row['citytown'];
    $results[$counter]['state'] = $row['stateprovincecounty'];
    $results[$counter]['country'] = $row['country'];
    $results[$counter]['zip'] = $row['zippostcode'];
    $results[$counter]['levscore'] = -1;
    $counter++;
}

$input = $rowven['venuename'];
$shortest = -1;

$counter = 0;
foreach ($results as $words) {

    // First we check if the address is eaxct match
    if (($sourceAddress['address'] == $words['address']) && ($sourceAddress['town'] == $words['town'])) {
      if ($sourceAddress['venue'] == $words['venue']) {
        $results[$counter]['levscore'] = "Venue &amp; address exact match";
      } else {
        $results[$counter]['levscore'] = "Address and town exact match";
      }
    } else {
      
      // Otherwise do a check on the venue name and report accordingly
      $word = $words['venue'];
      $dl = new DamerauLevenshtein($input, $word, 1,6,6,1);
      $lev = $dl->getSimilarity();
      
      if ($lev >= 40) {
          unset($results[$counter]);
      } else {
          // Add lev score to the array
          $results[$counter]['levscore'] = "Venue name &amp; address similar";
      }
    }
    
    $counter++;
    
}

// Count the number of array entries
$numSimilarVenues = count($results);
// Reset the index
$results = array_values($results);
// Sort by lev score
usort($results, function($a, $b) {
    return $a['levscore'] - $b['levscore'];
});

function returnMatch ($distance) {
    
    if ($distance == 0) {
        $message = "<font color='green'>Exact Match Venue</font>";
    } else {
        $message = $distance;
    }
    return $message;

} ?>

<tr>
  <td colspan="2" style="padding: 10px;">
    <br /><h1 style="font-family:Arial">Checking for duplicate venues ...</h1><br />
    
    <?php
    //echo "<pre>";
    //  print_r($results);
    //echo "</pre>";
    ?>
    
    <table width="100%">
    <tr>
        <td colspan="4">
            <?php
            if ($numSimilarVenues > 0) {
              
                $ifare = ($numSimilarVenues <= 1) ? array("is","venue") : array("are","venues");
                echo "There ".$ifare[0]." <font style='font-weight:bold;font-size:18px;'>".$numSimilarVenues."</font> ".$ifare[1]." that ".$ifare[0]." similar to the venue you are proposing.
                    To ensure you are not proposing a venue that is already in use by another Franchise, please double check the list below.  If you are the first franchise to propose this
                    venue <strong>OR</strong> you have an agreeement to share the venue with another franchise owner, click the <strong>Submit Venue for approval</strong> link at the bottom of the page to start
                    the approval process.<br /><br />";
            }
            ?>
        </td>
    </tr>
    <?php
    $counter = 0;
    foreach ($results As $res) {
        $address = $res['venue'].",".$res['address'].",".$res['town'].",".$res['state'].",".$res['zip'].",".$res['country'];
        echo $address."<br />";
        $latLong = getLatLongFromPostcode($address);
        //print_r($latLong); 
        ?>
        <tr>
            <td>
                <?php
                //echo "Match: ".returnMatch($res['levscore'])."<br />";
                //echo "Latitude: ".$latLong[0]."<br />";
                //echo "Longitude: ".$latLong[0]."<br />"; ?>
            </td>
            <td>
              <br>
              <?php
                echo "<strong>".$res['venue']."</strong><br />";
                echo $res['address'].", ".$res['town'].", ".$res['state'].", ".$res['zip'].", ".$res['country'];
                echo "<br><br>Reason: <font color='red'>".$res['levscore']."</font>"; ?>
                <br /><br />
                <div id="map_<?php echo $counter;?>"></div>
                <br /><br />
            </td>
            <!--<td>
                <textarea style="width:400px;height:150px;"><iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" width="600" height="450" src="https://maps.google.com/maps?hl=en&q=<?php echo $address;?>&ie=UTF8&t=roadmap&z=10&iwloc=B&output=embed"><div></div></iframe></textarea>
            </td>-->
        </tr>
        <style>
        #map_<?php echo $counter;?> {
            height: 250px;
            width: 550px;
            padding:23px;
        }
        </style>
    <?php
        $counter++;
    } ?>
    
    <script>
        var map;
        function initMap() {
            <?php
            $counter = 0;
            foreach ($results AS $res) {
                $address = $res['venue'].",".$res['address'].",".$res['town'].",".$res['state'].",".$res['zip'].",".$res['country'];
                $latLong = getLatLongFromPostcode($address);
                
                ?>
                map = new google.maps.Map(document.getElementById('map_<?php echo $counter;?>'), {
                    center: {lat: <?php echo $latLong[0];?>, lng: <?php echo $latLong[1];?>},
                    zoom: 15
                });
                
                var marker = new google.maps.Marker({
                    position: {lat: <?php echo $latLong[0];?>, lng: <?php echo $latLong[1];?>},
                    map: map,
                    title: '<?php echo $res['venue'];?>'
                });
                
                // Add circle overlay and bind to marker
                var circle = new google.maps.Circle({
                    map: map,
                    radius: 400,    // 10 miles in metres
                    strokeColor: '#FF0000',
                    strokeOpacity: 0.4,
                    strokeWeight: 2,
                    fillColor: '#FF0000',
                    fillOpacity: 0.2
                });
                circle.bindTo('center', marker, 'position');
                
            <?php
                $counter++;
            } ?>
        }
    </script>
    <tr>
        <td colspan="3">
          <?php
          if ($numSimilarVenues < 1) { ?>
            Congratulations! This venue is available as a new Playball franchise venue.<br>If you are ready to submit this venue, please click the "Submit my venue for approval" button below the map. <br /><br />
            <?php
                $sourceAddressStr = str_replace("#","",$sourceAddress['address']).", ".$sourceAddress['town'].", ".$sourceAddress['zip'].", ".$sourceAddress['state'].", ".$sourceAddress['country'];
            ?>
            <iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" width="450" height="350" src="https://maps.google.com/maps?hl=en&q=<?php echo $sourceAddressStr;?>&ie=UTF8&t=roadmap&z=15&iwloc=B&output=embed"><div></div></iframe>
            <br /><br /><a href="<?php echo $actual_link;?>&venueapproval=proceed&dups=<?php echo base64_encode($counter);?>" style="background:#0088CC;padding:5px;color:white">Submit my venue for approval</a><br /><br />
          <?php
          } else { ?>
            Please select from the following:<br><br />
            My venue is not the same as the venue(s) above or is,<br />but will be shared with other franchise owners<br /><br />
            <a href="<?php echo $actual_link;?>&venueapproval=proceed&dups=<?php echo base64_encode($counter);?>" style="background:#0088CC;padding:5px;color:white;">Submit my venue for approval</a><br /><br />
            My venue is listed above and I am not in an agreement<br>to share with another franchise owner:<br /><br />
            <a style="background:red;padding:5px;color:white;" onclick="return confirm('Are you sure you want to delete this record?.')" href="thinline.php?id=1:<?php echo $gval[1] ?>:<?php echo $gval[2].":".$gval[3].":".$gval[4].":".$gval[5].":".$gval[6].":".$gval[7].":".$gval[8].":".$gval[9].":".$gval[10].":".$gval[11].":".$gval[12].":".$gval[13].":".$gval[14].":".$gval[15].":".$gval[16].":".$gval[17].":".$gval[18].":".$gval[19].":".$gval[20].":".$gval[21].":".$gval[22].":".$gval[23].":".$gval[24]; ?>">Delete my request</a>
          <?php
          } ?>
        </td>
    </tr>
    </table>
    <br /><br />
    <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $googleMapsAPIKey;?>&callback=initMap" defer async></script>
  </td>
</tr>