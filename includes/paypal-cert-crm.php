<?php
$MY_KEY_FILE = "/var/www/secure/playball_private_cert.pem";
$MY_CERT_FILE = "/var/www/secure/my-public-certificate.pem";
$PAYPAL_CERT_FILE = "/var/www/secure/paypal_cert.pem";
$OPENSSL = "/usr/bin/openssl";


$form = array('cmd' => '_s-xclick',
            'business' => 'patrick@mediatomcat.net',
            'cert_id' => '4H6KW3QPSQJVW',
            'lc' => 'UK',
            'custom' => $_POST['smscredit'],
            'currency_code' => $currencyCode,
            'no_shipping' => '1',
            'item_name' => 'Payment for '.$_POST['smscredit'].' texts',
            'item_number' => 'Franchise ID:'.$deptval,
            'amount' => $totalcost
	);
    
    /*echo "<pre>";
    print_r($form);
    echo "</pre>";*/

    $encrypted = paypal_encrypt($form);

function paypal_encrypt($hash) {
    global $MY_KEY_FILE;
    global $MY_CERT_FILE;
    global $PAYPAL_CERT_FILE;
    global $OPENSSL;
    
    if (!file_exists($MY_KEY_FILE)) {
        echo "ERROR: MY_KEY_FILE $MY_KEY_FILE not found\n";
    }
    
    if (!file_exists($MY_CERT_FILE)) {
        echo "ERROR: MY_CERT_FILE $MY_CERT_FILE not found\n";
    }
    
    if (!file_exists($PAYPAL_CERT_FILE)) {
        echo "ERROR: PAYPAL_CERT_FILE $PAYPAL_CERT_FILE not found\n";
    }

    //Assign Build Notation for PayPal Support
    $hash['bn']= 'playball-buy-texts.php_EWP2';

    $data = "";
    foreach ($hash as $key => $value) {
        if ($value != "") {
            $data .= "$key=$value\n";
        }
    }

    $openssl_cmd = "($OPENSSL smime -sign -signer $MY_CERT_FILE -inkey $MY_KEY_FILE " .
                    "-outform der -nodetach -binary <<_EOF_\n$data\n_EOF_\n) | " .
                    "$OPENSSL smime -encrypt -des3 -binary -outform pem $PAYPAL_CERT_FILE";
    exec($openssl_cmd, $output, $error);

    if (!$error) {
        return implode("\n",$output);
    } else {
        return "ERROR: encryption failed";
    }
    
} ?>

<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="encrypted" value="<?php echo $encrypted; ?>">