<?php
$vtypes = array("School", "Other","");
// Get their maximums
$msql = "SELECT schools_permitted, other_permitted
            FROM departments
            WHERE isactive = 1
            AND id = ".$deptval." 
            ";
//echo $msql."<br>";

$mres = mysql_query($msql);
$marray = array();

while ($mrow = mysql_fetch_array($mres)) {
    $marray['School'] = $mrow['schools_permitted'];
    $marray['Other'] = $mrow['other_permitted'];
}

/*echo "<pre>";
print_r($marray);
echo "</pre>";*/

// Get their venue numbers
$vsql = "SELECT schoolstatus, COUNT(department) AS numvenues 
            FROM `udf_5737034557EF5B8C02C0E46513B98F90`
            WHERE issaved = 1
            ".$addsqldepartments."
            GROUP BY schoolstatus
            ";
//echo $vsql."<br>";
$vres = mysql_query($vsql);
$varray = array();
while ($vrow = mysql_fetch_array($vres)) {
    $varray[$vrow['schoolstatus']] = $vrow['numvenues'];
}

/*echo "<pre>";
print_r($varray);
echo "</pre>";*/

// lets check their numbers
// If UK x schools and y other
// rest of world is x sum of both
foreach ($vtypes AS $v) {
    if ($_SESSION['franchisecountry'] == "United Kingdom") {
        if ($v == "School") {
            $totalschools = $marray[$v] - $varray[$v];
            //echo $v." - ".$totalschools." remaining<br>";
        } elseif ($v == "Other") {
            $totalother = $marray[$v] - $varray[$v];
            //echo $v." / ".$totalother." remaining<br>";
        } 
    } else {
        // Other
        $totalvenues = $varray[$v] + $totalvenues;
        $totalmax = $marray[$v] + $totalmax;
    }
}

$venuesallowed = 1;
$schoolsallowed = 1;
$othersallowed = 1;

$schoolmessage = "";
$othermessage = "";

if ($_SESSION['franchisecountry'] == "United Kingdom") {
    //echo $totalschools."<br />";
    //echo $totalother."<br />";
    
    if ($totalschools <= 0) {
        $schoolmessage = "Maximum schools reached or exceeded ";
        $schoolsallowed = 0;
    } else {
        $schoolmessage = "You can add <strong>".$totalschools."</strong> additional school(s)";
        $schoolsallowed = 1;
    }
    
    if ($totalother <= 0) {
        $othermessage = " and maximum non-school venues reached or exceeded<br />";
        $othersallowed = 0;
    } else {
        $othermessage = "and <strong>".$totalother."</strong> additional non-school venue(s).";
        $othersallowed = 1;
    }
    
    if (($othersallowed == 0) && ($schoolsallowed == 0)) {
        $venuemessage = "Reached or exceeded maximum venues, please delete a venue to add more.";
        $venuesallowed = 0;
    }
    
} else {
    //echo $totalvenues."<br />";
    //echo $totalmax."<br />";
    
    if ($totalvenues < $totalmax) {
        //echo "Allowed";
        $venuesallowed = 1;
    } else{
        $venuemessage = "Reached or exceeded maximum venues, please delete a venue to add more.";
        $venuesallowed = 0;
    }
    
}

// Have a few mnaster users that have unlimited venues
if (($_SESSION['username'] == "playballadmin") || ($_SESSION['username'] == "pmccarthy")) {
    $venuesallowed = 1;
}


?>