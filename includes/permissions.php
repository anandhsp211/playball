<?php
// when the functioin is called it will look 
function permissions($zone, $isitpop) {
	$tUnixTime = time();
	$sGMTMySqlString = date("Y-m-d H:i:s", $tUnixTime);
	// Permissions zone is SETTINGS
	$sql = "SELECT * FROM permissions_users WHERE zone = '".$zone."' AND userid=".UID;
	//echo $query;
	$res = mysql_query($sql);
	$perfield = array();
	$perag = array();
	// Lets build the array
	while ($row = mysql_fetch_array($res)) {
		array_push($perfield, $row['field']);
		array_push($perag,$row['accessgranted']);
	}
	// Combine the array to ensure they are allowed to enter here.
	$permissions = array_combine($perfield, $perag);
	
	if (($permissions['thezone'] != 1) && ($isitpop != 1)) {
		if ($_SESSION['loggedin'] != 1) {
			header("Location: login.php");
		} else {
			///  Audit trail = action / table / item id, datestamp
			audittrail("access denied", $zone, "THEZONE", $sGMTMySqlString);
			header("Location: accessdenied.php");
		}
	} elseif ((($permissions['thezone'] != 1) && ($isitpop == 1) && ($permissions['view'] == NULL)) || (($permissions['thezone'] == NULL) && ($isitpop == 1) && ($permissions['view'] != 1))) {
		// This is a popup and we need to close it.
		 return $onload = 'onload="window.parent.location = window.parent.location;self.close();return false;"';
	} else {
		// Ok, they are allowed to view the page, lets pass the array for other permissions
		return $permissions;
	}
}

function permissions_multi($templatename, $zonemulti) {
	if ($templatename == "") {
		$gettemplate = "SELECT templatename FROM permissions_usertmp WHERE userid=".UID;
		//echo $gettemplate;
		$restermplate = mysql_query($gettemplate);
		$rowtemplate = mysql_fetch_array($restermplate) or die ("Hello");
		$templatename = md5($rowtemplate['templatename']);
	}
	$sql = "SELECT * FROM permissions_templates WHERE MD5(templatename) = LOWER('".$templatename."') AND zone = '".$zonemulti."' AND registrantid=".RID;
	$res = mysql_query($sql);
	
	$prm_field = array();
	$prm_zone = array();
	while ($row = mysql_fetch_array($res)) {
		array_push($prm_field, $row['field']);
		array_push($prm_zone, $row['zone']);
	}
	$permissionsmulti = array_combine($prm_field, $prm_zone);
	//print_r($permissionsmulti);
	return $permissionsmulti;
}

function accessdenied($zone,$thefield) {
	///  Audit trail = action / table / item id, datestamp
	$tUnixTime = time();
	$sGMTMySqlString = date("Y-m-d H:i:s", $tUnixTime);
	audittrail("access denied", $zone, $thefield, $sGMTMySqlString);
	header("Location: accessdenied.php");
}
?>