<?php
   	$conn = mysql_connect($serverid,$dbusername,$dbpassword);
	mysql_select_db($databasename);
   	$db = new buildNav;
   	$db->firstset = "id=".$gval[0].":".$gval[1].":".$gval[2].":".$gval[3].":".$gval[4].":".$gval[5].":".$gval[6].":".$gval[7].":".$gval[8].":";
	$db->secondset = ":".$gval[10].":".$gval[11].":".$gval[12].":".$gval[13].":".$gval[14].":".$gval[15].":".$gval[16].":".$gval[17].":".$gval[18].":".$gval[19].":".$gval[20].":".$gval[21].":".$gval[22].":".$gval[23].":".$gval[24].":".$gval[25].":".$gval[26];
	$db->gval9 = $gval[9];
	$db->iszero = 0;
	$db->number_type = 'number';
   	$db->limit = $size_of_group;
   	$db->execute($query_pages);
   	$pages = $db->show_num_pages('&laquo;','&laquo; prev','&raquo;','next &raquo;','','class=moi');   // show pages
   // OUTPUT THE NAV
   print "<div class=\"pages\">";
   print "".$pages;
   $info = $db->show_info();
   print "</div>";
   print "<font class=\"note\">&nbsp; &nbsp; ".$info."</font>";
   
    class buildNav // [Class : Controls all Functions for Prev/Next Nav Generation]
    {
        var $limit, $execute, $query;
        function execute($query) // [Function : mySQL Query Execution]
        {
            !isset($this->gval9) ? $GLOBALS[$this->offset] = 0 : $GLOBALS[$this->offset] = $this->gval9;
            $this->sql_result = mysql_query($query);
            $this->total_result = mysql_num_rows($this->sql_result);
            if(isset($this->limit))
            {
                $query .= " LIMIT " . $GLOBALS[$this->offset] . ", $this->limit";
                $this->sql_result = mysql_query($query);
                $this->num_pages = ceil($this->total_result/$this->limit);
            }
        }

        function show_num_pages($frew = '', $rew = '', $ffwd = '', $fwd = '', $separator = '|', $objClass = '') // [Function : Generates Prev/Next Links]
        {
            $current_pg = $GLOBALS[$this->offset]/$this->limit+1;
            if ($current_pg > 5)
            {
                $fgp = $current_pg - 5 > 0 ? $current_pg - 5 : 1;
                $egp = $current_pg+4;
                if ($egp > $this->num_pages)
                {
                    $egp = $this->num_pages;
                    $fgp = $this->num_pages - 9 > 0 ? $this->num_pages  - 9 : 1;
                }
            }
            else {
                $fgp = 1;
                $egp = $this->num_pages >= 10 ? 10 : $this->num_pages;
            }

            if($this->num_pages > 1) {
                /* searching for http_get_vars
                foreach ($GLOBALS[HTTP_GET_VARS] as $_get_name => $_get_value) {
                    if ($_get_name != $this->offset) {
                        $this->_get_vars .= "&$_get_name=$_get_value";
                    }
                } */
                $this->successivo = $GLOBALS[$this->offset] + $this->limit;
                $this->precedente = $GLOBALS[$this->offset] - $this->limit;
                $this->theClass = $objClass;
                if (!empty($rew)) {
                    $return .= ($GLOBALS[$this->offset] > 0) ? 
						"<a href=\"$_SERVER[PHP_SELF]?$this->firstset$this->iszero$this->secondset$this->_get_vars\" $this->theClass>$frew</a> <a href=\"$GLOBALS[PHP_SELF]?$this->firstset$this->precedente$this->secondset$this->_get_vars\" $this->theClass>$rew</a> $separator " : "<span class=\"nextprev\">$rew</span> $separator ";
                }

                // showing pages
                if ($this->show_pages_number || !isset($this->show_pages_number))
                {
                    for($this->a = $fgp; $this->a <= $egp; $this->a++)
                    {
                        $this->theNext = ($this->a-1)*$this->limit;
                        $_ss_k = floor($this->theNext/26);
                        if ($this->theNext != $GLOBALS[$this->offset])
                        {
                            $return .= " <a href=\"$GLOBALS[PHP_SELF]?$this->firstset$this->theNext$this->secondset$this->_get_vars\" $this->theClass> ";
                            if ($this->number_type == 'alpha')
                            {
                                 if($_ss_k>0)
                                 {
                                    $theLink = chr(64 + ($_ss_k));
                                    for($b = 0; $b < $_ss_k; $b++)
                                    {
                                       $theLink .= chr(64 + ($this->theNext%26)+1);
                                    }
                                    $return .= $theLink;
                                 } else {
                                 $return .= chr(64 + ($this->a));
                                 }
                            } else {
                                $return .= $this->a;
                            }
                            $return .= "</a> ";
                        } else {
                            if ($this->number_type == 'alpha')
                            {
                                 if($_ss_k>0)
                                 {
                                    $theLink = chr(64 + ($_ss_k));
                                    for($b = 0; $b < $_ss_k; $b++)
                                    {
                                       $theLink .= chr(64 + ($this->theNext%26)+1);
                                    }
                                    $return .= $theLink;
                                 } else {
                                 $return .= chr(64 + ($this->a));
                                 }
                            } else {
                                $return .= "<span class=\"current\">".$this->a."</span>";
                            }
                            $return .= ($this->a < $this->num_pages) ? " $separator " : " ";
                        }
                    }
                    $this->theNext = $GLOBALS[$this->offset] + $this->limit;
                    if (!empty($fwd)) {
                        $offset_end = ($this->num_pages-1)*$this->limit;
                        $return .= ($GLOBALS[$this->offset] + $this->limit < $this->total_result) ? "$separator <a href=\"$GLOBALS[PHP_SELF]?$this->firstset$this->successivo$this->secondset$this->_get_vars\" $this->theClass>$fwd</a> <a href=\"$GLOBALS[PHP_SELF]?$this->firstset$offset_end$this->secondset$this->_get_vars\" $this->theClass>$ffwd</a>" : "$separator <span class=\"nextprev\">$fwd</span>";
                    }
                }
            }
            return $return;
        }

        function show_info() // [Function : Showing the Information for the Offset]
        {
           if($GLOBALS[$this->offset] >= $this->total_result || $GLOBALS[$this->offset] < 0) return false;
            $return .= "<br />&nbsp;&nbsp;&nbsp;<strong>".$this->total_result . "</strong> Total Results - ";
            $_from = $GLOBALS[$this->offset] + 1;
            $GLOBALS[$this->offset] + $this->limit >= $this->total_result ? $_to = $this->total_result : $_to = $GLOBALS[$this->offset] + $this->limit;
            $return .= "Showing Results from <strong>" . $_from . "</strong> to <strong>" . $_to . "</strong><br>";
            return $return;
        }
    }
?>