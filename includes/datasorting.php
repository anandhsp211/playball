<?php 
	if (($getidvalues[3] == 2) || ($getidvalues[3] == 4)) {
		$f1database = "contacts_individuals";
		$finerfiltering = "displayingas";
		$actionrequired = 7;
		$actiontable = 2;
		$deleteid = 30;
		$deletemarker = 2;
		
		if ($getidvalues[12] == NULL) {
			$f1orderby = "displayingas";
			$whichway = "ASC";
		} elseif ($getidvalues[12] == 11) {
			$f1orderby = "displayingas";
			$whichway = "ASC";
		} elseif ($getidvalues[12] == 12) {
			$f1orderby = "displayingas";
			$whichway = "DESC";
		} elseif ($getidvalues[12] == 13) {
			$f1orderby = "emailaddress";
			$whichway = "ASC";
		} elseif ($getidvalues[12] == 14) {
			$f1orderby = "emailaddress";
			$whichway = "DESC";
		} elseif ($getidvalues[12] == 15) {
			$f1orderby = "telephonenumber";
			$whichway = "ASC";
		} elseif ($getidvalues[12] == 16) {
			$f1orderby = "telephonenumber";
			$whichway = "DESC";
		} elseif ($getidvalues[12] == 17) {
			$f1orderby = "mobilenumber";
			$whichway = "ASC";
		} elseif ($getidvalues[12] == 18) {
			$f1orderby = "mobilenumber";
			$whichway = "DESC";
		} elseif ($getidvalues[12] == 19) {
			$f1orderby = "firstname";
			$whichway = "ASC";
		} elseif ($getidvalues[12] == 20) {
			$f1orderby = "firstname";
			$whichway = "DESC";
		} else {
			$f1orderby = "displayingas";
			$whichway = "ASC";
		}
		
	} else {
		$actionrequired = 6;
		$actiontable = 1;
		$deleteid = 29;
		$deletemarker = 1;
		$f1database = "contacts_company";
		$finerfiltering = "companyname";
		
		if ($getidvalues[12] == NULL) {
			$f1orderby = "companyname";
			$whichway = "ASC";
		} elseif ($getidvalues[12] == 1) {
			$f1orderby = "companyname";
			$whichway = "ASC";
		} elseif ($getidvalues[12] == 2) {
			$f1orderby = "companyname";
			$whichway = "DESC";
		} elseif ($getidvalues[12] == 3) {
			$f1orderby = "emailaddress";
			$whichway = "ASC";
		} elseif ($getidvalues[12] == 4) {
			$f1orderby = "emailaddress";
			$whichway = "DESC";
		} elseif ($getidvalues[12] == 5) {
			$f1orderby = "telephonenumber";
			$whichway = "ASC";
		} elseif ($getidvalues[12] == 6) {
			$f1orderby = "telephonenumber";
			$whichway = "DESC";
		} elseif ($getidvalues[12] == 7) {
			$f1orderby = "mobilenumber";
			$whichway = "ASC";
		} elseif ($getidvalues[12] == 8) {
			$f1orderby = "mobilenumber";
			$whichway = "DESC";
		} elseif ($getidvalues[12] == 9) {
			$f1orderby = "firstname";
			$whichway = "ASC";
		} elseif ($getidvalues[12] == 10) {
			$f1orderby = "firstname";
			$whichway = "DESC";
		} else {
			$f1orderby = "companyname";
			$whichway = "ASC";
		}
	}
?>