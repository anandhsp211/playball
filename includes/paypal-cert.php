<?php
$MY_KEY_FILE = "/var/www/thinline/playball-prvkey.pem";
$MY_CERT_FILE = "/var/www/thinline/playball-pubcert.pem";
$PAYPAL_CERT_FILE = "/var/www/thinline/paypal_cert.pem";
$OPENSSL = "/usr/bin/openssl";

//echo "Currency: ".$currency."<br>";
if (($currency == "�") || ($currency == "&#8364;")) {
    $currencycode = "EUR";
} elseif (($currency == "�") || ($currency == "&#163;")) {
    $currencycode = "GBP";
} elseif (($currency == "$") || ($currency == "&#36;")) {
    $currencycode = "USD";
} else {
    $currencycode = "GBP";
}
//echo "Currency Code: ".$currencycode;

foreach ($_POST['childid'] AS $childidval) {
    $childidval = explode("||",$childidval);
    $dataarray .= $childidval[0]."||";
}

$dataarray = substr($dataarray, 0, -2);

//echo $_SESSION['paypalemailaddress'];
//echo $_SESSION['certificateid'];
//echo number_format($totalfee,2);
$form = array('cmd' => '_xclick',
            'business' => $_SESSION['paypalemailaddress'],
            'cert_id' => $_SESSION['certificateid'],
            'lc' => 'UK',
            'custom' => $dataarray,
            'currency_code' => $currencycode,
            'no_shipping' => '1',
            'item_name' => 'Fees for Class:'.mysql_escape_string($venue),
            'item_number' => mysql_escape_string($_GET['classid']),
            'amount' => mysql_escape_string(number_format($totalfee,2))
	);
/*echo "<pre>";
print_r($form);
echo "</pre>";*/
$encrypted = paypal_encrypt($form);

function paypal_encrypt($hash) {
    global $MY_KEY_FILE;
    global $MY_CERT_FILE;
    global $PAYPAL_CERT_FILE;
    global $OPENSSL;

    if (!file_exists($MY_KEY_FILE)) {
        echo "ERROR: MY_KEY_FILE $MY_KEY_FILE not found\n";
    }

    if (!file_exists($MY_CERT_FILE)) {
        echo "ERROR: MY_CERT_FILE $MY_CERT_FILE not found\n";
    }

    if (!file_exists($PAYPAL_CERT_FILE)) {
        echo "ERROR: PAYPAL_CERT_FILE $PAYPAL_CERT_FILE not found\n";
    }

    //Assign Build Notation for PayPal Support
    $hash['bn']= 'playball-bookings.php_EWP2';

    $data = "";
    foreach ($hash as $key => $value) {
        if ($value != "") {
            $data .= "$key=$value\n";
        }
    }

    $openssl_cmd = "($OPENSSL smime -sign -signer $MY_CERT_FILE -inkey $MY_KEY_FILE " .
                    "-outform der -nodetach -binary <<_EOF_\n$data\n_EOF_\n) | " .
                    "$OPENSSL smime -encrypt -des3 -binary -outform pem $PAYPAL_CERT_FILE";
    exec($openssl_cmd, $output, $error);

    if (!$error) {
        return implode("\n",$output);
    } else {
        return "ERROR: encryption failed";
    }

} ?>

<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="encrypted" value="<?php echo $encrypted; ?>">
