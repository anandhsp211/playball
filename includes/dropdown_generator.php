<?php 
function generateSelect($toplabel,$sname,$currentvalue,$bigdaddy,$sitearea) { ?>
<div class="edittext">
	<span class="left">
	<?php 
	/// Lets see if they want this as a compulsory field.
	$sqliscomp = "SELECT iscompulsory FROM custom_settings WHERE registrantid = ".RID." AND fieldname = '".$sname."' AND fieldvalue = 'silverIsCompulsory' ";
	$resiscomp = mysql_query($sqliscomp);
	$rowsiscomp = mysql_fetch_array($resiscomp);
	if ($rowsiscomp['iscompulsory'] == 1) {
		$redstar = '<font color="red" size="3">*</font>';
	}
	if ($bigdaddy != 1) {?>
	<?php echo $toplabel ?>: <?php echo $redstar; ?><br>
	<?php 
	}
	// Pull and sort the output of data
	$sqlsbox = "SELECT fieldvalue,isdefault FROM custom_settings WHERE registrantid = ".RID." AND fieldname = '".$sname."' AND fieldvalue != 'silverIsCompulsory' ORDER BY fieldvalue ASC";
	//echo $sqlsbox;
	$ressbox = mysql_query($sqlsbox);?>
	<select onchange="javascript:DataChange();" name="<?php echo $sname; ?>" class="standardselect">
	<?php 
	while ($rowsbox = mysql_fetch_array($ressbox)) { 
		$fieldvalue = $rowsbox['fieldvalue'];
		if (strtolower($rowsbox['fieldvalue']) == strtolower($currentvalue)) { 
			$sboxselect = "selected"; 
		} elseif (($rowsbox['isdefault'] == 1) && ($currentvalue == NULL)) {
			$sboxselect = "selected"; 
		}
		  /// Make sure select one does not store a value
		  if (strtolower($fieldvalue) == "select one") {
		  	$fieldvalueasvalue = "";
		  } else {
		  	$fieldvalueasvalue = $fieldvalue;
		  }
		?>
		<option <?php echo $sboxselect; ?> value="<?php echo $fieldvalueasvalue ?>"><?php echo $fieldvalue ?></option>
	<?php
		unset($sboxselect);
	} ?>
	</select>
	</span>
	<?php 
	if ($bigdaddy != 1) {?>
		<span class="right"><a href="edit_manager.php?editting=<?php echo $sname; ?>&sarea=<?php echo $sitearea; ?>" <?php echo LFD ?>>edit</a> | <a href="edit_manager.php?help=<?php echo $sname; ?>" <?php echo LFD ?>>help</a></span>
		</div>
	<?php 
	}
}


function generateUDFselect($toplabel,$sname,$currentvalue,$fieldname,$object_master_id,$revfull,$ispartner,$canEditValues, $duplicator,$locationhashid,$recordhashid,$labelmsg) { 
	$gval = explode(":",$revfull);
	$gval[21] = ($locationhashid != "") ? $locationhashid : $gval[21];
	$gval[1] = ($recordhashid != "") ? $recordhashid : $gval[21];
	?>
	<div class="edittext">
	<span class="left">
	<?php
	
	/// Lets see if they want this as a compulsory field.
	$sqliscomp = "SELECT iscompulsory, locked, sharedvalues FROM udf_definitions WHERE registrantid = ".RID." AND hashid = '".$sname."'";
	//echo $sqliscomp;
	// echo $gval[21];
	$resiscomp = mysql_query($sqliscomp);
	$rowsiscomp = mysql_fetch_array($resiscomp);
	$islocked = $rowsiscomp['locked'];
	$sharedvalues = $rowsiscomp['sharedvalues'];
	//echo $islocked."<---";
	if ($rowsiscomp['iscompulsory'] == 1) {
		$redstar = '<font color="red" size="3">*</font>';
	} ?>
	<?php echo ($duplicator != 1) ? ucwords($toplabel).": ".$redstar."<br>" : "" ; ?>
	<?php 
	// Lets check if this is a slave object
	if ($object_master_id != NULL) {
		$sname = $object_master_id;
	}
	
	// Add departmental security
	if (($_SESSION['securityarrdept'][0] != "") && ($sharedvalues != 1)) {
		$deptcounter = 0;
		$addsqldepartments = '';
		foreach ($_SESSION['securityarrdept'] AS $deptval) {
			if ($deptcounter == 0) {
				$deptoperator = " AND ";
			} else {
				$deptoperator = " OR ";
			}
			$addsqldepartments .= $deptoperator." FIND_IN_SET('".$deptval."', department)";
			$deptcounter++;
		}
	}
	
	// Pull and sort the output of data
	$sqlsbox = "SELECT * FROM udf_multi_value WHERE udfdefid = '".$sname."'  ORDER BY value ASC";
	//echo $sqlsbox;
	$ressbox = mysql_query($sqlsbox) or die ("Not workingdsfsdf");
	//echo FULLNAME;
	
	if ($canEditValues == "disabled") {
		$fieldname = "";
	} ?>
	<select <?php echo $canEditValues;?> onchange="javascript:DataChange(2,this.value,'<?php echo $gval[21] ?>','<?php echo $gval[1] ?>','<?php echo columnnames($toplabel);?>');" id="<?php echo $fieldname; ?>" name="<?php echo $fieldname; ?>" class="standardselect">
	<?php
	$ccheck = 1;
	while ($rowsbox = mysql_fetch_array($ressbox)) {
		// Validate if this user has set a default
		if ((array_key_exists($rowsbox['udfdefid'], $_SESSION['udfdefaults'])) && ($ccheck == 1)) {
			$defaultval = $_SESSION['udfdefaults'][$rowsbox['udfdefid']];
			echo $defaultval;
		}
		
		$fieldvalue = $rowsbox['value'];
		
		if (strtolower($rowsbox['value']) == strtolower($currentvalue) && ($triggertype == 0)) { 
			$sboxselect = "selected";
			$lockedCurrentvalue = $rowsbox['value'];
		} elseif ((($rowsbox['isdefault'] == 1) && ($currentvalue == NULL) && ($triggertype == 0)) && (!isset($defaultval))) {
			$sboxselect = "selected";
			$lockedCurrentvalue = $rowsbox['value'];
		} elseif (($defaultval == $rowsbox['id']) && ($currentvalue == NULL)) {
			$sboxselect = "selected";
			$lockedCurrentvalue = $rowsbox['value'];
		} elseif ($triggertype == 1) {
			if (($currentvalue == NULL) && ($fieldvalue == FULLNAME)) {
				$sboxselect = "selected"; 
				echo $currentvalue;
				$lockedCurrentvalue = $rowsbox['value'];
			} elseif (($currentvalue != NULL) && ($currentvalue == $fieldvalue)) {
				$sboxselect = "selected"; 
				echo $currentvalue;
				$lockedCurrentvalue = $rowsbox['value'];
			}
		}
		
		
		
		/// Make sure select one does not store a value
		if (strtolower($fieldvalue) == "select one") {
		      $fieldvalueasvalue = "";
		} else {
		      $fieldvalueasvalue = $fieldvalue;
		} ?>
		
		<option <?php echo $sboxselect; ?> value="<?php echo $fieldvalueasvalue ?>"><?php echo $fieldvalue ?></option>
	<?php
		unset($sboxselect, $triggertype);
		$ccheck = 2;
	} ?>
	</select>
	<?php echo ($labelmsg != "") ? "<br style='line-height:5px;'><font style='font-size:11px;'>".$labelmsg."</font>" : ""; ?>
	</span>
	<?php
	
	if ($canEditValues == "disabled") {
		//echo "<input type='hidden' name='".$fieldname."' value='".$lockedCurrentvalue."' />";
	}
	
	if (($toplabel != "Charge for Each") && ($islocked != 1))  { ?>
	<span class="right"><a href="udf_edit_manager.php?editting=<?php echo $sname; ?>" <?php echo LFD ?>>edit</a></span>
	<?php 
	}
	
	?>
	</div>
	<?php
	
} 

function generateSearchselect($udflocation,$itemlabel,$fieldname,$currentval,$resutlstring) { 
	$checklistx = "SELECT searchresults FROM reports_results_arrays WHERE sessionid = '".session_id()."'";
	//echo $checklistx;
	$reslistx = mysql_query($checklistx);
	$rowlistx = mysql_fetch_array($reslistx);
	$resutlstring = $rowlistx['searchresults'];
	//echo $resutlstring;
	$resutlstr = explode("&&", $resutlstring);
	foreach ($resutlstr as $reskey) {
		$resutlstr2 = explode("||", $reskey);
		if (in_array(columnnames($key[2]), $resutlstr2)) {
			break;
		} 
	}
	$txtfieldval = $resutlstr2[1];
	// Lets get the udfhashid
	$sqlsbox = "SELECT hashid, object_master_id, sharedvalues  FROM udf_definitions WHERE location = '".$udflocation."' AND LOWER(label) = '".strtolower($itemlabel)."' LIMIT 1";
	//echo $sqlsbox;
	$ressbox = mysql_query($sqlsbox) or die ("Not working edfsfsd");
	$rowsbox = mysql_fetch_array($ressbox);
	$sname = $rowsbox['hashid'];
	$sharedvalues = $rowsbox['sharedvalues'];
	
	if ($rowsbox['object_master_id'] != NULL) {
		$sname = $rowsbox['object_master_id'];
	}
	
	// Add departmental security
	if (($_SESSION['securityarrdept'][0] != "") && ($sharedvalues != 1)) {
		$deptcounter = 0;
		$addsqldepartments = '';
		foreach ($_SESSION['securityarrdept'] AS $deptval) {
			if ($deptcounter == 0) {
				$deptoperator = " AND ";
			} else {
				$deptoperator = " OR ";
			}
			$addsqldepartments .= $deptoperator." FIND_IN_SET('".$deptval."', department)";
			$deptcounter++;
		}
	}
	
	// Pull and sort the output of data
	if ( ($udflocation == "2B8A61594B1F4C4DB0902A8A395CED93") && ($fieldname == "year") ) {
		 ?>
		<select style="width:95%;height:47px" name="<?php echo $fieldname; ?>[]" multiple class="microselect">
			<?php
			$lstartyear = 2014;
			$lendyear = (date("Y")+1);
			for ($fieldvalue = $lendyear;$fieldvalue >= $lstartyear;$fieldvalue--) {
				$sboxselect = ($fieldvalue == $_POST[$fieldname][0]) ? "selected" : ""; ?>
				<option <?php echo $sboxselect; ?> value="<?php echo $fieldvalue ?>"><?php echo $fieldvalue ?></option>
			<?php
			} ?>
		</select>
	<?php
	} else {
		$sqlsbox = "SELECT * FROM udf_multi_value WHERE udfdefid = '".$sname."' ".$addsqldepartments." ORDER BY value ASC";
		//echo $sqlsbox;
		$ressbox = mysql_query($sqlsbox) or die ("Not working fdgdf"); 
		//print_r($resutlstring); ?>
		<select style="width:95%;height:47px" name="<?php echo $fieldname; ?>[]" multiple class="microselect">
		<?php 
		$hundred = 100;
		while ($rowsbox = mysql_fetch_array($ressbox)) { 
			$fieldvalue = $rowsbox['value'];
			if (isset($_POST[$fieldname])) { 
				foreach ($_POST[$fieldname] as $postedvals) {
					if ($fieldvalue == $postedvals) {
						$sboxselect = "selected";
						break;
					}
				}
			} else {
				// So there is no post value lets check if we are storing it in the 
				// stored search results
				$resutlstr = explode("&&", $resutlstring);
				foreach ($resutlstr as $reskey) {
					$resutlstr2 = explode("||", $reskey);
					if (in_array($fieldname, $resutlstr2, true)) {
						$resultskey = explode("/",$resutlstr2[1]);
						foreach ($resultskey as $resultskeyval) {
							if ($fieldvalue == $resultskeyval) {
								$sboxselect = "selected";
								break;
							}
						}
						
					} 
				}
				$txtfieldval = $resutlstr2[1];
			} 
			/// Make sure select one does not store a value
			if (strtolower($fieldvalue) != "select one") { ?>
			<option <?php echo $sboxselect; ?> value="<?php echo $fieldvalue ?>"><?php echo $fieldvalue ?></option>
		<?php
			}
			unset($sboxselect);
		} ?>
	</select><br>
	<?php
	}
} 

function generateProductSelect($udflocation,$itemlabel,$fieldname,$currentval,$resutlstring,$complevel,$lineitemid,$idcounter) { 
	// Lets get the udfhashid
	$sqlsbox = "SELECT hashid, object_master_id  FROM udf_definitions WHERE location = '".$udflocation."' AND LOWER(label) = '".strtolower($itemlabel)."' LIMIT 1";
	//echo $sqlsbox;
	$ressbox = mysql_query($sqlsbox) or die ("Not working fegdgd");
	$rowsbox = mysql_fetch_array($ressbox);
	$sname = $rowsbox['hashid'];	
	
	if ($rowsbox['object_master_id'] != NULL) {
		$sname = $rowsbox['object_master_id'];
	}
	
	// Pull and sort the output of data
	$sqlsbox = "SELECT * FROM udf_multi_value WHERE udfdefid = '".$sname."' ORDER BY value ASC";
	//echo $sqlsbox;
	$ressbox = mysql_query($sqlsbox) or die ("Not working dffsdf");?>
	<select onchange="updateproduct(this.value,'<?php echo $gval[1] ?>','<?php echo $complevel ?>','<?php echo $lineitemid ?>','<?php echo $fieldname; ?>','<?php echo $idcounter ?>');" style="font-family:Tahoma;font-size:11px" name="<?php echo $fieldname; ?>">
	<?php 
	$hundred = 100;
	while ($rowsbox = mysql_fetch_array($ressbox)) { 
		if ((($currentval == "") || ($currentval == NULL)) && ($rowsbox['value'] == "Select One")) {
			$sboxselect = "selected";
		} elseif ($rowsbox['value'] == $currentval) {
			$sboxselect = "selected";
		} 
		/// Make sure select one does not store a value
		if (strtolower($fieldvalue) != "select one") { ?>
		<option <?php echo $sboxselect; ?> value="<?php echo $rowsbox['value']; ?>"><?php echo $rowsbox['value']; ?></option>
	<?php
		}
		unset($sboxselect);
	} ?>
	</select>
	<?php 
} ?>