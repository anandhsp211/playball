<?php 
// Lets get the start date and end date values
if (isset($_POST[columnnames($value)."_0"])) {
	$dateoutput0 = $_POST[columnnames($value)."_0"];
	$dateoutput1 = $_POST[columnnames($value)."_1"];
} else {
	// So there is no post value lets check if we are storing it in the 
	// stored search results
	$resutlstr = explode("&&", $resutlstring);
	$loopy = 1;
	foreach ($resutlstr as $reskey) {
		$resutlstr2 = explode("||", $reskey);
		if (in_array(columnnames($value), $resutlstr2, true)) {
			$resultskey = explode("/",$resutlstr2[1]);
			foreach ($resultskey as $resultskeyval) {
				if ($loopy == 1) {
					$datstarter = explode("-",substr($resultskeyval,0,10));
					$datstarter = $datstarter[2]."-".convertDate($datstarter[1])."-".$datstarter[0];
					$timestarter = substr($resultskeyval,-9,6);
					$dateoutput0 = $datstarter." ".$timestarter;
				} else {
					$datender = explode("-",substr($resultskeyval,0,10));
					$datender = $datender[2]."-".convertDate($datender[1])."-".$datender[0];
					$timeender = substr($resultskeyval,-9,6);
					$dateoutput1 = $datender."".$timeender;
				}
				$loopy++;
			}
		} 
	}
	$txtfieldval = $resutlstr2[1];
	unset($loopy);
}
 ?>
<script language="Javascript">
	function calcEndDate(cal) {
		var startdate = document.getElementById("<?php echo columnnames($value); ?>_0");
		var hiddenstart = document.getElementById("hiddenstart");
		startdate.value = hiddenstart.value;
		
		var date = cal.date;
		var time = date.getTime();
		
		var hiddenend = document.getElementById("hiddenend");
		var date2 = new Date(time);
		hiddenend.value = date2.print("%d-%b-%Y 23:59");
		var enddate = document.getElementById("<?php echo columnnames($value); ?>_1");
		enddate.value = hiddenend.value;
		
		return true;
	}
	
	function calcUpdateEndDate() {
		var enddate = document.getElementById("<?php echo columnnames($value); ?>_1");
		var hiddenend = document.getElementById("hiddenend");
		enddate.value = hiddenend.value;
	}
</script>
<table cellpadding="0" cellspacing="3" border="0">
<tr>
	<td><input type="text" class="standardfield_date_srch" name="<?php echo columnnames($value); ?>_0" id="<?php echo columnnames($value); ?>_0" value="<?php echo $dateoutput0 ?>"></td>
	<td><img src="images/icons/calendar.png" id="<?php echo columnnames($value); ?>0" style="cursor: pointer;" title="Date selector"></td>
	<td><input type="hidden" class="standardfield_date_srch" name="hiddenstart" id="hiddenstart" value="<?php echo date("d-M-Y",time()) ?> 00:00"> 
		<script language="javascript">Calendar.setup({ inputField:"hiddenstart",button:"<?php echo columnnames($value); ?>0",align:"Ll00", ifFormat:"%d-%b-%Y %H:%M", showsTime: true, onUpdate : calcEndDate});</script>
	</td>
</tr>
<tr>
		<td><input type="text" class="standardfield_date_srch" name="<?php echo columnnames($value); ?>_1" id="<?php echo columnnames($value); ?>_1" value="<?php echo $dateoutput1 ?>"></td>
		<td><img src="images/icons/calendar.png" id="<?php echo columnnames($value); ?>1" style="cursor: pointer;" title="Date selector" >
		<td><input type="hidden" class="standardfield_date_srch" name="hiddenend" id="hiddenend"> 
		<script language="javascript">Calendar.setup({ inputField:"hiddenend",button:"<?php echo columnnames($value); ?>1",align:"Ll00", ifFormat:"%d-%b-%Y %H:%M",showsTime:true, onUpdate : calcUpdateEndDate});</script>
	</td>
</tr>
</table>