<?php 
function checkNum($udttotrows) {
	return ($udttotrows%2) ? TRUE : FALSE;
}

if ($active_file == "thinline.php") {
	$udf_area = $_SESSION['top_menu_tabs'];
	$hashlocation = $gval[2];
} 

$sqludf = "SELECT COUNT(id) AS udffields FROM udf_definitions WHERE registrantid =".RID." AND location='".$udf_area."' AND isactive = 1"; 
echo $sqludf;
$resudf = mysql_query($sqludf);
$rowudf = mysql_fetch_array($resudf);
$udttotrows = $rowudf['udffields'];

if ($rowudf['udffields']) {
	$classcounter = 1;
	$sqludf = "SELECT * FROM udf_definitions WHERE registrantid =".RID." AND location='".$udf_area."' AND isactive = 1 ORDER BY id ASC"; 
	$resudf = mysql_query($sqludf);
	
	$twocounter = 1;
	$totrecordcounter = 1; ?>
	<tr>
		<td colspan="2"><br><strong>User Defined Fields</strong></td>
	</tr>
	<tr>
<?php 
	while ($rowudf = mysql_fetch_array($resudf)) {
		if ($twocounter == 1) { echo "<tr>"; $tdwidth = "25%"; include("includes/ls.php"); }
		if ($twocounter == 2) { $tdwidth = "30%"; }
		if ((checkNum($udttotrows) === TRUE) && ($udttotrows == $totrecordcounter)) { $udfcolspan = "colspan=\"2\""; }
		$sqludfval = "SELECT udfvalue FROM udf_values WHERE registrantid =".RID." AND hashid = '".$hashlocation."' AND udfdefid='".$rowudf['hashid']."'";
		//echo $sqludfval;
		$resudfval = mysql_query($sqludfval);
		$rowudfval = mysql_fetch_array($resudfval);
		
		// Check for compulsory fields
		if ($rowudf['iscompulsory'] == 1) {
			$redstar = '<font color="red" size="3">*</font>';
		} 
		
		?>
		<td valign="top" <?php echo $udfcolspan; ?> class="ls_<?php echo $ls ?>">
			<?php 
			if ($rowudf['fieldtype'] == 1) { // A textbox ?>
			<?php echo $rowudf['label']; ?> <?php echo $redstar ?><br>
				<input onchange="javascript:DataChange();" type="text" class="standardfield" name="udf<?php echo $rowudf['hashid']; ?>" value="<?php echo $rowudfval['udfvalue']; ?>">
			<?php 
			} elseif ($rowudf['fieldtype'] == 2) { // A select ?>
				<?php 
				// Settings: Drop down Label, Field name, Current value
				generateUDFselect($rowudf['label'],$rowudf['hashid'],$rowudfval['udfvalue']);?>
			<?php 
			} elseif ($rowudf['fieldtype'] == 3) { // A checkbox  ?>
				<?php echo $rowudf['label']; ?> <?php echo $redstar ?><br>
			<?php
				if ($rowudfval['udfvalue'] == "on") {
					$checkboxcheck = "checked";
				} ?>
				<input onchange="javascript:DataChange();" <?php echo $checkboxcheck; ?> type="checkbox" name="udf<?php echo $rowudf['hashid']; ?>">
			<?php 
			} elseif ($rowudf['fieldtype'] == 4) { // A textarea ?>
				<?php echo $rowudf['label']; ?> <?php echo $redstar ?><br>
				<textarea style="width:95%;height:105px" onchange="javascript:DataChange();" name="udf<?php echo $rowudf['hashid']; ?>"><?php echo $rowudfval['udfvalue']; ?></textarea>
			<?php 
			} ?>
			
		</td>
<?php 	// Generate a udf string
		if ($totrecordcounter == 1) {
			$udf_field_elements = $rowudf['hashid'];
		} else {
			$udf_field_elements = $udf_field_elements.":".$rowudf['hashid'];
		}
		if ($twocounter == 2) { echo "</tr>"; $twocounter = 0; }
		$twocounter++;
		$totrecordcounter++;
		unset($checkboxcheck);
		unset($redstar);
	}
} ?>
</tr>
<input type="hidden" name="udflocation" value="<?php echo $udf_area; ?>">