<?php
include_once("_globalconnect.php");
if (($_GET['action'] == 'deletefile') && ($_GET['fileid'] != "")) {
    $sql = "SELECT * FROM printers WHERE id = ".mysql_real_escape_string($_GET['fileid']);
    //echo $sql."<br />";
    $res = mysql_query($sql);
    $row = mysql_fetch_array($res);
    
    $thumbnail = "./printers/".$row['thumb_file'];
    if (file_exists($thumbnail)) {
        unlink($thumbnail);
    } else {
        die("$thumbnail is not found");
    }
    
    $file_name = "./printers/".$row['file_name'];
    if (file_exists($file_name)) {
        unlink($file_name);
    } else {
        die("$file_name is not found");
    }
    
    $sql = "DELETE FROM printers WHERE id = ".mysql_real_escape_string($_GET['fileid']);
    mysql_query($sql);
    
    header("Location: /thinline.php?id=::::::::::::::::::::0AA1883C6411F7873CB83DACB17B0AFC");
    
}
?>
<br />
<center>
<table border="0" width="98%" cellpadding="0" cellspacing="3">
    <?php
    if (($_SESSION['username'] == "Andy Miyakawa") || (ISMASTER == 1)) { ?>
        <tr>
            <td colspan="5">
                <form action="printers-upload.php" method="post" enctype="multipart/form-data">
                <table border=0 width="100%">
                    <tr>
                        <td class="ls_top">
                            <input type="text" style="width:200px;padding:3px;font-size:14px;" name="revision" placeholder="Type a file revision (optional)" />
                            <br /><br />
                            <input type="text" name="folder" style="width:200px;padding:3px;font-size:14px" value="" placeholder="Type a folder name (optional) " />
                        </td>
                        <td class="ls_top" width="30%">
                            <textarea name="description" style="width:95%;height:75px;" placeholder="Enter a file description"></textarea>
                            <br />
                            
                        </td>
                        <td class="ls_top">
                            <table width="100%">
                                <tr>
                                    <td width="15%">Thumbnail:</td><td><input name="files[]" type="file" /><br /></td>
                                </tr>
                                <tr>
                                    <td width="15%">Main File:</td><td><input name="files[]" type="file" /><br /></td>
                                </tr>
                            </table>
                        </td>
                        <td class="ls_top" style="font-size:11px;">
                           <?php
                           $count = 1;
                            foreach ($registeredCountries AS $c) { ?>
                                <?php echo $c;?>:<input type="checkbox" name="countries[]" value="<?php echo $c;?>" />
                            <?php
                                if ($count == 3) {
                                    echo "<br />";
                                    $count = 0;
                                }
                                $count++;
                            } ?>
                        </td>
                        <td class="ls_top">
                            <input type="submit" style="font-size:14px;" value="Upload files" />
                        </td>
                    </tr>
                </table>
                </form>
                <br />
            </td>
        </tr>
    <?php
    } ?>
    <tr>
        <td class="ls_top"><strong>Thumbnail</strong></td>
        <td class="ls_top"><strong>File Name</strong></td>
        <td class="ls_top"><strong>Revision</strong></td>
        <td class="ls_top"><strong>Description</strong></td>
        <td class="ls_top"><strong>Actions</strong></td>
    </tr>
<?php
if (($_SESSION['username'] == "Andy Miyakawa") || (ISMASTER == 1)) {
    $sql = "SELECT * FROM printers ORDER BY file_name ASC";
} else {
    $sql = "SELECT * FROM printers WHERE FIND_IN_SET('".$_SESSION['franchisecountry']."', country) ORDER BY file_name ASC";
}
//echo $sql."<br />";
$res = mysql_query($sql);
while ($row = mysql_fetch_array($res)) {
    include("includes/ls.php"); ?>
    <tr>
        <td class="ls_<?php echo $ls ?>"><img src="/printers/<?php echo $row['thumb_file'];?>" /></td>  
        <td class="ls_<?php echo $ls ?>" style="font-size:14px;"><a target="_playball" href="/printers/<?php echo str_replace(" ","%20",$row['file_name']);?>"><?php echo $row['file_name'];?></a></td>
        <td class="ls_<?php echo $ls ?>" style="font-size:14px;"><?php echo $row['revision'];?></td>
        <td class="ls_<?php echo $ls ?>" style="font-size:14px;"><?php echo $row['description'];?></td>
        <td align="center" class="ls_<?php echo $ls ?>" style="font-size:14px;"><a href="/thinline.php?id=::::::::::::::::::::0AA1883C6411F7873CB83DACB17B0AFC&action=deletefile&fileid=<?php echo $row['id'];?>"><img src="images/icons/delete2_16.png" onclick="return confirm('Are you sure you want to delete this file?')" /></a></td>
    </tr>
<?php
} ?>
</table>
</center>
<br /><br />