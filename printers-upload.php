<?php
include_once("_globalconnect.php");
$valid_formats = array("jpg", "png", "gif", "zip", "bmp", "pdf", "jpeg", "ppt");
$max_file_size = 1024*10000; //10000 kb
$path = "printers/"; // Upload directory
$folder = $_POST['folder'];
$count = 0;

if(isset($_POST) AND $_SERVER['REQUEST_METHOD'] == "POST"){
    
	// Loop $_FILES to exeicute all files
	foreach ($_FILES['files']['name'] as $f => $name) {
        
        if ($count == 0) {
            $thumbnail = "./printers/".$name;
            if (file_exists($thumbnail)) {
                die("$name already exists.  Please go back, delete the file and resubmit");   
            }
        } elseif ($count == 1) {
            $file_name = "./printers/".$name;
            if (file_exists($file_name)) {
                die("$name already exists.  Please go back, delete the file and resubmit");   
            }
        }
    }
    
    $count = 0;
    foreach ($_FILES['files']['name'] as $f => $name) {    
        if ($_FILES['files']['error'][$f] == 4) {
	        continue; // Skip file if any error found
	    }	       
	    if ($_FILES['files']['error'][$f] == 0) {	           
	        if ($_FILES['files']['size'][$f] > $max_file_size) {
	            $message[] = "$name is too large!.";
                die("$name is too large");
	            continue; // Skip large files
	        } elseif( ! in_array(pathinfo(strtolower($name), PATHINFO_EXTENSION), $valid_formats) ){
				$message[] = "$name is not a valid format";
				die("$name is not a valid format");
                continue; // Skip invalid file formats
			} else { // No error found! Move uploaded files 
	            if(move_uploaded_file($_FILES["files"]["tmp_name"][$f], $path.$name))
	            $count++; // Number of successfully uploaded file
                
                if ($count == 1) {
                    $thumnail_file = $name;
                }
                
                // lets add the file details to the db
                if ($count == 2) {
                    
                    // generate country list
                    foreach ($_POST['countries'] AS $c) {
                        $countries .= $c.",";
                    }
                    
                    $sql = "INSERT INTO `printers` (`thumb_file`, `file_name`, `folder`, `revision`, `description`, `country`)
                            VALUES ('".mysql_real_escape_string($thumnail_file)."',
                                    '".mysql_real_escape_string($name)."',
									'".mysql_real_escape_string($folder)."',
                                    '".mysql_real_escape_string($_POST['revision'])."',
                                    '".mysql_real_escape_string($_POST['description'])."',
                                    '".mysql_real_escape_string(substr($countries,0,-2))."');";
                    //echo $sql."<br />";
                    mysql_query($sql);
                }
                
	        }
	    }
	}
}

header("Location: /thinline.php?id=::::::::::::::::::::0AA1883C6411F7873CB83DACB17B0AFC");

?>