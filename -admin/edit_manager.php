<?php
include_once("_globalconnect.php");

// Lets set the default value.
if (($_POST['savebtn'] == "Save Default & Compulsory") || ($_POST['savebtn'] == "Save Default") ) {
	
	/// Lets make sure a iscompulsory value exists
	if ($_POST['iscompulsory'] == "on") {
		$query = "SELECT fieldvalue FROM custom_settings WHERE fieldname = '".$_GET['editting']."' AND fieldvalue = 'silverIsCompulsory' AND registrantid=".RID;
		$result = mysql_query($query);
		$row = mysql_fetch_array($result);
		
		if (!$row) {
			$querybb = "INSERT INTO custom_settings (registrantid, fieldname, fieldvalue, fieldvalue2, iscompulsory) VALUES (
				".RID.",
				'".$_GET['editting']."',
				'silverIsCompulsory',
				'".$_POST['sarea']."',
				'1'
	  			)";
			mysql_query($querybb) Or Die ("Cannot submit entry!");
			echo $querybb;
		}
	}
	
	/// UPdate the compulsory field straight away.
	if ($_POST['iscompulsory'] == "on") { $iscomvalue = 1; } else { $iscomvalue = 0; }
	$querydel = "UPDATE custom_settings SET iscompulsory  = ".$iscomvalue." WHERE fieldname = '".$_GET['editting']."' AND fieldvalue = 'silverIsCompulsory' AND registrantid=".RID;
	mysql_query($querydel);
	
	// Let's see if the value already exists before we delete it.
	$query = "SELECT id FROM custom_settings WHERE registrantid = ".$_SESSION['registrantid'];
	$result = mysql_query($query);
	
	while ($row = mysql_fetch_array($result)) {
		if (strtoupper(md5($row['id'])) == $_POST['isdefault']) {
			$querydel2 = "UPDATE custom_settings SET isdefault = 0 WHERE fieldname = '".$_GET['editting']."' AND registrantid=".RID;
			mysql_query($querydel2) Or Die ("Cannot submit entry!");
			//echo $querydel2."<br>";
			$querydel = "UPDATE custom_settings SET isdefault = 1 WHERE id = ".$row['id']." LIMIT 1";
			mysql_query($querydel) Or Die ("Cannot submit entry!");
			//echo $querydel;
			break;
		}
	}
	
}

if ($_GET['editting'] == "department") {
	$usetable = "contacts_individuals";
} else {
	$usetable = "contacts_company";
}

if ($_POST['processsubmit'] == 1) {
	$query = "INSERT INTO custom_settings (registrantid, fieldname, fieldvalue, fieldvalue2) VALUES (
		".$_SESSION['registrantid'].",
		'".$_GET['editting']."',
		'".$_POST['addvalue']."',
		'".$_POST['addvalue2']."'
	  	)";
	mysql_query($query) Or Die ("Cannot submit entry!");
	
	////  Audit trail = action / table / item id, datestamp
	$atfiledetail = $_GET['editting']."#:#".$_POST['addvalue'];
	audittrail("created custom value", "custom_settings", $atfiledetail, $sGMTMySqlString);
}

if ($_GET['action'] == "d") {
	// Let's see if the value already exists before we delete it.
	$query = "SELECT id, fieldvalue FROM custom_settings WHERE registrantid = ".$_SESSION['registrantid'];
	$result = mysql_query($query);
	while ($row = mysql_fetch_array($result)) {
		if (strtoupper(md5($row['id'])) == $_GET['id']) {
			$duplicatefield = $row['fieldvalue'];
			break;
		}
	}
	
	$query = "SELECT ".$_GET['editting']." FROM ".$usetable." WHERE ".$_GET['editting']." = '".$duplicatefield."' AND registrantid = ".$_SESSION['registrantid'];
		//echo $query."<br>";
	$result = mysql_query($query);
	$row = mysql_fetch_array($result);
	
	if (!$row[$_GET['editting']]) {
		// Then we can delete the value
		$query = "SELECT id FROM custom_settings WHERE registrantid = ".$_SESSION['registrantid'];
		$result = mysql_query($query);
		while ($row = mysql_fetch_array($result)) {
			if (strtoupper(md5($row['id'])) == $_GET['id']) {
				$querydel = "DELETE FROM custom_settings WHERE id = ".$row['id']." LIMIT 1";
					//echo $querydel;
				mysql_query($querydel) Or Die ("Cannot submit entry!");
				break;
			}
		}
	} else {
		// Then lets tell them they have custoemrs with this value already set.
		$cannotdelete = 1;
		$cannotdeletetrigger = 'onload="cannotDelete();"';
	}
}

if ($_GET['action'] == "e") {
	// Let's see if the value already exists before we delete it.
	$query = "SELECT id, fieldvalue FROM custom_settings WHERE registrantid = ".$_SESSION['registrantid'];
	$result = mysql_query($query);
	while ($row = mysql_fetch_array($result)) {
		if (strtoupper(md5($row['id'])) == $_GET['id']) {
			$duplicatefield = $row['fieldvalue'];
			break;
		}
	}
	
	$query = "SELECT ".$_GET['editting']." FROM ".$usetable."  WHERE ".$_GET['editting']." = '".$duplicatefield."' AND registrantid = ".$_SESSION['registrantid'];
		//echo $query."<br>";
	$result = mysql_query($query);
	$row = mysql_fetch_array($result);
	
	if (!$row[$_GET['editting']]) {
		// Then we can delete the value
		$query = "SELECT id FROM custom_settings WHERE registrantid = ".$_SESSION['registrantid'];
			//echo $query;
		$result = mysql_query($query);
		while ($row = mysql_fetch_array($result)) {
			if (strtoupper(md5($row['id'])) == $_GET['id']) {
				// Not in use so let's modify
				$querydel = "UPDATE custom_settings SET fieldvalue = '".$_POST['currentvalue']."' WHERE id = ".$row['id']." LIMIT 1";
					//echo $querydel;
				mysql_query($querydel) Or Die ("Cannot submit entry!");
				break;
			}
		}
	} else {
		// Then lets tell them they have custoemrs with this value already set.
		$cannotdelete = 1;
		$cannotdeletetrigger = 'onload="cannotModify();"';
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "includes/xhtml1-transitional.dtd">
<html>
<head>
	<title>Removals Manager</title>
	<?php 
	if (ERES > 1024) { ?>
		<style type="text/css" media="all">@import "css/style.css";</style>
	<?php 
	} else { ?>
		<style type="text/css" media="all">@import "css/style_small.css";</style>
	<?php
	}?>
	<?php
	if ($cannotdelete == 1) { ?>
	<SCRIPT LANGUAGE="JavaScript">
	<!--
	function cannotDelete() {
	   alert("This value is in use and cannot be deleted?\nCheck our help section for assistance.");
	}
	function cannotModify() {
	   alert("This value is in use and cannot be modified?\nCheck our help section for assistance.");
	}
	// -->
    </SCRIPT>
	<?php 
	}?>
	<script language="JavaScript" type="text/javascript">
	<!--
	function checkform ( form ) {
	  if (form.addvalue.value == "") {
	    alert( "Please fill in a value." );
	    form.addvalue.focus();
	    return false ;
	  }
	  return true ;
	}
	//-->
	</script>
</head>
<body <?php echo $cannotdeletetrigger; ?>>
	<table cellspacing="0" cellpadding="0" border="0" width="97%">
	<tr>
		<td><img src="/images/<?php echo $_SESSION['franchisedata']['logo'];?>" alt="" / align="center" border="0"></td>
		<td align="right"></td>
	</tr>
	</table><br><br>
	<?php
	//echo "Editting: ".$_GET['editting']."<br>";
	//echo "Help: ".$_GET['help']."<br>";
	function tablebody($icon,$helpheader,$helpdtext) { ?>
	<table cellspacing="0" cellpadding="5" border="0" width="100%" height="300">
	<tr>
		<td colspan="4" height="5"><img src="/images/spacer.gif" height="5" width="1"></td>
	</tr>
	<tr>
		<td valign="top" align="right" rowspan="2" width="5%">
			<img src="/images/icons/<?php echo $icon?>"><img src="/images/spacer.gif" height="1" width="10"><br>
			<img src="/images/spacer.gif" height="55" width="1">
		</td>
		<td valign="top" height="5">
			<h1><?php echo $helpheader ?></h1>
		</td>
		<td rowspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td valign="top" height="100%">
			<?php echo $helpdtext ?><br>
		</td>
	</tr>
	</table>
	<?php
	}  
	
	function pulltablevalue($fieldname,$fieldtitle,$bigdaddy) {
		$query = "SELECT id,fieldvalue, fieldvalue2 , isdefault, iscompulsory FROM custom_settings WHERE registrantid = ".$_SESSION['registrantid']." AND fieldname = '".$fieldname."' AND fieldvalue != 'silverIsCompulsory' ORDER BY fieldvalue ASC";
		//echo $query;
		$tUnixTime = time();
		$sGMTMySqlString = date("Y-m-d H:i:s", $tUnixTime);
		////  Audit trail = action / table / item id, datestamp
		audittrail("viewed custom values", "custom_settings", $fieldname, $sGMTMySqlString);
		$result = mysql_query($query); ?>
			<table cellspacing="0" cellpadding="5" border="0" width="100%">
			<tr>
				<td valign="top" align="right" width="5%">
					<img src="/images/icons/folder_edit.png"><img src="/images/spacer.gif" height="1" width="10"><br>
					
				</td>
				<td valign="top" height="5">
					<h1>Change <?php echo $fieldtitle ?> values</h1>
				</td>
			</tr>
			
			<tr>
				<td valign="top">
					&nbsp;
				</td>
				<td valign="top">
				<form method="POST" name="addvalue" onsubmit="return checkform(this);" action=""edit_manager.php?editting=<?php echo $_GET['editting'] ?>&action=&id=<?php echo $_GET['id'] ?>>
				<strong>Add Value</strong>:<br>
				<table border="0" cellpadding="0" cellspacing="3" width="100%">
					<tr>
						<td class="lightshade_on">
							Value: <input type="text" maxlength="40" name="addvalue" class="standardfield_medium"><br>
							<?php 
							if ($_GET['editting'] == 'showtimeas') { ?>
							Value 2: <input type="text" maxlength="40" name="addvalue2" class="standardfield_medium">
							<?php
							}?>
							</td>
						<td width="5%" class="lightshade_on"><input type="image" src="/images/icons/note_add_16.png" alt="Click here to add this value."></td>
						<td><img src="images/spacer.gif" height="1" width="10"></td>
						<td>
						<div class="note">After creating the value, return to the card and click the browser <strong>Refresh</strong> button to update the
						<strong><?php echo $fieldtitle ?></strong> drop down menu.
						<br><br>
						<strong>Important:</strong> If you have entered information we recommend you exit this screen by clicking the background window and click <strong>Save</strong> first to prevent having to re-enter the information.
						</div>
						</td>
					</tr>
				</table>
				<input type="hidden" name="processsubmit" value="1">
				</form>
				<form method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?>?editting=<?php echo $_GET['editting'] ?>&action=e&id=<?php echo strtoupper(md5($row['id'])) ?>" name="form_<?php echo $divnum1 ?>">
				 	<script>
					function confirmDelete(delUrl) {
						  if (confirm("Are you sure you want to delete this value.")) {
						    document.location = delUrl;
						  }
					}
					function show_hide(sh1,hi1) {
						document.getElementById(hi1).style.display='none';
						obj=document.getElementById(sh1)
						obj.style.display == "block" ? obj.style.display = "block" : obj.style.display = "block"; //  {
					}
					</script>
				
				<table cellpadding="0" cellspacing="2" width="100%" border="0">
				<tr>
				  <td colspan="4">
					<table cellpadding="0" cellspacing="2" width="100%" border="0">
					<tr>
						<td align="left" width="14%">
						<?php 
						if ($bigdaddy != 1) {?>
							<font style="font-family:Tahoma;font-size:11px">Is Compulsory?</font></td>
							<?php 
							$sqlis = "SELECT iscompulsory FROM custom_settings WHERE registrantid = ".$_SESSION['registrantid']." AND fieldname = '".$fieldname."' AND fieldvalue = 'silverIsCompulsory'";
							$resultiscom = mysql_query($sqlis);
							$rowiscom = mysql_fetch_array($resultiscom);
							if ($rowiscom['iscompulsory'] == 1)  {
								$iscompulsorychecked = 'checked';
							}
						}
						?>
						<td align="left" width="3%">
						<?php 
						if ($bigdaddy != 1) {?>
							<input type="checkbox" name="iscompulsory" <?php echo $iscompulsorychecked; ?>>
						<?php 
						}?>
						</td>
						<td align="left" width="3%">&nbsp;</td>
						<td>
							<font style="font-family:Tahoma;font-size:11px">
						<?php 
						if ($bigdaddy != 1) {
							$comptext = " or the 'Is Compulsory?' field";
							$comptext_2 = " &amp; Compulsory";
							$comptext_3 = " & Compulsory";
						}
						?>
							If you change the 'Default' field<?php echo $comptext; ?>, <strong>you must</strong> click the Save Default<?php echo $comptext_2; ?> button.
							</font>
						</td>
						<td>&nbsp;</td>
						<td align="right"><input type="submit" style="font-family:tahoma;font-size:11px;padding:3px" name="savebtn" value="Save Default<?php echo $comptext_3; ?>"></td>
					</tr>
					</table>
				  </td>
				<tr><td colspan="4">&nbsp;</td></tr>
				<tr align="center">
					<td class="lightshade_top"><strong>Value</strong></td>
					<?php 
					if ($_GET['editting'] == 'showtimeas') { ?>
					<td class="lightshade_top"><strong>Value 2</strong></td>
					<?php 
					} ?>
					<td class="lightshade_top"><strong>Default</strong></td>
					<td class="lightshade_top"><strong>Delete</strong></td>
				</tr>
					<?php 
			$classcounter = 2;
			$divnum1 = 1;
			$divnum2 = 2;
			while ($row = mysql_fetch_array($result)) {
			if ($classcounter == 2) { 
				$lightshade = "on";
				$classcounter = 1;
			} else { 
				$lightshade = "off";
				$classcounter = 2;
			}?>		<script>
					function submitform_<?php echo $divnum1 ?>() {
					    document.form_<?php echo $divnum1 ?>.submit();
					}
					</script>
					<tr>
						<td class="lightshade_<?php echo $lightshade ?>">
							<div class="displayadress_2">
								<span id="s_<?php echo $divnum1 ?>">
									<?php echo ucfirst($row['fieldvalue']); ?>
								</span>
							</div>
						</td>
						<?php 
						if ($_GET['editting'] == 'showtimeas') { ?>
						<td class="lightshade_<?php echo $lightshade ?>">
							<div class="displayadress_2">
									<?php echo ucfirst($row['fieldvalue2']); ?>
							</div>
						</td>
					<?php 
						} ?>
						<?php 
						if ($row['isdefault'] == 1) {
							$checkingmeout = "checked";
							
						} else {
							$checkingmeout = "";
						}?>
						<td align="center" class="lightshade_<?php echo $lightshade ?>"><input <?php echo $checkingmeout; ?> type="radio" name="isdefault" value="<?php echo strtoupper(md5($row['id'])); ?>"></td>
						
						<td align="center" width="5%" class="lightshade_<?php echo $lightshade ?>">
							<a href="javascript:confirmDelete('<?php echo $_SERVER['PHP_SELF'] ?>?editting=<?php echo $_GET['editting'] ?>&action=d&id=<?php echo strtoupper(md5($row['id'])) ?>')"><img src="/images/icons/note_delete_16.png" border="0"></a>
						</td>
					</tr>
					
			<?php
			$divnum1 = $divnum1 + 10;
			$divnum2 = $divnum2 + 10;
			} ?>
				</table>
				</td>
			</tr>
		<input type="hidden" name="sarea" value="<?php echo $_GET['sarea'] ?>">
		</form>
			</table>
	<?php
	}
	?>
	
	<?php
	if ($_GET['help'] == "selectaddress") {
		$helpheader = "Submitting Address Information";
		$helptext = "Add a customer address here.  You will add the pickup and drop off addresses for the job when creating a job.  You can copy this address
					to a job or quotation.
					";
		tablebody("help_32.png",$helpheader,$helptext);
	} elseif ($_GET['help'] == "contacttype") {
		$helpheader = "Setting Contact Type";
		$helptext = "With Removals Manager you are able to have <strong>Public</strong> or <strong>Private</strong> contacts within
		the contacts database.  <br><br>If you set a contact as Private then only you will be able to see this contact within the system.
		If you set the contact as Public then the contact will be visible <strong>to all users</strong> within your business.
					";
		tablebody("help_32.png",$helpheader,$helptext);
	} elseif ($_GET['help'] == "status") {
		$helpheader = "Setting Contact Status";
		$helptext = "Our system offers the flexibility to create any number of customer statuses.  We have created a few default values, but these can be deleted
		and changed to values that suit your business.  All you need to do is click the edit link to the right of the right of the Status drop down menu.
		<br><br>Please note that once a contact has been set with this Status value, the value cannot be deleted until the contacts Status value has been changed.
					";
		tablebody("help_32.png",$helpheader,$helptext);
	} elseif ($_GET['help'] == "lineofbusiness") {
		$helpheader = "Setting Line of Business";
		$helptext = "Our system offers the flexibility to create as many Line of Business types as you require.  We have created a few default values, but these can be deleted
		and changed to values that suit your business.  All you need to do is click the edit link to the right of the right of the Line of Business drop down menu.
		<br><br>Please note that once a contact has been set with this Line of Business value, the value cannot be deleted until the contacts Line of Business value has been changed.
					";
		tablebody("help_32.png",$helpheader,$helptext);
	} elseif ($_GET['help'] == "isdriver") {
		$helpheader = "Is this user a driver ?";
		$helptext = "If you wish to assign jobs to a particular user, you must check this box. 
					";
		tablebody("help_32.png",$helpheader,$helptext);
	} elseif ($_GET['help'] == "driverallocation") {
		$helpheader = "Allocated Driver";
		$helptext = "Once a quotation becomes a job you can allocate it to the river who will attend the job.  This will help you to manage
		your job scheduling.
					";
		tablebody("help_32.png",$helpheader,$helptext);
	} elseif ($_GET['help'] == "category") {
		$helpheader = "Setting Category";
		$helptext = "Our system offers the flexibility to create as many Category types as you require.  We have created a few default values, but these can be deleted
		and changed to values that suit your business.  All you need to do is click the edit link to the right of the right of the Category drop down menu.
		<br><br>Please note that once a contact has been set with this Category value, the value cannot be deleted until the contacts Category value has been changed.
					";
		tablebody("help_32.png",$helpheader,$helptext);
	} elseif ($_GET['help'] == "reference") {
		$helpheader = "Setting Reference";
		$helptext = "The Reference setting refers to a reference within your business.  Having this value set makes it easy to find the right person responsible for this contact should this contact call in
		or send an email to a shared mailbox.<br><br>To create additional references, you will need to go through the Settings Tab and click on Users.
					";
		tablebody("help_32.png",$helpheader,$helptext);
	} elseif ($_GET['help'] == "parentcompany") {
		$helpheader = "Setting Linked to Company / Parent Company";
		$helptext = "If this contact has a parent company or is linked to a company, you can set that link here.<br><br>All Public  company contacts will be listed in the Link to Company drop down menu.
					";
		tablebody("help_32.png",$helpheader,$helptext);
	} elseif ($_GET['help'] == "volumemeasure") {
		$helpheader = "Add additional volume measures.";
		$helptext = "You can add as many Volume measures as you like.  Just click the edit link to the right of the help link and enter a value in the Add Value field.<br><br>
					You can also delete these value by clicking on the delete icon next to each entry.
					";
		tablebody("help_32.png",$helpheader,$helptext);
	} elseif ($_GET['help'] == "unitmeasure") {
		$helpheader = "Add additional weight measures.";
		$helptext = "You can add as many Weight measures as you like.  Just click the edit link to the right of the help link and enter a value in the Add Value field.<br><br>
					You can also delete these value by clicking on the delete icon next to each entry.
					";
		tablebody("help_32.png",$helpheader,$helptext);
	} elseif ($_GET['help'] == "leadmenrequired") {
		$helpheader = "Select the number of Men Required.";
		$helptext = "Select the total number of men requested by the customer.
					";
		tablebody("help_32.png",$helpheader,$helptext);
	} elseif ($_GET['help'] == "leadcollectfloor") {
		$helpheader = "Select the Collection Floor.";
		$helptext = "Select the floor for the Collection Address.
					";
		tablebody("help_32.png",$helpheader,$helptext);
	} elseif ($_GET['help'] == "leadcoll_lift") {
		$helpheader = "Select Collection Lift.";
		$helptext = "Select whether or not there is a lift at the Collection Address.
					";
		tablebody("help_32.png",$helpheader,$helptext);
	} elseif ($_GET['help'] == "leaddel_lift") {
		$helpheader = "Select Delivery Lift.";
		$helptext = "Select whether or not there is a lift at the Delivery Address.
					";
		tablebody("help_32.png",$helpheader,$helptext);
	} elseif ($_GET['help'] == "leadparking1") {
		$helpheader = "Select Collection Parking.";
		$helptext = "Identify the collection address parking arrangement.
					";
		tablebody("help_32.png",$helpheader,$helptext);
	} elseif ($_GET['help'] == "leadparking2") {
		$helpheader = "Select Delivery Parking.";
		$helptext = "Identify the delivery address parking arrangement.
					";
		tablebody("help_32.png",$helpheader,$helptext);
	} elseif ($_GET['help'] == "leaddeliverfloor") {
		$helpheader = "Select the Delivery Floor.";
		$helptext = "Select the floor for the Delivery Address.
					";
		tablebody("help_32.png",$helpheader,$helptext);
	} elseif ($_GET['editting'] == "leadparking2") {
		pulltablevalue($_GET['editting'],"Delivery Parking");
	} elseif ($_GET['editting'] == "leaddeliverfloor") {
		pulltablevalue($_GET['editting'],"Delivery Floor");
	} elseif ($_GET['editting'] == "leadparking1") {
		pulltablevalue($_GET['editting'],"Collection Parking");
	} elseif ($_GET['editting'] == "leadcoll_lift") {
		pulltablevalue($_GET['editting'],"Collection Lift");
	} elseif ($_GET['editting'] == "leaddel_lift") {
		pulltablevalue($_GET['editting'],"Delivery Lift");
	} elseif ($_GET['editting'] == "leadcollectfloor") {
		pulltablevalue($_GET['editting'],"Collection Floor");
	} elseif ($_GET['editting'] == "companystatus") {
		pulltablevalue($_GET['editting'],"Contact Status");
	} elseif ($_GET['editting'] == "lineofbusiness") {
		pulltablevalue($_GET['editting'],"Line of Business");
	} elseif ($_GET['editting'] == "companycategory") {
		pulltablevalue($_GET['editting'],"Company Category");
	} elseif ($_GET['editting'] == "contacttype") {
		pulltablevalue($_GET['editting'],"Contact Type");
	} elseif ($_GET['editting'] == "department") {
		pulltablevalue($_GET['editting'],"Department");
	} elseif ($_GET['editting'] == "int_department") {
		pulltablevalue($_GET['editting'],"Internal Departments",1);
	} elseif ($_GET['help'] == "viewaddress") {
		$helpheader = "Viewing Address Information";
		$helptext = "You can have up to 4 addresses for each contact.<br><br>Select the address link in the right hand 
					cell and the address will appear in the left hand cell.
					";
		tablebody("help_32.png",$helpheader,$helptext);
	} elseif ($_GET['editting'] == "productcategory") {
		pulltablevalue($_GET['editting'],"Product Categories");
	} elseif ($_GET['editting'] == "unit") {
		pulltablevalue($_GET['editting'],"Unit Categories");
	} elseif ($_GET['editting'] == "productgroup") {
		pulltablevalue($_GET['editting'],"Product Group Categories");
	} elseif ($_GET['editting'] == "subgroup") {
		pulltablevalue($_GET['editting'],"Sub Group Categories");
	} elseif ($_GET['editting'] == "volumemeasure") {
		pulltablevalue($_GET['editting'],"Volume Measure");
	} elseif ($_GET['editting'] == "unitmeasure") {
		pulltablevalue($_GET['editting'],"Weight Measure");
	} elseif ($_GET['editting'] == "leadtype") {
		pulltablevalue($_GET['editting'],"Quote Type");
	} elseif ($_GET['editting'] == "leadsource") {
		pulltablevalue($_GET['editting'],"Quote Status");
	} elseif ($_GET['editting'] == "leadstatus") {
		pulltablevalue($_GET['editting'],"Quote Source");
	} elseif ($_GET['editting'] == "probability") {
		pulltablevalue($_GET['editting'],"Quote Probability");
	} elseif ($_GET['editting'] == "activitytype") {
		pulltablevalue($_GET['editting'],"Activity Type",1);
	} elseif ($_GET['editting'] == "activitystatus") {
		pulltablevalue($_GET['editting'],"Activity Status",1);
	} elseif ($_GET['editting'] == "showtimeas") {
		pulltablevalue($_GET['editting'],"Show Time As",1);
	} elseif ($_GET['editting'] == "priority") { // Tickets
		pulltablevalue($_GET['editting'],"Ticket Priority",1);
	} elseif ($_GET['editting'] == "ticketcategory") { // Tickets
		pulltablevalue($_GET['editting'],"Ticket Category",1);
	} elseif ($_GET['editting'] == "severity") { // Tickets
		pulltablevalue($_GET['editting'],"Ticket Severity",1);
	} elseif ($_GET['editting'] == "ticketstatus") { // Tickets
		pulltablevalue($_GET['editting'],"Ticket Status",1);
	} elseif ($_GET['editting'] == "jobpaymenttype") { // Payments
		pulltablevalue($_GET['editting'],"Payment Type",1);
	} elseif ($_GET['editting'] == "contactcategory") { // Tickets
		pulltablevalue($_GET['editting'],"Contacts Category");
	} elseif ($_GET['editting'] == "leadmenrequired") { // Leads
		pulltablevalue($_GET['editting'],"Men Required");
	} elseif ($_GET['help'] == "productbuilder") {
		$helpheader = "Product Builder for Leads";
		$helptext = "Simply select which products you want to add to the lead and click the add button to transfer them to the <strong>Active box</strong>.<br><br>Once you have added all the products to the
					lead, click the generate button and this will create the individual lines so you can add quanities, discount or change pricing.
					";
		tablebody("help_32.png",$helpheader,$helptext);
	} else {
	 ?>
	<table cellspacing="0" cellpadding="5" border="0" width="100%" height="300">
	<tr>
		<td width="20%">&nbsp;</td>
		<td valign="center" rowspan="2">
			<img src="/images/icons/warning.png"><img src="/images/spacer.gif" height="1" width="10"><br>
			<img src="/images/spacer.gif" height="55" width="1">
		</td>
		<td valign="bottom">
			<h1>No help file available.</h1>
		</td>
		<td width="20%">&nbsp;</td>
	</tr>
	<tr>
		<td width="20%">&nbsp;</td>
		<td valign="top" colspan="2">
			There is no help topic available.<br>Please email <a href="mailto:support@removalsmanager.com">support@removalsmanager.com</a>
			if you require further support.
		</td>
		
	</tr>
	</table>
	<?php 
	}
	?>
</body>
</html>
