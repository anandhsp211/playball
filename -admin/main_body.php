<?php include_once("..//_globalconnect.php");

//var_dump($_POST['pform']);
//die;
//var_dump($gval[0]);

if ($_POST['pform'] == 1) {
    // Create a new top level module

    if ($_POST['subcategories'] == 1) {

        $query = "INSERT INTO menu_tabs (registrantid, label, type, datecreated) VALUES (
			  " . RID . ",
			  '" . $_POST['label'] . "',
			  '" . $_POST['type'] . "',
			  '" . $sGMTMySqlString . "'
			  )";
        mysql_query($query) Or Die ("Cannot submit entry! 1");

        // Set Hashid - pass table name & Update the ordering
        sethashidandOrder("menu_tabs");

        /////////////////////////////////////////////////////////////////////////////////
        // Now the tab has been created we may have to create some standard sub modules//
        /////////////////////////////////////////////////////////////////////////////////

        // First type is Calendar //
        if ($_POST['type'] == "Calendar") {
            $arr = array("my calendar", "schedule overview");
            createSubTabs($arr, $sGMTMySqlString);
        } elseif ($_POST['type'] == "Document Store") {
            $arr = array("document list");
            createSubTabs($arr, $sGMTMySqlString);
        } elseif ($_POST['type'] == "Standard Form") {
            $arr = array("list", "new", "search", "bin");
            createSubTabs($arr, $sGMTMySqlString, 0, 0, $_POST['numberseries']);
        } elseif ($_POST['type'] == "Activities") {
            $arr = array("list", "new");
            createSubTabs($arr, $sGMTMySqlString);
        } elseif ($_POST['type'] == "Accounts") {
            $arr = array("receive money", "pay money", "bank accounts", "transaction types");
            createSubTabs($arr, $sGMTMySqlString);
        } elseif ($_POST['type'] == "Reports") {
            $arr = array("list", "new");
            createSubTabs($arr, $sGMTMySqlString);
        } elseif ($_POST['type'] == "Dashboard") {
            $arr = array("dashboard");
            createSubTabs($arr, $sGMTMySqlString);
        } elseif ($_POST['type'] == "Printers") {
            $arr = array("print list");
            createSubTabs($arr, $sGMTMySqlString);
        }

        // We are going to do a redirect to the newly created page...
        $sql = "SELECT MAX(id) AS maxid FROM menu_tabs WHERE registrantid =" . RID;
        $res = mysql_query($sql);
        $row = mysql_fetch_array($res);

        header("Location:" . $_SERVER['PHP_SELF'] . "?id=8:" . md5($row['maxid']) . ":1	");

    } else {
        $query = "INSERT INTO menu_tabs (registrantid, label, type, ismultitier, datecreated) VALUES (
				  " . RID . ",
				  '" . $_POST['label'] . "',
				  '" . $_POST['type'] . "',
				  1,
				  '" . $sGMTMySqlString . "'
				  )";
        mysql_query($query) Or Die ("Cannot submit entry! 101");

        header("Location:" . $_SERVER['PHP_SELF'] . "?id=13:" . sethashidandOrder("menu_tabs") . ":" . $_POST['subcategories'] . "");
    }

} elseif ($_POST['pform'] == 3) {
    // Update a sub level tab
    $query = "UPDATE menusub_tabs SET label = '" . $_POST['label'] . "' WHERE hashid = '" . $_POST['hashid'] . "' AND registrantid=" . RID;
    mysql_query($query) Or Die ("Cannot submit entry! F1239");
    header("Location:" . $_SERVER['PHP_SELF'] . "?id=8:" . $_POST['menu_tabhashid'] . "");
} elseif ($_POST['pform'] == 4) {
    /// Create sub multitier tabs
    $arr = array("list", "new", "search");
    createSubTabs($arr, $sGMTMySqlString, $_POST['label_1'], $_POST['label_2'], $_POST['numberseries_1'], $_POST['numberseries_2'], $_POST['label_singular_1'], $_POST['label_singular_2']);
    header("Location: " . $_SERVER['PHP_SELF'] . "?id=8:" . $_POST['hashid'] . ":1");
} elseif ($_POST['pform'] == 5) {
    // Lets get the highest sort value and add one more
    $menutabdata = explode("&&", $_POST['udfmodule']);
    $query = "SELECT MAX(sortorder) AS EditID FROM udf_definitions WHERE location = '" . $menutabdata[1] . "'AND registrantid = " . RID;
    $result = mysql_query($query);
    $row = mysql_fetch_array($result);
    $sortid = ($row['EditID'] + 1);

    // Lets see if this field requires linking
    if ($_POST['datalinktable'] != "") {
        $islinked = 1;
    } else {
        $islinked = 0;
    }

    // If it is a section header we want to set the
    // single row value to 1
    if ($_POST['udftype'] == 12) {
        $singlelargeval = 1;
        $singlerowval = 1;
    } elseif ($_POST['udftype'] == 11) {
        $singlelargeval = 0;
        $singlerowval = 1;
    } else {
        $singlelargeval = 0;
        $singlerowval = 0;
    }

    // if the drop down is to be created as an obect we need to add a
    // few extra elemetns to the insert..
    if ($_POST['createasobject'] == 1) {
        $is_object = "is_object, is_object_master,";
        $is_object_vals = "1,1,";
    }

    $query = "INSERT INTO udf_definitions (registrantid, fieldtype, columntype, label, labelmsg, location, sortorder, inlists, visibleonlyto, islinked, tablabel, singlerow, singlelarge, " . $is_object . " isactive) VALUES (
			" . RID . ",
			" . $_POST['udftype'] . ",
			'" . $_POST['columntype'] . "',
			'" . str_replace(":", "", mysql_real_escape_string($_POST['udflabel'])) . "',
			'" . mysql_real_escape_string($_POST['labelmsg']) . "',
			'" . $menutabdata[1] . "',
			" . $sortid . ",
			'" . $_POST['inlists'] . "',
			'" . $_POST['visibleonlyto'] . "',
			'" . $islinked . "',
			'" . $_POST['tablabel'] . "',
			" . $singlerowval . ",
			" . $singlelargeval . ",
			" . $is_object_vals . "
			0
			)";

    mysql_query($query) or die ("Cannot create user defined field");

//    var_dump($query);
//    die;

    $query = "SELECT MAX(id) AS EditID FROM udf_definitions";
    $result = mysql_query($query);
    $row = mysql_fetch_array($result);
    $udfdefid = strtoupper(md5($row['EditID']));

    // Set the hash so we don't have to repeat the do while statement all the time
    $query = "UPDATE udf_definitions SET hashid='" . $udfdefid . "' WHERE id=" . $row['EditID'];
    mysql_query($query) Or Die ("Cannot submit entry! ase1");

    $sql = "ALTER TABLE udf_" . $menutabdata[1] . " ADD " . columnnames($_POST['udflabel']) . " " . $_POST['columntype'] . " NULL;";
    //echo $sql;
    mysql_query($sql) or die ("Cannot alter table");

    // Lets build the relationship matrix
    if ($_POST['datalinktable'] != "") {
        $datalinkdata = explode("&&", $_POST['datalinktable']);

        $querysss = "SELECT MAX(sortorder) AS EditID FROM relations_matrix WHERE parenttable  = '" . $datalinkdata[0] . "'AND registrantid = " . RID;
        $resultsss = mysql_query($querysss);
        $rowsss = mysql_fetch_array($resultsss);
        $sortidsss = ($rowsss['EditID'] + 1);

        $relsql = "INSERT INTO relations_matrix (registrantid, menutabid, parenttable, childtable, ispredefined, labelhashid, udf_columnid, sortorder) VALUES (
					" . RID . ",
					'" . $menutabdata[0] . "',
					'" . $datalinkdata[0] . "',
					'" . $menutabdata[1] . "',
					0,
					'" . $udfdefid . "',
					'" . $datalinkdata[1] . "',
					" . $sortidsss . "
					)";
        mysql_query($relsql);

        $queryl = "SELECT MAX(id) AS EditID FROM relations_matrix";
        $resultl = mysql_query($queryl);
        $rowl = mysql_fetch_array($resultl);
        var_dump($rowl);
        die;

        $queryll = "UPDATE udf_definitions SET linkedid='" . $rowl['EditID'] . "' WHERE id=" . $row['EditID'];
        mysql_query($queryll) Or Die ("Cannot submit entry! 1uht");
    }
    header("Location: " . $_SERVER['PHP_SELF'] . "?id=14:" . $_POST['strredirect'] . "");

} elseif ($_POST['pform'] == 6) {

    // If it is a section header we want to set the
    // single row value to 1
    if ($_POST['udftype'] == 12) {
        $singlelargeval = 1;
        $singlerowval = 1;
    } elseif ($_POST['udftype'] == 11) {
        $singlelargeval = 0;
        $singlerowval = 1;
    } else {
        $singlelargeval = 0;
        $singlerowval = 0;
    }

    $menutabdata = explode("&&", $_POST['udfmodule']);
    $query = "UPDATE udf_definitions SET 
			 fieldtype = " . $_POST['udftype'] . ", 
			 label = '" . $_POST['udflabel'] . "',
			 labelmsg = '" . $_POST['labelmsg'] . "',
			 location = '" . $menutabdata[1] . "',
			 tablabel = '" . $_POST['tablabel'] . "',
			 singlerow = " . $singlerowval . ",
			 singlelarge = " . $singlelargeval . ",
			 inlists = '" . $_POST['inlists'] . "',
			 visibleonlyto = '" . $_POST['visibleonlyto'] . "'
			 WHERE hashid = '" . $_POST['hashid'] . "' 
			 AND registrantid = " . RID . "
			 ";
//    echo $query."<br><br>";
//    die;
    mysql_query($query) Or Die ("Cannot submit entry! 1254");

    $sql = "ALTER TABLE udf_" . $menutabdata[1] . " CHANGE " . $_POST['redundantcolumnname'] . " " . columnnames($_POST['udflabel']) . " " . $_POST['columntype'] . " NULL DEFAULT NULL";
    //echo $sql."<br><br>";
    mysql_query($sql);

    $datalinkdata = explode("&&", $_POST['datalinktable']);
    $relsql = "UPDATE relations_matrix SET 
					menutabid = '" . $menutabdata[0] . "',
					parenttable ='" . $datalinkdata[0] . "',
					childtable = '" . $menutabdata[1] . "',
					udf_columnid ='" . $datalinkdata[1] . "'
					WHERE id = " . $gval[3] . " 
			 		AND registrantid = " . RID . "
					";
    //echo $relsql."<br>";
    mysql_query($relsql);

    header("Location: " . $_SERVER['PHP_SELF'] . "?id=14:" . $_POST['strredirect'] . ":3");
} elseif ($_POST['pform'] == 7) {
    $totalupdatesort = $_POST['hiddensortvalues'];
    $getids = explode(",", $_POST['idarray']);
    for ($x = 1; $x <= $totalupdatesort; $x++) {
        if ($_POST['sortorder_' . $x] < $x) {
            $idkey = $getids[$x - 1];
            $lessthanorginal = 1;
            $startpoint = $x;
            break;
        } elseif ($_POST['sortorder_' . $x] > $x) {
            $idkey = $getids[$x - 1];
            $lessthanorginal = 2;
            $startpoint = $x;
            $endpoint = $_POST['sortorder_' . $x];
            break;
        }
    }

    function updatenumbering($id, $newnumber)
    {
        $sql = "UPDATE udf_definitions SET sortorder = " . $newnumber . " WHERE id = " . $id . "";
        //echo $sql."<br>";
        mysql_query($sql);
    }

    if ($lessthanorginal == 1) {
        for ($x = $_POST['sortorder_' . $x]; $x <= $totalupdatesort; $x++) {
            if ($x < $startpoint) {
                //echo $getids[$x-1]." - ".$x." becomes ".($_POST['sortorder_'.$x]+1)."<br>";
                updatenumbering($getids[$x - 1], ($_POST['sortorder_' . $x] + 1));
            } elseif ($x == $startpoint) {
                //echo $getids[$x-1]." - ".$x." becomes ".($_POST['sortorder_'.$x])."<br>";
                updatenumbering($getids[$x - 1], ($_POST['sortorder_' . $x]));
            } else {
                //echo $getids[$x-1]." - ".$x." becomes ".($_POST['sortorder_'.$x])."<br>";
                updatenumbering($getids[$x - 1], ($_POST['sortorder_' . $x]));
            }
        }
    }

    if ($lessthanorginal == 2) {
        //echo $startpoint."<br>";
        for ($x = $startpoint; $x <= $endpoint; $x++) {
            if ($x == $startpoint) {
                //echo $getids[$x-1]." - ".$x." becomes ".($_POST['sortorder_'.$x])."<br>";
                updatenumbering($getids[$x - 1], ($_POST['sortorder_' . $x]));
            } else {
                //echo $getids[$x-1]." - ".$x." becomes ".($_POST['sortorder_'.$x]-1)."<br>";
                updatenumbering($getids[$x - 1], ($_POST['sortorder_' . $x] - 1));
            }
        }
    }
}

if ($gval[0] == 2) {
    $query = "UPDATE menu_tabs SET isactive = 1 WHERE hashid = '" . $gval[1] . "' AND registrantid=" . RID;
    mysql_query($query) Or Die ("Cannot submit entry!gggd");

    // Active a sub level tab
    $query = "UPDATE menusub_tabs SET isactive = 1 WHERE menu_tabhashid  = '" . $gval[1] . "' AND registrantid=" . RID;
    //echo $query."<br>";
    mysql_query($query) Or Die ("Cannot submit entry!wqe");

    $queryl = "SELECT hashid FROM menusub_tabs WHERE menu_tabhashid = '" . $gval[1] . "' AND type = 'new' LIMIT 1";
    $resultl = mysql_query($queryl);
    $rowl = mysql_fetch_array($resultl);
    // Active a sub level tab
    $query = "UPDATE udf_definitions SET isactive = 1 WHERE location = '" . $rowl['hashid'] . "' AND registrantid=" . RID;
    //echo $query."<br>";
    mysql_query($query) Or Die ("Cannot submit entry!lop");

    $query = "UPDATE relations_matrix SET isactive = 1 WHERE childtable = '" . $rowl['hashid'] . "' AND registrantid=" . RID;
    // echo $query."<br>";
    mysql_query($query) Or Die ("pol");

    header("Location:" . $_SERVER['PHP_SELF'] . "?id=8:" . $gval[1]);
} elseif ($gval[0] == 3) {
    // Deactive a top level tab
    $query = "UPDATE menu_tabs SET isactive = 0 WHERE hashid = '" . $gval[1] . "' AND registrantid=" . RID;
    mysql_query($query) Or Die ("Cannot submit entry!ytr");
    // Deactive a sub level tab
    $query = "UPDATE menusub_tabs SET isactive = 0 WHERE menu_tabhashid = '" . $gval[1] . "' AND registrantid=" . RID;
    mysql_query($query) Or Die ("Cannot submit entry!bgr");
    $queryl = "SELECT hashid FROM menusub_tabs WHERE menu_tabhashid = '" . $gval[1] . "' AND type = 'new' LIMIT 1";
    $resultl = mysql_query($queryl);
    $rowl = mysql_fetch_array($resultl);
    // Active a sub level tab
    $query = "UPDATE udf_definitions SET isactive = 0 WHERE location = '" . $rowl['hashid'] . "' AND registrantid=" . RID;
    //echo $query."<br>";
    mysql_query($query) Or Die ("Cannot submit entry!xde");

    $query = "UPDATE relations_matrix SET isactive = 0 WHERE childtable = '" . $rowl['hashid'] . "' AND registrantid=" . RID;
    //echo $query."<br>";
    mysql_query($query) Or Die ("Cannot submit entry!edx");

    header("Location:" . $_SERVER['PHP_SELF'] . "?id=8:" . $gval[1]);
} elseif ($gval[0] == 5) {
    // Let's get the sort order value before deleting it so we can,
    // if required, update subsequent values.
    $sqlmulti = "SELECT type, sortorder FROM menu_tabs WHERE hashid  = '" . $gval[1] . "' AND registrantid=" . RID;
    //echo $sqlmulti."<br	>";
    $res = mysql_query($sqlmulti);
    $row = mysql_fetch_array($res);
    $deletedsortid = $row['sortorder'];
    $menutabtype = $row['type'];
    //echo $deletedsortid."<br>";

    $sqlmulti = "SELECT hashid FROM menusub_tabs WHERE menu_tabhashid = '" . $gval[1] . "' AND type='new' AND registrantid=" . RID;
    $res = mysql_query($sqlmulti);
    while ($row = mysql_fetch_array($res)) {
        $deletedsortidlocation = $row['hashid'];
        // Delete the related columns
        $specials = array(" ", ":", ";", "�", "'", "�", "!", "�", "$", "%", "^", "&", "*", "(", ")", "_", "_", "+", "=", "{", "}", "[", "]", "@", "'", "~", "#", "|", "\\", "<", ",", ">", "?", "/", ".");
        $deletedsortidlocation = str_replace(' ', ' ', str_replace($specials, '', $deletedsortidlocation));
        $sql = "DROP TABLE udf_" . $deletedsortidlocation . "";
        // echo $sql."<br><br>";
        mysql_query($sql);

        // Delete from udf definitions
        $sqludf = "DELETE FROM udf_definitions WHERE location ='" . $deletedsortidlocation . "' AND registrantid=" . RID;
        //echo $sqludf."<br>";
        mysql_query($sqludf) or die ('Dead4');
    }

    // Delete all the activites and related notes
    $sqludf = "DELETE FROM menu_tabs WHERE registrantid=" . RID . " AND hashid='" . $gval[1] . "' LIMIT 1";
    //echo $sqludf."<br><br>";
    mysql_query($sqludf) or die ('Dead 120');

    // Delete the top level tabs
    if ($menutabtype == "Calendar") {
        // Because activities are no longer attached to a single module we will need to
        // find a diffrenet way of cleaning these up..

        //$sqludf = "DELETE FROM activities WHERE registrantid=".RID." AND menutabid ='' AND location = ''";
        //echo $sqludf."<br><br>";
        //mysql_query($sqludf) or die ('Dead  121');
    } elseif ($menutabtype == "Standard Form") {
        // Because activities are no longer attached to a single module we will need to
        // find a diffrenet way of cleaning these up..

        ///$sqludf = "DELETE FROM activities WHERE registrantid=".RID." AND menutabid ='".$gval[1]."'";
        ///echo $sqludf."<br><br>";
        // mysql_query($sqludf) or die ('Dead 122');

        $sqludf = "DELETE FROM activities_notes WHERE menutabid ='" . $gval[1] . "' AND registrantid =" . RID;
        //echo $sqludf."<br><br>";
        mysql_query($sqludf) or die ('Dead 124');

        $sqludf = "DELETE FROM messaging WHERE menutabid ='" . $gval[1] . "' AND registrantid =" . RID;
        //echo $sqludf."<br><br>";
        mysql_query($sqludf) or die ('Dead 125');

        $sqludf = "DELETE FROM relations_matrix WHERE menutabid ='" . $gval[1] . "' AND registrantid =" . RID;
        //echo $sqludf."<br><br>";
        mysql_query($sqludf) or die ('Dead 126');

        $sqludf = "DELETE FROM relations_values WHERE menutabid ='" . $gval[1] . "' AND registrantid =" . RID;
        //echo $sqludf."<br><br>";
        mysql_query($sqludf) or die ('Dead 127');

        $sqludf = "DELETE FROM address_info WHERE menuhashid ='" . $gval[1] . "' AND registrantid =" . RID;
        //echo $sqludf."<br><br>";
        mysql_query($sqludf);

        $sqludf = "DELETE FROM udf_multi_value WHERE menutabid ='" . $gval[1] . "' AND registrantid =" . RID;
        //echo $sqludf."<br><br>";
        mysql_query($sqludf);

        $sqludf = "DELETE FROM relations_predefined  WHERE location ='" . $gval[1] . "' AND registrantid =" . RID;
        //echo $sqludf."<br><br>";
        mysql_query($sqludf);

        $sqludf = "DELETE FROM record_history WHERE location = '" . $gval[1] . "'";
        //echo $sqludf."<br><br>";
        mysql_query($sqludf);
    }

    // Delete the sub level tabs
    $sqludf = "DELETE FROM menusub_tabs WHERE menu_tabhashid ='" . $gval[1] . "' AND registrantid=" . RID;
    //echo $sqludf."<br><br>";
    mysql_query($sqludf) or die ('Dead2');

    // Before we delete the UDF data we need to remove all the
    // udf_multi_value data first
    $sqlmulti = "SELECT hashid FROM udf_definitions WHERE location  ='" . $gval[1] . "' AND fieldtype = 2 AND registrantid=" . RID;
    $res = mysql_query($sqlmulti);
    while ($row = mysql_fetch_array($res)) {
        // Delete from udf multi
        $sqludf = "DELETE FROM udf_multi_value WHERE udfdefid ='" . $row['hashid'] . "' AND registrantid=" . RID;
        //echo $sqludf."<br><br>";
        mysql_query($sqludf) or die ('Dead3');
    }

    // Update sortorder
    $sqlmulti = "SELECT MAX(sortorder) AS maxsortorder FROM menu_tabs WHERE registrantid=" . RID;
    $res = mysql_query($sqlmulti);
    $row = mysql_fetch_array($res);
    $deleteddiff = $row['maxsortorder'] - $deletedsortid;

    $newsortnum = ($deletedsortid + 1);
    for ($number = 0; $number < $deleteddiff; $number++) {
        $query = "UPDATE menu_tabs SET sortorder = " . $deletedsortid . " WHERE sortorder = " . $newsortnum . " AND registrantid=" . RID;
        //echo $query."<br><br>";
        mysql_query($query);
        $deletedsortid++;
        $newsortnum++;
    }

    header("Location:" . $_SERVER['PHP_SELF'] . "?id=::1");

} elseif ($gval[0] == 9) {
    // Move the value up the order
    $numtochange = ($gval[2] - 1);

    $query = "UPDATE menusub_tabs SET sortorder = " . $numtochange . " WHERE hashid = '" . $gval[1] . "' AND menu_tabhashid = '" . $gval[3] . "' AND registrantid=" . RID;
    // echo $query."<br>";
    mysql_query($query) Or Die ("Cannot submit entry!123w");

    $query = "UPDATE menusub_tabs SET sortorder = " . $gval[2] . " WHERE sortorder = " . $numtochange . " AND menu_tabhashid = '" . $gval[3] . "'AND hashid != '" . $gval[1] . "' AND registrantid=" . RID;
    // echo $query."<br>";
    mysql_query($query) Or Die ("Cannot submit entry!32w");

    header("Location:" . $_SERVER['PHP_SELF'] . "?id=8:" . $gval[3] . "");
} elseif ($gval[0] == 10) {
    // Move the value down the order
    $numtochange = ($gval[2] + 1);

    $query = "UPDATE menusub_tabs SET sortorder = " . $numtochange . " WHERE hashid = '" . $gval[1] . "' AND menu_tabhashid = '" . $gval[3] . "' AND registrantid=" . RID;
    // echo $query."<br>";
    mysql_query($query) Or Die ("Cannot submit entry!gtr");

    $query = "UPDATE menusub_tabs SET sortorder = " . $gval[2] . " WHERE sortorder = " . $numtochange . " AND hashid != '" . $gval[1] . "' AND menu_tabhashid = '" . $gval[3] . "' AND registrantid=" . RID;
    // echo $query."<br>";
    mysql_query($query) Or Die ("Cannot submit entry!456");

    header("Location:" . $_SERVER['PHP_SELF'] . "?id=8:" . $gval[3] . "");
} elseif ($gval[0] == 11) {
    // Active a sub level tab
    $query = "UPDATE menusub_tabs SET isactive = 1 WHERE hashid = '" . $gval[1] . "' AND registrantid=" . RID;
    mysql_query($query) Or Die ("Cannot submit entry!9op");
    header("Location:" . $_SERVER['PHP_SELF'] . "?id=8:" . $gval[2] . "");
} elseif ($gval[0] == 12) {
    // Deactive a sub level tab
    $query = "UPDATE menusub_tabs SET isactive = 0 WHERE hashid = '" . $gval[1] . "' AND registrantid=" . RID;
    mysql_query($query) Or Die ("Cannot submit entry!jyt");
    header("Location:" . $_SERVER['PHP_SELF'] . "?id=8:" . $gval[2] . "");
} elseif ($gval[0] == 15) {
    $query = "UPDATE udf_definitions SET isactive = 1 WHERE hashid = '" . $gval[4] . "' AND registrantid=" . RID;
    //echo $query;
    mysql_query($query) Or Die ("Cannot submit entry!cdr");
    header("Location:" . $_SERVER['PHP_SELF'] . "?id=14:" . $gval[1] . ":3");
} elseif ($gval[0] == 16) {
    $query = "UPDATE udf_definitions SET isactive = 0 WHERE hashid = '" . $gval[4] . "' AND registrantid=" . RID;
    mysql_query($query) Or Die ("Cannot submit entry!ploi");
    //echo $query;
    header("Location:" . $_SERVER['PHP_SELF'] . "?id=14:" . $gval[1] . ":3");
} elseif ($gval[0] == 17) {
    $query = "UPDATE udf_definitions SET iscompulsory = " . $gval[5] . " WHERE hashid = '" . $gval[4] . "' AND registrantid=" . RID;
    mysql_query($query) Or Die ("Cannot submit entry!zxsw");
    //echo $query;
    header("Location:" . $_SERVER['PHP_SELF'] . "?id=14:" . $gval[1] . ":3");
} elseif ($gval[0] == 18) {
    // Let's get the sort order value before deleting it so we can,
    // if required, update subsequent values.
    $sqlmulti = "SELECT sortorder, location, label FROM udf_definitions WHERE hashid  ='" . $gval[4] . "' AND registrantid=" . RID;
    $res = mysql_query($sqlmulti);
    $row = mysql_fetch_array($res);
    $deletedsortid = $row['sortorder'];
    $deletedsortidlocation = $row['location'];
    $deletecolumn = $row['label'];

    // Before we delete the UDF data we need to remove all the
    // udf_multi_value data first
    $sqlmulti = "SELECT hashid FROM udf_definitions WHERE location  ='" . $gval[4] . "' AND fieldtype = 2 AND registrantid=" . RID;
    $res = mysql_query($sqlmulti);
    while ($row = mysql_fetch_array($res)) {
        // Delete from udf multi
        $sqludf = "DELETE FROM udf_multi_value WHERE udfdefid ='" . $row['hashid'] . "' AND registrantid=" . RID;
        //echo $sqludf."<br>";
        mysql_query($sqludf) or die ('Dead1 129');
    }

    // Delete from udf definitions
    $sqludf = "DELETE FROM udf_definitions WHERE hashid ='" . $gval[4] . "' AND registrantid=" . RID . " LIMIT 1";
    //echo $sqludf."<br>";
    mysql_query($sqludf) or die ('Dead 130');

    $sqlmulti = "SELECT MAX(sortorder) AS maxsortorder FROM udf_definitions WHERE location  ='" . $deletedsortidlocation . "' AND registrantid=" . RID;
    $res = mysql_query($sqlmulti);
    $row = mysql_fetch_array($res);
    $deleteddiff = $row['maxsortorder'] - $deletedsortid;

    $newsortnum = ($deletedsortid + 1);
    // echo $newsortnum."<br>";
    for ($number = 0; $number < $deleteddiff; $number++) {
        $query = "UPDATE udf_definitions SET sortorder = " . $deletedsortid . " WHERE location  ='" . $deletedsortidlocation . "' AND sortorder = " . $newsortnum . " AND registrantid=" . RID;
        //echo $query."<br>";
        mysql_query($query) Or Die ("Cannot submit entry!bga");
        $deletedsortid++;
        $newsortnum++;
    }

    $specials = array(" ", ":", ";", "�", "'", "�", "!", "�", "$", "%", "^", "&", "*", "(", ")", "_", "_", "+", "=", "{", "}", "[", "]", "@", "'", "~", "#", "|", "\\", "<", ",", ">", "?", "/");
    $deletecolumn = str_replace(' ', ' ', str_replace($specials, '', $deletecolumn));

    // Delete the related columns
    $sql = "ALTER TABLE udf_" . $deletedsortidlocation . " DROP " . columnnames($deletecolumn) . "";
    //echo $sql."<br>";
    mysql_query($sql);

    if ($gval[5] > 0) {
        // Delete fields and value relationships
        $sqludf = "DELETE FROM relations_matrix WHERE labelhashid  ='" . $gval[4] . "' AND registrantid=" . RID . " LIMIT 1";
        //echo $sqludf."<br>";
        mysql_query($sqludf) or die ('Dead 131');

        $sqludf = "DELETE FROM relations_values  WHERE matrix_id =" . $gval[5] . " AND registrantid=" . RID;
        //echo $sqludf."<br>";
        mysql_query($sqludf) or die ('Dead 132');
    }

    header("Location: " . $_SERVER['PHP_SELF'] . "?id=14:" . $gval[1] . "");
} elseif ($gval[0] == 19) {
    // Move the value down the order
    $numtochange = ($gval[5] - 1);

    $query = "UPDATE udf_definitions SET sortorder = " . $numtochange . " WHERE hashid = '" . $gval[4] . "' AND registrantid=" . RID . " LIMIT 1";
    // echo $query."<br>";
    mysql_query($query) Or Die ("Cannot submit entry!lp;");

    $query = "UPDATE udf_definitions SET sortorder = " . $gval[5] . " WHERE sortorder = " . $numtochange . " AND location = '" . $gval[6] . "' AND hashid != '" . $gval[4] . "' AND registrantid=" . RID . " LIMIT 1";
    // echo $query."<br>";
    mysql_query($query) Or Die ("Cannot submit entry!zaq");

    header("Location:" . $_SERVER['PHP_SELF'] . "?id=14:" . $gval[1] . "");
} elseif ($gval[0] == 20) {
    // Move the value up the order
    $numtochange = ($gval[5] + 1);

    $query = "UPDATE udf_definitions SET sortorder = " . $numtochange . " WHERE hashid = '" . $gval[4] . "' AND registrantid=" . RID . " LIMIT 1";
    // echo $query."<br>";
    mysql_query($query) Or Die ("Cannot submit entry!asf");

    $query = "UPDATE udf_definitions SET sortorder = " . $gval[5] . " WHERE sortorder = " . $numtochange . " AND location = '" . $gval[6] . "' AND hashid != '" . $gval[4] . "' AND registrantid=" . RID . " LIMIT 1";
    // echo $query."<br>";
    mysql_query($query) Or Die ("Cannot submit entry!ghl");

    header("Location:" . $_SERVER['PHP_SELF'] . "?id=14:" . $gval[1] . "");
} elseif ($gval[0] == 21) {
    $query = "UPDATE udf_definitions SET inlists = " . $gval[5] . " WHERE hashid = '" . $gval[4] . "' AND registrantid=" . RID;
    mysql_query($query) Or Die ("Cannot submit entry!npe");
    //echo $query;
    header("Location:" . $_SERVER['PHP_SELF'] . "?id=14:" . $gval[1] . ":3");
} elseif ($gval[0] == 23) {
    $query = "UPDATE menusub_tabs SET icon = '' WHERE hashid = '" . $gval[2] . "' AND registrantid=" . RID;
    //echo $query;
    mysql_query($query) Or Die ("Cannot submit entry!zeft");
    header("Location:" . $_SERVER['PHP_SELF'] . "?id=8:" . $gval[1]);
} elseif ($gval[0] == 24) {
    // Move the value up the order
    $numtochange = ($gval[2] - 1);

    $query = "UPDATE relations_matrix SET sortorder = " . $numtochange . " WHERE id = '" . $gval[1] . "' AND parenttable = '" . $gval[4] . "' LIMIT 1";
    //echo $query."<br>";
    mysql_query($query) Or Die ("Cannot submit entry!brty");

    $query = "UPDATE relations_matrix SET sortorder = " . $gval[2] . " WHERE sortorder = " . $numtochange . " AND id != '" . $gval[1] . "'  AND parenttable = '" . $gval[4] . "' LIMIT 1";
    //echo $query."<br>";
    mysql_query($query) Or Die ("Cannot submit entry!o7yt");

    header("Location:" . $_SERVER['PHP_SELF'] . "?id=8:" . $gval[3] . "");
} elseif ($gval[0] == 25) {
    // Move the value down the order
    $numtochange = ($gval[2] + 1);

    $query = "UPDATE relations_matrix SET sortorder = " . $numtochange . " WHERE id = '" . $gval[1] . "'  AND parenttable = '" . $gval[4] . "' LIMIT 1";
    //echo $query."<br>";
    mysql_query($query) Or Die ("Cannot submit entry!vddd");

    $query = "UPDATE relations_matrix SET sortorder = " . $gval[2] . " WHERE sortorder = " . $numtochange . " AND id != '" . $gval[1] . "' AND parenttable = '" . $gval[4] . "' LIMIT 1";
    //echo $query."<br>";
    mysql_query($query) Or Die ("Cannot submit entry!gggg");

    header("Location:" . $_SERVER['PHP_SELF'] . "?id=8:" . $gval[3] . "");
} elseif ($gval[0] == 26) {
    // Delete the sub level tabs
    $sqludf = "DELETE FROM relations_matrix WHERE id ='" . $gval[1] . "' AND parenttable = '" . $gval[4] . "' LIMIT 1";
    mysql_query($sqludf) or die ('Dead2');

    $deletedsortid = $gval[2];

    //updatesortorder("relations_matrix");
    $sqlmulti = "SELECT MAX(sortorder) AS maxsortorder FROM relations_matrix WHERE parenttable = '" . $gval[4] . "' AND registrantid=" . RID;
    //echo $sqlmulti."<br>";
    $res = mysql_query($sqlmulti);
    $row = mysql_fetch_array($res);
    $deleteddiff = $row['maxsortorder'] - $deletedsortid;

    $newsortnum = ($deletedsortid + 1);
    for ($number = 0; $number < $deleteddiff; $number++) {
        $query = "UPDATE relations_matrix SET sortorder = " . $deletedsortid . " WHERE sortorder = " . $newsortnum . " AND parenttable = '" . $gval[4] . "' AND registrantid=" . RID;
        echo $query . "<br>";
        mysql_query($query);
        $deletedsortid++;
        $newsortnum++;
    }

    header("Location:" . $_SERVER['PHP_SELF'] . "?id=8:" . $gval[3] . "");
} elseif ($gval[0] == 27) {
    $query = "UPDATE menu_tabs SET label = '" . $_POST['newlabel'] . "' WHERE hashid = '" . $gval[1] . "' AND registrantid = " . RID;
    //echo $query."<br>";
    mysql_query($query) Or Die ("Cannot submit entry!mkiu");

    // Lets check if we have a predefined value that has the same label.  We will need to change that too.
    $query = "UPDATE relations_predefined SET label = '" . $_POST['newlabel'] . "' WHERE location = '" . $gval[1] . "' AND registrantid = " . RID;
    // echo $query."<br>";
    mysql_query($query);

    header("Location:" . $_SERVER['PHP_SELF'] . "?id=8:" . $gval[1] . ":1");
} elseif ($gval[0] == 29) {
    // Lets update the master tag first
    $masterupdate = "UPDATE udf_definitions SET label = '" . $_POST['newlabel'] . "' WHERE hashid = '" . $gval[1] . "' AND registrantid = " . RID;
    //echo $query."<br>";
    mysql_query($masterupdate) Or Die ("Cannot submit entry!tre");

    // And now all the slave labels
    $masterupdate = "UPDATE udf_definitions SET label = '" . $_POST['newlabel'] . "' WHERE object_master_id = '" . $gval[1] . "' AND registrantid = " . RID;
    //echo $query."<br>";
    mysql_query($masterupdate) Or Die ("Cannot submit entry!zswqq");

    header("Location:" . $_SERVER['PHP_SELF'] . "?id=28:" . $gval[1] . ":1");
} elseif ($gval[0] == 30) {
    // set the element as a component
    $query = "UPDATE menusub_tabs SET iscomponent = 1 WHERE hashid = '" . $gval[2] . "' AND registrantid = " . RID;
    //echo $query."<br>";
    mysql_query($query) Or Die ("Cannot submit entry!qqq");

    $sqlmulti = "SELECT label FROM menu_tabs WHERE hashid = '" . $gval[1] . "' AND registrantid = " . RID;
    // echo $sqlmulti."<br>";
    $res = mysql_query($sqlmulti);
    $row = mysql_fetch_array($res);

    $relsql = "INSERT INTO relations_predefined (registrantid, label, location, productcomponent) VALUES (
				" . RID . ",
				'" . $row['label'] . "',
				'" . $gval[1] . "',
				'" . $gval[3] . "'
				)";
    mysql_query($relsql);


    // We need to make sure that they dont generate fields which already exist in this for, so we run a little check.
    $sqlmulti = "SELECT label FROM udf_definitions WHERE location = '" . $gval[2] . "' AND registrantid=" . RID;
    // echo $sqlmulti."<br>";
    $res = mysql_query($sqlmulti);
    $entryarray = array();
    while ($row = mysql_fetch_array($res)) {
        array_push($entryarray, columnnames($row['label']));
    }

    // uses function(generate elements)
    // fieldtype, columntype, label, location, iscompulsory, inlists, islinked, singlerow, singlelarge, iscomponent, componentlabel, isactive
    if (!in_array(columnnames("Product Name"), $entryarray)) {
        generateFieldElements(12, 'VARCHAR(350)', 'Product Name', $gval[2], 1, 1, 0, 1, 1, 1, 'Product Name', 1); // for Product Name
    }
    if (!in_array(columnnames("Product Code"), $entryarray)) {
        generateFieldElements(1, 'VARCHAR(50)', 'Product Code', $gval[2], 0, 1, 0, 0, 0, 1, 'Product Code', 1); // for Description Field
    }
    if (!in_array(columnnames("Price Information"), $entryarray)) {
        generateFieldElements(11, 'VARCHAR(150)', 'Price Information', $gval[2], 0, 0, 0, 1, 0, 1, 'Cost Price', 1); // for Pricing Section Header
    }
    if (!in_array(columnnames("Cost Price"), $entryarray)) {
        generateFieldElements(13, 'DECIMAL(11,2)', 'Cost Price', $gval[2], 0, 1, 0, 0, 0, 1, 'Cost Price', 1); // for Pricing Section Header
    }
    if (!in_array(columnnames("Sales Price"), $entryarray)) {
        generateFieldElements(13, 'DECIMAL(11,2)', 'Sales Price', $gval[2], 0, 1, 0, 0, 0, 1, 'Sales Price', 1); // for Pricing Section Header
    }
    if (!in_array(columnnames("Account Code"), $entryarray)) {
        generateFieldElements(2, 'VARCHAR(350)', 'Account Code', $gval[2], 0, 1, 0, 0, 0, 0, 'Account Code', 1); // for Account Code
    }
    if (!in_array(columnnames("Tax Code"), $entryarray)) {
        generateFieldElements(2, 'VARCHAR(350)', 'Tax Code', $gval[2], 0, 1, 0, 0, 0, 0, 'Tax Code', 1); // for Tax Code
    }

    // We need to create a sales and purchases table for this customer
    $sql = "CREATE TABLE 'sales_" . RID . "' (
  			'id' int(11) NOT NULL auto_increment,
  			'registrantid' int(11) NOT NULL,
			'location' varchar(350) NOT NULL,
  			'linkedorderid' varchar(350) NOT NULL,
			'productname' varchar(350) NOT NULL,
  			'productcode' varchar(150) NOT NULL,
  			'salesprice' decimal(10,2) NOT NULL,
  			'costprice' decimal(10,2) default NULL,
  			'taxcode' varchar(350) NOT NULL,
  			'accountcode' varchar(250) default NULL,
  			'discount' decimal(10,2) default '0.00',
  			'quantity' int(11) default '1',
  			'datecreated' datetime NOT NULL,
  			PRIMARY KEY  ('id')
			) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
    mysql_query($sql);

    $sql = "CREATE TABLE 'purchases_" . RID . "' (
		 	'id' int(11) NOT NULL auto_increment,
  			'registrantid' int(11) NOT NULL,
			'location' varchar(350) NOT NULL,
  			'linkedorderid' varchar(350) NOT NULL,
  			'productname' varchar(350) default NULL,
  			'productcode' varchar(150) default NULL,
  			'salesprice' decimal(10,2) default '0.00',
  			'costprice' decimal(10,2) default NULL,
  			'taxcode' varchar(350) default NULL,
  			'accountcode' varchar(250) default NULL,
  			'received' int(11) default '0',
  			'discount' decimal(10,2) default '0.00',
  			'quantity' int(11) default '1',
  			'datecreated' datetime NOT NULL,
  			PRIMARY KEY  ('id')
			) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
    mysql_query($sql);

    header("Location:" . $_SERVER['PHP_SELF'] . "?id=8:" . $gval[1] . "");
} elseif ($gval[0] == 31) {
    // removbe the element as a component
    $query = "UPDATE menusub_tabs SET iscomponent = 0 WHERE hashid = '" . $gval[2] . "' AND registrantid = " . RID;
    //echo $query."<br>";
    mysql_query($query) Or Die ("Cannot submit entry!");

    $sqlmulti = "SELECT id FROM relations_predefined WHERE location = '" . $gval[1] . "' AND registrantid = " . RID;
    // echo $sqlmulti."<br>";
    $res = mysql_query($sqlmulti);
    $row = mysql_fetch_array($res);

    // delete the predefined entry
    $sqludf = "DELETE FROM relations_predefined WHERE id = " . $row['id'] . " AND registrantid=" . RID . " LIMIT 1";
    //echo $sqludf."<br>";
    mysql_query($sqludf) or die ('Dead 131');

    // delete the predefined entry
    $sqludf = "DELETE FROM relations_matrix WHERE childtable = " . $row['id'] . " AND registrantid=" . RID;
    //echo $sqludf."<br>";
    mysql_query($sqludf);

    // delete the sales table
    $sqludf = "DROP TABLE sales_" . RID . "";
    //echo $sqludf."<br>";
    mysql_query($sqludf);

    // delete the purchases table
    $sqludf = "DROP TABLE purchases_" . RID . "";
    //echo $sqludf."<br>";
    mysql_query($sqludf);

    header("Location:" . $_SERVER['PHP_SELF'] . "?id=8:" . $gval[1] . "");
} elseif ($gval[0] == 32) {
    // set the element as a component
    $query = "UPDATE menusub_tabs SET componentabtype  = " . $gval[2] . " WHERE menu_tabhashid = '" . $gval[1] . "' AND type = 'new' AND registrantid = " . RID;
    //echo $query."<br>";
    mysql_query($query) Or Die ("Cannot submit entry! 202368");

    // Also add the required field to this table
    if ($gval[4] == 1) {
        $sql = "ALTER TABLE udf_" . $gval[3] . " ADD tlsrecidinvoiced TINYINT NULL AFTER lastupdateddate ,
		ADD tlsrecidinvoiceddate DATE NULL AFTER tlsrecidinvoiced, 
		ADD tlsinvoiceterms INT NULL AFTER tlsrecidinvoiceddate,
		ADD tlsrecidinvoiceispaid INT NULL DEFAULT '0' AFTER tlsinvoiceterms;";
        mysql_query($sql);
    } elseif ($gval[4] == 2) {
        $sql = "ALTER TABLE udf_" . $gval[3] . " ADD tlsrecidinvoiced TINYINT NULL DEFAULT '0' AFTER lastupdateddate ,
				ADD supplierinvoiceamountexvat DECIMAL( 10, 2 ) NULL AFTER tlsrecidinvoiced ,
				ADD supplierinvoiceamountincvat DECIMAL( 10, 2 ) NULL AFTER supplierinvoiceamountexvat ,
				ADD tlsrecidinvoiceddate DATE NULL AFTER tlsrecidinvoiced ,
				ADD tlsinvoiceterms INT NULL AFTER tlsrecidinvoiced,
				ADD tlsrecidinvoiceispaid INT NULL DEFAULT '0' AFTER tlsinvoiceterms ;";
        // echo $sql;
        mysql_query($sql) Or Die ("Cannot submit entry! 6985");
    } elseif ($gval[4] == 3) {
        $sql = "ALTER TABLE udf_" . $gval[3] . " DROP tlsrecidinvoiced ,
				DROP supplierinvoiceamountexvat ,
				DROP supplierinvoiceamountincvat ,
				DROP tlsinvoiceterms ,
				DROP tlsrecidinvoiceddate ,
				DROP tlsrecidinvoiceispaid ;";
        echo $sql;
        mysql_query($sql) Or Die ("Cannot submit entry! 6986");
    } elseif ($gval[4] == 4) {
        $sql = "ALTER TABLE udf_" . $gval[3] . " DROP tlsrecidinvoiced ,
				DROP tlsinvoiceterms ,
				DROP tlsrecidinvoiceddate,
				DROP tlsrecidinvoiceispaid ;";
        //echo $sql;
        mysql_query($sql) Or Die ("Cannot submit entry! 6987");
    }
    // echo $sql;
    header("Location:" . $_SERVER['PHP_SELF'] . "?id=8:" . $gval[1] . "");
} elseif ($gval[0] == 33) {
    $sql = "ALTER TABLE `udf_" . $gval[6] . "` ADD INDEX ( `" . $gval[7] . "` ) ";
    //echo $sql;
    mysql_query($sql) Or Die ("Cannot submit entry! 6fdg987");
    header("Location:" . $_SERVER['PHP_SELF'] . "?id=14:" . $gval[1] . "");
} elseif ($gval[0] == 35) {
    $sql = "ALTER TABLE `udf_" . $gval[6] . "` DROP INDEX  `" . $gval[7] . "`  ";
    //echo $sql;
    mysql_query($sql) Or Die ("Cannot submit entry! 69ergt87");
    header("Location:" . $_SERVER['PHP_SELF'] . "?id=14:" . $gval[1] . "");
} elseif ($gval[0] == 36) {
    $sql = "INSERT INTO  `playball`.`relations_predefined`
		(`registrantid` ,`label` ,`location` ,`productcomponent` ,`comtype`,`ismultilink`)
	VALUES ('" . RID . "',  '" . $gval[2] . "',  '" . $gval[1] . "',  '0', NULL, '1');";
    //echo $sql;
    mysql_query($sql) Or Die ("Cannot submit entry! 69erg45t87");
    header("Location:" . $_SERVER['PHP_SELF'] . "?id=8:" . $gval[1] . "");
} elseif ($gval[0] == 37) {
    $sql = "SELECT id FROM `relations_predefined` WHERE location = '" . $gval[1] . "';";
    $res = mysql_query($sql);
    $row = mysql_fetch_array($res);

    $sql = "DELETE FROM `relations_matrix` WHERE childtable = '" . $row['id'] . "';";
    //echo $sql;
    mysql_query($sql) Or Die ("Cannot submit entry! 645zt87");

    $sql = "DELETE FROM `relations_predefined` WHERE location = '" . $gval[1] . "';";
    //echo $sql;
    mysql_query($sql) Or Die ("Cannot submit entry! 645t87");

    // Delete records
    $sql = "SELECT hashid FROM  `menusub_tabs` WHERE  `menu_tabhashid` =  '" . $gval[1] . "' AND label =  'new' LIMIT 1";
    $res = mysql_query($sql);
    $row = mysql_fetch_array($res);

    $sql = "DELETE FROM `multilink` WHERE `parent_table` LIKE '" . $row['hashid'] . "'";
    mysql_query($sql) Or Die ("Cannot submit entry! 645trerwx87");

    header("Location:" . $_SERVER['PHP_SELF'] . "?id=8:" . $gval[1] . "");
} elseif ($gval[0] == 38) {
    // lock a dropdown field
    $masterupdate = "UPDATE udf_definitions SET locked = '0' WHERE hashid = '" . $gval[4] . "' AND registrantid = " . RID;
    //echo $masterupdate."<br>";
    mysql_query($masterupdate) Or Die ("Cannot submit entry!nnnn");
    header("Location:" . $_SERVER['PHP_SELF'] . "?id=14:" . $gval[1] . "");
} elseif ($gval[0] == 39) {
    // lock a dropdown field
    $masterupdate = "UPDATE udf_definitions SET locked = '1' WHERE hashid = '" . $gval[4] . "' AND registrantid = " . RID;
    //echo $masterupdate."<br>";
    mysql_query($masterupdate) Or Die ("Cannot submit entry!cdrr");
    header("Location:" . $_SERVER['PHP_SELF'] . "?id=14:" . $gval[1] . "");
} elseif ($gval[0] == 40) {
    // lock a dropdown field
    $masterupdate = "UPDATE udf_definitions SET sharedvalues = '1' WHERE hashid = '" . $gval[4] . "' AND registrantid = " . RID;
    //echo $masterupdate."<br>";
    mysql_query($masterupdate) Or Die ("Cannot submit entry!njuy");
    header("Location:" . $_SERVER['PHP_SELF'] . "?id=14:" . $gval[1] . "");
} elseif ($gval[0] == 41) {
    // lock a dropdown field
    $masterupdate = "UPDATE udf_definitions SET sharedvalues = '0' WHERE hashid = '" . $gval[4] . "' AND registrantid = " . RID;
    //echo $masterupdate."<br>";
    mysql_query($masterupdate) Or Die ("Cannot submit entry!aaswer");
    header("Location:" . $_SERVER['PHP_SELF'] . "?id=14:" . $gval[1] . "");
} elseif ($gval[0] == 42) {
    // Enable or disable create record for module
    // lock a dropdown field
    $masterupdate = "UPDATE menu_tabs SET `can_create_records` = '" . mysql_real_escape_string($_GET['action']) . "' WHERE hashid = '" . $gval[1] . "' AND registrantid = " . RID;
    echo $masterupdate . "<br>";
    mysql_query($masterupdate) Or Die ("Cannot submit entry!fghgbf");
    header("Location:" . $_SERVER['PHP_SELF'] . "?id=8:" . $gval[1] . "");
} elseif ($gval[0] == 45) {
    $masterupdate = "UPDATE udf_definitions SET showonnew = '0' WHERE hashid = '" . $gval[4] . "' AND registrantid = " . RID;
    //echo $masterupdate."<br>";
    mysql_query($masterupdate) Or Die ("Cannot submit entry!dfhhh");
    header("Location:" . $_SERVER['PHP_SELF'] . "?id=14:" . $gval[1] . "");
} elseif ($gval[0] == 46) {
    $masterupdate = "UPDATE udf_definitions SET showonnew = '1' WHERE hashid = '" . $gval[4] . "' AND registrantid = " . RID;
    //echo $masterupdate."<br>";
    mysql_query($masterupdate) Or Die ("Cannot submit entry!sgfhhjh");
    header("Location:" . $_SERVER['PHP_SELF'] . "?id=14:" . $gval[1] . "");
} elseif ($gval[0] == 47) {
    $masterupdate = "UPDATE udf_definitions SET caneditvalues = '0' WHERE hashid = '" . $gval[4] . "' AND registrantid = " . RID;
    //echo $masterupdate."<br>";
    mysql_query($masterupdate) Or Die ("Cannot submit entry!ghjjfd");
    header("Location:" . $_SERVER['PHP_SELF'] . "?id=14:" . $gval[1] . "");
} elseif ($gval[0] == 48) {
    $masterupdate = "UPDATE udf_definitions SET caneditvalues = '1' WHERE hashid = '" . $gval[4] . "' AND registrantid = " . RID;
    echo $masterupdate . "<br>";
    mysql_query($masterupdate) Or Die ("Cannot submit entry!dghhjj");
    header("Location:" . $_SERVER['PHP_SELF'] . "?id=14:" . $gval[1] . "");
}

// a function for generating a string of udf elements
// and then ltering their respective tables
function generateFieldElements($fieldtype, $columntype, $label, $location, $iscompulsory, $inlists, $islinked, $singlerow, $singlelarge, $iscomponent, $componentlabel, $isactive)
{

    $query = "SELECT MAX(sortorder) AS EditID FROM udf_definitions WHERE location = '" . $location . "' AND registrantid = " . RID;
    $result = mysql_query($query);
    $row = mysql_fetch_array($result);
    $sortid = ($row['EditID'] + 1);

    $query = "INSERT INTO udf_definitions (registrantid, fieldtype, columntype, label, location, sortorder, iscompulsory, inlists, islinked, singlerow, singlelarge, is_component, component_label, isactive) VALUES (
			" . RID . ",
			" . $fieldtype . ",
			'" . $columntype . "',
			'" . $label . "',
			'" . $location . "',
			" . $sortid . ",
			'" . $iscompulsory . "',
			'" . $inlists . "',
			0,
			" . $singlerow . ",
			" . $singlelarge . ",
			" . $iscomponent . ",
			'" . $componentlabel . "',
			" . $isactive . "
			)";
    echo $query . "<br><br>";
    mysql_query($query) or die ("Cannot create user defined field");

    $query = "SELECT MAX(id) AS EditID FROM udf_definitions";
    $result = mysql_query($query);
    $row = mysql_fetch_array($result);
    $udfdefid = strtoupper(md5($row['EditID']));

    // Set the hash so we don't have to repeat the do while statement all the time
    $query = "UPDATE udf_definitions SET hashid='" . $udfdefid . "' WHERE id=" . $row['EditID'];
    // echo $query."<br><br>";
    mysql_query($query) Or Die ("Cannot submit entry! 1");

    // Make sure we dont try an alter for headers
    $sql = "ALTER TABLE udf_" . $location . " ADD " . columnnames($label) . " " . $columntype . " NULL;";
    echo $sql;
    mysql_query($sql) or die ("Cannot alter table");

}

?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>Thinline Software</title>
    <script type="text/javascript" language="javascript" src="../lytebox/lytebox.js"></script>
    <link rel="stylesheet" href="../lytebox/lytebox.css" type="text/css" media="screen"/>
    <script src="../js/prototype.js" type="text/javascript"></script>
    <script src="../js/scriptaculous.js?load=effects,builder" type="text/javascript"></script>
    <style type="text/css" media="all">@import "css/main_body.css";</style>
</head>
<style type="text/css">

</style>
<body>
<?php
if ($gval[0] == "") {
// if this is a newly created module then we need to refresh the select_menu frame
if ($gval[2] == 1) { ?>
    <script language="Javascript">
        var reloadselect_menu = parent.select_menu.location.reload();
    </script>
<?php
} ?>
    <table width="100%" border="0">
        <tr>
            <td colspan="2"><img src="../images/spacer.gif" height="220" width="1"></td>
        </tr>
        <tr>
            <td width="15%" align="center">&nbsp;</td>
            <td width="7%" align="center"><img src="../images/box_tall.png"></td>
            <td align="left"><h1>Thinline Software - App Builder</h1>Please select a menu option from the top or left
                menus....
            </td>
        </tr>
    </table>
    <?php
}
if ($gval[0] == 8) { ?>
    <script type="text/javascript">
        parent.frames['select_menu'].location.reload();
    </script>
    <?php
} ?>
<?php
if (($gval[0] == 1) || ($gval[0] == 4)) { ?>
    <script language="JavaScript" type="text/javascript">
        <!--
        function checkform(form) {
            if ((form.type.value == "") || (form.label.value == "")) {
                alert("Please complete all fields");
                form.type.focus();
                return false;
            }
            return true;
        }

        function displayOption(fieldname) {
            var showOpt = document.getElementById('ShowMulti');
            if (document.form.type.value == "Standard Form") {
                showOpt.style.display = '';
            } else {
                showOpt.style.display = 'none';
            }
        }

        function displayOption2(fieldname) {
            var showOpt = document.getElementById('ShowNumSeries');
            var showOpt2 = document.getElementById('ShowNumSeriesMSG');
            if (document.form.subcategories.value == 2) {
                showOpt.style.display = 'none';
                showOpt2.style.display = '';
            } else {
                showOpt.style.display = '';
                showOpt2.style.display = 'none';
            }
        }

        //-->
    </script>
<?php
if ($gval[0] == 1) {
    $labelval = "Create New";
    $labelbutton = "Create Module";
    $pformvalue = 1;
} elseif ($gval[0] == 4) {
    $labelval = "Edit";
    $labelbutton = "Edit Module";
    $pformvalue = 2;
    $sql = "SELECT label, type FROM menu_tabs WHERE hashid = '" . $gval[1] . "' AND registrantid=" . RID;
    $res = mysql_query($sql);
    $row = mysql_fetch_array($res);
    $editlabel = $row['label'];
    $edittype = $row['type'];
    $taburl = '<a href="' . $_SERVER['PHP_SELF'] . '?id=1">Create New Module</a>';
    $hidddenhash = '<input type="hidden" name="hashid" value="' . $gval[1] . '">';
} ?>
    <form method="POST" name="form" action="<?php echo $_SERVER['PHP_SELF'] ?>?id=1" onsubmit="return checkform(this);">
        <table cellpadding="5" cellspacing="3" border="0" width="95%">
            <tr>
                <td colspan="4"><h1><?php echo $labelval; ?> Module</h1></td>
            </tr>
            <tr>
                <td class="ls_on" width="15%">Label:</td>
                <td class="ls_on" width="35%"><input type="text" name="label" class="standardfield"
                                                     value="<?php echo $editlabel; ?>"> <font color="red">*</font></td>
                <td class="ls_on">Module Type:</td>
                <td class="ls_on">
                    <select name="type" class="standardselect" onchange="displayOption(this);">
                        <option value="">Select One</option>
                        <?php
                        $sql = "SELECT label FROM menu_tabs_types ORDER BY label ASC";
                        $res = mysql_query($sql);
                        while ($row = mysql_fetch_array($res)) {
                            if ($row['label'] == $edittype) {
                                $typeselected = "selected";
                            } ?>
                            <option <?php echo $typeselected; ?>
                                    value="<?php echo $row['label']; ?>"><?php echo $row['label']; ?></option>
                            <?php
                            unset($typeselected);
                        } ?>
                    </select> <font color="red">*</font>
                </td>
            </tr>
        </table>
        <!---->
        <span id="ShowMulti" style="display:none"><br/>
            <!--<table cellpadding="5" cellspacing="3" border="0" width="95%">
            <tr>
                <td class="ls_on" width="15%">Sub Categories:</td>
                <td class="ls_on" width="35%">
                    <select name="subcategories" class="standardselect" onchange="displayOption2(this);">
                        <option value="1">1</option>
                        <option value="2">2</option>
                    </select>
                </td>
                <td class="ls_on" >
                    <font class="note">For example if you created a Top Level of Customers, you might have<br> two sub-categories; 1 being Companies and another being Contacts.<br>
                    If you select 1, the Module label you create will be the sub-category.</font>
                </td>
            </tr>
            </table>-->
            <span id="ShowNumSeries">
            <table cellpadding="5" cellspacing="3" border="0" width="95%">
            <tr>
                <td class="ls_on" width="15%">Number Series:</td>
                <td class="ls_on" width="35%"><input type="text" name="numberseries" class="standardfield_ns"
                                                     value="100"></td>
                <td class="ls_on"><font class="note">Please enter a number for this modules number series.</font></td>
            </tr>
            </table>
            </span>
            <span id="ShowNumSeriesMSG" style="display:none">
            <table cellpadding="5" cellspacing="3" border="0" width="95%">
            <tr>
                <td class="ls_on" width="15%">
                    You will be prompted on the following screen to submit the sub category labels and number series for each
                    individual sub category.
                </td>
            </tr>
            </table>
            </span>
        </span><br/>
        <input type="hidden" name="subcategories" value="1">
        <input type="submit" value="<?php echo $labelbutton; ?>">
        <input type="hidden" name="pform" value="<?php echo $pformvalue; ?>">
        <?php echo $hidddenhash; ?>
    </form>
    <?php echo $taburl; ?><br>
<?php
}
elseif ($gval[0] == 8) {
$sqlgetnum = "SELECT label, type, isactive, hashid, can_delete, can_create_records FROM menu_tabs WHERE hashid = '" . $gval[1] . "' AND registrantid=" . RID;
//echo $sqlgetnum."<br>";
$resgetnum = mysql_query($sqlgetnum);
$rowgetnum = mysql_fetch_array($resgetnum);
$elemtype = $rowgetnum['type'];
$top_level_label = $rowgetnum['label'];
$can_delete = $rowgetnum['can_delete'];
$can_create_records = $rowgetnum['can_create_records'];

// if this is a newly created module then we need to refresh the select_menu frame
if ($gval[2] == 1) { ?>
    <script language="Javascript">
        var reloadselect_menu = parent.select_menu.location.reload();
    </script>
<?php
} ?>

    <table cellpadding="5" cellspacing="5" border="0" width="95%">
        <form method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?>?id=27:<?php echo $rowgetnum['hashid'] ?>">
            <tr>
                <td width="25%">
                    <?php
                    if ($gval[4] == 2) { ?>
                        <input type="text" name="newlabel" class="standardfield_large"
                               value="<?php echo $rowgetnum['label'] ?>">
                        <?php
                    } else { ?>
                        <h1><?php echo $sectionlabel = $rowgetnum['label'] ?></h1>
                        <?php
                    } ?>
                </td>
                <?php
                if ($rowgetnum['isactive'] == 0) {
                    $newclass = "classactive";
                } else {
                    $newclass = "classdeactive";
                } ?>
                <?php
                if ($gval[4] == 2) { ?>
                    <td><input type="submit" value="Save Change"></td>
                    <?php
                } else { ?>
                    <td width="2%" class="<?php echo $newclass; ?>">&nbsp;</td>
                    <td>
                        <?php
                        if ($rowgetnum['isactive'] == 0) { ?>
                            <a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=2:<?php echo $rowgetnum['hashid'] ?>">Enable</a>
                            <?php
                        } else { ?>
                            <a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=3:<?php echo $rowgetnum['hashid'] ?>">Disable</a>
                            <?php
                        } ?>
                    </td>
                    <?php
                } ?>
                <td width="25%">&nbsp;</td>
                <td class="messageboard" width="15%" align="center">
                    <a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=8:<?php echo $rowgetnum['hashid'] ?>:::2">Change
                        Label</a>
                </td>

                <?php
                if ($can_create_records == "Yes") { ?>
            <td class="classdeactive" width="15%" align="center">
            <a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=42:<?php echo $rowgetnum['hashid'] ?>:::2&action=No">Add
                Records</a>
            <?php
            } else { ?>
                <td class="classactive" width="15%" align="center">
                    <a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=42:<?php echo $rowgetnum['hashid'] ?>:::2&action=Yes">Add
                        Records</a>
                    <?php
                    } ?>
                </td>
                <?php
                if ($can_delete == 1) { ?>
                    <td class="classactive" width="15%" align="center">
                        <a onclick="return confirm('WARNING\nAre you sure you want to delete this module item? This action cannot be undone.\nAll data related to this module will also be deleted.')"
                           href="<?php echo $_SERVER['PHP_SELF'] ?>?id=5:<?php echo $rowgetnum['hashid'] ?>:2">Delete</a>
                    </td>
                    <?php
                } ?>
            </tr>

    </table><br/>
    <table cellpadding="2" cellspacing="3" width="95%">
        <?php
        if ($gval[4] == 1) { ?>
        <form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
            <?php
            } ?>
            <tr>
                <td class="ls_top" align="center"><strong>Label</strong></td>
                <td class="ls_top" align="center"><strong>Type</strong></td>
                <td class="ls_top" align="center"><strong>Sort Order</strong></td>
                <td colspan="2" class="ls_top" align="center" width="15%"><strong>Actions</strong></td>
            </tr>
            <?php
            $sql = "SELECT * FROM menusub_tabs WHERE menu_tabhashid = '" . $gval[1] . "' ORDER BY sortorder ASC";
            $res = mysql_query($sql);
            $sqlgetnum = "SELECT MAX(sortorder) AS highestnumber FROM menusub_tabs WHERE menu_tabhashid = '" . $gval[1] . "'";
            $resgetnum = mysql_query($sqlgetnum);
            $rowgetnum = mysql_fetch_array($resgetnum);
            $highest = $rowgetnum['highestnumber'];
            $numcounter = 1;
            while ($row = mysql_fetch_array($res)) {
                include("..//includes/ls.php"); ?>
                <tr>
                    <td class="ls_<?php echo $ls; ?>">
                        <?php
                        if ($gval[4] != 1) {
                            echo $row['label'];
                        } elseif (($gval[4] == 1) && ($row['hashid'] == $gval[2])) { ?>
                            <input class="standardfield" type="text" name="label" value="<?php echo $row['label']; ?>">
                            <input type="hidden" name="hashid" value="<?php echo $row['hashid']; ?>">
                            <input type="hidden" name="menu_tabhashid" value="<?php echo $gval[1]; ?>">
                            <input type="hidden" name="pform" value="3">
                            <?php
                        } elseif (($gval[4] == 1) && ($row['hashid'] != $gval[2])) {
                            echo $row['label'];
                        } ?>
                    </td>
                    <td class="ls_<?php echo $ls; ?>"><?php echo $row['type']; ?></td>
                    <td class="ls_<?php echo $ls; ?>"><?php echo $row['sortorder']; ?> <font class="note">&nbsp; |
                            <?php
                            if ($numcounter != 1) { ?>
                                <a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=9:<?php echo $row['hashid'] ?>:<?php echo $row['sortorder']; ?>:<?php echo $gval[1] ?>">up</a>
                                <?php
                            }
                            if ($numcounter != $highest) { ?>
                            <a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=10:<?php echo $row['hashid'] ?>:<?php echo $row['sortorder']; ?>:<?php echo $gval[1] ?>">down</a></font>
                    </td>
                    <?php
                    } ?>
                    <?php
                    if ($row['isactive'] == 0) {
                        $newclass = "classactive";
                    } else {
                        $newclass = "classdeactive";
                    } ?>
                    <td class="<?php echo $newclass; ?>">
                        &nbsp;
                    </td>
                    <td class="ls_<?php echo $ls; ?>" align="center">
                        <?php
                        if ($row['isactive'] == 0) { ?>
                            <a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=11:<?php echo $row['hashid'] ?>:<?php echo $gval[1] ?>"><font
                                        class="note">Enable</font></a> |
                            <?php
                        } else { ?>
                            <a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=12:<?php echo $row['hashid'] ?>:<?php echo $gval[1] ?>"><font
                                        class="note">Disable</font></a> |
                            <?php
                        } ?>
                        <a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=8:<?php echo $gval[1] ?>:<?php echo $row['hashid'] ?>::1"><font
                                    class="note">Edit</font></a>
                    </td>
                </tr>
                <?php
                $numcounter++;
            } ?>
            <tr>
                <td>
                    <?php
                    if ($gval[4] == 1) { ?>
                    <br>
                    <input style="font-family:Arial;font-size:14px;padding:2px" type="submit" value="Update Tab">
        </form>
    <?php
    } ?>
        </td>
        </tr>
    </table>
<br>
<?php
if ($elemtype == "Standard Form") { ?>
<?php
$sql = "SELECT productcomponent, location FROM relations_predefined WHERE productcomponent = 1 AND registrantid = " . RID . " LIMIT 1";
// echo $sql;
$res = mysql_query($sql);
$row = mysql_fetch_array($res);
$set_location = $row['location'];
$set_productcomp = $row['productcomponent'];

if ((($set_productcomp == 1) && ($set_location == $gval[1])) || (($set_productcomp == "") && ($set_location == ""))) {

$sql = "SELECT hashid, iscomponent, label FROM menusub_tabs WHERE menu_tabhashid = '" . $gval[1] . "' AND type = 'new' LIMIT 1";
// echo $sql;
$res = mysql_query($sql);
$row = mysql_fetch_array($res); ?>
    <h2>Product / Service Component Settings</h2>
    <table width="95%" cellspacing="2" cellpadding="2" border="0">
        <tr>
            <td class="ls_on" colspan="2">
                <?php
                if ($row['iscomponent'] == 0) { ?>
                    Make
                    <strong><?php echo ucwords(str_replace("new", "", $row['label'])) ?></strong> available as Product / Services component:
                    <?php
                } else { ?>
                    This module is available to other modules as a Products or Services component.
                    <?php
                } ?>
            </td>
            <td class="ls_on" align="center" width="15%">
                <?php
                if ($row['iscomponent'] == 0) { ?>
                    <a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=30:<?php echo $gval[1] ?>:<?php echo $row['hashid'] ?>:1"><font
                                class="note">Set as component</font></a>
                    <?php
                } else { ?>
                    <a onclick="return confirm('!!! WARNING !!!\n If you remove this as a component ALL \nsales and purchases data will be lost.');"
                       href="<?php echo $_SERVER['PHP_SELF'] ?>?id=31:<?php echo $gval[1] ?>:<?php echo $row['hashid'] ?>"><font
                                class="note">Remove as Component</font></a>
                    <?php
                } ?>
            </td>
        </tr>
        <tr>
            <td class="ls_off" colspan="2">
                <?php
                if ($row['iscomponent'] == 0) { ?>
                    <font class="note">
                        By making this module available to other modules as a component, you will be able to sell
                        the items created in the <strong><?php echo $top_level_label; ?></strong> module.
                    </font>
                    <?php
                } ?>
            </td>
        </tr>
    </table><br/><br/>
    <?php
} else {
    // Lets see if they have allocated a products module
    $sql = "SELECT ispredefined FROM relations_matrix WHERE menutabid = '" . $gval[1] . "' AND ispredefined = 2 AND registrantid = " . RID . " LIMIT 1";
    $res = mysql_query($sql);
    $row = mysql_fetch_array($res);
    $mod_exists = $row['ispredefined'];
    // So this is not a product module, but we need to check if they want to do purchasing or sales
if ($mod_exists == 2) { ?>
    <h2>Product / Service Component Settings</h2>
    <table width="95%" cellspacing="2" cellpadding="2" border="0">

        <?php
        $sql = "SELECT hashid, componentabtype FROM menusub_tabs WHERE menu_tabhashid = '" . $gval[1] . "' AND type = 'new' LIMIT 1";
        // echo $sql;
        $res = mysql_query($sql);
        $row = mysql_fetch_array($res);
        if ($row['componentabtype'] == 0) { ?>
            <tr>
                <td class="ls_on" colspan="2">
                    You have assigned a Product / Services module to <strong><?php echo $top_level_label ?></strong>.
                    You can now choose whether to raise Purchase Orders or Sales Orders in this module.
                    Please select from one of the two options below.
                </td>
            </tr>
            <tr>
                <td width="35%" class="ls_off">Sales Orders</td>
                <td class="ls_off"><a
                            href="<?php echo $_SERVER['PHP_SELF'] ?>?id=32:<?php echo $gval[1] ?>:1:<?php echo $row['hashid'] ?>:1"><font
                                class="note">Enable</font></a></td>
            </tr>
            <tr>
                <td class="ls_off">Purchase Orders</td>
                <td class="ls_off"><a
                            href="<?php echo $_SERVER['PHP_SELF'] ?>?id=32:<?php echo $gval[1] ?>:2:<?php echo $row['hashid'] ?>:2"><font
                                class="note">Enable</font></a></td>
            </tr>
            <?php
        } elseif ($row['componentabtype'] == 1) { ?>
            <tr>
                <td class="ls_on" align="center"><strong>Order Type Enabled</strong></td>
                <td class="ls_on" align="center" width="15%"><strong>Actions</strong></td>
            </tr>
            <tr>
                <td class="ls_off">Sales Orders has been enabled:</td>
                <td class="ls_off" align="center" width="15%"><a
                            href="<?php echo $_SERVER['PHP_SELF'] ?>?id=32:<?php echo $gval[1] ?>:0:<?php echo $row['hashid'] ?>:4"><font
                                class="note">Disable</font></a></td>
            </tr>
            <?php
        } elseif ($row['componentabtype'] == 2) { ?>
            <tr>
                <td class="ls_on" align="center"><strong>Order Type Enabled</strong></td>
                <td class="ls_on" align="center" width="15%"><strong>Actions</strong></td>
            </tr>
            <tr>
                <td class="ls_off">Purchase Orders has been enabled:</td>
                <td class="ls_off" align="center" width="15%"><a
                            href="<?php echo $_SERVER['PHP_SELF'] ?>?id=32:<?php echo $gval[1] ?>:0:<?php echo $row['hashid'] ?>:3"><font
                                class="note">Disable</font></a></td>
            </tr>
            <?php
        } ?>
    </table><br/>
<br/>
    <?php
}
} ?>

    <h2>Options:</h2>
    <table width="95%" cellspacing="2" cellpadding="2" border="0">
        <tr>
            <td class="ls_top" align="center" colspan="2"><strong>Feature</strong></td>
            <td class="ls_top" align="center" width="15%"><strong>Actions</strong></td>
        </tr>
        <?php
        $sqlicon = "SELECT hashid, label, icon FROM menusub_tabs WHERE menu_tabhashid ='" . $gval[1] . "' AND type = 'new' AND registrantid =" . RID . "";
        //echo $sqlicon;
        $resicon = mysql_query($sqlicon);
        while ($rowicon = mysql_fetch_array($resicon)) {
            ?>
            <tr>
                <td class="ls_off" align="center">Set Module Icon</td>
                <td class="ls_off" align="center">
                    <?php
                    if (($rowicon['icon'] == "") || ($rowicon['icon'] == NULL)) {
                        $theiconlabel = ucwords(str_replace("new", "", $rowicon['label']));
                        if ($theiconlabel != "") {
                            $theiconlabel = "to " . $theiconlabel;
                        }
                        echo "<a " . LFD . " href='icon_manager.php?id=3:24:" . $rowicon['hashid'] . "'><font class='note'>Click here to assign icon " . $theiconlabel . "</font></a>" ?>
                        <?php
                    } else { ?>
                        <img src="../images/icons/24_shadow/<?php echo $rowicon['icon']; ?>"><?php
                    } ?>
                </td>
                <td class="ls_off" align="center" width="15%">&nbsp;
                    <?php
                    if (!$rowicon['icon'] == "") { ?>
                        <a href="<?php $_SERVER['PHP_SELF'] ?>?id=23:<?php echo $gval[1] ?>:<?php echo $rowicon['hashid']; ?>"><font
                                    class="note">Delete</font></a>
                        <?php
                    } ?>
                </td>
            </tr>
            <?php
        } ?>
        <tr>
            <td class="ls_off">
                Enable multi-linking<br>
                <font style="font-size:11px">When multi-linking a) Enable the multi-link in the two modules you are
                    linking b) Assign the as "New Components" in each module</font></td>
            <td class="ls_off" align="center" colspan="2">
                <?php
                $getmoddetail = "SELECT id FROM `relations_predefined` WHERE location = '" . $gval[1] . "'";
                //echo $getmoddetail;
                $resgm = mysql_query($getmoddetail);
                $rowgm = mysql_fetch_array($resgm);
                if (!$rowgm['id']) { ?>
                    <a href="main_body.php?id=36:<?php echo $gval[1] . ":" . $sectionlabel; ?>"
                       class="note">Activate</a>
                    <?php
                } else { ?>
                    <a onclick="return confirm('Are you sure you want to remove this multilink?')"
                       href="main_body.php?id=37:<?php echo $gval[1] . ":" . $sectionlabel; ?>" class="note">Remove</a>
                    <?php
                } ?>
            </td>
        </tr>
    </table>
<br/><br>
    <h2>Assigned Component(s):</h2>
    <a name="moduletabs"></a>
    <?php
    $sql = "SELECT hashid, label FROM menusub_tabs WHERE menu_tabhashid ='" . $gval[1] . "' AND type = 'new' AND registrantid =" . RID . " ORDER BY sortorder ASC";
    $res = mysql_query($sql);
    while ($row = mysql_fetch_array($res)) {
        if ($row['label'] != "new") {
            $multimessage = "to " . ucwords(str_replace("new", "", $row['label']));
        }
        ?><br/>
        <a rel="lyteframe" name="lyteframe" rev=" width: 700px; height: 450px; scrolling: yes;"
           href="../assign_tabs.php?id=<?php echo $row['hashid'] ?>:<?php echo $gval[1] ?>"><font class="note">Assign
                New Component(s) <?php echo $multimessage; ?></font></a><br><br>
        <table width="95%" cellspacing="2" cellpadding="2" border="0">
            <tr>
                <td class="ls_top" align="center"><strong>Component Label</strong></td>
                <td class="ls_top" align="center"><strong>Sort Order</strong></td>
                <td class="ls_top" align="center" width="15%"><strong>Actions</strong></td>
            </tr>
            <?php
            unset($multimessage);
            // Lets get the items allocated to this module
            $query = "SELECT * FROM relations_matrix WHERE parenttable = '" . $row['hashid'] . "' ORDER BY sortorder ASC";
            // echo $query;
            $result = mysql_query($query);
            $sqlgetnum = "SELECT MAX(sortorder) AS highestnumber FROM relations_matrix WHERE parenttable = '" . $row['hashid'] . "'";
            $resgetnum = mysql_query($sqlgetnum);
            $rowgetnum = mysql_fetch_array($resgetnum);
            $highest = $rowgetnum['highestnumber'];
            $numcounter = 1;
            while ($row = mysql_fetch_array($result)) {
                include("..//includes/ls.php");
                if ($row['ispredefined'] >= 1) {
                    $getpre = "SELECT label FROM relations_predefined WHERE id = " . $row['childtable'] . " LIMIT 1";
                    // echo $getpre;
                    $preres = mysql_query($getpre);
                    $prerow = mysql_fetch_array($preres);
                    $labelval = $prerow['label'];
                } else {
                    $getpre = "SELECT tablabel FROM udf_definitions WHERE hashid = '" . $row['labelhashid'] . "' LIMIT 1";
                    // echo $getpre;
                    $preres = mysql_query($getpre);
                    $prerow = mysql_fetch_array($preres);
                    $labelval = $prerow['tablabel'];
                } ?>
                <tr>
                    <td class="ls_<?php echo $ls; ?>"><?php echo $labelval; ?></td>
                    <td class="ls_<?php echo $ls; ?>"><?php echo $row['sortorder']; ?><font class="note">&nbsp; |
                            <?php
                            if ($numcounter != 1) { ?>
                                <a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=24:<?php echo $row['id'] ?>:<?php echo $row['sortorder']; ?>:<?php echo $gval[1] ?>:<?php echo $row['parenttable'] ?>:#moduletabs">up</a>
                                <?php
                            }
                            if ($numcounter != $highest) { ?>
                            <a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=25:<?php echo $row['id'] ?>:<?php echo $row['sortorder']; ?>:<?php echo $gval[1] ?>:<?php echo $row['parenttable'] ?>:#moduletabs">down</a></font>
                    </td>
                    <?php
                    }
                    ?></font>
                    </td>
                    <td class="ls_<?php echo $ls; ?>" align="center" width="15%">
                        <a href="<?php $_SERVER['PHP_SELF'] ?>?id=26:<?php echo $row['id'] ?>:<?php echo $row['sortorder']; ?>:<?php echo $gval[1] ?>:<?php echo $row['parenttable'] ?>:#moduletabs"><font
                                    class="note">Remove</font></a>
                    </td>
                </tr>
                <?php
                $numcounter++;
            }
            ?>
        </table><br>
        <?php
    }
} ?><br>
    </table>
<?php
}
elseif ($gval[0] == 13)  { ?>
    <script language="JavaScript" type="text/javascript">
        <!--
        function checkform(form) {
            if ((form.label_1.value == "") || (form.label_singular_1.value == "") || (form.label_2.value == "") || (form.label_singular_2.value == "") || (form.numberseries_1.value == "") || (form.numberseries_2.value == "")) {
                alert("Please complete all fields");
                form.label_1.focus();
                return false;
            }
            return true;
        }

        //-->
    </script>
    <form method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?>" onsubmit="return checkform(this);">
        <table cellpadding="5" cellspacing="3" border="0" width="95%">
            <tr>
                <td><h1>Create Sub Category Labels</h1></td>
            </tr>
        </table>
        <br>
        <?php
        $colorcount = 0;
        for ($number = 0; $number < $gval[2]; $number++) {
            if ($colorcount == 0) {
                $lscol = "on";
            } else {
                $lscol = "mid";
            } ?>
            <table cellpadding="5" cellspacing="3" border="0" width="95%">
                <tr>
                    <td class="ls_<?php echo $lscol; ?>">Plural Label <?php echo($number + 1); ?>: <font
                                color="red">*</font></td>
                    <td class="ls_<?php echo $lscol; ?>"><input type="text" name="label_<?php echo($number + 1) ?>"
                                                                class="standardfield"></td>
                </tr>
                <tr>
                    <td class="ls_<?php echo $lscol; ?>">Singluar Label <?php echo($number + 1); ?>: <font
                                color="red">*</font></td>
                    <td class="ls_<?php echo $lscol; ?>"><input type="text"
                                                                name="label_singular_<?php echo($number + 1) ?>"
                                                                class="standardfield"></td>
                </tr>
                <tr>
                    <td class="ls_<?php echo $lscol; ?>">Number Series <?php echo($number + 1); ?>: <font
                                color="red">*</font></td>
                    <td class="ls_<?php echo $lscol; ?>"><input type="text"
                                                                name="numberseries_<?php echo($number + 1) ?>"
                                                                class="standardfield_ns"></td>
                </tr>
            </table>
            <?php
            $colorcount++;
        } ?>
        <br/>
        <table cellpadding="5" cellspacing="3" border="0" width="95%">
            <tr>
                <td align="center" class="messageboard">It is important that you complete this step or your new module
                    will not functional properly.
                </td>
            </tr>
        </table>
        <br/>
        <input type="submit" value="Create">
        <input type="hidden" name="totcategories" value="<?php echo $number; ?>">
        <input type="hidden" name="pform" value="4">
        <input type="hidden" name="hashid" value="<?php echo $gval[1]; ?>">
    </form>
<?php
}
elseif ($gval[0] == 14){
// form manager section
$gvaltest = explode("||", $gval[1]);

//	var_dump($gval[1]);
//	var_dump($gvaltest);
//	die;
$query = "SELECT * FROM udf_definitions WHERE registrantid = " . RID . " AND hashid = '" . $gval[4] . "'";
$result = mysql_query($query);
$row = mysql_fetch_array($result);
//echo 'test';
//var_dump($row);
?>

    <script language="JavaScript" type="text/javascript">
        <!--
        function checkform(form) {
            if ((form.udflabel.value == "") || (form.columntype.value == "") || (form.udftype.value == "")) {
                alert("Please complete all fields marked with a red asterisk.");
                return false;
            }
            return true;
        }

        //-->
    </script>
    <table border="0" width="95%" cellpadding="2" cellspacing="3">
        <tr>
            <td><h1>Manage your Forms</h1></td>
            <td class="messageboard" width="15%" align="center">
                <a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=14:<?php echo $gval[1] ?>">Reset Form</a>
            </td>
        </tr>
        <tr>
            <td colspan="2"><br/>
                <form method="POST" name="form" action="<?php echo $_SERVER['PHP_SELF']; ?>"
                      onsubmit="return checkform(this);">
                    <input type="hidden" name="udfmodule"
                           value="<?php echo $gvaltest[0] ?>&&<?php echo $gvaltest[1] ?>">

                    <!-- Manage your Forms -->

                    <table border="0" width="100%" cellpadding="2" cellspacing="3">
                        <tr>
                            <td width="15%" class="ls_on" align="right">Add Field Label: <font color="red">*</font></td>
                            <td class="ls_on" align="left">
                                <script language="javascript" type="text/javascript"
                                        src="../js/udf_duplicate_checker.js"></script>
                                <input onkeyup="javascript:checkdup(this.value);" type="text" name="udflabel"
                                       class="standardfield" value="<?php echo $row['label']; ?>">
                                <span style="color:red;" id="txtHint"></span>
                            </td>
                            <td width="15%" class="ls_on" align="right">Add Field Message:</td>
                            <td class="ls_on" align="left">
                                <input type="text" name="labelmsg" class="standardfield"
                                       value="<?php echo $row['labelmsg']; ?>">
                            </td>
                        </tr>
                        <tr>
                            <td class="ls_on" align="right">Select Field Type: <font color="red">*</font></td>
                            <td class="ls_on" align="left">
                                <?php
                                if ($row['fieldtype'] == 1) {
                                    $udf1 = "selected";
                                } elseif ($row['fieldtype'] == 2) {
                                    $udf2 = "selected";
                                } elseif ($row['fieldtype'] == 3) {
                                    $udf3 = "selected";
                                } elseif ($row['fieldtype'] == 4) {
                                    $udf4 = "selected";
                                } elseif ($row['fieldtype'] == 5) {
                                    $udf5 = "selected";
                                } elseif ($row['fieldtype'] == 6) {
                                    $udf6 = "selected";
                                } elseif ($row['fieldtype'] == 7) {
                                    $udf7 = "selected";
                                } elseif ($row['fieldtype'] == 8) {
                                    $udf8 = "selected";
                                } elseif ($row['fieldtype'] == 9) {
                                    $udf9 = "selected";
                                } elseif ($row['fieldtype'] == 10) {
                                    $udf10 = "selected";
                                } elseif ($row['fieldtype'] == 11) {
                                    $udf11 = "selected";
                                } elseif ($row['fieldtype'] == 12) {
                                    $udf12 = "selected";
                                } elseif ($row['fieldtype'] == 13) {
                                    $udf13 = "selected";
                                } elseif ($row['fieldtype'] == 14) {
                                    $udf14 = "selected";
                                } elseif ($row['fieldtype'] == 15) {
                                    $udf15 = "selected";
                                } elseif ($row['fieldtype'] == 16) {
                                    $udf16 = "selected";
                                } elseif ($row['fieldtype'] == 17) {
                                    $udf17 = "selected";
                                } elseif ($row['fieldtype'] == 18) {
                                    $udf18 = "selected";
                                } ?>
                                <select id="udftype" name="udftype" class="standardselect"
                                        onchange="displayOption(this.value);">
                                    <option value="">Select One</option>
                                    <option <?php echo $udf3; ?> value="3">Checkbox</option>
                                    <option <?php echo $udf13; ?> value="13">Currency</option>
                                    <option <?php echo $udf10; ?> value="10">Data Link Field</option>
                                    <option <?php echo $udf8; ?> value="8">Date</option>
                                    <option <?php echo $udf9; ?> value="9">Date & Time</option>
                                    <option <?php echo $udf2; ?> value="2">Drop Down Select Menu</option>
                                    <option <?php echo $udf5; ?> value="5">Email Address</option>
                                    <option <?php echo $udf7; ?> value="7">Mobile Number</option>
                                    <option <?php echo $udf14; ?> value="14">Number (No Decimal)</option>
                                    <option <?php echo $udf6; ?> value="6">Phone Number</option>
                                    <option <?php echo $udf11; ?> value="11">Section Header</option>
                                    <option <?php echo $udf1; ?> value="1">Text Field</option>
                                    <option <?php echo $udf12; ?> value="12">Text Field - Header</option>
                                    <option <?php echo $udf4; ?> value="4">Text Area</option>
                                    <option <?php echo $udf15; ?> value="15">Show in Calendar</option>
                                    <option <?php echo $udf16; ?> value="16">Alert</option>
                                    <option <?php echo $udf17; ?> value="17">Image Upload</option>
                                    <option <?php echo $udf18; ?> value="18">Document Upload</option>
                                </select>
                            </td>
                            <td class="ls_on" align="right">Data Type: <font color="red">*</font></td>
                            <td class="ls_on" align="left">
                                <?php
                                unset($udf1, $udf2, $udf3, $udf4, $udf5, $udf6, $udf7, $udf8, $udf9, $udf10, $udf11, $udf12, $udf13, $udf14, $udf15, $udf16, $udf17, $udf18);
                                if ($row['columntype'] == "DATE") {
                                    $udf1 = "selected";
                                } elseif ($row['columntype'] == "DATETIME") {
                                    $udf2 = "selected";
                                } elseif ($row['columntype'] == "FLOAT") {
                                    $udf3 = "selected";
                                } elseif ($row['columntype'] == "INT") {
                                    $udf4 = "selected";
                                } elseif ($row['columntype'] == "TEXT") {
                                    $udf5 = "selected";
                                } elseif ($row['columntype'] == "TIME") {
                                    $udf6 = "selected";
                                } elseif ($row['columntype'] == "VARCHAR(350)") {
                                    $udf7 = "selected";
                                } elseif ($row['columntype'] == "VARCHAR(10)") {
                                    $udf8 = "selected";
                                } elseif ($row['columntype'] == "VARCHAR(50)") {
                                    $udf9 = "selected";
                                } elseif ($row['columntype'] == "DECIMAL(11,2)") {
                                    $udf10 = "selected";
                                } elseif ($row['columntype'] == "DECIMAL(11,4)") {
                                    $udf11 = "selected";
                                } ?>
                                <select name="columntype" class="standardselect">
                                    <option value="">Select One</option>
                                    <option <?php echo $udf10 ?> value="DECIMAL(11,2)">Currency (2 Decimal Places)
                                    </option>
                                    <option <?php echo $udf11 ?> value="DECIMAL(11,4)">Currency (4 Decimal Places)
                                    </option>
                                    <option <?php echo $udf1 ?> value="DATE">Date (Date Only)</option>
                                    <option <?php echo $udf2 ?> value="DATETIME">Date Time (Date Time)</option>
                                    <option <?php echo $udf3 ?> value="FLOAT">Float (Decimal)</option>
                                    <option <?php echo $udf4 ?> value="INT">Integer (No Decimal)</option>
                                    <option <?php echo $udf5 ?> value="TEXT">Text (Unlimited Text)</option>
                                    <option <?php echo $udf6 ?> value="TIME">Time (Time Only)</option>
                                    <option <?php echo $udf8 ?> value="VARCHAR(10)">Varchar (Max 10 Chars)</option>
                                    <option <?php echo $udf9 ?> value="VARCHAR(50)">Varchar (Max 50 Chars)</option>
                                    <option <?php echo $udf7 ?> value="VARCHAR(350)">Varchar (Max 350 Chars)</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="ls_on" align="right">Display in Lists:</td>
                            <td class="ls_on" align="left">
                                <?php
                                if ($row['inlists'] == 0) {
                                    $checkmeloc = "selected";
                                } elseif ($row['inlists'] == 1) {
                                    $checkmeloc2 = "selected";
                                } ?>
                                <select name="inlists" class="standardselect">
                                    <option <?php echo $checkmeloc ?> value="0">No</option>
                                    <option <?php echo $checkmeloc2 ?> value="1">Yes</option>
                                </select>
                            </td>
                            <?php
                            if ($gval[1]) {
                                $dbref = explode("||", $gval[1]);
                                if ($dbref && $dbref[1] == '81448138F5F163CCDBA4ACC69819F280') { ?>
                                    <td width="15%" class="ls_on" align="right">Visible only to:</td>
                                    <td width="15%" class="ls_on" align="left">
                                        <?php
                                        //echo $row['visibleonlyto'];
                                        if ($row['visibleonlyto'] && $row['visibleonlyto'] == 1) {
                                            $checkvisible = "selected";
                                        } ?>
                                        <select name="visibleonlyto" class="standardselect">
                                            <option <?php echo $checkvisible ?> value="0">--Select--</option>
                                            <option <?php echo $checkvisible ?> value="1">Franchise owner</option>
                                        </select>
                                    </td>
                                <?php }
                            }
                            ?>
                        </tr>
                    </table>
                    <script language="Javascript">
                        function displayOption(udftype) {
                            if (udftype == 1) {
                                document.form.columntype.options[11] = new Option("Varchar (Max 350 Chars)", "VARCHAR(350)", true, true);
                            } else if (udftype == 2) {
                                document.form.columntype.options[11] = new Option("Varchar (Max 350 Chars)", "VARCHAR(350)", true, true);
                            } else if (udftype == 3) {
                                document.form.columntype.options[9] = new Option("Varchar (Max 10 Chars)", "VARCHAR(10)", true, true);
                            } else if (udftype == 4) {
                                document.form.columntype.options[7] = new Option("Text (Unlimited Text)", "TEXT", true, true);
                            } else if (udftype == 5) {
                                document.form.columntype.options[11] = new Option("Varchar (Max 350 Chars)", "VARCHAR(350)", true, true);
                            } else if (udftype == 6) {
                                document.form.columntype.options[10] = new Option("Varchar (Max 50 Chars)", "VARCHAR(50)", true, true);
                            } else if (udftype == 7) {
                                document.form.columntype.options[10] = new Option("Varchar (Max 50 Chars)", "VARCHAR(50)", true, true);
                            } else if (udftype == 8) {
                                document.form.columntype.options[4] = new Option("Date (Date Only)", "DATE", true, true);
                            } else if (udftype == 9) {
                                document.form.columntype.options[4] = new Option("Date Time (Date Time)", "DATETIME", true, true);
                            } else if (udftype == 10) {
                                document.form.columntype.options[11] = new Option("Varchar (Max 350 Chars)", "VARCHAR(350)", true, true);
                            } else if (udftype == 11) {
                                document.form.columntype.options[11] = new Option("Varchar (Max 350 Chars)", "VARCHAR(350)", true, true);
                            } else if (udftype == 12) {
                                document.form.columntype.options[11] = new Option("Varchar (Max 350 Chars)", "VARCHAR(350)", true, true);
                            } else if (udftype == 13) {
                                document.form.columntype.options[1] = new Option("Currency (2 Decimal Places)", "DECIMAL(11,2)", true, true);
                            } else if (udftype == 14) {
                                document.form.columntype.options[6] = new Option("Integer (No Decimal)", "INT", true, true);
                            } else if (udftype == 15) {
                                document.form.columntype.options[6] = new Option("Integer (No Decimal)", "INT", true, true);
                            } else if (udftype == 16) {
                                document.form.columntype.options[4] = new Option("Date Time (Date Time)", "DATETIME", true, true);
                            } else if (udftype == 17) {
                                document.form.columntype.options[11] = new Option("Varchar (Max 350 Chars)", "VARCHAR(350)", true, true);
                            } else if (udftype == 18) {
                                document.form.columntype.options[11] = new Option("Varchar (Max 350 Chars)", "VARCHAR(350)", true, true);
                            }

                            var showOptx = document.getElementById('datalinkfieldx');
                            if (udftype == 2) {
                                showOptx.style.display = '';
                            } else {
                                showOptx.style.display = 'none';
                            }

                            var showOpt = document.getElementById('datalinkfield1');
                            if (udftype == 10) {
                                showOpt.style.display = '';
                            } else {
                                showOpt.style.display = 'none';
                            }
                        }
                    </script>
                    <span id="datalinkfieldx" style="display:none">
                    <table border="0" width="100%" cellpadding="2" cellspacing="3">
                    <tr>
                        <td width="15%" class="ls_on" align="right">Create as Object: </td>
                        <td class="ls_on" width="20%" align="center">
                            <select class="standardselect" name="createasobject">
                                <option value="">No</option>
                                <option value="1">Yes</option>
                            </select>
                        </td>
                        <td class="ls_on">
                            <font class="note">
                                If you create this field as an <em>Object</em>, you will be able to apply it to other forms. Drop Down objects
                                share a single source of data, so if its source data is changed the changes will be applied to all forms where the
                                <em>Object</em> is present.
                            </font>
                        </td>
                    </tr>
                    </table>
                    </span>
                    <?php
                    if ($gval[3] <= 0) {
                        $stylee = 'style="display:none"'; ?>
                        <?php
                    } ?>

                    <span id="datalinkfield1" <?php echo $stylee; ?>>
                        <table border="0" width="100%" cellpadding="2" cellspacing="3">
                            <tr>
                            <td class="ls_on" align="right">Link to field: </td>
                            <td class="ls_on" width="40%">
                                <?php
                                $linksql = "SELECT udf.tablabel AS tablabel, mt.label AS mtlabel, mst.label AS mstlabel, mst.menu_tabhashid, udf.location AS udflocation, udf.label AS udflabel, udf.hashid AS hashid
                                            FROM menu_tabs mt,menusub_tabs mst,udf_definitions udf
                                            WHERE mst.menu_tabhashid = mt.hashid 
                                            AND udf.location = mst.hashid
                                            AND mt.registrantid =" . RID . "
                                            ORDER BY mtlabel, mstlabel, udflabel ASC";
                                // echo $linksql;
                                $reslink = mysql_query($linksql);

                                // Check if a record exists in the relations matrix, if so we can make it select  on edit...
                                $checkrel = "SELECT parenttable, udf_columnid FROM relations_matrix WHERE labelhashid = '" . $gval[4] . "' AND registrantid =" . RID . " LIMIT 1";
                                $checkrelreslink = mysql_query($checkrel);
                                $checkrelrowlink = mysql_fetch_array($checkrelreslink); ?>
                                <select name="datalinktable" class="standardselect">
                                    <option value="">Select One</option>
                                    <?php
                                    while ($rowlink = mysql_fetch_array($reslink)) {
                                        if (($checkrelrowlink['parenttable'] == $rowlink['udflocation']) && ($checkrelrowlink['udf_columnid'] == $rowlink['hashid'])) {
                                            $relchecker = "selected";
                                        }
                                        if (ucwords(str_replace("new", "", $rowlink['mstlabel'])) != "") { ?>
                                            <option <?php echo $relchecker; ?> value="<?php echo $rowlink['udflocation'] ?>&&<?php echo $rowlink['hashid'] ?>"><?php echo $rowlink['mtlabel'] ?> / <?php echo ucwords(str_replace("new", "", $rowlink['mstlabel'])) ?>  / <?php echo ucwords($rowlink['udflabel']) ?></option>
                                            <?php
                                        } else { ?>
                                            <option <?php echo $relchecker; ?> value="<?php echo $rowlink['udflocation'] ?>&&<?php echo $rowlink['hashid'] ?>"><?php echo $rowlink['mtlabel'] ?> / <?php echo ucwords($rowlink['udflabel']) ?></option>
                                            <?php
                                        }
                                        unset($relchecker);
                                    } ?>
                                </select>
                            </td>
                            <td class="ls_on" align="right">Corresponding Menu Tab Label:</td>
                            <td class="ls_on">
                                <input type="text" class="standardfield" name="tablabel"
                                       value="<?php echo $row['tablabel'] ?>">
                            </td>
                        </tr>
                        </table>
                    </span>

                    <!--                not display-->
                    <table border=0>
                        <tr>
                            <?php
                            unset($udf1, $udf2, $udf3, $udf4, $udf5, $udf6, $udf7, $checkmeloc2, $checkmeloc);
                            ?>
                            <td align="left" colspan="6"><br>
                                <?php
                                if ($gval[4] != "") { ?>
                                    <input style="font-family:Arial;font-size:11px;width:150px;" type="submit"
                                           value="Update Field">
                                    <?php
                                    if ($gval[2] == 2) {
                                        echo "<br><br><font style='background-color:#91c21b;padding:3px;color:white'>Update Field</strong></font>";
                                    } ?>
                                    <?php
                                } else { ?>
                                    <input style="font-family:Arial;font-size:11px;width:150px;" type="submit"
                                           name="createfield" value="Create Field">
                                    <?php
                                    if ($gval[2] == 3) {
                                        echo "<br><br><font style='background-color:#91c21b;padding:3px;color:white'>Saved - Update or Create New Field</strong></font>";
                                    } ?>
                                    <?php
                                } ?>
                            </td>
                        </tr>
                    </table>

                    <?php
                    if ($gval[4] != "") { ?>
                        <input type="hidden" value="<?php echo $gval[4] ?>" name="hashid">
                        <input type="hidden" name="pform" value="6">
                        <input type="hidden" name="redundantcolumnname"
                               value="<?php echo columnnames($row['label']); ?>">
                    <?php } else { ?>
                        <input type="hidden" name="pform" value="5">
                        <?php
                    } ?>
                    <input type="hidden" value="<?php echo $gval[1] ?>" name="strredirect">
                </form>
            </td>
        </tr>
    </table>
<br/>
<?php
$querygroup = "SELECT udf_definitions.location AS location, menu_tabs.hashid AS menutabid, menu_tabs.label AS menulabel, menusub_tabs.label AS menusublabel 
					    FROM udf_definitions, menu_tabs, menusub_tabs 
					    WHERE udf_definitions.registrantid = " . RID . "
					    AND menusub_tabs.hashid  = udf_definitions.location
					    AND menu_tabhashid = menu_tabs.hashid
					    AND location = '" . $gvaltest[1] . "' LIMIT 1";
// echo $querygroup."<br>";
$resultgroup = mysql_query($querygroup);
$rowgroup = mysql_fetch_array($resultgroup);
?>
    <a name="<?php echo $rowgroup['menulabel'] . " - " . $rowgroup['menusublabel']; ?>"></a>
    <script language="Javascript">
        function hideElement(hideThisElement) {
            document.getElementById(hideThisElement).style.display = "none";
        }
    </script>
    <form method="POST" name="form1" action="<?php echo $_SERVER['PHP_SELF'] ?>?id=<?php echo $revfull ?>">
        <table class="note" border="0" width="95%" cellpadding="2" cellspacing="3">
            <tr>
                <td colspan="2"><font
                            style="font-weight:bold;font-size:16px">&nbsp;<?php echo $rowgroup['menulabel'] . "  " . ucwords(str_replace("new", "", $rowgroup['menusublabel'])); ?></font>
                </td>
                <td colspan="2">
                    <script type="text/javascript" src="../js/dropdown.js"></script>
                    <div id="quick_menu_menu_parent" class="quick_menu"><img src="../images/arrowbullet_2.gif"
                                                                             border="0" hspace="5" align="left">Form
                        Functions
                    </div>
                    <div id="quick_menu_menu_child">
                        <a <?php echo LFD ?> class="quick_menu_item"
                                             href="form_functions.php?id=2:<?php echo $gval[1] ?>"><img
                                    src="../images/arrowbullet.gif" border="0" hspace="5" align="left"> &nbsp;Replicate
                            Form Field(s)</a>
                        <a <?php echo LFD ?> class="quick_menu_item"
                                             href="form_functions.php?id=3:<?php echo $gval[1] ?>"><img
                                    src="../images/arrowbullet.gif" border="0" hspace="5" align="left"> &nbsp;Apply
                            Object</a>
                    </div>
                    <script type="text/javascript">
                        at_attach("quick_menu_menu_parent", "quick_menu_menu_child", "hover", "y", "pointer");
                    </script>
                    <br/>
                </td>
            </tr>
            <tr>
                <td class="ls_on" width="25%" align="center" width="15%"><strong>Field Type</strong></td>
                <td class="ls_on" align="center"><strong>Label</strong></td>
                <td class="ls_on" align="center"><strong>Index</strong></td>
                <td class="ls_on" align="center"><strong>Sort Order</strong></td>
                <td class="ls_on" width="17%" align="center" width="10%"><strong>Actions</strong></td>
            </tr>
            <?php
            $indextable = explode("||", $gval[1]);
            $keysarray = array();
            $iresult = mysql_query("SHOW KEYS FROM udf_" . $indextable[1] . "");
            if (mysql_num_rows($iresult) > 0) {
                while ($irow = mysql_fetch_assoc($iresult)) {
                    array_push($keysarray, $irow['Key_name']);
                }
            } ?>

            <?php
            $query = "SELECT * FROM udf_definitions WHERE registrantid = " . RID . " AND location = '" . $rowgroup['location'] . "' ORDER BY sortorder ASC";
            // echo $query."<br>";
            $result = mysql_query($query);
            $ls = 1;
            $sqlgetnum = "SELECT MAX(sortorder) AS highestnumber FROM udf_definitions WHERE location = '" . $rowgroup['location'] . "' AND registrantid=" . RID;
            // echo $sqlgetnum."<br>";
            $resgetnum = mysql_query($sqlgetnum);
            $rowgetnum = mysql_fetch_array($resgetnum);
            $highest = $rowgetnum['highestnumber'];
            $numcounter = 1;
            while ($row = mysql_fetch_array($result)) {
                include("..//includes/ls.php");
                if ($row['fieldtype'] == 1) {
                    $fieldtype = "Text Field";
                } elseif ($row['fieldtype'] == 2) {
                    $fieldtype = "Drop Down Select Menu";
                    if ((($row['is_object'] == 1) && ($row['is_object_master'] == NULL)) && ($row['label'] != "Charge for Each")) {
                        $endurl = ' | <a ' . LFD . ' href="../udf_edit_manager.php?fromadmin=1&editting=' . $row['object_master_id'] . ':' . $rowgroup['menutabid'] . '">Add / Change Drop Down Values</a>';
                    } elseif ($row['label'] == "Charge for Each") {
                        $endurl = '';
                    } else {
                        $endurl = ' | <a ' . LFD . ' href="../udf_edit_manager.php?fromadmin=1&editting=' . $row['hashid'] . ':' . $rowgroup['menutabid'] . '">Add / Change Drop Down Values</a>';
                    }
                } elseif ($row['fieldtype'] == 3) {
                    $fieldtype = "Checkbox";
                } elseif ($row['fieldtype'] == 4) {
                    $fieldtype = "Text Area";
                } elseif ($row['fieldtype'] == 5) {
                    $fieldtype = "Email Address";
                } elseif ($row['fieldtype'] == 6) {
                    $fieldtype = "Phone Number";
                } elseif ($row['fieldtype'] == 7) {
                    $fieldtype = "Mobile Number";
                } elseif ($row['fieldtype'] == 17) {
                    $fieldtype = "Image Upload";
                } elseif ($row['fieldtype'] == 18) {
                    $fieldtype = "Document Upload";
                } elseif ($row['fieldtype'] == 8) {
                    $fieldtype = "Date";
                } elseif ($row['fieldtype'] == 9) {
                    $fieldtype = "Date &amp; Time";
                } elseif ($row['fieldtype'] == 10) {
                    // Get the relations matrix id for editting
                    $datalinkrecordid = $row['linkedid'];
                    $endurl = " | <a " . LFD . " href='icon_manager.php?id=1:16:" . $row['hashid'] . "'>Add / Change Tab Icon</a>";
                    $queryico = "SELECT size, filename FROM icons_user WHERE id = " . $row['icon'] . " LIMIT 1";
                    // echo $query."<br>";
                    $resico = mysql_query($queryico);
                    $rowico = mysql_fetch_array($resico);

                    if ($row['icon']) {
                        $displayicon = '<table width="100%" border="0">
                                            <tr>
                                                <td align="right"><font class="note">Corresponding Menu Tab Icon:</font></td>
                                                <td width="5%"><img align="right" src="../images/icons/' . $rowico['size'] . '_shadow/' . $rowico['filename'] . '"></td>
                                            </table>';
                    }
                    $fieldtype = "Data Link Field";
                    $matrixvalue = $row['linkedid'];
                } elseif ($row['fieldtype'] == 11) {
                    $fieldtype = "<strong>Section Header</strong>";
                } elseif ($row['fieldtype'] == 12) {
                    $fieldtype = "Text Field - Header";
                } elseif ($row['fieldtype'] == 13) {
                    $fieldtype = "Currency";
                } elseif ($row['fieldtype'] == 14) {
                    $fieldtype = "Number";
                }
                ?>
                <tr>
                    <td class="ls_<?php echo $ls ?>"><?php echo $fieldtype; ?></td>
                    <td class="ls_<?php echo $ls ?>"><?php echo $fronturl . $row['label'] . $endurl; ?><?php echo $displayicon ?></td>
                    <td class="ls_<?php echo $ls ?>" align="center">
                        <?php
                        if ($row['fieldtype'] != 11) {
                            if (in_array(columnnames($row['label']), $keysarray)) { ?>
                                <a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=35:<?php echo $gval[1] ?>:::<?php echo $row['hashid'] ?>:<?php echo $row['sortorder']; ?>:<?php echo $row['location'] ?>:<?php echo columnnames($row['label']) ?>">Remove</a>
                                <?php
                            } else { ?>
                                <a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=33:<?php echo $gval[1] ?>:::<?php echo $row['hashid'] ?>:<?php echo $row['sortorder']; ?>:<?php echo $row['location'] ?>:<?php echo columnnames($row['label']) ?>">Create</a>
                                <?php
                            }
                        } else {
                            echo "&nbsp;";
                        } ?>
                    </td>
                    <td class="ls_<?php echo $ls; ?>">
                        <select style="font-family:Arial; font-size:11px"
                                name="sortorder_<?php echo $row['sortorder']; ?>" onChange="document.form1.submit()">
                            <?php
                            $idarray .= $row['id'] . ",";
                            for ($f = 1; $f <= $highest; $f++) {
                                if ($f == $row['sortorder']) {
                                    $selected = "selected";
                                } ?>
                                <option <?php echo $selected; ?> value="<?php echo $f; ?>"><?php echo $f; ?></option>
                                <?php
                                $selected = "";;
                            } ?>
                        </select>
                        <?php
                        if ($numcounter != 1) { ?>
                            <a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=19:<?php echo $gval[1] ?>:::<?php echo $row['hashid'] ?>:<?php echo $row['sortorder']; ?>:<?php echo $row['location'] ?>#<?php echo $rowgroup['menulabel'] . " - " . $rowgroup['menusublabel']; ?>">up</a>
                            <?php
                        }
                        if ($numcounter != $highest) { ?>
                        <a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=20:<?php echo $gval[1] ?>:::<?php echo $row['hashid'] ?>:<?php echo $row['sortorder']; ?>:<?php echo $row['location'] ?>#<?php echo $rowgroup['menulabel'] . " - " . $rowgroup['menusublabel']; ?>">down</a></font>
                    </td>
                    <?php
                    } ?>
                    <td class="ls_<?php echo $ls ?>" align="center">
                        <?php
                        if ($row['isactive'] == 0) { ?>
                            <?php
                            if (($row['fieldtype'] == 10) && ($row['tablabel'] == "")) { ?>
                                <a href="#go"
                                   onclick="alert('You need to assign a \'Corresponding Menu Tab Label\'\n to this field before activation. Please select edit and\nupdate the \'Corresponding Menu Tab Label\' field.');"><img
                                            src="../images/icons/media_play_green.png" title="Activate" border="0"></a>
                                <?php
                            } else { ?>
                                <a href="<?php $_SERVER['PHP_SELF'] ?>?id=15:<?php echo $gval[1] ?>:::<?php echo $row['hashid'] ?>#<?php echo $rowgroup['menulabel'] . " - " . $rowgroup['menusublabel']; ?>"><img
                                            src="../images/icons/media_play_green.png" title="Activate" border="0"></a>
                                <?php
                            }
                        } else { ?>
                            <a href="<?php $_SERVER['PHP_SELF'] ?>?id=16:<?php echo $gval[1] ?>:::<?php echo $row['hashid'] ?>#<?php echo $rowgroup['menulabel'] . " - " . $rowgroup['menusublabel']; ?>"><img
                                        src="../images/icons/media_play.png" title="Deactivate" border="0"></a>
                            <?php
                        } ?>

                        <?php
                        if ($row['iscompulsory'] == 0) {
                            ?>
                            <a href="<?php $_SERVER['PHP_SELF'] ?>?id=17:<?php echo $gval[1] ?>:::<?php echo $row['hashid'] ?>:1#<?php echo $rowgroup['menulabel'] . " - " . $rowgroup['menusublabel']; ?>"><img
                                        border="0" src="../images/icons/star_yellow.png"
                                        title="This is not a compulsory field"></a>
                            <?php
                        } else { ?>
                            <a href="<?php $_SERVER['PHP_SELF'] ?>?id=17:<?php echo $gval[1] ?>:::<?php echo $row['hashid'] ?>:0#<?php echo $rowgroup['menulabel'] . " - " . $rowgroup['menusublabel']; ?>"><img
                                        border="0" src="../images/icons/star_red.png"
                                        title="This is a compulsory field"></a>
                            <?php
                        } ?>

                        <?php
                        if ($row['fieldtype'] == 2) {
                            if ($row['locked'] == 1) { ?>
                                <a href="<?php $_SERVER['PHP_SELF'] ?>?id=38:<?php echo $gval[1] ?>:::<?php echo $row['hashid'] ?>:1#<?php echo $rowgroup['menulabel'] . " - " . $rowgroup['menusublabel']; ?>"><img
                                            border="0" src="../images/icons/lock.png" title="This field is locked"></a>
                                <?php
                            } else { ?>
                                <a href="<?php $_SERVER['PHP_SELF'] ?>?id=39:<?php echo $gval[1] ?>:::<?php echo $row['hashid'] ?>:0#<?php echo $rowgroup['menulabel'] . " - " . $rowgroup['menusublabel']; ?>"><img
                                            border="0" src="../images/icons/lock_open.png"
                                            title="This is not locked"></a>
                                <?php
                            }

                            if ($row['sharedvalues'] != 1) { ?>
                                <a href="<?php $_SERVER['PHP_SELF'] ?>?id=40:<?php echo $gval[1] ?>:::<?php echo $row['hashid'] ?>:1#<?php echo $rowgroup['menulabel'] . " - " . $rowgroup['menusublabel']; ?>"><img
                                            border="0" src="../images/icons/checks.png"
                                            title="These field values are not shared"></a>
                                <?php
                            } else { ?>
                                <a href="<?php $_SERVER['PHP_SELF'] ?>?id=41:<?php echo $gval[1] ?>:::<?php echo $row['hashid'] ?>:0#<?php echo $rowgroup['menulabel'] . " - " . $rowgroup['menusublabel']; ?>"><img
                                            border="0" src="../images/icons/checks_r.png"
                                            title="These field values are shared"></a>
                                <?php
                            }

                        } else {
                            echo '<img src="../images/icons/lock_open_g.png" title="This setting does not apply to this field." />	';
                            echo '<img src="../images/icons/checks_g.png" title="This setting does not apply to this field." />	';
                        } ?>

                        <?php
                        if ($row['inlists'] == 0) {
                            ?>
                            <a href="<?php $_SERVER['PHP_SELF'] ?>?id=21:<?php echo $gval[1] ?>:::<?php echo $row['hashid'] ?>:1#<?php echo $rowgroup['menulabel'] . " - " . $rowgroup['menusublabel']; ?>"><img
                                        border="0" src="../images/icons/inlist_no.png"
                                        title="This field is not shown in lists"></a>
                            <?php
                        } else { ?>
                            <a href="<?php $_SERVER['PHP_SELF'] ?>?id=21:<?php echo $gval[1] ?>:::<?php echo $row['hashid'] ?>:0#<?php echo $rowgroup['menulabel'] . " - " . $rowgroup['menusublabel']; ?>"><img
                                        border="0" src="../images/icons/inlist.png"
                                        title="This field is shown in lists"></a>
                            <?php
                        } ?>

                        <?php
                        if ($row['showonnew'] == 1) { ?>
                            <a href="<?php $_SERVER['PHP_SELF'] ?>?id=45:<?php echo $gval[1] ?>:::<?php echo $row['hashid'] ?>"><img
                                        src="../images/icons/16_shadow/window_dialog.png"
                                        title="Shown when creating new record"/></a>
                            <?php
                        } else { ?>
                            <a href="<?php $_SERVER['PHP_SELF'] ?>?id=46:<?php echo $gval[1] ?>:::<?php echo $row['hashid'] ?>"><img
                                        src="../images/icons/16_shadow/window_dialog_g.png"
                                        title="Not shown when creating new record"/></a>
                            <?php
                        } ?>

                        <?php
                        if ($row['caneditvalues'] == 1) { ?>
                            <a href="<?php $_SERVER['PHP_SELF'] ?>?id=47:<?php echo $gval[1] ?>:::<?php echo $row['hashid'] ?>"><img
                                        src="../images/icons/16_shadow/pen_red.png"
                                        title="Can change record value"/></a>
                            <?php
                        } else { ?>
                            <a href="<?php $_SERVER['PHP_SELF'] ?>?id=48:<?php echo $gval[1] ?>:::<?php echo $row['hashid'] ?>"><img
                                        src="../images/icons/16_shadow/pen_red_g.png"
                                        title="Cannot change record value"/></a>
                            <?php
                        } ?>

                        <?php
                        if ($row['can_edit_field'] == 1) { ?>
                            <a href="<?php $_SERVER['PHP_SELF'] ?>?id=14:<?php echo $gval[1] ?>:2:<?php echo $datalinkrecordid ?>:<?php echo $row['hashid'] ?>"><img
                                        src="../images/icons/document_edit.png" title="Edit" border="0"></a>
                            <a onclick="return confirm('WARNING\nAre you sure you want to delete this user defined field? This action cannot be undone.\nAll values related to this user defined field will also be deleted.')"
                               href="<?php echo $_SERVER['PHP_SELF'] ?>?id=18:<?php echo $gval[1] ?>:::<?php echo $row['hashid'] ?>:<?php echo $matrixvalue ?>#<?php echo $rowgroup['menulabel'] . " - " . $rowgroup['menusublabel']; ?>"><img
                                        src="../images/icons/delete2_16.png" title="Delete" border="0"></a>
                            <?php
                        } else { ?>
                            <img src="../images/icons/document_edit_g.png" title="This is System Dependant Field"
                                 border="0">
                            <img src="../images/icons/delete2_16_g.png" title="This is System Dependant Field"
                                 border="0">
                            <?php
                        } ?>
                    </td>
                </tr>
                <?php
                $numcounter++;
                unset($fronturl, $matrixvalue, $endurl, $displayicon, $datalinkrecordid);
            } ?>
        </table>
        <input type="hidden" name="hiddensortvalues" value="<?php echo($numcounter - 1); ?>">
        <input type="hidden" name="idarray" value="<?php echo $idarray; ?>">
        <input type="hidden" name="pform" value="7">
    </form>
<br>
<?php
}
elseif ($gval[0] == 28) {
if ($gval[2] == 1) { ?>
    <script language="Javascript">
        var reloadselect_menu = parent.select_menu.location.reload();
    </script>
<?php
} ?>
<?php
// Lets set the default value.
// echo $gval[1];
if ($_POST['savebtn'] == "Save Default") {
    /// Set default and compulsory fields
    $query = "UPDATE udf_multi_value SET isdefault = 0 WHERE registrantid = " . RID . " AND udfdefid = '" . $gval[1] . "'";
    // echo $query."<br>";
    mysql_query($query) Or Die ("Cannot submit entry!dgbdsw");

    $query = "UPDATE udf_multi_value SET isdefault = 1 WHERE registrantid = " . RID . " AND id = " . $_POST['isdefault'];
    // echo $query;
    mysql_query($query) Or Die ("Cannot submit entry!oijh");
}

if ($_POST['processsubmit'] == 1) {
    $query = "INSERT INTO udf_multi_value (registrantid, udfdefid, value) VALUES (
				'" . RID . "',
				'" . $gval[1] . "',
				'" . $_POST['addvalue'] . "'
			  	)";
    // echo $query;
    mysql_query($query) Or Die ("Cannot submit entry!mjues");
}

if ($gval[3] == 1) {
    // Let's see if the value already exists before we delete it.
    $querydel = "DELETE FROM udf_multi_value WHERE md5(id) = '" . strtolower($gval[2]) . "' LIMIT 1";
    // echo $querydel;
    mysql_query($querydel) Or Die ("Cannot submit entry!ngzlk");
} ?>
    <script language="JavaScript" type="text/javascript">
        <!--
        function checkform(form) {
            if (form.addvalue.value == "") {
                alert("Please fill in a value.");
                form.addvalue.focus();
                return false;
            }
            return true;
        }

        //-->
    </script>
    <table border="0" width="95%" cellpadding="2" cellspacing="3">
        <?php
        $query = "SELECT label, hashid FROM udf_definitions WHERE hashid = '" . $gval[1] . "' AND registrantid =" . RID . " LIMIT 1";
        // echo $query;
        $res = mysql_query($query);
        $row = mysql_fetch_array($res); ?>
        <tr>
            <?php
            if ($gval[2] == 2) { ?>
                <form method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?>?id=29:<?php echo $row['hashid'] ?>">
                    <td width="20%"><input type="text" class="standardfield_large" name="newlabel"
                                           value="<?php echo $row['label'] ?>"></td>
                    <td><input type="submit" value="Save Change"></td>
                    <td width="45%"><font class="noteg">Changing the label in the <em>Object Manager</em> will change
                            the label across all forms where this object is present.</font></td>
                    <td width="5%">&nbsp;</td>
                </form>
                <?php
            } else { ?>
                <td><h1><?php echo $row['label'] ?></h1></td>
                <?php
            } ?>
            <td class="messageboard" width="15%" align="center">
                <a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=28:<?php echo $gval[1] ?>:2">Change Label</a>
            </td>
        </tr>
    </table><br/>
<br/>
<?php
//echo "Editting: ".$_GET['editting']."<br>";
//echo "Help: ".$_GET['help']."<br>";
function tablebody($icon, $helpheader, $helpdtext) { ?>
    <table cellspacing="0" cellpadding="5" border="1" width="100%" height="300">
        <tr>
            <td colspan="4" height="5"><img src="../images/spacer.gif" height="5" width="1"></td>
        </tr>
        <tr>
            <td valign="top" align="right" rowspan="2" width="5%">
                <img src="../images/icons/<?php echo $icon ?>"><img src="../images/spacer.gif" height="1"
                                                                    width="10"><br>
                <img src="../images/spacer.gif" height="55" width="1">
            </td>
            <td valign="top" height="5">
                <h1><?php echo $helpheader ?></h1>
            </td>
            <td rowspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td valign="top" height="100%">
                <?php echo $helpdtext ?><br>
            </td>
        </tr>
    </table>
<?php
}

function pulltablevalue($fieldname, $fieldtitle) {
$editdetail = explode(":", $_GET['editting']);
$query = "SELECT * FROM udf_multi_value WHERE udfdefid = '" . $fieldname . "' ORDER BY value ASC";
//echo $query;
$tUnixTime = time();
$sGMTMySqlString = date("Y-m-d H:i:s", $tUnixTime);
$result = mysql_query($query) or die ("Cannot find value"); ?>
    <table cellspacing="0" cellpadding="5" border="0" width="95%">
        <tr>
            <td valign="top">
                <form method="POST" name="addvalue" onsubmit="return checkform(this);" action=""
                      <?php echo $_SERVER['PHP_SELF'] ?>.php?28:<?php echo $gval[1] ?>">
                <table border="0" cellpadding="0" cellspacing="3" width="100%">
                    <tr>
                        <td width="25%" class="ls_on">
                            Add New Value:<br><input type="text" maxlength="40" name="addvalue"
                                                     class="standardfield"><br>
                        </td>
                        <td width="15%" class="ls_on" align="center"><input type="submit" value="Add Value"
                                                                            title="Click here to add this value."></td>
                        <td width="1%">&nbsp;</td>
                        <td width="50%" class="ls_off" valign="top">
                            <font class="noteg">
                                To create new Drop Down values, simply enter a value and click the Add Value
                                button.<br/>
                                If want a user to select an option, create a new value called <em>Select One</em> and
                                set
                                this value as the default. You can make this field <em>Compulsory</em> in the Form
                                Manager.
                            </font>
                        </td>
                    </tr>
                </table>
                <input type="hidden" name="processsubmit" value="1">
                </form>
                <form method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?>?id=28:<?php echo $fieldname ?>&action=e"
                      name="form_<?php echo $divnum1 ?>">
                    <script>
                        function confirmDelete(delUrl) {
                            if (confirm("Are you sure you want to delete this value.")) {
                                document.location = delUrl;
                            }
                        }

                        function show_hide(sh1, hi1) {
                            document.getElementById(hi1).style.display = 'none';
                            obj = document.getElementById(sh1)
                            obj.style.display == "block" ? obj.style.display = "block" : obj.style.display = "block"; //  {
                        }
                    </script>
                    <table cellpadding="0" cellspacing="2" width="100%" border="0">
                        <tr>
                            <td colspan="4">
                                <table cellpadding="0" cellspacing="2" width="100%" border="0">
                                    <tr>
                                        <td align="left" width="3%">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td align="right"><input type="submit"
                                                                 style="font-family:tahoma;font-size:11px;padding:3px"
                                                                 name="savebtn" value="Save Default"><br><br></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr align="center">
                            <td width="45%" class="ls_top"><strong>Value</strong></td>
                            <td width="25%" class="ls_top"><strong>Default</strong></td>
                            <td width="15%" class="ls_top"><strong>Delete</strong></td>
                        </tr>
                        <?php
                        $classcounter = 2;
                        $divnum1 = 1;
                        $divnum2 = 2;
                        while ($row = mysql_fetch_array($result)) {
                            include("..//includes/ls.php"); ?>
                            <script>
                                function submitform_<?php echo $divnum1 ?>() {
                                    document.form_<?php echo $divnum1 ?>.submit();
                                }
                            </script>
                            <tr>
                                <td class="ls_<?php echo $ls ?>">
                                    <div class="displayadress_2">
							<span id="s_<?php echo $divnum1 ?>">
								<?php echo ucfirst($row['value']); ?>
							</span>
                                    </div>
                                </td>
                                <?php
                                if ($row['isdefault'] == 1) {
                                    $checkingmeout = "checked";
                                } else {
                                    $checkingmeout = "";
                                } ?>
                                <td align="center" class="ls_<?php echo $ls ?>"><input <?php echo $checkingmeout; ?>
                                            type="radio" name="isdefault" value="<?php echo $row['id']; ?>"></td>
                                <td align="center" width="5%" class="ls_<?php echo $ls ?>">
                                    <a href="javascript:confirmDelete('<?php echo $_SERVER['PHP_SELF'] ?>?id=28:<?php echo $fieldname ?>:<?php echo strtoupper(md5($row['id'])) ?>:1')"><img
                                                src="../images/icons/note_delete_16.png" border="0"></a>
                                </td>
                            </tr>
                            <?php
                            $divnum1 = $divnum1 + 10;
                            $divnum2 = $divnum2 + 10;
                        } ?>
                        <tr>
                            <td colspan="4">
                                <table cellpadding="0" cellspacing="2" width="100%" border="0">
                                    <tr>
                                        <td align="left" width="3%">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td align="right"><br><br style="line-height:2px"><input type="submit"
                                                                                                 style="font-family:tahoma;font-size:11px;padding:3px"
                                                                                                 name="savebtn"
                                                                                                 value="Save Default"><br><br>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
            </td>
        </tr>
        </form>
    </table>
    <?php
}
    // First we need to see which drop down value we are dealing with...
    $mysql = "SELECT label FROM udf_definitions WHERE hashid = '" . $editdetail[0] . "' AND registrantid=" . RID;
    //echo $mysql;
    $res = mysql_query($mysql);
    $row = mysql_fetch_array($res);

    pulltablevalue($gval[1]);

    ?>
    <?php
}
elseif ($gval[0] == "notice1") {
    include_once 'create-system-notices.php';
} elseif ($gval[0] == "notice2") {
    include_once 'system-notices.php';
} ?>
</body>
</html>
