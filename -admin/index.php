<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "includes/xhtml1-transitional.dtd">
<?php include_once("..//_globalconnect.php");
date_default_timezone_set('Europe/London');
?>
<html>
<head>
	<title>Admin Section</title>
</head>
<!-- frames -->
<frameset rows="32px,*">
    <frame name="top_menu" src="top_menu.php" marginwidth="10" marginheight="10" noresize scrolling="no" frameborder="0">
    <frameset rows="95%,*">
        <frameset cols="390px,*">
            <frame name="select_menu" src="select_menu.php" marginwidth="10" marginheight="10" scrolling="auto" frameborder="0">
            <frame name="main_body" src="main_body.php" marginwidth="10" marginheight="10" scrolling="auto" frameborder="0">
        </frameset>
        <frame name="footer_menu" src="footer_menu.php" noresize marginwidth="10" marginheight="10" scrolling="no" frameborder="0">
		<noframes>
			<body>Your browser does not handle frames!</body>
		</noframes>
    </frameset>
</frameset>
</body>
</html>