<?php
if (isset($_GET['activate'])) {
    $sql = "UPDATE `notices` SET `active` = '".mysql_real_escape_string($_GET['state'])."' WHERE `notices`.`id` = '".mysql_real_escape_string($_GET['activate'])."';";
    //echo $sql;
    mysql_query($sql);
    header("Location: main_body.php?id=notice2");
}

if (isset($_GET['delete'])) {
     $sql = "DELETE FROM `notices` WHERE `notices`.`id` = '".mysql_real_escape_string($_GET['delete'])."';";
    //echo $sql;
    mysql_query($sql);
    header("Location: main_body.php?id=notice2");
}

?>
<table border="0" width="95%" cellpadding="2" cellspacing="3" class="note">
    <tr>
        <td colspan=2><h1>System Notices</h1><br /></td>
    </tr>
    <tr>
	<td class="ls_top" width="5%"><strong>ID</strong></td>
	<td class="ls_top" width="15%"><strong>Date</strong></td>
	<td class="ls_top" width="25%"><strong>Subject</strong></td>
	<td class="ls_top"><strong>Notice</strong></td>
	<td class="ls_top"><strong>Active</strong></td>
	<td class="ls_top" width="15%"><strong>Actions</strong></td>
    </tr>
    <?php
    $sql = "SELECT * FROM `notices` ORDER BY id DESC;";
    //echo $sql;
    $res = mysql_query($sql);
    while ($row = mysql_fetch_array($res)) {
        include("..//includes/ls.php"); ?>
        <tr>
            <td class="ls_<?php echo $ls ?>"><?php echo $row['id'];?></td>
            <td class="ls_<?php echo $ls ?>"><?php echo date("d M Y", strtotime($row['datecreated']));?></td>
            <td class="ls_<?php echo $ls ?>"><?php echo $row['subject'];?></td>
            <td class="ls_<?php echo $ls ?>"><?php echo $row['notice'];?></td>
            <td class="ls_<?php echo $ls ?>"><?php echo $row['active'];?></td>
            <td class="ls_<?php echo $ls ?>">
                <?php
                if ($row['active'] == "No") { ?>
                    <a href="main_body.php?id=notice2&activate=<?php echo $row['id'];?>&state=Yes">Activate</a>
                <?php
                } else { ?>
                    <a href="main_body.php?id=notice2&activate=<?php echo $row['id'];?>&state=No">Deactivate</a>
                <?php
                }
				if ($row['emailed'] == "No") { ?>
					| <a href="email-notification.php?id=<?php echo $row['id'];?>" onclick="return confirm('Are you sure?')">Email</a>
				<?php
				} else { ?>
					| Emailed
				<?php
				} ?>
                 | <a onclick="return confirm('Are you sure you want to delete this notice?');" href="main_body.php?id=notice2&delete=<?php echo $row['id'];?>">Delete</a>
            </td>
        </tr>
    <?php
    } ?>
</table>