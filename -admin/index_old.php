<?php 
include_once("../_globalconnect.php");
if ($_POST['pform'] == 1) {
	// Create a new top level module
	
	if ($_POST['subcategories'] == 1) {
	
		$query = "INSERT INTO menu_tabs (registrantid, label, type, datecreated) VALUES (
			  ".RID.",
			  '".$_POST['label']."',
			  '".$_POST['type']."',
			  '".$sGMTMySqlString."'
			  )";
		mysql_query($query) Or Die ("Cannot submit entry! 1");
		
		// Set Hashid - pass table name & Update the ordering
		sethashidandOrder("menu_tabs");
		
		/////////////////////////////////////////////////////////////////////////////////
		// Now the tab has been created we may have to create some standard sub modules//
		/////////////////////////////////////////////////////////////////////////////////
				
		// First type is Calendar //
		if ($_POST['type'] == "Calendar") {
			$arr = array("my calendar", "my activity list","schedule overview");
			createSubTabs($arr,$sGMTMySqlString);
		} elseif ($_POST['type'] == "Document Store") {
			$arr = array("document list", "document search");
			createSubTabs($arr,$sGMTMySqlString);
		} elseif ($_POST['type'] == "Standard Form") {
			$arr = array("list", "new","search");
			createSubTabs($arr,$sGMTMySqlString,0,0,$_POST['numberseries']);
		}
		
	} else {
		$query = "INSERT INTO menu_tabs (registrantid, label, type, ismultitier, datecreated) VALUES (
				  ".RID.",
				  '".$_POST['label']."',
				  '".$_POST['type']."',
				  1,
				  '".$sGMTMySqlString."'
				  )";
			mysql_query($query) Or Die ("Cannot submit entry! 1");
			
		header("Location:".$_SERVER['PHP_SELF']."?id=13:".sethashidandOrder("menu_tabs").":".$_POST['subcategories']."");
	}
	
	// No redirect as we will now load the Label creator for multi-tier.
	
} elseif ($_POST['pform'] == 2) {
	// Update a top level module
	$query = "UPDATE menu_tabs SET label = '".$_POST['label']."', type = '".$_POST['type']."' WHERE hashid = '".$_POST['hashid']."' AND registrantid=".RID;
	mysql_query($query) Or Die ("Cannot submit entry!");
    header("Location:".$_SERVER['PHP_SELF']."?id=1");
} elseif ($_POST['pform'] == 3) {
	// Update a sub level tab
	$query = "UPDATE menusub_tabs SET label = '".$_POST['label']."' WHERE hashid = '".$_POST['hashid']."' AND registrantid=".RID;
	mysql_query($query) Or Die ("Cannot submit entry!");
    header("Location:".$_SERVER['PHP_SELF']."?id=8:".$_POST['menu_tabhashid']."");
} elseif ($_POST['pform'] == 4) {
	/// Create sub multitier tabs
	$arr = array("list", "new","search");
	createSubTabs($arr,$sGMTMySqlString,$_POST['label_1'],$_POST['label_2'],$_POST['numberseries_1'],$_POST['numberseries_2'],$_POST['label_singular_1'],$_POST['label_singular_2']);
	header("Location: ".$_SERVER['PHP_SELF']."?id=1");
} elseif ($_POST['pform'] == 5) {
	
	// Lets get the highest sort value and add one more
	$menutabdata = explode("&&",$_POST['udfmodule']);
	$query = "SELECT MAX(sortorder) AS EditID FROM udf_definitions WHERE location = '".$menutabdata[1]."'AND registrantid = ".RID;
	$result = mysql_query($query);
	$row = mysql_fetch_array($result);
	$sortid = ($row['EditID']+1);
	
	// Lets see if this field requires linking
	if ($_POST['datalinktable'] != "") { $islinked = 1; } else { $islinked = 0; }
	
	// If it is a section header we want to set the 
	// single row value to 1
	if ($_POST['udftype'] == 12) { 
		$singlelargeval = 1; 
		$singlerowval = 1; 
	} elseif ($_POST['udftype'] == 11) { 
		$singlelargeval = 0; 
		$singlerowval = 1; 
	} else {
		$singlelargeval = 0; 
		$singlerowval = 0; 
	}
	$query = "INSERT INTO udf_definitions (registrantid, fieldtype, columntype, label, location, sortorder, inlists, islinked, tablabel, singlerow, singlelarge, isactive) VALUES (
			".RID.",
			".$_POST['udftype'].",
			'".$_POST['columntype']."',
			'".str_replace(":","",$_POST['udflabel'])."',
			'".$menutabdata[1]."',
			".$sortid.",
			'".$_POST['inlists']."',
			'".$islinked."',
			'".$_POST['tablabel']."',
			".$singlerowval.",
			".$singlelargeval.",
			0
			)";
	// echo $query."<br>";
	mysql_query($query) or die ("Cannot create user defined field");
		
	$query = "SELECT MAX(id) AS EditID FROM udf_definitions";
	$result = mysql_query($query);
	$row = mysql_fetch_array($result);
	$udfdefid = strtoupper(md5($row['EditID']));
		
	// Set the hash so we don't have to repeat the do while statement all the time
	$query = "UPDATE udf_definitions SET hashid='".$udfdefid."' WHERE id=".$row['EditID'];
	mysql_query($query) Or Die ("Cannot submit entry! 1");
	
	$sql = "ALTER TABLE udf_".$menutabdata[1]." ADD ".columnnames($_POST['udflabel'])." ".$_POST['columntype']." NULL;";
	//echo $sql;
	mysql_query($sql) or die ("Cannot alter table");
	
	// Lets build the relationship matrix
	if ($_POST['datalinktable'] != "") { 
		$datalinkdata = explode("&&",$_POST['datalinktable']);
		
		$querysss = "SELECT MAX(sortorder) AS EditID FROM relations_matrix WHERE parenttable  = '".$datalinkdata[0]."'AND registrantid = ".RID;
		$resultsss = mysql_query($querysss);
		$rowsss = mysql_fetch_array($resultsss);
		$sortidsss = ($rowsss['EditID']+1);
		
		$relsql = "INSERT INTO relations_matrix (registrantid, menutabid, parenttable, childtable, ispredefined, labelhashid, udf_columnid, sortorder) VALUES (
					".RID.",
					'".$menutabdata[0]."',
					'".$datalinkdata[0]."',
					'".$menutabdata[1]."',
					0,
					'".$udfdefid."',
					'".$datalinkdata[1]."',
					".$sortidsss."
					)";
		mysql_query($relsql);
	
		$queryl = "SELECT MAX(id) AS EditID FROM relations_matrix";
		$resultl = mysql_query($queryl);
		$rowl = mysql_fetch_array($resultl);
	
		$queryll = "UPDATE udf_definitions SET linkedid='".$rowl['EditID']."' WHERE id=".$row['EditID'];
		mysql_query($queryll) Or Die ("Cannot submit entry! 1");
	}
	header("Location: ".$_SERVER['PHP_SELF']."?id=14");
	
} elseif ($_POST['pform'] == 6) {
	$menutabdata = explode("&&",$_POST['udfmodule']);
		$query = "UPDATE udf_definitions SET 
			 fieldtype = ".$_POST['udftype'].", 
			 label = '".$_POST['udflabel']."',
			 location = '".$menutabdata[1]."',
			 tablabel = '".$_POST['tablabel']."',
			 inlists = '".$_POST['inlists']."'
			 WHERE hashid = '".$_POST['hashid']."' 
			 AND registrantid = ".RID."
			 ";
	//echo $query."<br><br>";
	mysql_query($query) Or Die ("Cannot submit entry! 1254");
		
	$sql ="ALTER TABLE udf_".$menutabdata[1]." CHANGE ".$_POST['redundantcolumnname']." ".columnnames($_POST['udflabel'])." ".$_POST['columntype']." NULL DEFAULT NULL";
	// echo $sql."<br><br>";
	mysql_query($sql);
	
	$datalinkdata = explode("&&",$_POST['datalinktable']);
	$relsql = "UPDATE relations_matrix SET 
					menutabid = '".$menutabdata[0]."',
					parenttable ='".$datalinkdata[0]."',
					childtable = '".$menutabdata[1]."',
					udf_columnid ='".$datalinkdata[1]."'
					WHERE id = ".$gval[3]." 
			 		AND registrantid = ".RID."
					";
	// echo $relsql."<br>";
	mysql_query($relsql);
	
	header("Location: ".$_SERVER['PHP_SELF']."?id=14::3");
}

if ($gval[0] == 2) {
	$query = "UPDATE menu_tabs SET isactive = 1 WHERE hashid = '".$gval[1]."' AND registrantid=".RID;
	mysql_query($query) Or Die ("Cannot submit entry!");
    header("Location:".$_SERVER['PHP_SELF']."?id=1");
} elseif ($gval[0] == 3) {
	// Deactive a top level tab
	$query = "UPDATE menu_tabs SET isactive = 0 WHERE hashid = '".$gval[1]."' AND registrantid=".RID;
	mysql_query($query) Or Die ("Cannot submit entry!");
		// Deactive a sub level tab
		$query = "UPDATE menusub_tabs SET isactive = 0 WHERE menu_tabhashid = '".$gval[1]."' AND registrantid=".RID;
		mysql_query($query) Or Die ("Cannot submit entry!");
    header("Location:".$_SERVER['PHP_SELF']."?id=1");
} elseif ($gval[0] == 3) {
	// Active a top level tab
	$query = "UPDATE menu_tabs SET isactive = 0 WHERE hashid = '".$gval[1]."' AND registrantid=".RID;
	mysql_query($query) Or Die ("Cannot submit entry!");
    header("Location:".$_SERVER['PHP_SELF']."?id=1");
} elseif ($gval[0] == 5) {
	// Let's get the sort order value before deleting it so we can,
	// if required, update subsequent values.
	$sqlmulti = "SELECT type, sortorder FROM menu_tabs WHERE hashid  = '".$gval[1]."' AND registrantid=".RID;
	//echo $sqlmulti."<br	>";
	$res = mysql_query($sqlmulti);
	$row = mysql_fetch_array($res);
	$deletedsortid = $row['sortorder'];
	$menutabtype = $row['type'];
	//echo $deletedsortid."<br>";
	
	$sqlmulti = "SELECT hashid FROM menusub_tabs WHERE menu_tabhashid = '".$gval[1]."' AND type='new' AND registrantid=".RID;
	$res = mysql_query($sqlmulti);
	while ($row = mysql_fetch_array($res)) {
		$deletedsortidlocation = $row['hashid'];
		// Delete the related columns
		$specials = array(" ",":",";","�","`","�","!","�","$","%","^","&","*","(",")","_","_","+","=","{","}","[","]","@","'","~","#","|","\\","<",",",">","?","/");
		$deletedsortidlocation = str_replace(' ',' ',str_replace($specials,'',$deletedsortidlocation));
		$sql = "DROP TABLE udf_".$deletedsortidlocation."";
		// echo $sql."<br><br>";
		mysql_query($sql);
		
		// Delete from udf definitions
		$sqludf = "DELETE FROM udf_definitions WHERE location ='".$deletedsortidlocation."' AND registrantid=".RID;
		//echo $sqludf."<br>";
		mysql_query($sqludf) or die ('Dead4');
	}
	
	// Delete all the activites and related notes
	$sqludf = "DELETE FROM menu_tabs WHERE registrantid=".RID." AND hashid='".$gval[1]."' LIMIT 1";
	//echo $sqludf."<br><br>";
	mysql_query($sqludf) or die ('Dead1');
	
	// Delete the top level tabs
	if ($menutabtype == "Calendar") {
		$sqludf = "DELETE FROM activities WHERE registrantid=".RID." AND menutabid ='' AND location = ''";
		//echo $sqludf."<br><br>";
		 mysql_query($sqludf) or die ('Dead1');
	} elseif ($menutabtype == "Standard Form") {
		$sqludf = "DELETE FROM activities WHERE registrantid=".RID." AND menutabid ='".$gval[1]."'";
		//echo $sqludf."<br><br>";
		 mysql_query($sqludf) or die ('Dead1');
		
		$sqludf = "DELETE FROM activities_notes WHERE menutabid ='".$gval[1]."' AND registrantid =".RID;
		//echo $sqludf."<br><br>";
		 mysql_query($sqludf) or die ('Dead1');
		
		$sqludf = "DELETE FROM messaging WHERE menutabid ='".$gval[1]."' AND registrantid =".RID;
		//echo $sqludf."<br><br>";
		 mysql_query($sqludf) or die ('Dead1');
		
		$sqludf = "DELETE FROM relations_matrix WHERE menutabid ='".$gval[1]."' AND registrantid =".RID;
		//echo $sqludf."<br><br>";
		 mysql_query($sqludf) or die ('Dead1');
		
		$sqludf = "DELETE FROM relations_values WHERE menutabid ='".$gval[1]."' AND registrantid =".RID;
		//echo $sqludf."<br><br>";
		 mysql_query($sqludf) or die ('Dead1');
		
		$sqludf = "DELETE FROM address_info WHERE menuhashid ='".$gval[1]."' AND registrantid =".RID;
		//echo $sqludf."<br><br>";
		mysql_query($sqludf) or die ('Dead1');
		
		$sqludf = "DELETE FROM udf_multi_value WHERE menutabid ='".$gval[1]."' AND registrantid =".RID;
		//echo $sqludf."<br><br>";
		mysql_query($sqludf) or die ('Dead1');
	}

	// Delete the sub level tabs
	$sqludf = "DELETE FROM menusub_tabs WHERE menu_tabhashid ='".$gval[1]."' AND registrantid=".RID;
	//echo $sqludf."<br><br>";
	mysql_query($sqludf) or die ('Dead2');

	// Before we delete the UDF data we need to remove all the
	// udf_multi_value data first
	$sqlmulti = "SELECT hashid FROM udf_definitions WHERE location  ='".$gval[1]."' AND fieldtype = 2 AND registrantid=".RID;
	$res = mysql_query($sqlmulti);
	while ($row = mysql_fetch_array($res)) {
		// Delete from udf multi
		$sqludf = "DELETE FROM udf_multi_value WHERE udfdefid ='".$row['hashid']."' AND registrantid=".RID;
		//echo $sqludf."<br><br>";
		mysql_query($sqludf) or die ('Dead3');
	}
	
	// Update sortorder
	$sqlmulti = "SELECT MAX(sortorder) AS maxsortorder FROM menu_tabs WHERE registrantid=".RID;
	$res = mysql_query($sqlmulti);
	$row = mysql_fetch_array($res);
	$deleteddiff = $row['maxsortorder'] - $deletedsortid;
	
	$newsortnum = ($deletedsortid + 1);
	for ($number = 0; $number < $deleteddiff; $number++) { 
		$query = "UPDATE menu_tabs SET sortorder = ".$deletedsortid." WHERE sortorder = ".$newsortnum." AND registrantid=".RID;
		//echo $query."<br><br>";
		mysql_query($query);
		$deletedsortid++;
		$newsortnum++;
	}
	
	header("Location:".$_SERVER['PHP_SELF']."?id=1");
	
} elseif ($gval[0] == 6) {
	// Move the value up the order
	$numtochange = ($gval[2]-1);
	
	$query = "UPDATE menu_tabs SET sortorder = ".$numtochange." WHERE hashid = '".$gval[1]."' AND registrantid=".RID;
	//echo $query."<br>";
	mysql_query($query) Or Die ("Cannot submit entry!");
	
	$query = "UPDATE menu_tabs SET sortorder = ".$gval[2]." WHERE sortorder = ".$numtochange." AND hashid != '".$gval[1]."' AND registrantid=".RID;
	//echo $query."<br>";
	mysql_query($query) Or Die ("Cannot submit entry!");
	
    header("Location:".$_SERVER['PHP_SELF']."?id=1");
} elseif ($gval[0] == 7) {
	// Move the value down the order
	$numtochange = ($gval[2]+1);
	
	$query = "UPDATE menu_tabs SET sortorder = ".$numtochange." WHERE hashid = '".$gval[1]."' AND registrantid=".RID;
	//echo $query."<br>";
	mysql_query($query) Or Die ("Cannot submit entry!");
	
	$query = "UPDATE menu_tabs SET sortorder = ".$gval[2]." WHERE sortorder = ".$numtochange." AND hashid != '".$gval[1]."' AND registrantid=".RID;
	//echo $query."<br>";
	mysql_query($query) Or Die ("Cannot submit entry!");
	
    header("Location:".$_SERVER['PHP_SELF']."?id=1");
} elseif ($gval[0] == 9) {
	// Move the value up the order
	$numtochange = ($gval[2]-1);
	
	$query = "UPDATE menusub_tabs SET sortorder = ".$numtochange." WHERE hashid = '".$gval[1]."' AND registrantid=".RID;
	//echo $query."<br>";
	mysql_query($query) Or Die ("Cannot submit entry!");
	
	$query = "UPDATE menusub_tabs SET sortorder = ".$gval[2]." WHERE sortorder = ".$numtochange." AND hashid != '".$gval[1]."' AND registrantid=".RID;
	//echo $query."<br>";
	mysql_query($query) Or Die ("Cannot submit entry!");
	
    header("Location:".$_SERVER['PHP_SELF']."?id=8:".$gval[3]."");
} elseif ($gval[0] == 10) {
	// Move the value down the order
	$numtochange = ($gval[2]+1);
	
	$query = "UPDATE menusub_tabs SET sortorder = ".$numtochange." WHERE hashid = '".$gval[1]."' AND registrantid=".RID;
	//echo $query."<br>";
	mysql_query($query) Or Die ("Cannot submit entry!");
	
	$query = "UPDATE menusub_tabs SET sortorder = ".$gval[2]." WHERE sortorder = ".$numtochange." AND hashid != '".$gval[1]."' AND registrantid=".RID;
	echo $query."<br>";
	mysql_query($query) Or Die ("Cannot submit entry!");
	
    header("Location:".$_SERVER['PHP_SELF']."?id=8:".$gval[3]."");
} elseif ($gval[0] == 11) {
	// Active a sub level tab
	$query = "UPDATE menusub_tabs SET isactive = 1 WHERE hashid = '".$gval[1]."' AND registrantid=".RID;
	mysql_query($query) Or Die ("Cannot submit entry!");
    header("Location:".$_SERVER['PHP_SELF']."?id=8:".$gval[2]."");
} elseif ($gval[0] == 12) {
	// Deactive a sub level tab
	$query = "UPDATE menusub_tabs SET isactive = 0 WHERE hashid = '".$gval[1]."' AND registrantid=".RID;
	mysql_query($query) Or Die ("Cannot submit entry!");
    header("Location:".$_SERVER['PHP_SELF']."?id=8:".$gval[2]."");
} elseif ($gval[0] == 15) {
	$query = "UPDATE udf_definitions SET isactive = 1 WHERE hashid = '".$gval[1]."' AND registrantid=".RID;
	//echo $query;
	mysql_query($query) Or Die ("Cannot submit entry!");
    header("Location:".$_SERVER['PHP_SELF']."?id=14::3");
} elseif ($gval[0] == 16) {
	$query = "UPDATE udf_definitions SET isactive = 0 WHERE hashid = '".$gval[1]."' AND registrantid=".RID;
	mysql_query($query) Or Die ("Cannot submit entry!");
	//echo $query;
	header("Location:".$_SERVER['PHP_SELF']."?id=14::3");
}  elseif ($gval[0] == 17) {
	$query = "UPDATE udf_definitions SET iscompulsory = ".$gval[2]." WHERE hashid = '".$gval[1]."' AND registrantid=".RID;
	mysql_query($query) Or Die ("Cannot submit entry!");
	//echo $query;
	
	header("Location:".$_SERVER['PHP_SELF']."?id=14::3");
} elseif ($gval[0] == 18) {
	// Let's get the sort order value before deleting it so we can,
	// if required, update subsequent values.
	$sqlmulti = "SELECT sortorder, location, label FROM udf_definitions WHERE hashid  ='".$gval[1]."' AND registrantid=".RID;
	$res = mysql_query($sqlmulti);
	$row = mysql_fetch_array($res);
	$deletedsortid = $row['sortorder'];
	$deletedsortidlocation = $row['location'];
	$deletecolumn = $row['label'];
	
	// Before we delete the UDF data we need to remove all the
	// udf_multi_value data first
	$sqlmulti = "SELECT hashid FROM udf_definitions WHERE location  ='".$gval[1]."' AND fieldtype = 2 AND registrantid=".RID;
	$res = mysql_query($sqlmulti);
	while ($row = mysql_fetch_array($res)) {
		// Delete from udf multi
		$sqludf = "DELETE FROM udf_multi_value WHERE udfdefid ='".$row['hashid']."' AND registrantid=".RID;
		//echo $sqludf."<br>";
		mysql_query($sqludf) or die ('Dead11');
	}
	
	// Delete from udf definitions
	$sqludf = "DELETE FROM udf_definitions WHERE hashid ='".$gval[1]."' AND registrantid=".RID." LIMIT 1";
	//echo $sqludf."<br>";
	mysql_query($sqludf) or die ('Dead12');
	
	$sqlmulti = "SELECT MAX(sortorder) AS maxsortorder FROM udf_definitions WHERE location  ='".$deletedsortidlocation."' AND registrantid=".RID;
	$res = mysql_query($sqlmulti);
	$row = mysql_fetch_array($res);
	$deleteddiff = $row['maxsortorder'] - $deletedsortid;
	
	$newsortnum = ($deletedsortid + 1);
	// echo $newsortnum."<br>";
	for ($number = 0; $number < $deleteddiff; $number++) { 
		$query = "UPDATE udf_definitions SET sortorder = ".$deletedsortid." WHERE location  ='".$deletedsortidlocation."' AND sortorder = ".$newsortnum." AND registrantid=".RID;
		//echo $query."<br>";
		mysql_query($query) Or Die ("Cannot submit entry!");
		$deletedsortid++;
		$newsortnum++;
	}
	
	$specials = array(" ",":",";","�","`","�","!","�","$","%","^","&","*","(",")","_","_","+","=","{","}","[","]","@","'","~","#","|","\\","<",",",">","?","/");
	$deletecolumn = str_replace(' ',' ',str_replace($specials,'',$deletecolumn));
	
	// Delete the related columns
	$sql = "ALTER TABLE udf_".$deletedsortidlocation." DROP ".columnnames($deletecolumn)."";
	//echo $sql."<br>";
	mysql_query($sql);
	
	if ($gval[2] > 0) {
		// Delete fields and value relationships
		$sqludf = "DELETE FROM relations_matrix WHERE labelhashid  ='".$gval[1]."' AND registrantid=".RID." LIMIT 1";
		//echo $sqludf."<br>";
		mysql_query($sqludf) or die ('Dead12');
	
		$sqludf = "DELETE FROM relations_values  WHERE matrix_id =".$gval[2]." AND registrantid=".RID;
		//echo $sqludf."<br>";
		mysql_query($sqludf) or die ('Dead12');
	}
	
	header("Location: ".$_SERVER['PHP_SELF']."?id=14");
} elseif ($gval[0] == 19) {
	// Move the value down the order
	$numtochange = ($gval[2]-1);
	
	$query = "UPDATE udf_definitions SET sortorder = ".$numtochange." WHERE hashid = '".$gval[1]."' AND registrantid=".RID." LIMIT 1";
	echo $query."<br>";
	mysql_query($query) Or Die ("Cannot submit entry!");
	
	$query = "UPDATE udf_definitions SET sortorder = ".$gval[2]." WHERE sortorder = ".$numtochange." AND location = '".$gval[3]."' AND hashid != '".$gval[1]."' AND registrantid=".RID." LIMIT 1";
	//echo $query."<br>";
	mysql_query($query) Or Die ("Cannot submit entry!");
	
    header("Location:".$_SERVER['PHP_SELF']."?id=14");
} elseif ($gval[0] == 20) {
	// Move the value up the order
	$numtochange = ($gval[2]+1);
	
	$query = "UPDATE udf_definitions SET sortorder = ".$numtochange." WHERE hashid = '".$gval[1]."' AND registrantid=".RID." LIMIT 1";
	echo $query."<br>";
	mysql_query($query) Or Die ("Cannot submit entry!");
	
	$query = "UPDATE udf_definitions SET sortorder = ".$gval[2]." WHERE sortorder = ".$numtochange." AND location = '".$gval[3]."' AND hashid != '".$gval[1]."' AND registrantid=".RID." LIMIT 1";
	//echo $query."<br>";
	mysql_query($query) Or Die ("Cannot submit entry!");
	
    header("Location:".$_SERVER['PHP_SELF']."?id=14");
} elseif ($gval[0] == 21) {
	$query = "UPDATE udf_definitions SET inlists = ".$gval[2]." WHERE hashid = '".$gval[1]."' AND registrantid=".RID;
	mysql_query($query) Or Die ("Cannot submit entry!");
	//echo $query;
	
	header("Location:".$_SERVER['PHP_SELF']."?id=14::3");
} elseif ($gval[0] == 22) {
	$sql = "SELECT filename FROM icons WHERE id =".$gval[2];
	$res = mysql_query($sql);
	$row = mysql_fetch_array($res);
	
	$query = "UPDATE menusub_tabs SET icon = '".$row['filename']."' WHERE hashid = '".$gval[3]."' AND registrantid=".RID;
	//echo $query;
	mysql_query($query) Or Die ("Cannot submit entry!");
	
	header("Location:".$_SERVER['PHP_SELF']."?id=8:".$gval[1]);
} elseif ($gval[0] == 23) {
	
	$query = "UPDATE menusub_tabs SET icon = '' WHERE hashid = '".$gval[2]."' AND registrantid=".RID;
	//echo $query;
	mysql_query($query) Or Die ("Cannot submit entry!");
	
	header("Location:".$_SERVER['PHP_SELF']."?id=8:".$gval[1]);
} elseif ($gval[0] == 24) {
	// Move the value up the order
	$numtochange = ($gval[2]-1);
	
	$query = "UPDATE relations_matrix SET sortorder = ".$numtochange." WHERE id = '".$gval[1]."' AND parenttable = '".$gval[4]."' LIMIT 1";
	//echo $query."<br>";
	mysql_query($query) Or Die ("Cannot submit entry!");
	
	$query = "UPDATE relations_matrix SET sortorder = ".$gval[2]." WHERE sortorder = ".$numtochange." AND id != '".$gval[1]."'  AND parenttable = '".$gval[4]."' LIMIT 1";
	//echo $query."<br>";
	mysql_query($query) Or Die ("Cannot submit entry!");
	
    header("Location:".$_SERVER['PHP_SELF']."?id=8:".$gval[3]."");
} elseif ($gval[0] == 25) {
	// Move the value down the order
	$numtochange = ($gval[2]+1);
	
	$query = "UPDATE relations_matrix SET sortorder = ".$numtochange." WHERE id = '".$gval[1]."'  AND parenttable = '".$gval[4]."' LIMIT 1";
	//echo $query."<br>";
	mysql_query($query) Or Die ("Cannot submit entry!");
	
	$query = "UPDATE relations_matrix SET sortorder = ".$gval[2]." WHERE sortorder = ".$numtochange." AND id != '".$gval[1]."' AND parenttable = '".$gval[4]."' LIMIT 1";
	//echo $query."<br>";
	mysql_query($query) Or Die ("Cannot submit entry!");
	
    header("Location:".$_SERVER['PHP_SELF']."?id=8:".$gval[3]."");
} elseif ($gval[0] == 26) {
	// Delete the sub level tabs
	$sqludf = "DELETE FROM relations_matrix WHERE id ='".$gval[1]."' AND parenttable = '".$gval[4]."' LIMIT 1";
	mysql_query($sqludf) or die ('Dead2');
	
	$deletedsortid = $gval[2];
	
	//updatesortorder("relations_matrix");
	$sqlmulti = "SELECT MAX(sortorder) AS maxsortorder FROM relations_matrix WHERE parenttable = '".$gval[4]."' AND registrantid=".RID;
	echo $sqlmulti."<br>";
	$res = mysql_query($sqlmulti);
	$row = mysql_fetch_array($res);
	$deleteddiff = $row['maxsortorder'] - $deletedsortid;
	
	$newsortnum = ($deletedsortid + 1);
	for ($number = 0; $number < $deleteddiff; $number++) { 
		$query = "UPDATE relations_matrix SET sortorder = ".$deletedsortid." WHERE sortorder = ".$newsortnum." AND parenttable = '".$gval[4]."' AND registrantid=".RID;
		echo $query."<br>";
		mysql_query($query);
		$deletedsortid++;
		$newsortnum++;
	}
	
	header("Location:".$_SERVER['PHP_SELF']."?id=8:".$gval[3]."");
}
 ?>
 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "includes/xhtml1-transitional.dtd">
<html>
<head>
	<title>TLS Admin</title>
	<?php 
	if (ERES > 1024) { ?>
		<style type="text/css" media="all">@import "/css/style.css";</style>
	<?php 
	} else { ?>
		<style type="text/css" media="all">@import "/css/style_small.css";</style>
	<?php
	}?>
	<script type="text/javascript" language="javascript" src="/lytebox/lytebox.js"></script>
	<link rel="stylesheet" href="/lytebox/lytebox.css" type="text/css" media="screen" />
</head>
<body>
<?php include_once("header_menu.php");?><br>
<?php 
if ($gval[0] == "") { ?>
<br><a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=1">Create New Module</a><br>
<a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=14">User Defined Field Creator</a>
<?php 
} elseif (($gval[0] == 1) || ($gval[0] == 4)) { ?>
	<script language="JavaScript" type="text/javascript">
	<!--
	function checkform ( form ) {
		if ((form.type.value == "") || (form.label.value == "")) {
			alert( "Please complete all fields" );
			form.type.focus();
			return false ;
		}
		return true ;
	}
	
	function displayOption ( fieldname ) {
		var showOpt = document.getElementById('ShowMulti');
		if (document.form.type.value == "Standard Form") {
			showOpt.style.display = '';
		} else {
			showOpt.style.display = 'none';
		}
	}
	function displayOption2 ( fieldname ) {
		var showOpt = document.getElementById('ShowNumSeries');
		var showOpt2 = document.getElementById('ShowNumSeriesMSG');
		if (document.form.subcategories.value == 2) {
			showOpt.style.display = 'none';
			showOpt2.style.display = '';
		} else {
			showOpt.style.display = '';
			showOpt2.style.display = 'none';
		}
	}
	//-->
	</script>
	<?php 
	if ($gval[0] == 1) {
		$labelval = "Create New";
		$labelbutton = "Create Module";
		$pformvalue = 1;
	} elseif ($gval[0] == 4) { 
		$labelval = "Edit";
		$labelbutton = "Edit Module";
		$pformvalue = 2;
		$sql = "SELECT label, type FROM menu_tabs WHERE hashid = '".$gval[1]."' AND registrantid=".RID;
		$res = mysql_query($sql);
		$row = mysql_fetch_array($res);
		$editlabel = $row['label'];
		$edittype = $row['type'];
		$taburl = '<a href="'.$_SERVER['PHP_SELF'].'?id=1">Create New Module</a>';
		$hidddenhash = '<input type="hidden" name="hashid" value="'.$gval[1].'">';
	}?>
	<h1><?php echo $labelval; ?> Module</h1>
	<form method="POST" name="form" action="<?php echo $_SERVER['PHP_SELF'] ?>?id=1" onsubmit="return checkform(this);">
	<a href="<?php echo $_SERVER['PHP_SELF']; ?>?id=14">User Defined Field Creator</a> | <a href="<?php echo $_SERVER['PHP_SELF']; ?>">Up Level</a><br><br>
	Label:<br>
	<input type="text" name="label" class="standardfield" value="<?php echo $editlabel; ?>"> <font color="red">*</font><br>
	Module Type:<br>
	<select name="type" class="standardselect" onchange="displayOption(this);">
		<option value="">Select One</option>
	<?php 
	$sql = "SELECT label FROM menu_tabs_types";
	$res = mysql_query($sql);
	while ($row = mysql_fetch_array($res)) {
		if ($row['label'] == $edittype) { $typeselected = "selected"; }
	 ?>
		<option <?php echo $typeselected; ?> value="<?php echo $row['label']; ?>"><?php echo $row['label']; ?></option>
	<?php 
	unset($typeselected);
	} ?>
	</select> <font color="red">*</font><br><br>
	<span id="ShowMulti" style="display:none">
		Sub Categories: 
			<select name="subcategories" class="standardselect" onchange="displayOption2(this);">
				<option value="1">1</option>
				<option value="2">2</option>
			</select><br><br>
			<font class="note">For example if you created a Top Level of Customers, you might have<br> two sub-categories; 1 being Companies and another being Contacts.<br>
			If you select 1, the Module label you create will be the sub-category.</font>
			<br><br>
			<span id="ShowNumSeries" style="display:">
			Number Series:<br>
			<input type="text" name="numberseries" class="standardfield_ns"><br>
			<font class="note">
				Please enter a start number for this modules number series.<br>
			</font><br><br>
			</span>
			<span id="ShowNumSeriesMSG" style="display:none">
			<font class="note">You will be asked to define the number series after clicking the Create Module button.</font><br><br>
			</span>
	</span>
	<input type="submit" value="<?php echo $labelbutton; ?>">
	<input type="hidden" name="pform" value="<?php echo $pformvalue; ?>">
	<?php echo $hidddenhash; ?>
	</form>
	<?php echo $taburl; ?><br>
	<table cellpadding="2" cellspacing="3" width="50%">
	<tr>
		<td class="ls_top" align="center"><strong>Label</strong></td>
		<td class="ls_top" align="center"><strong>Type</strong></td>
		<td class="ls_top" align="center"><strong>Sort Order</strong></td>
		<td class="ls_top" align="center"><strong>Actions</strong></td>
	</tr>
	<?php 
	$sql = "SELECT * FROM menu_tabs WHERE registrantid = ".RID." ORDER BY sortorder ASC";
	$res = mysql_query($sql);
	$sqlgetnum = "SELECT MAX(sortorder) AS highestnumber FROM menu_tabs WHERE registrantid=".RID;
	$resgetnum = mysql_query($sqlgetnum);
	$rowgetnum = mysql_fetch_array($resgetnum);
	$highest =  $rowgetnum['highestnumber'];
	$numcounter = 1;
	while ($row = mysql_fetch_array($res)) { 
		include("..//includes/ls.php");?>
		<tr>
			<td class="ls_<?php echo $ls; ?>"><a href="<?php $_SERVER['PHP_SELF'] ?>?id=8:<?php echo $row['hashid'] ?>"><?php echo $row['label']; ?></a></td>
			<td class="ls_<?php echo $ls; ?>"><?php echo $row['type']; ?></td>
			<td class="ls_<?php echo $ls; ?>"><?php echo $row['sortorder']; ?> <font class="note">&nbsp; |
				<?php 
				if ($numcounter != 1) { ?>
					<a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=6:<?php echo $row['hashid'] ?>:<?php echo $row['sortorder']; ?>">up</a>
				<?php 
				}
				if ($numcounter != $highest) { ?>
				<a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=7:<?php echo $row['hashid'] ?>:<?php echo $row['sortorder']; ?>">down</a></font></td>
				<?php 
				} ?>
			<td class="ls_<?php echo $ls; ?>" align="center">
			<?php 
			if ($row['isactive'] == 0) { ?>
				<a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=2:<?php echo $row['hashid'] ?>"><img src="/images/icons/media_play_green.png" alt="Activate" border="0"></a>
			<?php 
			} else { ?>
				<a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=3:<?php echo $row['hashid'] ?>"><img src="/images/icons/media_play.png" alt="Deactivate" border="0"></a>
			<?php 
			} ?>
				<a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=4:<?php echo $row['hashid'] ?>"><img src="/images/icons/document_edit.png" alt="Edit" border="0"></a>
				<a onclick="return confirm('WARNING\nAre you sure you want to delete this module item? This action cannot be undone.\nAll data related to this module will also be deleted.')" href="<?php echo $_SERVER['PHP_SELF'] ?>?id=5:<?php echo $row['hashid'] ?>"><img src="/images/icons/delete2_16.png" alt="Delete" border="0"></a>
			</td>
		</tr>
	<?php 
	$numcounter++;
	}
} elseif ($gval[0] == 8)  { 
	$sqlgetnum = "SELECT label FROM menu_tabs WHERE hashid = '".$gval[1]."' AND registrantid=".RID;
	$resgetnum = mysql_query($sqlgetnum);
	$rowgetnum = mysql_fetch_array($resgetnum);
	echo "<h1>".$rowgetnum['label']."</h1><br>"; ?>
	<a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=14">User Defined Field Creator</a> | <a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=1">Up Level</a><br><br>
	<?php 
	if ($gval[4] == 1) { ?>
		<form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
	<?php 
	} ?>
	<table cellpadding="2" cellspacing="3" width="100%">
	<tr>
		<td class="ls_top" align="center"><strong>Label</strong></td>
		<td class="ls_top" align="center"><strong>Type</strong></td>
		<td class="ls_top" align="center"><strong>Sort Order</strong></td>
		<td class="ls_top" align="center" width="15%"><strong>Actions</strong></td>
	</tr>
	<?php 
	$sql = "SELECT * FROM menusub_tabs WHERE menu_tabhashid = '".$gval[1]."' ORDER BY sortorder ASC";
	$res = mysql_query($sql);
	$sqlgetnum = "SELECT MAX(sortorder) AS highestnumber FROM menusub_tabs WHERE menu_tabhashid = '".$gval[1]."'";
	$resgetnum = mysql_query($sqlgetnum);
	$rowgetnum = mysql_fetch_array($resgetnum);
	$highest =  $rowgetnum['highestnumber'];
	$numcounter = 1;
	while ($row = mysql_fetch_array($res)) { 
		include("../includes/ls.php");?>
		<tr>
			<td class="ls_<?php echo $ls; ?>">
				<?php 
				if ($gval[4] != 1) { 
					echo $row['label'];
				} elseif (($gval[4] == 1) && ($row['hashid'] == $gval[2])) {  ?>
					<input type="text" name="label" value="<?php echo $row['label']; ?>">
					<input type="hidden" name="hashid" value="<?php echo $row['hashid']; ?>">
					<input type="hidden" name="menu_tabhashid" value="<?php echo $gval[1]; ?>">
					<input type="hidden" name="pform" value="3">
				<?php
				} elseif (($gval[4] == 1) && ($row['hashid'] != $gval[2])) {
					echo $row['label'];
				} ?>
			</td>
			<td class="ls_<?php echo $ls; ?>"><?php echo $row['type']; ?></td>
			<td class="ls_<?php echo $ls; ?>"><?php echo $row['sortorder']; ?> <font class="note">&nbsp; |
				<?php 
				if ($numcounter != 1) { ?>
					<a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=9:<?php echo $row['hashid'] ?>:<?php echo $row['sortorder']; ?>:<?php echo $gval[1] ?>">up</a>
				<?php 
				}
				if ($numcounter != $highest) { ?>
				<a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=10:<?php echo $row['hashid'] ?>:<?php echo $row['sortorder']; ?>:<?php echo $gval[1] ?>">down</a></font></td>
				<?php 
				} 
				?>
			<td class="ls_<?php echo $ls; ?>" align="center">
			<?php 
			if ($row['isactive'] == 0) { ?>
				<a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=11:<?php echo $row['hashid'] ?>:<?php echo $gval[1] ?>"><img src="/images/icons/media_play_green.png" alt="Activate" border="0"></a>
			<?php 
			} else { ?>
				<a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=12:<?php echo $row['hashid'] ?>:<?php echo $gval[1] ?>"><img src="/images/icons/media_play.png" alt="Deactivate" border="0"></a>
			<?php 
			} ?>
				<a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=8:<?php echo $gval[1] ?>:<?php echo $row['hashid'] ?>::1"><img src="/images/icons/document_edit.png" alt="Edit" border="0"></a>
			</td>
		</tr>
	<?php 
	$numcounter++;
	}?>
	</table>
	<?php 
	if ($gval[4] == 1) { ?>
		<br>
		<input type="submit" value="Update Tab">
	</form>
	<?php 
	} ?>
	<br>
	<h3>Assign(ed) Icon(s) to Modules:</h1>
	<table width="100%" cellspacing="2" cellpadding="2" border="0">
	<tr>
		<td class="ls_top" align="center"><strong>Section</strong></td>
		<td class="ls_top" align="center"><strong>Icon</strong></td>
		<td class="ls_top" align="center" width="15%"><strong>Actions</strong></td>
	</tr>
	<?php 
	$sqlicon = "SELECT hashid, label, icon FROM menusub_tabs WHERE menu_tabhashid ='".$gval[1]."' AND icon !='' AND type = 'new' AND registrantid =".RID." ORDER BY sortorder ASC"; 
	$resicon = mysql_query($sqlicon);
	while ($rowicon = mysql_fetch_array($resicon)) { ?>
		<tr>
			<td class="ls_off"><?php echo ucwords($rowicon['label']); ?></td>
			<td class="ls_off" align="center"><img src="/images/icons/<?php echo $rowicon['icon']; ?>"></td>
			<td class="ls_off" align="center" width="15%">
				<a href="<?php $_SERVER['PHP_SELF'] ?>?id=23:<?php echo $gval[1] ?>:<?php echo $rowicon['hashid']; ?>"><img src="/images/icons/delete2_16.png" alt="Delete and/or Change" border="0"></a>
			</td>
		</tr>
	<?php
	}?>
	</table>
	<?php
	$sqlicon = "SELECT hashid, label FROM menusub_tabs WHERE menu_tabhashid ='".$gval[1]."' AND (icon ='' OR icon IS NULL) AND type = 'new' AND registrantid =".RID." ORDER BY id ASC"; 
	$resicon = mysql_query($sqlicon);
	while ($rowicon = mysql_fetch_array($resicon)) { 
		echo "<br><strong>Assign icon to ".ucwords(str_replace("new","",$rowicon['label']))."</strong><br><br>";?>
		<table width="100%" cellspacing="2" cellpadding="2" border="0">
		<?php 
		$sql = "SELECT id, filename FROM icons WHERE isactive = 1 ORDER BY filename ASC";
		$res = mysql_query($sql);
		$counter = 0;
		while ($row = mysql_fetch_array($res)) {
			$file = $row['filename'];
			if ($counter == 0) {
				echo "<tr>";
			}
			echo '<td align="center" class="ls_off"><a href="'.$_SERVER['PHP_SELF']."?id=22:".$gval[1].':'.$row['id'].':'.$rowicon['hashid'].'"><img src="/images/icons/'.$file.'" border="0"></a></td>';$counter++;
			if ($counter == 3) {
				echo "</tr>";
				$counter = 0;
			}
		}?>
	 	</table><br><br>
	 <?php 
	}?>
	<h3>Assign(ed) Module Tab(s):</h1>
	<a name="moduletabs"></a>
	<?php 
	$sql = "SELECT hashid, label FROM menusub_tabs WHERE menu_tabhashid ='".$gval[1]."' AND type = 'new' AND registrantid =".RID." ORDER BY sortorder ASC"; 
	$res = mysql_query($sql);
	while ($row = mysql_fetch_array($res)) { 
		?>
		<a <?php echo LFD ?> href="/assign_tabs.php?id=<?php echo $row['hashid'] ?>:<?php echo $gval[1] ?>">Assign New Module Tab(s) to <?php echo ucwords(str_replace("new","",$row['label'])) ?></a><br><br>
		<table width="100%" cellspacing="2" cellpadding="2" border="0">
		<tr>
			<td class="ls_top" align="center"><strong>Module Tab Label</strong></td>
			<td class="ls_top" align="center"><strong>Sort Order</strong></td>
			<td class="ls_top" align="center" width="15%"><strong>Actions</strong></td>
		</tr>
		<?php 
		// Lets get the items allocated to this module
		$query = "SELECT * FROM relations_matrix WHERE parenttable = '".$row['hashid']."' ORDER BY sortorder ASC";
		// echo $query;
		$result = mysql_query($query);
		$sqlgetnum = "SELECT MAX(sortorder) AS highestnumber FROM relations_matrix WHERE parenttable = '".$row['hashid']."'";
		$resgetnum = mysql_query($sqlgetnum);
		$rowgetnum = mysql_fetch_array($resgetnum);
		$highest =  $rowgetnum['highestnumber'];
		$numcounter = 1;
		while ($row = mysql_fetch_array($result)) { 
			include("../includes/ls.php");
			if ($row['ispredefined'] == 1) {
				$getpre = "SELECT label FROM relations_predefined WHERE id = ".$row['childtable']." LIMIT 1";
				// echo $getpre;
				$preres = mysql_query($getpre);
				$prerow = mysql_fetch_array($preres);
				$labelval = $prerow['label'];
			} else {
				$getpre = "SELECT tablabel FROM udf_definitions WHERE hashid = '".$row['labelhashid']."' LIMIT 1";
				// echo $getpre;
				$preres = mysql_query($getpre);
				$prerow = mysql_fetch_array($preres);
				$labelval = $prerow['tablabel'];
			} ?>
		<tr>
			<td class="ls_<?php echo $ls; ?>"><?php echo $labelval; ?></td>
			<td class="ls_<?php echo $ls; ?>"><?php echo $row['sortorder']; ?><font class="note">&nbsp; |
				<?php 
				if ($numcounter != 1) { ?>
					<a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=24:<?php echo $row['id'] ?>:<?php echo $row['sortorder']; ?>:<?php echo $gval[1] ?>:<?php echo $row['parenttable'] ?>:#moduletabs">up</a>
				<?php 
				}
				if ($numcounter != $highest) { ?>
				<a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=25:<?php echo $row['id'] ?>:<?php echo $row['sortorder']; ?>:<?php echo $gval[1] ?>:<?php echo $row['parenttable'] ?>:#moduletabs">down</a></font></td>
				<?php 
				} 
				?></font>
			</td>
			<td class="ls_<?php echo $ls; ?>" align="center" width="15%">
				<a href="<?php $_SERVER['PHP_SELF'] ?>?id=26:<?php echo $row['id'] ?>:<?php echo $row['sortorder']; ?>:<?php echo $gval[1] ?>:<?php echo $row['parenttable'] ?>:#moduletabs"><img src="/images/icons/delete2_16.png" alt="Delete and/or Change" border="0"></a>
			</td>
		</tr>
	<?php 
			$numcounter++;
		}
		 ?>
		</table><br><br>
	<?php 
	} ?><br>
<?php
} elseif ($gval[0] == 13)  {  ?>
	<script language="JavaScript" type="text/javascript">
	<!--
	function checkform ( form ) {
		if ((form.label_1.value == "") || (form.label_singular_1.value == "") || (form.label_2.value == "") || (form.label_singular_2.value == "") || (form.numberseries_1.value == "") || (form.numberseries_2.value == "")) {
			alert( "Please complete all fields" );
			form.label_1.focus();
			return false ;
		}
		return true ;
	}
	//-->
	</script>
	<form method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?>" onsubmit="return checkform(this);">
<?php 
	for ($number = 0; $number < $gval[2]; $number++) { ?>
		Plural Label <?php echo ($number+1); ?>: <font color="red">*</font><br>
		<input type="text" name="label_<?php echo ($number+1) ?>" class="standardfield"><br>
		Singluar Label <?php echo ($number+1); ?>: <font color="red">*</font><br>
		<input type="text" name="label_singular_<?php echo ($number+1) ?>" class="standardfield"><br>
		Number Series <?php echo ($number+1); ?>: <font color="red">*</font><br>
		<input type="text" name="numberseries_<?php echo ($number+1) ?>" class="standardfield_ns"><br><br>
		
<?php 
	}?><br>
 	<input type="submit" value="Create Sub">
	<input type="hidden" name="totcategories" value="<?php echo $number; ?>">
	<input type="hidden" name="pform" value="4">
	<input type="hidden" name="hashid" value="<?php echo $gval[1]; ?>">
	</form>
<?php
}  elseif ($gval[0] == 14) { 
				$query = "SELECT * FROM udf_definitions WHERE registrantid = ".RID." AND hashid = '".$gval[1]."'";
				// echo $query;
				$result = mysql_query($query);
				$row = mysql_fetch_array($result);?>
				<script language="JavaScript" type="text/javascript">
				<!--
				function checkform ( form ) {
					if ((form.udfmodule.value == "") || (form.udflabel.value == "") || (form.columntype.value == "") || (form.udftype.value == "")) {
						alert( "Please complete all fields marked with a red asterisk." );
						
						return false ;
					}
					return true ;
				}
				//-->
				</script>
				<table border="0" width="100%" cellpadding="2" cellspacing="3">
				<tr>
					<td colspan="2"><strong><font style="font-size:140%;font-family:Trebuchet MS">User Defined Field Creator</font></strong><br><br></td>
				</tr>
				<tr>
					<td colspan="2">
						<form method="POST" name="form" action="<?php $_SERVER['PHP_SELF']; ?>" onsubmit="return checkform(this);">
						<a href="<?php echo $SERVER['PHP_SELF'] ?>?id=14">New User Defined Field</a> |
						<a href="<?php echo $SERVER['PHP_SELF'] ?>?id=1">Up Level</a><br><br style="line-height:10px">
						<table border="0" width="100%" cellpadding="2" cellspacing="2">
						<tr>
							<td width="15%" class="ls_on" align="right">Add to Module: <font color="red">*</font></td>
							<td class="ls_on" align="left">
								<?php 
								$sql2 = "SELECT menusub_tabs.menu_tabhashid AS tlhashid, menusub_tabs.hashid AS hashid, menu_tabs.label AS menulabel, menusub_tabs.label AS menusublabel
										FROM menu_tabs, menusub_tabs 
										WHERE menusub_tabs.type = 'new' 
										AND menusub_tabs.registrantid = ".RID." 
										AND menu_tabhashid = menu_tabs.hashid
										ORDER BY menulabel, menusublabel ASC";  
								// echo $sql2."<br>";
								$res2 = mysql_query($sql2);?>
								<select name="udfmodule" class="standardselect" onchange="unlockFields(this.value);">
									<option value="">Select One</option>
								<?php
								while ($row2 = mysql_fetch_array($res2)) { 
									if ($row2['hashid'] == $row['location']) {
										$checkmeloc = "selected";
									} ?>
									<option <?php echo $checkmeloc; ?> value="<?php echo $row2['tlhashid']; ?>&&<?php echo $row2['hashid']; ?>"><?php echo $row2['menulabel']." / ".ucwords($row2['menusublabel']) ?></option>
								<?php
								unset($checkmeloc);
								}?>
								</select>
							</td>
						</tr>
						</table>
						<script language="Javascript">
						function unlockFields(elemval) {
							if (document.form.udfmodule.value == "") {
								document.form.udflabel.disabled = true;
							} else {
								document.form.udflabel.disabled=false;
							}
						}
						</script>
						<table  border="0" width="100%" cellpadding="2" cellspacing="3">
						<tr>
							<td width="15%" class="ls_on" align="right">Add Field Label: <font color="red">*</font></td>
							<td class="ls_on" align="left">
								<script language="javascript" type="text/javascript" src="/js/udf_duplicate_checker.js"></script>
								<input disabled onkeyup="checkdup(this.value);" type="text" name="udflabel" class="standardfield" value="<?php echo $row['label']; ?>"><span style="color:red;" id="txtHint"></span>
							</td>
							<td class="ls_on" align="right">Display in Lists:</td>
							<td class="ls_on" align="left">
							<?php 
							if ($row['inlists'] == 0) {
								$checkmeloc = "selected";
							} elseif ($row['inlists'] == 1) {
								$checkmeloc2 = "selected";
							} ?>
								<select name="inlists" class="standardselect">
									<option <?php echo $checkmeloc ?> value="0">No</option>
									<option <?php echo $checkmeloc2 ?> value="1">Yes</option>
								</select>
							</td>
						</tr>
						<tr>
							<td class="ls_on" align="right">Select Field Type: <font color="red">*</font></td>
							<td class="ls_on" align="left">
							<?php 
							if ($row['fieldtype'] == 1) {
								$udf1 = "selected";
							} elseif ($row['fieldtype'] == 2) {
								$udf2 = "selected";
							} elseif ($row['fieldtype'] == 3) {
								$udf3 = "selected";
							} elseif ($row['fieldtype'] == 4) {
								$udf4 = "selected";
							} elseif ($row['fieldtype'] == 5) {
								$udf5 = "selected";
							} elseif ($row['fieldtype'] == 6) {
								$udf6 = "selected";
							} elseif ($row['fieldtype'] == 7) {
								$udf7 = "selected";
							} elseif ($row['fieldtype'] == 8) {
								$udf8 = "selected";
							} elseif ($row['fieldtype'] == 9) {
								$udf9 = "selected";
							} elseif ($row['fieldtype'] == 10) {
								$udf10 = "selected";
							} elseif ($row['fieldtype'] == 11) {
								$udf11 = "selected";
							} elseif ($row['fieldtype'] == 12) {
								$udf12 = "selected";
							} ?>
							<select id="udftype" name="udftype" class="standardselect" onchange="displayOption(this);">
								<option value="">Select One</option>
								<option <?php echo $udf3; ?> value="3">Checkbox</option>
								<option <?php echo $udf10; ?> value="10">Data Link Field</option>
								<option <?php echo $udf8; ?> value="8">Date</option>
								<option <?php echo $udf9; ?> value="9">Date & Time</option>
								<option <?php echo $udf2; ?> value="2">Drop Down Select Menu</option>
								<option <?php echo $udf5; ?> value="5">Email Address</option>
								<option <?php echo $udf7; ?> value="7">Mobile Number</option>
								<option <?php echo $udf6; ?> value="6">Phone Number</option>
								<option <?php echo $udf11; ?> value="11">Section Header</option>
								<option <?php echo $udf1; ?> value="1">Text Field</option>
								<option <?php echo $udf12; ?> value="12">Text Field - Header</option>
								<option <?php echo $udf4; ?> value="4">Text Area</option>		
							</select>
							</td>
							<td class="ls_on" align="right">Data Type: <font color="red">*</font></td>
							<td class="ls_on" align="left">
							<?php 
							if ($row['columntype'] == "DATE") {
								$udf1 = "selected";
							} elseif ($row['columntype'] == "DATETIME") {
								$udf2 = "selected";
							} elseif ($row['columntype'] == "FLOAT") {
								$udf3 = "selected";
							} elseif ($row['columntype'] == "INT") {
								$udf4 = "selected";
							} elseif ($row['columntype'] == "TEXT") {
								$udf5 = "selected";
							} elseif ($row['columntype'] == "TIME") {
								$udf6 = "selected";
							} elseif ($row['columntype'] == "VARCHAR(350)") {
								$udf7 = "selected";
							} ?>
								<select name="columntype" class="standardselect">
									<option value="">Select One</option>
									<option <?php echo $udf1 ?> value="DATE">Date (Date Only)</option>
									<option <?php echo $udf2 ?> value="DATETIME">Date Time (Date Time)</option>
									<option <?php echo $udf3 ?> value="FLOAT">Float (Decimal)</option>
									<option <?php echo $udf4 ?> value="INT">Integer (No Decimal)</option>
									<option <?php echo $udf5 ?> value="TEXT">Text (Unlimited Text)</option>
									<option <?php echo $udf6 ?> value="TIME">Time (Time Only)</option>
									<option <?php echo $udf7 ?> value="VARCHAR(350)">Varchar (Max 350 Chars)</option>
								</select>
							</td>
						</table>
							<script language="Javascript">
								function displayOption ( fieldname ) {
									var showOpt = document.getElementById('datalinkfield1');
									var showOpt2 = document.getElementById('datalinkfield2');
									var showOpt3 = document.getElementById('datalinkfield3');
									var showOpt4 = document.getElementById('datalinkfield4');
									if (document.form.udftype.value == 10) {
										showOpt.style.display = '';
										showOpt2.style.display = '';
										showOpt3.style.display = '';
										showOpt4.style.display = '';
									} else {
										showOpt.style.display = 'none';
										showOpt2.style.display = 'none';
										showOpt3.style.display = 'none';
										showOpt4.style.display = 'none';
									}
								}
							</script>
						<table border="0" width="100%" cellpadding="2" cellspacing="3">
						<tr>
						<?php 
						if (($gval[1] != "") && ($row['fieldtype'] == 10)) {
							$displayval = "";
						} else {
							$displayval = 'style="display:none;"';
						}?>
							<td class="ls_on" align="right" id="datalinkfield1" <?php echo $displayval; ?>>Link to table: </td>
							<td class="ls_on" width="40%" id="datalinkfield2" <?php echo $displayval; ?>>
								<?php 
								$linksql = "SELECT udf.tablabel AS tablabel, mt.label AS mtlabel, mst.label AS mstlabel, mst.menu_tabhashid, udf.location AS udflocation, udf.label AS udflabel, udf.hashid AS hashid
											FROM menu_tabs mt,menusub_tabs mst,udf_definitions udf
											WHERE mst.menu_tabhashid = mt.hashid 
											AND udf.location = mst.hashid
											AND mt.registrantid =".RID."
											ORDER BY mtlabel, mstlabel, udflabel ASC";
								// echo $linksql; 
								$reslink = mysql_query($linksql);
								
								// Check if a record exists in the relations matrix, if so we can make it select  on edit...
									$checkrel = "SELECT parenttable, udf_columnid FROM relations_matrix WHERE labelhashid = '".$gval[1]."' AND registrantid =".RID." LIMIT 1";
									$checkrelreslink = mysql_query($checkrel);
									$checkrelrowlink = mysql_fetch_array($checkrelreslink);?>
								<select name="datalinktable" class="standardselect">
									<option value="">Select One</option>
									<?php
									while ($rowlink = mysql_fetch_array($reslink)) { 
										if (($checkrelrowlink['parenttable'] == $rowlink['udflocation']) &&  ($checkrelrowlink['udf_columnid'] == $rowlink['hashid']))  {
											$relchecker = "selected";
										} ?>
										<option <?php echo $relchecker; ?> value="<?php echo $rowlink['udflocation'] ?>&&<?php echo $rowlink['hashid'] ?>"><?php echo $rowlink['mtlabel'] ?> / <?php echo ucwords($rowlink['mstlabel']) ?>  / <?php echo ucwords($rowlink['udflabel']) ?></option>
									<?php 
										unset($relchecker);
									} ?>
								</select>
							</td>
							<td class="ls_on" align="right" id="datalinkfield3" <?php echo $displayval; ?>>Corresponding Menu Tab Label:</td>
							<td class="ls_on" id="datalinkfield4" <?php echo $displayval; ?>>
								<input type="text" class="standardfield" name="tablabel" value="<?php echo $row['tablabel'] ?>">
							</td>
						</tr>
						</table>
						
						<table border=0>
						<tr>
							<?php
								unset($udf1,$udf2,$udf3,$udf4,$udf5,$udf6,$udf7,$checkmeloc2,$checkmeloc);
							?>
							<td align="left" colspan="6"><br>
							<?php 
							if ($gval[1] != "") { ?>
								<input style="font-family:Arial;font-size:11px;width:150px;" type="submit" value="Update Field">
								<?php 
								if ($gval[2] == 2) {
									echo "<br><br><font style='background-color:#91c21b;padding:3px;color:white'>Update Field</strong></font>";
								}?>
							<?php 
							} else { ?>
								<input style="font-family:Arial;font-size:11px;width:150px;" type="submit" name="createfield" value="Create Field">
								<?php 
								if ($gval[2] == 3) {
									echo "<br><br><font style='background-color:#91c21b;padding:3px;color:white'>Saved - Edit or Create New Field</strong></font>";
								}?>
							<?php 
							} ?>
							</td>
						</tr>
						</table>
						<?php 
						if ($gval[1] != "") { ?>
							<input type="hidden" value="<?php echo $gval[1] ?>" name="hashid">
							<input type="hidden" name="pform" value="6">
							<input type="hidden" name="redundantcolumnname" value="<?php echo columnnames($row['label']); ?>">
						<?php 
							} else { ?>
							<input type="hidden" name="pform" value="5">
						<?php 
							} ?>
						</form>
					</td>
				</tr>
				</table>
				<br>
				<?php 
				//$querygroup = "SELECT location FROM udf_definitions WHERE registrantid = ".RID." GROUP BY location";
				//echo $query;
				$querygroup = "SELECT udf_definitions.location AS location, menu_tabs.hashid AS menutabid, menu_tabs.label AS menulabel, menusub_tabs.label AS menusublabel 
								FROM udf_definitions, menu_tabs, menusub_tabs 
								WHERE udf_definitions.registrantid = ".RID."
								AND menusub_tabs.hashid  = udf_definitions.location
								AND menu_tabhashid = menu_tabs.hashid
								GROUP BY location";
				// echo $querygroup."<br>";
				$resultgroup = mysql_query($querygroup);
				while ($rowgroup = mysql_fetch_array($resultgroup)) {  
					?>
				<a name="<?php echo $rowgroup['menulabel']." - ".$rowgroup['menusublabel']; ?>"></a>
				<table border="0" width="100%" cellpadding="2" cellspacing="3">
				<tr>
					<td colspan="3"><font style="font-weight:bold;font-size:16px"><?php echo $rowgroup['menulabel']." - ".$rowgroup['menusublabel']; ?></font></td>
				</tr>
				<tr>
					<td class="ls_on" width="25%" align="center" width="15%"><strong>Field Type</strong></td>
					<td class="ls_on" align="center"><strong>Label</strong></td>
					<td class="ls_on" width="20%" align="center"><strong>Sort Order</strong></td>
					<td class="ls_on" align="center" width="10%"><strong>Actions</strong></td>
				</tr>
				<?php 
					$query = "SELECT * FROM udf_definitions WHERE registrantid = ".RID." AND location = '".$rowgroup['location']."' ORDER BY sortorder ASC";
					// echo $query."<br>";
					$result = mysql_query($query);
					$ls = 1;
					$sqlgetnum = "SELECT MAX(sortorder) AS highestnumber FROM udf_definitions WHERE location = '".$rowgroup['location']."' AND registrantid=".RID;
					// echo $sqlgetnum."<br>";
					$resgetnum = mysql_query($sqlgetnum);
					$rowgetnum = mysql_fetch_array($resgetnum);
					$highest =  $rowgetnum['highestnumber'];
					$numcounter = 1;
					while ($row = mysql_fetch_array($result)) { 
						include("../includes/ls.php");
						if ($row['fieldtype'] == 1) {
							$fieldtype = "Text";
						} elseif ($row['fieldtype'] == 2) {
							$fieldtype = "Drop Down Select Menu";
							//$fronturl = '<a '.LFD.' href="/udf_edit_manager.php?editting='.$row['hashid'].'">';
							$endurl = ' | <a '.LFD.' href="/udf_edit_manager.php?editting='.$row['hashid'].':'.$rowgroup['menutabid'].'">Add / Change Drop Down Values</a>';
						} elseif ($row['fieldtype'] == 3) {
							$fieldtype = "Checkbox";
						} elseif ($row['fieldtype'] == 4) {
							$fieldtype = "Text Area";
						} elseif ($row['fieldtype'] == 5) {
							$fieldtype = "Email Address";
						} elseif ($row['fieldtype'] == 6) {
							$fieldtype = "Phone Number";
						} elseif ($row['fieldtype'] == 7) {
							$fieldtype = "Mobile Number";
						} elseif ($row['fieldtype'] == 8) {
							$fieldtype = "Date";
						} elseif ($row['fieldtype'] == 9) {
							$fieldtype = "Date &amp; Time";
						} elseif ($row['fieldtype'] == 10) {
							// Get the relations matrix id for editting
							$datalinkrecordid = ":".$row['linkedid'];
							$endurl = " | <a ".LFD." href='/-admin/icon_manager.php?id=1:16:".$row['hashid']."'>Add / Change Tab Icon</a>";
							$queryico = "SELECT size, filename FROM icons_user WHERE id = ".$row['icon']." LIMIT 1";
							// echo $query."<br>";
							$resico = mysql_query($queryico);
							$rowico = mysql_fetch_array($resico);
							
							if ($row['icon']) {
								$displayicon = '<table width="100%" border="0">
											<tr>
												<td align="right"><font class="note">Corresponding Menu Tab Icon:</font></td>
												<td width="5%"><img align="right" src="/images/icons/'.$rowico['size'].'_shadow/'.$rowico['filename'].'"></td>
											</table>';
							}
							$fieldtype = "Data Link Field";
							$matrixvalue = $row['linkedid'];
						} elseif ($row['fieldtype'] == 11) {
							$fieldtype = "<strong>Section Header</strong>";
						} elseif ($row['fieldtype'] == 11) {
							$fieldtype = "<strong>Section Header</strong>";
						} elseif ($row['fieldtype'] == 12) {
							$fieldtype = "Text Field - Header";
						} 
						?>
					<tr>
						<td class="ls_<?php echo $ls ?>"><?php echo $fieldtype; ?></td>
						<td class="ls_<?php echo $ls ?>"><?php echo $fronturl.$row['label'].$endurl; ?><?php echo $displayicon ?></td>
						<td class="ls_<?php echo $ls; ?>"><?php echo $row['sortorder']; ?> <font class="note">&nbsp; |
							<?php 
							if ($numcounter != 1) { ?>
								<a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=19:<?php echo $row['hashid'] ?>:<?php echo $row['sortorder']; ?>:<?php echo $row['location'] ?>#<?php echo $rowgroup['menulabel']." - ".$rowgroup['menusublabel']; ?>">up</a>
								<?php 
							}
							if ($numcounter != $highest) { ?>
								<a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=20:<?php echo $row['hashid'] ?>:<?php echo $row['sortorder']; ?>:<?php echo $row['location'] ?>#<?php echo $rowgroup['menulabel']." - ".$rowgroup['menusublabel']; ?>">down</a></font>
							</td>
							<?php 
							} ?>
							<td class="ls_<?php echo $ls ?>" align="center">
						<?php 
						if ($row['isactive'] == 0) { ?>
								<a href="<?php $_SERVER['PHP_SELF'] ?>?id=15:<?php echo $row['hashid'] ?>#<?php echo $rowgroup['menulabel']." - ".$rowgroup['menusublabel']; ?>"><img src="/images/icons/media_play_green.png" alt="Activate" border="0"></a>
							<?php 
						} else { ?>
								<a href="<?php $_SERVER['PHP_SELF'] ?>?id=16:<?php echo $row['hashid'] ?>#<?php echo $rowgroup['menulabel']." - ".$rowgroup['menusublabel']; ?>"><img src="/images/icons/media_play.png" alt="Deactivate" border="0"></a>
							<?php 
						} ?>
						
						<?php
						if ($row['iscompulsory'] == 0) {
						?>
							<a href="<?php $_SERVER['PHP_SELF'] ?>?id=17:<?php echo $row['hashid'] ?>:1#<?php echo $rowgroup['menulabel']." - ".$rowgroup['menusublabel']; ?>"><img border="0" src="/images/icons/star_yellow.png" alt="This is not a compulsory field"></a>
						<?php
						} else { ?>
							<a href="<?php $_SERVER['PHP_SELF'] ?>?id=17:<?php echo $row['hashid'] ?>:0#<?php echo $rowgroup['menulabel']." - ".$rowgroup['menusublabel']; ?>"><img border="0" src="/images/icons/star_red.png" alt="This is a compulsory field"></a>
						<?php
						} ?>
						
						<?php
						if ($row['inlists'] == 0) {
						?>
							<a href="<?php $_SERVER['PHP_SELF'] ?>?id=21:<?php echo $row['hashid'] ?>:1#<?php echo $rowgroup['menulabel']." - ".$rowgroup['menusublabel']; ?>"><img border="0" src="/images/icons/inlist_no.png" alt="This field is not shown in lists"></a>
						<?php
						} else { ?>
							<a href="<?php $_SERVER['PHP_SELF'] ?>?id=21:<?php echo $row['hashid'] ?>:0#<?php echo $rowgroup['menulabel']." - ".$rowgroup['menusublabel']; ?>"><img border="0" src="/images/icons/inlist.png" alt="This field is shown in lists"></a>
						<?php
						} ?>
						
						
						
							<a href="<?php $_SERVER['PHP_SELF'] ?>?id=14:<?php echo $row['hashid'] ?>:2<?php echo $datalinkrecordid ?>"><img src="/images/icons/document_edit.png" alt="Edit" border="0"></a>
							<a onclick="return confirm('WARNING\nAre you sure you want to delete this user defined field? This action cannot be undone.\nAll values related to this user defined field will also be deleted.')" href="<?php echo $_SERVER['PHP_SELF'] ?>?id=18:<?php echo $row['hashid'] ?>:<?php echo $matrixvalue ?>#<?php echo $rowgroup['menulabel']." - ".$rowgroup['menusublabel']; ?>"><img src="/images/icons/delete2_16.png" alt="Delete" border="0"></a>
						</td>
					</tr>
					<?php 
					$numcounter++;
					unset($fronturl,$matrixvalue,$endurl, $displayicon);
					} ?>
					</table>
					<br>
				<?php 
				} ?>
			<?php
} ?>
</body>
</html>
