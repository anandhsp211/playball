<?php
include_once("../_globalconnect.php");
include_once("../functions.php");

if ($_POST['pform'] == 1) {
	$num_row = count($_POST['formfields']);
	if ($num_row > 1) {
		// if there is more than one row, then we build a string
		$multicount = 0;
		foreach ($_POST['formfields'] as &$value) {
    		// echo $value."<br>";
			// Check what columns we are dealing with and build a query string
			if ($multicount == 0) {
				$hashidstring .= "(hashid = '".$value."'";
			} else {
				$hashidstring .= " OR hashid = '".$value."'";
			}
			if ($multicount == ($num_row-1)) { $hashidstring .= ")"; }
			$multicount++;
		}
	} else {
		// Only one string so we just pull that value
		$hashidstring = "hashid = '".$_POST['formfields'][0]."'";
	}
	
	// Lets get dest location
	$destinationloc = explode("||", $_POST['destinationlocation']);
	
	// Lets get dest last sort order num
	$sql = "SELECT MAX(sortorder) AS maxsort FROM udf_definitions WHERE location = '".$destinationloc[1]."' AND registrantid = ".RID;
	// echo $sql."<br><br>";
	$res = mysql_query($sql) or die ('Dead');
	$row = mysql_fetch_array($res);
	$nextsortid = ($row['maxsort'] + 1);
	
	// Select all the rows we are replicating and start inserting
	$sql = "SELECT * FROM udf_definitions WHERE ".$hashidstring." AND registrantid = ".RID;
	//echo $sql."<br><br>";
	$res = mysql_query($sql) or die ('Dead');
	while ($row = mysql_fetch_array($res)) { 
		// Now we need to insert each new row into the udf_def table with the new location..
		$queryinsert = "INSERT INTO udf_definitions (registrantid, fieldtype, columntype, label, location, sortorder, iscompulsory, inlists, singlerow, singlelarge, isactive) VALUES (
		".RID.",
		".$row['fieldtype'].",
		'".$row['columntype']."',
		'".addslashes($row['label'])."',
		'".$destinationloc[1]."',
		".$nextsortid.", 
		".$row['iscompulsory'].",
		".$row['inlists'].",
		".$row['singlerow'].",
		".$row['singlelarge'].",
		0
		)";
		// echo $queryinsert."<br><br>";
	
		mysql_query($queryinsert) Or Die ("Cannot submit entry!");
		
		$sqlsethashid = "SELECT MAX(id) AS HashID FROM udf_definitions";
		$ressethashid = mysql_query($sqlsethashid);
		$rowsethashid = mysql_fetch_array($ressethashid)  Or Die ("Cannot set ID, please notify systems administrator with this code: SetHID: 1");
	
		$querysethashid = "UPDATE udf_definitions SET hashid = '".strtoupper(md5($rowsethashid['HashID']))."' WHERE id = ".$rowsethashid['HashID'];
		// echo $querysethashid."<br><br>";
		mysql_query($querysethashid) Or Die ("Cannot set ID, please notify systems administrator with this code: SetHID: 1452");
		
		$sql = "ALTER TABLE udf_".$destinationloc[1]." ADD ".columnnames($row['label'])." ".$row['columntype']." NULL;";
		// echo $sql."<br><br>";
		mysql_query($sql) or die ("Cannot alter table");
		
		// if fieldtype = 2 then it is a multi drop down and we will need to replicate the values in the udf_multi_value table
		if ($row['fieldtype'] == 2) {
			$sqlmulti = "SELECT value, isdefault FROM udf_multi_value WHERE udfdefid = '".$_POST['formfields'][0]."' AND registrantid = ".RID;
			// echo $sqlmulti."<br><br>";
			$resmulti = mysql_query($sqlmulti) or die ('Dead');
			while ($rowmulti = mysql_fetch_array($resmulti)) { 
				$sqlins = "INSERT INTO udf_multi_value (registrantid, menutabid, udfdefid, value, isdefault) VALUES ( 
					".RID.",
					'".$destinationloc[0]."',
					'".strtoupper(md5($rowsethashid['HashID']))."',
					'".$rowmulti['value']."',
					'".$rowmulti['isdefault']."'
					)";
					// echo $sqlins."<br><br>";
					mysql_query($sqlins) Or Die ("Cannot submit entry!");
			}
		}
		
		// if fieldtype = 10 then it is a relationship field and we also need to update the relations matrix table
		if ($row['fieldtype'] == 10) {
			$sqlreplicate = "SELECT parenttable, udf_columnid FROM relations_matrix WHERE id = ".$row['linkedid']." LIMIT 1";
			// echo $sqlreplicate."<br><br>";
			$resreplicate = mysql_query($sqlreplicate) or die ('Dead');
			$rowreplicate = mysql_fetch_array($resreplicate);
			
			// Quickly get sort order
			$sqlreplicates = "SELECT MAX(sortorder) AS mxsort FROM relations_matrix WHERE parenttable = '".$rowreplicate['parenttable']."' LIMIT 1";
			// echo $sqlreplicates."<br><br>";
			$resreplicates = mysql_query($sqlreplicates);
			$rowreplicates = mysql_fetch_array($resreplicates) or die ('Dead');
			
			$sqlins = "INSERT INTO relations_matrix (registrantid, menutabid, parenttable , childtable, ispredefined, labelhashid, udf_columnid, sortorder) VALUES ( 
					".RID.",
					'".$destinationloc[0]."',
					'".$rowreplicate['parenttable']."',
					'".$destinationloc[1]."',
					0,
					'".strtoupper(md5($rowsethashid['HashID']))."',
					'".$rowreplicate['udf_columnid']."',
					".($rowreplicates['mxsort'] + 1)."
					)";
					// echo $sqlins."<br><br>";
					mysql_query($sqlins) Or Die ("Cannot submit entry iD: 785698!");
			
			$sqlgetmx = "SELECT MAX(id) AS NextID FROM relations_matrix";
			$resgetmx = mysql_query($sqlgetmx);
			$rowgetmx = mysql_fetch_array($resgetmx) Or Die ("Cannot set ID, please notify systems administrator with this code: SetHID: 1254");
			
			$querysethashid = "UPDATE udf_definitions SET linkedid = ".$rowgetmx['NextID'].", islinked = 1 WHERE hashid = '".strtoupper(md5($rowsethashid['HashID']))."'";
			// echo $querysethashid."<br><br>";
			mysql_query($querysethashid) Or Die ("Cannot set ID, please notify systems administrator with this code: SetHID: 1452");
			
		}
		
		$nextsortid++;
	}
	
	$onload = 'onload="window.parent.location = window.parent.location;self.close();return false;"';
	
} elseif ($_POST['pform'] == 2) {
	$num_row = count($_POST['formfields']);
	if ($num_row > 1) {
		// if there is more than one row, then we build a string
		$multicount = 0;
		foreach ($_POST['formfields'] as &$value) {
    		// echo $value."<br>";
			// Check what columns we are dealing with and build a query string
			if ($multicount == 0) {
				$hashidstring .= "(hashid = '".$value."'";
			} else {
				$hashidstring .= " OR hashid = '".$value."'";
			}
			if ($multicount == ($num_row-1)) { $hashidstring .= ")"; }
			$multicount++;
		}
	} else {
		// Only one string so we just pull that value
		$hashidstring = "hashid = '".$_POST['formfields'][0]."'";
	}
	
	// Lets get dest location
	$destinationloc = explode("||", $_POST['destinationlocation']);
	
	// Lets get dest last sort order num
	$sql = "SELECT MAX(sortorder) AS maxsort FROM udf_definitions WHERE location = '".$destinationloc[1]."' AND registrantid = ".RID;
	// echo $sql."<br><br>";
	$res = mysql_query($sql) or die ('Dead');
	$row = mysql_fetch_array($res);
	$nextsortid = ($row['maxsort'] + 1);
	
	// Select all the rows we are replicating and start inserting
	$sql = "SELECT * FROM udf_definitions WHERE ".$hashidstring." AND registrantid = ".RID;
	// echo $sql."<br><br>";
	$res = mysql_query($sql) or die ('Dead');
	while ($row = mysql_fetch_array($res)) { 
		// Now we need to insert each new row into the udf_def table with the new location..
		$queryinsert = "INSERT INTO udf_definitions (registrantid, fieldtype, columntype, label, location, sortorder, iscompulsory, inlists, singlerow, singlelarge, is_object, object_master_id, isactive) VALUES (
		".RID.",
		".$row['fieldtype'].",
		'".$row['columntype']."',
		'".addslashes($row['label'])."',
		'".$destinationloc[1]."',
		".$nextsortid.", 
		".$row['iscompulsory'].",
		".$row['inlists'].",
		".$row['singlerow'].",
		".$row['singlelarge'].",
		1,
		'".$row['hashid']."',
		0
		)";
		// echo $queryinsert."<br><br>";
	
		 mysql_query($queryinsert) Or Die ("Cannot submit entry!");
		
		$sqlsethashid = "SELECT MAX(id) AS HashID FROM udf_definitions";
		$ressethashid = mysql_query($sqlsethashid);
		$rowsethashid = mysql_fetch_array($ressethashid)  Or Die ("Cannot set ID, please notify systems administrator with this code: SetHID: 1");
	
		$querysethashid = "UPDATE udf_definitions SET hashid = '".strtoupper(md5($rowsethashid['HashID']))."' WHERE id = ".$rowsethashid['HashID'];
		// echo $querysethashid."<br><br>";
		 mysql_query($querysethashid) Or Die ("Cannot set ID, please notify systems administrator with this code: SetHID: 1452");
		
		$sql = "ALTER TABLE udf_".$destinationloc[1]." ADD ".columnnames($row['label'])." ".$row['columntype']." NULL;";
		// echo $sql."<br><br>";
		mysql_query($sql) or die ("Cannot alter table");
		
		$nextsortid++;
	}
	$onload = 'onload="window.parent.location = window.parent.location;self.close();return false;"';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "includes/xhtml1-transitional.dtd">
<html>
<head>
	<title>Thin Line Software</title>
	<?php 
	if (ERES > 1024) { ?>
		<style type="text/css" media="all">@import "/css/style.css";</style>
	<?php 
	} else { ?>
		<style type="text/css" media="all">@import "/css/style_small.css";</style>
	<?php
	}?>
</head>
<body <?php echo $onload ?>>
<?php 
if ($gval[0] == 2) { ?>
	<table cellspacing="0" cellpadding="5" border="0" width="100%">
	<tr>
		<td valign="top" align="right" rowspan="2" width="5%">
			<img src="/images/icons/document_text_32.png"><img src="/images/spacer.gif" height="1" width="10">
		</td>
		<td valign="center" height="5"><strong><font style="font-size:130%;font-family:Trebuchet MS">Replicate Form Field(s)</font></strong></td
	</tr>
	</table><br>
	<table border="0" cellpadding="5" cellspacing="3" width="95%">
	<tr>
		<td width="7%">&nbsp;</td>
		<td class="ls_top" colspan="3"><strong>Select Module</strong></td>
	</tr>
	<?php 
	$destinationloc = explode("||", $gval[1]);
	$sql = "SELECT * FROM menu_tabs WHERE registrantid = ".RID." AND type = 'Standard Form' ORDER BY sortorder ASC";
	$res = mysql_query($sql);
	while ($row = mysql_fetch_array($res)) { 
		include("..//includes/ls.php"); 
		if ($row['ismultitier'] == 0) { 
			$query = "SELECT label, hashid FROM menusub_tabs WHERE menu_tabhashid = '".$row['hashid']."' AND type = 'new' AND registrantid = ".RID;
			$resm = mysql_query($query);
			$rowm = mysql_fetch_array($resm); ?>
			<tr>
				<td>&nbsp;</td>
				<td width="100%" colspan="2" class="ls_<?php echo $ls ?>">
					<table>
					<tr>
						<td><img src="/images/icons/navigate_right2_16.png"></td>
						<td>&nbsp;</td>
						<td>
							<?php 
							if ($rowm['hashid'] != $destinationloc[1]) { ?>
								<a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=4:<?php echo $row['hashid'] ?>||<?php echo $rowm['hashid'] ?>:<?php echo $gval[1] ?>"><?php echo $row['label']; ?></a>
							<?php 
							} else { ?>
								<?php echo $row['label']; ?>
							<?php 
							} ?>
						</td>
					</tr>
					</table>
				</td>
			</tr>
	<?php 
		} else { ?>
			<tr>
				<td>&nbsp;</td>
				<td colspan="2" class="ls_<?php echo $ls ?>">
					<table border=0>
					<tr>
						<td width="4%"><img src="/images/icons/navigate_right2_16.png"></td>
						<td><?php echo $row['label']; ?></td>
						<td width="20%"><font style="text-align:right" class="noteg">Select a Sub Category</font></td>
					</tr>
					</table>
				</td>
			</tr>
	<?php  // If it is a multi-tier then we deliver the two sub items
			$query = "SELECT label, hashid FROM menusub_tabs WHERE menu_tabhashid = '".$row['hashid']."' AND type = 'new' AND registrantid = ".RID;
			$resm = mysql_query($query);
			while ($rowm = mysql_fetch_array($resm)) { ?>
			<tr>
				<td>&nbsp;</td>
				<td width="3%">&nbsp;</td>
				<td class="ls_<?php echo $ls ?>">
					<table>
					<tr>
						<td><img src="/images/icons/navigate_right_16.png"></td>
						<td>&nbsp;</td>
						<td>
							<?php 
							if ($rowm['hashid'] != $destinationloc[1]) { ?>
								<a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=4:<?php echo $row['hashid'] ?>||<?php echo $rowm['hashid'] ?>:<?php echo $gval[1] ?>"><?php echo ucwords(str_replace("new","", $rowm['label'])); ?></a>
							<?php 
							} else { ?>
								<?php echo ucwords(str_replace("new","", $rowm['label'])); ?>
							<?php 
							} ?>
						</td>
					</tr>
					</table>
				</td>
			</tr>
	<?php 
			}
		}
	}
} elseif ($gval[0] == 4) { ?>
	<table cellspacing="0" cellpadding="5" border="0" width="95%">
	<tr>
		<td valign="top" align="right" rowspan="2" width="5%">
			<img src="/images/icons/document_text_32.png"><img src="/images/spacer.gif" height="1" width="10">
		</td>
		<td valign="center" height="5"><strong><font style="font-size:130%;font-family:Trebuchet MS">Replicate Form Field(s)</font></strong></td>
		<td align="right">&nbsp;</td>
	</tr>
	</table><br>
	<table border="0" cellpadding="5" cellspacing="3" width="95%">
	<tr>
		<td width="7%">&nbsp;</td>
		<td colspan="2" class="ls_top"><strong>Select Fields</strong></td>
	</tr>
	<tr>
		<td width="7%">&nbsp;</td>
		<td align="center" width="75%" class="ls_top" ><strong>Field Names</strong></td>
		<td align="center" class="ls_top"><strong>Select Fields</strong></td>
	</tr>
	</table>
	<form method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?>">
	<table border="0" cellpadding="5" cellspacing="3" width="95%">
	<?php 
	// Now we know which module we are dealing with lets select 
	// the fields
	$location = explode("||",$gval[1]);
	$destinationloc = explode("||",$gval[2]);

	// Lets build an array of the current fields for this module...
	$sql = "SELECT label FROM udf_definitions WHERE location = '".$destinationloc[1]."' AND registrantid = ".RID;
	// echo $sql."<br><br>";
	$res = mysql_query($sql) or die ('Dead');
	$destloc = array();
	while ($row = mysql_fetch_array($res)) {
		array_push($destloc, $row['label']);
	}

	$sql = "SELECT hashid, label FROM udf_definitions WHERE location = '".$location[1]."' AND registrantid = ".RID." AND fieldtype != 11 ORDER BY sortorder ASC";
	// echo $sql;
	$res = mysql_query($sql);
	while ($row = mysql_fetch_array($res)) { 
			include("..//includes/ls.php"); ?>
		<tr>
			<td width="7%">&nbsp;</td>
			<td width="65%" class="ls_<?php echo $ls ?>"><?php echo $row['label']; ?></td>
			<?php 
			if (!in_array($row['label'],$destloc)) { ?>
			<td align="center" class="ls_<?php echo $ls ?>"><input name="formfields[]" value="<?php echo $row['hashid'] ?>" type="checkbox"></td>
			<?php 
			} else { ?>
			<td align="center" class="ls_<?php echo $ls ?>"><font class="noteg">Field name already used</font></td>
			<?php 
			} ?>
		</tr>
<?php 
	} ?>
	<tr>
		<td>&nbsp;</td>
		<td align="left"><br><a href="#" onclick="history.go(-1)">&laquo; Back</a></td>
		<td align="right"><br><input type="submit" value="Replicate &raquo;"></td>
	</tr>
	</table>
	<input type="hidden" name="pform" value="1">
	<input type="hidden" name="sourcelocation" value="<?php echo $location[1]; ?>">
	<input type="hidden" name="destinationlocation" value="<?php echo $gval[2]; ?>">
	</form>
<?php 
} elseif ($gval[0] == 3) { ?>
	<form method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?>">
	<table cellspacing="0" cellpadding="5" border="0" width="100%">
	<tr>
		<td valign="top" align="right" rowspan="2" width="5%">
			<img src="/images/icons/component_32.png"><img src="/images/spacer.gif" height="1" width="10">
		</td>
		<td valign="center" height="5"><strong><font style="font-size:130%;font-family:Trebuchet MS">Apply Object(s)</font></strong></td
	</tr>
	</table><br>
	<table border="0" cellpadding="5" cellspacing="3" width="95%">
	<tr>
		<td width="7%">&nbsp;</td>
		<td class="ls_top" colspan="3"><strong>Select Obect(s)</strong></td>
	</tr>
	<?php 
	$query = "SELECT hashid, label FROM udf_definitions WHERE is_object = 1 AND is_object_master = 1 AND registrantid =".RID." ORDER BY label ASC"; 
	 //echo $query;
	$res = mysql_query($query);
	while ($row = mysql_fetch_array($res)) { 
		include("..//includes/ls.php");?>
		<tr>
			<td width="7%">&nbsp;</td>
			<td class="ls_<?php echo $ls ?>"><?php echo $row['label'] ?></td>
			<td width="15%" align="center" class="ls_<?php echo $ls ?>"><input name="formfields[]" value="<?php echo $row['hashid'] ?>" type="checkbox"></td>
		</tr>
	<?php 
	} ?>
	<tr>
		<td>&nbsp;</td>
		<td align="left">&nbsp;</td>
		<td align="right"><br><input type="submit" value="Apply &raquo;"></td>
	</tr>
	</table>
	<input type="hidden" name="destinationlocation" value="<?php echo $gval[1]; ?>">
	<input type="hidden" name="pform" value="2">
	</form>
<?php 
} ?>
</body>
</html>
