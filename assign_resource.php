<?php 
include_once("_globalconnect.php");

if ($gval[0] == 2) {
	// Lets get the date selection to find the right active price
	$sql = "SELECT fromdate, todate FROM resource_allocation WHERE id = '".$gval[2]."'";
	// echo $sql."<br><br>";
	$res = mysql_query($sql);
	$row = mysql_fetch_array($res);
	$startdate = $row['fromdate'];
	$enddate = $row['todate'];
	
	// Lets get the rates for this particular resource
	$querygetprice = "SELECT validfrom, validto, price FROM pricing WHERE location = '7647966B7343C29048673252E490F736' AND recordid = '".$gval[1]."'";
	// echo $querygetprice."<br>";
	$resgetprice = mysql_query($querygetprice);
	while ($rowgetprice = mysql_fetch_array($resgetprice)) {
		$startdatei = $rowgetprice['validfrom']." 00:00:00";
		$enddatei = $rowgetprice['validto']." 23:59:59";
		
		//echo "From Date: ".$startdate." - ".$startdatei."<br>";
		//echo "To Date: ".$enddate." - ".$enddatei."<br><br>";
		
		if ((($startdate >= $startdatei) && ($enddate <= $enddatei)) 
				|| (($startdate <= $enddatei) && ($startdate >= $startdatei))
				|| ( ($enddate >= $startdatei) && ($enddate <= $enddatei) )
				|| ( ($startdate <= $startdatei) && ($enddate >= $enddatei) )) {
			//echo "<br>Match in range...";
			$addrated = $rowgetprice['price'];
			break;
		}
	}
	
	// Lets add the resource to the line item...
	$queryudf = "UPDATE resource_allocation SET 
				resourceid = '".$gval[1]."',
				chargeforeach = '".$gval[3]."',
				rate = '".$addrated."'
				WHERE id = ".$gval[2]."";
	// echo $queryudf;
	mysql_query($queryudf) Or Die ("Cannot submit entry! 115");
	
	$onload = 'onload="window.parent.location = window.parent.location;self.close();return false;"';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "includes/xhtml1-transitional.dtd">
<html>
<head>
	<title>Link Records</title>
	<?php 
	if (ERES > 1024) { ?>
		<style type="text/css" media="all">@import "css/style.css";</style>
	<?php 
	} else { ?>
		<style type="text/css" media="all">@import "css/style_small.css";</style>
	<?php
	}?>		
</head>

<body <?php echo $onload ?>>
<form method="POST" name="resource_manager" action="thinline.php?id=<?php echo ":".$gval[1].":".$gval[2].":".$gval[3].":".$gval[4].":".$gval[5].":".$gval[6].":".$gval[7].":".$gval[8].":".$gval[9].":".$gval[10].":".$gval[11].":".$gval[12].":".$gval[13].":".$gval[14].":".$gval[15].":".$gval[16].":".$gval[17].":".$gval[18].":".$gval[19].":".$gval[20].":".$gval[21].":".$gval[22].":".$gval[23].":".$gval[24].":".$gval[25]; ?>">
	<table cellspacing="0" cellpadding="0" border="0" width="97%">
	<tr>
		<td><img src="images/<?php echo $_SESSION['franchisedata']['logo'];?>" alt="" / align="center" border="0"></td>
	</tr>
	</table><br>
	<table cellspacing="0" cellpadding="0" border="0" width="98%">
	<tr>
		<td valign="top" align="right" rowspan="2" width="2%">&nbsp;</td>
		<td valign="top" align="right" width="5%">
			<img src="images/icons/link.png"><img src="images/spacer.gif" height="1" width="10">
		</td>
		<td valign="top" height="5" width="60%">
			<strong><font style="font-size:130%;font-family:Trebuchet MS">Add Item</font></strong>
		</td>
		<td width="25%"><?php include_once("includes/record_gen.php"); ?></td>
		<td align="right" valign="top" height="5" >
		<div id="menu_bar">
		<script language="JavaScript">
		function submitform() {
			document.resource_manager.submit();
		}
		</script>
			<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td><img src="images/menu/menu_01.gif" width="13" height="25" alt=""></td>
				<td><img src="images/menu/new_g.gif" width="18" height="25" border="0" alt=""></a></td>
				<td><a href="javascript: submitform()"><img src="images/menu/save.gif" width="23" height="25" alt="Update &amp; Close" border="0"></a></td>
				<td><img src="images/menu/menu_08.gif" width="8" height="25" alt=""></td>
				<td><a href="thinline.php?id=<?php echo $revfull ?>"><img src="images/refresh.gif" border="0" alt="Refresh this page."></a></td>
				<td><img src="images/menu/menu_11.gif" width="9" height="25" alt=""></td>
				<td><img src="images/menu/menu_13.gif" width="18" height="25" alt=""></td>
			</tr>
		</table>
		</div>
		</td>
		<td rowspan="3">&nbsp;</td>
	</tr>
	
	<tr>
		<td>&nbsp;</td>
		<td colspan="3" align="left"><br><font class="note">Click the <strong>Save</strong> button when you have completed adding resources.</font><br><br style="line-height:10px"></td>
	</tr>
	<tr>
		<td width="10"><img src="images/spacer.gif" width="25"></td>
		<td valign="top" colspan="4">
			<table class="note" width="98%" border="0" cellspacing="2" cellpadding="1">
						<?php 
						// We need to get the dates from the booking to check which resources are available
						$querymsh = "SELECT fromdate, todate FROM resource_allocation WHERE id=".$gval[3]."";
						// echo $querymsh."<br>";
						$resmsh = mysql_query($querymsh);
						$row = mysql_fetch_array($resmsh);
						$startdate = $row['fromdate'];
						$enddate = $row['todate'];
						
						// echo "From Date: ".$startdate."<br>";
						// echo "To Date: ".$enddate."<br><br>";
						
						// Lets determine which resources can be shown based on the
						// already allocated items
						$querymsh = "SELECT resourceid, fromdate, todate FROM resource_allocation WHERE NOT resourceid = 'undefined'";
						// echo $querymsh."<br>";
						$resmsh = mysql_query($querymsh);
						while ($row = mysql_fetch_array($resmsh)) {
							$startdatei = $row['fromdate'];
							$enddatei = $row['todate'];
							// Need to loop through all the resources and check which items
							// don't have $startdate and $enddate assigned against them.
							
							if ((($startdate >= $startdatei) && ($enddate <= $enddatei)) 
									|| (($startdate <= $enddatei) && ($startdate >= $startdatei))
									|| ( ($enddate >= $startdatei) && ($enddate <= $enddatei) )
									|| ( ($startdate <= $startdatei) && ($enddate >= $enddatei) )) {
									// echo "<br>Match in range...";
									$excludeids .= "AND hashid != '".$row['resourceid']."' ";
								}
						}
						
						// Get Parent Table Location 
						$num_rows = 2;
						$location = $gval[1];
						$tablecolumns = "Resource Name, Charge for Each";
						$columnstr = "resourcename,chargeforeach";
						$tablecolumns = explode(",", $tablecolumns);
						$trcounter = 1;
						$getprimary = 1;
						foreach($tablecolumns as &$value) {
							if ($trcounter == 1) { echo "<tr>\n";}
							if ($getprimary == 1) {	$primarysortlabel = columnnames($value); }
							echo "<td class=\"ls_top\" align='center'>
							<table border=0><tr><td><strong>".$value."</strong>&nbsp;</td>";
							if ($gval[18] == "ASC") {
								echo '<td><a href="thinline.php?id='.$gval[0].':'.$gval[1].':'.$gval[2].':::::'.$gval[7].':'.$gval[8].':'.$gval[9].':'.$gval[10].':'.$gval[11].':'.$gval[12].':'.$gval[13].':'.$gval[14].':'.$gval[15].':'.$gval[16].':'.$gval[17].':DESC:'.columnnames($value).':'.$gval[20].':'.$gval[21].':'.$gval[22].':'.$gval[23].':'.$gval[24].':'.$gval[25].'">';
								if ($gval[19] == columnnames($value)) {
									echo "<img src=\"images/icons/sort_asc.gif\" border=\"0\"></a></td>";
								} else {
									echo "<img src=\"images/icons/sort_asc_g.gif\" border=\"0\"></a></td>";
								}
							} else {
								echo '<td><a href="thinline.php?id='.$gval[0].':'.$gval[1].':'.$gval[2].':::::'.$gval[7].':'.$gval[8].':'.$gval[9].':'.$gval[10].':'.$gval[11].':'.$gval[12].':'.$gval[13].':'.$gval[14].':'.$gval[15].':'.$gval[16].':'.$gval[17].':ASC:'.columnnames($value).':'.$gval[20].':'.$gval[21].':'.$gval[22].':'.$gval[23].':'.$gval[24].':'.$gval[25].'">';
								if ($gval[19] == columnnames($value)) {
									echo "<img src=\"images/icons/sort_desc.gif\" border=\"0\"></a></td>";
								} else {
									echo "<img src=\"images/icons/sort_asc_g.gif\" border=\"0\"></a></td>";
								}
							}
							echo "</tr></table></td>\n";
							if ($trcounter == $num_rows) { 
								echo "<td colspan=\"2\">&nbsp;</td>\n";
								echo "</tr>\n"; $trcounter = 0; 
							}
							$trcounter++;
							$getprimary++;
						}
						$columns = explode(",", $columnstr);
						$sql = "SELECT hashid, resourcename, chargeforeach, accountcode, taxcode
								FROM udf_7647966B7343C29048673252E490F736
								WHERE registrantid = ".RID." ".$excludeids." ".$addsql;
								// echo $sql."<br>";
						$query_pages = $sql;
						if ($gval[18] == "") {
							if ($gval[9] == "") {
								$sql .= " ORDER BY ".$columns[0]." ASC LIMIT 0, ".$size_of_group;
							} else {
								$sql .= " ORDER BY ".$columns[0]." ASC LIMIT ".$gval[9].", ".$size_of_group."";
							}
						} else {
							if ($gval[9] == "") {
								$gval[9] = 0;
							} 
							$sql .= " ORDER BY ".$gval[19]." ".$gval[18]." LIMIT ".$gval[9].", ".$size_of_group."";
						}
						// echo $sql."<br><br>";
						$res = mysql_query($sql);
						$num_rows5 = mysql_num_rows($res);
						$rec_remaining = ($gval[9]-10);
						if ((!$num_rows5) && ($rec_remaining >= 0)) {
							// Need to change page ?>
							<script type="text/javascript">
							<!--
								window.location = "add_resources.php?id=:::::<?php echo $gval[5] ?>::::<?php echo $rec_remaining ?>:::::::::::<?php echo $gval[20] ?>:<?php echo $gval[21] ?>::::"
							//-->
							</script>
					<?php 
						}
						$res = mysql_query($sql);
						while ($row = mysql_fetch_array($res)) { 
							include("includes/ls.php"); 
							echo "<tr>";
							for ($number = 0; $number < $num_rows; $number++) { 
								if (array_key_exists($row[$columns[$number]], $droppedinarray)) {
								    $newclass = "messageboard";
									$added_already = $droppedinarray[$row[$columns[$number]]];
								} 
								
								if (preg_match("(\d{4}[/-]\d{2}[/-]\d{2})", $row[$columns[$number]])) { ?>
									<td class="ls_<?php echo $ls ?>"><?php echo date("d-M",strtotime($row[$columns[$number]])); ?>&nbsp;</td>
								<?php 
								}  else {?>
									<td class="ls_<?php echo $ls ?>"><?php echo $row[$columns[$number]]; ?>&nbsp;</td>
					<?php 		}
								if ($number == 0) {
									$passval = $row[$columns[$number]];
								}
							}
							echo '<td width="10%" align="center" class="ls_'.$ls.'"><a href="thinline.php?id=2:'.$row['hashid'].':'.$gval[3].':'.$row['chargeforeach'].'"><font class="note">Add</font></a></td>';
							echo "</tr>";
							unset($added_already);
						} ?>
			</table>
			<table width="100%" border="0" cellspacing="2" cellpadding="1">
			<tr>
				<td colspan="6"><?php include("includes/pages_next_prev.php"); ?></td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
<input type="hidden" name="updatesave" value="1">
</form>
</body>
</html>