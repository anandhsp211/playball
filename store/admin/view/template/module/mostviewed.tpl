<?php echo $header; ?><link rel="stylesheet" type="text/css" href="view/stylesheet/mvstyle.css" /><?php echo $column_left; ?>

<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-mostviewed" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
  <div class="panel panel-default">
  <div class="panel-heading">
  <h3 class="panel-title"><?php echo $text_real_mv; ?></h3>
  </div>
  <div class="panel-body">
  	<div class="table-responsive">
  		<table class="table table-bordered table-striped">
            <tbody>
            <?php $i = 1; ?>
            <?php foreach ($real_mv_products as $real_mv_product) { ?>
                <?php if($i == 1 || $i == 6 || $i == 11 || $i == 16) { ?>
                	<tr>
                <?php } ?>
                	<td><div class="text-name pull-left"><b><?php echo $i; ?>.</b>&nbsp;<?php echo $real_mv_product['name']; ?></div><img src="<?php echo $real_mv_product['thumb']; ?>" class="img-thumbnail pull-right" alt="<?php echo $real_mv_product['name']; ?>"><div style="clear:both;"></div></td>
                
                <?php if($i == 5 || $i == 10 || $i == 15 || $i == 20) { ?>
                	</tr>
                <?php } ?>
                 <?php $i++; ?>
            <?php } ?>
            </tbody>
  		</table>
	</div>
    </div>
  </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-mostviewed" class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
            <div class="col-sm-10">
              <input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
              <?php if ($error_name) { ?>
              <div class="text-danger"><?php echo $error_name; ?></div>
              <?php } ?>
            </div>
          </div>          
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-product"><?php echo $entry_product; ?></label>
            <div class="col-sm-10">
              <input type="text" name="product" value="" placeholder="<?php echo $entry_product; ?>" id="input-product" class="form-control" />
              <div id="mostviewed-product" class="well well-sm" style="height: 150px; overflow: auto;">
                <?php foreach ($products as $product) { ?>
                <div id="mostviewed-product<?php echo $product['product_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $product['name']; ?>
                  <input type="hidden" name="product[]" value="<?php echo $product['product_id']; ?>" />
                </div>
                <?php } ?>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-limit"><?php echo $entry_limit; ?></label>
            <div class="col-sm-10">
              <input type="text" name="limit" value="<?php echo $limit; ?>" placeholder="<?php echo $entry_limit; ?>" id="input-limit" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-width"><?php echo $entry_width; ?></label>
            <div class="col-sm-10">
              <input type="text" name="width" value="<?php echo $width; ?>" placeholder="<?php echo $entry_width; ?>" id="input-width" class="form-control" />
              <?php if ($error_width) { ?>
              <div class="text-danger"><?php echo $error_width; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-height"><?php echo $entry_height; ?></label>
            <div class="col-sm-10">
              <input type="text" name="height" value="<?php echo $height; ?>" placeholder="<?php echo $entry_height; ?>" id="input-height" class="form-control" />
              <?php if ($error_height) { ?>
              <div class="text-danger"><?php echo $error_height; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
              <select name="status" id="input-status" class="form-control">
                <?php if ($status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
					var ajaxMostViewed = $('input[name=\'product\']');
						var customAutocomplete = null;
						ajaxMostViewed.autocomplete({
							delay: 500,
							responsea : function (items){
								if (items.length) {
									for (i = 0; i < items.length; i++) {
										this.items[items[i]['value']] = items[i];
									}
								}
								var html='';
								if(items.length){
									$.each(items,function(key,item){
										if(item.product_id!=0){
										html += '<li data-value="' + item['value'] + '"><a href="#"><h5>'+item.label+'</h5><img title="'+item.name+'" src="'+item.image+'"/><div class="clear"></div></a></li>';
										}
									});
								}	
								if (html) {
									ajaxMostViewed.siblings('ul.dropdown-menu').show();
								} else {
									ajaxMostViewed.siblings('ul.dropdown-menu').hide();
								}

								$(ajaxMostViewed).siblings('ul.dropdown-menu').html(html);
							},
							source: function(request, responsea) {
							customAutocomplete = this;
								$.ajax({
									url: 'index.php?route=catalog/mvproduct/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
									dataType : 'json',
									success : function(json) {
										customAutocomplete.responsea($.map(json, function(item) {
											return {
												label: item.name,
												name: item.name,
												value: item.product_id,
												image: item.image
											}
										}));
									}
								});
							},
	select: function(item) {
		$('input[name=\'product\']').val('');
		
		$('#mostviewed-product' + item['value']).remove();
		
		$('#mostviewed-product').append('<div id="mostviewed-product' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product[]" value="' + item['value'] + '" /></div>');	
	}
						});			
	
$('#mostviewed-product').delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();
});
//--></script></div>
<?php echo $footer; ?>