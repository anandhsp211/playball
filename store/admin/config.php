<?php

$server = $_SERVER['HTTP_HOST'];

if ($server == "backoffice.playballkids.local") {
    // Development Server
    $serverPath = "D:/xampp/htdocs/thinline";
    $serverURL = "http://backoffice.playballkids.local";
} elseif ($server == "staging-backoffice.playballkids.com") {
    // Staging Server
    $serverPath = "/var/www/staging-backoffice.playballkids.com";
    $serverURL = "http://staging-backoffice.playballkids.com";
} elseif ($server == "backoffice.playballkids.com") {
    // Staging Server
    $serverPath = "/var/www/thinline";
    $serverURL = "http://backoffice.playballkids.com";
}

// HTTP
define('HTTP_SERVER', $serverURL.'/store/admin/');
define('HTTP_CATALOG', $serverURL.'/store/');

// HTTPS
define('HTTPS_SERVER', $serverURL.'/store/admin/');
define('HTTPS_CATALOG', $serverURL.'http/store/');

// DIR
define('DIR_APPLICATION', $serverPath.'/store/admin/');
define('DIR_SYSTEM', $serverPath.'/store/system/');
define('DIR_LANGUAGE', $serverPath.'/store/admin/language/');
define('DIR_TEMPLATE', $serverPath.'/store/admin/view/template/');
define('DIR_CONFIG', $serverPath.'/store/system/config/');
define('DIR_IMAGE', $serverPath.'/store/image/');
define('DIR_CACHE', $serverPath.'/store/system/storage/cache/');
define('DIR_DOWNLOAD', $serverPath.'/store/system/storage/download/');
define('DIR_LOGS', $serverPath.'/store/system/storage/logs/');
define('DIR_MODIFICATION', $serverPath.'/store/system/storage/modification/');
define('DIR_UPLOAD', $serverPath.'/store/system/storage/upload/');
define('DIR_CATALOG', $serverPath.'/store/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'mediatomcat');
define('DB_PASSWORD', 'uTTKYP5ztuTGc3YA');
define('DB_DATABASE', 'franchise_playball_store');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
