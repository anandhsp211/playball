<?php
// Text
$_['text_title']       = 'Per Order Basis';
$_['text_description'] = 'You will be contacted by the supplier with the shipping costs - this figure is set to zero in the meantime.';