<div class="box featured_box">
  <h3><?php echo $heading_title; ?></h3>
 
      <div class="box-products  featured_carousel slide" id="featured_slide" >

        <?php if( count($products) > $itemspage ) { ?>
        <div class="carousel-controls">
        <a class="carousel-control left" href="#featured_slide"   data-slide="prev">&lsaquo;</a>
        <a class="carousel-control right" href="#featured_slide"  data-slide="next">&rsaquo;</a>
        </div>
        <?php } ?>
        <div class="carousel-inner ">
         <?php
          $span = 12/$itemscolumn;
          $pages = array_chunk( $products, $itemspage);
         ?>
          <?php foreach ($pages as  $k => $tproducts ) {   ?>
            <div class="item <?php if($k==0) {?>active<?php } ?>">
              <?php foreach( $tproducts as $i => $product ) {  $i=$i+1;?>
                <?php if( $i%$itemscolumn == 1 ) { ?>
                  <div class="row box-product">
                <?php } ?>
                    <div class="col-lg-<?php echo $span;?> col-sm-<?php echo $span;?> col-xs-12 product-block">
                    <div class="product-thumb product-inner">
                      <?php if ($product['thumb']) { ?>
                      <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
                      <?php } ?>
                      <div class="caption">
                        <div class="name"><h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4></a></div>
                        <div class="description">
                          <?php echo utf8_substr( strip_tags($product['description']),0,58);?>...
                        </div>
                        <?php if ($product['price']) { ?>
                        <div class="price">
                          <?php if (!$product['special']) { ?>
                          <?php echo $product['price']; ?>
                          <?php } else { ?>
                          <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
                          <?php } ?>
                        </div>
                        <?php } ?>
                        <?php if ($product['rating']) { ?>
                        <div class="rating">
                          <?php for ($i = 1; $i <= 5; $i++) { ?>
                          <?php if ($product['rating'] < $i) { ?>
                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                          <?php } else { ?>
                          <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                          <?php } ?>
                          <?php } ?>
                        </div>
                        <?php } ?>
                      </div>  
                      <div class="button-group">
                        <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>
                        <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button>
                        <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></button>
                      </div>
                    </div>
                    </div>

                <?php if( $i%$itemscolumn == 0 || $i==count($tproducts) ) { ?>
                 </div>
                <?php } ?>
              <?php } //endforeach; ?>
            </div>
          <?php } ?>
        </div>
      </div>
  
 
   </div>
<script>
$('.featured_carousel').carousel({interval:false,auto:false,pause:'hover'});
</script>