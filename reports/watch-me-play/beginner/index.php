<?php
include $_SERVER['DOCUMENT_ROOT'].'/reports/reports-data.php'; ?>
<head>
  <title>Beginner - Watch Me Play</title>
  <?php include $_SERVER['DOCUMENT_ROOT'].'/reports/reports-header.php'; ?>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table id="Table_01" width="1240" height="1755" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="6">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-1_01.gif" width="1240" height="328" alt=""></td>
	</tr>
	<tr>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-1_02.gif" width="317" height="1426" alt=""></td>
		<td colspan="3" width="542" height="45">
			<?php echo $grade;?></td>
		<td colspan="2" rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-1_04.gif" width="381" height="91" alt=""></td>
	</tr>
	<tr>
		<td colspan="3">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-1_05.gif" width="542" height="46" alt=""></td>
	</tr>
	<tr>
		<td rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-1_06.gif" width="210" height="1335" alt=""></td>
		<td colspan="3" width="563" height="63">
			<?php echo $childs_name;?></td>
		<td rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-1_08.gif" width="150" height="1335" alt=""></td>
	</tr>
	<tr>
		<td colspan="3">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-1_09.gif" width="563" height="12" alt=""></td>
	</tr>
	<tr>
		<td rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-1_10.gif" width="54" height="1260" alt=""></td>
		<td colspan="2" width="509" height="69">
			<?php echo $venue;?></td>
	</tr>
	<tr>
		<td colspan="2">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-1_12.gif" width="509" height="1191" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/spacer.gif" width="317" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/spacer.gif" width="210" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/spacer.gif" width="54" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/spacer.gif" width="278" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/spacer.gif" width="231" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/spacer.gif" width="150" height="1" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-2_01.gif" width="1240" height="1265" alt=""></td>
	</tr>
	<tr>
		<td rowspan="16">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-2_02.gif" width="984" height="489" alt=""></td>
		<td width="27" height="29">
			<?php echo ($answers[0] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="16">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-2_04.gif" width="74" height="489" alt=""></td>
		<td width="29" height="29">
			<?php echo ($answers[0] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="16">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-2_06.gif" width="126" height="489" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-2_07.gif" width="27" height="9" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-2_08.gif" width="29" height="9" alt=""></td>
	</tr>
	<tr>
		<td width="27" height="30">
			<?php echo ($answers[1] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="29" height="30">
			<?php echo ($answers[1] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-2_11.gif" width="27" height="10" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-2_12.gif" width="29" height="10" alt=""></td>
	</tr>
	<tr>
		<td width="27" height="28">
			<?php echo ($answers[2] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="29" height="28">
			<?php echo ($answers[2] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-2_15.gif" width="27" height="15" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-2_16.gif" width="29" height="15" alt=""></td>
	</tr>
	<tr>
		<td width="27" height="28">
			<?php echo ($answers[3] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="29" height="28">
			<?php echo ($answers[3] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-2_19.gif" width="27" height="12" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-2_20.gif" width="29" height="12" alt=""></td>
	</tr>
	<tr>
		<td width="27" height="27">
			<?php echo ($answers[4] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="29" height="27">
			<?php echo ($answers[4] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-2_23.gif" width="27" height="12" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-2_24.gif" width="29" height="12" alt=""></td>
	</tr>
	<tr>
		<td width="27" height="29">
			<?php echo ($answers[5] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="29" height="29">
			<?php echo ($answers[5] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-2_27.gif" width="27" height="33" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-2_28.gif" width="29" height="33" alt=""></td>
	</tr>
	<tr>
		<td width="27" height="29" >
			<?php echo ($answers[6] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="29" height="29">
			<?php echo ($answers[6] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-2_31.gif" width="27" height="12" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-2_32.gif" width="29" height="12" alt=""></td>
	</tr>
	<tr>
		<td width="27" height="28">
			<?php echo ($answers[7] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="29" height="28">
			<?php echo ($answers[7] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-2_35.gif" width="27" height="158" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-2_36.gif" width="29" height="158" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-3_01.gif" width="1240" height="370" alt=""></td>
	</tr>
	<tr>
		<td rowspan="18">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-3_02.gif" width="811" height="1384" alt=""></td>
		<td width="37" height="38">
			<?php echo ($answers[8] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="18">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-3_04.gif" width="67" height="1384" alt=""></td>
		<td width="40" height="38">
			<?php echo ($answers[8] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="18">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-3_06.gif" width="285" height="1384" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-3_07.gif" width="37" height="27" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-3_08.gif" width="40" height="27" alt=""></td>
	</tr>
	<tr>
		<td width="37" height="41">
			<?php echo ($answers[9] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="41">
			<?php echo ($answers[9] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-3_11.gif" width="37" height="24" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-3_12.gif" width="40" height="24" alt=""></td>
	</tr>
	<tr>
		<td width="37" height="36">
			<?php echo ($answers[10] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="36">
			<?php echo ($answers[10] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-3_15.gif" width="37" height="328" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-3_16.gif" width="40" height="328" alt=""></td>
	</tr>
	<tr>
		<td width="37" height="36">
			<?php echo ($answers[11] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="36">
			<?php echo ($answers[11] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-3_19.gif" width="37" height="30" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-3_20.gif" width="40" height="30" alt=""></td>
	</tr>
	<tr>
		<td width="37" height="39">
			<?php echo ($answers[12] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="39">
			<?php echo ($answers[12] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-3_23.gif" width="37" height="30" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-3_24.gif" width="40" height="30" alt=""></td>
	</tr>
	<tr>
		<td width="37" height="37">
			<?php echo ($answers[13] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="37">
			<?php echo ($answers[13] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-3_27.gif" width="37" height="351" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-3_28.gif" width="40" height="351" alt=""></td>
	</tr>
	<tr>
		<td width="37" height="39">
			<?php echo ($answers[14] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="39">
			<?php echo ($answers[14] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-3_31.gif" width="37" height="27" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-3_32.gif" width="40" height="27" alt=""></td>
	</tr>
	<tr>
		<td width="37" height="37">
			<?php echo ($answers[15] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="37">
			<?php echo ($answers[15] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-3_35.gif" width="37" height="30" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-3_36.gif" width="40" height="30" alt=""></td>
	</tr>
	<tr>
		<td width="37" height="38">
			<?php echo ($answers[16] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="38">
			<?php echo ($answers[16] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-3_39.gif" width="37" height="196" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-3_40.gif" width="40" height="196" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-4_01.gif" width="1240" height="274" alt=""></td>
	</tr>
	<tr>
		<td rowspan="16">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-4_02.gif" width="811" height="1480" alt=""></td>
		<td width="36" height="37">
			<?php echo ($answers[17] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="16">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-4_04.gif" width="67" height="1480" alt=""></td>
		<td width="40" height="37">
			<?php echo ($answers[17] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="16">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-4_06.gif" width="286" height="1480" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-4_07.gif" width="36" height="52" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-4_08.gif" width="40" height="52" alt=""></td>
	</tr>
	<tr>
		<td width="36" height="39">
			<?php echo ($answers[18] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="39" >
			<?php echo ($answers[18] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-4_11.gif" width="36" height="32" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-4_12.gif" width="40" height="32" alt=""></td>
	</tr>
	<tr>
		<td width="36" height="37">
			<?php echo ($answers[19] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="37">
			<?php echo ($answers[19] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-4_15.gif" width="36" height="318" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-4_16.gif" width="40" height="318" alt=""></td>
	</tr>
	<tr>
		<td width="36" height="39">
			<?php echo ($answers[20] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="39" >
			<?php echo ($answers[20] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-4_19.gif" width="36" height="37" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-4_20.gif" width="40" height="37" alt=""></td>
	</tr>
	<tr>
		<td width="36" height="38">
			<?php echo ($answers[21] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="38">
			<?php echo ($answers[21] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-4_23.gif" width="36" height="30" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-4_24.gif" width="40" height="30" alt=""></td>
	</tr>
	<tr>
		<td width="36" height="37">
			<?php echo ($answers[22] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="37">
			<?php echo ($answers[22] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-4_27.gif" width="36" height="339" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-4_28.gif" width="40" height="339" alt=""></td>
	</tr>
	<tr>
		<td width="36" height="39">
			<?php echo ($answers[23] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="39">
			<?php echo ($answers[23] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-4_31.gif" width="36" height="36" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-4_32.gif" width="40" height="36" alt=""></td>
	</tr>
	<tr>
		<td width="36" height="37">
			<?php echo ($answers[24] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="37">
			<?php echo ($answers[24] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-4_35.gif" width="36" height="333" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-4_36.gif" width="40" height="333" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-5_01.gif" width="1240" height="266" alt=""></td>
	</tr>
	<tr>
		<td rowspan="14">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-5_02.gif" width="811" height="1488" alt=""></td>
		<td width="38" height="39">
			<?php echo ($answers[25] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="14">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-5_04.gif" width="66" height="1488" alt=""></td>
		<td width="39" height="39">
			<?php echo ($answers[25] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="14">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-5_06.gif" width="286" height="1488" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-5_07.gif" width="38" height="34" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-5_08.gif" width="39" height="34" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="39">
			<?php echo ($answers[26] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="39">
			<?php echo ($answers[26] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-5_11.gif" width="38" height="23" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-5_12.gif" width="39" height="23" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="38">
			<?php echo ($answers[27] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="38">
			<?php echo ($answers[27] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-5_15.gif" width="38" height="327" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-5_16.gif" width="39" height="327" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="38">
			<?php echo ($answers[28] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="38">
			<?php echo ($answers[28] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-5_19.gif" width="38" height="26" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-5_20.gif" width="39" height="26" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="38">
			<?php echo ($answers[29] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="38">
			<?php echo ($answers[29] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-5_23.gif" width="38" height="26" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-5_24.gif" width="39" height="26" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="37">
			<?php echo ($answers[30] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="37">
			<?php echo ($answers[30] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-5_27.gif" width="38" height="515" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-5_28.gif" width="39" height="515" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="37">
			<?php echo ($answers[31] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="37">
			<?php echo ($answers[31] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-5_31.gif" width="38" height="271" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-5_32.gif" width="39" height="271" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1755" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="9">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-6_01.gif" width="1240" height="321" alt=""></td>
	</tr>
	<tr>
		<td colspan="4" rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-6_02.gif" width="812" height="1054" alt=""></td>
		<td width="38" height="38">
			<?php echo ($answers[32] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-6_04.gif" width="66" height="1054" alt=""></td>
		<td width="37" height="38">
			<?php echo ($answers[32] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td colspan="2" rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-6_06.gif" width="287" height="1054" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-6_07.gif" width="38" height="417" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-6_08.gif" width="37" height="417" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="38">
			<?php echo ($answers[33] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="37" height="38">
			<?php echo ($answers[33] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-6_11.gif" width="38" height="561" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-6_12.gif" width="37" height="561" alt=""></td>
	</tr>
	<tr>
		<td rowspan="3">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-6_13.gif" width="111" height="379" alt=""></td>
		<td colspan="7" width="1020" height="187">
			<?php echo $comments;?></td>
		<td rowspan="3">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-6_15.gif" width="109" height="379" alt=""></td>
	</tr>
	<tr>
		<td rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-6_16.gif" width="63" height="192" alt=""></td>
		<td width="573" height="53">
			<?php echo $coach;?></td>
		<td colspan="2" rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-6_18.gif" width="103" height="192" alt=""></td>
		<td colspan="3" width="281" height="53">
			<?php echo $report_date;?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-6_20.gif" width="573" height="139" alt=""></td>
		<td colspan="3">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/watch-me-play-beginner-6_21.gif" width="281" height="139" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/spacer.gif" width="111" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/spacer.gif" width="63" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/spacer.gif" width="573" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/spacer.gif" width="65" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/spacer.gif" width="38" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/spacer.gif" width="66" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/spacer.gif" width="37" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/spacer.gif" width="178" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/beginner/images/spacer.gif" width="109" height="1" alt=""></td>
	</tr>
</table>
</body>
</html>