<?php
include $_SERVER['DOCUMENT_ROOT'].'/reports/reports-data.php'; ?>
<head>
  <title>Intermediate - Watch Me Play</title>
  <?php include $_SERVER['DOCUMENT_ROOT'].'/reports/reports-header.php'; ?>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table id="Table_01" width="1240" height="1755" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="6">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-1_01.gif" width="1240" height="329" alt=""></td>
	</tr>
	<tr>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-1_02.gif" width="311" height="1425" alt=""></td>
		<td colspan="3" width="546" height="47">
			<?php echo $grade;?></td>
		<td colspan="2" rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-1_04.gif" width="383" height="92" alt=""></td>
	</tr>
	<tr>
		<td colspan="3">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-1_05.gif" width="546" height="45" alt=""></td>
	</tr>
	<tr>
		<td rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-1_06.gif" width="212" height="1333" alt=""></td>
		<td colspan="3" width="565" height="62">
			<?php echo $childs_name;?></td>
		<td rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-1_08.gif" width="152" height="1333" alt=""></td>
	</tr>
	<tr>
		<td colspan="3">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-1_09.gif" width="565" height="24" alt=""></td>
	</tr>
	<tr>
		<td rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-1_10.gif" width="57" height="1247" alt=""></td>
		<td colspan="2" width="508" height="55">
			<?php echo $venue;?></td>
	</tr>
	<tr>
		<td colspan="2">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-1_12.gif" width="508" height="1192" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/spacer.gif" width="311" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/spacer.gif" width="212" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/spacer.gif" width="57" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/spacer.gif" width="277" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/spacer.gif" width="231" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/spacer.gif" width="152" height="1" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-2_01.gif" width="1240" height="1264" alt=""></td>
	</tr>
	<tr>
		<td rowspan="16">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-2_02.gif" width="983" height="490" alt=""></td>
		<td width="29" height="30">
			<?php echo ($answers[0] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="16">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-2_04.gif" width="73" height="490" alt=""></td>
		<td width="29" height="30">
			<?php echo ($answers[0] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="16">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-2_06.gif" width="126" height="490" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-2_07.gif" width="29" height="10" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-2_08.gif" width="29" height="10" alt=""></td>
	</tr>
	<tr>
		<td width="29" height="29">
			<?php echo ($answers[1] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="29" height="29">
			<?php echo ($answers[1] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-2_11.gif" width="29" height="11" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-2_12.gif" width="29" height="11" alt=""></td>
	</tr>
	<tr>
		<td width="29" height="26">
			<?php echo ($answers[2] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="29" height="26">
			<?php echo ($answers[2] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-2_15.gif" width="29" height="17" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-2_16.gif" width="29" height="17" alt=""></td>
	</tr>
	<tr>
		<td width="29" height="27">
			<?php echo ($answers[3] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="29" height="27" >
			<?php echo ($answers[3] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-2_19.gif" width="29" height="12" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-2_20.gif" width="29" height="12" alt=""></td>
	</tr>
	<tr>
		<td width="29" height="27">
			<?php echo ($answers[4] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="29" height="27">
			<?php echo ($answers[4] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-2_23.gif" width="29" height="11" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-2_24.gif" width="29" height="11" alt=""></td>
	</tr>
	<tr>
		<td width="29" height="29">
			<?php echo ($answers[5] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="29" height="29">
			<?php echo ($answers[5] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-2_27.gif" width="29" height="35" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-2_28.gif" width="29" height="35" alt=""></td>
	</tr>
	<tr>
		<td width="29" height="29">
			<?php echo ($answers[6] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="29" height="29">
			<?php echo ($answers[6] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-2_31.gif" width="29" height="10" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-2_32.gif" width="29" height="10" alt=""></td>
	</tr>
	<tr>
		<td width="29" height="29">
			<?php echo ($answers[7] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="29" height="29">
			<?php echo ($answers[7] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-2_35.gif" width="29" height="158" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-2_36.gif" width="29" height="158" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1241" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-3_01.gif" width="1240" height="374" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/spacer.gif" width="1" height="374" alt=""></td>
	</tr>
	<tr>
		<td rowspan="20">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-3_02.gif" width="810" height="1380" alt=""></td>
		<td width="39" height="37">
			<?php echo ($answers[8] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="20">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-3_04.gif" width="65" height="1380" alt=""></td>
		<td width="41" height="37">
			<?php echo ($answers[8] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="20">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-3_06.gif" width="285" height="1380" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/spacer.gif" width="1" height="37" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-3_07.gif" width="39" height="33" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-3_08.gif" width="41" height="33" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/spacer.gif" width="1" height="33" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="38">
			<?php echo ($answers[9] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="41" height="38">
			<?php echo ($answers[9] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/spacer.gif" width="1" height="38" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-3_11.gif" width="39" height="35" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-3_12.gif" width="41" height="35" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/spacer.gif" width="1" height="35" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="35">
			<?php echo ($answers[10] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="2" width="41" height="40">
			<?php echo ($answers[10] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/spacer.gif" width="1" height="35" alt=""></td>
	</tr>
	<tr>
		<td rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-3_15.gif" width="39" height="316" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/spacer.gif" width="1" height="5" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-3_16.gif" width="41" height="311" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/spacer.gif" width="1" height="311" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="36">
			<?php echo ($answers[11] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="2" width="41" height="41">
			<?php echo ($answers[11] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/spacer.gif" width="1" height="36" alt=""></td>
	</tr>
	<tr>
		<td rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-3_19.gif" width="39" height="27" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/spacer.gif" width="1" height="5" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-3_20.gif" width="41" height="22" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/spacer.gif" width="1" height="22" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="37">
			<?php echo ($answers[12] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="41" height="37">
			<?php echo ($answers[12] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/spacer.gif" width="1" height="37" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-3_23.gif" width="39" height="24" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-3_24.gif" width="41" height="24" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/spacer.gif" width="1" height="24" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="40">
			<?php echo ($answers[13] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="41" height="40">
			<?php echo ($answers[13] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/spacer.gif" width="1" height="40" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-3_27.gif" width="39" height="365" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-3_28.gif" width="41" height="365" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/spacer.gif" width="1" height="365" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="38">
			<?php echo ($answers[14] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="41" height="38">
			<?php echo ($answers[14] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/spacer.gif" width="1" height="38" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-3_31.gif" width="39" height="29" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-3_32.gif" width="41" height="29" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/spacer.gif" width="1" height="29" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="38">
			<?php echo ($answers[15] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="41" height="38">
			<?php echo ($answers[15] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/spacer.gif" width="1" height="38" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-3_35.gif" width="39" height="31" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-3_36.gif" width="41" height="31" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/spacer.gif" width="1" height="31" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="38">
			<?php echo ($answers[16] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="41" height="38">
			<?php echo ($answers[16] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/spacer.gif" width="1" height="38" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-3_39.gif" width="39" height="183" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-3_40.gif" width="41" height="183" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/spacer.gif" width="1" height="183" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-4_01.gif" width="1240" height="248" alt=""></td>
	</tr>
	<tr>
		<td rowspan="18">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-4_02.gif" width="811" height="1506" alt=""></td>
		<td width="39" height="39">
			<?php echo ($answers[17] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="18">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-4_04.gif" width="64" height="1506" alt=""></td>
		<td width="41" height="39">
			<?php echo ($answers[17] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="18">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-4_06.gif" width="285" height="1506" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-4_07.gif" width="39" height="47" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-4_08.gif" width="41" height="47" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="38">
			<?php echo ($answers[18] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="41" height="38">
			<?php echo ($answers[18] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-4_11.gif" width="39" height="49" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-4_12.gif" width="41" height="49" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="37">
			<?php echo ($answers[19] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="41" height="37">
			<?php echo ($answers[19] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-4_15.gif" width="39" height="314" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-4_16.gif" width="41" height="314" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="38">
			<?php echo ($answers[20] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="41" height="38">
			<?php echo ($answers[20] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-4_19.gif" width="39" height="32" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-4_20.gif" width="41" height="32" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="37">
			<?php echo ($answers[21] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="41" height="37">
			<?php echo ($answers[21] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-4_23.gif" width="39" height="30" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-4_24.gif" width="41" height="30" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="38">
			<?php echo ($answers[22] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="41" height="38">
			<?php echo ($answers[22] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-4_27.gif" width="39" height="329" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-4_28.gif" width="41" height="329" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="38" >
			<?php echo ($answers[23] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="41" height="38" >
			<?php echo ($answers[23] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-4_31.gif" width="39" height="32" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-4_32.gif" width="41" height="32" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="38" >
			<?php echo ($answers[24] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="41" height="38">
			<?php echo ($answers[24] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-4_35.gif" width="39" height="32" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-4_36.gif" width="41" height="32" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="37">
			<?php echo ($answers[25] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="41" height="37" >
			<?php echo ($answers[25] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-4_39.gif" width="39" height="301" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-4_40.gif" width="41" height="301" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-5_01.gif" width="1240" height="264" alt=""></td>
	</tr>
	<tr>
		<td rowspan="14">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-5_02.gif" width="810" height="1490" alt=""></td>
		<td width="39" height="38">
			<?php echo ($answers[26] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="14">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-5_04.gif" width="65" height="1490" alt=""></td>
		<td width="40" height="38">
			<?php echo ($answers[26] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="14">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-5_06.gif" width="286" height="1490" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-5_07.gif" width="39" height="28" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-5_08.gif" width="40" height="28" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="39" >
			<?php echo ($answers[27] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="39">
			<?php echo ($answers[27] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-5_11.gif" width="39" height="28" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-5_12.gif" width="40" height="28" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="37">
			<?php echo ($answers[28] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="37">
			<?php echo ($answers[28] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-5_15.gif" width="39" height="326" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-5_16.gif" width="40" height="326" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="37" >
			<?php echo ($answers[29] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="37">
			<?php echo ($answers[29] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-5_19.gif" width="39" height="40" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-5_20.gif" width="40" height="40" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="36">
			<?php echo ($answers[30] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="36" >
			<?php echo ($answers[30] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-5_23.gif" width="39" height="29" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-5_24.gif" width="40" height="29" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="38">
			<?php echo ($answers[31] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="38">
			<?php echo ($answers[31] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-5_27.gif" width="39" height="517" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-5_28.gif" width="40" height="517" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="38">
			<?php echo ($answers[32] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="38">
			<?php echo ($answers[32] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-5_31.gif" width="39" height="259" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-5_32.gif" width="40" height="259" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1755" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="10">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-6_01.gif" width="1240" height="313" alt=""></td>
	</tr>
	<tr>
		<td colspan="4" rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-6_02.gif" width="810" height="1058" alt=""></td>
		<td colspan="2" width="40" height="39">
			<?php echo ($answers[33] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-6_04.gif" width="65" height="1058" alt=""></td>
		<td width="39" height="39">
			<?php echo ($answers[33] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td colspan="2" rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-6_06.gif" width="286" height="1058" alt=""></td>
	</tr>
	<tr>
		<td colspan="2">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-6_07.gif" width="40" height="475" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-6_08.gif" width="39" height="475" alt=""></td>
	</tr>
	<tr>
		<td colspan="2" width="40" height="37">
			<?php echo ($answers[34] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="37">
			<?php echo ($answers[34] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td colspan="2">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-6_11.gif" width="40" height="507" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-6_12.gif" width="39" height="507" alt=""></td>
	</tr>
	<tr>
		<td rowspan="3">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-6_13.gif" width="112" height="383" alt=""></td>
		<td colspan="8" width="1016" height="199">
			<?php echo $comments;?></td>
		<td rowspan="3">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-6_15.gif" width="112" height="383" alt=""></td>
	</tr>
	<tr>
		<td rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-6_16.gif" width="60" height="184" alt=""></td>
		<td width="587" height="47">
			<?php echo $coach;?></td>
		<td colspan="2" rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-6_18.gif" width="78" height="184" alt=""></td>
		<td colspan="4" width="291" height="47">
			<?php echo $report_date;?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-6_20.gif" width="587" height="137" alt=""></td>
		<td colspan="4">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/watch-me-play-intermed-6_21.gif" width="291" height="137" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/spacer.gif" width="112" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/spacer.gif" width="60" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/spacer.gif" width="587" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/spacer.gif" width="51" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/spacer.gif" width="27" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/spacer.gif" width="13" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/spacer.gif" width="65" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/spacer.gif" width="39" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/spacer.gif" width="174" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/intermediate/images/spacer.gif" width="112" height="1" alt=""></td>
	</tr>
</table>
</body>
</html>