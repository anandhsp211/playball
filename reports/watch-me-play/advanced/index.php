<?php
include $_SERVER['DOCUMENT_ROOT'].'/reports/reports-data.php'; ?>
<head>
  <title>Advanced - Watch Me Play</title>
  <?php include $_SERVER['DOCUMENT_ROOT'].'/reports/reports-header.php'; ?>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table id="Table_01" width="1240" height="1755" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="6">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-1_01.gif" width="1240" height="330" alt=""></td>
	</tr>
	<tr>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-1_02.gif" width="315" height="1424" alt=""></td>
		<td colspan="3" width="538" height="44">
			<?php echo $grade;?></td>
		<td colspan="2" rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-1_04.gif" width="387" height="75" alt=""></td>
	</tr>
	<tr>
		<td colspan="3">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-1_05.gif" width="538" height="31" alt=""></td>
	</tr>
	<tr>
		<td rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-1_06.gif" width="212" height="1349" alt=""></td>
		<td colspan="3" width="564" height="75">
			<?php echo $childs_name;?></td>
		<td rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-1_08.gif" width="149" height="1349" alt=""></td>
	</tr>
	<tr>
		<td colspan="3">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-1_09.gif" width="564" height="19" alt=""></td>
	</tr>
	<tr>
		<td rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-1_10.gif" width="52" height="1255" alt=""></td>
		<td colspan="2" width="512" height="65">
			<?php echo $venue;?></td>
	</tr>
	<tr>
		<td colspan="2">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-1_12.gif" width="512" height="1190" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/spacer.gif" width="315" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/spacer.gif" width="212" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/spacer.gif" width="52" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/spacer.gif" width="274" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/spacer.gif" width="238" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/spacer.gif" width="149" height="1" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-2_01.gif" width="1240" height="1265" alt=""></td>
	</tr>
	<tr>
		<td rowspan="16">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-2_02.gif" width="983" height="489" alt=""></td>
		<td width="29" height="29">
			<?php echo ($answers[0] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="16">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-2_04.gif" width="74" height="489" alt=""></td>
		<td width="27" height="29">
			<?php echo ($answers[0] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="16">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-2_06.gif" width="127" height="489" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-2_07.gif" width="29" height="11" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-2_08.gif" width="27" height="11" alt=""></td>
	</tr>
	<tr>
		<td width="29" height="28">
			<?php echo ($answers[1] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="27" height="28">
			<?php echo ($answers[1] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-2_11.gif" width="29" height="9" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-2_12.gif" width="27" height="9" alt=""></td>
	</tr>
	<tr>
		<td width="29" height="30">
			<?php echo ($answers[2] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="27" height="30">
			<?php echo ($answers[2] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-2_15.gif" width="29" height="15" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-2_16.gif" width="27" height="15" alt=""></td>
	</tr>
	<tr>
		<td width="29" height="26">
			<?php echo ($answers[3] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="27" height="26">
			<?php echo ($answers[3] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-2_19.gif" width="29" height="13" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-2_20.gif" width="27" height="13" alt=""></td>
	</tr>
	<tr>
		<td width="29" height="27">
			<?php echo ($answers[4] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="27" height="27">
			<?php echo ($answers[4] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-2_23.gif" width="29" height="12" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-2_24.gif" width="27" height="12" alt=""></td>
	</tr>
	<tr>
		<td width="29" height="28">
			<?php echo ($answers[5] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="27" height="28">
			<?php echo ($answers[5] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-2_27.gif" width="29" height="37" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-2_28.gif" width="27" height="37" alt=""></td>
	</tr>
	<tr>
		<td width="29" height="25">
			<?php echo ($answers[6] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="27" height="25">
			<?php echo ($answers[6] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-2_31.gif" width="29" height="14" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-2_32.gif" width="27" height="14" alt=""></td>
	</tr>
	<tr>
		<td width="29" height="26">
			<?php echo ($answers[7] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="27" height="26">
			<?php echo ($answers[7] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-2_35.gif" width="29" height="159" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-2_36.gif" width="27" height="159" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-3_01.gif" width="1240" height="362" alt=""></td>
	</tr>
	<tr>
		<td rowspan="18">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-3_02.gif" width="810" height="1392" alt=""></td>
		<td width="39" height="37">
			<?php echo ($answers[8] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="18">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-3_04.gif" width="64" height="1392" alt=""></td>
		<td width="42" height="37">
			<?php echo ($answers[8] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="18">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-3_06.gif" width="285" height="1392" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-3_07.gif" width="39" height="27" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-3_08.gif" width="42" height="27" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="40">
			<?php echo ($answers[9] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="42" height="40">
			<?php echo ($answers[9] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-3_11.gif" width="39" height="28" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-3_12.gif" width="42" height="28" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="39">
			<?php echo ($answers[10] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="42" height="39">
			<?php echo ($answers[10] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-3_15.gif" width="39" height="319" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-3_16.gif" width="42" height="319" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="41">
			<?php echo ($answers[11] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="42" height="41" >
			<?php echo ($answers[11] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-3_19.gif" width="39" height="26" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-3_20.gif" width="42" height="26" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="40">
			<?php echo ($answers[12] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="42" height="40">
			<?php echo ($answers[12] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-3_23.gif" width="39" height="26" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-3_24.gif" width="42" height="26" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="40">
			<?php echo ($answers[13] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="42" height="40">
			<?php echo ($answers[13] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-3_27.gif" width="39" height="351" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-3_28.gif" width="42" height="351" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="38">
			<?php echo ($answers[14] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="42" height="38">
			<?php echo ($answers[14] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-3_31.gif" width="39" height="43" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-3_32.gif" width="42" height="43" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="39">
			<?php echo ($answers[15] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="42" height="39">
			<?php echo ($answers[15] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-3_35.gif" width="39" height="27" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-3_36.gif" width="42" height="27" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="41">
			<?php echo ($answers[16] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="42" height="41">
			<?php echo ($answers[16] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-3_39.gif" width="39" height="190" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-3_40.gif" width="42" height="190" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-4_01.gif" width="1240" height="281" alt=""></td>
	</tr>
	<tr>
		<td rowspan="18">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-4_02.gif" width="810" height="1473" alt=""></td>
		<td width="40" height="37">
			<?php echo ($answers[17] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="18">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-4_04.gif" width="64" height="1473" alt=""></td>
		<td width="41" height="37">
			<?php echo ($answers[17] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="18">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-4_06.gif" width="285" height="1473" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-4_07.gif" width="40" height="18" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-4_08.gif" width="41" height="18" alt=""></td>
	</tr>
	<tr>
		<td width="40" height="38">
			<?php echo ($answers[18] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="41" height="38">
			<?php echo ($answers[18] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-4_11.gif" width="40" height="18" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-4_12.gif" width="41" height="18" alt=""></td>
	</tr>
	<tr>
		<td width="40" height="39">
			<?php echo ($answers[19] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="41" height="39">
			<?php echo ($answers[19] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-4_15.gif" width="40" height="328" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-4_16.gif" width="41" height="328" alt=""></td>
	</tr>
	<tr>
		<td width="40" height="38">
			<?php echo ($answers[20] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="41" height="38">
			<?php echo ($answers[20] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-4_19.gif" width="40" height="48" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-4_20.gif" width="41" height="48" alt=""></td>
	</tr>
	<tr>
		<td width="40" height="38">
			<?php echo ($answers[21] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="41" height="38">
			<?php echo ($answers[21] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-4_23.gif" width="40" height="40" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-4_24.gif" width="41" height="40" alt=""></td>
	</tr>
	<tr>
		<td width="40" height="41">
			<?php echo ($answers[22] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="41" height="41">
			<?php echo ($answers[22] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-4_27.gif" width="40" height="329" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-4_28.gif" width="41" height="329" alt=""></td>
	</tr>
	<tr>
		<td width="40" height="40">
			<?php echo ($answers[23] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="41" height="40">
			<?php echo ($answers[23] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-4_31.gif" width="40" height="24" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-4_32.gif" width="41" height="24" alt=""></td>
	</tr>
	<tr>
		<td width="40" height="38">
			<?php echo ($answers[24] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="41" height="38">
			<?php echo ($answers[24] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-4_35.gif" width="40" height="25" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-4_36.gif" width="41" height="25" alt=""></td>
	</tr>
	<tr>
		<td width="40" height="38">
			<?php echo ($answers[25] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="41" height="38">
			<?php echo ($answers[25] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-4_39.gif" width="40" height="296" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-4_40.gif" width="41" height="296" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-5_01.gif" width="1240" height="243" alt=""></td>
	</tr>
	<tr>
		<td rowspan="14">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-5_02.gif" width="810" height="1511" alt=""></td>
		<td width="39" height="40">
			<?php echo ($answers[26] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="14">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-5_04.gif" width="64" height="1511" alt=""></td>
		<td width="42" height="40">
			<?php echo ($answers[26] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="14">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-5_06.gif" width="285" height="1511" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-5_07.gif" width="39" height="48" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-5_08.gif" width="42" height="48" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="40">
			<?php echo ($answers[27] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="42" height="40">
			<?php echo ($answers[27] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-5_11.gif" width="39" height="28" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-5_12.gif" width="42" height="28" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="40">
			<?php echo ($answers[28] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="42" height="40">
			<?php echo ($answers[28] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-5_15.gif" width="39" height="313" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-5_16.gif" width="42" height="313" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="39">
			<?php echo ($answers[29] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="42" height="39">
			<?php echo ($answers[29] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-5_19.gif" width="39" height="28" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-5_20.gif" width="42" height="28" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="38">
			<?php echo ($answers[30] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="42" height="38">
			<?php echo ($answers[30] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-5_23.gif" width="39" height="29" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-5_24.gif" width="42" height="29" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="38">
			<?php echo ($answers[31] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="42" height="38">
			<?php echo ($answers[31] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-5_27.gif" width="39" height="520" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-5_28.gif" width="42" height="520" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="40">
			<?php echo ($answers[32] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="42" height="40">
			<?php echo ($answers[32] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-5_31.gif" width="39" height="270" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-5_32.gif" width="42" height="270" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1755" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="10">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-6_01.gif" width="1240" height="336" alt=""></td>
	</tr>
	<tr>
		<td colspan="4" rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-6_02.gif" width="811" height="1036" alt=""></td>
		<td colspan="2" width="37" height="37">
			<?php echo ($answers[33] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-6_04.gif" width="67" height="1036" alt=""></td>
		<td width="38" height="37">
			<?php echo ($answers[33] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td colspan="2" rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-6_06.gif" width="287" height="1036" alt=""></td>
	</tr>
	<tr>
		<td colspan="2">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-6_07.gif" width="37" height="491" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-6_08.gif" width="38" height="491" alt=""></td>
	</tr>
	<tr>
		<td colspan="2" width="37" height="39">
			<?php echo ($answers[34] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="38" height="39">
			<?php echo ($answers[34] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td colspan="2">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-6_11.gif" width="37" height="469" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-6_12.gif" width="38" height="469" alt=""></td>
	</tr>
	<tr>
		<td rowspan="3">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-6_13.gif" width="116" height="382" alt=""></td>
		<td colspan="8" width="1019" height="207">
			<?php echo $comments;?></td>
		<td rowspan="3">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-6_15.gif" width="105" height="382" alt=""></td>
	</tr>
	<tr>
		<td rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-6_16.gif" width="60" height="175" alt=""></td>
		<td width="582" height="39">
			<?php echo $coach;?></td>
		<td colspan="2" rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-6_18.gif" width="87" height="175" alt=""></td>
		<td colspan="4" width="290" height="39">
			<?php echo $report_date;?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-6_20.gif" width="582" height="136" alt=""></td>
		<td colspan="4">
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/watch-me-play-advanced-6_21.gif" width="290" height="136" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/spacer.gif" width="116" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/spacer.gif" width="60" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/spacer.gif" width="582" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/spacer.gif" width="53" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/spacer.gif" width="34" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/spacer.gif" width="3" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/spacer.gif" width="67" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/spacer.gif" width="38" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/spacer.gif" width="182" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/watch-me-play/advanced/images/spacer.gif" width="105" height="1" alt=""></td>
	</tr>
</table>
</body>
</html>