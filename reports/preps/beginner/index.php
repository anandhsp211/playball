<?php
include $_SERVER['DOCUMENT_ROOT'].'/reports/reports-data.php'; ?>
<head>
  <title>Beginner - Preps</title>
  <?php include $_SERVER['DOCUMENT_ROOT'].'/reports/reports-header.php'; ?>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<table id="Table_01" width="1240" height="1755" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="6">
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-1_01.gif" width="1240" height="328" alt=""></td>
	</tr>
	<tr>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-1_02.gif" width="316" height="1426" alt=""></td>
		<td colspan="3" width="541" height="45">
			<?php echo $grade;?></td>
		<td colspan="2" rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-1_04.gif" width="383" height="83" alt=""></td>
	</tr>
	<tr>
		<td colspan="3">
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-1_05.gif" width="541" height="38" alt=""></td>
	</tr>
	<tr>
		<td rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-1_06.gif" width="205" height="1343" alt=""></td>
		<td colspan="3" width="570" height="71">
			<?php echo $childs_name;?></td>
		<td rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-1_08.gif" width="149" height="1343" alt=""></td>
	</tr>
	<tr>
		<td colspan="3">
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-1_09.gif" width="570" height="23" alt=""></td>
	</tr>
	<tr>
		<td rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-1_10.gif" width="57" height="1249" alt=""></td>
		<td colspan="2" width="513" height="59">
			<?php echo $venue;?></td>
	</tr>
	<tr>
		<td colspan="2">
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-1_12.gif" width="513" height="1190" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/spacer.gif" width="316" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/spacer.gif" width="205" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/spacer.gif" width="57" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/spacer.gif" width="279" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/spacer.gif" width="234" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/spacer.gif" width="149" height="1" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-2_01.gif" width="1240" height="1265" alt=""></td>
	</tr>
	<tr>
		<td rowspan="16">
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-2_02.gif" width="983" height="489" alt=""></td>
		<td width="28" height="27">
			<?php echo ($answers[0] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="16">
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-2_04.gif" width="73" height="489" alt=""></td>
		<td width="29" height="27">
			<?php echo ($answers[0] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="16">
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-2_06.gif" width="127" height="489" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-2_07.gif" width="28" height="13" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-2_08.gif" width="29" height="13" alt=""></td>
	</tr>
	<tr>
		<td width="28" height="27" >
			<?php echo ($answers[1] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="29" height="27">
			<?php echo ($answers[1] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-2_11.gif" width="28" height="12" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-2_12.gif" width="29" height="12" alt=""></td>
	</tr>
	<tr>
		<td width="28" height="27">
			<?php echo ($answers[2] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="29" height="27">
			<?php echo ($answers[2] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-2_15.gif" width="28" height="17" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-2_16.gif" width="29" height="17" alt=""></td>
	</tr>
	<tr>
		<td width="28" height="26">
			<?php echo ($answers[3] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="29" height="26">
			<?php echo ($answers[3] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-2_19.gif" width="28" height="13" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-2_20.gif" width="29" height="13" alt=""></td>
	</tr>
	<tr>
		<td width="28" height="26">
			<?php echo ($answers[4] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="29" height="26">
			<?php echo ($answers[4] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-2_23.gif" width="28" height="11" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-2_24.gif" width="29" height="11" alt=""></td>
	</tr>
	<tr>
		<td width="28" height="30">
			<?php echo ($answers[5] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="29" height="30" >
			<?php echo ($answers[5] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-2_27.gif" width="28" height="38" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-2_28.gif" width="29" height="38" alt=""></td>
	</tr>
	<tr>
		<td width="28" height="25">
			<?php echo ($answers[6] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="29" height="25">
			<?php echo ($answers[6] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-2_31.gif" width="28" height="15" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-2_32.gif" width="29" height="15" alt=""></td>
	</tr>
	<tr>
		<td width="28" height="26" >
			<?php echo ($answers[7] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="29" height="26">
			<?php echo ($answers[7] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-2_35.gif" width="28" height="156" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-2_36.gif" width="29" height="156" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-3_01.gif" width="1240" height="342" alt=""></td>
	</tr>
	<tr>
		<td rowspan="24">
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-3_02.gif" width="811" height="1412" alt=""></td>
		<td width="38" height="36">
			<?php echo ($answers[8] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="24">
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-3_04.gif" width="66" height="1412" alt=""></td>
		<td width="38" height="36">
			<?php echo ($answers[8] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="24">
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-3_06.gif" width="287" height="1412" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-3_07.gif" width="38" height="41" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-3_08.gif" width="38" height="41" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="36">
			<?php echo ($answers[9] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="38" height="36">
			<?php echo ($answers[9] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-3_11.gif" width="38" height="53" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-3_12.gif" width="38" height="53" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="36">
			<?php echo ($answers[10] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="38" height="36">
			<?php echo ($answers[10] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-3_15.gif" width="38" height="31" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-3_16.gif" width="38" height="31" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="38" >
			<?php echo ($answers[11] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="38" height="38" >
			<?php echo ($answers[11] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-3_19.gif" width="38" height="230" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-3_20.gif" width="38" height="230" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="35">
			<?php echo ($answers[12] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="38" height="35" >
			<?php echo ($answers[12] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-3_23.gif" width="38" height="36" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-3_24.gif" width="38" height="36" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="35">
			<?php echo ($answers[13] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="38" height="35">
			<?php echo ($answers[13] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-3_27.gif" width="38" height="53" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-3_28.gif" width="38" height="53" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="37">
			<?php echo ($answers[14] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="38" height="37">
			<?php echo ($answers[14] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-3_31.gif" width="38" height="27" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-3_32.gif" width="38" height="27" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="36">
			<?php echo ($answers[15] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="38" height="36">
			<?php echo ($answers[15] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-3_35.gif" width="38" height="229" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-3_36.gif" width="38" height="229" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="35">
			<?php echo ($answers[16] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="38" height="35">
			<?php echo ($answers[16] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-3_39.gif" width="38" height="27" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-3_40.gif" width="38" height="27" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="38">
			<?php echo ($answers[17] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="38" height="38">
			<?php echo ($answers[17] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-3_43.gif" width="38" height="48" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-3_44.gif" width="38" height="48" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="36">
			<?php echo ($answers[18] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="38" height="36">
			<?php echo ($answers[18] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-3_47.gif" width="38" height="32" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-3_48.gif" width="38" height="32" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="39" >
			<?php echo ($answers[19] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="38" height="39" >
			<?php echo ($answers[19] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-3_51.gif" width="38" height="168" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-3_52.gif" width="38" height="168" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-4_01.gif" width="1240" height="240" alt=""></td>
	</tr>
	<tr>
		<td rowspan="24">
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-4_02.gif" width="811" height="1514" alt=""></td>
		<td width="37" height="34" >
			<?php echo ($answers[20] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="24">
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-4_04.gif" width="69" height="1514" alt=""></td>
		<td width="36" height="34" >
			<?php echo ($answers[20] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="24">
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-4_06.gif" width="287" height="1514" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-4_07.gif" width="37" height="26" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-4_08.gif" width="36" height="26" alt=""></td>
	</tr>
	<tr>
		<td width="37" height="39">
			<?php echo ($answers[21] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="36" height="39" >
			<?php echo ($answers[21] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-4_11.gif" width="37" height="47" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-4_12.gif" width="36" height="47" alt=""></td>
	</tr>
	<tr>
		<td width="37" height="37">
			<?php echo ($answers[22] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="36" height="37">
			<?php echo ($answers[22] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-4_15.gif" width="37" height="44" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-4_16.gif" width="36" height="44" alt=""></td>
	</tr>
	<tr>
		<td width="37" height="37">
			<?php echo ($answers[23] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="36" height="37">
			<?php echo ($answers[23] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-4_19.gif" width="37" height="293" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-4_20.gif" width="36" height="293" alt=""></td>
	</tr>
	<tr>
		<td width="37" height="34">
			<?php echo ($answers[24] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="36" height="34">
			<?php echo ($answers[24] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-4_23.gif" width="37" height="41" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-4_24.gif" width="36" height="41" alt=""></td>
	</tr>
	<tr>
		<td width="37" height="36" >
			<?php echo ($answers[25] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="36" height="36" >
			<?php echo ($answers[25] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-4_27.gif" width="37" height="51" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-4_28.gif" width="36" height="51" alt=""></td>
	</tr>
	<tr>
		<td width="37" height="38" >
			<?php echo ($answers[26] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="36" height="38" >
			<?php echo ($answers[26] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-4_31.gif" width="37" height="37" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-4_32.gif" width="36" height="37" alt=""></td>
	</tr>
	<tr>
		<td width="37" height="38" >
			<?php echo ($answers[27] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="36" height="38" >
			<?php echo ($answers[27] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-4_35.gif" width="37" height="283" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-4_36.gif" width="36" height="283" alt=""></td>
	</tr>
	<tr>
		<td width="37" height="36" >
			<?php echo ($answers[28] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="36" height="36">
			<?php echo ($answers[28] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-4_39.gif" width="37" height="46" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-4_40.gif" width="36" height="46" alt=""></td>
	</tr>
	<tr>
		<td width="37" height="39">
			<?php echo ($answers[29] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="36" height="39">
			<?php echo ($answers[29] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-4_43.gif" width="37" height="50" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-4_44.gif" width="36" height="50" alt=""></td>
	</tr>
	<tr>
		<td width="37" height="35">
			<?php echo ($answers[30] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="36" height="35">
			<?php echo ($answers[30] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-4_47.gif" width="37" height="36" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-4_48.gif" width="36" height="36" alt=""></td>
	</tr>
	<tr>
		<td width="37" height="36">
			<?php echo ($answers[31] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="36" height="36" >
			<?php echo ($answers[31] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-4_51.gif" width="37" height="121" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-4_52.gif" width="36" height="121" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-5_01.gif" width="1240" height="251" alt=""></td>
	</tr>
	<tr>
		<td rowspan="18">
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-5_02.gif" width="811" height="1503" alt=""></td>
		<td width="39" height="41" >
			<?php echo ($answers[32] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="18">
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-5_04.gif" width="66" height="1503" alt=""></td>
		<td width="38" height="41">
			<?php echo ($answers[32] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="18">
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-5_06.gif" width="286" height="1503" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-5_07.gif" width="39" height="34" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-5_08.gif" width="38" height="34" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="39" >
			<?php echo ($answers[33] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="38" height="39">
			<?php echo ($answers[33] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-5_11.gif" width="39" height="43" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-5_12.gif" width="38" height="43" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="36" >
			<?php echo ($answers[34] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="38" height="36">
			<?php echo ($answers[34] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-5_15.gif" width="39" height="30" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-5_16.gif" width="38" height="30" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="39" >
			<?php echo ($answers[35] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="38" height="39" >
			<?php echo ($answers[35] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-5_19.gif" width="39" height="281" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-5_20.gif" width="38" height="281" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="39" >
			<?php echo ($answers[36] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="38" height="39" >
			<?php echo ($answers[36] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-5_23.gif" width="39" height="43" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-5_24.gif" width="38" height="43" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="39" >
			<?php echo ($answers[37] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="38" height="39" >
			<?php echo ($answers[37] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-5_27.gif" width="39" height="45" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-5_28.gif" width="38" height="45" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="36" >
			<?php echo ($answers[38] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="38" height="36" >
			<?php echo ($answers[38] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-5_31.gif" width="39" height="42" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-5_32.gif" width="38" height="42" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="35" >
			<?php echo ($answers[39] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="38" height="35">
			<?php echo ($answers[39] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-5_35.gif" width="39" height="430" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-5_36.gif" width="38" height="430" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="39">
			<?php echo ($answers[40] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="38" height="39">
			<?php echo ($answers[40] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-5_39.gif" width="39" height="212" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-5_40.gif" width="38" height="212" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1755" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="10">
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-6_01.gif" width="1240" height="275" alt=""></td>
	</tr>
	<tr>
		<td colspan="4" rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-6_02.gif" width="811" height="1092" alt=""></td>
		<td width="35" height="36">
			<?php echo ($answers[41] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td colspan="2" rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-6_04.gif" width="69" height="1092" alt=""></td>
		<td width="37" height="36" >
			<?php echo ($answers[41] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td colspan="2" rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-6_06.gif" width="288" height="1092" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-6_07.gif" width="35" height="413" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-6_08.gif" width="37" height="413" alt=""></td>
	</tr>
	<tr>
		<td width="35" height="37" >
			<?php echo ($answers[42] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="37" height="37" >
			<?php echo ($answers[42] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-6_11.gif" width="35" height="606" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-6_12.gif" width="37" height="606" alt=""></td>
	</tr>
	<tr>
		<td rowspan="3">
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-6_13.gif" width="111" height="387" alt=""></td>
		<td colspan="8" width="1012" height="189">
			<?php echo $comments;?></td>
		<td rowspan="3">
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-6_15.gif" width="117" height="387" alt=""></td>
	</tr>
	<tr>
		<td rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-6_16.gif" width="67" height="198" alt=""></td>
		<td width="576" height="63" >
			<?php echo $coach;?></td>
		<td colspan="3" rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-6_18.gif" width="112" height="198" alt=""></td>
		<td colspan="3" width="257" height="63" >
			<?php echo $report_date;?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-6_20.gif" width="576" height="135" alt=""></td>
		<td colspan="3">
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/preps-beginner-6_21.gif" width="257" height="135" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/spacer.gif" width="111" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/spacer.gif" width="67" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/spacer.gif" width="576" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/spacer.gif" width="57" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/spacer.gif" width="35" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/spacer.gif" width="20" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/spacer.gif" width="49" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/spacer.gif" width="37" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/spacer.gif" width="171" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/beginner/images/spacer.gif" width="117" height="1" alt=""></td>
	</tr>
</table>
</body>
</html>