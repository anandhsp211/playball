<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<?php
include $_SERVER['DOCUMENT_ROOT'].'/reports/reports-data.php'; ?>
<head>
  <title>Intermediate - Preps</title>
  <?php include $_SERVER['DOCUMENT_ROOT'].'/reports/reports-header.php'; ?>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table id="Table_01" width="1240" height="1755" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="6">
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-1_01.gif" width="1240" height="329" alt=""></td>
	</tr>
	<tr>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-1_02.gif" width="314" height="1425" alt=""></td>
		<td colspan="3" width="539" height="46">
			<?php echo $grade;?></td>
		<td colspan="2" rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-1_04.gif" width="387" height="82" alt=""></td>
	</tr>
	<tr>
		<td colspan="3">
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-1_05.gif" width="539" height="36" alt=""></td>
	</tr>
	<tr>
		<td rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-1_06.gif" width="211" height="1343" alt=""></td>
		<td colspan="3" width="566" height="71" >
			<?php echo $childs_name;?></td>
		<td rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-1_08.gif" width="149" height="1343" alt=""></td>
	</tr>
	<tr>
		<td colspan="3">
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-1_09.gif" width="566" height="15" alt=""></td>
	</tr>
	<tr>
		<td rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-1_10.gif" width="54" height="1257" alt=""></td>
		<td colspan="2" width="512" height="67">
			<?php echo $venue;?></td>
	</tr>
	<tr>
		<td colspan="2">
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-1_12.gif" width="512" height="1190" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="314" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="211" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="54" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="274" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="238" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="149" height="1" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-2_01.gif" width="1240" height="1265" alt=""></td>
	</tr>
	<tr>
		<td rowspan="16">
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-2_02.gif" width="983" height="489" alt=""></td>
		<td width="27" height="28">
			<?php echo ($answers[0] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="16">
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-2_04.gif" width="74" height="489" alt=""></td>
		<td width="29" height="28">
			<?php echo ($answers[0] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="16">
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-2_06.gif" width="127" height="489" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-2_07.gif" width="27" height="11" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-2_08.gif" width="29" height="11" alt=""></td>
	</tr>
	<tr>
		<td width="27" height="28">
			<?php echo ($answers[1] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="29" height="28" >
			<?php echo ($answers[1] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-2_11.gif" width="27" height="12" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-2_12.gif" width="29" height="12" alt=""></td>
	</tr>
	<tr>
		<td width="27" height="27" >
			<?php echo ($answers[2] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="29" height="27" >
			<?php echo ($answers[2] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-2_15.gif" width="27" height="16" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-2_16.gif" width="29" height="16" alt=""></td>
	</tr>
	<tr>
		<td width="27" height="26" >
			<?php echo ($answers[3] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="29" height="26" >
			<?php echo ($answers[3] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-2_19.gif" width="27" height="15" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-2_20.gif" width="29" height="15" alt=""></td>
	</tr>
	<tr>
		<td width="27" height="25" >
			<?php echo ($answers[4] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="29" height="25" >
			<?php echo ($answers[4] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-2_23.gif" width="27" height="13" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-2_24.gif" width="29" height="13" alt=""></td>
	</tr>
	<tr>
		<td width="27" height="25" >
			<?php echo ($answers[5] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="29" height="25">
			<?php echo ($answers[5] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-2_27.gif" width="27" height="41" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-2_28.gif" width="29" height="41" alt=""></td>
	</tr>
	<tr>
		<td width="27" height="27" >
			<?php echo ($answers[6] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="29" height="27">
			<?php echo ($answers[6] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-2_31.gif" width="27" height="13" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-2_32.gif" width="29" height="13" alt=""></td>
	</tr>
	<tr>
		<td width="27" height="26" >
			<?php echo ($answers[7] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="29" height="26" >
			<?php echo ($answers[7] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-2_35.gif" width="27" height="156" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-2_36.gif" width="29" height="156" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-3_01.gif" width="1240" height="338" alt=""></td>
	</tr>
	<tr>
		<td rowspan="24">
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-3_02.gif" width="810" height="1416" alt=""></td>
		<td width="39" height="39" >
			<?php echo ($answers[8] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="24">
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-3_04.gif" width="65" height="1416" alt=""></td>
		<td width="39" height="39" >
			<?php echo ($answers[8] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="24">
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-3_06.gif" width="287" height="1416" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-3_07.gif" width="39" height="27" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-3_08.gif" width="39" height="27" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="38" >
			<?php echo ($answers[9] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="38">
			<?php echo ($answers[9] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-3_11.gif" width="39" height="39" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-3_12.gif" width="39" height="39" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="38" >
			<?php echo ($answers[10] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="38">
			<?php echo ($answers[10] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-3_15.gif" width="39" height="35" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-3_16.gif" width="39" height="35" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="39" >
			<?php echo ($answers[11] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="39" >
			<?php echo ($answers[11] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-3_19.gif" width="39" height="255" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-3_20.gif" width="39" height="255" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="38">
			<?php echo ($answers[12] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="38">
			<?php echo ($answers[12] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-3_23.gif" width="39" height="32" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-3_24.gif" width="39" height="32" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="38">
			<?php echo ($answers[13] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="38">
			<?php echo ($answers[13] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-3_27.gif" width="39" height="40" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-3_28.gif" width="39" height="40" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="39" >
			<?php echo ($answers[14] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="39" >
			<?php echo ($answers[14] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-3_31.gif" width="39" height="26" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-3_32.gif" width="39" height="26" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="37" >
			<?php echo ($answers[15] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="37" >
			<?php echo ($answers[15] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-3_35.gif" width="39" height="248" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-3_36.gif" width="39" height="248" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="40">
			<?php echo ($answers[16] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="40">
			<?php echo ($answers[16] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-3_39.gif" width="39" height="38" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-3_40.gif" width="39" height="38" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="36" >
			<?php echo ($answers[17] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="36">
			<?php echo ($answers[17] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-3_43.gif" width="39" height="45" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-3_44.gif" width="39" height="45" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="37">
			<?php echo ($answers[18] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="37">
			<?php echo ($answers[18] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-3_47.gif" width="39" height="34" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-3_48.gif" width="39" height="34" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="38" >
			<?php echo ($answers[19] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="38" >
			<?php echo ($answers[19] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-3_51.gif" width="39" height="140" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-3_52.gif" width="39" height="140" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1241" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="6">
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-4_01.gif" width="1240" height="272" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="1" height="272" alt=""></td>
	</tr>
	<tr>
		<td rowspan="25">
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-4_02.gif" width="811" height="1482" alt=""></td>
		<td width="39" height="40" >
			<?php echo ($answers[20] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="25">
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-4_04.gif" width="64" height="1482" alt=""></td>
		<td width="39" height="40" >
			<?php echo ($answers[20] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td colspan="2" rowspan="20">
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-4_06.gif" width="287" height="1223" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="1" height="40" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-4_07.gif" width="39" height="45" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-4_08.gif" width="39" height="45" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="1" height="45" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="38" >
			<?php echo ($answers[21] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="38" >
			<?php echo ($answers[21] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="1" height="38" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-4_11.gif" width="39" height="48" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-4_12.gif" width="39" height="48" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="1" height="48" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="39" >
			<?php echo ($answers[22] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="39" >
			<?php echo ($answers[22] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="1" height="39" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-4_15.gif" width="39" height="28" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-4_16.gif" width="39" height="28" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="1" height="28" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="37" >
			<?php echo ($answers[23] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="37">
			<?php echo ($answers[23] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="1" height="37" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-4_19.gif" width="39" height="272" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-4_20.gif" width="39" height="272" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="1" height="272" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="39" >
			<?php echo ($answers[24] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="39" >
			<?php echo ($answers[24] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="1" height="39" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-4_23.gif" width="39" height="47" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-4_24.gif" width="39" height="47" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="1" height="47" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="37" >
			<?php echo ($answers[25] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="37" >
			<?php echo ($answers[25] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="1" height="37" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-4_27.gif" width="39" height="51" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-4_28.gif" width="39" height="51" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="1" height="51" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="39" >
			<?php echo ($answers[26] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="39">
			<?php echo ($answers[26] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="1" height="39" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-4_31.gif" width="39" height="38" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-4_32.gif" width="39" height="38" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="1" height="38" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="40">
			<?php echo ($answers[27] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="40">
			<?php echo ($answers[27] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="1" height="40" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-4_35.gif" width="39" height="235" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-4_36.gif" width="39" height="235" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="1" height="235" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="37">
			<?php echo ($answers[28] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="37" >
			<?php echo ($answers[28] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="1" height="37" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-4_39.gif" width="39" height="33" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-4_40.gif" width="39" height="33" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="1" height="33" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="37" >
			<?php echo ($answers[29] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="37" >
			<?php echo ($answers[29] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="1" height="37" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-4_43.gif" width="39" height="43" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-4_44.gif" width="39" height="43" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="1" height="43" alt=""></td>
	</tr>
	<tr>
		<td rowspan="2" width="39" height="39">
			<?php echo ($answers[30] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td colspan="2" width="47" height="35">
			<?php echo ($answers[30] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="5">
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-4_47.gif" width="279" height="259" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="1" height="35" alt=""></td>
	</tr>
	<tr>
		<td colspan="2" rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-4_48.gif" width="47" height="40" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="1" height="4" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-4_49.gif" width="39" height="36" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="1" height="36" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="35" >
			<?php echo ($answers[31] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="35" >
			<?php echo ($answers[31] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-4_52.gif" width="8" height="184" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="1" height="35" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-4_53.gif" width="39" height="149" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-4_54.gif" width="39" height="149" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="1" height="149" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-5_01.gif" width="1240" height="258" alt=""></td>
	</tr>
	<tr>
		<td rowspan="18">
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-5_02.gif" width="811" height="1496" alt=""></td>
		<td width="38" height="39" >
			<?php echo ($answers[32] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="18">
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-5_04.gif" width="66" height="1496" alt=""></td>
		<td width="38" height="39" >
			<?php echo ($answers[32] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="18">
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-5_06.gif" width="287" height="1496" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-5_07.gif" width="38" height="25" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-5_08.gif" width="38" height="25" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="38" >
			<?php echo ($answers[33] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="38" height="38">
			<?php echo ($answers[33] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-5_11.gif" width="38" height="42" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-5_12.gif" width="38" height="42" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="40" >
			<?php echo ($answers[34] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="38" height="40">
			<?php echo ($answers[34] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-5_15.gif" width="38" height="41" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-5_16.gif" width="38" height="41" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="38" >
			<?php echo ($answers[35] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="38" height="38" >
			<?php echo ($answers[35] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-5_19.gif" width="38" height="244" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-5_20.gif" width="38" height="244" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="37">
			<?php echo ($answers[36] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="38" height="37">
			<?php echo ($answers[36] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-5_23.gif" width="38" height="50" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-5_24.gif" width="38" height="50" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="38" >
			<?php echo ($answers[37] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="38" height="38" >
			<?php echo ($answers[37] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-5_27.gif" width="38" height="44" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-5_28.gif" width="38" height="44" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="37" >
			<?php echo ($answers[38] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="38" height="37">
			<?php echo ($answers[38] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-5_31.gif" width="38" height="40" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-5_32.gif" width="38" height="40" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="38" >
			<?php echo ($answers[39] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="38" height="38">
			<?php echo ($answers[39] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-5_35.gif" width="38" height="442" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-5_36.gif" width="38" height="442" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="39" >
			<?php echo ($answers[40] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="38" height="39" >
			<?php echo ($answers[40] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-5_39.gif" width="38" height="224" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-5_40.gif" width="38" height="224" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1755" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="10">
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-6_01.gif" width="1240" height="278" alt=""></td>
	</tr>
	<tr>
		<td colspan="4" rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-6_02.gif" width="810" height="1097" alt=""></td>
		<td width="39" height="40" >
			<?php echo ($answers[41] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td colspan="2" rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-6_04.gif" width="65" height="1097" alt=""></td>
		<td width="40" height="40" >
			<?php echo ($answers[41] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td colspan="2" rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-6_06.gif" width="286" height="1097" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-6_07.gif" width="39" height="400" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-6_08.gif" width="40" height="400" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="41">
			<?php echo ($answers[42] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="41">
			<?php echo ($answers[42] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-6_11.gif" width="39" height="616" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-6_12.gif" width="40" height="616" alt=""></td>
	</tr>
	<tr>
		<td rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-6_13.gif" width="113" height="379" alt=""></td>
		<td colspan="8" width="1013" height="174">
			<?php echo $comments;?></td>
		<td rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-6_15.gif" width="114" height="379" alt=""></td>
	</tr>
	<tr>
		<td rowspan="3">
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-6_16.gif" width="63" height="205" alt=""></td>
		<td rowspan="2" width="551" height="67" >
			<?php echo $coach;?></td>
		<td colspan="6">
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-6_18.gif" width="399" height="8" alt=""></td>
	</tr>
	<tr>
		<td colspan="3" rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-6_19.gif" width="151" height="197" alt=""></td>
		<td colspan="3" width="248" height="59" >
			<?php echo $report_date;?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-6_21.gif" width="551" height="138" alt=""></td>
		<td colspan="3">
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/preps-intermediate-6_22.gif" width="248" height="138" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="113" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="63" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="551" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="83" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="39" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="29" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="36" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="40" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="172" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/preps/intermediate/images/spacer.gif" width="114" height="1" alt=""></td>
	</tr>
</table>
</body>
</html>