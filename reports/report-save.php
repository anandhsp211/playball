<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/_globalconnect.php';

/*echo "<pre>";
print_r($_POST);
echo "</pre>";*/

// Get child id
$sql = "SELECT rv.parent_recordid AS recordid
			FROM udf_definitions udf, relations_matrix rx, relations_values rv, menusub_tabs mst
			WHERE rx.id = 364 AND rx.id = rv.matrix_id AND rx.udf_columnid = udf.hashid
			AND rv.recordid = '".mysql_real_escape_string($_POST['booking_id'])."'
			AND rx.parenttable = mst.hashid
			AND rx.registrantid = 1 ORDER BY rv.id DESC LIMIT 1";
$res = mysql_query($sql);
$row = mysql_fetch_array($res);
$child_id = $row['recordid'];

// Check if this kid has this report a report for this range already
$sql = "SELECT id FROM reports WHERE
			class_id = '".mysql_real_escape_string($_POST['class_id'])."'
			AND child_id = '".mysql_real_escape_string($child_id)."' LIMIT 1";
//echo $sql."<br>";
$res = mysql_query($sql);
$row = mysql_fetch_array($res);
$duplicate_id = $row['id'];

if ($duplicate_id > 0) {
	$sql = "DELETE FROM reports WHERE id = ".$duplicate_id;
	//echo $sql;
	mysql_query($sql);
	
	$sql = "DELETE FROM reports_answers WHERE report_id = ".$duplicate_id;
	mysql_query($sql);
}

// Add the reports main info
$sql = "INSERT INTO `reports` (`id`, `child_id`, `grade`, `booking_id`, `class_id`, `group_name`, `group_stage`, `comments`, `coach_id`, `datecreated`, `createdby`)
		VALUES (NULL,
				'".mysql_real_escape_string($child_id)."',
				'".mysql_real_escape_string($_POST['grade'])."',
				'".mysql_real_escape_string($_POST['booking_id'])."',
				'".mysql_real_escape_string($_POST['class_id'])."',
				'".mysql_real_escape_string($_POST['group'])."',
				'".mysql_real_escape_string($_POST['stage'])."',
				'".mysql_real_escape_string($_POST['comments'])."',
				'".mysql_real_escape_string($_POST['coach'])."',
				'".date("Y-m-d H:i:s")."',
				'".$_SESSION['userid']."');";
//echo $sql."<br />";
$res = mysql_query($sql);
$last_id = mysql_insert_id();

foreach ($_POST AS $key=>$value) {
	// Build the question insert string
	$check_q = explode("_", $key);
	if ($check_q[0] == "question") {
		// Start building answer string
		$sql_answers .= "('".$last_id."', '".$check_q[1]."', '".$value."'),";
	} 
}

$sql = "INSERT INTO `reports_answers` (`report_id`, `action_id`, `answer`) VALUES ".substr($sql_answers,0,-1);
//echo $sql."<br />";
mysql_query($sql);

$url = base64_decode($_POST['return']);
header("Location: /".$url."#bottom");

?>