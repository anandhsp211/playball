<?php 
include_once $_SERVER['DOCUMENT_ROOT'].'/_globalconnect.php';
if ($_POST) {
	$sql = "INSERT INTO `reports_groups` (`group_name`, `group_stage`, `group_category`, `group_action`, `action_descriptors`,`added_by`,`date_added`)
				VALUES ('".mysql_real_escape_string($_POST['group'])."',
				        '".mysql_real_escape_string($_POST['stage'])."',
						'".mysql_real_escape_string($_POST['category'])."',
						'".mysql_real_escape_string($_POST['action'])."',
						'".mysql_real_escape_string($_POST['descriptor'])."',
						'".$_SESSION['userid']."',
						'".date("Y-m-d H:I:s")."');";
	//echo $sql;
	mysql_query($sql);
	$onload = 'onload="window.parent.location = window.parent.location;self.close();return false;"';
} ?>
<html>
<head>
	<title>Add Report Element</title>
	<?php 
	if (ERES > 1024) { ?>
		<style type="text/css" media="all">@import "/css/style.css";</style>
	<?php 
	} else { ?>
		<style type="text/css" media="all">@import "/css/style_small.css";</style>
	<?php
	}?>
    <script type='text/javascript' src='/js/calendar_pop.js'></script>
    <style type="text/css" media="all">@import "/css/calendar_pop.css";</style>		
</head>

<body <?php echo $onload ?>>
<?php $getvalues = "?id=".$revfull; ?>
<script language="JavaScript">
            
function submitform() {
    var group = document.getElementById('group');
	var stage = document.getElementById('stage');
	var category = document.getElementById('category');
	var action = document.getElementById('action');
	var descriptor = document.getElementById('descriptor');
    if ((group.value.length == 0) || (stage.value.length == 0) || (category.value.length == 0) || (action.value.length == 0) || (descriptor.value.length == 0)) {
        alert("Please complete all fields");
        return false;
    }
    
    document.add_element.submit();
    
}
</script>

<form method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?><?php echo $getvalues; ?>" name="add_element" id="add_element">
	<table cellspacing="0" cellpadding="0" border="0" width="97%">
	<tr>
		<td><img src="/images/<?php echo $_SESSION['franchisedata']['logo'];?>" alt="" / align="center" border="0"></td>
		<td align="right"></td>
	</tr>
	</table>
	
	
	<table cellspacing="0" cellpadding="6" border="0" width="95%">
	<tr>
		<td height="5" colspan="5"><img src="/images/spacer.gif" height="5" width="1"></td>
	</tr>
	<tr>
		<td width="3%" rowspan="12">&nbsp;</td>
		<td colspan="2" valign="top" height="5" width="60%">
			<img src="/images/icons/24_shadow/error.png" align="left" style="padding:5px;">
			<strong><font style="font-size:120%;font-family:Trebuchet MS">Add Report Element</font></strong><br />
            <span class="note">Add a new element to a Playball report. Note that this will affect all countries and all reports from the time you add the new element.</span>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<br />
			<table border=0 width="100%">
				<tr>
					<td colspan="2" width="50%" class="ls_on">
						Group:<br />
						<?php $group_array = array("I Can Do","Watch Me Play","Dinkies","Preps"); ?>
						<select name="group" id="group" style="font-size:14px;padding:3px;width:175px">
							<option value="">Select One</option>
							<?php
							foreach ($group_array AS $group) {
								$checked = ($group == $_GET['group']) ? " selected " : "" ;
								echo '<option '.$checked.'value="'.$group.'">'.$group.'</option>';
							}
							?>
						</select>
					</td>
					<td class="ls_on">
						Stage:<br />
						<?php $stage_array = array("Beginner","Intermediate","Advanced"); ?>
						<select name="stage" id="stage" style="font-size:14px;padding:3px;width:175px">
							<option value="">Select One</option>
							<?php
							foreach ($stage_array AS $stage) {
								$checked = ($stage == $_GET['stage']) ? " selected " : "" ;
								echo '<option '.$checked.'value="'.$stage.'">'.$stage.'</option>';
							}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td colspan="2" width="50%" class="ls_on">
						Category:<br />
						<select name="category" id="category" style="font-size:14px;padding:3px;width:175px">
							<option value="">Select One</option>
							<option value="Social Skills">Social Skills</option>
							<option value="Manipulation / Object Control Skills">Manipulation / Object Control Skills</option>
							<option value="Locomotion & Stability Skills / Body Management Skills">Locomotion & Stability Skills / Body Management Skills</option>
						</select>
					</td>
					<td class="ls_on">
						Action:<br />
						<input type="text" name="action" id="action" style="font-size:14px;padding:3px;width:98%" />
						<br />E.g. Communication or Kick a big ball off a beacon
					</td>
				</tr>
				<tr>
					<td class="ls_on" colspan="3">
						Action Description:
						<textarea name="descriptor" id="descriptor" style="font-size:14px;padding:3px;width:98%;height: 100px" placeholder='E.g. Understand that there is a lead leg that should be used consistently to drive the movement'></textarea>
					</td>
				</tr>
				<tr>
					<td colspan="3" align="right"><br /><input onclick="javascript: submitform(); return false;" type="submit" value="Add Element" style="font-size:14px;padding:3px;" /></td>
				</tr>
			</table>
		</td>
	</tr>
	</table>
	</form>
</body>
</html>
