<?php 
include_once $_SERVER['DOCUMENT_ROOT'].'/_globalconnect.php';
if ($_POST) {
	$sql = "UPDATE `reports_groups` SET
				`group_name` = '".mysql_real_escape_string($_POST['group'])."',
				`group_stage` = '".mysql_real_escape_string($_POST['stage'])."',
				`group_category` = '".mysql_real_escape_string($_POST['category'])."',
				`group_action` = '".mysql_real_escape_string($_POST['action'])."',
				`action_descriptors` = '".mysql_real_escape_string($_POST['descriptor'])."',
				`added_by` = '".$_SESSION['userid']."',
				`date_added` = '".date("Y-m-d H:I:s")."'
				WHERE id = '".mysql_real_escape_string($_GET['id'])."';";
	mysql_query($sql);
	$onload = 'onload="window.parent.location = window.parent.location;self.close();return false;"';
} ?>
<html>
<head>
	<title>Add Report Element</title>
	<?php 
	if (ERES > 1024) { ?>
		<style type="text/css" media="all">@import "/css/style.css";</style>
	<?php 
	} else { ?>
		<style type="text/css" media="all">@import "/css/style_small.css";</style>
	<?php
	}?>
    <script type='text/javascript' src='/js/calendar_pop.js'></script>
    <style type="text/css" media="all">@import "/css/calendar_pop.css";</style>		
</head>

<body <?php echo $onload ?>>
<?php $getvalues = "?id=".$revfull; ?>
<script language="JavaScript">
            
function submitform() {
	var action = document.getElementById('action');
	var descriptor = document.getElementById('descriptor');
    if ((action.value.length == 0) || (descriptor.value.length == 0)) {
        alert("Please complete all fields");
        return false;
    }
    
    document.add_element.submit();
    
}
</script>

<form method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?><?php echo $getvalues; ?>" name="add_element" id="add_element">
	<table cellspacing="0" cellpadding="0" border="0" width="97%">
	<tr>
		<td><img src="/images/<?php echo $_SESSION['franchisedata']['logo'];?>" alt="" / align="center" border="0"></td>
		<td align="right"></td>
	</tr>
	</table>
	
	
	<table cellspacing="0" cellpadding="6" border="0" width="95%">
	<tr>
		<td height="5" colspan="5"><img src="/images/spacer.gif" height="5" width="1"></td>
	</tr>
	<tr>
		<td width="3%" rowspan="12">&nbsp;</td>
		<td colspan="2" valign="top" height="5" width="60%">
			<img src="/images/icons/24_shadow/error.png" align="left" style="padding:5px;">
			<strong><font style="font-size:120%;font-family:Trebuchet MS">Add Report Element</font></strong><br />
            <span class="note">Add a new element to a Playball report. Note that this will affect all countries and all reports from the time you add the new element.</span>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<br />
			<table border=0 width="100%">
				<tr>
					<td colspan="2" width="50%" class="ls_on">
						
						<?php
						
						$sql = "SELECT * FROM reports_groups WHERE id = '".$_GET['id']."' LIMIT 1;";
						$res = mysql_query($sql);
						$row = mysql_fetch_array($res);
						
						$group_array = array();
						$sqli = "SELECT rg.group_name AS group_name FROM reports_groups rg
									LEFT JOIN reports_ordering ro ON ro.group_name = rg.group_name
									GROUP BY rg.group_name
									ORDER BY ro.group_order ASC";
						$resi = mysql_query($sqli);
						$first_item = 1;
						while ($rowi = mysql_fetch_array($resi)) {
							array_push($group_array,$rowi['group_name']);
						} 
						$stages_array = array("Beginner","Intermediate","Advanced"); 
						$category_array = array("Social Skills","Manipulation / Object Control Skills","Locomotion & Stability Skills / Body Management Skills"); ?>
						
						Group:<br />
						<select name="group" id="group" style="font-size:14px;padding:3px;width:175px">
							<?php
							foreach ($group_array AS $group) {
								$selected = ($row['group_name'] == $group) ? "selected " : "" ;
								echo '<option '.$selected.'value="'.$group.'">'.$group.'</option>';
							} ?>
						</select>
					</td>
					<td class="ls_on">
						Stage:<br />
						<select name="stage" id="stage" style="font-size:14px;padding:3px;width:175px">
							<?php
							foreach ($stages_array AS $stage) {
								$selected = ($row['group_stage'] == $stage) ? "selected " : "" ;
								echo '<option '.$selected.'value="'.$stage.'">'.$stage.'</option>';
							} ?>
						</select>
					</td>
				</tr>
				<tr>
					<td colspan="2" width="50%" class="ls_on">
						Category:<br />
						<select name="category" id="category" style="font-size:14px;padding:3px;width:175px">
							<?php
							foreach ($category_array AS $category) {
								$selected = ($row['group_category'] == $category) ? "selected " : "" ;
								echo '<option '.$selected.'value="'.$category.'">'.$category.'</option>';
							} ?>
						</select>
					</td>
					<td class="ls_on">
						Action:<br />
						<input type="text" name="action" id="action" style="font-size:14px;padding:3px;width:98%" value="<?php echo $row['group_action'] ;?>" />
						<br />E.g. Communication or Kick a big ball off a beacon
					</td>
				</tr>
				<tr>
					<td class="ls_on" colspan="3">
						Action Description:
						<textarea name="descriptor" id="descriptor" style="font-size:14px;padding:3px;width:98%;height: 100px" placeholder='E.g. Understand that there is a lead leg that should be used consistently to drive the movement'><?php echo $row['action_descriptors'];?></textarea>
					</td>
				</tr>
				<tr>
					<td colspan="3" align="right"><br /><input onclick="javascript: submitform(); return false;" type="submit" value="Save Element" style="font-size:14px;padding:3px;" /></td>
				</tr>
			</table>
		</td>
	</tr>
	</table>
	<input type="hidden" name="record_id" value="<?php echo $_GET['id'];?>" />
	</form>
</body>
</html>
