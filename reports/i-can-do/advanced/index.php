<?php
include $_SERVER['DOCUMENT_ROOT'].'/reports/reports-data.php'; ?>
<head>
  <title>Advanced - I Can Do Report</title>
  <?php include $_SERVER['DOCUMENT_ROOT'].'/reports/reports-header.php'; ?>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table id="Table_01" width="1240" height="1755" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="6">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-1_01.gif" width="1240" height="327" alt=""></td>
	</tr>
	<tr>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-1_02.gif" width="315" height="1427" alt=""></td>
		<td colspan="3" width="535" height="48">
			<?php echo $grade;?></td>
		<td colspan="2" rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-1_04.gif" width="390" height="88" alt=""></td>
	</tr>
	<tr>
		<td colspan="3">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-1_05.gif" width="535" height="40" alt=""></td>
	</tr>
	<tr>
		<td rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-1_06.gif" width="214" height="1339" alt=""></td>
		<td colspan="3" width="563" height="67">
			<?php echo $childs_name;?></td>
		<td rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-1_08.gif" width="148" height="1339" alt=""></td>
	</tr>
	<tr>
		<td colspan="3">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-1_09.gif" width="563" height="18" alt=""></td>
	</tr>
	<tr>
		<td rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-1_10.gif" width="50" height="1254" alt=""></td>
		<td colspan="2" width="513" height="63">
			<?php echo $venue;?></td>
	</tr>
	<tr>
		<td colspan="2" width="513" height="1191">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-1_12.gif" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/spacer.gif" width="315" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/spacer.gif" width="214" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/spacer.gif" width="50" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/spacer.gif" width="271" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/spacer.gif" width="242" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/spacer.gif" width="148" height="1" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-2_01.gif" width="1240" height="1283" alt=""></td>
	</tr>
	<tr>
		<td rowspan="14">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-2_02.gif" width="983" height="471" alt=""></td>
		<td width="28" height="29">
			<?php echo ($answers[0] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="14">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-2_04.gif" width="73" height="471" alt=""></td>
		<td width="31" height="29">
			<?php echo ($answers[0] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="14">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-2_06.gif" width="125" height="471" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-2_07.gif" width="28" height="10" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-2_08.gif" width="31" height="10" alt=""></td>
	</tr>
	<tr>
		<td width="28" height="29">
			<?php echo ($answers[1] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="31" height="29">
			<?php echo ($answers[1] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-2_11.gif" width="28" height="34" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-2_12.gif" width="31" height="34" alt=""></td>
	</tr>
	<tr>
		<td width="28" height="29">
			<?php echo ($answers[2] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="31" height="29">
			<?php echo ($answers[2] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-2_15.gif" width="28" height="10" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-2_16.gif" width="31" height="10" alt=""></td>
	</tr>
	<tr>
		<td width="28" height="30">
			<?php echo ($answers[3] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="31" height="30">
			<?php echo ($answers[3] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-2_19.gif" width="28" height="10" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-2_20.gif" width="31" height="10" alt=""></td>
	</tr>
	<tr>
		<td width="28" height="29">
			<?php echo ($answers[4] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="31" height="29">
			<?php echo ($answers[4] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-2_23.gif" width="28" height="37" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-2_24.gif" width="31" height="37" alt=""></td>
	</tr>
	<tr>
		<td width="28" height="30">
			<?php echo ($answers[5] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="31" height="30">
			<?php echo ($answers[5] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-2_27.gif" width="28" height="9" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-2_28.gif" width="31" height="9" alt=""></td>
	</tr>
	<tr>
		<td width="28" height="31">
			<?php echo ($answers[6] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="31" height="31">
			<?php echo ($answers[6] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-2_31.gif" width="28" height="154" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-2_32.gif" width="31" height="154" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-3_01.gif" width="1240" height="428" alt=""></td>
	</tr>
	<tr>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-3_02.gif" width="810" height="1326" alt=""></td>
		<td width="41" height="39">
			<?php echo ($answers[7] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-3_04.gif" width="64" height="1326" alt=""></td>
		<td width="39" height="39">
			<?php echo ($answers[7] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-3_06.gif" width="286" height="1326" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-3_07.gif" width="41" height="472" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-3_08.gif" width="39" height="472" alt=""></td>
	</tr>
	<tr>
		<td width="41" height="37">
			<?php echo ($answers[8] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="37">
			<?php echo ($answers[8] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-3_11.gif" width="41" height="463" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-3_12.gif" width="39" height="463" alt=""></td>
	</tr>
	<tr>
		<td width="41" height="38">
			<?php echo ($answers[9] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="38">
			<?php echo ($answers[9] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-3_15.gif" width="41" height="277" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-3_16.gif" width="39" height="277" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-4_01.gif" width="1240" height="284" alt=""></td>
	</tr>
	<tr>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-4_02.gif" width="811" height="1470" alt=""></td>
		<td width="38" height="36">
			<?php echo ($answers[10] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-4_04.gif" width="66" height="1470" alt=""></td>
		<td width="38" height="36">
			<?php echo ($answers[10] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-4_06.gif" width="287" height="1470" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-4_07.gif" width="38" height="466" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-4_08.gif" width="38" height="466" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="39">
			<?php echo ($answers[11] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="38" height="39">
			<?php echo ($answers[11] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-4_11.gif" width="38" height="489" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-4_12.gif" width="38" height="489" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="38">
			<?php echo ($answers[12] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="38" height="38">
			<?php echo ($answers[12] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-4_15.gif" width="38" height="402" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-4_16.gif" width="38" height="402" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-5_01.gif" width="1240" height="318" alt=""></td>
	</tr>
	<tr>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-5_02.gif" width="812" height="1436" alt=""></td>
		<td width="37" height="41">
			<?php echo ($answers[13] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-5_04.gif" width="66" height="1436" alt=""></td>
		<td width="39" height="41">
			<?php echo ($answers[13] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-5_06.gif" width="286" height="1436" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-5_07.gif" width="37" height="476" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-5_08.gif" width="39" height="476" alt=""></td>
	</tr>
	<tr>
		<td width="37" height="38">
			<?php echo ($answers[14] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="38">
			<?php echo ($answers[14] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-5_11.gif" width="37" height="607" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-5_12.gif" width="39" height="607" alt=""></td>
	</tr>
	<tr>
		<td width="37" height="40">
			<?php echo ($answers[15] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="40">
			<?php echo ($answers[15] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-5_15.gif" width="37" height="234" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-5_16.gif" width="39" height="234" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1755" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="10">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-6_01.gif" width="1240" height="283" alt=""></td>
	</tr>
	<tr>
		<td colspan="4" rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-6_02.gif" width="810" height="1084" alt=""></td>
		<td colspan="2" width="39" height="40">
			<?php echo ($answers[16] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-6_04.gif" width="65" height="1084" alt=""></td>
		<td width="39" height="40">
			<?php echo ($answers[16] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td colspan="2" rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-6_06.gif" width="287" height="1084" alt=""></td>
	</tr>
	<tr>
		<td colspan="2">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-6_07.gif" width="39" height="369" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-6_08.gif" width="39" height="369" alt=""></td>
	</tr>
	<tr>
		<td colspan="2" width="39" height="40">
			<?php echo ($answers[17] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="40">
			<?php echo ($answers[17] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td colspan="2">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-6_11.gif" width="39" height="352" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-6_12.gif" width="39" height="352" alt=""></td>
	</tr>
	<tr>
		<td colspan="2" width="39" height="40">
			<?php echo ($answers[18] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="40">
			<?php echo ($answers[18] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td colspan="2">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-6_15.gif" width="39" height="243" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-6_16.gif" width="39" height="243" alt=""></td>
	</tr>
	<tr>
		<td rowspan="3">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-6_17.gif" width="120" height="387" alt=""></td>
		<td colspan="8" width="1017" height="206">
			<?php echo $comments;?></td>
		<td rowspan="3">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-6_19.gif" width="103" height="387" alt=""></td>
	</tr>
	<tr>
		<td rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-6_20.gif" width="52" height="181" alt=""></td>
		<td width="566" height="46">
			<?php echo $coach;?></td>
		<td colspan="2" rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-6_22.gif" width="102" height="181" alt=""></td>
		<td colspan="4" width="297" height="46">
			<?php echo $report_date;?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-6_24.gif" width="566" height="135" alt=""></td>
		<td colspan="4">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/i-can-do-advanced-6_25.gif" width="297" height="135" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/spacer.gif" width="120" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/spacer.gif" width="52" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/spacer.gif" width="566" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/spacer.gif" width="72" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/spacer.gif" width="30" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/spacer.gif" width="9" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/spacer.gif" width="65" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/spacer.gif" width="39" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/spacer.gif" width="184" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/advanced/images/spacer.gif" width="103" height="1" alt=""></td>
	</tr>
</table>
</body>
</html>