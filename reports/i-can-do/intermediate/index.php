<?php
include $_SERVER['DOCUMENT_ROOT'].'/reports/reports-data.php'; ?>
<head>
  <title>Intermediate - I Can Do Report</title>
  <?php include $_SERVER['DOCUMENT_ROOT'].'/reports/reports-header.php'; ?>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<!-- ImageReady Slices (i-can-do-intermediate-1.png) -->
<table id="Table_01" width="1240" height="1755" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="6">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-1_01.gif" width="1240" height="329" alt=""></td>
	</tr>
	<tr>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-1_02.gif" width="314" height="1425" alt=""></td>
		<td colspan="3" width="544" height="46">
			<?php echo $grade;?></td>
		<td colspan="2" rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-1_04.gif" width="382" height="86" alt=""></td>
	</tr>
	<tr>
		<td colspan="3">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-1_05.gif" width="544" height="40" alt=""></td>
	</tr>
	<tr>
		<td rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-1_06.gif" width="212" height="1339" alt=""></td>
		<td colspan="3" width="560" height="67">
			<?php echo $childs_name;?></td>
		<td rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-1_08.gif" width="154" height="1339" alt=""></td>
	</tr>
	<tr>
		<td colspan="3">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-1_09.gif" width="560" height="12" alt=""></td>
	</tr>
	<tr>
		<td rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-1_10.gif" width="55" height="1260" alt=""></td>
		<td colspan="2" width="505" height="70">
			<?php echo $venue;?></td>
	</tr>
	<tr>
		<td colspan="2">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-1_12.gif" width="505" height="1190" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="314" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="212" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="55" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="277" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="228" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="154" height="1" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1241" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-2_01.gif" width="1240" height="1281" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="1" height="1281" alt=""></td>
	</tr>
	<tr>
		<td rowspan="15">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-2_02.gif" width="985" height="473" alt=""></td>
		<td width="26" height="30">
			<?php echo ($answers[0] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="15">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-2_04.gif" width="75" height="473" alt=""></td>
		<td width="27" height="30">
			<?php echo ($answers[0] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="15">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-2_06.gif" width="127" height="473" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="1" height="30" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-2_07.gif" width="26" height="16" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-2_08.gif" width="27" height="16" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="1" height="16" alt=""></td>
	</tr>
	<tr>
		<td width="26" height="30">
			<?php echo ($answers[1] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="27" height="30">
			<?php echo ($answers[1] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="1" height="30" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-2_11.gif" width="26" height="29" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-2_12.gif" width="27" height="29" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="1" height="29" alt=""></td>
	</tr>
	<tr>
		<td rowspan="2" width="26" height="30">
			<?php echo ($answers[2] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="27" height="27">
			<?php echo ($answers[2] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="1" height="27" alt=""></td>
	</tr>
	<tr>
		<td rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-2_15.gif" width="27" height="13" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="1" height="3" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-2_16.gif" width="26" height="10" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="1" height="10" alt=""></td>
	</tr>
	<tr>
		<td width="26" height="27">
			<?php echo ($answers[3] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="27" height="27">
			<?php echo ($answers[3] == 3) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="1" height="27" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-2_19.gif" width="26" height="11" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-2_20.gif" width="27" height="11" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="1" height="11" alt=""></td>
	</tr>
	<tr>
		<td width="26" height="28">
			<?php echo ($answers[4] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="27" height="28">
			<?php echo ($answers[4] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="1" height="28" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-2_23.gif" width="26" height="28" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-2_24.gif" width="27" height="28" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="1" height="28" alt=""></td>
	</tr>
	<tr>
		<td width="26" height="28">
			<?php echo ($answers[5] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="27" height="28">
			<?php echo ($answers[5] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="1" height="28" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-2_27.gif" width="26" height="22" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-2_28.gif" width="27" height="22" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="1" height="22" alt=""></td>
	</tr>
	<tr>
		<td width="26" height="28">
			<?php echo ($answers[6] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="27" height="28">
			<?php echo ($answers[6] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="1" height="28" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-2_31.gif" width="26" height="156" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-2_32.gif" width="27" height="156" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="1" height="156" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-3_01.gif" width="1240" height="409" alt=""></td>
	</tr>
	<tr>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-3_02.gif" width="813" height="1345" alt=""></td>
		<td width="36" height="40">
			<?php echo ($answers[7] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-3_04.gif" width="67" height="1345" alt=""></td>
		<td width="38" height="40">
			<?php echo ($answers[7] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-3_06.gif" width="286" height="1345" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-3_07.gif" width="36" height="442" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-3_08.gif" width="38" height="442" alt=""></td>
	</tr>
	<tr>
		<td width="36" height="39">
			<?php echo ($answers[8] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="38" height="39">
			<?php echo ($answers[8] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-3_11.gif" width="36" height="465" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-3_12.gif" width="38" height="465" alt=""></td>
	</tr>
	<tr>
		<td width="36" height="40">
			<?php echo ($answers[9] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="38" height="40">
			<?php echo ($answers[9] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-3_15.gif" width="36" height="319" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-3_16.gif" width="38" height="319" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-4_01.gif" width="1240" height="302" alt=""></td>
	</tr>
	<tr>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-4_02.gif" width="812" height="1452" alt=""></td>
		<td width="37" height="40">
			<?php echo ($answers[10] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-4_04.gif" width="66" height="1452" alt=""></td>
		<td width="39" height="40">
			<?php echo ($answers[10] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-4_06.gif" width="286" height="1452" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-4_07.gif" width="37" height="453" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-4_08.gif" width="39" height="453" alt=""></td>
	</tr>
	<tr>
		<td width="37" height="39">
			<?php echo ($answers[11] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="39">
			<?php echo ($answers[11] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-4_11.gif" width="37" height="473" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-4_12.gif" width="39" height="473" alt=""></td>
	</tr>
	<tr>
		<td width="37" height="39">
			<?php echo ($answers[12] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="39">
			<?php echo ($answers[12] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-4_15.gif" width="37" height="408" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-4_16.gif" width="39" height="408" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-5_01.gif" width="1240" height="306" alt=""></td>
	</tr>
	<tr>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-5_02.gif" width="811" height="1448" alt=""></td>
		<td width="39" height="38">
			<?php echo ($answers[13] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-5_04.gif" width="64" height="1448" alt=""></td>
		<td width="40" height="38">
			<?php echo ($answers[13] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-5_06.gif" width="286" height="1448" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-5_07.gif" width="39" height="470" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-5_08.gif" width="40" height="470" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="40">
			<?php echo ($answers[14] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="40">
			<?php echo ($answers[14] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-5_11.gif" width="39" height="622" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-5_12.gif" width="40" height="622" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="37">
			<?php echo ($answers[15] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="37">
			<?php echo ($answers[15] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-5_15.gif" width="39" height="241" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-5_16.gif" width="40" height="241" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1241" height="1755" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="9">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-6_01.gif" width="1240" height="265" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="1" height="265" alt=""></td>
	</tr>
	<tr>
		<td colspan="4" rowspan="7">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-6_02.gif" width="810" height="1113" alt=""></td>
		<td width="40" height="39">
			<?php echo ($answers[16] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="7">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-6_04.gif" width="64" height="1113" alt=""></td>
		<td width="41" height="39">
			<?php echo ($answers[16] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td colspan="2" rowspan="7">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-6_06.gif" width="285" height="1113" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="1" height="39" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-6_07.gif" width="40" height="334" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-6_08.gif" width="41" height="334" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="1" height="334" alt=""></td>
	</tr>
	<tr>
		<td width="40" height="40">
			<?php echo ($answers[17] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="41" height="40">
			<?php echo ($answers[17] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="1" height="40" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-6_11.gif" width="40" height="348" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-6_12.gif" width="41" height="348" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="1" height="348" alt=""></td>
	</tr>
	<tr>
		<td width="40" height="38">
			<?php echo ($answers[18] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="2" width="41" height="41">
			<?php echo ($answers[18] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="1" height="38" alt=""></td>
	</tr>
	<tr>
		<td rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-6_15.gif" width="40" height="314" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="1" height="3" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-6_16.gif" width="41" height="311" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="1" height="311" alt=""></td>
	</tr>
	<tr>
		<td rowspan="3">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-6_17.gif" width="114" height="376" alt=""></td>
		<td colspan="7" width="1020" height="194">
			<?php echo $comments;?></td>
		<td rowspan="3">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-6_19.gif" width="106" height="376" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="1" height="194" alt=""></td>
	</tr>
	<tr>
		<td rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-6_20.gif" width="64" height="182" alt=""></td>
		<td width="505" height="49">
			<?php echo $coach;?></td>
		<td colspan="2" rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-6_22.gif" width="167" height="182" alt=""></td>
		<td colspan="3" width="284" height="49">
			<?php echo $report_date;?></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="1" height="49" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-6_24.gif" width="505" height="133" alt=""></td>
		<td colspan="3">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/i-can-do-intermediate-6_25.gif" width="284" height="133" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="1" height="133" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="114" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="64" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="505" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="127" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="40" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="64" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="41" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="179" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/intermediate/images/spacer.gif" width="106" height="1" alt=""></td>
		<td></td>
	</tr>
</table>
</body>
</html>