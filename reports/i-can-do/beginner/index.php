<?php
include $_SERVER['DOCUMENT_ROOT'].'/reports/reports-data.php'; ?>
<head>
  <title>Beginner - I Can Do Report</title>
  <?php include $_SERVER['DOCUMENT_ROOT'].'/reports/reports-header.php'; ?>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table id="Table_01" width="1240" height="1755" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="6">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-1_01.gif" width="1240" height="337" alt=""></td>
	</tr>
	<tr>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-1_02.gif" width="311" height="1417" alt=""></td>
		<td colspan="3" width="547" height="39">
			<?php echo $grade;?></td>
		<td colspan="2" rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-1_04.gif" width="382" height="108" alt=""></td>
	</tr>
	<tr>
		<td colspan="3">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-1_05.gif" width="547" height="69" alt=""></td>
	</tr>
	<tr>
		<td rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-1_06.gif" width="216" height="1309" alt=""></td>
		<td colspan="3" width="564" height="39">
			<?php echo $childs_name;?></td>
		<td rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-1_08.gif" width="149" height="1309" alt=""></td>
	</tr>
	<tr>
		<td colspan="3">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-1_09.gif" width="564" height="46" alt=""></td>
	</tr>
	<tr>
		<td rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-1_10.gif" width="54" height="1224" alt=""></td>
		<td colspan="2" width="510" height="35" >
			<?php echo $venue;?></td>
	</tr>
	<tr>
		<td colspan="2">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-1_12.gif" width="510" height="1189" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/spacer.gif" width="311" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/spacer.gif" width="216" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/spacer.gif" width="54" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/spacer.gif" width="277" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/spacer.gif" width="233" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/spacer.gif" width="149" height="1" alt=""></td>
	</tr>
</table>
<table id="Table_02" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-2_01.gif" width="1240" height="1263" alt=""></td>
	</tr>
	<tr>
		<td rowspan="16">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-2_02.gif" width="983" height="491" alt=""></td>
		<td width="28" height="30">
			<?php echo ($answers[0] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="16">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-2_04.gif" width="74" height="491" alt=""></td>
		<td width="28" height="30">
			<?php echo ($answers[0] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="16">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-2_06.gif" width="127" height="491" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-2_07.gif" width="28" height="10" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-2_08.gif" width="28" height="10" alt=""></td>
	</tr>
	<tr>
		<td width="28" height="29">
			<?php echo ($answers[1] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="28" height="29">
			<?php echo ($answers[1] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-2_11.gif" width="28" height="11" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-2_12.gif" width="28" height="11" alt=""></td>
	</tr>
	<tr>
		<td width="28" height="29">
			<?php echo ($answers[2] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="28" height="29">
			<?php echo ($answers[2] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-2_15.gif" width="28" height="14" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-2_16.gif" width="28" height="14" alt=""></td>
	</tr>
	<tr>
		<td width="28" height="28">
			<?php echo ($answers[3] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="28" height="28">
			<?php echo ($answers[3] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-2_19.gif" width="28" height="12" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-2_20.gif" width="28" height="12" alt=""></td>
	</tr>
	<tr>
		<td width="28" height="28">
			<?php echo ($answers[4] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="28" height="28">
			<?php echo ($answers[4] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-2_23.gif" width="28" height="10" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-2_24.gif" width="28" height="10" alt=""></td>
	</tr>
	<tr>
		<td width="28" height="28">
			<?php echo ($answers[5] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="28" height="28">
			<?php echo ($answers[5] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-2_27.gif" width="28" height="36" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-2_28.gif" width="28" height="36" alt=""></td>
	</tr>
	<tr>
		<td width="28" height="30">
			<?php echo ($answers[6] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="28" height="30">
			<?php echo ($answers[6] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-2_31.gif" width="28" height="11" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-2_32.gif" width="28" height="11" alt=""></td>
	</tr>
	<tr>
		<td width="28" height="30">
			<?php echo ($answers[7] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="28" height="30">
			<?php echo ($answers[7] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-2_35.gif" width="28" height="155" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-2_36.gif" width="28" height="155" alt=""></td>
	</tr>
</table>
<table id="Table_03" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-3_01.gif" width="1240" height="410" alt=""></td>
	</tr>
	<tr>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-3_02.gif" width="811" height="1344" alt=""></td>
		<td width="38" height="40">
			<?php echo ($answers[8] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-3_04.gif" width="67" height="1344" alt=""></td>
		<td width="38" height="40">
			<?php echo ($answers[8] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-3_06.gif" width="286" height="1344" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-3_07.gif" width="38" height="476" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-3_08.gif" width="38" height="476" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="38">
			<?php echo ($answers[9] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="38" height="38">
			<?php echo ($answers[9] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-3_11.gif" width="38" height="477" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-3_12.gif" width="38" height="477" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="41">
			<?php echo ($answers[10] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="38" height="41">
			<?php echo ($answers[10] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-3_15.gif" width="38" height="272" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-3_16.gif" width="38" height="272" alt=""></td>
	</tr>
</table>
<table id="Table_04" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-4_01.gif" width="1240" height="317" alt=""></td>
	</tr>
	<tr>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-4_02.gif" width="810" height="1437" alt=""></td>
		<td width="41" height="40">
			<?php echo ($answers[11] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-4_04.gif" width="63" height="1437" alt=""></td>
		<td width="42" height="40">
			<?php echo ($answers[11] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-4_06.gif" width="284" height="1437" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-4_07.gif" width="41" height="490" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-4_08.gif" width="42" height="490" alt=""></td>
	</tr>
	<tr>
		<td width="41" height="40">
			<?php echo ($answers[12] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="42" height="40">
			<?php echo ($answers[12] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-4_11.gif" width="41" height="468" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-4_12.gif" width="42" height="468" alt=""></td>
	</tr>
	<tr>
		<td width="41" height="42">
			<?php echo ($answers[13] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="42" height="42">
			<?php echo ($answers[13] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-4_15.gif" width="41" height="357" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-4_16.gif" width="42" height="357" alt=""></td>
	</tr>
</table>
<table id="Table_05" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-5_01.gif" width="1240" height="299" alt=""></td>
	</tr>
	<tr>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-5_02.gif" width="811" height="1455" alt=""></td>
		<td width="39" height="39">
			<?php echo ($answers[14] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-5_04.gif" width="65" height="1455" alt=""></td>
		<td width="39" height="39">
			<?php echo ($answers[14] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-5_06.gif" width="286" height="1455" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-5_07.gif" width="39" height="468" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-5_08.gif" width="39" height="468" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="40">
			<?php echo ($answers[15] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="40">
			<?php echo ($answers[15] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-5_11.gif" width="39" height="627" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-5_12.gif" width="39" height="627" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="40">
			<?php echo ($answers[16] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="40">
			<?php echo ($answers[16] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-5_15.gif" width="39" height="241" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-5_16.gif" width="39" height="241" alt=""></td>
	</tr>
</table>
<table id="Table_06" width="1240" height="1755" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="10">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-6_01.gif" width="1240" height="268" alt=""></td>
	</tr>
	<tr>
		<td colspan="4" rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-6_02.gif" width="810" height="1108" alt=""></td>
		<td colspan="2" width="40" height="42">
			<?php echo ($answers[17] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-6_04.gif" width="64" height="1108" alt=""></td>
		<td width="41" height="42">
			<?php echo ($answers[17] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td colspan="2" rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-6_06.gif" width="285" height="1108" alt=""></td>
	</tr>
	<tr>
		<td colspan="2">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-6_07.gif" width="40" height="348" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-6_08.gif" width="41" height="348" alt=""></td>
	</tr>
	<tr>
		<td colspan="2" width="40" height="39">
			<?php echo ($answers[18] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="41" height="39">
			<?php echo ($answers[18] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td colspan="2">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-6_11.gif" width="40" height="343" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-6_12.gif" width="41" height="343" alt=""></td>
	</tr>
	<tr>
		<td colspan="2" width="40" height="38">
			<?php echo ($answers[19] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="41" height="38">
			<?php echo ($answers[19] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td colspan="2">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-6_15.gif" width="40" height="298" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-6_16.gif" width="41" height="298" alt=""></td>
	</tr>
	<tr>
		<td rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-6_17.gif" width="115" height="378" alt=""></td>
		<td colspan="8"  width="1020" height="179">
			<?php echo $comments;?></td>
		<td rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-6_19.gif" width="105" height="378" alt=""></td>
	</tr>
	<tr>
		<td colspan="8">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-6_20.gif" width="1020" height="23" alt=""></td>
	</tr>
	<tr>
		<td rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-6_21.gif" width="58" height="176" alt=""></td>
		<td width="519" height="39">
			<?php echo $coach;?></td>
		<td colspan="2" rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-6_23.gif" width="143" height="176" alt=""></td>
		<td colspan="4" width="300" height="39">
			<?php echo $report_date;?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-6_25.gif" width="519" height="137" alt=""></td>
		<td colspan="4">
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/i-can-do-beginner-6_26.gif" width="300" height="137" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/spacer.gif" width="115" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/spacer.gif" width="58" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/spacer.gif" width="519" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/spacer.gif" width="118" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/spacer.gif" width="25" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/spacer.gif" width="15" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/spacer.gif" width="64" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/spacer.gif" width="41" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/spacer.gif" width="180" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/i-can-do/beginner/images/spacer.gif" width="105" height="1" alt=""></td>
	</tr>
</table>
<!-- End ImageReady Slices -->
</body>
</html>