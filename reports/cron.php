<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/_globalconnect.php';
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Mon, 26 Jul 2050 05:00:00 GMT");

// Lets send a message notification
require $_SERVER['DOCUMENT_ROOT'] . '/includes/phpmailer/PHPMailerAutoload.php';

$query = "SELECT ismultitier, hashid, label, type FROM menu_tabs WHERE isactive = 1 AND registrantid = " . RID . " AND hashid = '0AA1883C6411F7873CB83DACB17B0AFC' ORDER BY sortorder ASC LIMIT 1";

$result = mysql_query($query);

$document_details = array();

$nofication_time = '1 Week';

$email_config = "SELECT * FROM `franchise_master`.`email_server_details` WHERE `franchiseid` = 1";
$email_result = mysql_query($email_config);
$select_email_record = mysql_fetch_array($email_result);
$email_host = trim($select_email_record['host']);
$email_port = trim($select_email_record['port']);
$email_username = trim($select_email_record['username']);
$email_password = trim($select_email_record['password']);

if ($result && $email_host && $email_port && $email_username && $email_password) {
    $select_record = mysql_fetch_array($result);
    if ($select_record['hashid']) {
        $hash_id = $select_record['hashid'];
        if ($hash_id) {
            $db_query = "SELECT hashid, menu_tabhashid FROM menusub_tabs WHERE menu_tabhashid = '" . $hash_id . "' AND type = 'new' AND registrantid=" . RID;
            $selected_db_query = mysql_query($db_query);
            $select_record_res = mysql_fetch_array($selected_db_query);
            if ($select_record_res) {
                $selected_db_final = $select_record_res['hashid'];
                if ($selected_db_final) {
                    $selected_db_final = strtolower($selected_db_final);
                    $selected_mail_rec_query = "SELECT * FROM `udf_$selected_db_final` WHERE automatedupdateservice='Yes'";
                    $selected_mail_rec_query_result = mysql_query($selected_mail_rec_query);
                    $countme = 1;
                    if (count($selected_mail_rec_query_result)) {
                        while ($selected_mail_rec_final = mysql_fetch_assoc($selected_mail_rec_query_result, MYSQL_ASSOC)) {
                            if ($selected_mail_rec_final) {

                                $dept = $selected_mail_rec_final['department'];

                                $query = "SELECT * FROM `udf_33e8075e9970de0cfea955afd4644bb2` WHERE department =" . $dept;

                                $select_dept = mysql_query($query);

                                $select_dept_res = mysql_fetch_array($select_dept);

                                $to_email = $select_dept_res['primaryemail'];
                                $franchiseidentifier = $select_dept_res['franchiseidentifier'];
                                if ($franchiseidentifier) {
                                    $franchise_name = $franchiseidentifier;
                                } else {
                                    $franchise_name = 'franchise owner';
                                }

                                $document_details['notification']['addnotification'] = $selected_mail_rec_final['addnotification'];

                                $document_details['insurance']['name'] = 'Insurance';
                                $document_details['insurance']['certificate'] = $selected_mail_rec_final['insurancecertificate'];
                                $document_details['insurance']['physicalcopy'] = $selected_mail_rec_final['insurancephysicalcopy'];
                                $document_details['insurance']['companyname'] = $selected_mail_rec_final['insurancecompanyname'];
                                $document_details['insurance']['referencenumber'] = $selected_mail_rec_final['insurancereferencenumber'];
                                $document_details['insurance']['startdate'] = $selected_mail_rec_final['insurancestartdate'];
                                $document_details['insurance']['expirydate'] = $selected_mail_rec_final['insuranceexpirydate'];

                                $document_details['criminal']['name'] = 'Criminal';
                                $document_details['criminal']['criminalcheck'] = $selected_mail_rec_final['criminalcheck'];
                                $document_details['criminal']['physicalcopy'] = $selected_mail_rec_final['criminalphysicalcopy'];
                                $document_details['criminal']['referencenumber'] = $selected_mail_rec_final['criminalreferencenumber'];
                                $document_details['criminal']['startdate'] = $selected_mail_rec_final['criminalstartdate'];
                                $document_details['criminal']['expirydate'] = $selected_mail_rec_final['criminalexpirydate'];

                                $document_details['firstaid']['name'] = 'First Aid';
                                $document_details['firstaid']['certificate'] = $selected_mail_rec_final['firstaidcertificate'];
                                $document_details['firstaid']['physicalcopy'] = $selected_mail_rec_final['firstaidphysicalcopy'];
                                $document_details['firstaid']['companyname'] = $selected_mail_rec_final['firstaidcompanyname'];
                                $document_details['firstaid']['referencenumber'] = $selected_mail_rec_final['firstaidreferencenumber'];
                                $document_details['firstaid']['startdate'] = $selected_mail_rec_final['firstaidstartdate'];
                                $document_details['firstaid']['expirydate'] = $selected_mail_rec_final['firstaidexpirydate'];

                                $document_details['childprotection']['name'] = 'Child Protection';
                                $document_details['childprotection']['certificate'] = $selected_mail_rec_final['childprotectioncertificate'];
                                $document_details['childprotection']['physicalcopy'] = $selected_mail_rec_final['childphysicalcopy'];
                                $document_details['childprotection']['companyname'] = $selected_mail_rec_final['childcompanyname'];
                                $document_details['childprotection']['referencenumber'] = $selected_mail_rec_final['childreferencenumber'];
                                $document_details['childprotection']['startdate'] = $selected_mail_rec_final['childstartdate'];
                                $document_details['childprotection']['expirydate'] = $selected_mail_rec_final['childexpirydate'];

                                $document_details['debit']['name'] = 'Debit';
                                $document_details['debit']['debitorder'] = $selected_mail_rec_final['debitorder'];
                                $document_details['debit']['physicalcopy'] = $selected_mail_rec_final['debitphysicalcopy'];
                                $document_details['debit']['reference'] = $selected_mail_rec_final['debitreference'];
                                $document_details['debit']['paymentdate'] = $selected_mail_rec_final['debitpaymentdate'];

                                $document_details['franchisecontract']['name'] = 'Franchise Contract';
                                $document_details['franchisecontract']['francisecontract'] = $selected_mail_rec_final['francisecontract'];
                                $document_details['franchisecontract']['physicalcopy'] = $selected_mail_rec_final['franchisephysicalcopy'];
                                $document_details['franchisecontract']['startdate'] = $selected_mail_rec_final['franchisestartdate'];
                                $document_details['franchisecontract']['expirydate'] = $selected_mail_rec_final['franchiseexpirydate'];

                                $message = '';

                                if ($document_details['insurance']['expirydate']) {
                                    if ($nofication_time && $to_email) {
                                        $insurance_expiry_date = $document_details['insurance']['expirydate'];
                                        $status = dateTimeCheck($insurance_expiry_date, $nofication_time);
                                        $document_details['insurance']['state'] = $status;
                                        if ($status == 'expired') {
                                            $message = $document_details['insurance']['name'] . " - Reference No " . $document_details['insurance']['referencenumber'] . " is due on " . $insurance_expiry_date . "<br/>";
                                            if ($to_email) {
                                                mailSent($message, $to_email, $franchise_name, $email_host, $email_port, $email_username, $email_password);
                                            }
                                            sleep(1);
                                        }
                                    }
                                }

                                if ($document_details['criminal']['expirydate']) {
                                    if ($nofication_time && $to_email) {
                                        $criminal_expiry_date = $document_details['criminal']['expirydate'];
                                        $status = dateTimeCheck($criminal_expiry_date, $nofication_time);
                                        $document_details['criminal']['state'] = $status;
                                        if ($status == 'expired') {
                                            $message = $document_details['criminal']['name'] . " - Reference No " . $document_details['criminal']['referencenumber'] . " is due on " . $criminal_expiry_date . "<br/>";
                                            if ($to_email) {
                                                mailSent($message, $to_email, $franchise_name, $email_host, $email_port, $email_username, $email_password);
                                            }
                                            sleep(1);
                                        }
                                    }
                                }

                                if ($document_details['firstaid']['expirydate']) {
                                    if ($nofication_time && $to_email) {
                                        $firstaid_expiry_date = $document_details['firstaid']['expirydate'];
                                        $status = dateTimeCheck($firstaid_expiry_date, $nofication_time);
                                        $document_details['firstaid']['state'] = $status;
                                        if ($status == 'expired') {
                                            $message = $document_details['firstaid']['name'] . " - Reference No " . $document_details['firstaid']['referencenumber'] . " is due on " . $firstaid_expiry_date . "<br/>";
                                            if ($to_email) {
                                                mailSent($message, $to_email, $franchise_name, $email_host, $email_port, $email_username, $email_password);
                                            }
                                            sleep(1);
                                        }
                                    }
                                }

                                if ($document_details['childprotection']['expirydate']) {
                                    if ($nofication_time && $to_email) {
                                        $childprotection_expiry_date = $document_details['childprotection']['expirydate'];
                                        $status = dateTimeCheck($childprotection_expiry_date, $nofication_time);
                                        $document_details['childprotection']['state'] = $status;
                                        if ($status == 'expired') {
                                            $message = $document_details['childprotection']['name'] . " - Reference No " . $document_details['childprotection']['referencenumber'] . " is due on " . $childprotection_expiry_date . "<br/>";
                                            if ($to_email) {
                                                mailSent($message, $to_email, $franchise_name, $email_host, $email_port, $email_username, $email_password);
                                            }
                                            sleep(1);
                                        }
                                    }
                                }

                                if ($document_details['debit']['paymentdate']) {
                                    if ($nofication_time && $to_email) {
                                        $debit_payment_date = $document_details['debit']['paymentdate'];
                                        $status = dateTimeCheck($debit_payment_date, $nofication_time);
                                        $document_details['debit']['state'] = $status;
                                        if ($status == 'expired') {
                                            $message = $document_details['debit']['name'] . " - Reference No " . $document_details['debit']['reference'] . " is due on " . $childprotection_expiry_date . "<br/>";
                                            if ($to_email) {
                                                mailSent($message, $to_email, $franchise_name, $email_host, $email_port, $email_username, $email_password);
                                            }
                                            sleep(1);
                                        }
                                    }
                                }

                                if ($document_details['franchisecontract']['expirydate']) {
                                    if ($nofication_time && $to_email) {
                                        $francisecontract_expiry_date = $document_details['franchisecontract']['expirydate'];
                                        $status = dateTimeCheck($francisecontract_expiry_date, $nofication_time);
                                        $document_details['franchisecontract']['state'] = $status;
                                        if ($status == 'expired') {
                                            $message = $document_details['franchisecontract']['name'] . " - Physical Copy " . $document_details['franchisecontract']['physicalcopy'] . " is due on " . $francisecontract_expiry_date . "<br/>";
                                            if ($to_email) {
                                                mailSent($message, $to_email, $franchise_name, $email_host, $email_port, $email_username, $email_password);
                                            }
                                            sleep(1);
                                        }
                                    }
                                }
                            }
                            sleep(1);
                            $countme++;
                        }
                    }
                }
            }
        }
    }
}

function mailSent($message, $to_email, $franchise_name, $email_host, $email_port, $email_username, $email_password)
{
    if ($message) {
        $message_footer = "<br /> Thanks";
        $mail = new PHPMailer();
//        $mail->IsSMTP();                          // telling the class to use SMTP
        $mail->SMTPDebug = 0;                     // enables SMTP debug information (for testing)
        // 1 = errors and messages
        // 2 = messages only
        $mail->SMTPAuth = true;                   // enable SMTP authentication
        $mail->SMTPSecure = "tls";
        $mail->Host = $email_host;                // sets the SMTP server
        $mail->Port = $email_port;                // set the SMTP port for the GMAIL server
        $mail->IsHTML(true);
        $mail->Username = $email_username;        // SMTP account username
        $mail->Password = $email_password;        // SMTP account password

        $message = "Hi $franchise_name <br /><br />" . "Please note that your <br /> <br /> $message
        <br/>Please note this is an unmonitored email address, so please don't click reply to this email.<br/>" . $message_footer;

        $mail->addAddress($to_email);
        $mail->Subject = 'Document Expiry Notification';
        $mail->setFrom('no-reply@playballkids.com');
        $mail->addReplyTo('no-reply@playballkids.com');
        $mail->Body = $message;

        if (!$mail->Send()) {
//             echo "Mailer Error: " . $mail->ErrorInfo;
        } else if ($mail) {
            echo 'mail send';
        }
    }
}

function dateTimeCheck($givenDate, $week)
{
    $date = new DateTime($givenDate);
    $dateadd = $date->modify('-' . $week);
    $result = $dateadd->format('Y-m-d');
    if ($result) {
        $today_date = date('Y-m-d');
        // var_dump($today_date);
        // var_dump($result);
        if ($today_date == $result) {
            $status = 'expired';
        } else {
            $status = 'not_expired';
        }
        return $status;
    } else {
        return false;
    }
}

?>