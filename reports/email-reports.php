<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/_globalconnect.php';

$sql = "SELECT recordid, menutabid FROM relations_values WHERE location = '2BB232C0B13C774965EF8558F0FBD615'
			AND matrix_id = 390 AND parent_recordid = '".mysql_real_escape_string($_GET['classid'])."' AND registrantid = 1";
//echo $sql."<br /><br />";
$res = mysql_query($sql);

// Lets send a message notification
require $_SERVER['DOCUMENT_ROOT'].'/includes/phpmailer/PHPMailerAutoload.php';
while ($row = mysql_fetch_array($res)) {

	$sql_rep = "SELECT * FROM reports WHERE booking_id = '".$row['recordid']."' AND public = 'Yes'";
	//echo $sql_rep."<br />";
	$res_rep = mysql_query($sql_rep);
	$row_reports = mysql_fetch_array($res_rep);
	
	$dollar_r = base64_encode("/reports/".strtolower(str_replace(" ","-",$row_reports['group_name']))."/".strtolower($row_reports['group_stage'])."/?booking_id=".$row_reports['booking_id']."&class_id=".$row_reports['class_id']."&group=".$row_reports['group_name']."&stage=".$row_reports['group_stage']);
	
	$email_link = "https://www.playballkids.com/wkhtmltopdf/index.php?r=".$dollar_r;
	
	$sql_child = "SELECT childsname FROM udf_63538FE6EF330C13A05A3ED7E599D5F7 WHERE hashid = '".mysql_real_escape_string($row_reports['child_id'])."' AND issaved = 1 LIMIT 1";
	//echo $sql_child;
	$res_child = mysql_query($sql_child);
	$row_child = mysql_fetch_array($res_child);
	$report_link = "<a href='$email_link'>Click here to download ".$row_child['childsname']."'s report</a>";
	
	// get guardian email
	$sql_g = "SELECT `child_record` FROM multilink WHERE `parent_table` = '63538FE6EF330C13A05A3ED7E599D5F7'
				AND `parent_record` = '".mysql_real_escape_string($row_reports['child_id'])."' AND `child_table` = '45C48CCE2E2D7FBDEA1AFC51C7C6AD26';";
	//echo $sql_g;
	$res_g = mysql_query($sql_g);
	$row_g = mysql_fetch_array($res_g);
	$guardian_hashid = $row_g['child_record'];
	
	$sql_g = "SELECT `email` FROM udf_45C48CCE2E2D7FBDEA1AFC51C7C6AD26 WHERE hashid = '".mysql_real_escape_string($guardian_hashid)."' AND issaved = 1 LIMIT 1;";
	//echo $sql_g."<br />";
	$res_g = mysql_query($sql_g);
	$row_g = mysql_fetch_array($res_g);
	$guardian_email = $row_g['email'];
	
	if (($guardian_email != "") && ($row_reports['emailed'] == "")) {

		$mail = new PHPMailer();
		$mail->IsSMTP();                            // telling the class to use SMTP
		$mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
												   // 1 = errors and messages
        // 2 = messages only
		$mail->SMTPAuth   = true;                  // enable SMTP authentication
		$mail->SMTPSecure = "tls"; 
		$mail->Host       = $email_host; 			// sets the SMTP server
		$mail->Port       = $email_port;            // set the SMTP port for the GMAIL server
		$mail->IsHTML(true);
		$mail->Username   = $email_username; 		// SMTP account username
		$mail->Password   = $email_password;        // SMTP account password
		
		$message = "Dear Parent,<br /><br />
					We are pleased to notify you that your child's latest Playball report is now available to download.  You can download the report by click the link below or by logging into
					the <a href='https://www.playballkids.com/parent-zone/index.php?action=login'>Playball Parent Zone</a>.<br /><br />
					".$report_link."<br /><br />If you have any problems downloading this report or questions about it, please do not hesitate to contact your local Playball franchise.
					<br /><br />Kind regards,<br />The Playball Team<br /><br />Please note this is an unmonitored email address, so please don't click reply to this email.";
		
		$mail->addAddress($guardian_email);
		$mail->Subject = $row_child['childsname']."'s Playball Report is Ready to Download";
		$mail->setFrom('no-reply@playballkids.com');
		$mail->addReplyTo('no-reply@playballkids.com');
		$mail->Body = $message;
	
		if(!$mail->Send()) {
		  // echo "Mailer Error: " . $mail->ErrorInfo;
		} else {
		  // echo "Message has been sent";
		   $sql_e = "UPDATE `reports` SET `emailed` = '".date("Y-m-d H:i:s")."' WHERE `reports`.`id` = ".$row_reports['id'].";";
		   $res_e = mysql_query($sql_e); 
		}
	} else {
		//echo "No Email Address or already emailed<br />";
	}	
}

header("Location: /".base64_decode($_GET['r'])."&msg=emailssent#bottom");

?>