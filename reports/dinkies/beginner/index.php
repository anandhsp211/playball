<?php
include $_SERVER['DOCUMENT_ROOT'].'/reports/reports-data.php'; ?>
<head>
  <title>Beginner - Dinkies</title>
  <?php include $_SERVER['DOCUMENT_ROOT'].'/reports/reports-header.php'; ?>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table id="Table_01" width="1240" height="1755" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="6">
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-1_01.gif" width="1240" height="330" alt=""></td>
	</tr>
	<tr>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-1_02.gif" width="314" height="1424" alt=""></td>
		<td colspan="3" width="544" height="45">
			<?php echo $grade;?></td>
		<td colspan="2" rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-1_04.gif" width="382" height="85" alt=""></td>
	</tr>
	<tr>
		<td colspan="3">
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-1_05.gif" width="544" height="40" alt=""></td>
	</tr>
	<tr>
		<td rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-1_06.gif" width="212" height="1339" alt=""></td>
		<td colspan="3" width="568" height="67">
			<?php echo $childs_name;?></td>
		<td rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-1_08.gif" width="146" height="1339" alt=""></td>
	</tr>
	<tr>
		<td colspan="3">
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-1_09.gif" width="568" height="15" alt=""></td>
	</tr>
	<tr>
		<td rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-1_10.gif" width="53" height="1257" alt=""></td>
		<td colspan="2" width="515" height="65">
			<?php echo $venue;?></td>
	</tr>
	<tr>
		<td colspan="2">
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-1_12.gif" width="515" height="1192" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/spacer.gif" width="314" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/spacer.gif" width="212" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/spacer.gif" width="53" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/spacer.gif" width="279" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/spacer.gif" width="236" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/spacer.gif" width="146" height="1" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-2_01.gif" width="1240" height="1264" alt=""></td>
	</tr>
	<tr>
		<td rowspan="16">
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-2_02.gif" width="982" height="490" alt=""></td>
		<td width="29" height="29">
			<?php echo ($answers[0] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="16">
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-2_04.gif" width="74" height="490" alt=""></td>
		<td width="27" height="29">
			<?php echo ($answers[0] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="16">
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-2_06.gif" width="128" height="490" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-2_07.gif" width="29" height="10" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-2_08.gif" width="27" height="10" alt=""></td>
	</tr>
	<tr>
		<td width="29" height="30">
			<?php echo ($answers[1] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="27" height="30">
			<?php echo ($answers[1] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-2_11.gif" width="29" height="11" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-2_12.gif" width="27" height="11" alt=""></td>
	</tr>
	<tr>
		<td width="29" height="26">
			<?php echo ($answers[2] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="27" height="26">
			<?php echo ($answers[2] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-2_15.gif" width="29" height="35" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-2_16.gif" width="27" height="35" alt=""></td>
	</tr>
	<tr>
		<td width="29" height="28">
			<?php echo ($answers[3] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="27" height="28">
			<?php echo ($answers[3] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-2_19.gif" width="29" height="12" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-2_20.gif" width="27" height="12" alt=""></td>
	</tr>
	<tr>
		<td width="29" height="28">
			<?php echo ($answers[4] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="27" height="28">
			<?php echo ($answers[4] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-2_23.gif" width="29" height="38" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-2_24.gif" width="27" height="38" alt=""></td>
	</tr>
	<tr>
		<td width="29" height="27">
			<?php echo ($answers[5] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="27" height="27">
			<?php echo ($answers[5] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-2_27.gif" width="29" height="11" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-2_28.gif" width="27" height="11" alt=""></td>
	</tr>
	<tr>
		<td width="29" height="27">
			<?php echo ($answers[6] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="27" height="27">
			<?php echo ($answers[6] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-2_31.gif" width="29" height="13" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-2_32.gif" width="27" height="13" alt=""></td>
	</tr>
	<tr>
		<td width="29" height="27">
			<?php echo ($answers[7] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="27" height="27">
			<?php echo ($answers[7] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-2_35.gif" width="29" height="138" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-2_36.gif" width="27" height="138" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-3_01.gif" width="1240" height="430" alt=""></td>
	</tr>
	<tr>
		<td rowspan="12">
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-3_02.gif" width="811" height="1324" alt=""></td>
		<td width="39" height="39">
			<?php echo ($answers[8] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="12">
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-3_04.gif" width="65" height="1324" alt=""></td>
		<td width="38" height="39">
			<?php echo ($answers[8] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="12">
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-3_06.gif" width="287" height="1324" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-3_07.gif" width="39" height="54" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-3_08.gif" width="38" height="54" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="36">
			<?php echo ($answers[9] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="38" height="36">
			<?php echo ($answers[9] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-3_11.gif" width="39" height="374" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-3_12.gif" width="38" height="374" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="39">
			<?php echo ($answers[10] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="38" height="39">
			<?php echo ($answers[10] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-3_15.gif" width="39" height="55" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-3_16.gif" width="38" height="55" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="37">
			<?php echo ($answers[11] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="38" height="37">
			<?php echo ($answers[11] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-3_19.gif" width="39" height="392" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-3_20.gif" width="38" height="392" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="37">
			<?php echo ($answers[12] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="38" height="37">
			<?php echo ($answers[12] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-3_23.gif" width="39" height="52" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-3_24.gif" width="38" height="52" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="37">
			<?php echo ($answers[13] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="38" height="37">
			<?php echo ($answers[13] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-3_27.gif" width="39" height="172" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-3_28.gif" width="38" height="172" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-4_01.gif" width="1240" height="313" alt=""></td>
	</tr>
	<tr>
		<td rowspan="12">
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-4_02.gif" width="811" height="1441" alt=""></td>
		<td width="38" height="38">
			<?php echo ($answers[14] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="12">
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-4_04.gif" width="65" height="1441" alt=""></td>
		<td width="40" height="38">
			<?php echo ($answers[14] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="12">
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-4_06.gif" width="286" height="1441" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-4_07.gif" width="38" height="35" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-4_08.gif" width="40" height="35" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="38" >
			<?php echo ($answers[15] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="38">
			<?php echo ($answers[15] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-4_11.gif" width="38" height="402" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-4_12.gif" width="40" height="402" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="35">
			<?php echo ($answers[16] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="35">
			<?php echo ($answers[16] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-4_15.gif" width="38" height="77" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-4_16.gif" width="40" height="77" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="36">
			<?php echo ($answers[17] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="36">
			<?php echo ($answers[17] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-4_19.gif" width="38" height="405" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-4_20.gif" width="40" height="405" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="35">
			<?php echo ($answers[18] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="35">
			<?php echo ($answers[18] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-4_23.gif" width="38" height="52" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-4_24.gif" width="40" height="52" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="36">
			<?php echo ($answers[19] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="36">
			<?php echo ($answers[19] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-4_27.gif" width="38" height="252" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-4_28.gif" width="40" height="252" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-5_01.gif" width="1240" height="323" alt=""></td>
	</tr>
	<tr>
		<td rowspan="10">
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-5_02.gif" width="809" height="1431" alt=""></td>
		<td width="40" height="37">
			<?php echo ($answers[20] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="10">
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-5_04.gif" width="63" height="1431" alt=""></td>
		<td width="39" height="37">
			<?php echo ($answers[20] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="10">
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-5_06.gif" width="289" height="1431" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-5_07.gif" width="40" height="58" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-5_08.gif" width="39" height="58" alt=""></td>
	</tr>
	<tr>
		<td width="40" height="38">
			<?php echo ($answers[21] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="38">
			<?php echo ($answers[21] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-5_11.gif" width="40" height="428" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-5_12.gif" width="39" height="428" alt=""></td>
	</tr>
	<tr>
		<td width="40" height="38">
			<?php echo ($answers[22] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="38">
			<?php echo ($answers[22] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-5_15.gif" width="40" height="67" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-5_16.gif" width="39" height="67" alt=""></td>
	</tr>
	<tr>
		<td width="40" height="37">
			<?php echo ($answers[23] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="37">
			<?php echo ($answers[23] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-5_19.gif" width="40" height="477" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-5_20.gif" width="39" height="477" alt=""></td>
	</tr>
	<tr>
		<td width="40" height="39">
			<?php echo ($answers[24] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="39">
			<?php echo ($answers[24] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-5_23.gif" width="40" height="212" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-5_24.gif" width="39" height="212" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1755" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="9">
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-6_01.gif" width="1240" height="286" alt=""></td>
	</tr>
	<tr>
		<td colspan="4" rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-6_02.gif" width="811" height="1084" alt=""></td>
		<td width="39" height="39">
			<?php echo ($answers[25] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-6_04.gif" width="65" height="1084" alt=""></td>
		<td width="39" height="39">
			<?php echo ($answers[25] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td colspan="2" rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-6_06.gif" width="286" height="1084" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-6_07.gif" width="39" height="449" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-6_08.gif" width="39" height="449" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="39">
			<?php echo ($answers[26] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="39">
			<?php echo ($answers[26] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-6_11.gif" width="39" height="557" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-6_12.gif" width="39" height="557" alt=""></td>
	</tr>
	<tr>
		<td rowspan="3">
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-6_13.gif" width="114" height="384" alt=""></td>
		<td colspan="7" width="1021" height="194">
			<?php echo $comments;?></td>
		<td rowspan="3">
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-6_15.gif" width="105" height="384" alt=""></td>
	</tr>
	<tr>
		<td rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-6_16.gif" width="60" height="190" alt=""></td>
		<td width="592" height="52">
			<?php echo $coach;?></td>
		<td colspan="2" rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-6_18.gif" width="84" height="190" alt=""></td>
		<td colspan="3" width="285" height="52">
			<?php echo $report_date;?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-6_20.gif" width="592" height="138" alt=""></td>
		<td colspan="3">
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/dinkies-beginner-6_21.gif" width="285" height="138" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/spacer.gif" width="114" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/spacer.gif" width="60" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/spacer.gif" width="592" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/spacer.gif" width="45" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/spacer.gif" width="39" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/spacer.gif" width="65" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/spacer.gif" width="39" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/spacer.gif" width="181" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/beginner/images/spacer.gif" width="105" height="1" alt=""></td>
	</tr>
</table>
</body>
</html>