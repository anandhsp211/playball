<?php
include $_SERVER['DOCUMENT_ROOT'].'/reports/reports-data.php'; ?>
<head>
  <title>Intermediate - Dinkies</title>
  <?php include $_SERVER['DOCUMENT_ROOT'].'/reports/reports-header.php'; ?>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table id="Table_01" width="1240" height="1755" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="6">
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-1_01.gif" width="1240" height="331" alt=""></td>
	</tr>
	<tr>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-1_02.gif" width="313" height="1423" alt=""></td>
		<td colspan="3" width="548" height="43">
			<?php echo $grade;?></td>
		<td colspan="2" rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-1_04.gif" width="379" height="99" alt=""></td>
	</tr>
	<tr>
		<td colspan="3">
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-1_05.gif" width="548" height="56" alt=""></td>
	</tr>
	<tr>
		<td rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-1_06.gif" width="209" height="1324" alt=""></td>
		<td colspan="3" width="569" height="53">
			<?php echo $childs_name;?></td>
		<td rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-1_08.gif" width="149" height="1324" alt=""></td>
	</tr>
	<tr>
		<td colspan="3">
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-1_09.gif" width="569" height="19" alt=""></td>
	</tr>
	<tr>
		<td rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-1_10.gif" width="57" height="1252" alt=""></td>
		<td colspan="2" width="512" height="61">
			<?php echo $venue;?></td>
	</tr>
	<tr>
		<td colspan="2">
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-1_12.gif" width="512" height="1191" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/spacer.gif" width="313" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/spacer.gif" width="209" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/spacer.gif" width="57" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/spacer.gif" width="282" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/spacer.gif" width="230" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/spacer.gif" width="149" height="1" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-2_01.gif" width="1240" height="1265" alt=""></td>
	</tr>
	<tr>
		<td rowspan="16">
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-2_02.gif" width="984" height="489" alt=""></td>
		<td width="27" height="27">
			<?php echo ($answers[0] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="16">
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-2_04.gif" width="74" height="489" alt=""></td>
		<td width="28" height="27">
			<?php echo ($answers[0] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="16">
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-2_06.gif" width="127" height="489" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-2_07.gif" width="27" height="12" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-2_08.gif" width="28" height="12" alt=""></td>
	</tr>
	<tr>
		<td width="27" height="27">
			<?php echo ($answers[1] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="28" height="27">
			<?php echo ($answers[1] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-2_11.gif" width="27" height="13" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-2_12.gif" width="28" height="13" alt=""></td>
	</tr>
	<tr>
		<td width="27" height="27">
			<?php echo ($answers[2] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="28" height="27">
			<?php echo ($answers[2] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-2_15.gif" width="27" height="35" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-2_16.gif" width="28" height="35" alt=""></td>
	</tr>
	<tr>
		<td width="27" height="26">
			<?php echo ($answers[3] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="28" height="26">
			<?php echo ($answers[3] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-2_19.gif" width="27" height="13" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-2_20.gif" width="28" height="13" alt=""></td>
	</tr>
	<tr>
		<td width="27" height="27">
			<?php echo ($answers[4] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="28" height="27">
			<?php echo ($answers[4] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-2_23.gif" width="27" height="39" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-2_24.gif" width="28" height="39" alt=""></td>
	</tr>
	<tr>
		<td width="27" height="27">
			<?php echo ($answers[5] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="28" height="27">
			<?php echo ($answers[5] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-2_27.gif" width="27" height="12" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-2_28.gif" width="28" height="12" alt=""></td>
	</tr>
	<tr>
		<td width="27" height="27">
			<?php echo ($answers[6] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="28" height="27">
			<?php echo ($answers[6] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-2_31.gif" width="27" height="12" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-2_32.gif" width="28" height="12" alt=""></td>
	</tr>
	<tr>
		<td width="27" height="28">
			<?php echo ($answers[7] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="28" height="28">
			<?php echo ($answers[7] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-2_35.gif" width="27" height="137" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-2_36.gif" width="28" height="137" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-3_01.gif" width="1240" height="371" alt=""></td>
	</tr>
	<tr>
		<td rowspan="18">
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-3_02.gif" width="811" height="1383" alt=""></td>
		<td width="38" height="39">
			<?php echo ($answers[8] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="18">
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-3_04.gif" width="65" height="1383" alt=""></td>
		<td width="40" height="39">
			<?php echo ($answers[8] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="18">
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-3_06.gif" width="286" height="1383" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-3_07.gif" width="38" height="25" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-3_08.gif" width="40" height="25" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="39">
			<?php echo ($answers[9] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="39">
			<?php echo ($answers[9] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-3_11.gif" width="38" height="72" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-3_12.gif" width="40" height="72" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="37">
			<?php echo ($answers[10] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="37">
			<?php echo ($answers[10] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-3_15.gif" width="38" height="296" alt=""></td>
		<td width="40" height="296">
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-3_16.gif" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="37">
			<?php echo ($answers[11] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="37">
			<?php echo ($answers[11] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-3_19.gif" width="38" height="44" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-3_20.gif" width="40" height="44" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="37">
			<?php echo ($answers[12] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="37">
			<?php echo ($answers[12] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-3_23.gif" width="38" height="66" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-3_24.gif" width="40" height="66" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="38">
			<?php echo ($answers[13] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="38">
			<?php echo ($answers[13] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-3_27.gif" width="38" height="303" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-3_28.gif" width="40" height="303" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="38">
			<?php echo ($answers[14] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="38">
			<?php echo ($answers[14] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-3_31.gif" width="38" height="34" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-3_32.gif" width="40" height="34" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="36">
			<?php echo ($answers[15] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="36">
			<?php echo ($answers[15] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-3_35.gif" width="38" height="64" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-3_36.gif" width="40" height="64" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="38">
			<?php echo ($answers[16] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="38">
			<?php echo ($answers[16] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-3_39.gif" width="38" height="140" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-3_40.gif" width="40" height="140" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-4_01.gif" width="1240" height="255" alt=""></td>
	</tr>
	<tr>
		<td rowspan="18">
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-4_02.gif" width="811" height="1499" alt=""></td>
		<td width="38" height="40">
			<?php echo ($answers[17] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="18">
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-4_04.gif" width="66" height="1499" alt=""></td>
		<td width="39" height="40">
			<?php echo ($answers[17] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="18">
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-4_06.gif" width="286" height="1499" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-4_07.gif" width="38" height="51" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-4_08.gif" width="39" height="51" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="40">
			<?php echo ($answers[18] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="40">
			<?php echo ($answers[18] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-4_11.gif" width="38" height="82" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-4_12.gif" width="39" height="82" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="40">
			<?php echo ($answers[19] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="40">
			<?php echo ($answers[19] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-4_15.gif" width="38" height="275" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-4_16.gif" width="39" height="275" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="37">
			<?php echo ($answers[20] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="37">
			<?php echo ($answers[20] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-4_19.gif" width="38" height="35" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-4_20.gif" width="39" height="35" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="38">
			<?php echo ($answers[21] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="38">
			<?php echo ($answers[21] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-4_23.gif" width="38" height="83" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-4_24.gif" width="39" height="83" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="38">
			<?php echo ($answers[22] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="38">
			<?php echo ($answers[22] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-4_27.gif" width="38" height="281" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-4_28.gif" width="39" height="281" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="36">
			<?php echo ($answers[23] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="36">
			<?php echo ($answers[23] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-4_31.gif" width="38" height="54" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-4_32.gif" width="39" height="54" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="36">
			<?php echo ($answers[24] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="36">
			<?php echo ($answers[24] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-4_35.gif" width="38" height="94" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-4_36.gif" width="39" height="94" alt=""></td>
	</tr>
	<tr>
		<td width="38" height="37">
			<?php echo ($answers[25] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="39" height="37">
			<?php echo ($answers[25] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-4_39.gif" width="38" height="202" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-4_40.gif" width="39" height="202" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-5_01.gif" width="1240" height="251" alt=""></td>
	</tr>
	<tr>
		<td rowspan="14">
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-5_02.gif" width="812" height="1503" alt=""></td>
		<td width="35" height="37">
			<?php echo ($answers[26] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="14">
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-5_04.gif" width="69" height="1503" alt=""></td>
		<td width="36" height="37">
			<?php echo ($answers[26] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="14">
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-5_06.gif" width="288" height="1503" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-5_07.gif" width="35" height="48" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-5_08.gif" width="36" height="48" alt=""></td>
	</tr>
	<tr>
		<td width="35" height="36">
			<?php echo ($answers[27] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="36" height="36">
			<?php echo ($answers[27] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-5_11.gif" width="35" height="74" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-5_12.gif" width="36" height="74" alt=""></td>
	</tr>
	<tr>
		<td width="35" height="37">
			<?php echo ($answers[28] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="36" height="37">
			<?php echo ($answers[28] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-5_15.gif" width="35" height="293" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-5_16.gif" width="36" height="293" alt=""></td>
	</tr>
	<tr>
		<td width="35" height="35">
			<?php echo ($answers[29] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="36" height="35">
			<?php echo ($answers[29] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-5_19.gif" width="35" height="36" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-5_20.gif" width="36" height="36" alt=""></td>
	</tr>
	<tr>
		<td width="35" height="37">
			<?php echo ($answers[30] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="36" height="37">
			<?php echo ($answers[30] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-5_23.gif" width="35" height="89" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-5_24.gif" width="36" height="89" alt=""></td>
	</tr>
	<tr>
		<td width="35" height="34">
			<?php echo ($answers[31] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="36" height="34">
			<?php echo ($answers[31] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-5_27.gif" width="35" height="492" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-5_28.gif" width="36" height="492" alt=""></td>
	</tr>
	<tr>
		<td width="35" height="35">
			<?php echo ($answers[32] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="36" height="35">
			<?php echo ($answers[32] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-5_31.gif" width="35" height="220" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-5_32.gif" width="36" height="220" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1755" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="9">
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-6_01.gif" width="1240" height="280" alt=""></td>
	</tr>
	<tr>
		<td colspan="4" rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-6_02.gif" width="811" height="1095" alt=""></td>
		<td width="39" height="38">
			<?php echo ($answers[33] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-6_04.gif" width="64" height="1095" alt=""></td>
		<td width="40" height="38">
			<?php echo ($answers[33] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td colspan="2" rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-6_06.gif" width="286" height="1095" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-6_07.gif" width="39" height="411" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-6_08.gif" width="40" height="411" alt=""></td>
	</tr>
	<tr>
		<td width="39" height="37">
			<?php echo ($answers[34] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="37">
			<?php echo ($answers[34] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-6_11.gif" width="39" height="609" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-6_12.gif" width="40" height="609" alt=""></td>
	</tr>
	<tr>
		<td rowspan="3">
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-6_13.gif" width="116" height="379" alt=""></td>
		<td colspan="7" width="1015" height="176">
			<?php echo $comments;?></td>
		<td rowspan="3">
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-6_15.gif" width="109" height="379" alt=""></td>
	</tr>
	<tr>
		<td rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-6_16.gif" width="59" height="203" alt=""></td>
		<td width="592" height="64">
			<?php echo $coach;?></td>
		<td colspan="2" rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-6_18.gif" width="83" height="203" alt=""></td>
		<td colspan="3" width="281" height="64">
			<?php echo $report_date;?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-6_20.gif" width="592" height="139" alt=""></td>
		<td colspan="3">
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/dinkies-intermediate-6_21.gif" width="281" height="139" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/spacer.gif" width="116" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/spacer.gif" width="59" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/spacer.gif" width="592" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/spacer.gif" width="44" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/spacer.gif" width="39" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/spacer.gif" width="64" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/spacer.gif" width="40" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/spacer.gif" width="177" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/intermediate/images/spacer.gif" width="109" height="1" alt=""></td>
	</tr>
</table>
</body>
</html>