<?php
include $_SERVER['DOCUMENT_ROOT'].'/reports/reports-data.php'; ?>
<head>
  <title>Advanced - Dinkies</title>
  <?php include $_SERVER['DOCUMENT_ROOT'].'/reports/reports-header.php'; ?>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table id="Table_01" width="1240" height="1755" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="6">
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-1_01.gif" width="1240" height="329" alt=""></td>
	</tr>
	<tr>
		<td rowspan="6">
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-1_02.gif" width="316" height="1425" alt=""></td>
		<td colspan="3" width="535" height="45">
			<?php echo $grade;?></td>
		<td colspan="2" rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-1_04.gif" width="389" height="85" alt=""></td>
	</tr>
	<tr>
		<td colspan="3">
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-1_05.gif" width="535" height="40" alt=""></td>
	</tr>
	<tr>
		<td rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-1_06.gif" width="211" height="1340" alt=""></td>
		<td colspan="3" width="567" height="69">
			<?php echo $childs_name;?></td>
		<td rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-1_08.gif" width="146" height="1340" alt=""></td>
	</tr>
	<tr>
		<td colspan="3">
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-1_09.gif" width="567" height="18" alt=""></td>
	</tr>
	<tr>
		<td rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-1_10.gif" width="53" height="1253" alt=""></td>
		<td colspan="2" width="514" height="62">
			<?php echo $venue;?></td>
	</tr>
	<tr>
		<td colspan="2">
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-1_12.gif" width="514" height="1191" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/spacer.gif" width="316" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/spacer.gif" width="211" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/spacer.gif" width="53" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/spacer.gif" width="271" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/spacer.gif" width="243" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/spacer.gif" width="146" height="1" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-2_01.gif" width="1240" height="1264" alt=""></td>
	</tr>
	<tr>
		<td rowspan="16">
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-2_02.gif" width="983" height="490" alt=""></td>
		<td width="28" height="30">
			<?php echo ($answers[0] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="16">
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-2_04.gif" width="73" height="490" alt=""></td>
		<td width="29" height="30">
			<?php echo ($answers[0] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="16">
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-2_06.gif" width="127" height="490" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-2_07.gif" width="28" height="10" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-2_08.gif" width="29" height="10" alt=""></td>
	</tr>
	<tr>
		<td width="28" height="28">
			<?php echo ($answers[1] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="29" height="28">
			<?php echo ($answers[1] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-2_11.gif" width="28" height="11" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-2_12.gif" width="29" height="11" alt=""></td>
	</tr>
	<tr>
		<td width="28" height="28">
			<?php echo ($answers[2] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="29" height="28">
			<?php echo ($answers[2] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-2_15.gif" width="28" height="36" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-2_16.gif" width="29" height="36" alt=""></td>
	</tr>
	<tr>
		<td width="28" height="26">
			<?php echo ($answers[3] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="29" height="26">
			<?php echo ($answers[3] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-2_19.gif" width="28" height="11" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-2_20.gif" width="29" height="11" alt=""></td>
	</tr>
	<tr>
		<td width="28" height="29">
			<?php echo ($answers[4] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="29" height="29">
			<?php echo ($answers[4] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-2_23.gif" width="28" height="37" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-2_24.gif" width="29" height="37" alt=""></td>
	</tr>
	<tr>
		<td width="28" height="27">
			<?php echo ($answers[5] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="29" height="27">
			<?php echo ($answers[5] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-2_27.gif" width="28" height="12" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-2_28.gif" width="29" height="12" alt=""></td>
	</tr>
	<tr>
		<td width="28" height="28">
			<?php echo ($answers[6] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="29" height="28">
			<?php echo ($answers[6] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-2_31.gif" width="28" height="11" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-2_32.gif" width="29" height="11" alt=""></td>
	</tr>
	<tr>
		<td width="28" height="28">
			<?php echo ($answers[7] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="29" height="28">
			<?php echo ($answers[7] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-2_35.gif" width="28" height="138" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-2_36.gif" width="29" height="138" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-3_01.gif" width="1240" height="389" alt=""></td>
	</tr>
	<tr>
		<td rowspan="18">
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-3_02.gif" width="811" height="1365" alt=""></td>
		<td width="35" height="35">
			<?php echo ($answers[8] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="18">
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-3_04.gif" width="72" height="1365" alt=""></td>
		<td width="35" height="35">
			<?php echo ($answers[8] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="18">
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-3_06.gif" width="287" height="1365" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-3_07.gif" width="35" height="39" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-3_08.gif" width="35" height="39" alt=""></td>
	</tr>
	<tr>
		<td width="35" height="36">
			<?php echo ($answers[9] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="35" height="36">
			<?php echo ($answers[9] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-3_11.gif" width="35" height="65" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-3_12.gif" width="35" height="65" alt=""></td>
	</tr>
	<tr>
		<td width="35" height="36">
			<?php echo ($answers[10] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="35" height="36">
			<?php echo ($answers[10] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-3_15.gif" width="35" height="285" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-3_16.gif" width="35" height="285" alt=""></td>
	</tr>
	<tr>
		<td width="35" height="34">
			<?php echo ($answers[11] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="35" height="34">
			<?php echo ($answers[11] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-3_19.gif" width="35" height="46" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-3_20.gif" width="35" height="46" alt=""></td>
	</tr>
	<tr>
		<td width="35" height="33">
			<?php echo ($answers[12] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="35" height="33">
			<?php echo ($answers[12] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-3_23.gif" width="35" height="62" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-3_24.gif" width="35" height="62" alt=""></td>
	</tr>
	<tr>
		<td width="35" height="38">
			<?php echo ($answers[13] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="35" height="38">
			<?php echo ($answers[13] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-3_27.gif" width="35" height="280" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-3_28.gif" width="35" height="280" alt=""></td>
	</tr>
	<tr>
		<td width="35" height="37">
			<?php echo ($answers[14] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="35" height="37">
			<?php echo ($answers[14] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-3_31.gif" width="35" height="32" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-3_32.gif" width="35" height="32" alt=""></td>
	</tr>
	<tr>
		<td width="35" height="38">
			<?php echo ($answers[15] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="35" height="38">
			<?php echo ($answers[15] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-3_35.gif" width="35" height="65" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-3_36.gif" width="35" height="65" alt=""></td>
	</tr>
	<tr>
		<td width="35" height="37">
			<?php echo ($answers[16] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="35" height="37">
			<?php echo ($answers[16] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-3_39.gif" width="35" height="167" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-3_40.gif" width="35" height="167" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-4_01.gif" width="1240" height="279" alt=""></td>
	</tr>
	<tr>
		<td rowspan="18">
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-4_02.gif" width="812" height="1475" alt=""></td>
		<td width="37" height="37">
			<?php echo ($answers[17] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="18">
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-4_04.gif" width="65" height="1475" alt=""></td>
		<td width="40" height="37">
			<?php echo ($answers[17] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="18">
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-4_06.gif" width="286" height="1475" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-4_07.gif" width="37" height="39" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-4_08.gif" width="40" height="39" alt=""></td>
	</tr>
	<tr>
		<td width="37" height="39">
			<?php echo ($answers[18] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="39">
			<?php echo ($answers[18] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-4_11.gif" width="37" height="64" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-4_12.gif" width="40" height="64" alt=""></td>
	</tr>
	<tr>
		<td width="37" height="37">
			<?php echo ($answers[19] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="37">
			<?php echo ($answers[19] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-4_15.gif" width="37" height="302" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-4_16.gif" width="40" height="302" alt=""></td>
	</tr>
	<tr>
		<td width="37" height="35">
			<?php echo ($answers[20] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="35">
			<?php echo ($answers[20] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-4_19.gif" width="37" height="40" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-4_20.gif" width="40" height="40" alt=""></td>
	</tr>
	<tr>
		<td width="37" height="39">
			<?php echo ($answers[21] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="39">
			<?php echo ($answers[21] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-4_23.gif" width="37" height="66" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-4_24.gif" width="40" height="66" alt=""></td>
	</tr>
	<tr>
		<td width="37" height="37">
			<?php echo ($answers[22] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="37">
			<?php echo ($answers[22] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-4_27.gif" width="37" height="324" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-4_28.gif" width="40" height="324" alt=""></td>
	</tr>
	<tr>
		<td width="37" height="38">
			<?php echo ($answers[23] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="38">
			<?php echo ($answers[23] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-4_31.gif" width="37" height="53" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-4_32.gif" width="40" height="53" alt=""></td>
	</tr>
	<tr>
		<td width="37" height="35">
			<?php echo ($answers[24] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="35">
			<?php echo ($answers[24] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-4_35.gif" width="37" height="68" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-4_36.gif" width="40" height="68" alt=""></td>
	</tr>
	<tr>
		<td width="37" height="37">
			<?php echo ($answers[25] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="40" height="37">
			<?php echo ($answers[25] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-4_39.gif" width="37" height="185" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-4_40.gif" width="40" height="185" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1754" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="5">
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-5_01.gif" width="1240" height="283" alt=""></td>
	</tr>
	<tr>
		<td rowspan="14">
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-5_02.gif" width="812" height="1471" alt=""></td>
		<td width="36" height="38">
			<?php echo ($answers[26] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="14">
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-5_04.gif" width="67" height="1471" alt=""></td>
		<td width="37" height="38">
			<?php echo ($answers[26] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td rowspan="14">
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-5_06.gif" width="288" height="1471" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-5_07.gif" width="36" height="38" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-5_08.gif" width="37" height="38" alt=""></td>
	</tr>
	<tr>
		<td width="36" height="36">
			<?php echo ($answers[27] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="37" height="36">
			<?php echo ($answers[27] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-5_11.gif" width="36" height="64" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-5_12.gif" width="37" height="64" alt=""></td>
	</tr>
	<tr>
		<td width="36" height="35">
			<?php echo ($answers[28] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="37" height="35">
			<?php echo ($answers[28] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-5_15.gif" width="36" height="301" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-5_16.gif" width="37" height="301" alt=""></td>
	</tr>
	<tr>
		<td width="36" height="37">
			<?php echo ($answers[29] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="37" height="37">
			<?php echo ($answers[29] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-5_19.gif" width="36" height="51" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-5_20.gif" width="37" height="51" alt=""></td>
	</tr>
	<tr>
		<td width="36" height="36">
			<?php echo ($answers[30] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="37" height="36" >
			<?php echo ($answers[30] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-5_23.gif" width="36" height="67" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-5_24.gif" width="37" height="67" alt=""></td>
	</tr>
	<tr>
		<td width="36" height="36">
			<?php echo ($answers[31] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="37" height="36" >
			<?php echo ($answers[31] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-5_27.gif" width="36" height="451" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-5_28.gif" width="37" height="451" alt=""></td>
	</tr>
	<tr>
		<td width="36" height="36">
			<?php echo ($answers[32] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="37" height="36" >
			<?php echo ($answers[32] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-5_31.gif" width="36" height="245" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-5_32.gif" width="37" height="245" alt=""></td>
	</tr>
</table>
<table id="Table_01" width="1240" height="1755" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="10">
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-6_01.gif" width="1240" height="280" alt=""></td>
	</tr>
	<tr>
		<td colspan="4" rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-6_02.gif" width="812" height="1091" alt=""></td>
		<td width="36" height="38" >
			<?php echo ($answers[33] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td colspan="2" rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-6_04.gif" width="68" height="1091" alt=""></td>
		<td width="37" height="38">
			<?php echo ($answers[33] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td colspan="2" rowspan="4">
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-6_06.gif" width="287" height="1091" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-6_07.gif" width="36" height="411" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-6_08.gif" width="37" height="411" alt=""></td>
	</tr>
	<tr>
		<td width="36" height="39">
			<?php echo ($answers[34] == 1) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
		<td width="37" height="39">
			<?php echo ($answers[34] == 2) ? "<img src='http://backoffice.playballkids.com/reports/tick.gif' />"  : "" ; ?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-6_11.gif" width="36" height="603" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-6_12.gif" width="37" height="603" alt=""></td>
	</tr>
	<tr>
		<td rowspan="3">
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-6_13.gif" width="117" height="383" alt=""></td>
		<td colspan="8" width="1005" height="196">
			<?php echo $comments;?></td>
		<td rowspan="3">
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-6_15.gif" width="118" height="383" alt=""></td>
	</tr>
	<tr>
		<td rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-6_16.gif" width="63" height="187" alt=""></td>
		<td width="564" height="53">
			<?php echo $coach;?></td>
		<td colspan="3" rowspan="2">
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-6_18.gif" width="114" height="187" alt=""></td>
		<td colspan="3" width="264" height="53">
			<?php echo $report_date;?></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-6_20.gif" width="564" height="134" alt=""></td>
		<td colspan="3">
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/dinkies-advanced-6_21.gif" width="264" height="134" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/spacer.gif" width="117" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/spacer.gif" width="63" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/spacer.gif" width="564" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/spacer.gif" width="68" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/spacer.gif" width="36" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/spacer.gif" width="10" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/spacer.gif" width="58" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/spacer.gif" width="37" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/spacer.gif" width="169" height="1" alt=""></td>
		<td>
			<img src="http://backoffice.playballkids.com/reports/dinkies/advanced/images/spacer.gif" width="118" height="1" alt=""></td>
	</tr>
</table>
</body>
</html>