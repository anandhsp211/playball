<?php 
include_once("_globalconnect.php");
if ($_POST['process'] == "Process") {
	
	// Clear out all the old stuff
	$sql = "DELETE FROM `guardian_priority` WHERE `department` = ".mysql_real_escape_string($deptval);
	mysql_query($sql);
	
	
    $sql = "SELECT guardiansemail  
                FROM `udf_2BB232C0B13C774965EF8558F0FBD615` 
                WHERE `termtime` LIKE '".mysql_real_escape_string($_POST['term'])."' AND `year` LIKE '".mysql_real_escape_string($_POST['termyear'])."'
                AND issaved = 1
                ".$addsqldepartments."
                ";
    //echo $sql;
    $ressethashid = mysql_query($sql);
    $guardianArr = array();
    // Get a list of the guardian email addresses
	while ($row = mysql_fetch_array($ressethashid)) {
        // Blast these boys into an array
        $guardianArr[$row['guardiansemail']] = $row['guardiansemail'];
    }
    
    //Now we go through all parents, if found set to yes, else set to no
    $sql = "SELECT id, hashid, priority, email
                FROM `udf_45C48CCE2E2D7FBDEA1AFC51C7C6AD26`
                WHERE issaved = 1
                ".$addsqldepartments."
                ORDER BY email ASC
                ";
    //echo $sql;
    $ressethashid = mysql_query($sql);
    $outputArr = array();
    while ($row = mysql_fetch_array($ressethashid)) {
        // check if the email address is in the array
        if ($guardianArr[$row['email']] == $row['email']) {
            
            if ($row['priority'] == "Yes") {
                $outputArr[$row['email']] = "Y|Y";
            } else {
                $sqli = "UPDATE `udf_45C48CCE2E2D7FBDEA1AFC51C7C6AD26`
                            SET `priority` = 'Yes'
                            WHERE `udf_45C48CCE2E2D7FBDEA1AFC51C7C6AD26`.`id` = ".$row['id'].";";
                //echo $sqli."<br>";
                mysql_query($sqli);
                $outputArr[$row['email']] = "N|Y";
            }
			
			$sqli = "INSERT INTO `guardian_priority` (`department`, `guardian_id`) VALUES ('".mysql_real_escape_string($deptval)."', '".$row['hashid']."');";
			mysql_query($sqli);
            
        } else {
            
            if ($row['priority'] == "No") {
                $outputArr[$row['email']] = "N|N";
            } else {
                $sqli = "UPDATE `udf_45C48CCE2E2D7FBDEA1AFC51C7C6AD26`
                            SET `priority` = 'No'
                            WHERE `udf_45C48CCE2E2D7FBDEA1AFC51C7C6AD26`.`id` = ".$row['id'].";";
                //echo $sqli."<br>";
                mysql_query($sqli);
                $outputArr[$row['email']] = "Y|N";
            }
        }
    }
	
	//$onload = 'onload="window.parent.location = window.parent.location;self.close();return false;"';
    
}
?>
<html>
<head>
	<title>Reset Guardian Priority</title>
	<?php 
	if (ERES > 1024) { ?>
		<style type="text/css" media="all">@import "css/style.css";</style>
	<?php 
	} else { ?>
		<style type="text/css" media="all">@import "css/style_small.css";</style>
	<?php
	}?>
    <script type='text/javascript' src='js/calendar_pop.js'></script>
    <style type="text/css" media="all">@import "css/calendar_pop.css";</style>		
</head>

<body <?php echo $onload ?>>
<?php $getvalues = "?id=".$revfull; ?>

<script language="JavaScript">
            
function submitform() {
    var term = document.getElementById('term');					   
    if (term.value.length == 0) {
        alert("Please select the term.");
        return false;
    }
    
    document.activity_manager.submit();
    
}
</script>

<form method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?><?php echo $getvalues; ?>" name="activity_manager">
	<table cellspacing="0" cellpadding="0" border="0" width="97%">
	<tr>
		<td><img src="images/<?php echo $_SESSION['franchisedata']['logo'];?>" alt="" / align="center" border="0"></td>
		<td align="right"></td>
	</tr>
	</table><br>
	<table cellspacing="0" cellpadding="1" border="0" width="95%">
	<tr>
		<td height="5" colspan="5"><img src="images/spacer.gif" height="5" width="1"></td>
	</tr>
	<tr>
		<td width="5%" rowspan="2">&nbsp;</td>
		<td valign="top" width="5%">
			<img src="images/icons/24_shadow/error.png"><img src="images/spacer.gif" height="1" width="10">
		</td>
		<td valign="top" height="5" width="60%">
			<strong><font style="font-size:120%;font-family:Trebuchet MS">Reset Guardian Priority</font></strong><br />
            <span class="note">This will reset all Guardians who are attending the selected term to priority Yes and all others to priorty No. Once changed, this cannot be undone.</span>
		</td>
		
	</tr>
	<tr>
		<td valign="top" colspan="4">
            <br>
            <?php
            if ($_POST['process'] == "Process") { ?>
                <table width="100%" border="0">
                    <tr>
                        <td class="ls_top"><strong>Guardian Email</strong></td>
                        <td class="ls_top"><strong>Old Priority</strong></td>
                        <td class="ls_top"><strong>New Priority</strong></td>
                    </tr>
                <?php
                foreach ($outputArr AS $key => $val) {
                    include 'includes/ls.php';
                    $vals = explode("|", $val);
                    ?>
                    <tr>
                        <td class="ls_<?php echo $ls;?>"><?php echo $key;?></td>
                        <td class="ls_<?php echo $ls;?>" align="center"><?php echo $vals[0];?></td>
                        <td class="ls_<?php echo $ls;?>" align="center"><?php echo $vals[1];?></td>
                    </tr>
                <?php
                } ?>
            </table>
            <?php
            } else { ?>
                <table width="100%" border="0">
                    <tr>
                        <td class="ls_on">Select Term to Update:</td>
                        <td  class="ls_on">
                            <select name="term" id="term" style="font-size:14px;padding:3px;width:175px">
                                <option value="">Select Term</option>
                                <option value="Spring">Spring</option>
                                <option value="Summer">Summer</option>
                                <option value="Autumn">Autumn</option>
								<option value="Winter">Winter</option>
								<option value="Term 1">Term 1</option>
                                <option value="Term 2">Term 2</option>
                                <option value="Term 3">Term 3</option>
								<option value="Term 4">Term 4</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="ls_on">Select Year to Update:</td>
                        <td  class="ls_on">
                            <select name="termyear" style="font-size:14px;padding:3px;width:175px">
                                <?php
                                $startYear = 2014;
                                $endYear = (date("Y")+2);
                                $selYear = date("Y");
                                for ($x=$endYear;$x >= $startYear;$x--) {
                                    $sel = ($x==$selYear) ? " selected" : "" ;?>
                                    <option<?php echo $sel;?>><?php echo $x;?></option>
                                <?php
                                } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <br />
                            <input type="submit" onclick="javascript: submitform();return false;" name="update" value="Update Guardians" style="font-size:14px;padding:3px;width:175px" />
                        </td>
                    </tr>
                </table>
            <?php
            } ?>
		</td>
	</tr>
	</table>
	<input type="hidden" name="process" value="Process">
	</form>
</body>
</html>
