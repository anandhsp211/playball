<?php
include_once("_globalconnect.php");
function dateTimeCheck($givenDate)
{
    if ($givenDate) {
        $date = new DateTime($givenDate);
        $result = $date->format('Y-m-d');
        if ($result) {
            $today_date = date('Y-m-d');
            if ($today_date > $result) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    } else {
        return 'no_date';
    }
}
$filename = $rootpath . 'document_store/temp_files/' . strtotime('now') . '.csv';
$tab_select = mysql_real_escape_string($_GET['tab']); ?>
<?php
$search = mysql_real_escape_string($_GET['search']);
$tab_select = mysql_real_escape_string($_GET['tab']);
$country = mysql_real_escape_string($_GET['country']);
$addsql = '';
if ($_GET['country'] != "" && isset($tab_select) && $tab_select == 2) {
    $addsql = " AND country = '" . mysql_real_escape_string($_GET['country']) . "' ";
}
if (isset($search) && $search && $search != 'all') {
    $headers = "Franchise Owner A,Franchise Owner B,Province";
    if ($search == 'business_address') {
        $headers .= "Business Address";
        $val = ', businessaddressline1, businessaddressline2';
    } else if ($search == 'home_address') {
        $headers .= ",Home Address";
        $val = ', addressline1, addressline2';
    } else if ($search == 'insurance_certificate') {
        $headers .= ",Insurance Certificate";
        $val = ', insurancephysicalcopy, insuranceexpirydate';
    } else if ($search == 'criminal_check') {
        $headers .= ",Criminal Check";
        $val = ', criminalphysicalcopy, criminalexpirydate';
    } else if ($search == 'first_aid') {
        $headers .= ",First Aid Certificate";
        $val = ', firstaidphysicalcopy, firstaidexpirydate';
    } else if ($search == 'child_protection') {
        $headers .= ",Child Protection";
        $val = ', childphysicalcopy, childexpirydate';
    } else if ($search == 'debit_order') {
        $headers .= ",Debit Order";
        $val = ', debitphysicalcopy, debitpaymentdate';
    } else if ($search == 'franchise_contract') {
        $headers .= ",Franchise Contract";
        $val = ', franchisephysicalcopy, franchiseexpirydate';
    }
    $admin_sql = "SELECT id, franchiseownera, franchiseownerb, provencecounty $val $addsql from udf_1728EFBDA81692282BA642AAFD57BE3A WHERE issaved = 1";
} else {
    $headers = "Completed,Franchise Owner A,Franchise Owner B,Province,Home Address,Business Address,Insurance Certificate,Criminal Check,First Aid Certificate,Child Protection,Debit Order,Franchise Contract,Coaches,";
    $admin_sql = "SELECT * from udf_1728EFBDA81692282BA642AAFD57BE3A WHERE issaved = 1 $addsql";
}
$admin_res = mysql_query($admin_sql);
$admin_list = array();
$valueranchise_list = array();
$admin_check = 1;
while ($admin_row = mysql_fetch_assoc($admin_res, MYSQL_ASSOC)) {
    array_push($admin_list, $admin_row);
    $admin_check++;
}
if ($tab_select == 1) {
    $addSQLs = ($_GET['country'] != "") ? "WHERE u.country ='" . mysql_real_escape_string($_GET['country']) . "'" : "";
}
$valueranchise_sql = "select count(case when schoolstatus = 'Other' then 1 else null end) as OtherCount, count(case when schoolstatus = 'School' then 1 else null end) as SchoolCount, count(case when schoolstatus = '' then 1 else null end) as CountEmpty, count(case when schoolstatus = 'Other' AND status = 'Approved' then 1 else null end) as OtherCountActive, count(case when schoolstatus = 'School' AND status = 'Approved' then 1 else null end) as SchoolCountActive,
        u.username,
        ho.franchiseownera,
        ho.id,
        ho.franchiseownerb, 
        ho.franchiseownerb,
        ho.provencecounty,
        ho.franchiseareaa FROM 
		`udf_1728efbda81692282ba642aafd57be3a` as ho
        LEFT JOIN users as u
        ON u.departments = ho.department
		LEFT JOIN `udf_5737034557EF5B8C02C0E46513B98F90` as udf_venue		
        ON udf_venue.department = u.departments
        " . $addSQLs . " 
        AND ho.department = udf_venue.department 
        AND udf_venue.issaved = 1 
        AND ho.issaved = 1
        AND udf_venue.status = 'Approved' 
        AND u.isactive = 1 
        GROUP BY departments 
        ORDER BY ho.id ASC";
$valueranchise = mysql_query($valueranchise_sql);
$valueranchise_check = 1;
while ($valueranchise_row = mysql_fetch_assoc($valueranchise, MYSQL_ASSOC)) {
    array_push($valueranchise_list, $valueranchise_row);
    $valueranchise_check++;
}
$tab_select = $_GET['tab'];
$excel = $_GET['excel'];
if ($tab_select == 1 && $excel == 1 || $country) {
    $headers = "Record ID,Franchise Name,Franchise Owner A,Franchise Owner B,Province/Country/State,Franchise Area,Venue,School,";
    foreach ($valueranchise_list as $key => $value) {
        $line = '';
        $venue = $value['OtherCount'] + $value['CountEmpty'];
        $venue_active = $value['OtherCountActive'];
        $SchoolCount = $value['SchoolCount'];
        $SchoolCountActive = $value['SchoolCountActive'];
        $venue_count = $venue_active . "--" . $venue;
        $school_count = $SchoolCountActive . "--" . $SchoolCount;
        $value = '"' . $value['id'] . '"' . "," . '"' . $value['username'] . '"' . "," . '"' . $value['franchiseownera'] . '"' . "," . '"' . $value['franchiseownerb'] . '"' . "," . '"' . $value['provencecounty'] . '"' . "," . '"' . $value['franchiseareaa'] . '"' . "," . '"' . $venue_count . '"' . "," . '"' . $school_count . '"' . ",";
        $line .= $value;
        $data .= trim($line) . "\n";
    }
    $data = str_replace("\r", "", $data);
    if ($data == "") {
        $data = "\n(0) Records Found!\n";
    }
}
else if ($tab_select == 2 && $excel == 1 || $country || $search) {
    foreach ($admin_list as $key => $value) {
        $line = '';
        $search = $_GET['search'];
        if (isset($search) && $search == 'all') {
            $selc = true;
        } else if (isset($search) && $search != 'all') {
            $selc = false;
        } else {
            $selc = true;
        }
        if ($value['insuranceexpirydate'] == '0000-00-00') {
            $value['insuranceexpirydate'] = '';
        } else if ($value['criminalexpirydate'] == '0000-00-00') {
            $value['criminalexpirydate'] = '';
        } else if ($value['firstaidexpirydate'] == '0000-00-00') {
            $value['firstaidexpirydate'] = '';
        } else if ($value['childexpirydate'] == '0000-00-00') {
            $value['childexpirydate'] = '';
        } else if ($value['debitpaymentdate'] == '0000-00-00') {
            $value['debitpaymentdate'] = '';
        }

        if ($key >= 0) {
            $date_check = '';
            $date_global = '';
            $date_check1 = dateTimeCheck($value['insuranceexpirydate']);
            $date_check2 = dateTimeCheck($value['criminalexpirydate']);
            $date_check3 = dateTimeCheck($value['firstaidexpirydate']);
            $date_check4 = dateTimeCheck($value['childexpirydate']);
            $date_check5 = dateTimeCheck($value['debitpaymentdate']);
            $date_check6 = dateTimeCheck($value['franchiseexpirydate']);
            if ($date_check1 == true &&
                $date_check2 == true &&
                $date_check3 == true &&
                $date_check4 == true &&
                $date_check5 == true &&
                $date_check6 == true) {
                $date_global = true;
            } else if ($date_check1 == false &&
                $date_check2 == false &&
                $date_check3 == false &&
                $date_check4 == false &&
                $date_check5 == false &&
                $date_check6 == false) {
                $date_global = false;
            } else {
                $date_global = true;
            }
            if ($date_check1 === 'no_date') {
                $date_check = '';
                $date_check = 'no_date';
            } else if ($date_check2 === 'no_date') {
                $date_check = '';
                $date_check = 'no_date';
            } else if ($date_check3 === 'no_date') {
                $date_check = '';
                $date_check = 'no_date';
            } else if ($date_check4 === 'no_date') {
                $date_check = '';
                $date_check = 'no_date';
            } else if ($date_check5 === 'no_date') {
                $date_check = '';
                $date_check = 'no_date';
            } else if ($date_check6 === 'no_date') {
                $date_check = '';
                $date_check = 'no_date';
            }
        }
        if ($selc && !empty(trim($value['franchiseownera'])) &&
            !empty(trim($value['franchiseownerb'])) &&
            !empty(trim($value['provencecounty'])) &&
            !empty(trim($value['addressline1'])) &&
            !empty(trim($value['addressline2'])) &&
            !empty(trim($value['businessaddressline1'])) &&
            !empty(trim($value['businessaddressline2'])) &&
            !empty(trim($value['insurancephysicalcopy'])) &&
            !empty(trim($value['criminalphysicalcopy'])) &&
            !empty(trim($value['firstaidphysicalcopy'])) &&
            !empty(trim($value['childphysicalcopy'])) &&
            !empty(trim($value['debitphysicalcopy'])) &&
            !empty(trim($value['franchisephysicalcopy'])) &&
            !empty(trim($value['coacha'])) &&
            !empty(trim($value['coachb'])) &&
            !empty(trim($value['coachc'])) &&
            !empty(trim($value['coachd'])) &&
            $date_global == true && $date_check != 'no_date') {
            $Complete = 'Missing';
        } else if ($selc &&
            !empty(trim($value['franchiseownera'])) &&
            !empty(trim($value['franchiseownerb'])) &&
            !empty(trim($value['provencecounty'])) &&
            !empty(trim($value['addressline1'])) &&
            !empty(trim($value['addressline2'])) &&
            !empty(trim($value['businessaddressline1'])) &&
            !empty(trim($value['businessaddressline2'])) &&
            !empty(trim($value['insurancephysicalcopy'])) &&
            !empty(trim($value['criminalphysicalcopy'])) &&
            !empty(trim($value['firstaidphysicalcopy'])) &&
            !empty(trim($value['childphysicalcopy'])) &&
            !empty(trim($value['debitphysicalcopy'])) &&
            !empty(trim($value['franchisephysicalcopy'])) &&
            !empty(trim($value['coacha'])) &&
            !empty(trim($value['coachb'])) &&
            !empty(trim($value['coachc'])) &&
            !empty(trim($value['coachd'])) &&
            $date_global == false && $date_check != 'no_date') {
            $Complete = 'Yes';
        } else if ($selc != false && $date_check == 'no_date') {
            $Complete = 'No';
        } else if ($selc != false) {
            $Complete = 'No';
        }
        $FranchiseOwnerA = trim($value['franchiseownera']);
        $FranchiseOwnerB = trim($value['franchiseownerb']);
        $ProvinceCountryState = trim($value['provencecounty']);

        if ($value['addressline1'] && $value['addressline2']) {
            $HomeAddress = 'Yes';
        } else if (!$search || $search == 'all') {
            $HomeAddress = 'No';
        }
        if ($value['businessaddressline1'] && $value['businessaddressline2']) {
            $BusinessAddress = 'Yes';
        } else if (!$search || $search == 'all') {
            $BusinessAddress = 'No';
        }

        if (!empty(trim($value['insurancephysicalcopy'])) && !empty(trim($value['insuranceexpirydate']))) {
            $date_check = dateTimeCheck($value['insuranceexpirydate']);
            if ($date_check == true) {
                $InsuranceCertificate = 'Missing';
            } else { ?>
                <?php if (!empty(trim($value['insurancephysicalcopy'])) && !empty(trim($value['insuranceexpirydate']))) {
                    $InsuranceCertificate = 'Yes';
                } else if (!$search || $search == 'all') {
                    $InsuranceCertificate = 'No';
                } ?>
            <?php }

        } else if (!$search || $search == 'all') {
            $InsuranceCertificate = 'No';
        }

        if (!empty(trim($value['criminalphysicalcopy'])) && !empty(trim($value['criminalexpirydate']))) {
            $date_check = dateTimeCheck($value['criminalexpirydate']);
            if ($date_check == true) {
                $CriminalCheck = 'Missing';
            } else { ?>
                <?php if (!empty(trim($value['criminalphysicalcopy'])) && !empty(trim($value['criminalexpirydate']))) {
                    $CriminalCheck = 'Yes';
                } else if (!$search || $search == 'all') {
                    $CriminalCheck = 'No';
                } ?>
            <?php }
        } else if (!$search || $search == 'all') {
            $CriminalCheck = 'No';
        }

        if (!empty(trim($value['firstaidphysicalcopy'])) && !empty(trim($value['firstaidexpirydate']))) {
            $date_check = dateTimeCheck($value['firstaidexpirydate']);
            if ($date_check == true) {
                $FirstAidCertificate = 'Missing';
            } else { ?>
                <?php if (!empty(trim($value['firstaidphysicalcopy'])) && !empty(trim($value['firstaidexpirydate']))) {
                    $FirstAidCertificate = 'Yes';
                } else if (!$search || $search == 'all') {
                    $FirstAidCertificate = 'No';
                } ?>
            <?php }
        } else if (!$search || $search == 'all') {
            $FirstAidCertificate = 'No';
        }
        if (!empty(trim($value['childphysicalcopy'])) && !empty(trim($value['childexpirydate']))) {
            $date_check = dateTimeCheck($value['childexpirydate']);
            if ($date_check == true) {
                $ChildProtection = 'Missing';
            } else { ?>
                <?php if (!empty(trim($value['childphysicalcopy'])) && !empty(trim($value['childexpirydate']))) {
                    $ChildProtection = 'Yes';
                } else if (!$search || $search == 'all') {
                    $ChildProtection = 'No';
                } ?>
            <?php }
        } else if (!$search || $search == 'all') {
            $ChildProtection = 'No';
        }
        if (!empty(trim($value['debitphysicalcopy'])) && !empty(trim($value['debitpaymentdate']))) {
            $date_check = dateTimeCheck($value['debitpaymentdate']);
            if ($date_check == true) {
                $DebitOrder = 'Missing';
            } else { ?>
                <?php if (!empty(trim($value['debitphysicalcopy'])) && !empty(trim($value['debitpaymentdate']))) {
                    $DebitOrder = 'Yes';
                } else if (!$search || $search == 'all') {
                    $DebitOrder = 'No';
                } ?>
            <?php }
        } else if (!$search || $search == 'all') {
            $DebitOrder = 'No';
        }
        if (!empty(trim($value['franchisephysicalcopy'])) && !empty(trim($value['franchiseexpirydate']))) {
            $date_check = dateTimeCheck($value['franchiseexpirydate']);
            if ($date_check == true) {
                $FranchiseContract = 'Missing';
            } else { ?>
                <?php if (!empty(trim($value['franchisephysicalcopy'])) && !empty(trim($value['franchiseexpirydate']))) {
                    $FranchiseContract = 'Yes';
                } else if (!$search || $search == 'all') {
                    $FranchiseContract = 'No';
                } ?>
            <?php }
        } else if (!$search || $search == 'all') {
            $FranchiseContract = 'No';
        }
        if (!empty(trim($value['coacha'])) && !empty(trim($value['coachb'])) && !empty(trim($value['coachc'])) && !empty(trim($value['coachd']))) {
            $Coaches = 'Yes';
        } else if (!$search || $search == 'all') {
            $Coaches = 'No';
        }
        $value='';
        if(!empty($Complete) && $Complete){
            $value = '"' . $Complete . '"'. ",";
        }
        if(!empty($FranchiseOwnerA) && $FranchiseOwnerA){
            $value .= '"' . $FranchiseOwnerA . '"' . ",";
        }
        if(!empty($FranchiseOwnerB) && $FranchiseOwnerB){
            $value .= '"' . $FranchiseOwnerB . '"' . ",";
        }
        if(!empty($ProvinceCountryState) && $ProvinceCountryState){
            $value .= '"' . $ProvinceCountryState . '"' . ",";
        }
        if(!empty($HomeAddress) && $HomeAddress){
            $value .= '"' . $HomeAddress . '"' . ",";
        }
        if(!empty($BusinessAddress) && $BusinessAddress){
            $value .= '"' . $BusinessAddress . '"' . ",";
        }
        if(!empty($InsuranceCertificate) && $InsuranceCertificate){
            $value .= '"' . $InsuranceCertificate . '"' . ",";
        }
        if(!empty($CriminalCheck) && $CriminalCheck){
            $value .= '"' . $CriminalCheck . '"' . ",";
        }
        if(!empty($FirstAidCertificate) && $FirstAidCertificate){
            $value .= '"' . $FirstAidCertificate . '"' . ",";
        }
        if(!empty($ChildProtection) && $ChildProtection){
            $value .= '"' . $ChildProtection . '"' . ",";
        }
        if(!empty($DebitOrder) && $DebitOrder){
            $value .= '"' . $DebitOrder . '"' . ",";
        }
        if(!empty($FranchiseContract) && $FranchiseContract){
            $value .= '"' . $FranchiseContract . '"' . ",";
        }
        if(!empty($Coaches) && $Coaches){
            $value .= '"' . $Coaches . '"' . ",";
        }
        /*$value = '"' . $Complete . '"' . ","
                . '"' . $FranchiseOwnerA . '"' . ","
                . '"' . $FranchiseOwnerB . '"' . ","
                . '"' . $ProvinceCountryState . '"' . ","
                . '"' . $HomeAddress . '"' . ","
                . '"' . $BusinessAddress . '"' . ","
                . '"' . $InsuranceCertificate . '"' . ","
                . '"' .$CriminalCheck . '"' .","
                . '"' .$FirstAidCertificate . '"' .","
                . '"' .$ChildProtection . '"' .","
                . '"' .$DebitOrder . '"' .","
                . '"' .$FranchiseContract . '"' .","
                . '"' .$Coaches . '"' .",";*/
        $line .= $value;
        $data .= trim($line) . "\n";
    }
    $data = str_replace("\r", "", $data);
    if ($data == "") {
        $data = "\n(0) Records Found!\n";
    }
}
//var_dump($headers);
//var_dump($data);
//die;
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=franchise_export_" . date("Y-m-d H:i:s") . ".csv");
header("Pragma: no-cache");
header("Expires: 0");
print "\n$headers\n$data";
