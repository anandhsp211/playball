<?php
include_once("_globalconnect.php");

if ($_POST['action'] == "Cancel") {
    
    $_SESSION['done'] = "";
    unset($_SESSION['sessionmanager'],$_SESSION['done']);
    header("Location: /bulk-change-clear-cookies.php");
    exit();
}

if ($_POST['action'] == "Finished") {
    
    // Need to update the session data if applcale
    if ($_GET['module'] == "classes") {
        
        $recordList = "(";
        foreach ($_SESSION['hashidArr'] AS $records) {
            $recordList .= "parent_hashid = '".$records."' OR ";
        }
        $recordList = substr($recordList,0,-4);
        $recordList .= ")";
        
        $sql = "DELETE FROM class_sessions WHERE ".$recordList;
        //echo $sql."<br>";
        mysql_query($sql);
        
        function getSetDaysofWeek($startDate, $endDate, $weekdayNumber) {
            $startDate = strtotime($startDate);
            $endDate = strtotime($endDate);
        
            $dateArr = array();
        
            do {
            if (date("w", $startDate) != $weekdayNumber) {
                $startDate += (24 * 3600); // add 1 day
                }
            }
        
            while(date("w", $startDate) != $weekdayNumber);
            while($startDate <= $endDate) {
                $dateArr[] = date('Y-m-d', $startDate);
                $startDate = strtotime("+1 week", $startDate); // add 7 days
            }
            return($dateArr);
        }
        
        for ($x=0;$x<$_POST['totalrecords'];$x++) {
            
            $sessionStartDate = explode(" ",$_POST['sessionstart_'.$x]);
            $sessionStartDateSet = $sessionStartDate[0];
            $sessionStartTimeSet = $sessionStartDate[1];
            
            $sessionEndDate = explode(" ",$_POST['sessionend_'.$x]);
            $sessionEndDateSet = $sessionEndDate[0];
            $sessionEndTimeSet = $sessionEndDate[1];
            
            $dayoftheweek = date('w', strtotime($sessionStartDateSet));
            
            $exludeStartRange = date('Y-m-d', strtotime($_POST['sessionexstart_'.$x]));
            $exludeEndRange = date('Y-m-d', strtotime($_POST['sessionexend_'.$x]));
            
            // Add exclude date range to db
            $sqlex = "DELETE FROM `class_sessions_exclude` WHERE `parent_hashid` = '".$_POST['hashid_'.$x]."' LIMIT 1";
            mysql_query($sqlex);
            
            if ($_POST['sessionexstart_'.$x] != "") {
                $sqlex = "INSERT INTO `class_sessions_exclude` (`parent_hashid`, `start_date`, `end_date`)
                            VALUES ('".$_POST['hashid_'.$x]."', '".$exludeStartRange."', '".$exludeEndRange."');";
                //echo $sqlex."<br>";
                mysql_query($sqlex) ;
            }
                        
            $p = getSetDaysofWeek($sessionStartDateSet, $sessionEndDateSet, $dayoftheweek);
            
            $sqli .= "INSERT INTO `class_sessions`
                            (`registrantid`, `department`, `createdby`, `location`, `parent_hashid`, `session_date`, `session_start_time`, `session_end_time`, `datecreated`)
                                VALUES ";
            
            foreach ($p as $d) {
                
                // Determine if the date is between the exclude dates
                if (($d >= $exludeStartRange) && ($d <= $exludeEndRange)) {
                    $excludeDate = 1; // remove
                } else {
                    $excludeDate = 0; // add
                }
                
                if ($excludeDate == 0) {
                    // Let's generate each session and return home
                    $sqli .= "('1', '".$_SESSION['franchiseID']."','".$_SESSION['userid']."','2B8A61594B1F4C4DB0902A8A395CED93','".$_POST['hashid_'.$x]."','".$d."',
                                '".$sessionStartTimeSet."','".$sessionEndTimeSet."','".date("Y-m-d H:i:s")."'), ";
                }
            }
            
            $sqli = substr($sqli,0,-2);
            /*echo $sqli."<br /><br />";
            echo $_POST['hashid_'.$x];
            echo $x."<br /><br />";*/
            //echo $sqli."<br>";
            mysql_query($sqli);
            
            unset($sqli);
        }
        
    }
    
    $_SESSION['done'] = "";
    unset($_SESSION['sessionmanager'],$_SESSION['done']);
    header("Location: /thinline.php?id=::::::::::::::::::::".$_SESSION['menuTab'].":".$_SESSION['locationhash']);
    exit();
    
}
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title>Bulk Change Records</title>
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <link rel="shortcut icon" href="/favicon-<?php echo strtolower($_SESSION['franchisedata']['name']);?>.ico" type="image/x-icon" />
        <style type="text/css" media="all">@import "css/style.css?v=<?php echo strtotime(date("Y-m-d"));?>";</style>
        <style type="text/css" media="all">@import "css/calendar_pop.css?v=<?php echo strtotime(date("Y-m-d"));?>";</style>
        <script type='text/javascript' src='js/calendar_pop.js'></script>
        <script type="text/javascript" language="javascript" src="lytebox/lytebox.js"></script>
        <link rel="stylesheet" href="lytebox/lytebox.css?v=<?php echo strtotime(date("Y-m-d"));?>" type="text/css" media="screen" />
        <script src="/js/jquery-1.10.2.js"></script> 
    </head>
<body>
<style>
    #overlay {
        background: #ffffff;
        color: #666666;
        position: fixed;
        height: 100%;
        width: 100%;
        z-index: 5000;
        top: 0;
        left: 0;
        float: left;
        text-align: center;
        padding-top: 25%;
    }
</style>
<div id="overlay">
    <img src="/images/loader.gif" alt="Loading" /><br/>
    Loading...
</div>
<script>

jQuery(window).load(function(){
    jQuery('#overlay').fadeOut();
});

function clearText(field){
    field.value = '';
}
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
</script>
<br />
<a href="/"><img src="images/<?php echo $_SESSION['franchisedata']['logo'];?>" title="<?php echo $_SESSION['franchisedata']['name'];?> Franchise Management App" style="margin-top:-10px;" align="center" border="0"></a><br /><br />
<?php
if ($_GET['stage'] == 1) {
    $_SESSION['done'] = "";
    unset($_SESSION['sessionmanager'],$_SESSION['done']); ?>

    <form method="POST" name="Form" id="Form" action="bulk-change.php?stage=2">
    
    Please select the fields you would like to update: <br /><br />
        <table class="note" border="0" cellpadding="2" cellspacing="2">
            <?php
            $sqli = "SELECT id, label FROM udf_definitions
                        WHERE location = '".mysql_real_escape_string($_SESSION['locationhash'])."'
                        AND isactive = 1 AND registrantid =1
                        AND caneditvalues = 1
                        AND fieldtype != 11
                        ORDER BY sortorder ASC";
            //echo $sqli."<br>";
            $res = mysql_query($sqli);
            $counter = 0;
            $ls = "on";
            while ($row = mysql_fetch_array($res)) {
                $exfieldname = columnnames($row['label']);
                if (!in_array($exfieldname, $excludefieldarray[$_SESSION['franchisecountry']]))  {
                    if ($counter == 0) {
                        echo "<tr>";
                    } ?>
                        <td class="ls_<?php echo $ls;?>"><?php echo $row['label'];?></td>
                        <td class="ls_<?php echo $ls;?>"><input type="checkbox" name="fields[]" value="<?php echo $row['id'];?>"</td>
            <?php
                    if ($counter == 4) {
                        echo "</tr>";
                        $ls = ($ls == "off") ? "on" : "off";
                        $counter = 0;
                    } else {
                        $counter++;    
                    }
                    
                }
            } ?>
        </table>
        
        <br />
        <input type="submit" name="action" value="Cancel" style="padding:3px;"/>
        <input type="submit" name="action" value="Next Step" onclick="return validateForm();" style="padding:3px;"/>&nbsp;
        
    </form>
<?php
} elseif ($_GET['stage'] == 2) {
    
    unset($_SESSION['hashrecstoupdate']);
    
    if (isset($_POST['fields'])) {
        $_SESSION['fields'] = $_POST['fields'];
    }
    
    $f = "(";
    foreach ($_SESSION['fields'] AS $field) {
        $f .= "id = '".$field."' OR ";
    }
    $f = substr($f,0,-4);
    $f .= ")";
    
    // build the field type list
    $sqli = "SELECT id, fieldtype, columntype, label, hashid, linkedid,object_master_id FROM udf_definitions
                WHERE location = '".mysql_real_escape_string($_SESSION['locationhash'])."'
                AND ".$f."
                AND isactive = 1 AND registrantid = 1 ORDER BY sortorder ASC";
    //echo $sqli."<br>";
    $res = mysql_query($sqli);
    $columns = array();
    $counter = 0;
    while ($row = mysql_fetch_array($res)) {
        $columns[$counter]['id'] = $row['id'];
        $columns[$counter]['fieldtype'] = $row['fieldtype'];
        $columns[$counter]['columntype'] = $row['columntype'];
        $columns[$counter]['label'] = $row['label'];
        $columns[$counter]['hashid'] = $row['hashid'];
        $columns[$counter]['linkedid'] = $row['linkedid'];
        $columns[$counter]['object_master_id'] = $row['object_master_id'];
        
        $selectStr .= columnnames($row['label']).", ";
        
        $counter++;
    }
    
    //echo "<pre>";
    //print_r($_SESSION['hashrecstoupdate']);
    //print_r($columns);
    //print_r($_SESSION['hashidArr']);
    
    $recordList = "(";
    foreach ($_SESSION['hashidArr'] AS $records) {
        $recordList .= "hashid = '".$records."' OR ";
    }
    $recordList = substr($recordList,0,-4);
    $recordList .= ")";
    
    // Now we can create the output
    echo "<strong>Important:</strong><br /><font style='color:red;'>Once you change this information there is no way to reverse the action.<br />Please be sure before making any changes.</font><br/><br/>";
    
    if ($_SESSION['locationhash'] == "2B8A61594B1F4C4DB0902A8A395CED93") {
        $addChange = "&module=classes";
    }
    
    echo "<form method='POST' action='bulk-change.php?stage=1".$addChange."'> ";
    echo "<input type='submit' name='action' value='Cancel' style='padding:3px;'/>&nbsp;";
    echo "<input type='submit' name='action' value='Reselect Fields' style='padding:3px;'/>&nbsp;&nbsp;&nbsp;&nbsp;";
    echo "<input type='submit' name='action' value='Finished' style='padding:3px;'/><br>";
    echo "<script type='text/javascript' src='/js/savechanges.js'></script>";
    echo "<input type='hidden' name='cleanrecs' value='1' />";
    echo "<input name=\"hiddenDataChanged\" type=\"hidden\" id=\"hiddenDataChanged\" value=\"0\">";
    echo "<span style=\"color:red;\" id=\"txtHint\"></span>";
    echo "<br />";
    echo "<style>table {white-space:nowrap;</style>";
    echo "<table border=\"0\" cellpadding=\"2\" cellspacing=\"2\">";
    echo "<tr>";
    echo "<td class='ls_top'><strong>ID</strong></td>";
    foreach ($columns AS $column) {
        echo "<td class='ls_top'>";
            echo "<strong>".$column['label']."</strong><br />";
            if ($column['fieldtype'] == 10) {
                echo "<a href='bulk-update-datalink.php?id=2:::::::::::::::::::::".$_SESSION['locationhash']."::::".$column['linkedid'].":".$column['label'].":".$recordissaved."&linkedid=".$column['linkedid']."' ".LFD."><font class=\"note\">assign</font></a>";
            } else {
                $column['hashid'] = ($column['object_master_id'] != "") ? $column['object_master_id'] : $column['hashid'] ;
                echo "<a href='bulk-update.php?col=".$column['label']."&type=".$column['fieldtype']."&recs=hashid&colhashid=".$column['hashid']."' ".LFD."><font class=\"note\">update</font></a>";
            }
        echo "</td>";
    }
    if ($_SESSION['locationhash'] == "2B8A61594B1F4C4DB0902A8A395CED93") {
        echo "<td class='ls_top'><strong>Session Start</strong></td>";
        echo "<td class='ls_top'><strong>Session End</strong></td>";
        echo "<td class='ls_top'><strong>Exclude Range<br><font style='font-size:10px;'>Start</strong> &nbsp;&nbsp;<a href='bulk-update.php?col=excludestart&type=8&colhashid=ignore' ".LFD."><font class=\"note\">update</font></a></td>";
        echo "<td class='ls_top'><strong>Exclude Range<br><font style='font-size:10px;'>End</strong> &nbsp;&nbsp;<a href='bulk-update.php?col=excludeend&type=8&colhashid=ignore' ".LFD."><font class=\"note\">update</font></a></td>";
    }
    echo "</tr>";
    
    include_once 'display-fields.php';
    
    //echo "<pre>";
    //print_r($columns);
    
    $sql = "SELECT hashid, recordid, ".substr($selectStr,0,-2)." FROM udf_".mysql_real_escape_string($_SESSION['locationhash'])."
            WHERE ".$recordList." ORDER BY recordid ASC";
    //echo $sql."<br>";
    $res = mysql_query($sql);
    $counter = 0;
    
    while ($row = mysql_fetch_array($res)) {
        $_SESSION['hashrecstoupdate'][$row['hashid']] = $row['hashid'];
        include("includes/ls.php");
        echo "<tr>";
            echo "<td class='ls_".$ls."' style='font-size:14px;'>";
                echo $row['recordid']."<br />";
            echo "</td>";
            foreach ($columns AS $column) {
                $c = columnnames($column['label']);
                echo "<td class='ls_".$ls."' style='font-size:14px;'>";
                    echo displayFields($column['fieldtype'],$column['label'],$row[$c],columnnames($column['label']),$counter,$column['hashid'],$row['hashid'],$column['linkedid'],$_SESSION['locationhash'],$column['object_master_id']);
                echo "</td>";
            }
            
            // if this is camps and classes lets add the sessions to the lsit
            if ($_SESSION['locationhash'] == "2B8A61594B1F4C4DB0902A8A395CED93") {
                
                if ($_SESSION['done'] != "doe") {
                    // Get the session information and set the cookies to set values
                    $sqlsesh = "SELECT session_date, session_start_time FROM class_sessions WHERE parent_hashid = '".$row['hashid']."' ORDER BY id ASC LIMIT 1";
                    //echo $sqlsesh."<br>";
                    $ressesh = mysql_query($sqlsesh);
                    $rowsesh = mysql_fetch_array($ressesh);
                    
                    $sqlseshi = "SELECT session_date, session_end_time FROM class_sessions WHERE parent_hashid = '".$row['hashid']."' ORDER BY id DESC LIMIT 1";
                    //echo $sqlseshi."<br><br>";
                    $resseshi = mysql_query($sqlseshi);
                    $rowseshi = mysql_fetch_array($resseshi);
                    
                    $sqlex = "SELECT * FROM class_sessions_exclude WHERE parent_hashid = '".$row['hashid']."' ORDER BY id DESC LIMIT 1";
                    //echo $sqlex."<br><br>";
                    $resex = mysql_query($sqlex);
                    $rowex = mysql_fetch_array($resex);
                    
                    if ($rowex['start_date'] != "") {
                        $_SESSION['sessionmanager'][$row['hashid']]['excludestart'] =  date("d-M-Y",strtotime($rowex['start_date']));
                    }
                    
                    if ($rowex['end_date'] != "") {
                        $_SESSION['sessionmanager'][$row['hashid']]['excludeend'] =  date("d-M-Y",strtotime($rowex['end_date']));
                    }
                    
                    if ($rowsesh['session_date'] != "") {
                    
                        $_SESSION['sessionmanager'][$row['hashid']]['session_start_date'] = $rowsesh['session_date'];
                        $_SESSION['sessionmanager'][$row['hashid']]['session_start_time'] = $rowsesh['session_start_time'];
                        $_SESSION['sessionmanager'][$row['hashid']]['session_end_date'] = $rowseshi['session_date'];
                        $_SESSION['sessionmanager'][$row['hashid']]['session_end_time'] = $rowseshi['session_end_time'];
                        
                    } else {
                        $_SESSION['sessionmanager'][$row['hashid']]['session_start_date'] = "";
                    }
                    
                }
                
                // format the dates
                if ($_SESSION['sessionmanager'][$row['hashid']]['session_start_date']) {
                    $sessionStart = date("d-M-Y",strtotime($_SESSION['sessionmanager'][$row['hashid']]['session_start_date']))." ".$_SESSION['sessionmanager'][$row['hashid']]['session_start_time'];
                } else {
                    $sessionStart = "";
                }
                
                if ($_SESSION['sessionmanager'][$row['hashid']]['session_end_date']) {
                    $sessionEnd = date("d-M-Y",strtotime($_SESSION['sessionmanager'][$row['hashid']]['session_end_date']))." ".$_SESSION['sessionmanager'][$row['hashid']]['session_end_time'];
                } else {
                    $sessionEnd = "";
                }
                
                $excludeStart = ($_SESSION['sessionmanager'][$row['hashid']]['excludestart'] != "") ? date("d-M-Y",strtotime($_SESSION['sessionmanager'][$row['hashid']]['excludestart'])) : "";
                $excludeEnd = ($_SESSION['sessionmanager'][$row['hashid']]['excludeend'] != "") ? date("d-M-Y",strtotime($_SESSION['sessionmanager'][$row['hashid']]['excludeend'])) : "";
                
                echo "<td class='ls_".$ls."'>"; ?>
                    <input onchange="javascript:changeSessionValue('<?php echo $row['hashid'];?>','session_start', this.value);" style="width:135px" class="standardfield_date" type="text" style="text-align:right;font-family:Tahoma;font-size:14px;width:52%" name="sessionstart_<?php echo $counter;?>" id="sessionstart_<?php echo $counter;?>"  value="<?php echo $sessionStart;?>"> &nbsp;<img src="images/icons/calendar.png" id="sessionstart_<?php echo md5($counter);?>" style="cursor: pointer;" title="Date selector" ><script language="javascript">Calendar.setup({ inputField:"sessionstart_<?php echo $counter;?>",button:"sessionstart_<?php echo md5($counter);?>",align:"Ll00", ifFormat:"%d-%b-%Y %H:%M",showsTime:true,timeFormat:"24"   });</script>
                    <a href="#" onclick="javascript:clearText(sessionstart_<?php echo $counter;?>)"><img src="images/close.png" border="0"></a><br>
                    <input type="hidden" name="hashid_<?php echo $counter;?>" value="<?php echo $row['hashid'];?>" />
                <?php
                echo "</td>";
                echo "<td class='ls_".$ls."'>";?>
                    <input onchange="javascript:changeSessionValue('<?php echo $row['hashid'];?>','session_end', this.value);" style="width:135px" class="standardfield_date" type="text" style="text-align:right;font-family:Tahoma;font-size:14px;width:52%" name="sessionend_<?php echo $counter;?>" id="sessionend_<?php echo $counter;?>"  value="<?php echo $sessionEnd;?>"> &nbsp;<img src="images/icons/calendar.png" id="sessionend_<?php echo md5($counter);?>" style="cursor: pointer;" title="Date selector" ><script language="javascript">Calendar.setup({ inputField:"sessionend_<?php echo $counter;?>",button:"sessionend_<?php echo md5($counter);?>",align:"Ll00", ifFormat:"%d-%b-%Y %H:%M",showsTime:true,timeFormat:"24"   });</script>
                    <a href="#" onclick="javascript:clearText(sessionend_<?php echo $counter;?>)"><img src="images/close.png" border="0"></a><br>
                <?php
                echo "</td>";
                
                
                
                echo "<td class='ls_".$ls."'>";?>
                    <input onchange="javascript:changeSessionValue('<?php echo $row['hashid'];?>','excludestart', this.value);" style="width:135px" class="standardfield_date" type="text" style="text-align:right;font-family:Tahoma;font-size:14px;width:52%" name="sessionexstart_<?php echo $counter;?>" id="sessionexstart_<?php echo $counter;?>"  value="<?php echo $excludeStart;?>"> &nbsp;<img src="images/icons/calendar.png" id="sessionexstart_<?php echo md5($counter);?>" style="cursor: pointer;" title="Date selector" ><script language="javascript">Calendar.setup({ inputField:"sessionexstart_<?php echo $counter;?>",button:"sessionexstart_<?php echo md5($counter);?>",align:"Ll00", ifFormat:"%d-%b-%Y",showsTime:false});</script>
                    <a href="#" onclick="javascript:clearText(sessionexstart_<?php echo $counter;?>)"><img src="images/close.png" border="0"></a><br>
                <?php
                echo "</td>";
                echo "<td class='ls_".$ls."'>";?>
                    <input onchange="javascript:changeSessionValue('<?php echo $row['hashid'];?>','excludeend', this.value);" style="width:135px" class="standardfield_date" type="text" style="text-align:right;font-family:Tahoma;font-size:14px;width:52%" name="sessionexend_<?php echo $counter;?>" id="sessionexend_<?php echo $counter;?>"  value="<?php echo $excludeEnd;?>"> &nbsp;<img src="images/icons/calendar.png" id="sessionexend_<?php echo md5($counter);?>" style="cursor: pointer;" title="Date selector" ><script language="javascript">Calendar.setup({ inputField:"sessionexend_<?php echo $counter;?>",button:"sessionexend_<?php echo md5($counter);?>",align:"Ll00", ifFormat:"%d-%b-%Y",showsTime:false});</script>
                    <a href="#" onclick="javascript:clearText(sessionexend_<?php echo $counter;?>)"><img src="images/close.png" border="0"></a><br>
                <?php
                echo "</td>";
                
                
                
                
            } 
        echo "</tr>";
        $counter++;
    }
    
    /*echo "<pre>";
    print_r($_SESSION['sessionmanager']);
    echo "</pre>";*/
    
    echo "</table><br />";
    echo "<input type='hidden' name='totalrecords' value='".($counter)."' />";
    echo "<input type='submit' name='action' value='Cancel' style='padding:3px;'/>&nbsp;";
    echo "<input type='submit' name='action' value='Reselect Fields' style='padding:3px;'/>&nbsp;&nbsp;&nbsp;&nbsp;";
    echo "<input type='submit' name='action' value='Finished' style='padding:3px;'/><br>";
    echo "</form>";
    
    $_SESSION['done'] = "done";

} ?>
<script>
    (function(b){window.onbeforeunload=function(a){window.name+=" ["+b(window).scrollTop().toString()+"["+b(window).scrollLeft().toString()};b.maintainscroll=function(){if(0<window.name.indexOf("[")){var a=window.name.split("[");window.name=b.trim(a[0]);window.scrollTo(parseInt(a[a.length-1]),parseInt(a[a.length-2]))}};b.maintainscroll()})(jQuery);
function changeSessionValue(hashid,field,value)
{
    $.ajax({
       type: "GET",
       url: "change-session-data.php",
       data: {'hashid' : hashid,'field' : field,'sessionvalue' : value},
       dataType: "text",
       success: function(msg){
            //alert(msg);
       }
    });
}
</script>
</body>
</html>