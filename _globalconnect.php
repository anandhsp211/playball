<?php
include_once('_dbaccessdetails.php');
date_default_timezone_set('Europe/London');
// Get page load times
$time = microtime();
$time = explode(" ", $time);
$time = $time[1] + $time[0];
$start = $time;

$tUnixTime = time();
$sGMTMySqlString = date("Y-m-d H:i:s", $tUnixTime);
$midday = date("Y-m-d 12:00:00", $tUnixTime);
include_once("_sessions.php");
//include_once("login_check.php");
/// Format the $_GET values

$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

$gval = explode(":",mysql_real_escape_string($_GET['id']));
$fullidvalues = $gval[0].":".$gval[1].":".$gval[2].":".$gval[3].":".$gval[4].":".$gval[5].":".$gval[6].":".$gval[7].":".$gval[8].":".$gval[9].":".$gval[10].":".$gval[11]; // Dont take further than 11 or it will affect scripts
$truncstart = $gval[0].":".$gval[1].":".$gval[2].":".$gval[3].":".$gval[4].":".$gval[5].":".$gval[6].":".$gval[7].":".$gval[8].":".$gval[9].":".$gval[10];
$truncend = $gval[12].":".$gval[13].":".$gval[14].":".$gval[15].":".$gval[16].":".$gval[17].":".$gval[18].":".$gval[19].":".$gval[20].":".$gval[21].":".$gval[22].":".$gval[23].":".$gval[24].":".$gval[25];
$revfull = $gval[0].":".$gval[1].":".$gval[2].":".$gval[3].":".$gval[4].":".$gval[5].":".$gval[6].":".$gval[7].":".$gval[8].":".$gval[9].":".$gval[10].":".$gval[11].":".$gval[12].":".$gval[13].":".$gval[14].":".$gval[15].":".$gval[16].":".$gval[17].":".$gval[18].":".$gval[19].":".$gval[20].":".$gval[21].":".$gval[22].":".$gval[23].":".$gval[24].":".$gval[25].":".$gval[26].":".$gval[27];

// Check for SOG corruption
if ($_SESSION['sog'] < 10) {
	$_SESSION['sog'] = 20;
}
// We use the activities element tab a lot so it is worth storing as a session
if (($_SESSION['theactivitiesmtab'] == NULL) || ($_SESSION['theactivitiesmtab'] == "")) {
	$getact = "SELECT hashid FROM menu_tabs WHERE type = 'Activities' AND registrantid = ".$_SESSION['registrantid'];
	//echo $getact;
	$resgetact = mysql_query($getact);
	$rowgetact = mysql_fetch_array($resgetact);
		$_SESSION['theactivitiesmtab'] = $rowgetact['hashid'];
}

if ($_GET['recent'] == "hide") {
	$_SESSION['showrecent'] = 0;
	$sqlrec = "UPDATE users SET  `showrecent` =  '0' WHERE  `users`.`id` = ".$_SESSION['userid']." LIMIT 1 ;";
	mysql_query($sqlrec);
} elseif ($_GET['recent'] == "show")  {
	$_SESSION['showrecent'] = 1;
	$sqlrec = "UPDATE users SET  `showrecent` =  '1' WHERE  `users`.`id` = ".$_SESSION['userid']." LIMIT 1 ;";
	mysql_query($sqlrec);
}

// Generate departments sql;
// Add departmental security

// This is repeated in dropdowngenerator.php

if ($_SESSION['securityarrdept'][0] != "") {
	$deptcounter = 0;
	foreach ($_SESSION['securityarrdept'] AS $deptval) {
		if ($deptcounter == 0) {
			$deptoperator = " AND (";
		} else {
			$deptoperator = " OR ";
		}
		$addsqldepartments .= $deptoperator." FIND_IN_SET('".$deptval."', department)";
		$deptcounter++;
	}
	$addsqldepartments .= ")";
}

/// Some defintions for frequent use.
define("RID", $_SESSION['registrantid']); // Registrantid
define("UID", $_SESSION['userid']); // Userid
define("UNAME", $_SESSION['username']); // Username
define("ERES", $_SESSION['exactres']); // Exact Resolution W
define("ERESH", $_SESSION['exactresh']); // Exact Resolution H
define("SOG", $_SESSION['sog']); // Size of Group
define("CURRENCY", $_SESSION['currency']); // Set Currency Symbol
define("TAX", $_SESSION['taxvalue']); // Set Tax Value
define("TAXON", $_SESSION['taxon']); // Set Tax Value On
if ((!isset($gval[22])) || ($gval[22] == "")) { $gval[22] = 1; }
define("LOCTYPE", $gval[22]);
define("ACTID", $_SESSION['theactivitiesmtab']);
define("ISMASTER", $_SESSION['ismaster']); // Set as Is Master Account
define("FULLNAME", $_SESSION['fullname']); // Define full name for lists
define("SHOWRECENT", $_SESSION['showrecent']); // Show or hide rcents menu
define("CTAPIID", $_SESSION['clickatell']['ctApiID']); // clickatell api id
define("CTUSER", $_SESSION['clickatell']['ctUser']); // clickatell api id
define("CTPASS", $_SESSION['clickatell']['ctPassword']); // clickatell api id
$companyName = $_SESSION['franchisedata']['name']; // change in _ns as well
$domainname = $_SESSION['franchisedata']['url_website']; // need to change in the parent centre global file as well
$serverfullurl = $_SESSION['franchisedata']['url_crm'];
$franchiseID = $_SESSION['franchisedata']['frid']; // for document upload
//$rootpath = "/var/www/thinline/";
$rootpath = "";
$document_store_path = $rootpath."document_store/".$franchiseID."/";
$_SESSION['documentstorepath'] = $document_store_path;
/// All our required include files.
include_once("_timeformatting.php");
include_once('classes/browser.php');
include_once('functions.php');
//finclude_once('activity_engine_auto.php');
$active_file = $_SERVER['PHP_SELF'];
// Lyteframedata
//echo ERES." - ".ERESH;
if (ERES <= 800) {
	$lytewidth = 690;
} else {
	$lytewidth = 785;
}
if (ERESH <= 786) {
	$lyteheight = 485;
} else {
	$lyteheight = 585;
}
$_SESSION['lyteframedata'] = 'rel="lyteframe" name="lyteframe" rev=" width: '.$lytewidth.'px; height: '.$lyteheight.'px; scrolling: yes;"';
define("LFD", $_SESSION['lyteframedata']); // Lyteframe data session defined
$_SESSION['calremind'] = 'rel="lyteframe" name="lyteframe" rev=" width: 350px; height: 200px; scrolling: no;"';
define("CALR", $_SESSION['calremind']); // Lyteframe data session defined
function sort2d ($array, $index, $order='desc', $natsort=FALSE, $case_sensitive=FALSE)
    {
        if(is_array($array) && count($array)>0)
        {
           foreach(array_keys($array) as $key)
               $temp[$key]=$array[$key][$index];
               if(!$natsort)
                   ($order=='asc')? asort($temp) : arsort($temp);
              else
              {
                 ($case_sensitive)? natsort($temp) : natcasesort($temp);
                 if($order!='asc')
                     $temp=array_reverse($temp,TRUE);
           }
           foreach(array_keys($temp) as $key)
               (is_numeric($key))? $sorted[]=$array[$key] : $sorted[$key]=$array[$key];
           return $sorted;
      }
      return $array;
    }

function getLatLongFromPostcode($address) {
    $address = urlencode(trim($address));
    $file = "https://maps.googleapis.com/maps/api/geocode/json?address=".$address."&key=AIzaSyCFu4F84EiO3LYc11EV_WRgiWFAbUiHGkw&sensor=false";
    $contents = file_get_contents($file);
    $obj = json_decode($contents, true);
	
    $latitude = $obj['results'][0]['geometry']['location']['lat'];
    $longitude = $obj['results'][0]['geometry']['location']['lng'];
    
    return array ($latitude,$longitude);   
}

$registeredCountries = array("Botswana", "Canada", "China", "Germany", "Ireland", "Mexico", "Namibia", "New Zealand", "Qatar", "Singapore", "South Africa", "Switzerland", "The Netherlands", "United Kingdom", "United States", "Zimbabwe");

$googleMapsAPIKey = "AIzaSyDwYgGY1vNAzuzp6K5K5knvwP3rGlMVuLQ";

// Have an option to exlude fields from certain countries
$excludefieldarray = array(
	"United Kingdom"=>array("campinclusioncutoffdate","realex","realexinfo","realexmerchantid","realexsecretkey"), 
	"United States"=>array("sagepay","campinclusioncutoffdate","sagepaysettings","vendorid","encryptionpassword","realex","realexinfo","realexmerchantid","realexsecretkey","kitoptions","offerkit","kitfee","kitdescription","useshippingdistributor"),
	"Ireland"=>array("sagepay","sagepaysettings","vendorid","encryptionpassword","campinclusioncutoffdate"),
	"The Netherlands"=>array("sagepay","sagepaysettings","vendorid","encryptionpassword","campinclusioncutoffdate","realex","realexinfo","realexmerchantid","realexsecretkey"),
	"South Africa"=>array("sagepay","sagepaysettings","vendorid","encryptionpassword","campinclusioncutoffdate","realex","realexinfo","realexmerchantid","realexsecretkey"),
	"Botswana"=>array("sagepay","sagepaysettings","vendorid","encryptionpassword","campinclusioncutoffdate","realex","realexinfo","realexmerchantid","realexsecretkey"),
	"Switzerland"=>array("sagepay","sagepaysettings","vendorid","encryptionpassword","realex","realexinfo","realexmerchantid","realexsecretkey"),
	"Canada"=>array("sagepay","sagepaysettings","vendorid","encryptionpassword","campinclusioncutoffdate","realex","realexinfo","realexmerchantid","realexsecretkey"),
	"Mexico"=>array("sagepay","sagepaysettings","vendorid","encryptionpassword","campinclusioncutoffdate","realex","realexinfo","realexmerchantid","realexsecretkey"),
	"Qatar"=>array("sagepay","sagepaysettings","vendorid","encryptionpassword","campinclusioncutoffdate","realex","realexinfo","realexmerchantid","realexsecretkey"),
	"Singapore"=>array("sagepay","sagepaysettings","vendorid","encryptionpassword","campinclusioncutoffdate","realex","realexinfo","realexmerchantid","realexsecretkey"),
	"Zimbabwe"=>array("sagepay","sagepaysettings","vendorid","encryptionpassword","campinclusioncutoffdate","realex","realexinfo","realexmerchantid","realexsecretkey"),
	"New Zealand"=>array("sagepay","sagepaysettings","vendorid","encryptionpassword","campinclusioncutoffdate","realex","realexinfo","realexmerchantid","realexsecretkey")
	);

$approvedFields = array("venuename","address","citytown","zippostcode","stateprovincecounty","schoolstatus","venuetype","displayvenue","maincontact", "phonenumber", "emailaddress", "dontapplyregistrationfee", "dontapplykitfee", "notes", "openforregistration", "latitude", "longitude", "replacementbookingurl");

// Email server settings
//$email_host = "mail.playballkids.com";
//$email_port = "587";
//$email_username = "no-reply@playballkids.com";
//$email_password = "I4BhC28A8dkr";

$email_host = "smtp.sendgrid.net";
$email_port = "587";
$email_username = "mediatomcat";
$email_password = "m`l%f==;u#!1m@|c&yklmysu";

$age_group_colors = array("I Can Do"=>"#FFF100","Watch Me Play"=>"#00A651","Dinkies"=>"#EE1C25","Preps"=>"#00ADEE");

?>
