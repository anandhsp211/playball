var xmlHttp;
function updateresource(str,fieldname,recordid,script,txtHintid,chargeforeach) {
	if (str.length==0) { 
		document.getElementById("txtHint"+txtHintid).innerHTML="";
		return;
	}
	xmlHttp=GetXmlHttpObject()
	if (xmlHttp==null)	{
		alert ("Your browser does not support AJAX!");
		return;
  	} 
	
	if (script == 1) {
		var url="includes/update_resource.php";
		url=url+"?value="+str;
		url=url+"&fieldname="+fieldname;
		url=url+"&recorid="+recordid;
		url=url+"&script="+script;
		url=url+"&chargeforeach="+chargeforeach;
	} else if (script == 2) {
		var url="includes/update_resource.php";
		url=url+"?value="+str;
		url=url+"&fieldname="+fieldname;
		url=url+"&recorid="+recordid;
		url=url+"&script="+script;
		url=url+"&chargeforeach="+chargeforeach;
	}
	
	xmlHttp.onreadystatechange = stateChanged;
	
	function stateChanged() { 
		if (xmlHttp.readyState==4) { 
			document.getElementById("txtHint"+txtHintid).innerHTML = xmlHttp.responseText;
		}
	}
	
	xmlHttp.open("GET",url,true);
	xmlHttp.send(null);
} 