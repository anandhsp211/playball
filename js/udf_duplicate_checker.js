var xmlHttp;
function checkdup(str) {
    if (str.length == 0) {
        document.getElementById("txtHint").innerHTML = "";
        return;
    }
    xmlHttp = GetXmlHttpObject();
    if (xmlHttp == null)	{
        alert ("Your browser does not support AJAX!");
        return;
    }
    var url="../includes/udf_duplicate_checker.php";
    var locationID = document.forms['form'].elements['udfmodule'].value;
    url=url+"?name="+str;
    var locationIDs;
    locationIDs = locationID.replace(/&&/, "||");
    url=url+"&location="+locationIDs;
    console.log(url);
    xmlHttp.onreadystatechange = stateChanged;
    xmlHttp.open("GET",url,true);
    xmlHttp.send(null);
}

function stateChanged() {
    if (xmlHttp.readyState == 4) {
        document.getElementById("txtHint").innerHTML = xmlHttp.responseText;
        if (xmlHttp.responseText.length > 1) {
            document.forms['form'].elements['createfield'].disabled = true;
        } else {
            document.forms['form'].elements['createfield'].disabled = false;
        }
    }
}

function GetXmlHttpObject() {
    var xmlHttp=null;
    try {  // Firefox, Opera 8.0+, Safari
        xmlHttp=new XMLHttpRequest();
    }
    catch (e) { // Internet Explorer
        try {
            xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
    }
    return xmlHttp;
}