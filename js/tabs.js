var castsTimeout;

function myhide(elem) {
	var item = document.getElementById(elem);
	if (item) {
		item.style.display = 'none';
	}
}
function myshow(elem) {
	var item = document.getElementById(elem);
	if (item) {
		item.style.display = '';
		createCookie('tls_tab_display',elem,5);
	}
}
function selecttab(tab) {
	var tabs = tab.parentNode.parentNode.childNodes;
	for (var i = 0; i < tabs.length; i++) {
		if (tabs[i].tagName == "LI") {
			tabs[i].className = "";
		}
	}
	tab.parentNode.className = "selected";
}

