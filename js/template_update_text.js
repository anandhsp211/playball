var xmlHttp;
function showHint(str) {
	if (str.length==0) { 
		var formtextvalue = xmlHttp.responseText;
		document.forms['messageform'].elements['messagebody'].value = formtextvalue;
		return;
	}
	xmlHttp=GetXmlHttpObject()
	if (xmlHttp==null)	{
		alert ("Your browser does not support AJAX!");
		return;
  	} 
		
	var url="includes/template_generator.php";
	url=url+"?q="+str;
	url=url+"&id=Text";
		
	xmlHttp.onreadystatechange = stateChanged;
	xmlHttp.open("GET",url,true);
	xmlHttp.send(null);
} 

function stateChanged() { 
	if (xmlHttp.readyState==4) { 
		var formtextvalue = xmlHttp.responseText;
		document.forms['messageform'].elements['messagebody'].value = formtextvalue;
	}
}
					
function GetXmlHttpObject() {
	var xmlHttp=null;
	try {  // Firefox, Opera 8.0+, Safari
		xmlHttp=new XMLHttpRequest();
	}
	catch (e) { // Internet Explorer
		try {
    		xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
		}
  		catch (e) {
			xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}