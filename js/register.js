<!--
function checkform ( form ) {
  if (form.firstname.value == "") {
  	alert( "Please enter your first name.\nPlease complete all compulsory fields." );
    form.firstname.focus();
    return false ;
  } else if (form.lastname.value == "") {
    alert( "Please enter your last name.\nPlease complete all compulsory fields." );
    form.lastname.focus();
    return false ;
  } else if (form.email.value == "") {
    alert( "Please enter your email address.\nPlease complete all compulsory fields." );
    form.email.focus();
    return false ;
  } else if (form.confirmemail.value == "") {
    alert( "Please enter your confirm email address value.\nPlease complete all compulsory fields." );
    form.confirmemail.focus();
    return false ;
  } else if (form.username.value == "") {
    alert( "Please enter a username.\nPlease complete all compulsory fields." );
    form.username.focus();
    return false ;
  } else if (form.mainpassword.value == "") {
    alert( "Please enter a password.\nPlease complete all compulsory fields." );
    form.mainpassword.focus();
    return false ;
  } else if (form.confirmpassword.value == "") {
    alert( "Please enter a confirm password value.\nPlease complete all compulsory fields." );
    form.confirmpassword.focus();
    return false ;
  } else if (form.email.value != form.confirmemail.value) {
  	alert( "The email address you have submitted do not match.\nPlease complete all compulsory fields." );
    form.email.focus();
    return false ;
  } else if (form.mainpassword.value != form.confirmpassword.value) {
  	alert( "The passwords you have submitted do not match.\nPlease complete all compulsory fields." );
    form.mainpassword.focus();
    return false ;
  }
  return true ;
}

