var xmlHttp;
function showHint(str,fromwhere,numfield) {
	if (str.length==0) { 
		document.getElementById("txtHint").innerHTML="";
		return;
	}
	xmlHttp=GetXmlHttpObject()
	if (xmlHttp==null)	{
		alert ("Your browser does not support AJAX!");
		return;
  	} 
	
	/* Fromwhere 2 for lead duplicates
	   Fromwhere 1 for security  templates duplicates 
	   Fromwhere 3 for lead id duplicates
	   Fromwhere 4 for product id duplicates
	*/
	
	var url="includes/name_exists.php";
	url=url+"?q="+str;
	url=url+"&sid="+Math.random();
	url=url+"&loc="+fromwhere;
	
	if (numfield == 1) {
		xmlHttp.onreadystatechange = stateChanged;
	}
	
	if (numfield == 2) {
		xmlHttp.onreadystatechange = stateChanged2;
	}
	
	xmlHttp.open("GET",url,true);
	xmlHttp.send(null);
} 

function stateChanged() { 
	if (xmlHttp.readyState==4) { 
		document.getElementById("txtHint").innerHTML = xmlHttp.responseText;
	}
}

function stateChanged2() { 
	if (xmlHttp.readyState==4) { 
		document.getElementById("txtHint2").innerHTML = xmlHttp.responseText;
	}
}
					
function GetXmlHttpObject() {
	var xmlHttp=null;
	try {  // Firefox, Opera 8.0+, Safari
		xmlHttp=new XMLHttpRequest();
	}
	catch (e) { // Internet Explorer
		try {
    		xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
		}
  		catch (e) {
			xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}