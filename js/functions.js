function eomalert() {
	alert('This item cannot be modified as it was invoiced in a closed period.\nSpeak to an administrator if a change is required.');
}

function createCookie(name,value,days) {
	var date = new Date();
	date.setTime(date.getTime()+(10*24*60*60*1000));
	var expires = "; expires="+date.toGMTString();
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name,"",-1);
}

function slideLockOn(elem1,elem2,cookieid){
	createCookie(cookieid,'on',1);
	var item1 = document.getElementById(elem1);
	var item2 = document.getElementById(elem2);
	if (item1) {
		item1.style.display = 'none';
		item2.style.display = '';
	}
}

function slideLockOff(elem1,elem2,cookieid){
	createCookie(cookieid,'',1);
	var item1 = document.getElementById(elem1);
	var item2 = document.getElementById(elem2);
	if (item1) {
		item1.style.display = 'none';
		item2.style.display = '';
	}
}

function clearForm(formIdent) { 
  var inp = document.getElementsByTagName('input');
	for(var i = 0; i < inp.length; i++) {
		if(inp[i].type == 'text') {
			inp[i].value = '';
		}
	}
  var inp = document.getElementsByTagName('select');
	for(var i = 0; i < inp.length; i++) {
		inp[i].selectedIndex=-1;
		
	}
  document.listsearch.submit();

}

function filterClear(formIdent) {
  document.listsearch.submit();
  var inp = document.getElementsByTagName('input');
	for(var i = 0; i < inp.length; i++) {
		if(inp[i].type == 'text') {
			inp[i].value = '';
		}
	}
  var inp = document.getElementsByTagName('select');
	for(var i = 0; i < inp.length; i++) {
		inp[i].selectedIndex=0
	}
  document.listsearch.submit();
}
