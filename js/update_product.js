var xmlHttp;
function updateproduct(str,recordid,complevel,lineid,fieldname,txtHintid,location) {
	if (str.length==0) { 
		document.getElementById("txtHintproduct").innerHTML= "";
		return;
	}
	xmlHttp=GetXmlHttpObject()
	if (xmlHttp==null)	{
		alert ("Your browser does not support AJAX!");
		return;
  	} 
	
		var url="includes/update_product.php";
		url=url+"?value="+str;
		url=url+"&recordid="+recordid;
		url=url+"&complevel="+complevel;
		url=url+"&lineid="+lineid;
		url=url+"&fieldname="+fieldname;
		url=url+"&location="+location;
	
	xmlHttp.onreadystatechange = stateChangedproduct;
	
	function stateChangedproduct() { 
		if (xmlHttp.readyState==4) { 
			var thestringvalue = xmlHttp.responseText
			$newstring = thestringvalue.split(":"); 
			document.getElementById("txtHintproduct_1_"+txtHintid).innerHTML = $newstring[0];
			document.getElementById("txtHintproduct_2_"+txtHintid).innerHTML = $newstring[1];
		}
	}
	
	xmlHttp.open("GET",url,true);
	xmlHttp.send(null);
} 