function ShowDivSaveChanges(sender) {
    var hiddenDataChanged = document.getElementById("hiddenDataChanged");
    if (hiddenDataChanged.value == "1") {
        var div = document.getElementById("divSaveChanges");
        div.style.display = "block";
        var hid = document.getElementById("hiddenSender");
        hid.value = sender.id;

        var mehiddenurl = document.getElementById("hiddenurl");
        mehiddenurl.value = document.getElementById(sender.id);

    } else {
        document.location.href = sender;
    }
}

function CancelSaveChanges() {
    divSaveChanges.style.display = "none";
    var hid = document.getElementById("hiddenSender");
    hid.value = "";
}

function campSessionPlaces($sessionID, $numPlaces) {
    xmlHttp = GetXmlHttpObject();
    if (xmlHttp == null) {
        alert("Your browser does not support AJAX!");
        return;
    }
    var url = "includes/update_camp_session_places.php";
    url = url + "?sessionID=" + $sessionID;
    url = url + "&numPlaces=" + $numPlaces;
    xmlHttp.onreadystatechange = stateChanged;

    function stateChanged() {
        if (xmlHttp.readyState == 4) {
            document.getElementById("campSessionsChanged").innerHTML = xmlHttp.responseText;
        }
    }

    xmlHttp.open("GET", url, true);
    xmlHttp.send(null);
}

function ImageChange(fieldtype, str, location, recordid, columnname, value_check_db) {

    console.log(fieldtype);
    console.log(str);
    console.log(location);
    console.log(recordid);
    console.log(columnname);
    console.log(value_check_db);

    var photo = document.getElementById(columnname);
    var data = new FormData();
    var url = "image_uploader.php";
    data.append('image', photo.files[0]);
    data.append('fieldtype', fieldtype);
    data.append('value', str);
    data.append('gval_21', location);
    data.append('gval_1', recordid);
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", url);
    xmlhttp.send(data);
    xmlhttp.onreadystatechange = ImageChanged;

    function ImageChanged() {
        if (xmlhttp.readyState === XMLHttpRequest.DONE) {
            var parsedText = JSON.parse(xmlhttp.responseText);
            console.log(parsedText);
            try {
                var parsedText = JSON.parse(xmlhttp.responseText);
                console.log(parsedText);
                if (parsedText && parsedText.message) {
                    var errorMessage = document.getElementById("error_mesage_" + columnname);
                    errorMessage.innerHTML = parsedText.message;
                    setTimeout(
                        function () {
                            if (parsedText.message) {
                                document.getElementById("error_mesage_" + columnname).style.display = 'none';
                            }
                        }, 3000);
                    document.getElementById(columnname).value = null;
                    if (parsedText.message && parsedText.image_path) {
                        if (fieldtype === 17) {
                            if (value_check_db) {
                                document.getElementById("stored_image_" + columnname).style.display = "none";
                                document.getElementById("image_updates_" + columnname).style.display = "block";
                            } else {
                                document.getElementById("image_updates_" + columnname).style.display = "block";
                            }
                            var img = new Image();
                            var div = document.getElementById('image_updates_' + columnname);
                            div.setAttribute('href', parsedText.image_path);
                            img.onload = function () {
                                div.innerHTML += '<img alt="columnname" id="image_updated_src" class="image_updated_src" src="' + img.src + '" />';
                            };
                            img.src = parsedText.image_path;
                            var image_val_set = document.getElementById("image_val_set_" + columnname);
                            image_val_set.setAttribute('value', parsedText.image_name);
                        } else if (fieldtype === 18) {
                            console.log(parsedText.image_path);
                            var main_div = document.getElementById('document_updated_text_' + columnname);
                            main_div.style.display = "block";
                            main_div.innerText = parsedText.image_name;
                            // console.log('document_updated_text_' + columnname);
                            main_div.setAttribute('href', parsedText.image_path);
                            var document_val_set = document.getElementById("document_val_set" + columnname);
                            document_val_set.setAttribute('value', parsedText.image_name);
                        }
                        if (!value_check_db) {
                            console.log(!value_check_db);
                            if (fieldtype === 17) {
                                console.log('in_in');
                                console.log(fieldtype);
                                document.getElementById("image_upload_" + columnname).style.display = "none";
                            } else if (fieldtype === 18) {
                                console.log('out_out');
                                console.log(fieldtype);
                                document.getElementById("document_upload_" + columnname).style.display = "none";
                            }
                        }
                    }
                    if (parsedText && parsedText.image_name) {
                        var xmlHttp;
                        xmlHttp = GetXmlHttpObject();
                        if (xmlHttp == null) {
                            alert("Your browser does not support AJAX!");
                            return;
                        }
                        var update_form_url = "includes/update_standardform.php";
                        console.log(update_form_url);
                        update_form_url = update_form_url + "?fieldtype=" + fieldtype;
                        update_form_url = update_form_url + "&value=" + parsedText.image_name;
                        update_form_url = update_form_url + "&location=" + location;
                        update_form_url = update_form_url + "&recordid=" + recordid;
                        update_form_url = update_form_url + "&columnname=" + columnname;

                        function UpdateFormStateChanged() {
                            if (xmlHttp.readyState === 4) {
                                document.getElementById("txtHint").innerHTML = xmlHttp.responseText;
                            } else {
                                if (xmlHttp.readyState === XMLHttpRequest.DONE) {
                                    console.log(xmlHttp.responseText);
                                }
                            }
                        }

                        xmlHttp.onreadystatechange = UpdateFormStateChanged;
                        xmlHttp.open("GET", update_form_url, true);
                        xmlHttp.send(null);
                    }
                }
            } catch (e) {
                console.log(e);
            }
        }
    }
}

function imageClick(val) {
    console.log(val);
    document.getElementById(val).click();
}

function DataChange(fieldtype, str, location, recordid, columnname) {
    //do a few fancy bits if the field equals a certain type
    if (columnname === 'type') {
        if (str === 'Camps') {
            document.getElementById('places').disabled = 'disabled';
            document.getElementById('places').value = '';
            document.getElementById('campsplaces').disabled = '';
            document.getElementById('campsplaces').value = '';
        } else {
            document.getElementById('places').disabled = '';
            document.getElementById('places').value = '';
            document.getElementById('campsplaces').disabled = 'disabled';
            document.getElementById('campsplaces').value = '';
        }
    }
    if (str === "") {
        str = ' ';
    }
    var hidden = document.getElementById("hiddenDataChanged");  // Updated for playball.  Undo ato activate autosave.  Also change update_standard_from.php
    hidden.value = "1";
    var xmlHttp;
    if (str.length == 0) {
        document.getElementById("txtHint").innerHTML = "";
        return;
    }
    xmlHttp = GetXmlHttpObject();
    if (xmlHttp == null) {
        alert("Your browser does not support AJAX!");
        return;
    }
    var url = "includes/update_standardform.php";
    console.log(url);
    url = url + "?fieldtype=" + fieldtype;
    url = url + "&value=" + str;
    url = url + "&location=" + location;
    url = url + "&recordid=" + recordid;
    url = url + "&columnname=" + columnname;
    xmlHttp.onreadystatechange = stateChanged;

    function stateChanged() {
        if (xmlHttp.readyState == 4) {
            console.log(xmlHttp.readyState == 4);
            document.getElementById("txtHint").innerHTML = xmlHttp.responseText;
        }
    }
    xmlHttp.open("GET", url, true);
    xmlHttp.send(null);
}

function DateTimeValue(fieldtype, str, location, recordid, columnname) {
    console.log(fieldtype);
    console.log(str);
    console.log(location);
    console.log(recordid);
    console.log(columnname);

    if (fieldtype) {
        console.log(str);
        var data = new FormData();
        var url = "date_conversion.php";
        data.append('value', str);
        data.append('fieldtype', fieldtype);
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open("POST", url);
        xmlhttp.send(data);
        xmlhttp.onreadystatechange = stateChanged;
        function stateChanged() {
            if (xmlhttp.readyState === XMLHttpRequest.DONE) {
                try {
                    var parsedText = JSON.parse(xmlhttp.responseText);

                    console.log(parsedText.value);

                    if (parsedText && parsedText.value) {
                        console.log(parsedText.value);
                        if (parsedText.value) {
                            DataChange(fieldtype, parsedText.value, location, recordid, columnname);
                        }
                    }
                } catch (e) {
                    console.log(e);
                }
            }
        }
    }
}

function GetXmlHttpObject() {
    var xmlHttp = null;
    try {  // Firefox, Opera 8.0+, Safari
        xmlHttp = new XMLHttpRequest();
    } catch (e) { // Internet Explorer
        try {
            xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
    }
    return xmlHttp;
}

function SubmitNoButton(hiddenbutton) {
    var hiddennobutton = document.getElementById("hiddenurl");
    document.location.href = hiddennobutton.value;
    return true;
}

function pasted(element, fieldtype2, location2, recordid2, columnname2) {
    setTimeout(function () {
        var thisvalue = element.value;
        var fieldtype = fieldtype2;
        var location = location2;
        var recordid = recordid2;
        var columnname = columnname2;
        DataChange(fieldtype, thisvalue, location, recordid, columnname);
    }, 0); //or 4
}
