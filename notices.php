<?php
include_once("_globalconnect.php");
if ($_GET['action'] == "markread") {
    $id = mysql_real_escape_string($_GET['id']);
    $sql = "SELECT readby FROM notices WHERE id = ".$id." LIMIT 1;";
    //echo $sql."<br>";
    $res = mysql_query($sql);
    $row = mysql_fetch_array($res);
    $readbystr = $row['readby'];
    
    if ($readbystr != "") {
        $newstr = $readbystr.$deptval.",";
    } else {
        $newstr = $deptval.",";
    }
    
    $sql = "UPDATE `notices` SET `readby` = '".mysql_real_escape_string($newstr)."' WHERE `notices`.`id` = ".$id;
    //echo $sql;
    mysql_query($sql);
    
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "includes/xhtml1-transitional.dtd">
<html>
<head>
	<title><?php echo $_SESSION['franchisedata']['name'];?> Franchise Management App</title>
        <?php 
	if (ERES > 1024) { ?>
		<style type="text/css" media="all">@import "css/style.css";</style>
	<?php 
	} else { ?>
		<style type="text/css" media="all">@import "css/style_small.css";</style>
	<?php
	}?>
</head>
    <body>
	<table cellspacing="0" cellpadding="0" border="0" width="97%">
	<tr>
		<td><img src="images/<?php echo $_SESSION['franchisedata']['logo'];?>" alt="<?php echo $_SESSION['franchisedata']['name'];?> Franchise Management App" / align="center" border="0"></td>
		<td align="right"></td>
	</tr>
	</table><br><br>
	<table cellspacing="0" cellpadding="5" border="0" width="100%">
	<tr>
		<td colspan="4" height="5"><img src="images/spacer.gif" height="5" width="1"></td>
	</tr>
	<tr>
		<td valign="top" align="right" rowspan="2" width="5%">
			<img src="images/icons/folder_edit.png"><img src="images/spacer.gif" height="1" width="10"><br>
			<img src="images/spacer.gif" height="15" width="1">
		</td>
		<td valign="top" height="5">
			<h1>System Notices</h1>
		</td>
		<td rowspan="3">&nbsp;</td>
	</tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" width="99%" class="note">
	<tr>
            <td width="2%">&nbsp;</td>
            <td class="ls_top" width="15%"><strong>Date</strong></td>
            <td class="ls_top" width="25%"><strong>Subject</strong></td>
            <td class="ls_top"><strong>Notice</strong></td>
            <td class="ls_top" width="15%"><strong>Actions</strong></td>
        </tr>
        <?php
        $sql = "SELECT * FROM `notices` WHERE active = 'Yes' ORDER BY id DESC;";
        //echo $sql;
        $res = mysql_query($sql);
        while ($row = mysql_fetch_array($res)) {
            include("includes/ls.php"); ?>
            <tr>
                <td width="2%">&nbsp;</td>
                <td class="ls_<?php echo $ls ?>"><?php echo date("d M Y", strtotime($row['datecreated']));?></td>
                <td class="ls_<?php echo $ls ?>"><?php echo $row['subject'];?></td>
                <td class="ls_<?php echo $ls ?>"><?php echo nl2br($row['notice']);?></td>
                <td class="ls_<?php echo $ls ?>">
                <?php
                $readyby = explode(",", $row['readby']);
                if (in_array($deptval, $readyby)) { ?>
                    <font style="color:white;background:#7bcf0a;padding:2px;">Read</font>
                <?php
                } else { ?>
                    <a style="color:white;background:red;padding:2px;" href="notices.php?action=markread&id=<?php echo $row['id'];?>">Mark Read</a>
                <?php
                } ?>
                </td>
            </tr>
        <?php
        } ?>
        </table>
    </body>
</html>