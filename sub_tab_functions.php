<?php
function printSessionHeader() {

	echo '<table class="note" border="0" cellspacing="2" cellpadding="2" width="100%">';
	echo "<tr>";
	echo "<td class='ls_on' align='center'><strong>Lesson Date</strong></td>";
	echo "<td class='ls_on' align='center'><strong>Day of the Week</strong></td>";
	echo "<td class='ls_on' align='center'><strong>Lesson Start Time</strong></td>";
	echo "<td class='ls_on' align='center'><strong>Lesson End Time</strong></td>";
	if ($_SESSION['franchisedata']['campdays'] == "Yes") {
		echo "<td class='ls_on' align='center'><strong>Camp Places</strong></td>";
	}
	echo "<td class='ls_on' align='center'><input type='checkbox' name='checkcssall' onclick='checkedAll(standardcssformlist);'></td>";
	echo "</tr>";
}

function getSetDaysofWeek($startDate, $endDate, $weekdayNumber) {
    $startDate = strtotime($startDate);
    $endDate = strtotime($endDate);

    $dateArr = array();

    do {
	if (date("w", $startDate) != $weekdayNumber) {
		$startDate += (24 * 3600); // add 1 day
        }
    }

    while(date("w", $startDate) != $weekdayNumber);
    while($startDate <= $endDate) {
        $dateArr[] = date('Y-m-d', $startDate);
        $startDate = strtotime("+1 week", $startDate); // add 7 days
    }
    return($dateArr);
}

function displayPayments($revfull,$thisrecordhashid) {
	echo "Hello";
}

function displaycollege($revfull,$thisrecordhashid) {
	$gval = explode(":",$revfull);
	$sql = "SELECT name, recordid FROM udf_81448138F5F163CCDBA4ACC69819F280 WHERE hashid = '".mysql_real_escape_string($gval[1])."' LIMIT 1";
	$res = mysql_query($sql);
	$row = mysql_fetch_array($res);
	$replaceArr = array("-"," ");
	$username = strtolower(str_replace($replaceArr,"",$row['name']))."-".$row['recordid'];
	$coachid = $row['recordid'];
	//echo $sql."<br>"; ?>
	<font style="font-size:12px;">College Username: <strong><?php echo $username;?></strong></font><br /><br />
	<table class="note" border="0" cellspacing="2" cellpadding="2" width="100%">
		<tr>
			<td class="ls_top" style="font-size:11px;"><strong>Program</strong></td>
			<td class="ls_top" style="font-size:11px;"><strong>Stage</strong></td>
			<td class="ls_top" style="font-size:11px;"><strong>Week</strong></td>
			<td class="ls_top" style="font-size:11px;"><strong>Result</strong></td>
			<td class="ls_top" style="font-size:11px;"><strong>Date</strong></td>
		</tr>
		<?php
		$progArr = array("twocando"=>"2 Can Do","watchmeatthree"=>"Watch Me @ 3","dinkies"=>"Dinkies","preps"=>"Preps","players"=>"Players");
		
		$sql = "SELECT * FROM `franchise_playball_college`.`quiz_responses` qr
				LEFT JOIN `franchise_playball_college`.`quiz` q ON q.id = qr.quiz_id
				WHERE coachid = '".mysql_real_escape_string($coachid)."'
				ORDER BY FIELD(program,'twocando','watchmeatthree','dinkies','preps','players'), stage, week ASC";
		//echo $sql."<br>";
		$res = mysql_query($sql);
		while ($row = mysql_fetch_array($res)) {
			include("includes/ls.php");
			echo "<tr>";
				echo "<td class='ls_".$ls."'>";
					echo $progArr[$row['program']];
				echo "</td>";
				echo "<td class='ls_".$ls."'>";
					echo ucfirst($row['stage']);
				echo "</td>";
				echo "<td class='ls_".$ls."'>";
					echo $row['week'];
				echo "</td>";
				echo "<td class='ls_".$ls."'>";
					echo $row['result']."%";
				echo "</td>";
				echo "<td class='ls_".$ls."'>";
					echo date("d M Y",strtotime($row['datecreated']));
				echo "</td>";
			echo "</tr>";
		}
		
		?>
	</table>
<?php
}

function displaysessions($revfull,$thisrecordhashid, $campsswitch) {
	$gval = explode(":",$revfull);
	echo "<a name='csgenerator'></a>"; ?>
	<script>
	function submitcssform() {
		if ((document.standardcssform.class_session_startdate.value == "") || (document.standardcssform.class_session_enddate.value == "")) {
			alert( "Please complete a start and end date." );
			return false;
		} else  {
			document.standardcssform.submit();
		}
	} </script>
	<script type="text/javascript">
	checked=false;
	function checkedAll (standardcssformlist) {
		var aa = document.getElementById('standardcssformlist');
		if (checked == false) {
			checked = true
		} else {
			checked = false
		}
		for (var i =0; i < aa.elements.length; i++) {
			aa.elements[i].checked = checked;
		}
	} </script>
	<script>
	function submitsessionform() {
		myForm = document.standardcssformlist;
		var checkbox_choice = false;
		for(i=0;i<myForm.length;i++) {
			if (myForm.elements[i].type=="checkbox" && myForm.elements[i].checked) {
				checkbox_choice = true;
			}
		}
		if (!checkbox_choice) {
			alert("Please check at least one session.")
			return false;
		}
	} </script>
	<form method="POST" onsubmit="javascript: return submitcssform(this)" name="standardcssform" action="#csgenerator">
	<table class="note" border="0" cellspacing="2" cellpadding="2" width="100%">
	<tr>
		<td class='ls_on' valign=top width="12.5%"><br><strong>Start Date:</strong><br>
			<input value="" type="text" style="text-align:right;font-family:Tahoma;font-size:14px;width:52%" name="class_session_startdate" id="class_session_startdate" readonly> &nbsp;<img src="images/icons/calendar.png" id="class_session_startdate_sd" style="cursor: pointer;" title="Date selector" >
			<script language="javascript">Calendar.setup({ inputField:"class_session_startdate",button:"class_session_startdate_sd",align:"Ll00", ifFormat:"%d-%b-%Y"  });</script>
			<a href="#csgenerator" onclick="javascript:clearText(class_session_startdate)"><img src="images/close.png" border="0"></a><br><br>
		</td>
		<td class='ls_on' valign=top width="12.5%"><br><strong>End Date:</strong><br>
			<input value="" type="text" style="text-align:right;font-family:Tahoma;font-size:14px;width:52%" name="class_session_enddate" id="class_session_enddate" readonly> &nbsp;<img src="images/icons/calendar.png" id="class_session_enddate_sd" style="cursor: pointer;" title="Date selector" >
			<script language="javascript">Calendar.setup({ inputField:"class_session_enddate",button:"class_session_enddate_sd",align:"Ll00", ifFormat:"%d-%b-%Y"  });</script>
			<a href="#csgenerator" onclick="javascript:clearText(class_session_enddate)"><img src="images/close.png" border="0"></a><br><br>
		</td>
		<td class='ls_on' valign=top width="12.5%"><br><strong>Days:</strong><br>
			<select name="setDaysofweek">
				<option value="">All Days</option>
				<?php
				$daysArray = array("Monday"=>1,"Tuesday"=>2,"Wednesday"=>3,"Thursday"=>4,"Friday"=>5,"Saturday"=>6,"Sunday"=>0);
				foreach ($daysArray AS $slDay => $slInt) {
					echo "<option value='".$slInt."'>".$slDay."</option>";
				}
				?>
			</select>
		</td>
		<td class='ls_on' valign=top width="12.5%">
			<br><strong>Exclude Lesson Time:</strong><br>
	
				<script type="text/javascript"> 
				function disablefields(){ 
					if (document.getElementById('excludetime').checked == 1) { 
						document.getElementById('sessionstarttime').readOnly = true;
						document.getElementById('sessionstarttime').value='Exclude';
						document.getElementById('sessionendtime').readOnly = true;
						document.getElementById('sessionendtime').value='Exclude';
					} else { 
						document.getElementById('sessionstarttime').readOnly = false;
						document.getElementById('sessionstarttime').value=''; 
						document.getElementById('sessionendtime').readOnly = false;
						document.getElementById('sessionendtime').value=''; 
					} 
				} 

			</script>
			<input type="checkbox" onclick="javascript:disablefields()" name="excludetime" id="excludetime">
		</td>
		<td class='ls_on' valign=top width="12.5%"><br><strong>Lesson Start Time:</strong><br>
			<input type="text" style="width:80px;" name="sessionstarttime" value="" id="sessionstarttime"><br>24-Hour Format: hh:mm<br>E.g. 9AM = 09:00 or 11PM = 23:00<br>
		</td >
		<td class='ls_on' valign=top width="12.5%"><br><strong>Lesson End Time:</strong><br>
			<input type="text" style="width:80px;" name="sessionendtime" value="" id="sessionendtime"><br>24-Hour Format: hh:mm<br>E.g. 9AM = 09:00 or 11PM = 23:00<br>
		</td >
		<?php
		if ($_SESSION['franchisedata']['campdays'] == "Yes") { ?>
			<td class='ls_on' valign=top width="12.5%"><br><strong>Camps Places:</strong><br>
			    <input type="text" name="places" id="campsplaces" style="width: 100px;"><br>
			    Enter places available per day.  Camps can have different numbers per day.
			</td>
			<?php
		} ?>
		<td class='ls_on' valign=top align="center"><br><input style="font-size:11px;font-family:Tahoma;padding:3px;" type="submit" name="GenerateSessions" value="Generate Lessons"/></td>
	</tr>
	</table>
	
	</form>

	<?php
	//print_r($_POST);
	if ($_POST['GenerateSessions'] == "Generate Lessons") {
		echo '<form method="POST" name="standardcssformlist" id="standardcssformlist" action="#csgenerator">';
		echo "<table>";
		echo "<font style='font-size:12px'>";
		echo "<font style='background:red;color:white;padding:3px;'>IMPORTANT: After `Generating Lessons` you MUST select individual lessons and click the `Create Lessons` button.</font><br><br>";

		$timestamp = strtotime($_POST['class_session_startdate']);
		$csstartdate = date('Y-m-d', $timestamp);
		$timestamp = strtotime($_POST['class_session_enddate']);
		$csenddate = date('Y-m-d', $timestamp);
		printSessionHeader();
		$totalsessions = 1;
		if ($_POST['setDaysofweek'] != "") {
		    // Fixed days of the weeks
		    $p = getSetDaysofWeek($csstartdate, $csenddate, $_POST['setDaysofweek']);
		    foreach ($p as $d) {
			    include("includes/ls.php");
			    echo "<tr>";
			    echo "<td class='ls_".$ls." style='font-size:11px;font-family:Tahoma'>".date("d-M-y",strtotime($d))."</td>";
			    $thisdate = strtotime($d);
			    echo "<td class='ls_".$ls." style='font-size:11px;font-family:Tahoma'>".jddayofweek ( cal_to_jd(CAL_GREGORIAN, date("m",$thisdate),date("d",$thisdate), date("Y",$thisdate)) , 1 )."</td>";
			    //echo "<td class='ls_".$ls."' align='center'><input style=\"width:80px;\" name='sessionstarttime[]' value='".trim($_POST['sessionstarttime'])."' type='text'></td>";
			    echo "<td class='ls_".$ls."' align='center'>".trim($_POST['sessionstarttime'])."</td>";
			    echo "<td class='ls_".$ls."' align='center'>".trim($_POST['sessionendtime'])."</td>";
			    if ($_SESSION['franchisedata']['campdays'] == "Yes") {
					echo "<td class='ls_".$ls."' align='center'><input type='text' name='places_".$totalsessions."' id='places_".$totalsessions."' value='".$_POST['places']."' /></td>";
			    }
			    echo "<td class='ls_".$ls."' align='center'><input name='sessionelement[]' id='sessionelement' value='".trim($_POST['sessionstarttime'])."||".trim($_POST['sessionendtime'])."||".$d."||".$totalsessions."' type='checkbox'></td>";
			    echo "</tr>";
			    $totalsessions++;
				
				
		    }
			
			$jscamps = ($totalsessions-1);
			
		} else {
			// All days between two dates
			$end = new DateTime($csenddate);
			$end->modify('+1 day');
			$p = new DatePeriod(
				new DateTime($csstartdate),
				new DateInterval('P1D'),
				$end
			);

			foreach ($p as $d) {
				include("includes/ls.php");
				echo "<tr>";
				echo "<td class='ls_".$ls." style='font-size:11px;font-family:Tahoma'>".$d->format('d-M-Y')."</td>";
				$thisdate = strtotime($d->format('Y-m-d'));
				echo "<td class='ls_".$ls." style='font-size:11px;font-family:Tahoma'>".jddayofweek ( cal_to_jd(CAL_GREGORIAN, date("m",$thisdate),date("d",$thisdate), date("Y",$thisdate)) , 1 )."</td>";
				echo "<td class='ls_".$ls."' align='center'>".$_POST['sessionstarttime']."</td>";
				echo "<td class='ls_".$ls."' align='center'>".$_POST['sessionendtime']."</td>";
				if ($_SESSION['franchisedata']['campdays'] == "Yes") {
					echo "<td class='ls_".$ls."' align='center'><input type='text' name='places_".$totalsessions."' value='".$_POST['places']."' /></td>";
				}
				echo "<td class='ls_".$ls."' align='center'><input name='sessionelement[]' id='sessionelement' value='".trim($_POST['sessionstarttime'])."||".trim($_POST['sessionendtime'])."||".$d->format('Y-m-d')."||".$totalsessions."' type='checkbox'></td>";
				echo "</tr>";
				$totalsessions++;
			}
		}

		
		$btnColspan = ($_SESSION['franchisedata']['campdays'] == "Yes") ? 4 : 3 ;
		echo "<tr><td><strong>Total Lessons: ".($totalsessions-1)."</strong></td><td colspan='".$btnColspan."'>&nbsp</td><td align='center' width='15%'>";
		echo "<table border='0'><tr><td><!--<input style='font-size:11px;font-family:Tahoma;padding:3px;' type='submit' id='updatesessions' name='updatesessions' value='Update Lessons' />--></td>";
		echo "<td><input style='font-size:11px;font-family:Tahoma;padding:3px;' type='submit' onclick=\"javascript: return submitsessionform(this)\" name='createsessions' value='Create Lessons' /></td</tr></table>";
		echo "<input type='hidden' name='setDaysofweek' value='".$_POST['setDaysofweek']."' /><input type='hidden' name='class_session_startdate' value='".$_POST['class_session_startdate']."'><input type='hidden' name='class_session_enddate' value='".$_POST['class_session_enddate']."'>";
		echo "</table>";
		echo "</form>";
	}

	if ($_POST['updatesessions'] == "Update Lessons") {
		echo '<form method="POST" name="standardcssformlist" id="standardcssformlist" action="#csgenerator">';
		echo "<table>";
		echo "<font style='font-size:12px'>";
		echo "To apply different times to individual sessions, make the changes in the text boxes and click 'Update Lessons'.";
		echo "Once updated, select the checkboxes for the sessions you want to save and click 'Create Lessons'.</font><br><br>";
		echo "<font style='background-color:#91c21b;padding:3px;color:white;font-size:14px;'>Sessions Updated</font><br><br>";

		$timestamp = strtotime($_POST['class_session_startdate']);
		$csstartdate = date('Y-m-d', $timestamp);
		$timestamp = strtotime($_POST['class_session_enddate']);
		$csenddate = date('Y-m-d', $timestamp);
		printSessionHeader();
		$totalsessions = 1;
		$countsessions = 0;
		if ($_POST['setDaysofweek'] != "") {
			// Fixed days of the weeks
			$p = getSetDaysofWeek($csstartdate, $csenddate, $_POST['setDaysofweek']);
			foreach ($p as $d) {
				include("includes/ls.php");
				echo "<tr>";
				echo "<td class='ls_".$ls." style='font-size:11px;font-family:Tahoma'>".date("d-M-y",strtotime($d))."</td>";
				$thisdate = strtotime($d);
				echo "<td class='ls_".$ls." style='font-size:11px;font-family:Tahoma'>".jddayofweek ( cal_to_jd(CAL_GREGORIAN, date("m",$thisdate),date("d",$thisdate), date("Y",$thisdate)) , 1 )."</td>";
				echo "<td class='ls_".$ls."' align='center'><input style=\"width:80px;\" name='sessionstarttime[]' value='".trim($_POST['sessionstarttime'][$countsessions])."' type='text'></td>";
				echo "<td class='ls_".$ls."' align='center'><input style=\"width:80px;\" name='sessionendtime[]' value='".trim($_POST['sessionendtime'][$countsessions])."' type='text'></td>";
				echo "<td class='ls_".$ls."' align='center'><input name='sessionelement[]' id='sessionelement' value='".trim($_POST['sessionstarttime'][$countsessions])."||".trim($_POST['sessionendtime'][$countsessions])."||".$d."||".$totalsessions."' type='checkbox'></td>";
				echo "</tr>";
				$totalsessions++;
				$countsessions++;
			}
		} else {
			// All days between two dates
			$end = new DateTime($csenddate);
			$end->modify('+1 day');
			$p = new DatePeriod(
				new DateTime($csstartdate),
				new DateInterval('P1D'),
				$end
			);

			foreach ($p as $d) {
				include("includes/ls.php");
				echo "<tr>";
				echo "<td class='ls_".$ls." style='font-size:11px;font-family:Tahoma'>".$d->format('d-M-Y')."</td>";
				$thisdate = strtotime($d->format('Y-m-d'));
				echo "<td class='ls_".$ls." style='font-size:11px;font-family:Tahoma'>".jddayofweek ( cal_to_jd(CAL_GREGORIAN, date("m",$thisdate),date("d",$thisdate), date("Y",$thisdate)) , 1 )."</td>";
				echo "<td class='ls_".$ls."' align='center'><input style=\"width:80px;\" name='sessionstarttime[]' value='".trim($_POST['sessionstarttime'][$countsessions])."' type='text'></td>";
				echo "<td class='ls_".$ls."' align='center'><input style=\"width:80px;\" name='sessionendtime[]' value='".trim($_POST['sessionendtime'][$countsessions])."' type='text'></td>";
				echo "<td class='ls_".$ls."' align='center'><input name='sessionelement[]' id='sessionelement' value='".trim($_POST['sessionstarttime'][$countsessions])."||".trim($_POST['sessionendtime'][$countsessions])."||".$d->format('Y-m-d')."||".$totalsessions."' type='checkbox'></td>";
				echo "</tr>";
				$totalsessions++;
				$countsessions++;
			}
			unset($countsessions);
		}


		echo "<tr><td colspan='5'>&nbsp</td><td align='center' width='15%'>";
		echo "<table><tr><td>";
		echo "<input style='font-size:11px;font-family:Tahoma;padding:3px;' type='submit' name='updatesessions' value='Update Lessons' /></td>";
		echo "<td><input style='font-size:11px;font-family:Tahoma;padding:3px;' type='submit' onclick=\"javascript: return submitsessionform(this)\" name='createsessions' value='Create Lessons' /></td</tr></table>";
		echo "<input type='hidden' name='setDaysofweek' value='".$_POST['setDaysofweek']."' /><input type='hidden' name='class_session_startdate' value='".$_POST['class_session_startdate']."'><input type='hidden' name='class_session_enddate' value='".$_POST['class_session_enddate']."'>";
		echo "</table>";
		echo "</form>";
	}

	if ($_POST['createsessions'] == "Create Lessons") {
		$sessionelement = $_POST['sessionelement'];
		foreach ($sessionelement AS $thedate) {
			$outputting = explode("||",$thedate);
			$sql = "INSERT INTO `class_sessions`
		          (`registrantid` ,`department` ,`createdby` ,`location` ,`parent_hashid` ,`session_date` ,`session_start_time`,`session_end_time` ,`places` ,`datecreated`)
			  VALUES ('".$_SESSION['registrantid']."',
				  '".$_SESSION['securityarrdeptstr']."',
				  '".$_SESSION['userid']."',
				  '".$gval[21]."',
				  '".$thisrecordhashid."',
				  '".$outputting[2]."',  '".$outputting[0]."',  '".$outputting[1]."',
				  '".mysql_real_escape_string($_POST['places_'.$outputting[3]])."',
				  '".date('Y-m-d H:i:s')."');";
			//echo $sql;
			mysql_query($sql);
		}
		echo "<font style='background:#91c21b;padding:3px;color:white;'>Lessons Created</font><br><br>";
	}

	if ($_POST['deletesessions'] == "Delete Lessons") {
		foreach ($_POST['sessionelement'] AS $val) {
			$sql = "DELETE from class_sessions WHERE id = '".$val."' LIMIT 1";
			mysql_query($sql);
		}
	}

	$sql = "SELECT id, location, parent_hashid, session_date, session_start_time, session_end_time, places FROM class_sessions
		WHERE location = '".$gval[21]."' AND parent_hashid = '".$thisrecordhashid."'
		ORDER BY session_date ASC, session_start_time ASC";
	//echo $sql."<br>";
	$resmsh = mysql_query($sql);
	$row = mysql_fetch_array($resmsh);
	if ($row['id'] != "") {
		echo '<form method="POST" name="standardcssformlist" id="standardcssformlist" action="#csgenerator">';
		printSessionHeader();
		$resmsh = mysql_query($sql);
		$pcount = 1;
		while ($row = mysql_fetch_array($resmsh)) {
			include("includes/ls.php");
			$thisdate = strtotime($row['session_date']);
			echo "<tr>";
			echo "<td with='25%' class='ls_".$ls." style='font-size:11px;font-family:Tahoma'>".date("d-M-Y",strtotime($row['session_date']))."</td>";
			echo "<td with='25%' class='ls_".$ls." style='font-size:11px;font-family:Tahoma'>".jddayofweek ( cal_to_jd(CAL_GREGORIAN, date("m",$thisdate),date("d",$thisdate), date("Y",$thisdate)) , 1 )."</td>";
			echo "<td with='25%' class='ls_".$ls." style='font-size:11px;font-family:Tahoma'>".$row['session_start_time']."</td>";
			echo "<td with='25%' class='ls_".$ls." style='font-size:11px;font-family:Tahoma'>".$row['session_end_time']."</td>";
			if ($_SESSION['franchisedata']['campdays'] == "Yes") {
				echo "<td with='25%' class='ls_".$ls." style='font-size:11px;font-family:Tahoma'><input onkeyup=\"javascript:campSessionPlaces(".$row['id'].",this.value);\" type='text' id='places_".$pcount."' name='places_".$pcount."' value='".$row['places']."'></td>";
			}
			echo "<td class='ls_".$ls."' align='center'><input name='sessionelement[]' value='".$row['id']."' type='checkbox'></td>";
			echo "</tr>";
			$pcount++;
		}
		
		$jscamps = ($pcount-1);
		
		$btnColspan2 = ($_SESSION['franchisedata']['campdays'] == "Yes") ? 5 : 4 ;
		echo "<tr><td colspan='".$btnColspan2."'>";
		echo "<span id='campSessionsChanged'></span>";
		echo "</td>";
		echo "<td align='center' width='15%'><br><input style='font-size:11px;font-family:Tahoma;padding:3px;' onclick=\"return confirm('Are you sure you want to delete these lessons?');\"  type='submit' name='deletesessions' value='Delete Lessons' /></td</tr>";
		echo "</table></form>";
	}
	
	
	if ($campsswitch == "off") { ?>
		<script>
			document.getElementById('campsplaces').disabled='disabled';
			document.getElementById('campsplaces').value='';
			
			<?php
			for ($js=1;$js<=$jscamps;$js++) { ?>
				document.getElementById('places_<?php echo $js;?>').disabled='disabled';
				document.getElementById('places_<?php echo $js;?>').value='';
			<?php
			} ?>
			
		</script>
	<?php
	} 
}

function pricingmodels($revfull,$thisrecordhashid) {
	$gval = explode(":",$revfull);
	$querymsh = "SELECT weekendprice FROM pricing WHERE location ='".$gval[21]."' AND recordid  = '".$thisrecordhashid."' ORDER BY validfrom DESC";
	//echo $querymsh;
	$resmsh = mysql_query($querymsh);
	while ($rowmsh = mysql_fetch_array($resmsh)) {
		if ($rowmsh['weekendprice'] != NULL) {
			$tableforweekend = 1;
			break;
		}
	} ?>
	&nbsp;<a href="pricing.php?id=1:<?php echo $gval[1] ?>:<?php echo $gval[21] ?>" <?php echo LFD ?>><font class="note">Add Price</font></a>
	<br><br style="line-height:5px">
		<table class="note" border="0" cellspacing="2" cellpadding="2" width="100%">
		<?php
		// We have a cross price.
		if ($gval[25] == 1) { ?>
			<tr>
				<td class="messageboard" colspan="6">There is already an active price within this date range.  Please disable this price before activating a new price.</td>
			</tr>
		<?php
		} ?>
		<tr align="center">
			<td class="ls_on"><strong>Valid From Date</strong></td>
			<td class="ls_on"><strong>Valid To Date</strong></td>
			<?php
			if ($tableforweekend == 1) { ?>
			<td class="ls_on"><strong>Weekday Price</strong></td>
			<td class="ls_on"><strong>Weekend Price</strong></td>
			<?php
			} else { ?>
			<td class="ls_on"><strong>Price</strong></td>
			<?php
			} ?>
			<td class="ls_on"><strong>Actions</strong></td>
		</tr>
	<?php
	$querymsh = "SELECT * FROM pricing WHERE location ='".$gval[21]."' AND recordid  = '".$thisrecordhashid."' ORDER BY validfrom ASC";
	$resmsh = mysql_query($querymsh);
	while ($rowmsh = mysql_fetch_array($resmsh)) {
		include("includes/ls.php");
		if ($rowmsh['isactive'] == 1) {
			$loadactive = "class=\"greenboard\"";
		} else {
			$loadactive = "class='ls_".$ls."'";
		}?>
		<tr>
			<td align="center" <?php echo $loadactive; ?>><?php echo date("d-M-Y", strtotime($rowmsh['validfrom'])) ?></td>
			<td align="center" class="ls_<?php echo $ls; ?>"><?php echo date("d-M-Y", strtotime($rowmsh['validto'])) ?></td>
			<?php
			if ($tableforweekend == 1) { ?>
				<td class="ls_<?php echo $ls; ?>" align="right"><?php echo number_format($rowmsh['price'],2) ?></td>
				<td class="ls_<?php echo $ls; ?>" align="right"><?php echo number_format($rowmsh['weekendprice'],2) ?></td>
			<?php
			} else { ?>
				<td class="ls_<?php echo $ls; ?>" align="right"><?php echo number_format($rowmsh['price'],2) ?></td>
			<?php
			} ?>
			<td class="ls_<?php echo $ls; ?>" align="center">
				<?php
				if ($rowmsh['isactive'] == 0) { ?>
					<a href="thinline.php?id=11:<?php echo $gval[1].":".$gval[2].":".$gval[3].":".$gval[4].":".$gval[5].":".$gval[6].":".$gval[7].":".$gval[8].":".$gval[9].":".$gval[10].":".$gval[11].":".$gval[12].":".$gval[13].":".$gval[14].":".$gval[15].":".$gval[16].":".$gval[17].":".$gval[18].":".$gval[19].":".$gval[20].":".$gval[21].":".$gval[22].":".$gval[23].":".$gval[24].":".$rowmsh['id']; ?>"><font class="note">Enable</font></a>
				<?php
				} else { ?>
					<a href="thinline.php?id=12:<?php echo $gval[1].":".$gval[2].":".$gval[3].":".$gval[4].":".$gval[5].":".$gval[6].":".$gval[7].":".$gval[8].":".$gval[9].":".$gval[10].":".$gval[11].":".$gval[12].":".$gval[13].":".$gval[14].":".$gval[15].":".$gval[16].":".$gval[17].":".$gval[18].":".$gval[19].":".$gval[20].":".$gval[21].":".$gval[22].":".$gval[23].":".$gval[24].":".$rowmsh['id']; ?>"><font class="note">Disable</font></a>
				<?php
				} ?>
				| <a onclick="return confirm('Are you sure you want to delete this price?');" href="thinline.php?id=13:<?php echo $gval[1].":".$gval[2].":".$gval[3].":".$gval[4].":".$gval[5].":".$gval[6].":".$gval[7].":".$gval[8].":".$gval[9].":".$gval[10].":".$gval[11].":".$gval[12].":".$gval[13].":".$gval[14].":".$gval[15].":".$gval[16].":".$gval[17].":".$gval[18].":".$gval[19].":".$gval[20].":".$gval[21].":".$gval[22].":".$gval[23].":".$gval[24].":".$rowmsh['id']; ?>"><font class="note">Delete</font></a>
			</td>
		</tr>
		<?php
		} ?>
		</table>
<?php
}

function displayMessaging($revfull,$thisrecordhashid) {
	$gval = explode(":",$revfull);?>
		<script language="javascript">
		<!--
		function toggleDisplayMessage(obj) {
			var el = document.getElementById(obj);
			if ( el.style.display != 'none' ) {
				el.style.display = 'none';
			} else {
				el.style.display = '';
			}
		}
		//-->
		</script>
		<table class="note" border="0" cellspacing="2" cellpadding="2" width="100%">
		<tr>
			<td colspan="5" style="font-size:11px;">
				<?php
				if ($gval[1] != "") { ?>
					<a rel="lyteframe" name="lyteframe" rev=" width: 785px; height: 535px; scrolling: yes;" href="messaging.php?id=2:<?php echo $gval[1];?>:<?php echo $gval[21];?>:2:email:<?php echo $gval[20];?>">Email</a>
				<?php
				}  else {
					echo "<font class='note'><a href='javascript: submitform(this);'>This record must be saved first</a></font>";
				} ?>
			</td>
		</tr>
		<tr align="center">
			<td class="ls_on"><strong>Type</strong></td>
			<td class="ls_on"><strong>To</strong></td>
			<td class="ls_on"><strong>Subject</strong></td>
			<td class="ls_on"><strong>Date & Time</strong></td>
			<td class="ls_on"><strong>Actions</strong></td>
		</tr>
		<?php
		$querymsh = "SELECT * FROM messaging WHERE recordid='".mysql_escape_string($thisrecordhashid)."' AND location = '".mysql_escape_string($gval[21])."' AND registrantid=".RID." ORDER BY datecreated DESC";
		// echo $querymsh;
		$resmsh = mysql_query($querymsh);
		while ($rowmsh = mysql_fetch_array($resmsh)) {
			include("includes/ls.php"); ?>
			<tr>
				<td align="center" class="ls_<?php echo $ls; ?>">
				<?php
				if ($rowmsh['messagetype'] == "email") {
					echo "<img src='images/icons/mail.png' alt='Email Message'>";
					$msgtype = 1;
				} else {
					echo "<img src='images/icons/mobilephone1.png' alt='Text Message'>";
					$msgtype = 2;
				}?>
				</td>
				<td class="ls_<?php echo $ls; ?>">
				<?php
					echo $rowmsh['senttoaddress'];
					if ($rowmsh['cctoaddress'] != "") {
						echo "<br><strong>CC</strong>: ".$rowmsh['cctoaddress'];
					}
					if ($rowmsh['bcctoaddress'] != "") {
						echo "<br><strong>BCC</strong>: ".$rowmsh['bcctoaddress'];
					}?>
				</td>
				<td class="ls_<?php echo $ls; ?>">&nbsp;
					<?php
					if ($msgtype == 1) {
						echo $rowmsh['emailsubject'];
					} else {
						echo substr($rowmsh['messagebody'],0,30)." ...";
					}?>
				</td>
				<td align="center" class="ls_<?php echo $ls; ?>"><?php echo date("d-M-Y H:i", strtotime($rowmsh['datecreated'])); ?></td>
				<td align="center" class="ls_<?php echo $ls; ?>">
					<font style="font-family:Tahoma;font-size:11px">
					<a href="javascript:toggleDisplayMessage('<?php echo strtoupper(md5($rowmsh['id'])) ?>')">Message</a> |
					<a onclick="return confirm('Are you sure you want to delete this message?\nThis action cannot be undone.')"href="thinline.php?id=7:<?php echo $gval[1] ?>:<?php echo strtoupper(md5($rowmsh['id'])); ?>:<?php echo $gval[3] ?>:<?php echo $gval[4] ?>:<?php echo $gval[5] ?>:<?php echo $gval[6] ?>:<?php echo $gval[7] ?>:<?php echo $gval[8] ?>:<?php echo $gval[9] ?>:<?php echo $gval[10] ?>:<?php echo $gval[11] ?>:<?php echo $gval[12] ?>:<?php echo $gval[13] ?>:<?php echo $gval[14] ?>:<?php echo $gval[15] ?>:<?php echo $gval[16] ?>:<?php echo $gval[17] ?>:<?php echo $gval[18] ?>:<?php echo $gval[19] ?>:<?php echo $gval[20] ?>:<?php echo $gval[21] ?>:<?php echo $gval[22] ?>:<?php echo $gval[23] ?>:<?php echo $gval[24] ?>">Delete</a>
				</td>
			</tr>
			<tr>
				<td colspan="5">
					<span id="<?php echo strtoupper(md5($rowmsh['id'])) ?>" style="display:none;width:100%">
					<table class="note" width="100%" cellspacing=0 cellpadding=0>
					<tr>

						<td width="98%" class="ls_<?php echo $ls; ?>"><?php echo $rowmsh['messagebody']; ?></td>
					</tr>
					</table>
					</span>
				</td>
			</tr>
<?php 	}?>
		</table>
<?php
}

function displaydocuments($revfull,$thisrecordhashid) {

//    var_dump($revfull);
//    var_dump($thisrecordhashid);

	$gval = explode(":",$revfull);?>
	<form enctype="multipart/form-data" action="document_uploader.php?id=<?php echo $revfull ?>" method="POST">
	<table class="note" border="0" cellspacing="2" cellpadding="2" width="100%">
	<tr>
		<?php
			if ($gval[1] != "") { $classsavef = "class=\"ls_top\""; } ?>
		<td colspan="4" <?php echo $classsavef ?>>
		<?php
			if ($gval[1] != "") { ?>
				<font style="font-family:Tahoma;font-size:12px">Choose a file to upload: <input name="uploadedfile" type="file" style="font-family:Tahoma;font-size:12px">
				<input type="submit" value="Upload File"> &nbsp;</font>
		<?php
			} else {
				echo "<font class='note'><a href='javascript: submitform(this);'>This record must be saved first</a></font>";
			} ?>
		</td>
	</tr>
	<?php
	if ($gval[6] == "0") {
		echo '<tr><td colspan="4"><br><font color=green>File uploaded successfully.</font></td></tr>';
	} elseif ($gval[6] == "1") {
		echo '<tr><td colspan="4"><br><font color=red>There was an error uploading the file, please try again.</font></td></tr>';
	} elseif ($gval[6] == "2") {
		echo '<tr><td colspan="4"><br><font color=red>Due to security restrictions we cannot allow files with this extension to be uploaded. <br>Please change the extension.</font></font></td></tr>';
	} elseif ($gval[6] == "3") {
		echo '<tr><td colspan="4"><br><font color=red>This file is too large to download.  Our maximum upload file size is 15 MB (Megabytes).</font></font></td></tr>';
	}  elseif ($gval[6] == "4") {
		echo '<tr><td colspan="4"><br><font color=red>A file with this name already exists, please change the filename.</font></font></td></tr>';
	}

	if ($_SESSION['securityarrdept'][0] != "") {
		$deptcounter = 0;
		foreach ($_SESSION['securityarrdept'] AS $deptval) {
			if ($deptcounter == 0) {
				$deptoperator = " AND (";
			} else {
				$deptoperator = " OR ";
			}
			$addsqldepartments .= $deptoperator." FIND_IN_SET('".$deptval."', department)";
			$deptcounter++;
		}
		$addsqldepartments .= ")";
	}

	$queryfile = "SELECT * FROM filemanager WHERE location ='".$gval[21]."' AND recordid = '".$thisrecordhashid."' AND isdeleted != 1 ".$addsqldepartments." ORDER BY filename ASC";
	//echo $queryfile;
	$resultfile = mysql_query($queryfile); ?>
	<tr align="center">
		<td colspan="4">&nbsp;</td>
	</tr>
	<tr align="center">
		<td class="ls_on"><strong>Ext</strong></td>
		<td class="ls_on"><strong>File name</strong></td>
		<td width="15%" class="ls_on"><strong>File size</strong></td>
		<td width="15%" class="ls_on"><strong>Actions</strong></td>
	</tr>
	<?php
	while ($rowfile = mysql_fetch_array($resultfile)) {
		include("includes/ls.php");?>
	<tr>
		<td class="ls_<?php echo $ls ?>" width="2%">
		<?php
		$extension = strtolower(substr($rowfile['filename'],-3));
		if (($extension == "jpg") || ($extension == "jpeg") || ($extension == "gif") || ($extension == "png") || ($extension == "psd") ) {
			echo '<img src="images/icons/photo_portrait.png" border="0">';
		} elseif (($extension == "pdf")) {
			echo '<img src="images/icons/pdf_icon.png" border="0">';
		} elseif (($extension == "txt") || ($extension == "rtf")) {
			echo '<img src="images/icons/text.png" border="0">';
		} elseif (($extension == "ppt") || ($extension == "pptx")) {
			echo '<img src="images/icons/ppt_icon.png" border="0">';
		} elseif (($extension == "xls") || ($extension == "xlsx")) {
			echo '<img src="images/icons/xls_icon.png" border="0">';
		} elseif (($extension == "doc") || ($extension == "docx")) {
			echo '<img src="images/icons/word_icon.png" border="0">';
		} elseif (($extension == "zip") || ($extension == "gzip") || ($extension == "tar")) {
			echo '<img src="images/icons/zip_icon.png" border="0">';
		} elseif (($extension == "mp3") || ($extension == "mpeg") || ($extension == "wma") || ($extension == "mp4")  || ($extension == "wma")) {
			echo '<img src="images/icons/music.png" border="0">';
		} else {
			echo '<img src="images/icons/text.png" border="0">';
		} ?>
		</td>
		<td class="ls_<?php echo $ls ?>">
			<a href="<?php echo $_SESSION['documentstorepath']; ?><?php echo $gval[21] ?>/<?php echo $gval[1] ?>/<?php echo $rowfile['filename'] ?>"><?php echo $rowfile['filename'] ?></a>
		</td>
		<td align="right" class="ls_<?php echo $ls ?>"><?php echo number_format(($rowfile['filesize']/1000),2) ?> Kb</td>
		<td class="ls_<?php echo $ls ?>" align="center">
			<a href="<?php echo $_SESSION['documentstorepath']; ?><?php echo $gval[21] ?>/<?php echo $gval[1] ?>/<?php echo $rowfile['filename'] ?>"><font style="font-family:Tahoma;font-size:11px">Download</font></a>
			| <a onclick="return confirm('Are you sure you want to delete this file?')" href="thinline.php?id=3<?php echo ":".$gval[1].":".$gval[2].":".$gval[3].":".$gval[4].":".$gval[5].":".$gval[6].":".$gval[7].":".$gval[8].":".$gval[9].":".$gval[10].":".$gval[11].":".$gval[12].":".$gval[13].":".$gval[14].":".$gval[15].":".$gval[16].":".$gval[17].":".$gval[18].":".$gval[19].":".$gval[20].":".$gval[21].":".$gval[22].":".$gval[23].":".$gval[24] ?>:<?php echo $rowfile['filename']; ?>"><font style="font-family:Tahoma;font-size:11px">Delete</font></a>
		</td>
	</tr>
	<?php
	} ?>
	</table></form>
<?php
} ?>

<?php
function displayactivities($revfull,$thisrecordhashid) {
	$querygetact = "SELECT hashid FROM menu_tabs WHERE type = 'Activities' AND isactive = 1 AND registrantid = ".RID." LIMIT 1";
	//echo $querygetact;
	$resultact = mysql_query($querygetact);
	$rowact = mysql_fetch_array($resultact);
	$store_activities_id = $rowact['hashid'];
	$gval = explode(":",$revfull);?>
	<script language="javascript">
	<!--
	function toggleNote(obj) {
		var el = document.getElementById(obj);
		if ( el.style.display != 'none' ) {
			el.style.display = 'none';
		} else {
		el.style.display = '';
		}
	}
	//-->
	</script>
	<table class="note" border="0" cellspacing="2" cellpadding="2" width="100%">
	<tr>
		<td colspan="3">
		<font class="note" color="#cccccc">
		<?php
		$selectimages = '&nbsp;<img src="images/selected.gif">'; ?>
		<?php
			if ($gval[1] != "") {
				// I am passing gval20 to activate the activities tab.  But tab id is now postion 21 and the subtab is 22.  gval 1 is the recordid?>
		  	<a href="thinline.php?id=<?php echo ":".$gval[1].":".$gval[2].":".$gval[3].":".$gval[4].":".$gval[5].":".$gval[6].":".$gval[7].":".$gval[8].":".$gval[9].":".$gval[10].":".$gval[11].":".$gval[12].":".$gval[13].":".$gval[14].":".$gval[15].":".$gval[16].":".$gval[17].":".$gval[18].":".$gval[19].":".$store_activities_id.":".$gval[20].":".$gval[21].":".$gval[23].":".$gval[24] ?>">New</a>
		<?php
			} else { ?>
			This record must be saved first
		<?php
			} ?>
			</font>
		</td>
		<td colspan="3">&nbsp;</td>
		<td colspan="4" align="right" class="note">
		<?php if ($_COOKIE['act_tab_display'] == "ALL") { echo $selectimages;	} ?>
			<a onclick="javascript:createCookie('act_tab_display','ALL',5);" href="thinline.php?id=<?php echo $gval[0].":".$gval[1].":".$gval[2].":".$gval[3].":"; ?>ALL<?php echo ":".$gval[5].":".$gval[6].":".$gval[7].":".$gval[8].":".$gval[9].":".$gval[10].":".$gval[11].":".$gval[12].":".$gval[13].":".$gval[14].":".$gval[15].":".$gval[16].":".$gval[17].":".$gval[18].":".$gval[19].":".$gval[20].":".$gval[21].":".$gval[22].":".$gval[23].":".$gval[24] ?>#subtabs_section">All</a> |
		<?php if ($_COOKIE['act_tab_display'] == "INC") { echo $selectimages;	} ?>
			<a onclick="javascript:createCookie('act_tab_display','INC',5);" href="thinline.php?id=<?php echo $gval[0].":".$gval[1].":".$gval[2].":".$gval[3].":"; ?>INC<?php echo ":".$gval[5].":".$gval[6].":".$gval[7].":".$gval[8].":".$gval[9].":".$gval[10].":".$gval[11].":".$gval[12].":".$gval[13].":".$gval[14].":".$gval[15].":".$gval[16].":".$gval[17].":".$gval[18].":".$gval[19].":".$gval[20].":".$gval[21].":".$gval[22].":".$gval[23].":".$gval[24] ?>#subtabs_section">Incompleted</a> |
		<?php if ($_COOKIE['act_tab_display'] == "COMP") { echo $selectimages;	} ?>
			<a onclick="javascript:createCookie('act_tab_display','COMP',5);" href="thinline.php?id=<?php echo $gval[0].":".$gval[1].":".$gval[2].":".$gval[3].":"; ?>COMP<?php echo ":".$gval[5].":".$gval[6].":".$gval[7].":".$gval[8].":".$gval[9].":".$gval[10].":".$gval[11].":".$gval[12].":".$gval[13].":".$gval[14].":".$gval[15].":".$gval[16].":".$gval[17].":".$gval[18].":".$gval[19].":".$gval[20].":".$gval[21].":".$gval[22].":".$gval[23].":".$gval[24] ?>#subtabs_section">Completed</a>
		</td>
	</tr>
	<tr align="center">
		<td class="ls_on"><strong>Subject</strong></td>
		<td class="ls_on"><strong>Allocated To</strong></td>
		<td class="ls_on" colspan="2"><strong>Start</strong></td>
		<td class="ls_on" colspan="2"><strong>End</strong></td>
		<td class="ls_on"><strong>Status</strong></td>
		<td class="ls_on" width="18%"><strong>Actions</strong></td>
	</tr>
	<?php
	if (($gval[4] == "INC") || ($_COOKIE['act_tab_display'] == "INC")) {
		$fstatus = "AND activitystatus != 'Completed'";
	} elseif (($gval[4] == "COMP") || ($_COOKIE['act_tab_display'] == "COMP")) {
		$fstatus = "AND activitystatus = 'Completed'";
	}
	$queryactivity = "SELECT * FROM activities act, activity_relations actr
					WHERE recordid  = '".$thisrecordhashid."'
					AND act.hashid = actr.activityhashid
					AND ((actr.menutabid = '".$gval[20]."' AND location = '".$gval[21]."' AND privacy = 'public') OR
					(actr.menutabid = '".$gval[20]."' AND location = '".$gval[21]."' AND recordid = '".$gval[1]."' AND privacy = 'private' AND (allocatedto = ".UID." OR act.userid = ".UID.")))
					".$fstatus." ORDER BY datestart ASC";
	//echo $queryactivity;
	$resultactivity = mysql_query($queryactivity);
	$modalcounter = 3;
	$modalcounter_2 = 30000;
	while ($rowactivities = mysql_fetch_array($resultactivity)) {
		include("includes/ls.php"); ?>
		<tr>
			<td class="ls_<?php echo $ls; ?>"><a href="thinline.php?id=<?php echo $gval[0] ?>:<?php echo $gval[1] ?>:::<?php echo $gval[4] ?>:<?php echo $rowactivities['hashid'] ?>:<?php echo $gval[6] ?>:<?php echo $gval[7] ?>:<?php echo $gval[8] ?>:<?php echo $gval[9] ?>:<?php echo $gval[10] ?>:<?php echo $gval[11] ?>:::::::::<?php echo $store_activities_id ?>:<?php echo $gval[20] ?>:<?php echo $gval[21] ?>:<?php echo $gval[22] ?>"><?php echo $rowactivities['subject'] ?></a></td>
			<?php
			$queryactivity2 = "SELECT firstname, lastname FROM users WHERE id=".$rowactivities['allocatedto'];
			//echo $queryactivity;
			$resultactivity2 = mysql_query($queryactivity2);
			$rowactivities2 = mysql_fetch_array($resultactivity2);?>
			<td class="ls_<?php echo $ls; ?>"><?php echo $rowactivities2['firstname'] ?> <?php echo $rowactivities2['lastname'] ?></td>
			<td width="11%" align="center" class="ls_<?php echo $ls; ?>">
			<?php
				echo date("d-M-Y", strtotime($rowactivities['datestart'])); ?>
			</td>
			<td class="ls_<?php echo $ls; ?>">
			<?php
				echo date("H:i", strtotime($rowactivities['datestart'])); ?>
			</td>
			<td width="11%" align="center" class="ls_<?php echo $ls; ?>">
			<?php
				echo date("d-M-Y", strtotime($rowactivities['dateend'])); ?>
			</td>
			<td class="ls_<?php echo $ls; ?>">
			<?php
				echo date("H:i", strtotime($rowactivities['dateend'])); ?>
			</td>
			<td align="center" class="ls_<?php echo $ls; ?>">
			<?php echo $rowactivities['activitystatus']; ?>
			</td>
			<td width="15%" align="center" class="ls_<?php echo $ls; ?>">
				<font style="font-family:Tahoma;font-size:11px">
				<?php
				if (!$rowactivities['addtocalval'] == 1) { ?>
					<a href="activity_quicknotes.php?id=6:<?php echo $getidvalues[1].":".$getidvalues[2].":".$getidvalues[3].":".$getidvalues[4].":"; ?><?php echo $rowactivities['hashid'] ?>:<?php echo $gval[20] ?>" <?php echo LFD ?>>Add Note</a> |
					<?php
					$sqltn = "SELECT COUNT(id) AS TotalNotes FROM activities_notes WHERE activityhashid = '".$rowactivities['hashid']."'";
					$restn = mysql_query($sqltn);
					$rowtn = mysql_fetch_array($restn);?>
					<a href="javascript:toggleNote('note<?php echo $rowactivities['hashid'] ?>')">Notes [<?php echo $rowtn['TotalNotes']; ?>]</a>
					<?php if ($rowactivities['activitystatus'] != "Completed") { ?>
					| <a href="thinline.php?id=4:<?php echo $gval[1].":".$gval[2].":".$gval[3].":".$gval[4].":".$rowactivities['hashid'].":".$gval[6].":".$gval[7].":".$gval[8].":".$gval[9].":".$gval[10].":".$gval[11].":".$gval[12].":".$gval[13].":".$gval[14].":".$gval[15].":".$gval[16].":".$gval[17].":".$gval[18].":".$gval[19].":".$gval[20].":".$gval[21].":".$gval[22].":".$gval[23].":".$gval[24] ?>#subtabs_section">Complete</a>
					<?php } else { ?>
					| Completed
					<?php
						  }
				} else { ?>
					Do not modify this placeholder activity.
				<?php
				} ?>
				</font>
			</td>
		</tr>
		<tr>
			<td colspan="10">
				<span id="note<?php echo $rowactivities['hashid'] ?>" style="display:none;width:100%">
				<table class="note" cellpadding="2" cellspacing="2" border="0" width="100%">
				<tr align="center">
					<td width="2%">&nbsp;<img src="images/spacer.gif" width="30" height="1"></td>
					<td width="12%" class="ls_on"><strong>Added By</strong></td>
					<td class="ls_on"><strong>Date</strong></td>
					<td class="ls_on"><strong>Note</strong></td>
					<td class="ls_on" width="10%"><strong>Actions</strong></td>
				</tr>
				<?php
				$sqltn = "SELECT * FROM activities_notes WHERE activityhashid = '".$rowactivities['hashid']."' ORDER BY datecreated DESC";
				// echo $sqltn;
				$restn = mysql_query($sqltn);
				while ($rowtn = mysql_fetch_array($restn)) { ?>
				<tr>
					<td width="2%">&nbsp;<img src="images/spacer.gif" width="30" height="1"></td>
					<?php
					$queryactivity2 = "SELECT firstname, lastname FROM users WHERE id = ".$rowtn['userid'];
					//echo $queryactivity;
					$resultactivity2 = mysql_query($queryactivity2);
					$rowactivities2 = mysql_fetch_array($resultactivity2);?>
					<td class="ls_off"><?php echo $rowactivities2['firstname'] ?> <?php echo $rowactivities2['lastname'] ?></td>
					<td width="17%" class="ls_off">
					<?php
						echo date("d-M-Y H:i", strtotime($rowtn['datecreated'])); ?>
					</td>
					<td class="ls_off"><?php echo str_replace("\r","<br>",$rowtn['note']); ?></td>
					<td class="ls_off" align="center"><font class="note"><a href="thinline.php?id=8:<?php echo $gval[1] ?>:<?php echo strtoupper(md5($rowtn['id'])).":".$gval[3].":".$gval[4].":".$gval[5].":".$gval[6].":".$gval[7].":".$gval[8].":".$gval[9].":".$gval[10].":".$gval[11].":".$gval[12].":".$gval[13].":".$gval[14].":".$gval[15].":".$gval[16].":".$gval[17].":".$gval[18].":".$gval[19].":".$gval[20].":".$gval[21].":".$gval[22].":".$gval[23].":".$gval[24] ?>">Delete</a></font></td>
				</tr>
				<?php
				} ?>
			</table><br>
			</span>
		</td>
	</tr>
	<?php
	}?>
	</table><br><br>
<?php
}

function displaynotes($revfull,$thisrecordhashid) {
	$gval = explode(":",$revfull);?>
	<table class="note">
	<tr>
		<td>
			<font style="font-family:Tahoma;font-size:11px">
			<?php
			if ($gval[1] != "") { ?>
				<a href="notes.php?id=1:<?php echo $gval[1] ?>:<?php echo $gval[21] ?>" <?php echo LFD ?>>New</a><br>
			<?php
			} else { ?>
				<font class='note'><a href='javascript: submitform(this);'>This record must be saved first</a></font>
				<?php
			} ?>
		</td>
	</tr>
	</table>
	<table class="note" cellpadding="2" cellspacing="2" border="0" width="100%">
		<tr align="center">
					<td width="12%" class="ls_on"><strong>Added By</strong></td>
					<td width="12%" class="ls_on"><strong>Date</strong></td>
					<td class="ls_on"><strong>Note</strong></td>
					<td class="ls_on" width="10%"><strong>Actions</strong></td>
				</tr>
				<?php
				$sqltn = "SELECT * FROM notes WHERE recordid = '".$gval[1]."' AND menutabid = '".$gval[21]."' ORDER BY datecreated DESC";
				// echo $sqltn;
				$restn = mysql_query($sqltn);
				while ($rowtn = mysql_fetch_array($restn)) { ?>
				<tr>
					<?php
					$queryactivity2 = "SELECT firstname, lastname FROM users WHERE id = ".$rowtn['userid'];
					//echo $queryactivity;
					$resultactivity2 = mysql_query($queryactivity2);
					$rowactivities2 = mysql_fetch_array($resultactivity2);?>
					<td class="ls_off"><?php echo $rowactivities2['firstname'] ?> <?php echo $rowactivities2['lastname'] ?></td>
					<td width="17%" class="ls_off">
					<?php
						echo date("d-M-Y H:i", strtotime($rowtn['datecreated'])); ?>
					</td>
					<td class="ls_off"><?php echo str_replace("\r","<br>",$rowtn['note']); ?></td>
					<?php
					if (UID == $rowtn['userid']) {?>
						<td class="ls_off" align="center"><font class="note"><a href="thinline.php?id=16:<?php echo $gval[1] ?>:<?php echo strtoupper(md5($rowtn['id'])).":".$gval[3].":".$gval[4].":".$gval[5].":".$gval[6].":".$gval[7].":".$gval[8].":".$gval[9].":".$gval[10].":".$gval[11].":".$gval[12].":".$gval[13].":".$gval[14].":".$gval[15].":".$gval[16].":".$gval[17].":".$gval[18].":".$gval[19].":".$gval[20].":".$gval[21].":".$gval[22].":".$gval[23].":".$gval[24] ?>">Delete</a></font></td>
					<?php
					} else { ?>
						<td class="ls_off" align="center"><font class="noteg">Delete</font></td>
					<?php
					} ?>
				</tr>
				<?php
				} ?>
			</table><br>

<?php
}

function displayaaddresses($revfull) {
	$gval = explode(":",$revfull);?>
	<table class="note" border="0" cellspacing="2" cellpadding="3" width="100%">
	<tr>
		<td><font class="note" color="#cccccc">
			<?php
			if ($gval[1] != "") { ?>
				<a href="address_manager.php?id=<?php echo $gval[0].":".$gval[1].":".$gval[2].":".$gval[3].":".$gval[4].":".$gval[5].":".$gval[6].":".$gval[7].":".$gval[8].":".$gval[9].":".$gval[10].":".$gval[11].":".$gval[12].":".$gval[13].":".$gval[14].":".$gval[15].":".$gval[16].":".$gval[17].":".$gval[18].":".$gval[19].":".$gval[20].":".$gval[21].":".$gval[22].":".$gval[23].":".$gval[24] ?>" <?php echo LFD ?>>New</a>
			<?php
			} else { ?>
				This record must be saved first
			<?php
			} ?>
			</font>
		</td>
	<tr>
	<tr>
		<td class="ls_on" align="center"><strong>Address Line 1</strong></td>
		<td class="ls_on" align="center"><strong>Address Line 2</strong></td>
		<td class="ls_on" align="center"><strong>State/Province/County</strong></td>
		<td class="ls_on" align="center"><strong>Zip/Post Code</strong></td>
		<td class="ls_on" align="center"><strong>Country</strong></td>
		<td class="ls_on" align="center"><strong>Type</strong></td>
		<td class="ls_on" align="center"><strong>Actions</strong></td>
	</tr>
	<?php
	$sql = "SELECT * FROM  address_info WHERE location ='".$gval[21]."' AND recordid ='".$gval[1]."' AND registrantid =".RID;
	//echo $sql;
	$res = mysql_query($sql);
	while ($row = mysql_fetch_array($res)) {
		include("includes/ls.php");
	 ?>
	<tr>
		<td class="ls_<?php echo $ls ?>"><?php echo $row['addressline1'] ?>&nbsp;</td>
		<td class="ls_<?php echo $ls ?>"><?php echo $row['addressline2'] ?>&nbsp;</td>
		<td class="ls_<?php echo $ls ?>"><?php echo $row['city'] ?>&nbsp;</td>
		<td class="ls_<?php echo $ls ?>"><?php echo $row['postcode'] ?>&nbsp;</td>
		<td class="ls_<?php echo $ls ?>"><?php echo $row['country'] ?>&nbsp;</td>
		<td class="ls_<?php echo $ls ?>"><?php echo $row['addresstype'] ?>&nbsp;</td>
		<td align="center" class="ls_<?php echo $ls ?>">
			<font class="note"><a href="address_manager.php?id=<?php echo $gval[0].":".$gval[1].":".$gval[2].":".$gval[3].":".$gval[4].":".$row['hashid'].":".$gval[6].":".$gval[7].":".$gval[8].":".$gval[9].":".$gval[10].":".$gval[11].":".$gval[12].":".$gval[13].":".$gval[14].":".$gval[15].":".$gval[16].":".$gval[17].":".$gval[18].":".$gval[19].":".$gval[20].":".$gval[21].":".$gval[22].":".$gval[23].":".$gval[24] ?>" <?php echo LFD ?>>Edit</a> |
			<a onclick="return confirm('Are you sure you want to delete this address?')" href="thinline.php?id=5<?php echo ":".$gval[1].":".$gval[2].":".$gval[3].":".$gval[4].":".$row['hashid'].":".$gval[6].":".$gval[7].":".$gval[8].":".$gval[9].":".$gval[10].":".$gval[11].":".$gval[12].":".$gval[13].":".$gval[14].":".$gval[15].":".$gval[16].":".$gval[17].":".$gval[18].":".$gval[19].":".$gval[20].":".$gval[21].":".$gval[22].":".$gval[23].":".$gval[24] ?>">Delete</a></font>
		</td>
	</tr>
	<?php
	} ?>
	</table>
<?php
}?>
