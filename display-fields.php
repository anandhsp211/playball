<?php
function displayFields ($fieldType,$fieldLabel,$fieldValue,$dbfieldlabel,$counter,$udfhashid,$recordhashid,$linkedid,$location,$objectmasterid) {
    if ($fieldType == 2) {
        // drop down select
        if (($rowresgetvals[$dbfieldlabel] == "") && ($recSavedState != 1)) {
            $rowresgetvals[$dbfieldlabel] = date("Y");
        }
        
        include_once 'includes/dropdown_generator.php';
        generateUDFselect($fieldLabel,$udfhashid,$fieldValue,$dbfieldlabel."_".$counter,$objectmasterid, $revfull, "" , $canEditValues, $duplicator = 1,$location,$recordhashid);
        
    } elseif ( ($fieldType == 8) && (($fieldLabel != "excludestart") && ($fieldLabel != "excludeend")) ) {
        $fieldValue = date("d-M-Y",strtotime($fieldValue));
        if (($fieldValue == "01-Jan-1970") || ($fieldValue == "30-Nov-1999") || ($fieldValue == "01-Jan-1970") || ($fieldValue == "01-Jan-1971") || ($fieldValue == "30-Nov--0001")) {
            $fieldValue = "";
        } ?>
        <input readonly <?php echo "onchange=\"javascript:DataChange(8,this.value,'".$location."','".$recordhashid."','".$dbfieldlabel."');\" onPaste=\"pasted(this,6,'".$location."','".$recordhashid."','".$dbfieldlabel."')\" "; ?> type="text" style="text-align:right;font-family:Tahoma;font-size:14px;width:100px;" name="<?php echo $dbfieldlabel."_".$counter;?>" id="<?php echo $dbfieldlabel."_".$counter;?>" value="<?php echo $fieldValue; ?>"> &nbsp;<img src="images/icons/calendar.png" id="<?php echo md5($dbfieldlabel)."_".$counter;?>" style="cursor: pointer;" title="Date selector" ><script language="javascript">Calendar.setup({ inputField:"<?php echo $dbfieldlabel."_".$counter;?>",button:"<?php echo md5($dbfieldlabel)."_".$counter;?>",align:"Ll00", ifFormat:"%d-%b-%Y"  });</script>
        <a href="#" onclick="javascript:clearText(<?php echo $dbfieldlabel."_".$counter;?>)"><img src="images/close.png" border="0"></a>
    <?php
    } elseif ( ($fieldType == 8) && (($fieldLabel == "excludestart") || ($fieldLabel == "excludeend")) ) {
        if (($fieldValue == "01-Jan-1970") || ($fieldValue == "30-Nov-1999") || ($fieldValue == "01-Jan-1970") || ($fieldValue == "01-Jan-1971") || ($fieldValue == "30-Nov--0001")) {
            $fieldValue = "";
        } ?>
        <input readonly type="text" style="text-align:right;font-family:Tahoma;font-size:12px;width:100px;" name="<?php echo $dbfieldlabel;?>" id="<?php echo $dbfieldlabel;?>" value=""> &nbsp;<img src="images/icons/calendar.png" id="<?php echo md5($dbfieldlabel);?>" style="cursor: pointer;" title="Date selector" ><script language="javascript">Calendar.setup({ inputField:"<?php echo $dbfieldlabel;?>",button:"<?php echo md5($dbfieldlabel);?>",align:"Ll00", ifFormat:"%d-%b-%Y"  });</script>
        <input type="hidden" name="fieldnametype" value="<?php echo $fieldLabel;?>" />
        <a href="#" onclick="javascript:clearText(<?php echo $dbfieldlabel."_".$counter;?>)"><img src="images/close.png" border="0"></a>
    <?php
    } elseif ($fieldType == 10) {
        $getlrec = "SELECT mst.menu_tabhashid AS menutabhashid, udf.label AS columnname, rx.parenttable AS parenttable, rv.parent_recordid AS recordid
                FROM udf_definitions udf, relations_matrix rx, relations_values rv, menusub_tabs mst
                WHERE rx.id = ".$linkedid."
                AND rx.id = rv.matrix_id
                AND rx.udf_columnid = udf.hashid
                AND rv.recordid = '".$recordhashid."'
                AND rx.parenttable = mst.hashid
                AND rx.registrantid = ".RID."
                ORDER BY rv.id DESC
                LIMIT 1";
        //echo $getlrec."<br><br>";
        $resultlrec = mysql_query($getlrec);
        $rowlrec = mysql_fetch_array($resultlrec);
        // Declare
        $lreccolumnname = $rowlrec['columnname'];
        $lrectable = $rowlrec['parenttable'];
        $lrecrecordid = $rowlrec['recordid'];
        $lrecmenutab = $rowlrec['menutabhashid'];
        
        $getlrec = "SELECT hashid, ".columnnames($lreccolumnname)."
                    FROM udf_".$lrectable."
                    WHERE hashid = '".$lrecrecordid."'
                    AND registrantid = ".RID." LIMIT 1";
        //echo $getlrec."<br><br>";
        $resultlrec = mysql_query($getlrec);
        $rowlrec = mysql_fetch_array($resultlrec);
        $relatedvalue = $rowlrec[columnnames($lreccolumnname)];
        
        $result = "<input style='font-size:14px;width:125px' readonly type=\"text\" name='".$columnName."_".$counter."' value='".$relatedvalue."' />";
        $result .= "<br /> <a href='datalink.php?id=2:".$recordhashid."::::::::::::::::::::".$location."::::".$linkedid.":".$dbfieldlabel.":".$recordissaved."' ".LFD." id='assign_".$counter."'><font class=\"note\">assign</font></a>";
        return $result;
        
    } elseif ($fieldType == 4) {
        return "<textarea onkeyup=\"javascript:DataChange(6,this.value,'".$location."','".$recordhashid."','".$dbfieldlabel."');\" onPaste=\"pasted(this,6,'".$location."','".$recordhashid."','".$dbfieldlabel."')\" style=\"width:190px;height:70px;font-size:14px;\" name=\"".$dbfieldlabel."_".$counter."\">".$fieldValue."</textarea>";
    } else {
        
        //if (($fieldType == 1) || ($fieldType == 12) || ($fieldType == 13) || ($fieldType == 14) || ($fieldType == 7)  || ($fieldType == 5) || ($fieldType == 6)) {
        
        if ($fieldLabel == "Class Name")  {
            return "<textarea onkeyup=\"javascript:DataChange(6,this.value,'".$location."','".$recordhashid."','".$dbfieldlabel."');\" onPaste=\"pasted(this,6,'".$location."','".$recordhashid."','".$dbfieldlabel."')\" style='width:190px;height:70px;'>".$fieldValue."</textarea>";
        } else {
            return "<input onkeyup=\"javascript:DataChange(6,this.value,'".$location."','".$recordhashid."','".$dbfieldlabel."');\" onPaste=\"pasted(this,6,'".$location."','".$recordhashid."','".$dbfieldlabel."')\" style=\"width:175px;font-size:14px;\" type=\"text\" name=\"".$dbfieldlabel."_".$counter."\" value=\"".$fieldValue."\" >";
        }
        
    //} else
        
    }
}
?>
