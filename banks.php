<?php 
include_once("_globalconnect.php");

if ((($_POST['account_name'] == "") || ($_POST['starting_date'] == "") || ($_POST['opening_balance'] == "")) && (($_POST['updatesave'] == 1) || ($_POST['updatesave'] == 2))) {
	$error_message = "Please complete all required fields";
} else {
	if ($_POST['updatesave'] == 1) {
		$sql = "SELECT code FROM transaction_types WHERE code = '".$_POST['code']."' AND md5(id) != '".$_POST['hashid']."'";
		// echo $sql;
		$res = mysql_query($sql);
		$row = mysql_fetch_array($res);
		if (!$row['code']) {
			// Check the default account
			if ($_POST['defaultaccount'] == "checkboxon") {
				$sql = "UPDATE `bank_accounts` SET `defaultaccount` = '';";
				mysql_query($sql) or die ('Dead');
			}
			$startingmonth = explode("-",substr($_POST['starting_date'],0,11));
			$startdatefinal = $startingmonth[2]."-".convertDate($startingmonth[1])."-".$startingmonth[0];
			$query = "UPDATE `bank_accounts` SET `accountname` = '".$_POST['account_name']."',
					`starting_date` = '".$startdatefinal."',
					`opening_balance` = '".$_POST['opening_balance']."',
					`payment_methods_default_invoices` = '".$_POST['invoice_method']."' ,
					`payment_methods_default_purchases` = '".$_POST['purchase_method']."',
					`nominal_code` = '".$_POST['nominal_code']."' ,
					`defaultaccount` = '".$_POST['defaultaccount']."' 
					WHERE md5(id) = '".$_POST['hashid']."' LIMIT 1";
			// echo $query."<br><br>";
			mysql_query($query) Or Die ("Cannot submit entry!");
			$onload = 'onload="window.parent.location = window.parent.location;self.close();return false;"';
		} else {
			$error_message = "Code already in use, please choose another code.";
		}
	} elseif ($_POST['updatesave'] == 2) {
		$sql = "SELECT code FROM transaction_types WHERE code = '".$_POST['nominal_code']."'";
		// echo $sql;
		$res = mysql_query($sql);
		$row = mysql_fetch_array($res);
		if (!$row['code']) {
			// Check the default account
			if ($_POST['defaultaccount'] == "checkboxon") {
				$sql = "UPDATE `bank_accounts` SET `defaultaccount` = '';";
				mysql_query($sql) or die ('Dead');
			}
			$startingmonth = explode("-",substr($_POST['starting_date'],0,11));
			$startdatefinal = $startingmonth[2]."-".convertDate($startingmonth[1])."-".$startingmonth[0];
			$query = "INSERT INTO `bank_accounts` (`registrantid` , `accountname` , `starting_date` , `opening_balance` , `payment_methods_default_invoices` , `payment_methods_default_purchases` , `nominal_code`, `defaultaccount` ) 
						VALUES (
							'".RID."', 
							'".$_POST['account_name']."', 
							'".$startdatefinal."', 
							'".$_POST['opening_balance']."', 
							'".$_POST['invoice_method']."', 
							'".$_POST['purchase_method']."',
							'".$_POST['nominal_code']."',
							'".$_POST['defaultaccount']."'
						);";
			// echo $query."<br><br>";
			mysql_query($query) Or Die ("Cannot submit entry!");
			$query = "INSERT INTO `transaction_types` ( `codetype` , `code` , `name`  ) 
						VALUES ( 'Turnover', '".$_POST['nominal_code']."', '".$_POST['account_name']."');";
			// echo $query."<br><br>";
			mysql_query($query) Or Die ("Cannot submit entry!");
			$onload = 'onload="window.parent.location = window.parent.location;self.close();return false;"';
		} else {
			$error_message = "Code already in use, please choose another code.";
		}
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "includes/xhtml1-transitional.dtd">
<html>
<head>
	<title>Link Records</title>
	<?php 
	if (ERES > 1024) { ?>
		<style type="text/css" media="all">@import "css/style.css";</style>
	<?php 
	} else { ?>
		<style type="text/css" media="all">@import "css/style_small.css";</style>
	<?php
	}?>		
	<style type="text/css" media="all">@import "css/calendar_pop.css";</style>
	<script type='text/javascript' src='js/calendar_pop.js'></script>
</head>

<body <?php echo $onload ?>>
<form method="POST" name="banks_manager" action="<?php echo $_SERVER['PHP_SELF'] ?>?id=<?php echo ":".$gval[1].":".$gval[2].":".$gval[3].":".$gval[4].":".$gval[5].":".$gval[6].":".$gval[7].":".$gval[8].":".$gval[9].":".$gval[10].":".$gval[11].":".$gval[12].":".$gval[13].":".$gval[14].":".$gval[15].":".$gval[16].":".$gval[17].":".$gval[18].":".$gval[19].":".$gval[20].":".$gval[21].":".$gval[22].":".$gval[23].":".$gval[24].":".$gval[25]; ?>">
	<table cellspacing="0" cellpadding="0" border="0" width="97%">
	<tr>
		<td><img src="images/<?php echo $_SESSION['franchisedata']['logo'];?>" alt="" / align="center" border="0"></td>
	</tr>
	</table><br>
	<table cellspacing="0" cellpadding="0" border="0" width="98%">
	<tr>
		<td valign="top" align="right" rowspan="2" width="2%">&nbsp;</td>
		<td valign="top" align="right" width="5%">
			<img src="images/icons/24_shadow/office-building.png"><img src="images/spacer.gif" height="1" width="10">
		</td>
		<td valign="top" height="5" width="60%">
			<strong><font style="font-size:130%;font-family:Trebuchet MS">Add/Change Bank Accounts</font></strong>
		</td>
		
		<td align="right" valign="top" height="5" >
		<div id="menu_bar">
		<script language="JavaScript">
		function submitform() {
			document.banks_manager.submit();
		}
		</script>
			<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td><img src="images/menu/menu_01.gif" width="13" height="25" alt=""></td>
				<td><a href="javascript: submitform()"><img src="images/menu/save.gif" width="23" height="25" alt="Update &amp; Close" border="0"></a></td>
				<td><img src="images/menu/menu_08.gif" width="8" height="25" alt=""></td>
				<td><a href="<?php echo $_SERVER['PHP_SELF']?>?id=<?php echo $revfull ?>"><img src="images/refresh.gif" border="0" alt="Refresh this page."></a></td>
				<td><img src="images/menu/menu_11.gif" width="9" height="25" alt=""></td>
				<td><img src="images/menu/menu_13.gif" width="18" height="25" alt=""></td>
			</tr>
		</table>
		</div>
		</td>
		<td rowspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td width="10"><img src="images/spacer.gif" width="25"></td>
		<td valign="top" colspan="4">
		<?php 
		if ($gval[0] == 1) {
			$sql = "SELECT * FROM bank_accounts WHERE md5(id) = '".$gval[1]."'";
			// echo $sql;
			$res = mysql_query($sql);
			$row = mysql_fetch_array($res) or die ("Cannot retrieve details");
		} ?>
			<table width="98%" border="0" cellpadding="2" cellspacing="2">
			<?php 
			if ($error_message == "") { ?>
			<tr>
				<td colspan="2"><img src="images/spacer.gif" width="45"></td>
			</tr>
			<?php 
			}
			if ($error_message != "") { ?>
				<tr>
					<td colspan="2"><img src="images/spacer.gif" width="25"></td>
				</tr>
				<tr>
					<td colspan="2" class="messageboard"><?php echo $error_message; ?></td>
				</tr>
				<tr>
				<td colspan="2"><img src="images/spacer.gif" width="15"></td>
			</tr>
			<?php 
			} ?>
			<tr>
				<td class="ls_on">Account Name: <font color="red">*</font></td>
				<?php 
				if (($_POST['updatesave'] == 1) || ($_POST['updatesave'] == 2)) {
					$selectedvalue_2 = $_POST['account_name'];
				} else {
					$selectedvalue_2 = $row['accountname'];
				}?>
				<td class="ls_on"><input name="account_name" type="text" class="standardfield" value="<?php echo $selectedvalue_2 ?>"></td>
			</tr>
			<tr>
				<td class="ls_on">Starting Date: <font color="red">*</font></td>
				<?php 
				if (($_POST['updatesave'] == 1) || ($_POST['updatesave'] == 2)) {
					$selectedvalue_3 = $_POST['starting_date'];
				} else {
					if ($gval[0] == 1) {
						$startingmonth = explode("-",substr($row['starting_date'],0,11));
						$startdatefinal = $startingmonth[2]."-".convertDate($startingmonth[1])."-".$startingmonth[0];
					} else {
						$startdatefinal = date("d-M-Y", time());
					}
				}?>
				<td class="ls_on"><input class="standardfield_date_sm" type="text" name="starting_date" id="starting_date" readonly value="<?php echo $startdatefinal ?>"> &nbsp;<img src="images/icons/calendar.png" id="f_trigger_c_start" style="cursor: pointer;" title="Date selector" ><script language="javascript">Calendar.setup({ inputField:"starting_date",button:"f_trigger_c_start",align:"Ll00", ifFormat:"%d-%b-%Y"  });</script></td>
			</tr>
			<tr>
				<td class="ls_on">Opening Balance<br>(Number Only): <font color="red">*</font></td>
				<?php 
				if (($_POST['updatesave'] == 1) || ($_POST['updatesave'] == 2)) {
					$selectedvalue_4 = $_POST['opening_balance'];
				} else {
					$selectedvalue_4 = $row['opening_balance'];
					if ($row['opening_balance'] == NULL) {
						$selectedvalue_4 = "0.00";
					}
				} 
				 ?>
				<td class="ls_on"><input style="text-align:right" name="opening_balance" type="text" class="standardfield_short" value="<?php echo $selectedvalue_4 ?>"></td>
			</tr>
			<tr>
				<td class="ls_on">Bank Nominal Code: </td>
				<?php 
				if (($_POST['updatesave'] == 1) || ($_POST['updatesave'] == 2)) {
					$selectedvalue_5 = $_POST['nominal_code'];
				} else {
					$selectedvalue_5 = $row['nominal_code'];
				}?>
				<td class="ls_on"><input name="nominal_code" type="text" class="standardfield_short" value="<?php echo $selectedvalue_5 ?>"></td>
			</tr>
			<tr>
				<td class="ls_on">Default Payments Method:</td>
				<?php 
				if (($_POST['updatesave'] == 1) || ($_POST['updatesave'] == 2)) {
					$selectedvalue_4 = $_POST['description'];
				} else {
					$selectedvalue_4 = $row['description'];
				}?>
				<td class="ls_on">
					<table>
					<tr>
						<td>Invoices:</td>
						<td>
							<select name="invoice_method" class="standardselect">
							<?php 
							$sql_2 = "SELECT * FROM payment_methods WHERE available_to = 1 ORDER BY method_name ASC";
							//echo $sql;
							$res_2 = mysql_query($sql_2);
							while ($row_2 = mysql_fetch_array($res_2)) { 
								if ($row_2['default'] == 1) { $selected = "selected"; }?>
								<option <?php echo $selected ?> value="<?php echo $row_2['id'] ?>"><?php echo $row_2['method_name'] ?></option>
							<?php 	
								unset($selected);
							} ?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Purchases:</td>
						<td>
							<select name="purchase_method" class="standardselect">
							<?php 
							$sql_2 = "SELECT * FROM payment_methods WHERE available_to = 2 ORDER BY method_name ASC";
							//echo $sql;
							$res_2 = mysql_query($sql_2);
							while ($row_2 = mysql_fetch_array($res_2)) { 
								if ($row_2['default'] == 1) { $selected = "selected"; }?>
								<option <?php echo $selected ?> value="<?php echo $row_2['id'] ?>"><?php echo $row_2['method_name'] ?></option>
							<?php 	
								unset($selected);
							} ?>
							</select>
						</td>
					</tr>
					
					</table>
				</td>
			</tr>
			<tr>
				<td class="ls_on">Default Account:</td>
				<td class="ls_on"><input type="checkbox" value="checkboxon" name="defaultaccount"></td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
<input type="hidden" name="hashid" value="<?php echo $gval[1] ?>">
<?php  
if ($gval[0] == 1) { ?>
	<input type="hidden" name="updatesave" value="1">
<?php } else { ?>
	<input type="hidden" name="updatesave" value="2">
<?php 
} ?>
</form>
</body>
</html>