<?php 
// Get activities menutabid
$querygetact = "SELECT hashid FROM menu_tabs WHERE type = 'Activities' AND isactive = 1 AND registrantid = ".RID." LIMIT 1";
$resultact = mysql_query($querygetact);
$rowact = mysql_fetch_array($resultact);
$_SESSION['store_activities_id'] = $rowact['hashid'];

function showMonth($month, $year, $thismonth, $today, $top) {
	$gval = explode(":",$_GET['id']);
	if ($gval[0] == "") {
		$zeromonth = $month;
	} else {
		$zeromonth = $gval[0];
	}
    $date = mktime(12, 0, 0, $month, 1, $year);
	//echo date("Y-m-d H:i:s",$date);
    $daysInMonth = date("t", $date);
    // calculate the position of the first day in the calendar (sunday = 1st column, etc)
    $offset = date("w", $date);
    $rows = 1;
    echo '<table width="99%" cellpadding="2" cellspacing="1" border="0">';
		if ($top == 1) {
			echo '<tr class="mheader">';
	 		echo '<td class="toprung"><a href="thinline.php?id='.($month-1).':'.$gval[1].':'.$gval[2].':'.$gval[3].':'.$gval[4].'::::::::::::::::'.$gval[20].'"><img src="images/calendar/cal_left.png" border="0"></a></td>';
		 	echo '<td class="toprung" colspan="5" align="center"><a href="thinline.php?id='.$month.':'.$gval[1].':'.$gval[2].':'.$gval[3].':2::::::::::::::::'.$gval[20].'">'.date("F Y", $date).'</a></td>';
			echo '<td class="toprung"><a href="thinline.php?id='.($month+1).':'.$gval[1].':'.$gval[2].':'.$gval[3].':'.$gval[4].'::::::::::::::::'.$gval[20].'"><img src="images/calendar/cal_right.png" border="0"></a></td>';
	 		echo "</td></tr>";
		} else {
			echo "<tr>";
		 	echo '<td class="mheader" colspan="7" align="center"><a href="thinline.php?id='.$month.':'.$gval[1].':'.$gval[2].':'.$gval[3].':2::::::::::::::::'.$gval[20].'">' . date("F Y", $date) . "</a></td>";
	 		echo "</td></tr>";
		}
    echo '<tr class="wheader"><th>Su</th><th>M</th><th>Tu</th><th>W</th><th>Th</th><th>F</th><th>Sa</th></tr>';
    echo "\n\t<tr>";
    for($i = 1; $i <= $offset; $i++) {
        echo "<td></td>";
    }
    for($day = 1; $day <= $daysInMonth; $day++) {
        if( ($day + $offset - 1) % 7 == 0 && $day != 1) {
            echo '</tr><tr>';
            $rows++;
        }
		$date = mktime(12, 0, 0, $month, $day, $year);
		if (($month == date('n')) && ($gval[1] == "")) {
			if ($day == $today) {
    	    	echo '<td><strong><a href="thinline.php?id='.$zeromonth .':'.$date.':::::::::::::::::::'.$gval[20].'">'.$day.'</strong></td>';
			} else {
				echo '<td><a href="thinline.php?id='.$zeromonth .':'.$date.':::::::::::::::::::'.$gval[20].'">'.$day.'</td>';
			}
		} else {
			if (  ($day == date("d", $gval[1])) && (date("m", $date) == date("m",$gval[1])) && (date("Y", $date) == date("Y",$gval[1]))    ) {
    	    	echo '<td class="circulate_current"><a href="thinline.php?id='.$zeromonth .':'.$date.':'.$gval[2].':'.$gval[3].':::::::::::::::::'.$gval[20].'">'.$day.'</td>';
			} else {
				echo '<td class="circulate_notcurrent" align="center"><a href="thinline.php?id='.$zeromonth .':'.$date.':'.$gval[2].':'.$gval[3].':::::::::::::::::'.$gval[20].'">';
					if (!$gval[3]) {
						$whoscalendar = $_SESSION['userid'];
					} else {
						$query_indi2 = "SELECT id FROM users WHERE isactive = 1";
						 //echo $query_indi;
						$result_indi2 = mysql_query($query_indi2);
						while ($row_indi2 = mysql_fetch_array($result_indi2)) {
							if (strtoupper(md5($row_indi2['id'])) == $gval[3]) {
								$whoscalendar = $row_indi2['id'];
								break;
							}
						}
					}
					if ($whoscalendar == $_SESSION['userid']) {
						$query = "SELECT id FROM activities WHERE (datestart  >= '".date("Y-m-d 00:00:00", $date). "' AND datestart < '".date("Y-m-d 23:59:59", $date). "') AND addtocal='on' AND allocatedto = ".$whoscalendar." ORDER BY id DESC";
						$sqlquery = "SELECT id FROM activities WHERE ((((datestart  >= '".date("Y-m-d 00:00:00", $date). "' AND datestart < '".date("Y-m-d 23:59:59", $date). "') AND (dateend  > '".date("Y-m-d 23:59:59", $date). "')) OR (dateend  >= '".date("Y-m-d 00:00:00", $date). "' AND dateend < '".date("Y-m-d 23:59:59", $date). "') AND (datestart  < '".date("Y-m-d 00:00:00", $date). "')) OR (datestart  <= '".date("Y-m-d 00:00:00", $date). "' AND dateend >= '".date("Y-m-d 23:59:59", $date). "')) AND addtocal='on' AND allocatedto = ".$whoscalendar." ORDER BY id DESC";
					} else {
						$query = "SELECT id FROM activities WHERE (datestart  >= '".date("Y-m-d 00:00:00", $date). "' AND datestart < '".date("Y-m-d 23:59:59", $date). "') AND addtocal='on' AND allocatedto = ".$whoscalendar." AND privacy = 'public' ORDER BY id DESC";
						$sqlquery = "SELECT id FROM activities WHERE ((((datestart  >= '".date("Y-m-d 00:00:00", $date). "' AND datestart < '".date("Y-m-d 23:59:59", $date). "') AND (dateend  > '".date("Y-m-d 23:59:59", $date). "')) OR (dateend  >= '".date("Y-m-d 00:00:00", $date). "' AND dateend < '".date("Y-m-d 23:59:59", $date). "') AND (datestart  < '".date("Y-m-d 00:00:00", $date). "')) OR (datestart  <= '".date("Y-m-d 00:00:00", $date). "' AND dateend >= '".date("Y-m-d 23:59:59", $date). "')) AND addtocal='on' AND allocatedto = ".$whoscalendar." AND privacy = 'public' ORDER BY id DESC";
					}
					//echo $query."<br>";
					$result = mysql_query($query);
					$row = mysql_fetch_array($result);
					
					$resultquery = mysql_query($sqlquery);
					$rowquery = mysql_fetch_array($resultquery);
					if ($row['id'] || $rowquery['id']) {
						echo "<strong>".$day."</strong>";
					} else {
						echo $day;
					}
				echo '</td>';
			}
		}
    }
    while( ($day + $offset) <= $rows * 7) {
        echo "<td></td>";
        $day++;
    }
    echo "</tr>\n";
    echo "</table>\n"; 
}

function showMonth_2($month, $year, $thismonth, $today, $top) {
	$gval = explode(":",$_GET['id']);
	if ($gval[0] == "") {
		$zeromonth = $month;
	} else {
		$zeromonth = $gval[0];
	}
    $date = mktime(12, 0, 0, $month, 1, $year);
    $daysInMonth = date("t", $date);
    // calculate the position of the first day in the calendar (sunday = 1st column, etc)
    $offset = date("w", $date);
    $rows = 1;
    echo '<table width="99%" cellpadding="5" cellspacing="1" border="0">';
		if ($top == 1) {
			echo '<tr class="mheader">';
	 		echo '<td><a href="thinline.php?id='.($month-1).':'.$gval[1].':::::::::::::::::::'.$gval[20].'"><img src="images/calendar/cal_left.png" border="0"></a></td>';
		 	echo "<td colspan='5' align='center'>".date("F Y", $date)."</td>";
			echo '<td><a href="thinline.php?id='.($month+1).':'.$gval[1].':::::::::::::::::::'.$gval[20].'"><img src="images/calendar/cal_right.png" border="0"></a></td>';
	 		echo "</td></tr>";
		} else {
			echo "<tr>";
			echo '<td colspan="7">';
				echo '<table width="99%" border=0 cellpadding="0" cellspacing="2"><tr><td width="4%" align=center class="mheader2">';
				echo '<a href="thinline.php?id='.($gval[0]-1).':'.$gval[1].':'.$gval[2].':'.$gval[3].':'.$gval[4].':'.$gval[5].':::::::::::::::'.$gval[20].'"><img src="images/icons/navigate_left_24.png" alt="Previous Month" border="0"></a></td>';
		 		echo '<td class="mheader2" colspan="7" align="center"><strong>' . date("F Y", $date) . "</strong></td>";
				echo '<td class="mheader2" width="4%" align=center>';
				echo '<a href="thinline.php?id='.($gval[0]+1).':'.$gval[1].':'.$gval[2].':'.$gval[3].':'.$gval[4].':'.$gval[5].':::::::::::::::'.$gval[20].'"><img src="images/icons/navigate_right_24.png" alt="Next Month" border="0"></a>';
				echo '</td></tr></table>';
			echo '</td>';
	 		echo "</tr>";
		}
    echo '<tr><th class="mweeks" width="14%">Su</th><th class="mweeks" width="14%">M</th><th class="mweeks" width="14%">Tu</th><th class="mweeks" width="14%">W</th><th class="mweeks" width="14%">Th</th><th class="mweeks" width="14%">F</th><th class="mweeks" width="14%">Sa</th></tr>';
    echo "\n\t<tr>";
    for($i = 1; $i <= $offset; $i++) {
        echo "<td class=\"mdays\">&nbsp;</td>";
    }
    for($day = 1; $day <= $daysInMonth; $day++) {
        if( ($day + $offset - 1) % 7 == 0 && $day != 1) {
            echo '</tr><tr>';
            $rows++;
        }
		$date = mktime(12, 0, 0, $month, $day, $year);
		if (($month == date('n')) && ($gval[1] == "")) {
			if ($day == $today) {
    	    	echo '<td><strong><a href="thinline.php?id='.$zeromonth .':'.$date.':::::::::::::::::::'.$gval[20].'">'.$day.'</strong></td>';
			} else {
				echo '<td><a href="thinline.php?id='.$zeromonth .':'.$date.':::::::::::::::::::'.$gval[20].'">'.$day.'</td>';
			}
		} else {
			
				echo '<td class="mdays" valign="top" align="left">';
					if (!$gval[3]) {
						$whoscalendar = $_SESSION['userid'];
					} else {
						$query_indi2 = "SELECT id FROM users WHERE isactive = 1";
						 //echo $query_indi;
						$result_indi2 = mysql_query($query_indi2);
						while ($row_indi2 = mysql_fetch_array($result_indi2)) {
							if (strtoupper(md5($row_indi2['id'])) == $gval[3]) {
								$whoscalendar = $row_indi2['id'];
								break;
							}
						}
					}
					
					echo '<table cellpadding="0" cellspacing="0" width="99%" border="0">';
					echo '<tr>';
					echo '  <td>';
					echo '		<font style="font-weight:bold;font-family:Trebuchet MS,Arial;font-size:17px">'.$day.'</font>';
					echo '  </td>';
					echo '  <td align="right" width="5%">';
					$date_test = date("H", (time()+3600));
					$date_serial = mktime($date_test, 0, 0, $month, $day, $year);
					if ((strtoupper(md5(UID)) == $gval[3]) ) {
						echo '  <a href="thinline.php?id=27:'.$date_serial.':::::::::::::::::::'.$_SESSION['store_activities_id'].'"><img src="images/icons/calendar.png" border="0"></a>';
					} elseif ((strtoupper(md5(UID)) != $gval[3]) && ($gval[3] != NULL) ) {
						echo '	<a href="thinline.php?id=27:'.$date_serial.'::'.$gval[3].':::::::::::::::::'.$_SESSION['store_activities_id'].'"><img src="images/icons/calendar.png" border="0" alt="Create an activity for this user."></a>';
					} elseif ($gval[3] == NULL) {
						echo '  <a href="thinline.php?id=27:'.$date_serial.':::::::::::::::::::'.$_SESSION['store_activities_id'].'"><img src="images/icons/calendar.png" border="0"></a>';
					} 
					echo '  </td>';
					echo '</tr>';
					echo '</table>';
						
						if ($whoscalendar == $_SESSION['userid']) {
							$sqlquery = "SELECT id,subject,datestart,dateend FROM activities WHERE ((((datestart  >= '".date("Y-m-d 00:00:00", $date). "' AND datestart < '".date("Y-m-d 23:59:59", $date). "') AND (dateend  > '".date("Y-m-d 23:59:59", $date). "')) OR (dateend  >= '".date("Y-m-d 00:00:00", $date). "' AND dateend < '".date("Y-m-d 23:59:59", $date). "') AND (datestart  < '".date("Y-m-d 00:00:00", $date). "')) OR (datestart  <= '".date("Y-m-d 00:00:00", $date). "' AND dateend >= '".date("Y-m-d 23:59:59", $date). "')) AND addtocal='on' AND userid = ".$whoscalendar." ORDER BY id DESC";
						} else {
							$sqlquery = "SELECT id,subject,datestart,dateend FROM activities WHERE ((((datestart  >= '".date("Y-m-d 00:00:00", $date). "' AND datestart < '".date("Y-m-d 23:59:59", $date). "') AND (dateend  > '".date("Y-m-d 23:59:59", $date). "')) OR (dateend  >= '".date("Y-m-d 00:00:00", $date). "' AND dateend < '".date("Y-m-d 23:59:59", $date). "') AND (datestart  < '".date("Y-m-d 00:00:00", $date). "')) OR (datestart  <= '".date("Y-m-d 00:00:00", $date). "' AND dateend >= '".date("Y-m-d 23:59:59", $date). "')) AND addtocal='on' AND userid = ".$whoscalendar." AND privacy = 'public' ORDER BY id DESC";
						}
						//echo $sqlquery;
						$resultquery = mysql_query($sqlquery);
						while ($rowquery = mysql_fetch_array($resultquery)) {
							echo '<br><font style="font-family:Tahoma;font-size:11px;color:#0066CC">'.date("H:i",strtotime($rowquery['datestart'])).' '.date("H:i",strtotime($rowquery['dateend'])).'</font><br>';
							echo '<a href="thinline.php?id='.$gval[0].':'.$gval[1].':'.$gval[2].':'.$gval[3].':'.$gval[4].':'.strtoupper(md5($rowquery['id'])).':::::::::::::::'.$_SESSION['store_activities_id'].'">';
							$mystrlen = strlen($rowquery['subject']);
							if ($mystrlen > 15) {
								echo substr($rowquery['subject'], 0,15)."...";
							} else {
								echo $rowquery['subject'];
							}
							echo '</a><br>'; ?>						
<?php					$counterxi++;
						}
					
						if ($whoscalendar == $_SESSION['userid']) {
							$query = "SELECT id,subject,datestart,dateend FROM activities WHERE ((datestart  >= '".date("Y-m-d 00:00:00", $date). "' AND datestart < '".date("Y-m-d 23:59:59", $date). "') AND (dateend  >= '".date("Y-m-d 00:00:00", $date). "' AND dateend < '".date("Y-m-d 23:59:59", $date). "')) AND addtocal='on' AND userid = ".$whoscalendar." ORDER BY id DESC";
						} else {
							$query = "SELECT id,subject,datestart,dateend FROM activities WHERE ((datestart  >= '".date("Y-m-d 00:00:00", $date). "' AND datestart < '".date("Y-m-d 23:59:59", $date). "') AND (dateend  >= '".date("Y-m-d 00:00:00", $date). "' AND dateend < '".date("Y-m-d 23:59:59", $date). "')) AND addtocal='on' AND userid = ".$whoscalendar." AND privacy = 'public' ORDER BY id DESC";
						}
						$result = mysql_query($query);
						while ($row = mysql_fetch_array($result)) {
							echo '<a href="thinline.php?id='.$gval[0].':'.$gval[1].':'.$gval[2].':'.$gval[3].':'.$gval[4].':'.strtoupper(md5($row['id'])).':::::::::::::::'.$_SESSION['store_activities_id'].'">';
							$mystrlen = strlen($row['subject']);
							if ($mystrlen > 15) {
								echo substr($row['subject'], 0,15)."...";
							} else {
								echo $row['subject'];
							}
							echo '</a><br>'; 
							echo '<font style="font-family:Tahoma;font-size:11px;color:#0066CC">'.date("H:i",strtotime($row['datestart'])).' - '.date("H:i",strtotime($row['dateend'])).'</font><br><br>';?>
<?php					$counterxi++;
						}
				echo '</td>';
			
		}
    }
    while( ($day + $offset) <= $rows * 7) {
        echo "<td></td>";
        $day++;
    }
    echo "</tr>\n";
    echo "</table>\n"; 
}

/////////////////////////////////////////////////////////
///////////////// Start some functions //////////////////
/////////////////////////////////////////////////////////
function displayEntry($rowid,$totalcols,$createdatestring) {
	$gval = explode(":",$_GET['id']);
	include_once('classes/browser.php');
	$br = new Browser;
	//echo $br->Name;
	
	//////////////////////////////////////////////////////////////
	////// Use this to manage the width of appointments //////////
	//////////////////////////////////////////////////////////////
	if (ERES <= 800) {
		$divwidth = 80;
		$startingpoint = 105;
	} else {
		$divwidth = 250;
		$startingpoint = 140;
	}
	
	$querygetdates = "SELECT *  FROM activities WHERE id=".$rowid;
	$resultdates = mysql_query($querygetdates);
	$rowdates = mysql_fetch_array($resultdates);
	
	$datestart = strtotime($rowdates['datestart']);
	$dateend = strtotime($rowdates['dateend']);
	//echo $rowdates['dateend']."<br>";
	$rest1 = substr($rowdates['datestart'], -5, 2);    // returns "f"
	$rest = substr($rowdates['dateend'], -5, 2);    // returns "f"
	//echo $rest;
	
	$datstartfordisplay = $datestart;
	if ((date("i",$datestart) <= 30) && (date("i",$datestart) != 00)) {
		$adjustv = date("i",$datestart);
		$datestart = $datestart - ($adjustv * 60);
	} elseif ((date("i",$datestart) > 30) && (date("i",$datestart) != 00)) {
		$adjustv = date("i",$datestart);
		$datestart = $datestart-(30-(60 - $adjustv))*60;
	}
	
	$datendfordisplay = $dateend;
	
	if ((date("i",$dateend) < 30) && (date("i",$dateend) != 00)) {
		$adjustv = date("i",$dateend);
		$dateend = ($dateend - ($adjustv * 60)) + 1800;
	} elseif ((date("i",$dateend) > 30) && (date("i",$dateend) < 59) && (date("i",$dateend) != 00)) {
		$adjustv = date("i",$dateend);
		$dateend = ($dateend - ($adjustv * 60)) + 3600;
		//echo "<font size=1>In here</font>";
	} elseif ((date("i",$dateend) == 30) && (date("i",$dateend) != 00)) {
		$adjustv = date("i",$dateend);
		$dateend = ($dateend - ($adjustv * 60)) + 1800;
	} elseif (date("i",$dateend) == 00) {
		$adjustv = date("i",$dateend);
		$dateend = ($dateend - ($adjustv * 60));
	} 
	
	//echo ($dateend - $datestart)."<br>";
	
	$variance = $dateend - $datestart;
	//echo $variance;
	
	$queryduplicate = "SELECT * FROM calendar_temp WHERE session='".session_id()."' ORDER BY id ASC";
	//echo $queryduplicate;
	$resultduplicate = mysql_query($queryduplicate);
	while ($rowduplicate = mysql_fetch_array($resultduplicate)) {
		if ($_SESSION['vcounter'] >= 1) {
			if ($datestart < $rowduplicate['fullend']) {
				$m4 = $rowduplicate['position']+$startingpoint;
				$_SESSION['vprevpixel'] = $m4;
			}
		}
	}
	
	if ($_SESSION['vcounter'] == "") {
		$vcounternumber = 1;
		$_SESSION['vcounter'] = 1;
	} else {
		$vcounternumber = $_SESSION['vcounter'];
	}
	
	/////////////////////////////////////////////////////////////////
	////  Adjust the height of the appointments for each browser ////
	/////////////////////////////////////////////////////////////////
	
	if ($br->Name == "MSIE") {
		
		$m1 = "-2px";
		$m2 = 0;
		$m3 = 0;
		
		if ($variance == 1800) {
			$h = 21;
			$h2 = 21;
		} else {
			$variance = (($variance/60)/30)-1;
			// This makes sure the block is the exact right size for the minutes.
			if (($rest > 0) && ($rest <= 30)) {
				$rest = ($rest - 10);
				//echo "In here";
			} elseif (($rest > 30) && ($rest < 59))  {
				$rest = ($rest - 40);
				//echo $rest.":29+";
			} else {
				$rest = 19;
			}
			
			$h = 19+(34 * $variance);
			$h2 = $rest+(34 * $variance);
			// Bodge repair on times that start at exactly 30 past the hour.
			if ($rest1 == 30) {
				$h = ($h - 34);
				$h2 = ($h2 - 34);
			}
			
		}
	// All other browsers
	} else {
		$m1 = "-6px";
		$m2 = "0px";
		$m3 = "0px";
		if ($variance == 1800) {
			$h = 16;
		} else {
			$variance = (($variance/60)/30)-1;
			// This makes sure the block is the exact right size for the minutes.
			if (($rest > 0) && ($rest <= 30)) {
				$rest = ($rest - 13);
				//echo "In here";
			} elseif (($rest > 30) && ($rest < 59))  {
				$rest = ($rest - 43);
			} else {
				$rest = 16;
			}
			$h = 16+(31*$variance);
			$h2 = $rest+(31*$variance);
			// Bodge repair on times that start at exactly 30 past the hour.
			if ($rest1 == 30) {
				$h = ($h - 31);
				$h2 = ($h2 - 31);
			}
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////
	////  Adjust the start point of the appointments for each browser [first entry] ////
	////////////////////////////////////////////////////////////////////////////////////
	
	$_SESSION['vcounter'] = $_SESSION['vcounter'] + 1;
	if ($m4 == "") {
		if ($br->Name == "Firefox") {
			$m4 = 26;
			$font_size = 11;
		} elseif ($br->Name == "MSIE") {
			$m4 = 22;
			$font_size = 14;
		} elseif ($br->Name == "Opera") {
			$m4 = 22;
			$font_size = 11;
		} else {
			$m4 = 22;
			$font_size = 11;
		}
		$_SESSION['vprevpixel'] = $m4;
	}
	$querydraft = "INSERT INTO calendar_temp (session, selecteddate, fullstart, fullend, stringlength,vcounter,position) VALUES (
		  '".session_id()."',
		  '".$gval[1]."',
		  '".$datestart."',
		  '".$dateend."',
		  '".strlen($rowdates['subject'])."',
		  '".$vcounternumber."',
		  '".$m4."'
	 	 )";
	mysql_query($querydraft) Or Die ("Cannot submit entry!");
	if ($variance == 1800) {
		$font_size = 11;
		$font_type = "Tahoma";
	} else {
		$font_type = "Arial";
	}
	?>
	<div style="font-family:<?php echo $font_type ?>;font-size:<?php echo $font_size; ?>px;text-align:left;position:absolute;border:1px solid #000;background:#fff;padding:5px 15px 5px 5px;margin:<?php echo $m1." ".$m2." ".$m3." ".$m4?>px;height:<?php echo $h?>px;width:<?php echo $divwidth; ?>px">
	<?php
	$querycolor = "SELECT fieldvalue2 FROM custom_settings WHERE fieldvalue ='".$rowdates['showtimeas']."' AND fieldname='showtimeas'";
	$resultcolor = mysql_query($querycolor);
	$rowcolor = mysql_fetch_array($resultcolor);
	?>
	<div style="font-family:Arial;font-size:11px;position:absolute;margin:-5px 0 0 -5px;height:<?php echo ($h2+10)?>px;background:#<?php echo $rowcolor['fieldvalue2'] ?>;width:7px;"></div>
	<table cellpadding="2" cellspacing="0" border="0">
	<tr>
		<td rowspan="2">&nbsp;</td>
		<td>
			<a style="font-size:14px;text-decoration:underline;" href="<?php echo $_SERVER['PHP_SELF'] ?>?id=:<?php echo $rowdates['leadhashid']; ?>:<?php echo $rowdates['contacthashid'] ?>:::<?php echo strtoupper(md5($rowdates['id'])); ?>:::::::::::::::<?php echo $_SESSION['store_activities_id'] ?>"><?php 
			$mystrlen = strlen($rowdates['subject']);
			if (($mystrlen > 15) && ($variance <= 3600)) {
				echo substr($rowdates['subject'], 0,30)."...";
			} else {
				echo $rowdates['subject'];
			}
			//echo "<br>".$rest."<br>";
			//echo $variance."<br>";
			//echo $h2."<br>";
			?></a>
			<?php
			//echo "<br>".$_SESSION['vcounter']." --".$m4."-- ".floor($_SESSION['vprevpixel']/155)." <-- ";
			$_SESSION['setlastvalue'] = $m4;  // Do not remove
			?>
		</td>
	</tr>
	<?php 
	//if ($variance != 1800) {?>
	<tr>
		<td>
			<font style="color:#0066CC;">
			<?php echo date("H:i",$datstartfordisplay)." to ".date("H:i",$datendfordisplay); ?>
			</font><br>
			<?php 
			if ($rowdates['contactstableid']== 1) {
				
			} elseif ($rowdates['contactstableid']== 2) {
				$querycompanystatus = "SELECT displayingas FROM contacts_individuals WHERE hashid='".$rowdates['contacthashid']."'";
				$resultcompanystatus = mysql_query($querycompanystatus);
				$rowcompanystatus = mysql_fetch_array($resultcompanystatus);
				echo "<a style='font-size:11px;text-decoration:underline;' href='customers.php?id=7:2:".$rowdates['contacthashid']."'>".$rowcompanystatus['displayingas']."</a>";
			}
			 ?>
		</td>
	</tr>
	<?php 
	//}?>
	</table>
	</div>
<?php 
} ?>

	<style type="text/css" media="all">@import "css/calendar.css";</style>


			<table border="0" cellpadding="0" cellspacing="3" width="99%">
			<tr>
				<td colspan="3" rowspan="2">&nbsp;</td>
				<td colspan="3">&nbsp;</td>
			</tr>
			<tr>
				<?php 
				if ($gval[4] != 3) {?>
				<td align="left" valign="top" width="12%">
				<br style="line-height:9px">
					<br style="line-height:20px">
					<?php 
					if ((strtoupper(md5(UID)) != $gval[3]) && ($gval[3] != "")) {?>
					<a href="thinline.php?id=<?php echo date('n',$thetimestamp); ?>:<?php echo strtotime($middaytoday); ?>:<?php echo $gval[2];?>::<?php echo $gval[4];?>"><font class="note">Return to my Calendar</font></a><br><br style="line-height:8px">
					<?php 
					}?>
					<table cellspacing="0" cellpadding="5" style="background:#f1f1f1;border:solid 1px #ccc">
						<tr>
							<td><img src="images/icons/calendar_up.png" align="left"> &nbsp;Shared Calendars:</td>
						</tr>
						<tr>
							<td>
								<?php
								if ($_SESSION['securityarrdept'][0] != "") {
									$deptcounter = 0;
									$addsqldepartments = '';
									foreach ($_SESSION['securityarrdept'] AS $deptval) {
										if ($deptcounter == 0) {
											$deptoperator = " AND (";
										} else {
											$deptoperator = " OR ";
										}
										$addsqldepartments .= $deptoperator." FIND_IN_SET('".$deptval."', departments)";
										$deptcounter++;
									}
									$addsqldepartments .= ")";
								}
								$query_indi = "SELECT id,firstname,lastname FROM users WHERE registrantid = ".RID." ".$addsqldepartments." AND isactive = 1 ORDER BY firstname ASC";
					 			//echo $query_indi;
								$result_indi = mysql_query($query_indi);?>
								<select class="standardselect" onchange="window.location.href=this.options[this.selectedIndex].value">
									<?php 
									while ($row_indi = mysql_fetch_array($result_indi)) {
										if ((UID == $row_indi['id']) && ($gval[3] == "")) {
											$imselected = "selected";
										} elseif (strtoupper(md5($row_indi['id'])) == $gval[3]) {
											$imselected = "selected";
										} 
										echo '<option '.$imselected.' value="thinline.php?id='.$gval[0].':'.$gval[1].':'.$gval[2].':'.strtoupper(md5($row_indi['id'])).':'.$gval[4].'::::::::::::::::'.$gval[20].':'.$gval[21].'">'.$row_indi['firstname'].' '.$row_indi['lastname'].'</option>';
										$imselected = "";
									}
									?>
								</select>
							</td>
						</tr>
					</table>
					<br style="line-height:20px">
					<?php 
					$top = 1;
					if ($gval[0] != "") {
						$startmonth = $gval[0];
						$startyear = date('Y',$thetimestamp);
					} else {
						$startmonth = date('n',$thetimestamp);
						$startyear = date('Y',$thetimestamp);
					}
					$today = date('d',$thetimestamp);
					$thismonth = date('n',$thetimestamp);
					showMonth($startmonth, $startyear, $thismonth, $today, $top);
					?><br>
					<?php 
					showMonth(($startmonth+1), $startyear, $thismonth, $today);
					?><br>
					<?php 
					showMonth(($startmonth+2), $startyear, $thismonth, $today);
					?><br><br>
				</td>
				<td>
					<img src="images/spacer.gif" width="5" height="1">
				</td>
				<?php 
				}?>
				<td valign="top" width="100%">
<?php 
				if ($gval[4] == "") {?>					
					<table border="0">
					<tr>
						<td><img src="images/icons/arrow_green.png"></td>
						<td>
							<a href="thinline.php?id=<?php echo date('n',$thetimestamp); ?>:<?php echo strtotime($middaytoday); ?>:<?php echo $gval[2] ?>:<?php echo $gval[3] ?>:::::::::::::::::<?php echo $gval[20] ?>">Go to Today</a> |
							<a href="thinline.php?id=<?php echo $gval[0]; ?>:<?php echo $gval[1] ?>:<?php echo $gval[2] ?>:<?php echo $gval[3] ?>:2::::::::::::::::<?php echo $gval[20] ?>">Month View</a>
						</td>
						<td><img src="images/spacer.gif" width="30" height="1" border="0"></td>
						<td><img src="images/icons/arrow_green.png"></td>
						<td>
							<a href="thinline.php?id=<?php echo $gval[0] ?>:<?php echo $gval[1] ?>:2:<?php echo $gval[3] ?>:::::::::::::::::<?php echo $gval[20] ?>">Show All Day</a> | 
							<a href="thinline.php?id=<?php echo $gval[0] ?>:<?php echo $gval[1] ?>::<?php echo $gval[3] ?>:::::::::::::::::<?php echo $gval[20] ?>">Show Working Day</a>
						</td>
					</tr>
					</table>
					<?php 
					if (!$gval[3]) {
						$whoscalendar = $_SESSION['userid'];
					} else {
						$query_indi = "SELECT id FROM users WHERE isactive = 1 ";
						 //echo $query_indi;
						$result_indi = mysql_query($query_indi);
						while ($row_indi = mysql_fetch_array($result_indi)) {
							if (strtoupper(md5($row_indi['id'])) == $gval[3]) {
								$whoscalendar = $row_indi['id'];
								break;
							}
						}
					}
					
					if ($whoscalendar == $_SESSION['userid']) {
						$query = "SELECT COUNT(id) AS TotalCols FROM activities WHERE ((datestart  >= '".date("Y-m-d 00:00:00", $gval[1]). "' AND datestart < '".date("Y-m-d 23:59:59", $gval[1]). "') AND (dateend  >= '".date("Y-m-d 00:00:00", $gval[1]). "' AND dateend < '".date("Y-m-d 23:59:59", $gval[1]). "')) AND addtocal='on' AND userid = ".$whoscalendar." ORDER BY id DESC";
					} else {
						$query = "SELECT COUNT(id) AS TotalCols FROM activities WHERE ((datestart  >= '".date("Y-m-d 00:00:00", $gval[1]). "' AND datestart < '".date("Y-m-d 23:59:59", $gval[1]). "') AND (dateend  >= '".date("Y-m-d 00:00:00", $gval[1]). "' AND dateend < '".date("Y-m-d 23:59:59", $gval[1]). "')) AND addtocal='on' AND userid = ".$whoscalendar." AND privacy = 'public' ORDER BY id DESC";
					}
					//echo $query."<br>";
					$result = mysql_query($query);
					$row = mysql_fetch_array($result);
					
					$totalcols = $row['TotalCols'];
					
					if ($whoscalendar == $_SESSION['userid']) {
						$query = "SELECT * FROM activities WHERE ((datestart  >= '".date("Y-m-d 00:00:00", $gval[1]). "' AND datestart < '".date("Y-m-d 23:59:59", $gval[1]). "') AND (dateend  >= '".date("Y-m-d 00:00:00", $gval[1]). "' AND dateend < '".date("Y-m-d 23:59:59", $gval[1]). "')) AND addtocal='on' AND allocatedto = ".$whoscalendar." ORDER BY id DESC";
						$sqlquery = "SELECT id,subject,datestart FROM activities WHERE ((((datestart  >= '".date("Y-m-d 00:00:00", $gval[1]). "' AND datestart < '".date("Y-m-d 23:59:59", $gval[1]). "') AND (dateend  > '".date("Y-m-d 23:59:59", $gval[1]). "')) OR (dateend  >= '".date("Y-m-d 00:00:00", $gval[1]). "' AND dateend < '".date("Y-m-d 23:59:59", $gval[1]). "') AND (datestart  < '".date("Y-m-d 00:00:00", $gval[1]). "')) OR (datestart  <= '".date("Y-m-d 00:00:00", $gval[1]). "' AND dateend >= '".date("Y-m-d 23:59:59", $gval[1]). "')) AND addtocal='on' AND allocatedto = ".$whoscalendar." ORDER BY id DESC";
					} else {
						$query = "SELECT * FROM activities WHERE ((datestart  >= '".date("Y-m-d 00:00:00", $gval[1]). "' AND datestart < '".date("Y-m-d 23:59:59", $gval[1]). "') AND (dateend  >= '".date("Y-m-d 00:00:00", $gval[1]). "' AND dateend < '".date("Y-m-d 23:59:59", $gval[1]). "')) AND addtocal='on' AND allocatedto = ".$whoscalendar." AND privacy = 'public' ORDER BY id DESC";
						$sqlquery = "SELECT id,subject,datestart FROM activities WHERE ((((datestart  >= '".date("Y-m-d 00:00:00", $gval[1]). "' AND datestart < '".date("Y-m-d 23:59:59", $gval[1]). "') AND (dateend  > '".date("Y-m-d 23:59:59", $gval[1]). "')) OR (dateend  >= '".date("Y-m-d 00:00:00", $gval[1]). "' AND dateend < '".date("Y-m-d 23:59:59", $gval[1]). "') AND (datestart  < '".date("Y-m-d 00:00:00", $gval[1]). "')) OR (datestart  <= '".date("Y-m-d 00:00:00", $gval[1]). "' AND dateend >= '".date("Y-m-d 23:59:59", $gval[1]). "')) AND addtocal='on' AND allocatedto = ".$whoscalendar." AND privacy = 'public' ORDER BY id DESC";
					}
					//echo $query."<br>";
					//echo $whoscalendar;
					$result = mysql_query($query);
					$stack = array();
					while ($row = mysql_fetch_array($result)){
						//echo $row['id']."<br>";
						array_push($stack,$row['id']);
						array_push($stack, array($row['datestart'], $row['dateend']));
					}
					?>
					<table width="100%" cellspacing="3" cellpadding="1" border="0">
							<?php 
							$resultquery = mysql_query($sqlquery);
							while ($rowquery = mysql_fetch_array($resultquery)) { ?>
							<tr>
							<td class="alldayevent" align="center">
								<a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=9:<?php echo strtotime($rowquery['datestart']); ?>::::<?php echo strtoupper(md5($rowquery['id'])); ?>:::::::::::::::<?php echo $_SESSION['store_activities_id'] ?>"><img src="images/icons/cal_allday.png" alt="All day event" border="0"></a>
							</td>
							<td colspan="2" class="alldayevent">
								<a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=9:<?php echo strtotime($rowquery['datestart']); ?>::::<?php echo strtoupper(md5($rowquery['id'])); ?>:::::::::::::::<?php echo $_SESSION['store_activities_id'] ?>"><?php echo $rowquery['subject'] ?></a>
							</td>
						</tr>
					<?php 
							}?>
						<tr>
							<td></td>
						</tr>
					<?php 
					if ($gval[2] == 2) {
						$setstarttime = 0;
						$setendtime = 23;
					} else {
						$setstarttime = 6;
						$setendtime = 19;
					}
					$y = 100;
					$z = $setstarttime;
					
					////////////////////////////////////////////////////////////////////////////////////////////////////
					//////// Got to check if there are any items scheduled before the start of the working day. ////////
					////////////////////////////////////////////////////////////////////////////////////////////////////
					
					if ($gval[2] != 2) {
						
						$result = mysql_query($query);
						while ($row = mysql_fetch_array($result)){
							
							if (date("H",strtotime($row['datestart'])) < 10) {
								$comparenewstart = substr(date("H",strtotime($row['datestart'])),1,2);
							} else {
								$comparenewstart = date("H",strtotime($row['datestart']));
							}
							
							if (date("H",strtotime($row['dateend'])) < 10) {
								$comparenewend = substr(date("H",strtotime($row['dateend'])),1,2);
							} else {
								if ($comparenewend == "") {
									$comparenewend = date("H",strtotime($row['dateend']));
								} elseif (date("H",strtotime($row['dateend'])) > $comparenewend) {
									$comparenewend = date("H",strtotime($row['dateend']));
									
								}
							}
							
							if (($comparenewstart < 6) && ($comparenewstart != 0)) {
								$setstarttime = $comparenewstart-1;
							} elseif ($comparenewstart == 0) {
								$setstarttime = $comparenewstart;
							} 
							
							if ($comparenewend > 19) {
								$setendtime = $comparenewend;
							} 
						}
					}

					for ($x = $setstarttime; $x <= $setendtime; $x++) {?>
					<tr>
					    <td class="time_set" width="6%" rowspan="2">
							<strong><?php 
							if ($x < 10) { 
								echo "0".$x;
								$timestr1 = "0".$x;
							} else { 
								echo $x;
								$timestr1 = $x;
							}
							?></strong><sup style="font-family;Arial;font-size:13px">00</sup>
						</td>
						<?php 
						
						$createdatestring =  strtotime(date("Y-m-d",$gval[1])." ".$timestr1.":00");
						//echo date("H:i",$createdatestring)."<br>";
						if (($x <= 7) || ($x >= 18)) {
							$class = "time_slot_ah";
						} else {
							$class = "time_slot";
						}
						?>
						<td align="center" class="time_set_cal" width="2%">
							<?php
							if ($x < 10) {
								$createchecker = "0".$x;
							} else {
								$createchecker = $x;
							}
		
							$counter = 1;
							foreach ($stack as $i => $value) {
								if ($counter == 1) {
									$rowid = $value;
								}
								if ($counter == 2) {
									$counter++;
								}
								foreach ($value as $i => $value2) {
									$timestr = substr($value2, -8);
									$timestr = substr($timestr, 0,5);
									//echo "<font size=1>".$createchecker."</font><br>";
									for ($number = 0; $number <= 29; $number++) {
										if ($number < 10) {
											$createchecker2 = $createchecker.":0".$number;
										} else {
											$createchecker2 = $createchecker.":".$number;
										}
										//echo $number."<br>";
										if (($createchecker2 == $timestr) && ($counter == 3) && ($number <= 29)) {
											displayEntry($rowid,$totalcols,$createdatestring);
										}
									}
									$counter++;
								}
								if ($counter == 5) {
									$counter = 1;
								} else {
									$counter++;
								}
							}
							if ($gval[2] == 2) {
								$onthehour = ($gval[1]-($z*3600)-43200);
								$onthehalf = $onthehour;
							} else {
								$onthehour = ($gval[1]-($z*3600));
								$onthehalf = $onthehour;
							}
							
							
							?>
							<?php if ((strtoupper(md5(UID)) == $gval[3]) ) { ?>
								<a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=27:<?php echo ($createdatestring+1800); ?>:::::::::::::::::::<?php echo $_SESSION['store_activities_id']; ?>"><img src="images/icons/calendar.png" border="0"></a> 
							<?php } elseif ((strtoupper(md5(UID)) != $gval[3]) && ($gval[3] != NULL) ) { ?>
								<a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=27:<?php echo ($createdatestring+1800); ?>:::::::::::::<?php echo $gval[3] ?>::::::<?php echo $_SESSION['store_activities_id']; ?>"><img src="images/icons/calendar.png" border="0" alt=""></a>
							<?php } elseif (($gval[3] == NULL) ) { ?>
								<a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=27:<?php echo $createdatestring; ?>:::::::::::::::::::<?php echo $_SESSION['store_activities_id']; ?>"><img src="images/icons/calendar.png" border="0"></a> 
							<?php } ?>
						</td>
					    <td class="<?php echo $class ?>">
							&nbsp;<?php //echo date("Y-m-d H:i:s", $onthehour); ?>
						</td>
					<tr>
						<td align="center" class="time_set_cal" width="2%">
							<?php
							if ($x < 10) {
								$createchecker = "0".$x;
							} else {
								$createchecker = $x;
							}
							
							$counter = 1;
							foreach ($stack as $i => $value) {
								if ($counter == 1) {
									$rowid = $value;
								}
								if ($counter == 2) {
									$counter++;
								}
								foreach ($value as $i => $value2) {
									$timestr = substr($value2, -8);
									$timestr = substr($timestr, 0,5);
									//echo "<font size=1>".$createchecker."</font><br>";
									for ($number = 30; $number <= 59; $number++) {
										if ($number < 10) {
											$createchecker2 = $createchecker.":0".$number;
										} else {
											$createchecker2 = $createchecker.":".$number;
										}
										//echo $number."<br>";
										if (($createchecker2 == $timestr) && ($counter == 3) && ($number > 29)) {
											displayEntry($rowid,$totalcols,$createdatestring);
										}
									}
									$counter++;
								}
								if ($counter == 5) {
									$counter = 1;
								} else {
									$counter++;
								}
							}
							if ($gval[2] == 2) {
								$onthehour = ($gval[1]-($z*3600)-43200);
								$onthehalf = $onthehour;
							} else {
								$onthehour = ($gval[1]-($z*3600));
								$onthehalf = $onthehour;
							}
							
							?>
							<?php if ((strtoupper(md5(UID)) == $gval[3]) ) { ?>
								<a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=27:<?php echo ($createdatestring+1800); ?>:::::::::::::::::::<?php echo $_SESSION['store_activities_id']; ?>"><img src="images/icons/calendar.png" border="0"></a> 
							<?php } elseif ((strtoupper(md5(UID)) != $gval[3]) && ($gval[3] != NULL) ) { ?>
								<a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=27:<?php echo ($createdatestring+1800); ?>:::::::::::::<?php echo $gval[3] ?>::::::<?php echo $_SESSION['store_activities_id']; ?>"><img src="images/icons/calendar.png" border="0" alt=""></a>
							<?php } elseif (($gval[3] == NULL) ) { ?>
								<a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=27:<?php echo ($createdatestring+1800); ?>:::::::::::::::::::<?php echo $_SESSION['store_activities_id']; ?>"><img src="images/icons/calendar.png" border="0"></a> 
							<?php } ?>
						</td>
						<td class="<?php echo $class ?>">
							&nbsp;<?php //echo date("Y-m-d H:i:s", ($onthehalf+1800)); ?>
						</td>
					</tr>
					</tr>
					<?php 
					$y++;
					$z--;
					}
					$queryduplicate = "DELETE FROM calendar_temp WHERE session='".session_id()."'";
					mysql_query($queryduplicate);
					$_SESSION['vcounter'] = 0;
					?>
					</table>
					<?php 
					} 
					/////////////////////////////////////////////////////////////////////
					////////////////  End Daily View & Start Monthly View////////////////
					/////////////////////////////////////////////////////////////////////
					  elseif ($gval[4] == 2) {  ?>
						<table border="0">
					<tr>
						<td><img src="images/icons/arrow_green.png"></td>
						<td>
							<a href="thinline.php?id=<?php echo date('n',$thetimestamp); ?>:<?php echo strtotime($middaytoday); ?>:<?php echo $gval[2] ?>:<?php echo $gval[3] ?>">Go to Today</a> |
							<a href="thinline.php?id=<?php echo $gval[0]; ?>:<?php echo $gval[1] ?>:<?php echo $gval[2] ?>:<?php echo $gval[3] ?>:2">Month View</a>
						</td>
						<td><img src="images/spacer.gif" width="30" height="1" border="0"></td>
						<td><img src="images/icons/arrow_green.png"></td>
						<td>
							<a href="thinline.php?id=<?php echo $gval[0] ?>:<?php echo $gval[1] ?>:2:<?php echo $gval[3] ?>">Show All Day</a> | 
							<a href="thinline.php?id=<?php echo $gval[0] ?>:<?php echo $gval[1] ?>::<?php echo $gval[3] ?>">Show Working Day</a>
						</td>
					</tr>
					</table>
					<?php showMonth_2($gval[0], date("Y"), $thismonth, $today);
					}
					
					/////////////////////////////////////////////////////////////////////////
					////////////////  End Monthly View & Start Scheduled View////////////////
					/////////////////////////////////////////////////////////////////////////
					  elseif ($gval[4] == 3) {
					 	
						// Security rediret for the schedule section of the website.
						
					  	if ($_POST['_check'] == 1) {
							$_SESSION['includepeep'] = "";
							$mySelected = $_POST['users'];
							foreach ($mySelected as $item) {
								if ($_SESSION['includepeep'] ==  "") {
									$_SESSION['includepeep'] = $item;
								} else {
									$_SESSION['includepeep'] = $item.":".$_SESSION['includepeep'];
								}
							}
						}					
					  	$mecasa = 2;
						//echo $_SESSION['includepeep']."<br>";
					  	if ($_SESSION['includepeep'] == "") {
							 $query_indi2 = "SELECT id FROM users WHERE isactive = 1 AND registrantid=".$_SESSION['registrantid']." AND id=".$_SESSION['userid'];
						} else {
							$sqlbuild = explode(":", $_SESSION['includepeep']);
							foreach ($sqlbuild as $item) {
								if ($mecasa != 1) {
									$sqloutput = " AND idhash='".$item."'";
									$mecasa = 1;
								} else {
									$sqloutput .= " OR idhash='".$item."'";
								}
							}
							$query_indi2 = "SELECT id FROM users WHERE registrantid = ".$_SESSION['registrantid']." AND isactive = 1 ".$sqloutput." ORDER BY firstname ASC";
						}
						  $frameheight = 15;
						//  echo $query_indi2;
						  $result_indi2 = mysql_query($query_indi2);
						  while ($row_indi2 = mysql_fetch_array($result_indi2)) {
						  		$frameheight = $frameheight + 65;
								$counter++;
						  }
						  if ($counter == 1) {
						  	$frameheight = 190;
						  } elseif ($counter == 2) {
						  	$frameheight = 220;
						  }
						  //echo $frameheight;
						?>
					<form method="POST" action="thinline.php?id=<?php echo $gval[0].":".$allorworking.":".$gval[2].":".$gval[3].":".$gval[4].":".$gval[5].":".$gval[6].":".$gval[7].":".$gval[8].":".$gval[9].":::::::::::".$gval[20].""; ?>">
					<?php
						$mecasa = 2;
						if ($_SESSION['includepeep'] == "") {
							$query_indi2 = "SELECT firstname,lastname FROM users WHERE id = ".$_SESSION['userid'];
						} else {
							$sqlbuild = explode(":", $_SESSION['includepeep']);
							foreach ($sqlbuild as $item) {
								if ($mecasa != 1) {
									$sqloutput = " AND idhash='".$item."'";
									$mecasa = 1;
								} else {
									$sqloutput .= " OR idhash='".$item."'";
								}
							}
							$query_indi2 = "SELECT firstname,lastname FROM users WHERE registrantid = ".$_SESSION['registrantid']." AND isactive = 1 ".$sqloutput;
						}
						//echo $query_indi2."<br>";
						//echo $_SESSION['includepeep'];
					?> 
					  <table width="100%" cellpadding="3" cellspacing="3" border="0">
					  	<tr>
							<td>&nbsp;</td>
						  	<td class="multibackground">
								<img src="images/icons/calendar_32.png" align="left" hspace="3">
								Hold down the <strong>CTRL</strong> key and select the users you want displayed in the report.  After selecting the required
								users, please click the refresh button.
							</td>
							<td width="18%" class="multibackground">
								<?php
								if ($_SESSION['securityarrdept'][0] != "") {
									$deptcounter = 0;
									$addsqldepartments = '';
									foreach ($_SESSION['securityarrdept'] AS $deptval) {
										if ($deptcounter == 0) {
											$deptoperator = " AND (";
										} else {
											$deptoperator = " OR ";
										}
										$addsqldepartments .= $deptoperator." FIND_IN_SET('".$deptval."', departments)";
										$deptcounter++;
									}
									$addsqldepartments .= ")";
								}
								$query_indi2 = "SELECT id, firstname, lastname FROM users WHERE isactive = 1 ".$addsqldepartments." AND registrantid=".$_SESSION['registrantid']." ORDER BY firstname ASC";
						  		//echo $query_indi2;
							  	$result_indi2 = mysql_query($query_indi2) or Die ("Dead");?>
								<select multiple class="calmultiselect" name="users[]">
								<?php
							  	while ($row_indi2 = mysql_fetch_array($result_indi2)) {?>
							  		<option value="<?php echo strtoupper(md5($row_indi2['id'])) ?>"><?php echo $row_indi2['firstname'] ?> <?php echo $row_indi2['lastname'] ?></option>
							  <?php
							  	}?>
								</select>
							</td>
							<td width="30%" class="multibackground"><input type="submit" value="Refresh" style="font-family:Arial;font-size:14px"></td>
						</tr>
					  	<tr>
					  		<td align="right"><iframe src="calendar_names.php" width="100%" height="<?php echo $frameheight; ?>" frameborder="0" scrolling="auto" name="scheduleframe">You don't do iframes, please upgrade your browser!</iframe></td>
							<td width="80%" colspan="3"><iframe src="calendar_schedule.php?id=<?php echo date("m",$thetimestamp) ?>:2#todaysdate" width="100%" height="<?php echo $frameheight; ?>" frameborder="0" scrolling="auto" name="scheduleframe">You don't do iframes, please upgrade your browser!</iframe></td>
					  	</tr>
					  	<tr>
					  		<td colspan="4">&nbsp;</td>
					  	</tr>
					  </table>
					  <input type="hidden" name="_check" value="1">
					  </form>
					<?php 
					}?>
				</td>
			</tr>
			</table>
			<br><br>
			
