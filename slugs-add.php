<script language="JavaScript">
function submitform() {
    if (document.adddept.slug.value == '') {
        alert('You must enter a slug.');
    } else {
        document.adddept.submit();
    }
}
</script>

<form method="POST" name="adddept" action="settings.php?id=x48">
	<table width="100%" cellpadding="5" cellspacing="1" border="0">
	<tr>
		<td colspan="2">
			<table width="100%" cellpadding="2" cellspacing="2" border="0">
				<tr>
					<td colspan="2">
						<img src="images/icons/pin_yellow_24.png" align="left">
						&nbsp;<strong><font style="font-size:150%;font-family:Trebuchet MS">Add New Slug</font></strong>
						<br><br>
					</td>
				</tr>
				<tr>
					<td class="ls_on" width="10%">Franchise ID:</td>
					<td class="ls_on" >
						<?php
						$query = "SELECT u.firstname, u.lastname, d.department, d.id AS did
										FROM users u
										LEFT JOIN departments d ON d.id = u.departments
										LEFT JOIN security_templates st ON st.id = u.security_template
										WHERE u.registrantid =".RID."
										AND u.isactive = 1 
										AND d.id > 0
										ORDER BY d.department ASC ";
						//echo $query."<br>";
						$result = mysql_query($query);?>
						<select name="franchiseid" style="font-size:16px;padding:3px;width:50pc;">
						<?php
						while ($row = mysql_fetch_array($result)) { ?>
							<option value="<?php echo $row['did'];?>"><?php echo $row['department']." - ".$row['firstname']." ".$row['lastname']." (".$row['did'].")";?></option>
						<?php
						} ?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="ls_on" >Slug:</td>
					<td class="ls_on">
						<input style="font-size:16px;padding:3px;width:250px;" type="text" name="slug" id="slug" />
					</td>
				</tr>
				<tr>
					<td colspan=2><br><input onclick="javascript: submitform(); return false;" style="padding:3px;" type="submit" value="Add Slug"></td>
				</tr>
			</table>
		</td>
	</tr>
	</table>
	<input type="hidden" name="addslug" value="Add Slug" />
</form>