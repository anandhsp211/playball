<?php
include_once("_globalconnect.php");

// Can an include from globalconnect and manage their 
// permissions for this zone.  Because its a popup,
// we just close it down again if they dont have permission
// for that zone.
$onload =& permissions("TABCSELECT",1);
$prm_documents = permissions_multi("","DOCUMENTS");
//$prm_tickets = permissions_multi("","TICKETS");
$prm_messaging = permissions_multi("","MESSAGING");
$prm_leads = permissions_multi("","LEADS");

if ($_POST['processlogin'] == 1) {
	$query = "UPDATE users SET 
			 customer_menu_string = '".$_POST['addresses'].",".$_POST['contacts'].",".$_POST['tickets'].",".$_POST['activities'].",".$_POST['documents'].",".$_POST['myleads']."'
		 	 WHERE id = '".$_SESSION['userid']."'";
	mysql_query($query);
	$cookie = $_POST['addresses'].",".$_POST['contacts'].",".$_POST['tickets'].",".$_POST['activities'].",".$_POST['documents'].",".$_POST['myleads'];
	//  echo $cookie."<br>";
	setcookie('custmenu', '', time() + 999999);
	//  echo $_COOKIE['custmenu']."----";
	$onload = 'onload="window.parent.location = window.parent.location;self.close();return false;"';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "includes/xhtml1-transitional.dtd">
<html>
<head>
	<title>Removals Manager</title>
	<?php 
	if (ERES > 1024) { ?>
		<style type="text/css" media="all">@import "css/style.css";</style>
	<?php 
	} else { ?>
		<style type="text/css" media="all">@import "css/style_small.css";</style>
	<?php
	}?>
</head>
<body <?php echo $onload; ?>>
<form method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?>" name="updatetabs">
	<table cellspacing="0" cellpadding="0" border="0" width="93%">
	<tr>
		<td><img src="images/ss_logo.png" alt="Thinline Software" / align="center" border="0"></td>
		<td align="right"></td>
	</tr>
	</table>
	<table cellspacing="0" cellpadding="5" border="0" width="93%" height="300">
	<tr>
		<td rowspan="2"><img src="images/spacer.gif" height="1" width="40"></td>
		<td valign="center" rowspan="2">
			<img src="images/icons/more_tabs_32.png"><img src="images/spacer.gif" height="1" width="10"><br>
			<img src="images/spacer.gif" height="55" width="1">
		</td>
		<td valign="bottom">
			<br><br><br><br>
			<div id="menu_bar">
			<table id="Table_01" width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="85%" align="left"><strong><font style="font-size:150%;font-family:Trebuchet MS">Tab Selector</font></strong></td>
				<td width="70%">&nbsp;</td>
				<td><img src="images/menu/menu_01.gif" width="13" height="25" alt=""></td>
				<td><img src="images/menu/new_g.gif" width="18" height="25" border="0" alt=""></td>
				<td><img src="images/menu/edit_g.gif" width="18" height="25" border="0" alt="Edit Contact"></td>
				<td><img src="images/menu/open_g.gif" width="24" height="25" border="0" alt=""></td>
				<td><a href="javascript: submitform()"><img src="images/menu/save.gif" width="23" height="25" alt="Save Company Details" border="0"></a></td>
				<!--<td><img src="images/menu/sendmail_g.gif" width="22" height="25" alt=""></td>
				<td><img src="images/menu/menu_08.gif" width="8" height="25" alt=""></td>
				<td><img src="images/menu/print_g.gif" width="22" height="25" alt=""></td>-->
				<td><img src="images/menu/menu_08.gif" width="8" height="25" alt=""></td>
				<td><a href="<?php echo $_SERVER['PHP_SELF']?>?id=<?php echo $fullidvalues ?>"><img src="images/refresh.gif" border="0" alt="Refresh this page."></a></td>
				<td><img src="images/menu/menu_11.gif" width="9" height="25" alt=""></td>
				<td><img src="images/menu/menu_13.gif" width="18" height="25" alt=""></td>
			</tr>
			</table>
			</div>					
		</td>
	</tr>
	<tr>
		
		<td valign="top">
			Select the preferred tabs below.<br>
			<?php 
			$query = "SELECT customer_menu_string FROM users WHERE id= ".$_SESSION['userid'];
			$res = mysql_query($query);
			$row = mysql_fetch_array($res) or die ("Dead");
			$menu_string = $row['customer_menu_string'];
			
			function checkpresent($menu_string,$num)  {
				if (strrpos($menu_string, $num) !== false) {
					$active = "checked";
					return $active;
				}
			}
			?>
			<br>
			<script language="JavaScript">
			function submitform() {
				document.updatetabs.submit();
			}
			</script>
			<table width="100%" cellpadding="5">
			<tr>
				<td class="lightshade_on">Addresses:</td>
				<td class="lightshade_on"><input type="checkbox" value="1" name="addresses" <?php echo checkpresent($menu_string,"1") ?>></td>
				<td class="lightshade_on">Contacts:</td>
				<td class="lightshade_on"><input type="checkbox" value="2" name="contacts" <?php echo checkpresent($menu_string,"2") ?>></td>
				<td class="lightshade_on">Email/Text:</td>
				<?php 
				if ($prm_messaging['view'] != "MESSAGING") {
					$tickets_read = "disabled";
				}?>
				<td class="lightshade_on"><input <?php echo $tickets_read; ?> type="checkbox" value="3" name="tickets" <?php echo checkpresent($menu_string,"3") ?>></td>
			</tr>
			<tr>
				<td class="lightshade_on">Activities:</td>
				<td class="lightshade_on"><input type="checkbox" value="4" name="activities" <?php echo checkpresent($menu_string,"4") ?>></td>
				<td class="lightshade_on">Documents:</td>
				<?php 
				if ($prm_documents['customerdocsview'] != "DOCUMENTS") { 
					$documents_read = "disabled";
				}?>
				<td class="lightshade_on"><input <?php echo $documents_read; ?> type="checkbox" value="5" name="documents" <?php echo checkpresent($menu_string,"5") ?>></td>
				<td class="lightshade_on">Jobs:</td>
				<?php  
				if ($prm_leads['thezone'] != "LEADS") {
					$leads_read = "disabled";
				}?>
				<td class="lightshade_on"><input <?php echo $leads_read; ?> type="checkbox" value="7" name="myleads" <?php echo checkpresent($menu_string,"7") ?>></td>
			</tr>
			</table><br>
			<font style="font-family:tahoma;font-size:11px">
			You can select which tabs you would like displayed on the customer pages.  This is useful if you have a very low resolution screen
			or find that there are tabs which are used in frequently.<br><br>
			You are able to change or update your settings at any time.  These preferences will be loaded
			each time you log in to the system.
			</font>
		</td>
	</tr>
	</table>
<input type="hidden" name="processlogin" value="1">
</form>
</body>
</html>
