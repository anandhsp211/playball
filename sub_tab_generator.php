<?php
// Check if there is a menu manager entry for this user for this particular location
$sql = "SELECT id, menu_string FROM sub_tabs_select WHERE location = '" . $gval[21] . "' AND userid =" . UID . " AND registrantid =" . RID . " LIMIT 1";
//echo $sql."<br>";
$res = mysql_query($sql);
$row = mysql_fetch_array($res);
if (!$row['id']) {
    $menuupdate = 0;
} else {
    $menuupdate = 1;
    $menustring = $row['menu_string'];
    $menustring = explode(",", $menustring);
}
// Lets get the items allocated to this module
$query = "SELECT childtable, labelhashid, ispredefined, ismultilink FROM relations_matrix WHERE parenttable = '" . $gval[21] . "' AND isactive = 1 ORDER BY sortorder ASC";
//echo $query."<br>";
$getsubtabs = mysql_query($query);
$labelarray = array();
$labelarrayico = array();
$tablearray = array();
$iconarray = array();
while ($row = mysql_fetch_array($getsubtabs)) {
    if ($_SESSION['securityarrl2'][$row['childtable']] != 4) {
        if ($row['ispredefined'] == 1) {
            $getpre = "SELECT label, location FROM relations_predefined WHERE id = " . $row['childtable'] . " LIMIT 1";
            //echo $getpre."<br>";
            $preres = mysql_query($getpre);
            $prerow = mysql_fetch_array($preres);
            if (in_array($prerow['label'], $menustring) || ($menuupdate == 0)) {
                array_push($labelarray, $prerow['label']);
                if ($row['ismultilink'] == 1) {
                    $getico = "SELECT hashid, icon FROM menusub_tabs WHERE menu_tabhashid = '" . $prerow['location'] . "' AND type = 'new';";
                    $resgetico = mysql_query($getico);
                    $rowgetico = mysql_fetch_array($resgetico);
                    //echo $getico."<br />";
                    array_push($labelarrayico, $prerow['label'] . "&&1&&" . $rowgetico['icon'] . "&&5&&" . $rowgetico['hashid'] . "");
                } else {
                    array_push($labelarrayico, $prerow['label'] . "&&0&&Nada&&2");
                }
            }
        } elseif ($row['ispredefined'] == 2) {
            // for type product
            $generateinvoicebtn = 1;
            $getpre = "SELECT label, location FROM relations_predefined WHERE id = " . $row['childtable'] . " LIMIT 1";
            // echo $getpre."<br>";
            $preres = mysql_query($getpre);
            $prerow = mysql_fetch_array($preres);
            if (in_array($prerow['label'], $menustring) || ($menuupdate == 0)) {
                $sqlgetico = "SELECT icon, hashid FROM menusub_tabs WHERE menu_tabhashid  = '" . $prerow['location'] . "' AND type = 'new' AND registrantid =" . RID . " LIMIT 1";
                // echo $sqlgetico;
                $resgetico = mysql_query($sqlgetico);
                $rowgetico = mysql_fetch_array($resgetico);
                array_push($labelarray, $prerow['label']);
                array_push($labelarrayico, $prerow['label'] . "&&1&&" . $rowgetico['icon'] . "&&3&&" . $rowgetico['hashid'] . "");
            }
        } elseif ($row['ispredefined'] == 3) {
            // for type resource
            $getpre = "SELECT label, location FROM relations_predefined WHERE id = " . $row['childtable'] . " LIMIT 1";
            // echo $getpre."<br>";
            $preres = mysql_query($getpre);
            $prerow = mysql_fetch_array($preres);
            if (in_array($prerow['label'], $menustring) || ($menuupdate == 0)) {
                $sqlgetico = "SELECT icon, hashid FROM menusub_tabs WHERE menu_tabhashid  = '" . $prerow['location'] . "' AND type = 'new' AND registrantid =" . RID . " LIMIT 1";
                // echo $sqlgetico;
                $resgetico = mysql_query($sqlgetico);
                $rowgetico = mysql_fetch_array($resgetico);
                array_push($labelarray, $prerow['label']);
                array_push($labelarrayico, $prerow['label'] . "&&1&&" . $rowgetico['icon'] . "&&4&&" . $rowgetico['hashid'] . "");
            }
        } else {
            $getpre = "SELECT tablabel, icon FROM udf_definitions WHERE hashid = '" . $row['labelhashid'] . "' LIMIT 1";
            //echo $getpre."<br>";
            $preres = mysql_query($getpre);
            $prerow = mysql_fetch_array($preres);
            if (in_array($prerow['tablabel'], $menustring) || ($menuupdate == 0)) {
                array_push($labelarray, $prerow['tablabel']);
                array_push($labelarrayico, $prerow['tablabel'] . "&&1&&" . $prerow['icon'] . "&&1");
                array_push($tablearray, $row['childtable']);
            }
        }

    }
}

$num_rows = count($labelarray);
if ($num_rows > 0) {
// we are going to generate the invoice button outside of the tabs
// to simplify things
if ($generateinvoicebtn == 1) { ?>
    <script language="JavaScript">
        <!--
        function saveproducts(x) {
            document.product_manager.submit();
        }

        function validate_form_purchasing() {
            if ((document.product_manager.tlsrecidinvoiceddate.value == "") || (document.product_manager.supplierinvoiceamountexvat.value == "") || (document.product_manager.supplierinvoiceamountincvat.value == "")) {
                return confirm("Please confirm you do not want to enter the supplier invoice values");
            }
            return true;
        }

        function validate_form_sales() {
            if (document.product_manager.tlsrecidinvoiceddate.value == "") {
                alert("Please enter an invoice date");
                return false;
            }
            return true;
        }

        //-->
    </script>
    <?php
} ?>

<div id="big-tabs">
    <ul>
        <?php }
        $runonce = 0;
        $tabinexistence = 0; ?>

        <script>
            var castsTimeout;

            function myhide(elem) {
                var item = document.getElementById(elem);
                if (item) {
                    item.style.display = 'none';
                }
            }

            function myshow(elem) {
                var item = document.getElementById(elem);
                if (item) {
                    item.style.display = '';
                    createCookie('tls_tab_display<?php echo $gval[21] ?>', elem, 5);
                }
            }

            function selecttab(tab) {
                var tabs = tab.parentNode.parentNode.childNodes;
                for (var i = 0; i < tabs.length; i++) {
                    if (tabs[i].tagName == "LI") {
                        tabs[i].className = "";
                    }
                }
                tab.parentNode.className = "selected";
            }
        </script>

        <?php
        //print_r($labelarrayico);
        foreach ($labelarrayico as &$value) {
            $newval = explode("&&", $value);
            // First thing we need to do is see if the current cookie tabs exists for
            // this module, if so, no problem, if not then make the first tab display
            if ($runonce == 0) {
                foreach ($labelarrayico as &$value2) {
                    $newval2 = explode("&&", $value2);
                    if ($_COOKIE["tls_tab_display" . $gval[21]] == md5($newval2[0])) {
                        $tabinexistence = 1;
                    }
                }
                $runonce = 1;
            }

            if ($_COOKIE["tls_tab_display" . $gval[21]] == md5($newval[0]) || ($tabinexistence == 0)) {
                $classtab = "selected";
                $tabinexistence = 1;
            }

            else {
                $classtab = "tabs";
            } ?>

        <li class="<?php echo $classtab ?>"><a href="#<?php echo $newval[0] ?>" onclick="

        <?php
        for ($number = 0; $number < $num_rows; $number++) {
            if ($newval[0] == $labelarray[$number]) { ?>
                    myshow('<?php echo md5($labelarray[$number]); ?>');
            <?php } else { ?>
                    myhide('<?php echo md5($labelarray[$number]); ?>');
            <?php }
        } ?>
                selecttab(this); return false;">

            <?php
            if ($newval[1] == 1) {
                // Because it is a dynamic field we need to get the icon
                // In non-predefined the icons is stored as a number and
                // in predefined 2 it is stored as a filename
                //echo $newval[3];
                if ($newval[3] == 1) {
                    $sqlico = "SELECT size, filename FROM icons_user WHERE id=" . $newval[2];
                    $resico = mysql_query($sqlico);
                    $rowico = mysql_fetch_array($resico);
                    $icon_name = $rowico['filename'];
                    $icons_size = $rowico['size'];
                } else {
                    $icon_name = $newval[2];
                    $icons_size = 16;
                }
                ?>

                <style>
                    div#big-tabs span.udf_<?php echo md5($newval[2]) ?> {
                        background: url(images/icons/<?php echo $icons_size."_shadow/".$icon_name ?>) no-repeat center left;
                    }
                </style>

                <span class="icon udf_<?php echo md5($newval[2]) ?>"><?php echo $newval[0]; ?></span></a></li>
                <?php
            } else { ?>
                <span class="icon <?php echo strtolower($newval[0]) ?>"><?php echo $newval[0]; ?></span></a></li>
                <?php
            } ?>
            <?php
        }
        if ($num_rows > 0) { ?>
        <li class="tabs">
            <a title="Tab Selector" href="sub_tab_selector.php?id=<?php echo $gval[21] ?>" <?php echo LFD ?>><span class="icon tabs">&nbsp;</span></a>
        </li>
    </ul>
    <?php
    } ?>
</div>
<div class="clear"></div>
<?php
$num_rows = count($labelarray);
include_once("sub_tab_functions.php");
$tablecounter = 0;
$runonce = 0;
$tabinexistence = 0;
$generalcount = 0;
foreach ($labelarray as &$value) {
    // First thing we need to do is see if the current cookie tabs exists for
    // this module, if so, no problem, if not then make the first tab display
    if ($runonce == 0) {
        foreach ($labelarrayico as &$value2) {
            $newval2 = explode("&&", $value2);
            if ($_COOKIE["tls_tab_display" . $gval[21]] == md5($newval2[0])) {
                $tabinexistence = 1;
            }
        }
        $runonce = 1;
    }

    if ($_COOKIE["tls_tab_display" . $gval[21]] == md5($value) || ($tabinexistence == 0)) {
        $dizplay = 'class="selected"';
        $tabinexistence = 1;
    } else {
        $dizplay = 'style="display: none;"';
    }
    ?>
    <div id="<?php echo md5($value) ?>" <?php echo $dizplay; ?>>
        <div class="list-content">
            <br>
            <?php
            $predefval = explode("&&", $labelarrayico[$generalcount]);
            //print_r($predefval);
            // if (($value == "Messaging") || ($value == "Documents") || ($value == "Activities") || ($value == "Addresses")) {
            // 	echo $predefval[3];
            if ($predefval[3] == 2) {
                // First check if the value is a standard element
                // And then deliver the appropriate output
                if ($gval[1] != "") {
                    if ($value == "Messaging") {
                        displayMessaging($revfull, $thisrecordhashid);
                    } elseif ($value == "Documents") {
                        displaydocuments($revfull, $thisrecordhashid);
                    } elseif ($value == "Activities") {
                        displayactivities($revfull, $thisrecordhashid);
                    } elseif ($value == "Addresses") {
                        displayaaddresses($revfull, $thisrecordhashid);
                    } elseif ($value == "Pricing") {
                        pricingmodels($revfull, $thisrecordhashid);
                    } elseif ($value == "Notes") {
                        displaynotes($revfull, $thisrecordhashid);
                    } elseif ($value == "Lessons") {
                        displaysessions($revfull, $thisrecordhashid, $campsswitch);
                    } elseif ($value == "College") {
                        displaycollege($revfull, $thisrecordhashid);
                    } elseif ($value == "Payments") {
                        displayPayments($revfull, $thisrecordhashid);
                        // to change the icon make the name the same as the item in the tabs.css file
                    }
                } else {
                    echo "<font class='note'><a href='javascript: submitform(this);'>This record must be saved first</a></font>";
                    echo "</form>"; // form ends here if the record has not been saved to ensure that all compulsory fields are done.
                }
            } elseif ($predefval[3] == 5) {
                // This is the multi-link tiey
                include_once('multilink-output.php');
            } elseif ($predefval[3] == 4) {
                // for type resource ?>
                <form method="POST" action="thinline.php?id=<?php echo $revfull ?>">
                    <script type='text/javascript' src='/js/update_resource.js'></script>
                    <table class="note" border="0" cellpadding="3" cellspacing="2" width="100%">
                        <tr>
                            <td colspan="7">
                                <a href="thinline.php?id=14<?php echo ":" . $gval[1] . ":" . $gval[2] . ":" . $gval[3] . ":" . $gval[4] . ":" . $gval[5] . ":" . $gval[6] . ":" . $gval[7] . ":" . $gval[8] . ":" . $gval[9] . ":" . $gval[10] . ":" . $gval[11] . ":" . $gval[12] . ":" . $gval[13] . ":" . $gval[14] . ":" . $gval[15] . ":" . $gval[16] . ":" . $gval[17] . ":" . $gval[18] . ":" . $gval[19] . ":" . $gval[20] . ":" . $gval[21] . ":" . $gval[22] . ":" . $gval[23] . ":" . $gval[24] . ":0"; ?>"><font
                                            class="note">Add Date Charged</font></a> |
                                <a href="thinline.php?id=14<?php echo ":" . $gval[1] . ":" . $gval[2] . ":" . $gval[3] . ":" . $gval[4] . ":" . $gval[5] . ":" . $gval[6] . ":" . $gval[7] . ":" . $gval[8] . ":" . $gval[9] . ":" . $gval[10] . ":" . $gval[11] . ":" . $gval[12] . ":" . $gval[13] . ":" . $gval[14] . ":" . $gval[15] . ":" . $gval[16] . ":" . $gval[17] . ":" . $gval[18] . ":" . $gval[19] . ":" . $gval[20] . ":" . $gval[21] . ":" . $gval[22] . ":" . $gval[23] . ":" . $gval[24] . ":1"; ?>"><font
                                            class="note">Add Hourly Charged</font></a>
                                <br><br>
                            </td>
                            <td align="right"><input type="submit" value="Confirm"
                                                     style="font-family:Tahoma;font-size:11px"><br><br></td>
                        </tr>
                        <tr align="center">
                            <td class="ls_top" align="center"><strong>From</strong></td>
                            <td class="ls_top" align="center"><strong>To</strong></td>
                            <td width="100" class="ls_top" align="center"><strong>Charge<br>Type</strong></td>
                            <td class="ls_top"><strong>Item Name</strong></td>
                            <td class="ls_top" width="10%"><strong>Heads</strong><br></td>
                            <td class="ls_top"><strong>Rate</strong></td>
                            <td class="ls_top"><strong>Total</strong></td>
                            <td width="80" class="ls_top"><strong>Actions</strong></td>
                        </tr>
                        <?php
                        $querymsh = "SELECT * FROM resource_allocation WHERE location ='" . $gval[21] . "' AND recordid  = '" . $gval[1] . "' ORDER BY fromdate DESC";
                        // echo $querymsh."<br>";
                        $resmsh = mysql_query($querymsh);
                        $classcounter = 0;
                        $id_counter = 0;
                        while ($row = mysql_fetch_array($resmsh)) {
                            // We need to get the resource name and other information
                            $querygetrec = "SELECT * FROM udf_7647966B7343C29048673252E490F736 WHERE hashid = '" . $row['resourceid'] . "' LIMIT 1";
                            // echo $querymsh."<br>";
                            $resgetrec = mysql_query($querygetrec);
                            $rowgetrec = mysql_fetch_array($resgetrec);
                            $resourcename = $rowgetrec['resourcename'];
                            $chargeforeach = $rowgetrec['chargeforeach'];
                            $resourcerecid = $rowgetrec['hashid'];
                            include("includes/ls.php"); ?>
                            <tr>
                                <?php
                                if ($row['chargetype'] == 1) {
                                    $addtime = "H:i";
                                    $addtime_2 = ",showsTime:true,timeFormat:\"24\"";
                                    $addtime_3 = "%H:%M";
                                    $chartyledisplay = "Hourly";
                                } else {
                                    $chartyledisplay = "Date";
                                } ?>
                                <?php
                                // if the resource is allocated we lock down the date change
                                if ($row['resourceid'] == 'undefined') { ?>
                                    <td class="ls_<?php echo $ls ?>" align="left">
                                        <input onchange="updateresource(this.value,'fromdate','<?php echo $row['id'] ?>',1,<?php echo $id_counter ?>,'<?php echo $chargeforeach ?>');"
                                               class="standardfield_date_srch" type="text"
                                               name="recfromdate_<?php echo $id_counter ?>"
                                               id="recfromdate_<?php echo $id_counter ?>" readonly
                                               value="<?php echo date("d-M-Y " . $addtime . "", strtotime($row['fromdate'])); ?>">
                                        &nbsp;<img src="images/icons/calendar.png"
                                                   id="f_trigger_c_start_rec_<?php echo $id_counter ?>"
                                                   style="cursor: pointer;">
                                        <script language="javascript">Calendar.setup({
                                                inputField: "recfromdate_<?php echo $id_counter ?>",
                                                button: "f_trigger_c_start_rec_<?php echo $id_counter ?>",
                                                align: "Ll00",
                                                ifFormat: "%d-%b-%Y <?php echo $addtime_3 ?>"<?php echo $addtime_2 ?> });</script>
                                        <span style="color:red;" id="txtHint"></span>
                                    </td>
                                    <td class="ls_<?php echo $ls ?>" align="left">
                                        <input onchange="updateresource(this.value,'todate','<?php echo $row['id'] ?>',1,<?php echo $id_counter ?>,'<?php echo $chargeforeach ?>');"
                                               class="standardfield_date_srch" type="text"
                                               name="rectodate_<?php echo $id_counter ?>"
                                               id="rectodate_<?php echo $id_counter ?>" readonly
                                               value="<?php echo date("d-M-Y " . $addtime . "", strtotime($row['todate'])); ?>">
                                        &nbsp;<img src="images/icons/calendar.png"
                                                   id="f_trigger_c_end_rec_<?php echo $id_counter ?>"
                                                   style="cursor: pointer;">
                                        <script language="javascript">Calendar.setup({
                                                inputField: "rectodate_<?php echo $id_counter ?>",
                                                button: "f_trigger_c_end_rec_<?php echo $id_counter ?>",
                                                align: "Ll00",
                                                ifFormat: "%d-%b-%Y <?php echo $addtime_3 ?>"<?php echo $addtime_2 ?> });</script>
                                    </td>
                                    <?php
                                    // Need to create a recordidstring
                                    $recordidstring .= $row['id'] . ",";
                                } else { ?>
                                    <td align="left" class="ls_<?php echo $ls ?>">
                                        <?php echo date("d-M-Y " . $addtime . "", strtotime($row['fromdate'])); ?>
                                    </td>
                                    <td align="left" class="ls_<?php echo $ls ?>">
                                        <?php echo date("d-M-Y " . $addtime . "", strtotime($row['todate'])); ?>
                                    </td>
                                    <?php
                                } ?>
                                <td class="ls_<?php echo $ls ?>" align="left">
                                    <?php echo $chargeforeach ?>&nbsp;
                                </td>
                                <td class="ls_<?php echo $ls ?>" align="center">
                                    <?php
                                    // if the resource is allocated we lock down the date change
                                    if ($row['resourceid'] == 'undefined') { ?>
                                        <a href="assign_resource.php?id=1:<?php echo $gval[1] ?>:<?php echo $gval[21] . ":" . $row['id']; ?>" <?php echo LFD ?>><font
                                                    class="note">Add Item</font></a>
                                        <?php
                                    } else {
                                        echo $resourcename;
                                    } ?>

                                </td>
                                <td class="ls_<?php echo $ls ?>" align="center">
                                    <?php
                                    $pos = strrpos($chargeforeach, "Person");
                                    if ($pos === false) { // note: three equal signs
                                        echo "&nbsp;";
                                    } else { ?>
                                        <input onkeyup="updateresource(this.value,'heads','<?php echo $row['id'] ?>',2,<?php echo $id_counter ?>,'<?php echo $chargeforeach ?>');"
                                               type="text" name="heads_<?php echo $id_counter; ?>"
                                               style="font-family:Tahoma;font-size:11px;width:35px;text-align:center"
                                               value="<?php echo $row['heads'] ?>">
                                        <?php
                                    } ?>
                                </td>
                                <td class="ls_<?php echo $ls ?>" align="center">
                                    <?php
                                    if ($row['resourceid'] == 'undefined') { ?>
                                        &nbsp;
                                        <?php
                                    } else { ?>
                                        <select onchange="updateresource(this.value,'rate','<?php echo $row['id'] ?>',2,<?php echo $id_counter ?>,'<?php echo $chargeforeach ?>');"
                                                style="font-family:Tahoma;font-size:11px;text-align:center;width:70px">
                                            <?php
                                            // Lets get the rates for this particular resource
                                            $querygetprice = "SELECT * FROM pricing WHERE location = '7647966B7343C29048673252E490F736' AND recordid = '" . $resourcerecid . "' AND isactive = 1";
                                            // echo $querygetprice."<br>";
                                            $resgetprice = mysql_query($querygetprice);
                                            while ($rowgetprice = mysql_fetch_array($resgetprice)) {
                                                if ($row['rate'] == $rowgetprice['price']) {
                                                    $selectmenow = "selected";
                                                    $selectedrate = $rowgetprice['price'];
                                                } ?>
                                                <option <?php echo $selectmenow ?>
                                                        value="<?php echo $rowgetprice['price'] ?>"><?php echo $rowgetprice['price'] ?></option>
                                                <?php
                                                unset($selectmenow);
                                            } ?>
                                        </select>
                                        <?php
                                    } ?>
                                </td>
                                <td align="center" class="ls_<?php echo $ls ?>">
                                    <?php
                                    // Let's start calcultating some totals
                                    $diff = strtotime($row['todate']) - strtotime($row['fromdate']);
                                    if ($chargeforeach == "Hour") {
                                        $subtotal = number_format((($diff / 60) / 60) * $selectedrate, 2);
                                    } elseif ($chargeforeach == "Night") {
                                        $totalnights = round(($diff / 60) / 60 / 24) - 1;
                                        $subtotal = number_format(($totalnights * $selectedrate), 2);
                                    } elseif ($chargeforeach == "Night and Person") {
                                        $totalnights = round(($diff / 60) / 60 / 24) - 1;
                                        $subtotal = number_format(($totalnights * $selectedrate) * $row['heads'], 2);
                                    } elseif ($chargeforeach == "Day") {
                                        $totalnights = round(($diff / 60) / 60 / 24);
                                        $subtotal = number_format(($totalnights * $selectedrate), 2);
                                    } elseif ($chargeforeach == "Day and Person") {
                                        $totalnights = round(($diff / 60) / 60 / 24);
                                        $subtotal = number_format(($totalnights * $selectedrate) * $row['heads'], 2);
                                    } elseif ($chargeforeach == "Month") {
                                        $totalnights = round(($diff / 60) / 60 / 24 / 30);
                                        if ($totalnights < 1) {
                                            $totalnights = 1;
                                        }
                                        $subtotal = number_format(($totalnights * $selectedrate), 2);
                                    } elseif ($chargeforeach == "Month and Person") {
                                        $totalnights = round(($diff / 60) / 60 / 24 / 30);
                                        if ($totalnights < 1) {
                                            $totalnights = 1;
                                        }
                                        $subtotal = number_format(($totalnights * $selectedrate) * $row['heads'], 2);
                                    } elseif ($chargeforeach == "Person") {
                                        $subtotal = number_format($selectedrate * $row['heads'], 2);
                                    } elseif ($chargeforeach == "Week") {
                                        $totalnights = round(($diff / 60) / 60 / 24 / 7);
                                        $subtotal = number_format(($totalnights * $selectedrate), 2);
                                    } elseif ($chargeforeach == "Week and Person") {
                                        $totalnights = round(($diff / 60) / 60 / 24 / 7);
                                        $subtotal = number_format(($totalnights * $selectedrate) * $row['heads'], 2);
                                    } ?>
                                    <span id="txtHint<?php echo $id_counter ?>"
                                          style="font-weight:bold"><?php echo $subtotal ?></span>&nbsp;
                                </td>
                                <td class="ls_<?php echo $ls ?>" align="center"><a
                                            onclick="return confirm('Are you sure you want delete this item?')"
                                            href="thinline.php?id=15:<?php echo $gval[1] . ":" . $gval[2] . ":" . $gval[3] . ":" . $gval[4] . ":" . $gval[5] . ":" . $gval[6] . ":" . $gval[7] . ":" . $gval[8] . ":" . $gval[9] . ":" . $gval[10] . ":" . $gval[11] . ":" . $gval[12] . ":" . $gval[13] . ":" . $gval[14] . ":" . $gval[15] . ":" . $gval[16] . ":" . $gval[17] . ":" . $gval[18] . ":" . $gval[19] . ":" . $gval[20] . ":" . $gval[21] . ":" . $gval[22] . ":" . $gval[23] . ":" . $gval[24] . ":" . $row['id']; ?>"><font
                                                class="note">Delete</font></a></td>
                            </tr>
                            <?php
                            $id_counter++;
                            unset($addtime, $addtime_2, $chartyledisplay, $resourcerecid, $selectedrate, $subtotal);
                        } ?>
                    </table>
                    <input type="hidden" name="pform" value="4">
                </form>
                <?php
            } elseif ($predefval[3] == 3) {
                // for type product
                if ($gval[1] != "") {
                    $getcomptabtype = "SELECT componentabtype FROM menusub_tabs WHERE hashid = '" . $gval[21] . "' AND menu_tabhashid = '" . $gval[20] . "' AND registrantid = " . RID;
                    // echo $getcomptabtype."<br><br>";
                    $restt = mysql_query($getcomptabtype);
                    $rowtt = mysql_fetch_array($restt);
                    if ($rowtt['componentabtype'] == 1) {
                        $complevel = 1;
                        $table_s_name = "sales_";
                        $colspan_1 = 10;
                        $colspan_2 = 7;
                    } elseif ($rowtt['componentabtype'] == 2) {
                        $complevel = 2;
                        $table_s_name = "purchases_";
                        $colspan_1 = 11;
                        $colspan_2 = 8;
                    }

                    // echo $complevel;?>
                    <a name="salestabs"></a>
                    <form method="POST" name="product_manager"
                          action="thinline.php?id=<?php echo $revfull; ?>#salestabs">
                        <script type='text/javascript' src='/js/update_product.js'></script>
                        <table class="note" border="0" cellspacing="2" cellpadding="2" width="100%">
                            <?php
                            // Lets see if this record has been invoiced
                            $isinvoiced = "SELECT * FROM udf_" . $gval[21] . " WHERE issaved = 1 AND hashid = '" . $gval[1] . "' AND registrantid = " . RID . " LIMIT 1";
                            // echo $isinvoiced;
                            $resisinvoiced = mysql_query($isinvoiced);
                            $rowisinvoiced = mysql_fetch_array($resisinvoiced);
                            $is_invoiced = $rowisinvoiced['tlsrecidinvoiced'];
                            $date_invoiced = $rowisinvoiced['tlsrecidinvoiceddate'];
                            $supplier_invoiced_amountexvat = $rowisinvoiced['supplierinvoiceamountexvat'];
                            $supplier_invoiced_amountincvat = $rowisinvoiced['supplierinvoiceamountincvat'];
                            // echo $is_invoiced;
                            if (($is_invoiced != 1) || (ISMASTER == 1)) { ?>
                                <tr>
                                    <td colspan="<?php echo $colspan_1 ?>">
                                        <table class="note" border="0" cellspacing="3" cellpadding="0" width="100%">
                                            <tr>
                                                <td>
                                                    <a href="add_products.php?id=<?php echo $complevel ?>:<?php echo $predefval[4] ?>:::::::::<?php echo $gval[1] ?>:::::::::::<?php echo $gval[21] ?>" <?php echo LFD ?>><font
                                                                class="note">Add Items</font></a>
                                                    | <a id="AddProducts" onclick="saveproducts('save');"
                                                         href="thinline.php?id=10:<?php echo $gval[1] ?>:<?php echo $complevel ?>::<?php echo $gval[4] ?>:::::<?php echo $gval[9] ?>:::::::::<?php echo $gval[18] ?>:<?php echo $gval[19] ?>:<?php echo $gval[20] ?>:<?php echo $gval[21] ?>:<?php echo $gval[22] ?>:<?php echo $gval[23] ?>:"><font
                                                                class="note">Add Blank Item</font></a>
                                                    <?php
                                                    if (($is_invoiced == 1) && (ISMASTER == 1)) {
                                                        echo " | <font class='note' color='green'>Order has been Invoiced</font>";
                                                    } ?>
                                                    <br><br style="line-height:8px">
                                                </td>
                                                <?php
                                                if ($_GET['period'] == "closed") { ?>
                                                    <td width="30%" class="messageboard">
                                                        <font class=="note">This sales period is closed. Please assign
                                                            to an open sales period.<br></font>
                                                    </td>
                                                    <td width="15%">&nbsp;</td>
                                                    <?php
                                                } ?>
                                                <td class="ls_top" width="10%" align="center">
                                                    <?php
                                                    // Need to get the terms from the parent company...
                                                    $getterms = "SELECT termsdays FROM udf_" . $orderingtable . " WHERE issaved = 1 AND hashid = '" . $orderingid . "'";
                                                    // echo $getterms;
                                                    $restt = mysql_query($getterms);
                                                    $rowtt = mysql_fetch_array($restt);
                                                    if ($rowtt['termsdays'] == "") {
                                                        $setterms = 0;
                                                    } else {
                                                        $setterms = $rowtt['termsdays'];
                                                    }
                                                    ?>
                                                    <font class="note">Terms (Days)</font><br><input type="text"
                                                                                                     style="text-align:right;font-family:Tahoma;font-size:11px;width:32%"
                                                                                                     name="termsdays"
                                                                                                     value="<?php echo $setterms; ?>">
                                                </td>
                                                <?php
                                                $stamp = time();
                                                $todaysdate = date("d-M-Y", $stamp);
                                                if (($gval[14] == "") && ($date_invoiced == "")) {
                                                    $setthedate = $todaysdate;
                                                } else {
                                                    $setthedate = $gval[14];
                                                    if ($date_invoiced != "") {
                                                        $todaysdate = explode("-", $date_invoiced);
                                                        $setthedate = $todaysdate[2] . "-" . convertDate($todaysdate[1]) . "-" . $todaysdate[0];
                                                    }
                                                } ?>
                                                <td class="ls_top" width="15%" align="center"><font class="note">Invoice
                                                        Date</font> <img src="images/spacer.gif" width="25"
                                                                         height="1"><br>
                                                    <input class="standardfield_date" type="text"
                                                           style="text-align:right;font-family:Tahoma;font-size:11px;width:52%"
                                                           name="tlsrecidinvoiceddate" id="tlsrecidinvoiceddate"
                                                           readonly value="<?php echo $setthedate ?>"> &nbsp;<img
                                                            src="images/icons/calendar.png" id="f_trigger_c_start"
                                                            style="cursor: pointer;" title="Date selector">
                                                    <script language="javascript">Calendar.setup({
                                                            inputField: "tlsrecidinvoiceddate",
                                                            button: "f_trigger_c_start",
                                                            align: "Ll00",
                                                            ifFormat: "%d-%b-%Y"
                                                        });</script>
                                                    <?php
                                                    //if ($complevel == 2) { ?>
                                                    <!--<td class="ls_top" width="20%" align="center">Supplier Invoice Amount Ex. Tax<br><input type="text" style="text-align:right;font-family:Tahoma;font-size:11px;width:72%" name="supplierinvoiceamountexvat"></td>
                                                    <td class="ls_top" width="20%" align="center">Supplier Invoice Amount Incl. Tax<br><input type="text" style="text-align:right;font-family:Tahoma;font-size:11px;width:75%" name="supplierinvoiceamountincvat"></td>-->
                                                    <?php
                                                    //	$validationjsp = "validate_form_purchasing();";
                                                    //} else {
                                                    $validationjsp = "validate_form_sales();";
                                                    //} ?>
                                                <td align="right"><input onclick="return <?php echo $validationjsp; ?>"
                                                                         type="submit"
                                                                         style="font-family:Tahoma;font-size:11px"
                                                                         name="processorder_2" value="Order Completed">
                                                    &nbsp;<br><br style="line-height:10px"></td>
                                            </tr>
                                        </table>
                                        <?php
                                        if ($complevel == 2) { ?>
                                            <br>
                                            <?php
                                        } ?>
                                    </td>
                                </tr>
                                <?php
                            } else { ?>
                                <?php
                                //if ($complevel == 1) { ?>
                                <tr>
                                    <td colspan="11">
                                        <table class="note" cellpadding="5" cellspacing="3">
                                            <tr>
                                                <?php
                                                $date_invoiced_maintain = $date_invoiced;
                                                $date_invoiced = explode("-", $date_invoiced);
                                                $date_invoiced = $date_invoiced[2] . "-" . convertdate($date_invoiced[1]) . "-" . $date_invoiced[0]; ?>
                                                <td class="messageboard">Invoice date:</td>
                                                <td class="messageboard"><strong><?php echo $date_invoiced ?></strong>
                                                </td>
                                                <td>&nbsp;</td>
                                                <td class="messageboard">Invoice due date:</td>
                                                <td class="messageboard">
                                                    <strong><?php echo date("d-M-Y", (strtotime($date_invoiced) + ($rowisinvoiced['tlsinvoiceterms'] * 86400))); ?></strong>
                                                </td>
                                            </tr>
                                        </table>
                                        <br><br style="line-height:2px">
                                    </td>
                                </tr>
                                <?php
                                //	} elseif ($complevel == 2) { ?>
                                <!--	<tr>
											<td colspan="11">
												<table cellpadding="5" cellspacing="2">
												<tr>
													<td align="center" class="messageboard">Suppliers Invoice Amount EX Tax: </td>
													<td align="center" class="messageboard"><strong><?php echo number_format($supplier_invoiced_amountexvat, 2); ?></strong></td>
													<td>&nbsp;</td>
													<td align="center" class="messageboard">Suppliers Invoice Amount Incl. Tax: </td>
													<td align="center" class="messageboard"><strong><?php echo number_format($supplier_invoiced_amountincvat, 2); ?></strong></td>
												</tr>
												</table>
												<br><br style="line-height:2px">
											</td>
										</tr>-->
                                <?php
                                //	} ?>

                                <?php
                            } ?>
                            <tr>
                                <td class="ls_top" align="center" style='font-size:11px;'><strong>Item<br>Code</strong>
                                </td>
                                <td class="ls_top" align="center" style='font-size:11px;'><strong>Item Name</strong>
                                </td>
                                <td class="ls_top" align="center" style='font-size:11px;'><strong>Commission
                                        Code</strong></td>
                                <td class="ls_top" align="center" style='font-size:11px;'><strong>Tax
                                        Code<br>(%)</strong></td>
                                <td class="ls_top" align="center" style='font-size:11px;'>
                                    <strong>Discount<br>(%)</strong></td>
                                <td class="ls_top" align="center" style='font-size:11px;'><strong>Quantity</strong></td>
                                <?php
                                if ($complevel == 2) {
                                    // As this field if we are dealing with a purchase order ?>
                                    <td class="ls_top" align="center"><strong>Received</strong></td>
                                    <?php
                                } ?>
                                <td class="ls_top" align="center" width="10%" style='font-size:11px;'><strong>Unit<br>Price</strong>
                                </td>
                                <td class="ls_top" align="center" style='font-size:11px;'><strong>Sales<br>Price
                                        (Ex)</strong></td>
                                <td class="ls_top" align="center" style='font-size:11px;'><strong>Sales<br>Price (Incl)</strong>
                                </td>
                                <td class="ls_top" align="center" style='font-size:11px;'><strong>Actions</strong></td>
                            </tr>
                            <?php
                            // lets get the line items
                            $getcomptabtype = "SELECT * FROM " . $table_s_name . RID . " WHERE recordid = '" . $gval[1] . "' AND location = '" . $gval[21] . "' AND registrantid = " . RID . " ORDER BY id ASC";
                            // echo $getcomptabtype."<br><br>";
                            $restt = mysql_query($getcomptabtype);
                            $id_counter = 100;
                            // to lock down the transaction for previous montth
                            $alerterformonthend = 0;
                            $date1 = time();
                            $todays_date_check = date("Y-m", $date1);
                            $todays_date_check = explode("-", $todays_date_check);
                            $date_of_invoice = explode("-", $date_invoiced_maintain);
                            if (!ISMASTER) {
                                if ((($todays_date_check[0] == $date_of_invoice[0]) && ($todays_date_check[1] == $date_of_invoice[1])) || ($date_invoiced_maintain == "")) {
                                    $alerterformonthend = 0;
                                } else {
                                    $alerterformonthend = 1;
                                }
                            }
                            while ($rowtt = mysql_fetch_array($restt)) {
                                include("includes/ls.php");
                                $is_refunded = $rowtt['isrefunded'];
                                ?>
                                <tr>
                                    <td class="ls_<?php echo $ls ?>">&nbsp;
                                        <?php
                                        if ((($alerterformonthend == 0) && ($is_refunded != 1)) || (ISMASTER)) { ?>
                                            <input onkeyup="updateproduct(this.value,'<?php echo $gval[1] ?>','<?php echo $complevel ?>','<?php echo $rowtt['id'] ?>','productcode','<?php echo $id_counter ?>','<?php echo $gval[21] ?>');"
                                                   type="text" style="font-family:Tahoma;font-size:11px;width:90%"
                                                   name="productcode" value="<?php echo $rowtt['productcode']; ?>">
                                            <?php
                                        } else {
                                            echo $rowtt['productcode'];
                                        } ?>
                                    </td>
                                    <td class="ls_<?php echo $ls ?>">
                                        <?php
                                        if ((($alerterformonthend == 0) && ($is_refunded != 1)) || (ISMASTER)) { ?>
                                        <input onkeyup="updateproduct(this.value,'<?php echo $gval[1] ?>','<?php echo $complevel ?>','<?php echo $rowtt['id'] ?>','productname','<?php echo $id_counter ?>','<?php echo $gval[21] ?>');"
                                               type="text" name="productname"
                                               style="font-family:Tahoma;font-size:11px;width:95%"
                                               value="<?php echo $rowtt['productname'] ?>"></td>
                                    <?php
                                    } else {
                                        echo $rowtt['productname'];
                                    } ?>
                                    <td class="ls_<?php echo $ls ?>">&nbsp;<span style="color:red;"
                                                                                 id="txtHintproduct"></span>
                                        <?php
                                        if ((($alerterformonthend == 0) && ($is_refunded != 1)) || (ISMASTER)) { ?>
                                            <?php echo generateProductSelect($predefval[4], 'Account Code', 'accountcode', $rowtt['accountcode'], $resutlstring, $complevel, $rowtt['id'], $id_counter); ?>&nbsp;
                                            <?php
                                        } else {
                                            echo $rowtt['accountcode'];
                                        } ?>
                                    </td>
                                    <td align="center" class="ls_<?php echo $ls ?>">&nbsp;
                                        <?php
                                        if ((($alerterformonthend == 0) && ($is_refunded != 1)) || (ISMASTER)) { ?>
                                        <?php echo generateProductSelect($predefval[4], 'Tax Code', 'taxcode', $rowtt['taxcode'], $resutlstring, $complevel, $rowtt['id'], $id_counter); ?>
                                        &nbsp;
                                    </td>
                                <?php
                                } else {
                                    echo $rowtt['taxcode'];
                                } ?>
                                    <td align="center" class="ls_<?php echo $ls ?>">
                                        <?php
                                        if ((($alerterformonthend == 0) && ($is_refunded != 1)) || (ISMASTER)) { ?>
                                        <input onkeyup="updateproduct(this.value,'<?php echo $gval[1] ?>','<?php echo $complevel ?>','<?php echo $rowtt['id'] ?>','discount','<?php echo $id_counter ?>','<?php echo $gval[21] ?>');"
                                               type="text" name="discount"
                                               style="font-family:Tahoma;font-size:11px;width:40px;text-align:center"
                                               value="<?php echo $rowtt['discount'] ?>"></td>
                                <?php
                                } else {
                                    echo $rowtt['discount'];
                                } ?>
                                    <td align="center" class="ls_<?php echo $ls ?>">
                                        <?php
                                        if ((($alerterformonthend == 0) && ($is_refunded != 1)) || (ISMASTER)) { ?>
                                        <input onkeyup="updateproduct(this.value,'<?php echo $gval[1] ?>','<?php echo $complevel ?>','<?php echo $rowtt['id'] ?>','quantity','<?php echo $id_counter ?>','<?php echo $gval[21] ?>');"
                                               type="text" name="quantity"
                                               style="font-family:Tahoma;font-size:11px;width:35px;text-align:center"
                                               value="<?php echo $rowtt['quantity'] ?>"></td>
                                <?php
                                } else {
                                    echo $rowtt['quantity'];
                                } ?>
                                    <?php
                                    if ($complevel == 2) {
                                        // As this field if we are dealing with a purchase order ?>
                                        <td align="center" class="ls_<?php echo $ls ?>"><input
                                                    onkeyup="updateproduct(this.value,'<?php echo $gval[1] ?>','<?php echo $complevel ?>','<?php echo $rowtt['id'] ?>','received','<?php echo $id_counter ?>',,'<?php echo $gval[21] ?>');"
                                                    type="text" name="received"
                                                    style="font-family:Tahoma;font-size:11px;width:35px;text-align:center"
                                                    value="<?php echo $rowtt['received'] ?>"></td>
                                        <?php
                                    } ?>
                                    <td align="center" class="ls_<?php echo $ls ?>">
                                        <?php
                                        if ((($alerterformonthend == 0) && ($is_refunded != 1)) || (ISMASTER)) { ?>
                                        <input onkeyup="updateproduct(this.value,'<?php echo $gval[1] ?>','<?php echo $complevel ?>','<?php echo $rowtt['id'] ?>','salesprice','<?php echo $id_counter ?>','<?php echo $gval[21] ?>');"
                                               type="text" name="salesprice"
                                               style="font-family:Tahoma;font-size:11px;width:70%;text-align:right"
                                               value="<?php echo $rowtt['salesprice'] ?>"></td>
                                <?php
                                } else {
                                    echo $rowtt['salesprice'];
                                } ?>
                                    <td align="center" class="ls_<?php echo $ls ?>">
                                        <?php $prodsubtotal_1 = number_format((($rowtt['salesprice'] * $rowtt['quantity']) - ($rowtt['salesprice'] * $rowtt['quantity']) * (($rowtt['discount'] / 100))), 2) ?>
                                        <span id="txtHintproduct_1_<?php echo $id_counter ?>"><?php echo $prodsubtotal_1 ?></span>
                                    </td>
                                    <?php
                                    $unitprice = ($rowtt['salesprice'] * $rowtt['quantity']) - ($rowtt['salesprice'] * $rowtt['quantity']) * (($rowtt['discount'] / 100));
                                    $totalunitprice = $totalunitprice + $unitprice; ?>
                                    <td align="center" class="ls_<?php echo $ls ?>">
                                        <?php $subtotalinctax = round($unitprice * (($rowtt['taxcode'] / 100) + 1), 2); ?>
                                        <?php $prodsubtotal_2 = number_format($subtotalinctax, 2); ?>
                                        <?php $totalinctax = $totalinctax + $subtotalinctax; ?>
                                        <span id="txtHintproduct_2_<?php echo $id_counter ?>"
                                              style="font-weight:bold"><?php echo $prodsubtotal_2 ?></span>
                                    </td>
                                    <td align="center" class="ls_<?php echo $ls ?>">
                                        <?php
                                        if (((ISMASTER == 1) || (UID == CREATEDBY)) && ((($todays_date_check[0] == $date_of_invoice[0]) && ($todays_date_check[1] == $date_of_invoice[1])) || ($date_invoiced_maintain == ""))) {
                                            if ($is_refunded == 1) { ?>
                                                <font style="color:#CCCCCC;padding:7px">Delete | Refunded
                                                    [<?php echo date("d-M-Y", strtotime($rowtt['isrefundeddate'])) ?>
                                                    ]</font>
                                                <?php
                                            } else { ?>
                                                <a onclick="return confirm('Are you sure you want to delete this item?')"
                                                   href="standard_form.php?id=9:<?php echo $gval[1] ?>:<?php echo $complevel ?>:<?php echo strtoupper(md5($rowtt['id'])) ?>:<?php echo $gval[4] ?>:::::<?php echo $gval[9] ?>:::::::::<?php echo $gval[18] ?>:<?php echo $gval[19] ?>:<?php echo $gval[20] ?>:<?php echo $gval[21] ?>:<?php echo $gval[22] ?>:<?php echo $gval[23] ?>:#salestabs">Delete</a> |
                                                <a onclick="return confirm('Are you sure you want to refund this item?')"
                                                   href="standard_form.php?id=17:<?php echo $gval[1] ?>:<?php echo $complevel ?>:<?php echo strtoupper(md5($rowtt['id'])) ?>:<?php echo $gval[4] ?>:::::<?php echo $gval[9] ?>:::::::::<?php echo $gval[18] ?>:<?php echo $gval[19] ?>:<?php echo $gval[20] ?>:<?php echo $gval[21] ?>:<?php echo $gval[22] ?>:<?php echo $gval[23] ?>:#salestabs">Refund</a>
                                                <?php
                                            }
                                        } else {
                                            if (ISMASTER == 1) {
                                                if ($is_refunded == 1) { ?>
                                                    <a onclick="return confirm('Are you sure you want to delete this item?')"
                                                       href="standard_form.php?id=9:<?php echo $gval[1] ?>:<?php echo $complevel ?>:<?php echo strtoupper(md5($rowtt['id'])) ?>:<?php echo $gval[4] ?>:::::<?php echo $gval[9] ?>:::::::::<?php echo $gval[18] ?>:<?php echo $gval[19] ?>:<?php echo $gval[20] ?>:<?php echo $gval[21] ?>:<?php echo $gval[22] ?>:<?php echo $gval[23] ?>:#salestabs">Delete</a> |
                                                    <a onclick="return confirm('Are you sure you want to refund this item?')"
                                                       href="standard_form.php?id=17:<?php echo $gval[1] ?>:<?php echo $complevel ?>:<?php echo strtoupper(md5($rowtt['id'])) ?>:<?php echo $gval[4] ?>:::::<?php echo $gval[9] ?>:::::::::<?php echo $gval[18] ?>:<?php echo $gval[19] ?>:<?php echo $gval[20] ?>:<?php echo $gval[21] ?>:<?php echo $gval[22] ?>:<?php echo $gval[23] ?>:#salestabs">Refunded
                                                        [<?php echo date("d-M-Y", strtotime($rowtt['isrefundeddate'])) ?>
                                                        ]</a>
                                                    <?php
                                                } else { ?>
                                                    <a onclick="return confirm('Are you sure you want to delete this item?')"
                                                       href="standard_form.php?id=9:<?php echo $gval[1] ?>:<?php echo $complevel ?>:<?php echo strtoupper(md5($rowtt['id'])) ?>:<?php echo $gval[4] ?>:::::<?php echo $gval[9] ?>:::::::::<?php echo $gval[18] ?>:<?php echo $gval[19] ?>:<?php echo $gval[20] ?>:<?php echo $gval[21] ?>:<?php echo $gval[22] ?>:<?php echo $gval[23] ?>:#salestabs">Delete</a> |
                                                    <a onclick="return confirm('Are you sure you want to refund this item?')"
                                                       href="standard_form.php?id=17:<?php echo $gval[1] ?>:<?php echo $complevel ?>:<?php echo strtoupper(md5($rowtt['id'])) ?>:<?php echo $gval[4] ?>:::::<?php echo $gval[9] ?>:::::::::<?php echo $gval[18] ?>:<?php echo $gval[19] ?>:<?php echo $gval[20] ?>:<?php echo $gval[21] ?>:<?php echo $gval[22] ?>:<?php echo $gval[23] ?>:#salestabs">Refund</a>
                                                <?php }
                                            } else {
                                                if ($is_refunded == 1) { ?>
                                                    <font style="color:#CCCCCC;padding:7px">Delete | Refunded
                                                        [<?php echo date("d-M-Y", strtotime($rowtt['isrefundeddate'])) ?>
                                                        ]</font>
                                                <?php } else { ?>
                                                    <a onclick="return confirm('Are you sure you want to delete this item?')"
                                                       href="standard_form.php?id=9:<?php echo $gval[1] ?>:<?php echo $complevel ?>:<?php echo strtoupper(md5($rowtt['id'])) ?>:<?php echo $gval[4] ?>:::::<?php echo $gval[9] ?>:::::::::<?php echo $gval[18] ?>:<?php echo $gval[19] ?>:<?php echo $gval[20] ?>:<?php echo $gval[21] ?>:<?php echo $gval[22] ?>:<?php echo $gval[23] ?>:#salestabs">Delete</a> |
                                                    <a onclick="return confirm('Are you sure you want to refund this item?')"
                                                       href="standard_form.php?id=17:<?php echo $gval[1] ?>:<?php echo $complevel ?>:<?php echo strtoupper(md5($rowtt['id'])) ?>:<?php echo $gval[4] ?>:::::<?php echo $gval[9] ?>:::::::::<?php echo $gval[18] ?>:<?php echo $gval[19] ?>:<?php echo $gval[20] ?>:<?php echo $gval[21] ?>:<?php echo $gval[22] ?>:<?php echo $gval[23] ?>:#salestabs">Refund</a>
                                                <?php }
                                            }
                                        } ?>&nbsp;
                                        <input type="hidden" name="salestabrowid"
                                               value="<?php echo md5($rowtt['id']) ?>"></td>
                                </tr>
                                <?php unset($is_refunded);
                                $id_counter++;
                            } ?>
                            <tr>
                                <td colspan="<?php echo $colspan_2; ?>">&nbsp;</td>
                                <td class="ls_top" align="center"><font
                                            style="font-size:14px;font-weight:bold"><?php echo number_format($totalunitprice, 2); ?></font>
                                </td>
                                <td class="ls_top" align="center"><font
                                            style="font-size:14px;font-weight:bold"><?php echo number_format($totalinctax, 2); ?></font>
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                        <input type="hidden" name="grandtotalexvat" value="<?php echo $totalunitprice; ?>">
                        <input type="hidden" name="grandtotalincvat" value="<?php echo $totalinctax; ?>">
                        <input type="hidden" name="totalsalestabrows" value="<?php echo($salestabscounter - 1); ?>">
                        <input type="hidden" name="componentabtype" value="<?php echo $complevel; ?>">
                        <input type="hidden" name="pform" value="3">
                    </form>
                    <?php
                } else {
                    echo "<font class='note'><a href='javascript: submitform(this);'>This record must be saved first</a></font>";
                }

            } elseif ($predefval[3] == 1) {
                // First we biuld a list of records to pull

                // Added matrix_id = 390 2014-10-07 @ 19:10 Patrick was here.

                if ($gval[21] == "63538FE6EF330C13A05A3ED7E599D5F7") {
                    $extraSQL = "AND matrix_id = 364";
                } elseif ($gval[21] == "2B8A61594B1F4C4DB0902A8A395CED93") {
                    if ($tablearray[$tablecounter] == "2BB232C0B13C774965EF8558F0FBD615") {
                        $extraSQL = "AND matrix_id = 390";
                    } elseif ($tablearray[$tablecounter] == "3A0772443A0739141292A5429B952FE6") {
                        $extraSQL = "AND matrix_id = 408";
                    }
                } elseif ($gval[21] == "5737034557EF5B8C02C0E46513B98F90") {
                    $extraSQL = "AND matrix_id = 389";
                }

                $getrecdata = "SELECT recordid, menutabid FROM relations_values
									WHERE location = '" . $tablearray[$tablecounter] . "'
									" . $extraSQL . "
									AND parent_recordid = '" . $gval[1] . "' AND registrantid =" . RID;
                //echo $getrecdata."<br><br>";
                $getsubtabs = mysql_query($getrecdata);
                $rec_num_rows = mysql_num_rows($getsubtabs);
                $formulaterec = 1;
                unset($selectstr, $createand);
                while ($recrow = mysql_fetch_array($getsubtabs)) {
                    $predefinedlocationtab = $recrow['menutabid'];
                    ///echo $formulaterec;
                    if ($formulaterec == 1) {
                        $selectstr .= "(";
                    } else {
                        $createand = "OR ";
                    }

                    if ($rec_num_rows != $formulaterec) {
                        $selectstr .= $createand . " hashid ='" . $recrow['recordid'] . "' ";
                    } else {
                        $selectstr .= $createand . " hashid ='" . $recrow['recordid'] . "')";
                    }
                    $formulaterec++;
                }
                // Then we build a list of columns which must be pulled
                unset($columnsbuildcols, $headerbuildcols);
                $sqlbuildcols = "SELECT label FROM udf_definitions WHERE location = '" . $tablearray[$tablecounter] . "' AND inlists = 1 AND isactive = 1 AND registrantid =" . RID . " ORDER BY sortorder ASC";
                // echo $sqlbuildcols."<br><br>";
                $resbuildcols = mysql_query($sqlbuildcols);
                $num_rowsbuildcols = mysql_num_rows($resbuildcols);
                $commacounterbuildcols = 0;
                $columnstrbuildcols = "recordid,";
                $headerbuildcols .= "ID,";
                while ($rowbuildcols = mysql_fetch_array($resbuildcols)) {
                    $specialsbuildcols = array(" ", ":", ";", "&#65533;", "`", "&#65533;", "!", "&#65533;", "$", "%", "^", "&", "*", "(", ")", "_", "_", "+", "=", "{", "}", "[", "]", "@", "'", "~", "#", "|", "\\", "<", ",", ">", "?", "/");
                    $columnsbuildcols = str_replace(' ', ' ', str_replace($specialsbuildcols, '', $rowbuildcols['label']));
                    if (($commacounterbuildcols + 1) != $num_rowsbuildcols) {
                        $columnstrbuildcols .= strtolower($columnsbuildcols) . ",";
                        $headerbuildcols .= $rowbuildcols['label'] . ",";
                    } else {
                        $columnstrbuildcols .= strtolower($columnsbuildcols);
                        $headerbuildcols .= $rowbuildcols['label'];
                    }
                    $commacounterbuildcols++;

                }
                //echo $columnstrbuildcols;
                // Get the menutabid
                $gettabhashid = "SELECT menu_tabhashid, componentabtype FROM menusub_tabs WHERE hashid = '" . $tablearray[$tablecounter] . "' LIMIT 1";
                $gettabhashidres = mysql_query($gettabhashid);
                $gettabhashidrow = mysql_fetch_array($gettabhashidres);
                $rightmenutabid = $gettabhashidrow['menu_tabhashid'];
                $newcolspan = count(explode(",", $headerbuildcols));
                /// Lets create the sales modules and
                /// the accounting module
                if ($gettabhashidrow['componentabtype'] == 1) {
                    $getsales = "SELECT rv.menutabid, rv.location,udf.hashid as udfhashid, udf.recordid, udf.tlsrecidinvoiceddate,
										DATE_ADD(udf.tlsrecidinvoiceddate,INTERVAL udf.tlsinvoiceterms DAY) as duedate,
										DATEDIFF(DATE_ADD(udf.tlsrecidinvoiceddate,INTERVAL udf.tlsinvoiceterms DAY),
										CURRENT_DATE()) AS tildue
										FROM udf_9BF31C7FF062936A96D3C8BD1F8F2FF3 udf
										INNER JOIN relations_values rv ON udf.hashid = rv.recordid
										AND rv.location =  '" . $tablearray[$tablecounter] . "'
										AND rv.parent_recordid =  '" . $gval[1] . "'
										AND udf.issaved =1
										ORDER BY udf.recordid ASC";
                    //echo $getsales;
                    $resgetsales = mysql_query($getsales);
                    $num_rows = mysql_num_rows($resgetsales); ?>
                    <table class="note" border="0" width="100%">
                        <tr>
                            <td colspan="5">
                                <?php
                                if ($gval[1] == "") { ?>
                                    <font style="font-family:Tahoma;font-size:11px;color:#CCCCCC">This record must be
                                        saved first</font><br><br style="line-height:5px">
                                    <?php
                                } else { ?>
                                    <a href="thinline.php?id=::::::::::::::::::::1679091C5A880FAF6FB5E6087EB1B2DC:9BF31C7FF062936A96D3C8BD1F8F2FF3:2:9BF31C7FF062936A96D3C8BD1F8F2FF3:::::::<?php echo $gval['1'] ?>:<?php echo $gval['21'] ?>"><font
                                                class="note">New Lead</a> | </font>
                                    <?php
                                    if ($num_rows < 1) { ?>
                                        <font class="noteg">Receive Money</font><br><br style="line-height:5px">
                                        <?php
                                    } else { ?>
                                        <!--<a href="/accounts_receive.php?id=1:<?php echo $gval[21] ?>:<?php echo $gval[1] ?>" <?php echo LFD ?>><font class="note">Receive Money</font></a><br><br style="line-height:5px">-->
                                        <font class="noteg">Receive Money</font><br><br style="line-height:5px">
                                        <?php
                                    }
                                } ?>
                            </td>
                        </tr>
                        <tr align="center">
                            <td width="10%" class="ls_on"><strong>ID</strong></td>
                            <td class="ls_on"><strong>Invoice Date</strong></td>
                            <td class="ls_on"><strong>Due Date</strong></td>
                            <td class="ls_on"><strong>Balance</strong></td>
                            <td class="ls_on"><strong>Amount</strong></td>
                        </tr>
                        <?php
                        $classcounter = 2;
                        while ($rowgetsales = mysql_fetch_array($resgetsales)) {
                            include("includes/ls.php");
                            $getmon = "SELECT SUM(ROUND( (salesprice * quantity) * ((taxcode/100)+1),2)) as InvoiceAmount,
										   SUM(ROUND( (salesprice * quantity) * ((taxcode/100)+1),2))-IFNULL((
											SELECT SUM(amountdebtors) FROM payments pay WHERE pay.parent_recordid = '" . $rowgetsales['udfhashid'] . "'
											AND pay.location = '" . $rowgetsales['location'] . "' ),0) AS Balance
										   FROM sales_1 s1
										   WHERE s1.recordid = '" . $rowgetsales['udfhashid'] . "'
										   AND s1.location = '" . $rowgetsales['location'] . "'";
                            $getmonsales = mysql_query($getmon);
                            $rowmonsales = mysql_fetch_array($getmonsales);
                            //echo $getmon."<br><br>"; ?>
                            <tr>
                                <td align="center" class="ls_<?php echo $ls ?>"><a
                                            href="thinline.php?id=2:<?php echo $rowgetsales['udfhashid'] ?>:::::::::::::::::::<?php echo $rowgetsales['menutabid'] ?>:<?php echo $rowgetsales['location'] ?>:2"><?php echo $rowgetsales['recordid'] ?></a>
                                </td>
                                <td align="center" class="ls_<?php echo $ls ?>">
                                    <?php
                                    $startingmonth = explode("-", substr($rowgetsales['tlsrecidinvoiceddate'], 0, 11));
                                    $startdatefinal = $startingmonth[2] . "-" . convertDate($startingmonth[1]) . "-" . $startingmonth[0];
                                    echo $startdatefinal;
                                    unset($startdatefinal);
                                    ?>
                                </td>
                                <td align="center" class="ls_<?php echo $ls ?>">
                                    <?php
                                    $startingmonth = explode("-", substr($rowgetsales['duedate'], 0, 11));
                                    $startdatefinal = $startingmonth[2] . "-" . convertDate($startingmonth[1]) . "-" . $startingmonth[0];
                                    echo $startdatefinal;
                                    unset($startdatefinal);
                                    ?>
                                </td>
                                <td class="ls_<?php echo $ls ?>"
                                    align="right"><?php echo number_format($rowmonsales['Balance'], 2) ?></td>
                                <td class="ls_<?php echo $ls ?>"
                                    align="right"><?php echo number_format($rowmonsales['InvoiceAmount'], 2) ?></td>
                            </tr>
                            <?php
                        } ?>
                    </table>
                    <?php
                    $tablecounter++;
                } elseif ($gettabhashidrow['componentabtype'] == 2) {
                    // first thing we need to do is go into the sales table
                    // and get all th records attached to this customer
                    $getsales = "SELECT rv.menutabid, rv.location, udf.hashid as udfhashid, rmx.parenttable, rv.matrix_id, rv.parent_recordid ,udf.recordid, udf.tlsrecidinvoiceddate, udf.tlsinvoiceterms,
												DATE_ADD(udf.tlsrecidinvoiceddate,INTERVAL udf.tlsinvoiceterms DAY) as duedate,
												DATEDIFF(DATE_ADD(udf.tlsrecidinvoiceddate,INTERVAL udf.tlsinvoiceterms DAY),
												CURRENT_DATE()) AS tildue, SUM(ROUND( (p1.salesprice * quantity) * ((taxcode/100)+1),2)) as InvoiceAmount,
													SUM(ROUND( (p1.salesprice * quantity) * ((taxcode/100)+1),2))-IFNULL((
														SELECT SUM(amountcreditors) FROM payments pay WHERE pay.parenttable = rmx.parenttable
														AND pay.parent_recordid = rv.parent_recordid AND pay.recordid = udf.recordid ),0) AS Balance
														FROM udf_" . $tablearray[$tablecounter] . " udf
												INNER JOIN purchases_1 p1 ON udf.hashid = p1.recordid
												INNER JOIN relations_values rv ON udf.hashid = rv.recordid
												INNER JOIN relations_matrix rmx ON rmx.id = rv.matrix_id
												AND rv.location = '" . $tablearray[$tablecounter] . "'
												AND udf.issaved = 1
												AND rv.parent_recordid = '" . $gval[1] . "'
												WHERE udf.tlsrecidinvoiced = 1
												GROUP BY udf.recordid
												ORDER BY udf.recordid ASC ";
                    // echo $getsales;
                    $resgetsales = mysql_query($getsales);
                    $num_rows = mysql_num_rows($resgetsales); ?>
                    <table class="note" border="0" width="100%">
                        <tr>
                            <td colspan="5">
                                <?php
                                if ($gval[1] == "") { ?>
                                    <font style="font-family:Tahoma;font-size:11px;color:#CCCCCC">This record must be
                                        saved first </font><br><br style="line-height:5px">
                                    <?php
                                } else {
                                    if ($num_rows < 1) { ?>
                                        <font class="noteg">Pay Money</font><br><br style="line-height:5px">
                                        <?php
                                    } else { ?>
                                        <a href="accounts_pay.php?id=1:<?php echo $gval[21] ?>:<?php echo $gval[1] ?>" <?php echo LFD ?>><font
                                                    class="note">Pay Money</font></a><br><br style="line-height:5px">
                                        <?php
                                    }
                                } ?>
                            </td>
                        </tr>
                        <tr align="center">
                            <td width="10%" class="ls_top"><strong>Invoice ID</strong></td>
                            <td class="ls_top"><strong>Invoice Date</strong></td>
                            <td class="ls_top"><strong>Due Date</strong></td>
                            <td class="ls_top"><strong>Balance</strong></td>
                            <td class="ls_top"><strong>Amount</strong></td>
                        </tr>
                        <?php
                        $classcounter = 2;
                        while ($rowgetsales = mysql_fetch_array($resgetsales)) {
                            include("includes/ls.php"); ?>
                            <tr>
                                <td align="center" class="ls_<?php echo $ls ?>"><a
                                            href="thinline.php?id=2:<?php echo $rowgetsales['udfhashid'] ?>:::::::::::::::::::<?php echo $rowgetsales['menutabid'] ?>:<?php echo $rowgetsales['location'] ?>:2"><?php echo $rowgetsales['recordid'] ?></a>
                                </td>
                                <td align="center" class="ls_<?php echo $ls ?>">
                                    <?php
                                    $startingmonth = explode("-", substr($rowgetsales['tlsrecidinvoiceddate'], 0, 11));
                                    $startdatefinal = $startingmonth[2] . "-" . convertDate($startingmonth[1]) . "-" . $startingmonth[0];
                                    echo $startdatefinal;
                                    unset($startdatefinal);
                                    ?>
                                </td>
                                <td align="center" class="ls_<?php echo $ls ?>">
                                    <?php
                                    $startingmonth = explode("-", substr($rowgetsales['duedate'], 0, 11));
                                    $startdatefinal = $startingmonth[2] . "-" . convertDate($startingmonth[1]) . "-" . $startingmonth[0];
                                    echo $startdatefinal;
                                    unset($startdatefinal);
                                    ?>
                                </td>
                                <td class="ls_<?php echo $ls ?>"
                                    align="right"><?php echo number_format($rowgetsales['Balance'], 2) ?></td>
                                <td class="ls_<?php echo $ls ?>"
                                    align="right"><?php echo number_format($rowgetsales['InvoiceAmount'], 2) ?></td>
                            </tr>
                            <?php
                        } ?>
                    </table>
                    <?php
                    $tablecounter++;
                } else { ?>
                    <table class="note" border="0" width="100%" cellpadding="2" cellspacing="2">
                        <tr>
                            <td colspan="<?php echo($newcolspan + 1) ?>">
                                <font class="note" color="#cccccc">
                                    <?php
                                    if ($gval[1] != "") { ?>
                                    <a href="thinline.php?id=::::::::::::::::::::<?php echo $rightmenutabid; ?>:<?php echo $tablearray[$tablecounter]; ?>:2::::::::<?php echo $gval['1'] ?>:<?php echo $gval['21'] ?>">New</a>
                                    <?php
                                    // If this is the bookings tab then we show this menu item
                                    if ($gval[21] == "63538FE6EF330C13A05A3ED7E599D5F7") { ?>
                                    <a <?php echo LFD ?> href="paid-sessions.php?id=<?php echo $gval[1]; ?>"> | Set
                                        Paid</a></font>
                                <?php
                                }

                                // If this is the bookings tab then we show this menu item
                                if ($gval[21] == "2B8A61594B1F4C4DB0902A8A395CED93") {
                                    $return_url = base64_encode(basename($_SERVER['REQUEST_URI'])); ?>
                                    |
                                    <a href="make-public.php?a=public&id=<?php echo $gval[1]; ?>&r=<?php echo $return_url; ?>">Set
                                        Reports Public</a>
                                    |
                                    <a href="make-public.php?a=private&id=<?php echo $gval[1]; ?>&r=<?php echo $return_url; ?>">Set
                                        Reports Private</a>
                                    |
                                    <a onclick="return confirm('Are you sure you want to email all your guardians their report links for this class?  Once this process starts it cannot be stopped, so please ensure you have double checked all your reports and they have been completed correctly.')"
                                       href="/reports/email-reports.php?classid=<?php echo $gval[1]; ?>&r=<?php echo $return_url; ?>">Email
                                        Report Links to Parents</a></font>
                                    <?php
                                }

                                } else {
                                    echo "<font class='note'><a href='javascript: submitform(this);'>This record must be saved first</a></font>";
                                } ?>
                                </font>
                            </td>
                        </tr>
                        <?php
                        // Create the table column name output
                        $tablecolumnsbuildcols = explode(",", $headerbuildcols);
                        $trcounterbuildcols = 1;
                        $getprimarybuildcols = 1;
                        foreach ($tablecolumnsbuildcols as &$valuebuildcols) {
                            if ($trcounterbuildcols == 1) {
                                echo "<tr>\n";
                            }
                            if ($getprimarybuildcols == 1) {
                                $primarysortlabelbuildcols = columnnames($valuebuildcols);
                            }
                            echo "<td class=\"ls_on\" align='center'>
											<table border=0>
											  <tr>
												<td style=\"font-size:11px\" align=\"\"><strong>";
                            echo $valuebuildcols;
                            if ($valuebuildcols == "Booking Type") {
                                $needToCalWaitingList = 1; // So we know this is a booking type column and will need to possibly calculate a waiting list position
                            }
                            echo "</strong>&nbsp;";
                            echo "
												</td>";

                            if ($gval[18] == "ASC") {
                                echo '<td class=\"ls_on\"><a href="thinline.php?id=2:' . $gval[1] . '::::::' . $gval[7] . ':' . $gval[8] . ':' . $gval[9] . ':' . $gval[10] . ':' . $gval[11] . ':' . $gval[12] . ':' . $gval[13] . ':' . $gval[14] . ':' . $gval[15] . ':' . $gval[16] . ':' . $gval[17] . ':DESC:' . columnnames($valuebuildcols) . ':' . $gval[20] . ':' . $gval[21] . ':' . $gval[22] . ':' . $gval[23] . ':' . $gval[24] . '">';
                                if ($gval[19] == columnnames($valuebuildcols)) {
                                    echo "<img src=\"images/icons/sort_asc.gif\" border=\"0\"></a></td>";
                                } else {
                                    echo "<img src=\"images/icons/sort_asc_g.gif\" border=\"0\"></a></td>";
                                }
                            } else {
                                echo '<td class=\"ls_on\" ><a href="thinline.php?id=2:' . $gval[1] . '::::::' . $gval[7] . ':' . $gval[8] . ':' . $gval[9] . ':' . $gval[10] . ':' . $gval[11] . ':' . $gval[12] . ':' . $gval[13] . ':' . $gval[14] . ':' . $gval[15] . ':' . $gval[16] . ':' . $gval[17] . ':ASC:' . columnnames($valuebuildcols) . ':' . $gval[20] . ':' . $gval[21] . ':' . $gval[22] . ':' . $gval[23] . ':' . $gval[24] . '">';
                                if ($gval[19] == columnnames($valuebuildcols)) {
                                    echo "<img src=\"images/icons/sort_desc.gif\" border=\"0\"></a></td>";
                                } else {
                                    echo "<img src=\"images/icons/sort_asc_g.gif\" border=\"0\"></a></td>";
                                }
                            }

                            echo "</tr>
										    </table>
										</td>";
                            if ($trcounterbuildcols == ($num_rowsbuildcols + 1)) {
                                if ($rightmenutabid == "8F53295A73878494E9BC8DD6C3C7104F") { // this is for the printing of reports for kids.
                                    echo "<td class=\"ls_on\" align='center'><strong>Actions</strong></td>";
                                }
                                echo "</tr>\n";
                                $trcounterbuildcols = 0;
                            }
                            $trcounterbuildcols++;
                            $getprimarybuildcols++;
                        }
                        // Create the table content
                        $sqlbldcols = "SELECT id, hashid, " . $columnstrbuildcols . " FROM udf_" . $tablearray[$tablecounter] . " WHERE issaved = 1 ";
                        $sqlbldcols .= " AND NOT (createdby !=" . UID . " AND tlsprivate =1) ";
                        //echo $sqlbldcols."<br>";
                        // Sort by ASC Descending
                        $resbldcols = mysql_query($sqlbldcols);
                        /* get column metadata */
                        $i = 0;
                        if ($gval[19] == "") {
                            $orderbybldcols = $primarysortlabelbuildcols;
                        } else {
                            $orderbybldcols = $gval[19];
                        }
                        $foundcol = 0;

                        while ($i < mysql_num_fields($resbldcols)) {
                            $meta = mysql_fetch_field($resbldcols, $i);
                            if ($meta->name == $gval[19]) {
                                $foundcol++;
                                $orderbybldcols = $gval[19];
                                $whichwaybldcols = $gval[18];
                            }
                            $i++;
                        }

                        $sqlbldcols = "SELECT id, hashid, " . $columnstrbuildcols . ", createddate FROM udf_" . $tablearray[$tablecounter] . " WHERE issaved = 1 AND " . $selectstr . " AND registrantid =" . RID;
                        $sqlbldcols .= " AND NOT (createdby !=" . UID . " AND tlsprivate =1) " . $addsqldepartments . " ";
                        //echo $sqlbldcols;
                        $_SESSION['setPaid'] = $sqlbldcols;
                        if (($gval[19] == "") || ($foundcol == 0)) {
                            $orderbybldcols = $primarysortlabelbuildcols;
                            $whichwaybldcols = "ASC";
                        } else {
                            $orderbybldcols = $gval[19];
                            $whichwaybldcols = $gval[18];
                        }

                        if (($gval[7] == NULL) || ($gval[7] == "ALL")) {
                            //if ($gval[9] == "") {
                            // for some reason if you came n from any page but 1 it would not show the result because gval[9] is not zero.
                            // have temp disabled.  should fix the problem.
                            $sqlbldcols .= " ORDER BY " . $orderbybldcols . " " . $whichwaybldcols;
                            //} else {
                            //	$sqlbldcols .= " ORDER BY ".$orderbbldcols." ".$whichwaybldcols;
                            //}
                        }
                        //echo $sqlbldcols;

                        $resbldcols = mysql_query($sqlbldcols);
                        $columnsbldcols = explode(",", $columnstrbuildcols);
                        $trcounterbldcols = 1;
                        // echo $columnstrbuildcols."<br>";
                        //echo $sqlbldcols."<br>";
                        while ($rowbldcols = mysql_fetch_array($resbldcols)) {
                        include("includes/ls.php");
                        foreach ($columnsbldcols

                        as &$valuebldcols) {
                        // If it is a textbox lets center it in the cell.
                        if ($rowbldcols[$valuebldcols] == "checkboxon") {
                            $metdaligning = "align='center'";
                        } else {
                            $metdaligning = "";
                        }
                        // If it is the first column lets add the edit hyperlink
                        if ($trcounterbldcols == 1) {
                        echo "<tr>\n";
                        echo "<td " . $metdaligning . " class=\"ls_" . $ls . "\">"; ?>
                        <a onclick="javascript:ShowDivSaveChanges(this); return false;"
                           id="<?php echo md5($trcounterbldcols); ?>"
                           href="thinline.php?id=2:<?php echo "" . $rowbldcols['hashid'] . "::" . $gval[3] . "::" . $gval[5] . ":" . $gval[6] . ":" . $gval[7] . ":" . $gval8 . ":" . $gval9 . ":" . $gval[10] . ":" . $gval[11] . ":" . $gval[12] . ":" . $gval[13] . ":" . $gval[14] . ":" . $gval[15] . ":" . $gval[16] . ":" . $gval[17] . ":::" . $rightmenutabid . ":" . $tablearray[$tablecounter] . ":2:" . $gval[23] . ":" . $gval[24]; ?>">
                            <?php

                            if ($rowbldcols[$valuebldcols] != "") {
                                echo $rowbldcols[$valuebldcols] . "</a>&nbsp;</td>\n";
                            } else {
                                echo "Undefined</a>&nbsp;</td>\n";
                            }

                            } else {
                                echo "<td " . $metdaligning . " class=\"ls_" . $ls . "\">";
                                if (preg_match("(\d{4}[/-]\d{2}[/-]\d{2}[/ ]\d{2}[/:]\d{2}[/:]\d{2})", $rowbldcols[$valuebldcols])) {
                                    // Date Time String
                                    if ($rowbldcols[$valuebldcols] != "0000-00-00 00:00:00") {
                                        echo date("d-M-Y H:i", strtotime($rowbldcols[$valuebldcols]));
                                    }
                                } elseif (preg_match("(\d{4}[/-]\d{2}[/-]\d{2})", $rowbldcols[$valuebldcols])) {
                                    // Date String
                                    if ($rowbldcols[$valuebldcols] != "0000-00-00") {
                                        echo date("d-M-Y", strtotime($rowbldcols[$valuebldcols]));
                                    }
                                } elseif ($rowbldcols[$valuebldcols] == "checkboxon") { ?>
                                    <input type="checkbox" checked disabled>
                                    <?php
                                } elseif (check_email_address($rowbldcols[$valuebldcols]) == 1) {
                                    // Check if it is an email address.  If so then lets
                                    // hyperlink it
                                    echo "<a " . LFD . " href=\"/messaging.php?id=2:" . $rowbldcols['hashid'] . ":" . $tablearray[$tablecounter] . ":2:emailaddress:" . $predefinedlocationtab . "\">" . $rowbldcols[$valuebldcols] . "<a>";
                                } else {
                                    // Else it is a standard string
                                    echo $rowbldcols[$valuebldcols];
                                    if (($needToCalWaitingList == 1) && (($rowbldcols[$valuebldcols] == "Waiting List") || ($rowbldcols[$valuebldcols] == "Booking"))) {
                                        // Calculate a waiting list position
                                        echo " (" . date("d M Y H:i", strtotime($rowbldcols['createddate'])) . ")";
                                    }
                                }
                                echo "&nbsp;</td>\n";
                            }

                            if ($trcounterbldcols == ($num_rowsbuildcols + 1)) {
                                if (($rightmenutabid == "8F53295A73878494E9BC8DD6C3C7104F") && ($gval[21] == '2B8A61594B1F4C4DB0902A8A395CED93')) { // this is for the printing of reports for kids.
                                    $return_url = base64_encode(basename($_SERVER['REQUEST_URI']));
                                    $num_reports = 0;
                                    $sql_reports = "SELECT id, group_name, group_stage, public, emailed FROM reports WHERE booking_id = '" . $rowbldcols['hashid'] . "' LIMIT 1";
                                    //echo $sql_reports;
                                    $rs_reports = mysql_query($sql_reports);
                                    $row_reports = mysql_fetch_array($rs_reports);
                                    $add_report_text = ($row_reports['id'] != "") ? " style='text-decoration:line-through;color:green' onclick=\"return confirm('Are you sure you want to run this report again?')\" " : " style='color:red' ";
                                    echo "<td class=\"ls_" . $ls . "\" align='center'>";
                                    echo "<a " . $add_report_text . " href='/thinline.php?id=51:15::" . $rowbldcols['hashid'] . ":" . $gval[1] . "::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A&r=" . $return_url . "'>Run Report</a>";
                                    if ($row_reports != "") {
                                        echo " | <a href='/thinline.php?id=51:15:" . $row_reports['id'] . ":" . $rowbldcols['hashid'] . ":" . $gval[1] . "::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A&group=" . $row_reports['group_name'] . "&stage=" . $row_reports['group_stage'] . "&r=" . $return_url . "'>Edit</a>";
                                        echo " | <a href='/reports/" . strtolower(str_replace(" ", "-", $row_reports['group_name'])) . "/" . strtolower($row_reports['group_stage']) . "/?booking_id=" . $rowbldcols['hashid'] . "&class_id=" . $gval[1] . "&group=" . $row_reports['group_name'] . "&stage=" . $row_reports['group_stage'] . "' target='_reports'>View</a><br>";
                                        if ($row_reports['public'] == "No") {
                                            echo "<a href='/thinline.php?id=62:1:" . $row_reports['id'] . ":" . $rowbldcols['hashid'] . ":" . $gval[1] . "::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A&r=" . $return_url . "'>Make Public</a>";
                                        } else {
                                            echo "<a href='/thinline.php?id=62:2:" . $row_reports['id'] . ":" . $rowbldcols['hashid'] . ":" . $gval[1] . "::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A&r=" . $return_url . "'>Make Private</a>";
                                        }
                                        $dollar_r = base64_encode("/reports/" . strtolower(str_replace(" ", "-", $row_reports['group_name'])) . "/" . strtolower($row_reports['group_stage']) . "/?booking_id=" . $rowbldcols['hashid'] . "&class_id=" . $gval[1] . "&group=" . $row_reports['group_name'] . "&stage=" . $row_reports['group_stage']);
                                        echo " | <a href='/wkhtmltopdf/index.php?r=" . $dollar_r . "'>Save as PDF</a>";
                                        if (strtotime($row_reports['emailed']) > 0) {
                                            echo "<br /><a href='/reports/email-remove-date.php?id=" . $rowbldcols['hashid'] . "&r=" . $return_url . "' onclick=\"return confirm('Are you sure? This will remove the date stamp for the previous email and allow you to send another email to the guardian by click on the Email Report Links to Parents link')\">Email Sent (" . date("d M Y", strtotime($row_reports['emailed'])) . ")<a/>";
                                        }
                                    }
                                    echo "</td>";
                                } else {
                                    echo "<td class=\"ls_" . $ls . "\" align='center'>-</td>";
                                }
                                echo "</tr>\n";
                                $trcounterbldcols = 0;
                            }

                            $trcounterbldcols++;
                            }
                            } ?>
                    </table>
                    <?php
                    $tablecounter++;
                    unset($selectstr, $getrecdata, $createand, $columnstrbuildcols, $columnstrbuildcols, $headerbuildcols);
                }
            }
            ?>
        </div>
    </div>
    <?php
    $generalcount++;
} ?>
