<font style="font-family:Arial;font-size:14px;">
<?php
if ($_GET['id'] != "") {
    
    include_once("_globalconnect.php");
    
    $franchiseIDemail = $_SESSION['franchisedata']['frid'];
    // Get email server details
    $sqlemail = "SELECT * FROM `franchise_master`.`email_server_details` WHERE `franchiseid` = ".$franchiseIDemail." LIMIT 1;";
    //echo $sqlemail;
    $resemail = mysql_query($sqlemail);
    $rowemail = mysql_fetch_array($resemail);
    
    $sql = "SELECT * FROM `udf_2BB232C0B13C774965EF8558F0FBD615` WHERE hashid = '".mysql_real_escape_string($_GET['id'])."'";
    //echo $sql."<br>";
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
    
    if ($row['classtype'] == "Class") {
        $classtype = "Class";
    } elseif ($row['classtype'] == "Camp")  {
        $classtype = "Camp";
    }
    
    if ($row['bookingtype'] == "Booking") {
        $bookingtype = "BK";
    } elseif ($row['bookingtype'] == "Waiting List")  {
        $bookingtype = "WL";
    }
    
    if ($bookingtype == "BK") {
		$message = $rowemail['confirmation_bk'];
    } elseif ($bookingtype == "WL") {
		$message = '';
		$message .= $rowemail['confirmation_wl'].$classtype.': <br /><br />';
    }
    
    $message .= '<strong><u>'.$classtype.' Information</u></strong><br />';
    $message .= '<strong>'.$classtype.':</strong> '.$row['classname'].'<br />';
    
	
    // Need to get the Venue Address
	$sqli = "SELECT parent_recordid FROM `relations_values` 
				WHERE `location` LIKE '2BB232C0B13C774965EF8558F0FBD615' 
				AND `recordid` LIKE '".mysql_real_escape_string($_GET['id'])."'
				AND matrix_id = 390";
	$resulti = mysql_query($sqli);
    $rowi = mysql_fetch_array($resulti);
	$classidi = $rowi['parent_recordid'];
	
	$sqli = "SELECT coach, notes FROM `udf_2B8A61594B1F4C4DB0902A8A395CED93` WHERE hashid = '".$classidi."' AND issaved = 1";
	//echo $sqli;
	$resulti = mysql_query($sqli);
    $rowi = mysql_fetch_array($resulti);
	$classnotes = $rowi['notes'];
    $coach = $rowi['coach'];
	
	$sqli = "SELECT parent_recordid FROM `relations_values` 
				WHERE `location` LIKE '2B8A61594B1F4C4DB0902A8A395CED93' 
				AND `recordid` LIKE '".mysql_real_escape_string($classidi)."'
				AND matrix_id = 389";
	$resulti = mysql_query($sqli);
    $rowi = mysql_fetch_array($resulti);
	$venueid = $rowi['parent_recordid'];
	
    $sqli = "SELECT address, postcode
				FROM `udf_5737034557EF5B8C02C0E46513B98F90`
                WHERE hashid = '".mysql_real_escape_string($venueid)."'
				AND issaved = 1";
    //echo $sqli."<br>";
	$resulti = mysql_query($sqli);
    $rowi = mysql_fetch_array($resulti);
   
    
    // Lets get session information
    $sqls = "SELECT booking.department AS department, camps.hashid AS hashid FROM `udf_2BB232C0B13C774965EF8558F0FBD615` booking
                LEFT JOIN udf_2B8A61594B1F4C4DB0902A8A395CED93 camps ON camps.classname = booking.classname
                WHERE booking.hashid = '".mysql_real_escape_string($_GET['id'])."'";
    //echo $sqls."<br>";;
    $results = mysql_query($sqls);
    $rows = mysql_fetch_array($results);
    
    // Lets get the session data
    $sqlcs = "SELECT session_date FROM `class_sessions`
                WHERE `parent_hashid` LIKE '".$rows['hashid']."' ORDER BY `session_date` ASC";
    //echo $sqlcs."<br>";
    $resultcs = mysql_query($sqlcs);
    $scount = 0;
    while ($rowcs = mysql_fetch_array($resultcs)) {  
		if ($scount == 0) {
			$firstSession = $rowcs['session_date'];
		}
		$scount++;
        $lastSession = $rowcs['session_date'];
    }
    
    $sqlp = "SELECT paymentinstructions FROM udf_33E8075E9970DE0CFEA955AFD4644BB2 WHERE department = ".mysql_real_escape_string($rows['department'])." AND issaved = 1 LIMIT 1";
    $resp = mysql_query($sqlp);
    $rowp = mysql_fetch_array($resp);
    $bankDetails  = $rowp['paymentinstructions'];
    
    if ($bookingtype == "BK") {
    
	$message .= '<strong>'.$classtype.' Address:</strong> '.$rowi['address'].", ".$rowi['postcode'].'<br />';
	
	// remove apostrophe if only 1 session
	$firstSessionDate = date("d M Y",strtotime($firstSession));
	$lastSessionDate = date("d M Y",strtotime($lastSession));
	if ($firstSessionDate != $lastSessionDate) {
	    $addSpos = "'s";
	}
	
	$message .= '<strong>Session Info:</strong> '.date('l', strtotime($firstSession)).''.$addSpos.' - '.date("d M Y",strtotime($firstSession)).' to '.date("d M Y",strtotime($lastSession)).'<br>';
	$message .= '<strong>Coach:</strong> '.$coach.'<br />';
	$message .= '<strong>Booked In:</strong><br><br>';
	$message .= 'Name: '.$row['childsname'].'<br />';
	
	// Select kids data for medical notes and dob
	
	$sqln = "SELECT mst.menu_tabhashid AS menutabhashid, udf.label AS columnname, rx.parenttable AS parenttable, rv.parent_recordid AS recordid
		    FROM udf_definitions udf, relations_matrix rx, relations_values rv, menusub_tabs mst
		    WHERE rx.id = 364 AND rx.id = rv.matrix_id AND rx.udf_columnid = udf.hashid
		    AND rv.recordid = '".mysql_real_escape_string($_GET['id'])."' AND rx.parenttable = mst.hashid AND rx.registrantid = 1 ORDER BY rv.id DESC LIMIT 1";
	//echo $sqln."<br>";
	$resn = mysql_query($sqln);
	$rownn = mysql_fetch_array($resn);
	
	    $sqln = "SELECT birthday, medicalallergies FROM `udf_63538FE6EF330C13A05A3ED7E599D5F7` WHERE hashid = '".mysql_real_escape_string($rownn['recordid'])."' LIMIT 1";
	    $resn = mysql_query($sqln);
	    $rown = mysql_fetch_array($resn);
	
	$message .= 'DOB: '.date("d M Y", strtotime($rown['birthday'])).'<br />';
	$message .= 'Medical Notes (If any): '.$rown['medicalallergies'].'<br /><br />';
	
	if ($row['paid'] != "Yes") {
    
	    if ($row['paymentmethod'] == "Bank Transfer") {
		
		$row['paymentmethod'] = "<br /><strong>Bank Details:</strong><br>";
		$row['paymentmethod'] .= $bankDetails."<br>";
		$message .= $row['paymentmethod'];
		
	    } else {
		    $message .= '<strong>Payment Method:</strong> '.$row['paymentmethod'].'<br />';
	    }
	    $totalfee = ($row['amountdue']+$row['registrationfeeamount']);
	    $totalfee = ($row['chargedtax'] == "Yes") ? ($totalfee*1.20) : $totalfee;
	    $message .= '<br><strong>Amount:</strong> &pound; '.number_format($totalfee,2).'<br /><br />';
	    
	} else {
	    $message .= '<br><strong>Amount Due:</strong> Payment has been made.  Thank you.<br /><br />';
	}
	
	$message .= '<strong>Notes:<br /></strong>'.$classnotes.'<br />';
        
    } elseif ($bookingtype == "WL") {
	$message .= '<strong>'.$classtype.' Address:</strong> '.$rowi['address'].", ".$rowi['postcode'].'<br />';
	$message .= '<strong>Session Info:</strong> '.date('l', strtotime($firstSession)).'\'s - '.date("d M Y",strtotime($firstSession)).' to '.date("d M Y",strtotime($lastSession)).'<br>';
	$message .= '<strong>On Waiting List:</strong> '.$row['childsname'].'<br />';
    }
    
	$message .= $rowemail['confirmation_footer'];
	
    // Lets send a message notification
    require dirname(__FILE__).'/includes/phpmailer/PHPMailerAutoload.php';

    $mail             = new PHPMailer();

    $mail->IsSMTP();            // telling the class to use SMTP
    $mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
					       // 1 = errors and messages
					       // 2 = messages only
    $mail->SMTPAuth   = true;                  // enable SMTP authentication
    $mail->SMTPSecure = "tls"; 
    //$mail->Host       = "smtp.gmail.com"; // sets the SMTP server
    $mail->Host       = $email_host; 			// sets the SMTP server
	$mail->Port       = $email_port;            // set the SMTP port for the GMAIL server
	$mail->IsHTML(true);
	$mail->Username   = $email_username; 		// SMTP account username
	$mail->Password   = $email_password;        // SMTP account password
    
    $mail->addAddress($row['guardiansemail']);
    //$mail->addAddress("mediatomcat@gmail.com");
    
    $mail->addBCC($_SESSION['foemailaddress']);
    if ($bookingtype == "BK") {
	$mail->Subject = $rowemail['label'].' booking confirmation';
    } elseif ($bookingtype == "WL") {
	$mail->Subject = $rowemail['label'].' waiting list confirmation';
    }
    $mail->setFrom($rowemail['fromaddress'],$rowemail['fromaddress']);
    $mail->addReplyTo($rowemail['replyaddress']);
    $mail->Body = $message;
	
	//print_r($mail);
    
    if(!$mail->Send()) {
	echo "Mailer Error: " . $mail->ErrorInfo;
    } else {
	echo "Message has been sent";
        
        $sql = "INSERT INTO `bookings_email` (`hashid`, `emailtype`, `createddate`) VALUES ('".mysql_real_escape_string($_GET['id'])."', 'bookingconfirmation', '".date("Y-m-d")."');";
	//echo $sql;
        mysql_query($sql);
        
    }

} ?>
</font>