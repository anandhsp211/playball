<table border="1" cellpadding="2" style="font-size:14px;font-family:Arial">
<?php
include_once("..//_globalconnect.php");

function getLatLong($postcode) {
    $postcode = urlencode(trim($postcode));
    $file = "https://maps.googleapis.com/maps/api/geocode/json?address=".$postcode."&;key=AIzaSyDwYgGY1vNAzuzp6K5K5knvwP3rGlMVuLQ&sensor=false";
    $contents = file_get_contents($file);
    //echo $contents;
    $obj = json_decode($contents, true);
    
    return array ($obj['results'][0]['geometry']['location']['lat'], $obj['results'][0]['geometry']['location']['lng']);
}

$sql = "SELECT id, address, citytown, stateprovincecounty, country, zippostcode
        FROM udf_5737034557EF5B8C02C0E46513B98F90
        WHERE country = 'South Africa'
        AND ISNULL(latitude)
        AND issaved = 1
        AND importSA = 0 ORDER BY id DESC LIMIT 150";
$res = mysql_query($sql);
while ($row = mysql_fetch_array($res)) {
    
    // Check if address is prefixed by 0
    $addressZ = substr($row['address'],0,2);
    $strlength = strlen($row['address']);
    $newAddress = $row['address'];
    
    if ($addressZ == "0 " ) {
        $newAddress = substr($row['address'],2,$strlength);
    }
    
    echo "<tr>";
    echo "<td>".$row['id']."</td>";
    echo "<td>".$row['address'].", ".$row['citytown'].", ".$row['stateprovincecounty'].", ".$row['country'].", ".$row['zippostcode']."</td>";
    
    $fullAddress = $row['address'].", ".$row['citytown'].", ".$row['stateprovincecounty'].", ".$row['country'].", ".$row['zippostcode'];
    $latlon = getLatLong($fullAddress);
    
    
    echo "<td>".$latlon[0]."</td>";
    echo "<td>".$latlon[1]."</td>";
    echo "<td>";
    if ($latlon[0] != "") {
        
        $fullAddressV2 = str_replace(" ","%20",$row['address']).", ".str_replace(" ","%20",$row['citytown']).", ".str_replace(" ","%20",$row['stateprovincecounty']).", ".str_replace(" ","%20",$row['country']).",".str_replace(" ","%20",$row['zippostcode']);
        $mapcode = '<iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" width="425" height="350" src="https://maps.google.com/maps?hl=en&q='.mysql_real_escape_string($fullAddressV2).'&ie=UTF8&t=roadmap&z=16&iwloc=B&output=embed"><div></div></iframe>';
        
        $sqlins = "UPDATE `udf_5737034557EF5B8C02C0E46513B98F90` SET `latitude` = '".$latlon[0]."', `longitude` = '".$latlon[1]."', mapcode = '".$mapcode."' WHERE `id` = ".$row['id'].";";
        mysql_query($sqlins);
        //echo $sqlins;
    }
    
    $sqlu = "UPDATE `udf_5737034557EF5B8C02C0E46513B98F90` SET `importSA` = '1' WHERE `id` = '".$row['id']."';";
    mysql_query($sqlu);
    echo "</td>";
    
    echo "</tr>";
}
?>