<!--<div id="processdiv" class="process" style="text-align: center; ">
    <img src="modalbox-loading.gif" class="processimg" style="width:50%">
</div>-->
<?php 
session_start();

if (!isset($_SESSION['Table'])) {
   $_SESSION['Table'] = 'information';  
}

$tableName = $_SESSION['Table'];

include '_dbinfo.php';
$con = mysqli_connect($servername, $username, $password, $db);

//echo $_POST['franchise']."<br>";

$sql = "SELECT MAX(recordid) AS recordid FROM ".$tableName." LIMIT 1";
//echo $sql."<br>";
$sql = mysqli_query($con, $sql);
$row=mysqli_fetch_assoc($sql);

$next_record = $row['recordid'];

$targetPath = $_POST['targetPath'];
$fieldName = $_POST['fieldName'];
$fieldNum = $_POST['fieldNum'];
$fieldOption = $_POST['fieldOption'];
$franchise = $_POST['franchise'];

$str1 = "";
$fl = 0;
foreach ($fieldOption as $val) {
   if ($val!="") {
	  if ($fl!=0) {
		 $str1 .= ',';
	  }
	  $new_val = $con->real_escape_string($val);
	  $str1 .= $new_val;
	  $fl++;
   }
}

$file = fopen($targetPath, "r");
$kid_list = array();
$c = 0;
while (($getData = fgetcsv($file, 10000, ",")) !== FALSE) {
   $str2 = "";
   foreach ($fieldNum as $val) {
		 
	  if ($fieldOption[$val] != "") {
		 
		 if($str2 !="") {
			$str2.=",";	
		 }
		 
		 if (($fieldOption[$val] == "fullname") && (strtolower($tableName) == strtolower("udf_45C48CCE2E2D7FBDEA1AFC51C7C6AD26"))) { // for guardians
			$is_guardian = 1;
		 } else {
			$is_guardian = 0;
		 }
		 
		 if ((trim($getData[$val]) == "") && ($is_guardian == 1)) {
			$skip_record = 1;
			break;
		 } else {
			$new_string = $con->real_escape_string($getData[$val]);
			$new_string = trim($new_string);
			if (filter_var($new_string,FILTER_VALIDATE_EMAIL)) {
			   // Lets check if the email address exists already, if so ignore
			   $sql_email = "SELECT email FROM udf_45C48CCE2E2D7FBDEA1AFC51C7C6AD26 WHERE email = '".$new_string."' AND issaved =  1 LIMIT 1";
			   //echo $sql_email."<br />";
			   $sql_email = mysqli_query($con, $sql_email);
			   $row = mysqli_fetch_assoc($sql_email);
			   if ($row['email']) {
				  $new_string = "";
			   }
			}
			$str2.= "'".$new_string."'";
			$skip_record = 0;
		 }
	  }	  
   }
   
   if ($skip_record == 0) {
	  $next_record = $next_record + 1;
	  $add_sql_fields = ", recordid, department, issaved, createddate, registrantid";
	  //echo $franchise."<br>";
	  $add_sql_values = ", ".$next_record.", '".$franchise."', '1', '".date("Y-m-d H:i:s")."', 1";
	  
	  $sql = "INSERT INTO ".$tableName." (".$str1.$add_sql_fields.") VALUES (".$str2.$add_sql_values.")";
	  //echo $sql."<br>";
	  //die();
	  $insert = mysqli_query($con, $sql) or die(mysqli_error());
	  $last_id = $con->insert_id;
	  
	  $sql = "UPDATE ".$tableName." SET hashid = '".strtoupper(md5($last_id))."' WHERE id = '".$last_id."'";
	  $update = mysqli_query($con, $sql) or die(mysqli_error());
	  
	  if ($_POST['is_guardian'] == "Yes") {
		 // Add child multilink record
		 $sql = "INSERT INTO `multilink` (`registrantid`, `parent_table`, `parent_record`, `child_table`, `child_record`) VALUES 
				  (1, '63538FE6EF330C13A05A3ED7E599D5F7', '".strtoupper(md5($_SESSION['kid_ids'][$c]))."', '45C48CCE2E2D7FBDEA1AFC51C7C6AD26', '".strtoupper(md5($last_id))."');";
		 $insert = mysqli_query($con, $sql) or die(mysqli_error());
		 
		 // guardian link record
		 $sql = "INSERT INTO `multilink` (`registrantid`, `parent_table`, `parent_record`, `child_table`, `child_record`) VALUES
				  (1, '45C48CCE2E2D7FBDEA1AFC51C7C6AD26', '".strtoupper(md5($last_id))."', '63538FE6EF330C13A05A3ED7E599D5F7', '".strtoupper(md5($_SESSION['kid_ids'][$c]))."');";
		 $insert = mysqli_query($con, $sql) or die(mysqli_error());
		 
	  } else {
		 $kid_list[$c] = $last_id;
	  }
   }
   
   $c++;
   
   if ($insert) {
	  $_SESSION['message']='<div class="alert alert-success alert-dismissable fade in">
	     <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>Success!</strong> Your csv file successful upload. </div>';
   } else {
	  $_SESSION['message']=' <div class="alert alert-danger alert-dismissable fade in">
		 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		  <strong>Danger!</strong> Your csv file not successfully uploaded.</div> ';	
   } 
}

if ($_POST['is_guardian'] == "No") {
   $_SESSION['kid_ids'] = $kid_list;
} ?>

<script>
   window.location.assign("./index.php");
</script>