<?php
session_start();
if(!isset($_SESSION['Table'])) {
	$_SESSION['Table'] ='information';  
}

include '_dbinfo.php';
$con = mysqli_connect($servername, $username, $password, $db);

if (isset($_POST['tbName'])) {
	$_SESSION['Table']=$_POST['tbName'];	
}

$tableName=$_SESSION['Table'];

if (isset($_GET['field'])) {
	$field=$_GET['field'];
	//echo "ALTER TABLE `information` DROP `".$field."`";
	$updateTab=mysqli_query($con, "ALTER TABLE `".$tableName."` DROP `".$field."`") ;
}

if (isset($_POST['AddTable'])) {
	 $dbTable=$_POST['dbTable'];
	 $sql = "CREATE TABLE ".$dbTable." ( id INT NOT NULL AUTO_INCREMENT, PRIMARY KEY (id))";
	 $insert=mysqli_query($con, $sql);
	 $_SESSSION['Table'] = $dbTabl;
}

if (isset($_POST['newField'])) {
	$dbField=$_POST['dbField'];
	$update=mysqli_query($con,"ALTER TABLE  `".$tableName."` ADD  `".$dbField."` VARCHAR( 200 ) NOT NULL ");
} ?>
<!DOCTYPE html>
<html>
<head>
	<title>Data Import Tools</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="http://fontawesome.io/assets/font-awesome/css/font-awesome.css">
    <style type="text/css">
    	.linkconfig {float: right; font-weight: bold; }
    </style>
</head>
<body>
     <div id="wrap">
        <div class="container">
            <div class="row">
                <span class="linkconfig"> <a href="index.php"> Data Manager</a> </span>
                <legend>  Config  Manager </legend>
            </div>
             <div class="row">
             	    
             		<div class="col-md-4">


             			<form action="" method="post">
             				<?php 
             					$sql = "SHOW TABLES FROM ".$db;
								$result = mysqli_query($con,$sql);
             				?>
             				<div class="form-group">
							    <label for="dbTable"> Data Base Table</label>
							    <select class="form-control" name="tbName" onchange="this.form.submit()" >
							     	<?php  while ($row = mysqli_fetch_row($result)) { ?>
							     	<option  value="<?php echo $row[0]; ?>" <?php if($row[0] == $tableName){ echo "selected"; }   ?> > <?php echo $row[0]; ?></option>
							     	<?php } ?>
							     </select>

							    <!--  <input type="text" class="form-control" value="Information" required readonly > -->
			
							</div>
						</form>


             			<!--<form action="" method="post">
	             			<div class="form-group">
							    <label for="dbTable"> Data Base Table</label>
							    <input type="text" class="form-control" id="dbTable" name="dbTable" required>
							</div>
							<button type="submit" name="AddTable" class="btn btn-default">Add Table</button>
						</form>-->

						<br/>

						<form action="" method="post">
	             			<div class="form-group">
							    <label for="dbTable"> Table Field </label>
							    <input type="text" class="form-control" id="dbField" name="dbField">
							</div>
							<button type="submit"  name="newField" class="btn btn-default">Add New Field</button>
						</form>


             		</div>
             		<div class="col-md-6">
             			<?php 
             				$selectF=mysqli_query($con, "SHOW COLUMNS FROM `".$tableName."` ");
             				while ($row=mysqli_fetch_assoc($selectF)) {
             			?>
             			<div class="form-group">
						    <label for="email">
						    	<?php echo $row['Field']; ?>	  
						    </label>
						    <a href="dbconfig.php?field=<?php echo $row['Field']; ?>" class="linkconfig"> Delete  </a>
						</div>
						<?php } ?>
             		</div>
             	
              </div>

        </div>
    </div>
</body>
</html>