<?php
session_start();
if(!isset($_SESSION['Table'])){
 $_SESSSION['Table'] ='information';  
}
  
$tableName=$_SESSION['Table'];

include '_dbinfo.php';
$con = mysqli_connect($servername, $username, $password, $db);

$mimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');
if(in_array($_FILES['file']['type'],$mimes)){
?>
	<form action="new.php" method="post" enctype="multipart/form-data">
		
   		<table class="table table-hover">
		    <thead>
		      <tr>
		        <th>CSV Field</th>
		        <th>Import Field</th>
		        <th></th>
		      </tr>
		    </thead>
		    <tbody>
		      <?php 
				$filename=$_FILES["file"]["tmp_name"];  

		        $sourcePath = $_FILES['file']['tmp_name'];
				$targetPath = $_FILES['file']['name'];
				move_uploaded_file($sourcePath,$targetPath);  


		       if($_FILES["file"]["size"] > 0) {	
		          $file = fopen($targetPath, "r");
		          $count=0;
		          while (($getData = fgetcsv($file, 10000, ",")) !== FALSE){
		            foreach ($getData as $value) {
		        ?>
		          <tr>
		            <td  class="col-md-2"><?php echo $value; ?></td>
		            <td class="col-md-5">
		                <input type="hidden" value="<?php echo $value; ?>" name="fieldName[]">
		                <input type="hidden" value="<?php echo $count; ?>" ="" name="fieldNum[]">
		                 <select class="form-control" id="sel1" name="fieldOption[]">
		                    <option value=""> Don't map this field </option>
		                    <?php
								$sql = "SHOW COLUMNS FROM `". $tableName."`";
								echo $sql;
             					$selectF=mysqli_query($con, $sql);
             					while ($row=mysqli_fetch_assoc($selectF)) {
             				?>
		                   			 <option value="<?php echo $row['Field']; ?>"><?php echo $row['Field']; ?> </option>
		                    <?php } ?>

		                </select>
		            </td>
		            <td> <!-- <a href=""> Select form Existing </a> --> </td>
		            <td> <!-- <a href=""> Custom </a> --> </td>
		            <td> <!-- <a href=""> Suggest </a> --> </td>
		            <td> <!-- <a href=""> Add to Field </a>  --></td>
		          </tr>
		      	<?php  $count++;  }
		        	break;
		       	 }
		    	} ?>
		   </tbody>
	    </table>
        <input type="hidden" name="targetPath" value="<?php echo $targetPath; ?>">
		<div class="form-group">
			<div class="col-md-4">
				<select class="form-control" name="franchise" id="franchise" required>
					<option value="">Select One</option>
					<?php
					$sql = mysqli_query($con, "SELECT id, department FROM departments WHERE isactive = 1 ORDER BY department ASC");
					while ($row=mysqli_fetch_assoc($sql)) {
						echo "<option value='".$row['id']."'>".$row['department']." (".$row['id'].")</option>";
					}
					?>
				</select>	
			</div>
		</div><br /><br />
        <div class="form-group">
            <div class="col-md-4">
                <select name="is_guardian" class="form-control" id="is_guardian" required>
					<option value="">Is this a guardian?</option>
					<option value="No">No</option>
					<option value="Yes">Yes</option>
				</select>
            </div>
        </div><br /><br />
        <div class="form-group">
            <div class="col-md-4">
                <button type="submit" id="import" name="import" class="btn btn-primary button-loading" data-loading-text="Loading...">Import</button>
            </div>
        </div>        
    </form>
    <?php
	} else {
  		die("Sorry, mime type not allowed");
	}

	?>