<?php
function getdb() {
    include '_dbinfo.php';
    try {
        $con = mysqli_connect($servername, $username, $password, $db);     
    } catch(exception $e) {
        echo "Connection failed: " . $e->getMessage();
    }
    return $con;
} ?>