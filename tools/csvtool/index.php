<?PHP
session_start();
//print_r($_SESSION['kid_ids']);
if(!isset($_SESSION['Table'])){
   $_SESSION['Table'] ='information';  
}
   $tableName=$_SESSION['Table'];
  include '_dbinfo.php';
  $con = mysqli_connect($servername, $username, $password, $db);
  //include("config.php");
  //include("functions.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://fontawesome.io/assets/font-awesome/css/font-awesome.css">
    <style type="text/css">
      body { background-color: #f5f5f5;  }
      .container { background: #fff; padding: 50px; }
      .file { visibility: hidden; position: absolute; }
      .input-group-addon { padding: 15px 6px !important;}
      div#processdiv {text-align: center; }
      .linkconfig {float: right; font-weight: bold; }
    </style>
    <script type="text/javascript">
      $(document).on('click', '.browse', function(){
        var file = $(this).parent().parent().parent().find('.file');
        file.trigger('click');
      });
      $(document).on('change', '.file', function(){
        $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
      });
    </script>
</head>
 
<body>
    <div id="wrap">
        <div class="container">
            <div class="row">
              <script type="text/javascript">
                  $(document).ready(function (e){
                    $("#uploadForm").on('submit',(function(e){
                       $("#processdiv").show();
                      e.preventDefault();
                        $.ajax({
                        url: "upload.php",
                        type: "POST",
                        data:  new FormData(this),
                        contentType: false,
                        cache: false,
                        processData:false,
                        success: function(data){
                           $("#processdiv").hide();
                        $("#targetLayer").html(data);
                        },
                        error: function(){}           
                      });
                    }));
                  });
                  </script>
                   <span class="linkconfig"> <a href="dbconfig.php">Config</a> </span>
                <legend> Data Manager </legend>


                <?php 
                  if(isset($_SESSION['message'])){
                    echo $_SESSION['message'];
                    unset($_SESSION['message']);
                  }
                ?>




                <ul class="nav nav-tabs">
                  <li  class="active"><a data-toggle="tab" href="#menu1"> <i class="fa fa-bar-chart" aria-hidden="true"></i> Data Import</a></li>
                  <li><a data-toggle="tab" href="#menu2"> <i class="fa fa-money" aria-hidden="true"></i>  Data</a></li> 
                </ul>

                <div class="tab-content">
                  <div id="menu1" class="tab-pane fade in active">
                    <br/>
                    <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#demo" > CSV Import Contacts </button>
                     <br/> <br/>
                    
                  
                 </div>
                  <div id="menu2" class="tab-pane fade">

                  		<!--<table class="table table-hover">
                  			<tr>
                           <?php
							  $sql = "SHOW COLUMNS FROM `".$tableName."`";
                              $selectF=mysqli_query($con, $sql);
                              while ($row=mysqli_fetch_assoc($selectF)) {
                            ?>
                  				    <th><?php echo $row['Field']; ?></th>
                          <?php } ?>
                  			</tr>
                  			<?php 
                  				$sel=mysqli_query($con,"SELECT * FROM `".$tableName."` ");
                  			  while ($row=mysqli_fetch_assoc($sel)) {
                  			?>
                  				<tr>
                          <?php

                            foreach ($row as  $value) {
                          ?>
                  					<td> <?php   echo $value;  ?> </td>
                          <?php } ?>
                  				</tr>

                  			<?php 
                  			     }
                  			?>


                  		</table>-->

                   	
                  </div>
                </div>
            </div>
          
              <div id="demo" class="collapse" >
                  <form id="uploadForm" class="form-horizontal" action="" method="post" name="upload_excel" enctype="multipart/form-data">
                                 <div class="form-group">
                                    <input type="file" name="file" id="file" class="file">
                                    <div class="input-group col-xs-6">
                                      <input type="text" class="form-control input-lg" disabled placeholder="Select File - CSV Only">
                                      <span class="input-group-btn">
                                        <button class="browse btn btn-primary input-lg" type="button"><i class="glyphicon glyphicon-search"></i> Browse</button>
                                      </span>
                                    </div>
                                  </div>
                                <!-- Button -->
                                
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <button type="submit" id="submit" name="Continue" class="btn btn-primary button-loading" data-loading-text="Loading...">Continue</button>
                                    </div>
                                </div>
                        </form>
                  </div>
                    <div id="processdiv" class="process" style="display: none;">
                      <img src="modalbox-loading.gif" class="processimg">
                    </div>
                  <div id="targetLayer">
                  </div>




            <?php
			//echo "Hello";
			//print_r($_SESSION['kid_ids']);
             //get_all_records();
            ?>

            <!--   <div>
                <form class="form-horizontal" action="functions.php" method="post" name="upload_excel"   
                          enctype="multipart/form-data">
                  <div class="form-group">
                    <div class="col-md-4 col-md-offset-4">
                        <input type="submit" name="Export" class="btn btn-success" value="export to excel"/>
                    </div>
                   </div>                    
                </form>           
             </div> -->

        </div>
    </div>
</body>
 
</html>