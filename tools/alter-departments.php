<font style="font-size:14px;font-family:Arial">
<?php
include_once("..//_globalconnect.php"); 

$whichDB = "Classes";
$currentDepartment = 16;
$newDepartment = 29;
$filename = 'user-files/kingston-cass-list.csv';

if (file_exists($filename)) {
    echo "The file $filename exists<br><br>";
} else {
    echo "The file $filename does not exist<br><br>";
}

$arr = array();
$row = -1;
if (($handle = fopen($filename, "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        $num = count($data);
        $row++;
        if ($row != 0) {
        for ($c = 0; $c < $num; $c++) {
            $arr[$row][$c]= $data[$c];
        }
        }
    }
    
    fclose($handle);
}

switch ($whichDB) {

    case "Venues":
        $table = "udf_5737034557EF5B8C02C0E46513B98F90";
        $columns = "department, venuename"; // venue
        break;
    case "Classes":
        $table = "udf_2B8A61594B1F4C4DB0902A8A395CED93";
        $columns = "department, classname"; // classes
        break;
    case "Bookings":
        $table = "udf_2BB232C0B13C774965EF8558F0FBD615";
        $columns = "department, guardiansemail, childsname"; // bookings
        break;
        
}

if (($table == "udf_5737034557EF5B8C02C0E46513B98F90") || ($table == "udf_2B8A61594B1F4C4DB0902A8A395CED93")) {
    foreach ($arr AS $recordEntry) {
        echo "Record ID: ".$recordEntry[0]." - ".$recordEntry[1]." ( ".$currentDepartment." ) --- ";
        
        $sql = "SELECT ".$columns." FROM ".$table." WHERE recordid = '".$recordEntry[0]."' LIMIT 1";
        //echo $sql."<br>";
        $res = mysql_query($sql);
        $row = mysql_fetch_array($res);
        
        $checkcolumn = substr($columns, 12);
        echo $row[$checkcolumn]." ( ".$row['department']." )";
        
        if ( ($recordEntry[1] == $row[$checkcolumn]) && ($row['department'] == $currentDepartment) ) {
            echo " --- Match";
            
            // Seems to be a match.  Let's update
            if ($_GET['action'] == "go") {
                $sql = "UPDATE `".$table."` SET `department` = '".$newDepartment."' WHERE `recordid` = ".mysql_real_escape_string($recordEntry[0]).";";
                echo "<br />".$sql."<br>";
                mysql_query($sql);
            }
            
        } else {
            echo " --- <strong>Failed</strong>";
        }
        
        echo "<br>";
    }
} elseif ($table == "udf_2BB232C0B13C774965EF8558F0FBD615") {
    // Update bookings
    foreach ($arr AS $recordEntry) {
        echo "Record ID: ".$recordEntry[0]." - ".$recordEntry[1]." ( ".$currentDepartment." ) --- ";
        
        $sql = "SELECT ".$columns." FROM ".$table." WHERE recordid = '".$recordEntry[0]."' LIMIT 1";
        //echo "<br>".$sql."<br>";
        $res = mysql_query($sql);
        $row = mysql_fetch_array($res);
        $guardiansemail = $row['guardiansemail'];
        
        $checkcolumn = substr($columns, 28);
        echo $row[$checkcolumn]." ( ".$row['department']." )";
        
        if ( ($recordEntry[1] == $row[$checkcolumn]) && ($row['department'] == $currentDepartment) ) {
            
            $match = 1;
			// Update bookings
			$sql = "UPDATE `".$table."` SET `department` = '".$newDepartment."' WHERE `recordid` = ".mysql_real_escape_string($recordEntry[0])." LIMIT 1;";
			//echo "<br />".$sql."<br>";
			mysql_query($sql);
            
            // Good on the booking lets check the kids out.
            $sql = "SELECT recordid, childsname, department
                    FROM `udf_63538FE6EF330C13A05A3ED7E599D5F7`
                    WHERE childsname = '".$recordEntry[1]."' LIMIT 1";
            //echo "<br>".$sql;
            $res = mysql_query($sql);
            $row = mysql_fetch_array($res);
            $childrecorid = $row['recordid'];
            
            $match = ($row['department'] == $currentDepartment) ? 1 : 0;
            // Check the system dept isn't assigned
            if ($match == 0) { 
				$match = (substr($row['department'],3) == $currentDepartment) ? 1 : 0; 
				echo "<br>Child fail: ".$row['department'];
			}
            
			// Update kids
			$sql = "UPDATE `udf_63538FE6EF330C13A05A3ED7E599D5F7` SET `department` = '".$newDepartment."' WHERE `recordid` = ".mysql_real_escape_string($childrecorid)." LIMIT 1;";
			//echo $sql."<br>";
			mysql_query($sql);
			echo "<br>Kid ID: ".$childrecorid."<br>";
			
            //  Now we check the parent
            $sql = "SELECT recordid,  email, department
                    FROM `udf_45C48CCE2E2D7FBDEA1AFC51C7C6AD26`
                    WHERE email = '".$guardiansemail."' LIMIT 1";
            //echo "<br>".$sql;
            $res = mysql_query($sql);
            $row = mysql_fetch_array($res);
            $parentrecordid = $row['recordid'];
            
            $match = ($row['department'] == $currentDepartment) ? 1 : 0;
            // Check the system dept isn't assigned
            if ($match == 0) { 
				$match = (substr($row['department'],3) == $currentDepartment) ? 1 : 0;
				echo "<br>Parent fail: ".$row['department'];
			}
			
			// Update parents
			$sql = "UPDATE `udf_45C48CCE2E2D7FBDEA1AFC51C7C6AD26` SET `department` = '".$newDepartment."' WHERE `recordid` = ".mysql_real_escape_string($parentrecordid)." LIMIT 1;";
			//echo $sql."<br>";
			mysql_query($sql);
			echo "Parent ID: ".$parentrecordid."<br>";
                 
            if ($match == 1) {
                echo " --- Match";
                // Seems to be a match.  Let's update
                //if ($_GET['action'] == "go") {
                
                    
                    
                    
                    
                    
                    
                //}
            } else {
                echo " --- <strong><font color=red>Failed 1</font></strong>";
            }
            
        } else {
            echo " --- <strong><font color=red>Failed 2</font></strong>";
        }
        
        echo "<br><br>";
    }
    
} ?>

<p><a onclick="return confirm('Make sure you have backed up the database before making this change?');" href="<?php echo $_SERVER['PHP_SELF'];?>?action=go">Execute Update</a></p>

</font>