<?php 
include_once("_globalconnect.php");?>
<html>
<head>
	<meta http-equiv="refresh" content="60">
</head>
<body>
<font style="font-family:Tahoma;font-size:11px">
<?php
function sendsms($activityid,$mobilenumber,$subjectheader,$starttime,$notessend) {
	
	$totallength = (strlen($subjectheader) + strlen(substr($starttime,11,-3)) + strlen($notessend));
	$totallength = ($totallength + 41);
	
	$notessend = str_replace("\r"," ",$notessend);
	$notessend = str_replace("\n"," ",$notessend);
	$notessend = str_replace(" ","+",$notessend);
	$notessend = str_replace("++++++","+",$notessend);
	$notessend = str_replace("+++++","+",$notessend);
	$notessend = str_replace("++++","+",$notessend);
	$notessend = str_replace("+++","+",$notessend);
	$notessend = str_replace("++","+",$notessend);
	
	$textmessage = "Thinline+Software+Reminder:%0D%0A";
	$textmessage .= "Subj:+".strtoupper(str_replace(" ","+",$subjectheader))."%0D%0A";
	$textmessage .= "@:+".strtoupper(str_replace(" ","+",substr($starttime,11,-3)))."%0D%0A%0D%0A";
	$textmessage .= $notessend;
	
	if ($totallength > 160) {
		$textmessage = substr($textmessage,0,157);
		$textmessage = $textmessage."...";
	}
	echo $textmessage;
	
	$ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "http://api.clickatell.com/http/sendmsg?api_id=".CTAPIID."&user=".CTUSER."&password=".CTPASS."&to=".$mobilenumber."&text=".$textmessage."");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $definition = curl_exec($ch);
    curl_close($ch);
	
	include('includes/audit_trailing.php');
	////  Audit trail = action / table / item id, datestamp
	$details = strtoupper(md5($activityid)).":".$mobilenumber;
	audittrail("sent sms", "activities", $details, $sGMTMySqlString);
}

	// Lets find the time difference between a scheduled reminder and an alarm
	$query = "SELECT * FROM activities WHERE activitystatus != 'Completed' AND smsreminder = 'on' AND reminderdisabled != 1 ORDER BY ID DESC ";
	// echo $query;
	$result = mysql_query($query);
	while ($row = mysql_fetch_array($result)) {
		$time_since_schedule = $thetimestamp - strtotime($row['datestart']);
		echo $row['id']." // ".$row['datestart']." // <strong>".$time_since_schedule."</strong> // ".$row['remindertime']."<br>";
		
		if ($time_since_schedule >= 0) {
			// Lets check when the last one was sent out.
			$queryrem = "SELECT datecreated FROM activities_reminders WHERE activityid  = ".$row['id'];
			$resultrem = mysql_query($queryrem);
			$rowrem = mysql_fetch_array($resultrem);
			
			if ($rowrem['datecreated'] == "") {
				
				$queryins = "INSERT INTO activities_reminders (activityid, remindertype, datecreated) VALUES (
						".$row['id'].",
						'text',
						'".$sGMTMySqlString."'
						)";
				mysql_query($queryins);
				
				// No reminder been sent, lets fire one out.
				$querymob = "SELECT mobilenumber FROM users WHERE id = ".$row['userid'];
				$resultmob = mysql_query($querymob);
				$rowmob = mysql_fetch_array($resultmob);
				
				//Testing through cron
				$tUnixTime = time();
				$sGMTMySqlString = date("Y-m-d H:i:s", $tUnixTime);
				$query = "INSERT INTO text_msg_counter (userid, registrantid, datecreated) VALUES (
			 			  '".$row['userid']."',
						  '".$row['registrantid']."',
						  '".$sGMTMySqlString."'
						  )";
				mysql_query($query) Or Die ("Cannot submit entry!");
				//echo $query;
				sendsms($row['id'],$rowmob['mobilenumber'],$row['subject'],$row['datestart'],$row['notes']);
			} else {
				// A reminder has been sent let's check how long ago.
				echo "A reminder was sent this many seconds ago: <strong>".($thetimestamp - strtotime($rowrem['datecreated']))."</strong><br><br>";
			}
			
		} else {
			echo "<strong>No reminder required yet.</strong><br><br>";
		}
	}
?> 

</font>
</body>
</html>