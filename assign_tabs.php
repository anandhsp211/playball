<?php 
include_once("_globalconnect.php");

if ($_POST['generator'] == "Processing") {
	// Lets get the highest sort value and add one more
	$query = "SELECT MAX(sortorder) AS EditID FROM relations_matrix WHERE parenttable = '".$gval[0]."'";
	// echo $query;
	$result = mysql_query($query);
	$row = mysql_fetch_array($result);
	$sortid = ($row['EditID']+1);
	echo $sortid."<br>";
	
	$arr = $_POST['modtabc'];
	$counter = 0; 
	foreach ($arr as &$value) {
		$value2 = explode(":",$value);
		
		// Lets get the pre-defined element location, to see if we need to 
		// set it up as pre-defined
		$query = "SELECT location, comtype, ismultilink FROM relations_predefined WHERE id = ".$value2[0]." AND registrantid = ".RID;
		echo $query;
		$result = mysql_query($query);
		$row = mysql_fetch_array($result);
		if ($row['comtype'] == NULL) {
			$child_table = $value2[0];
			$predefined_val = 1;
		} elseif ($row['comtype'] == 1) {
			// type product
			$child_table = $value2[0];
			$predefined_val = 2;
		} elseif ($row['comtype'] == 2) {
			// type resource
			$child_table = $value2[0];
			$predefined_val = 3;
		}
		$ismultilink = $row['ismultilink'];
		if ($ismultilink == "") {
			$ismultilink = 0;
		}
		$query = "INSERT INTO relations_matrix (registrantid, menutabid, parenttable, childtable, ispredefined, ismultilink, sortorder) VALUES 
		(".RID.",
		'".strtoupper($gval[1])."',
		'".$value2[1]."',
		'".$child_table."',
		".$predefined_val.",
		".$ismultilink.",
		".$sortid++."
		)";
		mysql_query($query);
		echo $query."<br>";
		unset($predefined_val);
	}
	
	if ($_GET['reloaded'] == "") {
		header("Location: ".$_SERVER['PHP_SELF']."?reloaded=1");
	} 
}

	if ($_GET['reloaded'] == 1) {
		$onload = 'onload="window.parent.location = window.parent.location;self.close();return false;"';
	} 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "includes/xhtml1-transitional.dtd">
<html>
<head>
	<title>Thinline Software - Assign Module Tabs</title>
	<?php 
	if (ERES > 1024) { ?>
		<style type="text/css" media="all">@import "css/style.css";</style>
	<?php 
	} else { ?>
		<style type="text/css" media="all">@import "css/style_small.css";</style>
	<?php
	}?>
</head>

<body <?php echo $onload ?>>
<form method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?>?id=<?php echo $gval[0] ?>:<?php echo $gval[1] ?>" name="modulemanager">
<table cellspacing="0" cellpadding="0" border="0" width="97%">
	<tr>
		<td><img src="images/<?php echo $_SESSION['franchisedata']['logo'];?>" alt="" align="center" border="0"></td>
		<td align="right"></td>
	</tr>
	</table>
	<table cellspacing="0" cellpadding="1" border="0" width="98%">
	<tr>
		<td height="5" colspan="5"><img src="images/spacer.gif" height="5" width="1"></td>
	</tr>
	<tr>
		<td width="5%" rowspan="2">&nbsp;</td>
		<td valign="top" align="right" width="5%"><br>
			<img src="images/icons/dot-chart.png"><img src="images/spacer.gif" height="1" width="10">
		</td>
		<td valign="top" height="5" width="60%">
			<br><strong><font style="font-size:140%;font-family:Trebuchet MS">Assign Component(s)</font></strong><br><br>
		</td>
		<td valign="top" height="5" >
		&nbsp;
		</td>
		<td rowspan="3">&nbsp;<img src="images/spacer.gif" width="15"></td>
	</tr>
	<tr>
		<td valign="top" colspan="3" class="ls_on" align="center">
			<script type="text/javascript" language="JavaScript" src="js/formtool.js"></script>
			<table border="0" width="85%" cellpadding="5" cellspacing="3">
			<tr>
				<td valign="top">
				<?php
				$sql = "SELECT id, label, location FROM relations_predefined WHERE registrantid = ".RID." OR registrantid = 0 ORDER BY label ASC";
				//echo $sql;
				$res = mysql_query($sql);
				?>
			   		Available Component(s):<br>
			    	<select name="modtab[]" multiple style="font-family:Tahoma;font-size:11px;width:200px;height:100px">
				  <?php 
				 	while ($row = mysql_fetch_array($res)) { 
						$query = "SELECT id FROM relations_matrix WHERE parenttable = '".$gval[0]."' AND childtable ='".$row['id']."' LIMIT 1";
						// echo $query;
						$result = mysql_query($query);
						$rowcheck = mysql_fetch_array($result); 
						if (!$rowcheck['id'] != "") {?>
				  		<option label="<?php echo $row['label'] ?>" value="<?php echo $row['id'] ?>:<?php echo $gval[0] ?>:<?php echo $row['location'] ?>"><?php echo $row['label'] ?></option>
					<?php
						}
				  	} ?>										  
	 				</select>
    	  			<input type="hidden" name="modtab_save">      
				</td>
				<td align="center"><br>
					<input type=button value="Add &gt&gt;" style="font-family:Arial;font-size:14px;width:100px" onClick="formtool_move(this.form.elements['modtab[]'],this.form.elements['modtabc[]'],this.form.elements['modtab_save'],this.form.elements['modtabc_save'])"><br><br>
					<input type=button value="&lt&lt; Remove" style="font-family:Arial;font-size:14px;width:100px" onClick="formtool_move(this.form.elements['modtabc[]'],this.form.elements['modtab[]'],this.form.elements['modtab_save'],this.form.elements['modtabc_save'])">
				</td>
				<td valign="top">Component(s) to Add:<br>
					<select name="modtabc[]" multiple style="font-family:Tahoma;font-size:11px;width:200px;height:100px">
					</select><input type="hidden" name="modtabc_save">
				</td>
			</tr>
			<tr>
				<td align="left" colspan="4">
					<input type="submit" name="generator" value="Add Component(s)" onClick="this.value=formtool_selectall('modtabc[]', this.form.elements['modtabc[]'],'Add Module Tabs','Processing')" style="font-family:Arial;font-size:14px;">
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td colspan="2">
			<br>
			<font style="font-family:tahoma;font-size:11px">
			Select the Module Tabs on the right that you want to allocate to this module.  Click the Add button and then the Add Module Tabs.
			</font>
		</td>
	</tr>
	</table>
	</form>
</body>
</html>
