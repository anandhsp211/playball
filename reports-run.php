<?php
$sqlr = "SELECT * FROM udf_2BB232C0B13C774965EF8558F0FBD615 WHERE hashid = '".mysql_real_escape_string($gval[3])."'";
//echo $sqlr;
$resr = mysql_query($sqlr);
$row = mysql_fetch_array($resr);

if ($gval[2] != "") {
	$ans_array = array();
	$sql_r = "SELECT * FROM reports_answers WHERE report_id = '".mysql_real_escape_string($gval[2])."'";
	//echo $sql_r."<br>";
	$res_r = mysql_query($sql_r);
	while ($row_r = mysql_fetch_array($res_r)) {
		$ans_array[$row_r['action_id']] = $row_r['answer'];
	}
	
	$sql_r = "SELECT * FROM reports WHERE id = '".mysql_real_escape_string($gval[2])."'";
	//echo $sql_r."<br>";
	$res_r = mysql_query($sql_r);
	$row_r = mysql_fetch_array($res_r);
	
	$reports_comments = $row_r['comments'];
	$reports_grade = $row_r['grade'];
	
} ?>
<style>
	.grey {
		background: #fafafa;
	}
	
	a.test:before {
		padding-right: 5px;
		vertical-align: middle;
		content: url(/images/icons/add2.png);
	}
</style>
<h3 style="font-size:18px;">Report for <?php echo $row['childsname'];?></h3>
<?php

$group_array = array();
$id_vals = $_GET['id'];
$actual_link = "http://$_SERVER[HTTP_HOST]";
$actual_link = $actual_link."/thinline.php?id=".urldecode($id_vals);

$sqli = "SELECT rg.group_name AS group_name FROM reports_groups rg
			LEFT JOIN reports_ordering ro ON ro.group_name = rg.group_name
			GROUP BY rg.group_name
			ORDER BY ro.group_order ASC";
$resi = mysql_query($sqli);
$first_item = 1;
while ($rowi = mysql_fetch_array($resi)) {
	array_push($group_array,$rowi['group_name']);
} 
$stages_array = array("Beginner","Intermediate","Advanced"); ?>

<form method="POST">
<table border="0">
	<tr>
		
		<td style="width:25px;">&nbsp;</td>
		<td>
			<select onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);" style="font-size: 14px;padding:3px;" name="group">
				<option value="">Select One</option>
				<?php
				$_GET['stage'] = ($_GET['stage'] == "") ? "Beginner" : $_GET['stage'] ;
				foreach ($group_array AS $group) {
					$selected = ($_GET['group'] == $group) ? "selected " : "" ;
					echo '<option '.$selected.'value="'.$actual_link."&group=".str_replace(" ","+",$group).'&stage='.$_GET['stage'].'&r='.$_GET['r'].'">'.$group.'</option>';
				}
				?>
			</select>
		</td>
		<td>
			<select onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);" style="font-size: 14px;padding:3px;" name="stage">
				<?php
				$_GET['group'] = ($_GET['group'] == "") ? "All" : $_GET['group'] ;
				foreach ($stages_array AS $stage) {
					$selected = ($_GET['stage'] == $stage) ? "selected " : "" ;
					echo '<option '.$selected.'value="'.$actual_link."&group=".str_replace(" ","+",$_GET['group']).'&stage='.$stage.'&r='.$_GET['r'].'">'.$stage.'</option>';
				} ?>
			</select>
		</td>
		<td>&nbsp;&nbsp;</td>
		<td colspan="2">
			<?php
			if ($_COOKIE['default_icando'] == "on") { ?>
				<input type="submit" style="font-size:12px;" name="default_icando" value="Disable I Can Do as Default">
			<?php
			} else { ?>
				<input type="submit" style="font-size:12px;" name="default_icando" value="Set I Can Do as Default">
			<?php
			} ?>
			<input type="hidden" name="return" value="<?php echo $_GET['r'];?>" />
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="4">
			<font style="font-size:12px;">Remember to click save at the bottom of the page.</font>
		</td>
	</tr>
</table>
</form>
<br />
<script>
	function validateForm() {
    var x = document.forms["myform"]["grade"].value;
	if (x == "") {
        alert("Please enter the Grade/Standard/Year");
        return false;
    }
}
</script>
<style>
	input[type="radio"] {
    -ms-transform: scale(1.5); /* IE 9 */
    -webkit-transform: scale(1.5); /* Chrome, Safari, Opera */
    transform: scale(1.2);
}
</style>

<?php
if ($_GET['group'] != "All") { ?>
	<form method="POST" name="myform" action="/reports/report-save.php" onsubmit="return validateForm();">
		<table border="0" style="width:100%;font: 14px Arial" cellpadding="8" cellspacing="2">
		<?php
		
		$group_array = ($_GET['group'] == "All") ? "" : $group_array = array($_GET['group']) ;
		$stages_array = ($_GET['stage'] == "All") ? array("Beginner") : $stages_array = array($_GET['stage']) ;
		
		foreach ($group_array AS $group) {
			
			foreach ($stages_array AS $stage) {
			
				$sql = "SELECT * FROM reports_groups WHERE group_name = '".mysql_real_escape_string($group)."' AND group_stage = '".$stage."' ORDER BY group_category DESC, id ASC";
				//echo $sql;
				$res = mysql_query($sql);
				$count = 1;
				$name_c = 1;
				while ($row = mysql_fetch_array($res)) {
					
					include $_SERVER['DOCUMENT_ROOT'].'/includes/ls.php';
						
					if ($current_name != $row['group_name']) {
						$name_c = 1;	
					}
					
					if ($current_category != $row['group_category']) {
						$count = 1;	
					}
					
					if ($name_c == 1) {
						echo ($first_item != 1) ? '<tr><td colspan=3>&nbsp;</td></tr>' : "" ;
						echo "<tr style='background:".$age_group_colors[$row['group_name']].";'>";
						echo "<td colspan='3'><strong>".$row['group_name']." - ".$stage."</strong></td>";
						echo "</tr>";
						$current_name = $row['group_name'];
					}
					
					if ($count == 1) {	
						echo "<tr style='background:".$age_group_colors[$row['group_name']].";'>";
						echo "<td colspan='2'><strong>".$row['group_category']." &nbsp;(".$row['group_name']." - ".$stage.")</strong></td>";
						echo "<td width='10%' align='center'><strong>Actions</strong></td>";
						echo "</tr>";
						$current_category = $row['group_category'];
					}
					
					echo "<tr class='ls_".$ls."'>";
					echo "<td>".$row['group_action']."</td>";
					echo "<td>".nl2br($row['action_descriptors'])."</td>";
					echo "<td align='left'>";
					
					if ($gval[2] != "") {
						
						// we set it as the default forst and change accordingly below
						if ($_COOKIE['default_icando'] == 'on') {
							$check_1 = " checked ";
						}
						
						if ($ans_array[$row['id']] == 1) {
							$check_1 = " checked ";
						} elseif ($ans_array[$row['id']] == 2) {
							$check_2 = " checked ";
						}
					}
					
					?>
					<input <?php echo $check_1;?>type="radio" name="question_<?php echo $row['id'];?>" value="1" style="font-size:13px;" />I Can Do It<br />
					<input <?php echo $check_2;?>type="radio" name="question_<?php echo $row['id'];?>" value="2">Still Learning
					<!--<select name="question_<?php echo $row['id'];?>" style="font-size:14px; padding:3px;">
						<option value="1">I Can Do It</option>
						<option value="2">I Am Still Learning</option>
					</select>-->
					<?php
					unset($check_1,$check_2);
					echo "</td>";
					echo "</tr>";
					
					$count++;
					$name_c++;
					$first_item++;
				}
			}
		}
		?>
		<tr>
			<td colspan="3">
				<textarea name="comments" style="width:100%; height:135px;font-size:14px" placeholder="Insert comments here"><?php echo $reports_comments;?></textarea>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<?php
				$sql = "SELECT coach FROM udf_2B8A61594B1F4C4DB0902A8A395CED93 WHERE hashid = '".mysql_real_escape_string($gval[4])."' AND issaved = 1 ".$addsqldepartments.";";
				$res = mysql_query($sql);
				$row = mysql_fetch_array($res);
				$coach = $row['coach'];
				?>
				<select name="coach" style="font-size:14px; padding:3px;">
					<?php
					$sql = "SELECT * FROM udf_81448138F5F163CCDBA4ACC69819F280 WHERE issaved = 1 AND isactive = 'Yes' ".$addsqldepartments.";";
					//echo $sql."<br />";
					$res = mysql_query($sql);
					while ($row = mysql_fetch_array($res)) {
						$sel = ($coach == $row['name']) ? " selected " : "" ;
						echo "<option ".$sel." value='".$row['hashid']."'>".$row['name']."</option>";
					} ?>
				</select>
				<input type="text" name="grade" placeholder="Enter Grade/Standard/Year" style="font-size: 14px;padding:3px;width: 205px;" value="<?php echo $reports_grade;?>" />
				<input type="submit" value="Save Report" style="font-size:14px; padding:3px;">
			</td>
		</tr>
		</table>
		<input type="hidden" name="class_id" value="<?php echo $gval[4];?>" />
		<input type="hidden" name="booking_id" value="<?php echo $gval[3];?>" />
		<input type="hidden" name="group" value="<?php echo $_GET['group'];?>" />
		<input type="hidden" name="stage" value="<?php echo $_GET['stage'];?>" />
		<input type="hidden" name="return" value="<?php echo $_GET['r'];?>" />
	</form> 
<?php
} ?>