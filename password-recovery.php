<?php

if (isset($_POST['recoverPassword']) == "Recover Password") {
	
	include_once '_dbaccessmaster.php';
	
    $sql = "SELECT email, db FROM users WHERE username = '".mysql_real_escape_string($_POST['username'])."' LIMIT 1; ";
    //echo $sql."<br />";
    $res = mysql_query($sql);
    $row = mysql_fetch_array($res);
    
    if ($row['email'] != "") {
		
		// Now we need to know which frnachise we are dealing with...
		$sqlcrm = "SELECT url_crm FROM `franchise_master`.`franchises` WHERE db = '".$row['db']."' LIMIT 1";
		$rescrm = mysql_query($sqlcrm);
		$rowcrm = mysql_fetch_array($rescrm);
		
        
        $recoveryToken = base64_encode($username.date("Y-m-d H:i:s"));
        
        $sqli = "INSERT INTO `".$row['db']."`.`password_recovery` (`id`, `username`, `emailaddress`, `token`, `datecreated`) VALUES
                (NULL, '".mysql_real_escape_string($_POST['username'])."', '".mysql_real_escape_string($row['email'])."', '".mysql_real_escape_string($recoveryToken)."', '".date("Y-m-d H:i:s")."');";
		//echo $sqli;
        mysql_query($sqli);
        
        $emailSubject = $companyName." CRM Password Recovery";
        
        $message = "<p>We have received your request to recover your password.  Please click the link below to change your password.</p>";
        $message .= "<a href='".$rowcrm['url_crm']."password-change.php?token=".$recoveryToken."&reqid=".base64_encode($row['db'])."'>Recover Password</a>";
        $message .= "<p>If you are still unable to recover your password, please email <a href='mailto:patrick@mediatomcat.net'>patrick@mediatomcat.net</a></p>";
        
       // echo $message;
        
        // Lets send a message notification
        require dirname(__FILE__).'/includes/phpmailer/PHPMailerAutoload.php';
    
        $mail             = new PHPMailer();
        $mail->IsSMTP(); 							// telling the class to use SMTP
        $mail->SMTPDebug  = 0;                     	// enables SMTP debug information (for testing)
													// 1 = errors and messages
													// 2 = messages only
        $mail->SMTPAuth   = true;                  	// enable SMTP authentication
		$mail->SMTPSecure = "tls";
		
		$mail->Host       = $email_host; 			// sets the SMTP server
		$mail->Port       = $email_port;            // set the SMTP port for the GMAIL server
		$mail->IsHTML(true);
		$mail->Username   = $email_username; 		// SMTP account username
		$mail->Password   = $email_password;        // SMTP account password
        
		
		
		$mail->addAddress($row['email']);
        $mail->Subject = $emailSubject;
        $mail->setFrom('no-reply@playballkids.com');
        $mail->addReplyTo('no-reply@playballkids.com');
        $mail->Body = $message;
    
        if(!$mail->Send()) {
           echo "Mailer Error: " . $mail->ErrorInfo;
        } else {
           echo "Message has been sent";
        }
        header("Location: password-recovery.php?message=success"); 
    } else {
        $errorMessage = "Username does not exist, please try again.";
    }

}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "includes/xhtml1-transitional.dtd">
<html>
<head>
	<title>Password Recovery</title>
	<style type="text/css" media="all">@import "css/style.css";</style>
	<style type="text/css" media="all">@import "css/forms.css";</style>
	<link rel="shortcut icon" href="/favicon-thinline.ico" />
</head>
<body>	
	<table cellspacing="0" cellpadding="0" border="0" width="97%">
	<tr>
		<td><a href="/"><img style="padding:10px;" src="/images/playball_logo.png" alt="" / align="center" border="0"></a></td>
		<td align="right"><a href="login.php">Login</a></td>
	</tr>
	</table>
<br />
<fieldset style="width: 300px;">
<legend>Recover Password</legend>
<form action="<?php echo $_SERVER['PHP_SELF'];?>" id="f2" name="f2" method="post" class="form-large">
    <div>
        <label for="name">Enter your Username</label>
        <input type="text" name="username" size="25" tabindex="1" id="name" class="form-text" value="" />
    </div>
    <div class="fieldset-footer">&nbsp;<input type="submit" value="Recover Password" name="recoverPassword" class="button1" tabindex="4" />
</form>
</fieldset>
    <a a href="login.php">Login</a>
    <?php 
    if (isset($_GET['message']) == "success") {
        echo '<div style="color:green" ><br>An email has been sent for you to be able reset your password.</div>';
    } ?>
</div>
</body>
</html>