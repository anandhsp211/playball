<!DOCTYPE html>
<html>
    <head>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDwYgGY1vNAzuzp6K5K5knvwP3rGlMVuLQ&sensor=false" type="text/javascript"></script>
        <style type="text/css">
          html, body {height: 100%; margin: 0; font-family:Arial;font-size:14px; }
          td {font-family:Arial;font-size:14px; background: #f4f4f4;padding:10px;}
          input {font-family:Arial;font-size:14px;}
        </style>
    </head>
<body>
<br><br><br><br>
<center>
<form class="center" name="lat" action="<?php echo $_SERVER['PHP_SELF'];?>" method="POST">
<table cellpadding=5 border=0 width="60%">
     <tr>
        <td colspan=2 style="font-weight:bold;font-size:18px;">Latitude &amp; Longitude Generator</td>
     </tr>
    <tr>
        <td colspan=2>
        <label for="postcode">Postcode</label>
        <input id="postcode" style="padding:3px;width:300px;" name="postcode" type="text" value="<?php echo $_REQUEST['postcode']; ?>" />
        <input style="padding:3px;" name="submit" type="submit" value="Generate" /><br><br>
        
    </td>

<?php
function getLatLongFromPostcode($postcode) {
    $postcode = urlencode(trim($postcode));
    $file = "https://maps.googleapis.com/maps/api/geocode/json?address=".$postcode."&;key=AIzaSyCFu4F84EiO3LYc11EV_WRgiWFAbUiHGkw&sensor=false";
    $contents = file_get_contents($file);
    //echo $contents;
    $obj = json_decode($contents, true);
    
    echo "<tr><td>Lat:</td><td><input onclick='this.select();' type='text' value='".$obj['results'][0]['geometry']['location']['lat']."' /></td></tr>";
    echo "<tr><td>Lng:</td><td><input onclick='this.select();' type='text' value='".$obj['results'][0]['geometry']['location']['lng']."' /></td></tr>";
}

if (isset($_REQUEST['postcode'])) {
    getLatLongFromPostcode($_REQUEST['postcode']);
}

?>
    </tr>
</table>
     </form>
</center>
</body>
</html>