<style>
    .saletables td {
        font-family:Tahoma;
        font-size:11px;
        text-align:center;
        
        padding:3px;
    }
    .saletables td.cold { background:#ff0000;color:white; }
    .saletables td.pendingreview { background:#ff0000;color:white; }
    .saletables td.lead { background:#C0C0C0;color:black; }
    .saletables td.opportunity { background:#f2520d;color:white; }
    .saletables td.qualification { background:#ffcc00;}
    .saletables td.quotesent { background:#ff9900;color:black;}
    .saletables td.postquotenegotiation { background:#b9dd3c;}
    .saletables td.awaitingcustomerfeedback { background:#829f53;}
    .saletables td.won { background:#2c5700; color:white;}
    .saletables td.lost { background:red; color:white;}
</style>
<?php
$sqlstr = "SELECT datechanged, to_value FROM  status_change_log WHERE location = '".mysql_escape_string($gval[21])."'
            AND parent_hashid = '".mysql_escape_string($gval[1])."' ORDER BY datechanged ASC";
//echo $sqlstr;
$res = mysql_query($sqlstr);
$num_rows = mysql_num_rows($res);
$tdwidth = 100 / $num_rows;
?>
<table class="saletables" width="100%" border="0">
    <tr>
    <?php
    $res = mysql_query($sqlstr);
    while ($row = mysql_fetch_array($res)) { ?>
        <td class="<?php echo str_replace(" ", "", (strtolower($row['to_value']))); ?>" width="<?php echo number_format($tdwidth,0) ?>%"><?php echo $row['to_value'] ?></td>
    <?php
    }  ?>
    </tr><tr>
    <?php
    $res = mysql_query($sqlstr);
    while ($row = mysql_fetch_array($res)) { ?>
        <td class="<?php echo str_replace(" ", "", (strtolower($row['to_value']))); ?>" width="<?php echo number_format($tdwidth,0) ?>%"><?php echo date("d M Y", strtotime($row['datechanged'])) ?></td>
    <?php
    }  ?>
    </tr>
</table>
<br>