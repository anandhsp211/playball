<?php
include_once("_globalconnect.php");
// Can an include from globalconnect and manage their 
// permissions for this zone.  Because its a popup,
// we just close it down again if they dont have permission
// for that zone.

if ($_POST['action'] != "Attach File") {
	// Lets run a click counter to handle the modal windows
	if ($_SESSION['clickcounter'] == NULL) { $_SESSION['clickcounter'] = 1; }
	$_SESSION['clickcounter'] = ($_SESSION['clickcounter'] + 1);
	$clickcount = $_SESSION['clickcounter'];
}

if ($_POST['messagetypesubmit'] == "Text") {
	
	$mobilenumber = $_POST['messageto'];
	
	$textmessage = $_POST['messagebody'];
	$textmessage = str_replace("\r"," ",$textmessage);
	$textmessage = str_replace("\n"," ",$textmessage);
	$textmessage = str_replace(" ","+",$textmessage);
	$textmessage = str_replace("++++++","+",$textmessage);
	$textmessage = str_replace("+++++","+",$textmessage);
	$textmessage = str_replace("++++","+",$textmessage);
	$textmessage = str_replace("+++","+",$textmessage);
	$textmessage = str_replace("++","+",$textmessage);
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "http://api.clickatell.com/http/sendmsg?api_id=".CTAPIID."&user=".CTUSER."&password=".CTPASS."&to=".$mobilenumber."&text=".$textmessage);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$definition = curl_exec($ch);
	curl_close($ch);
	
	//Let's store some view and popularity detail.
	$query = "INSERT INTO messaging (messagetype, registrantid, menutabid, userid, location, recordid, senttoaddress, messagebody, datecreated) VALUES (
		'text',
		".RID.",
		'".$gval[5]."',
		".UID.",
		'".$gval[2]."',
		'".$gval[1]."',
		'".$mobilenumber."',
		'".$_POST['messagebody']."',
		'".$sGMTMySqlString."'
		)";
	//echo $query;
	mysql_query($query);
		
	$query = "SELECT MAX(id) AS HashID FROM activities";
	$result = mysql_query($query);
	$row = mysql_fetch_array($result);
	
	$query = "INSERT INTO text_msg_counter (userid, registrantid, department, datecreated) VALUES (
			  '".UID."',
			  '".RID."',
			  '".$_SESSION['securityarrdept'][0]."',
			  '".$sGMTMySqlString."'
			  )";
	//echo $query;
	mysql_query($query) Or Die ("Cannot submit entry!");
	 
	 $onload = 'onload="window.parent.location = window.parent.location;self.close();return false;"';

} elseif ($_POST['messagetypesubmit'] == "Email") {

	$_SESSION['messageset'] = $_POST['messageset'];
	include_once("messaging_mailsend.php");
	
	//Let's store some view and popularity detail.
	$query = "INSERT INTO messaging (messagetype, registrantid, menutabid, userid, location, recordid, senttoaddress, cctoaddress, bcctoaddress, emailsubject, messagebody, datecreated) VALUES (
		'email',
		".RID.",
		'".$gval[5]."',
		".UID.",
		'".$gval[2]."',
		'".$gval[1]."',
		'".$_POST['messageto']."',
		'".$_POST['messagecc']."',
		'".$_POST['messagebcc']."',
		'".$_POST['subject']."',
		'".$_POST['messagebody']."',
		'".$sGMTMySqlString."'
		)";
	mysql_query($query);
	
	$onload = 'onload="window.parent.location = window.parent.location;self.close();return false;"';
	
}

if ($gval[5] == 1) {
	
	// Lets delete the attachment
	$queryfile = "SELECT id, filename FROM messaging_files WHERE sessionid ='".session_id()."' AND userid = ".UID;
	$resultfile = mysql_query($queryfile);
	while ($rowfile = mysql_fetch_array($resultfile)) {
		if ($gval[6] == strtoupper(md5($rowfile['id']))) {
			$fnd = 1;
			break;
		}
	}
	
	if ($fnd == 1) {
		unlink($filepath.$rowfile['filename']);
		# Delete all the database entries as well
		$sql = "DELETE FROM messaging_files WHERE id =".$rowfile['id'];
		mysql_query($sql);
	}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "includes/xhtml1-transitional.dtd">
<html>
<head>
	<title>Thin Line Software - Messaging</title>
	<?php 
	if (ERES > 1024) { ?>
		<style type="text/css" media="all">@import "css/style.css";</style>
	<?php 
	} else { ?>
		<style type="text/css" media="all">@import "css/style_small.css";</style>
	<?php
	}?>
	<?php 
	if ($gval[3] == 2) { ?>
	<script language="javascript" type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
	<script language="javascript" type="text/javascript">
	tinyMCE.init({
		mode : "textareas",
		theme : "advanced",
		theme_advanced_fonts : "Arial=arial,helvetica,sans-serif;Courier New=courier new,courier,monospace",
		content_css : "css/mycontent.css",
		plugins : "spellchecker",
		theme_advanced_buttons3_add : "spellchecker",
		spellchecker_languages : "+English=en,Swedish=sv,Norwegian=no,German=de,Spanish=es,French=fr"
	});
	</script>
	<?php
	} elseif ($gval[3] == 1) { ?>
		<SCRIPT LANGUAGE="JavaScript">
		<!-- Begin
		function textCounter(field,cntfield,maxlimit) {
			if (field.value.length > maxlimit) // if too long...trim it!
				field.value = field.value.substring(0, maxlimit);
				// otherwise, update 'characters left' counter
			else
				cntfield.value = maxlimit - field.value.length;
			}
			//  End -->
		</script>
<?php
	}
	?>
</head>
<body <?php echo $onload ?>>
	<form enctype="multipart/form-data" action="messaging.php?id=<?php echo $gval[0] ?>:<?php echo $gval[1] ?>:<?php echo $gval[2] ?>:<?php echo $gval[3] ?>:<?php echo $gval[4] ?>:<?php echo $gval[5] ?>:<?php echo $clickcount; ?>" name="messageform" method="POST">
	<table cellspacing="0" cellpadding="0" border="0" width="97%">
	<tr>
		<td><img src="images/<?php echo $_SESSION['franchisedata']['logo'];?>" align="center" border="0"></td>
		<td align="right"></td>
	</tr>
	</table><br>
	<table cellspacing="0" cellpadding="5" border="0" width="100%">
	<tr>
		<td valign="top" align="right" rowspan="2" width="5%">
		<?php 
		if ($gval[3] == 1) {
			$mainicon = "mobilephone1_32.png";
		} else {
			$mainicon = "mail_new.png";
		}?>
			<img src="images/icons/<?php echo $mainicon; ?>"><img src="images/spacer.gif" height="1" width="10">
		</td>
		<td valign="top" height="5">
		<?php 
		
		
		$sql = "SELECT ".$gval[4]." FROM udf_".$gval[2]." WHERE hashid = '".$gval[1]."' AND registrantid = ".RID;
		// echo $sql;
		$res = mysql_query($sql);
		$row = mysql_fetch_array($res);
		
		if ($gval[3] == 1) {
			$datavalue = $row[$gval[4]];
			$pagemessage = "<br>Mobile numbers must be in the following format: +447123456789<br>[plus sign][countrycode][mobile number without preceeding zero]";
			$addcounter = 'onKeyDown="textCounter(document.messageform.messagebody,document.messageform.remLen1,160)" onKeyUp="textCounter(document.messageform.messagebody,document.messageform.remLen1,160)"';
			$addcounterbox = ' ';
			$messagename = "Text"; 
			$msgalign = "left"; ?>
			<table border="0" cellpadding="4" cellspacing="0" width="99%">
			<tr>
				<td class="ls_top" width="25%"><strong>Text Message</strong>:</td>
				<td class="ls_top"><input type="text" name="messageto" style="width:250px;font-family:Arial;font-size:14px" value="<?php echo str_replace(" ","",$datavalue); ?>"></td>
			</tr>
			</table>
			<table cellspacing="0" cellpadding="5" border="0" width="100%">
			<tr>
				<td><font style="font-family:Tahoma;font-size:11px"><?php echo $pagemessage; ?></font></td>
				<td colspan="2" align="right"><font class="note">Select a Template:</font>
				<script language="javascript" type="text/javascript" src="js/template_update_text.js"></script>
					<select style="font-family:tahoma;font-size:11px" name="templatename" onchange="showHint(this.value);">
						<option value="">Select One</option>
					<?php 
					$query = "SELECT id, templatename FROM messaging_templates WHERE registrantid=".RID." AND messagetype='Text' AND isactive = 1 ORDER BY templatename ASC";
					$result = mysql_query($query);
					while ($row = mysql_fetch_array($result)) { ?>
						<option value="<?php echo $row['id'] ?>"><?php echo $row['templatename'] ?></option>
					<?php 
					} ?>	
					</select>
				</td>
			</tr>
			</table>
		<?php 
		} elseif ($gval[3] == 2) {
			$datavalue = $row[$gval[4]];
			$addcounterbox = '<strong>HTML</strong> <input type="radio" value="text/html" name="messageset" checked><strong>Plain Text</strong><input type="radio" value="text/plain" name="messageset">'; 
			$messagename = "Email";
			$msgalign = "left";
			if ($passfail == 1) {
				echo "There was an error uploading the file, please try again!";
			} elseif ($passfail == 2) {
				echo "Due to security restrictions we cannot allow files with the extension <strong>".$extension."</strong> to be uploaded.";
			} elseif ($passfail == 3) {
				echo "This file is too large to download.";
			} elseif ($passfail == 4) {
				echo "File already exists.  Please change the filename or delete the existing file below.";
			}
			// Lets see if we need to apply a template.
			if ($gval[4] != "") {
				$querygmsg = "SELECT * FROM messaging_templates WHERE registrantid = ".RID." AND id = ".$gval[4];
				//echo $querygmsg;
				$resultmsg = mysql_query($querygmsg);
				$rowmsg = mysql_fetch_array($resultmsg);
				
				$templatesubject = $rowmsg['subject'];
				$templatebody = str_replace("userid",$gval[2],$rowmsg['message_body']);
				$templatebody = str_replace("regid",RID,$templatebody);
			} else {
				$templatesubject = $_POST['subject'];
				$templatebody = $_POST['messagebody'];
			}
			?>
			<table border="0" cellpadding="4" cellspacing="2" width="99%">
			<tr>
				<td class="ls_on" colspan="1"><strong>Subject</strong>: </td>
				<td class="ls_on" colspan="4"><input type="text" name="subject" style="width:250px;font-family:Arial;font-size:14px;width:560px" value="<?php echo $templatesubject; ?>"></td>
			</tr>
			<tr>
				<td class="ls_on"><strong>TO</strong>: </td>
				<?php 
				if ($_POST['messageto'] != "") {
					$datavalue = $_POST['messageto'];
				}?>
				<td class="ls_on"><input type="text" name="messageto" style="width:250px;font-family:Arial;font-size:14px" value="<?php echo $datavalue; ?>"></td>
				<td class="ls_on"><strong>CC</strong>:</td>
				<td class="ls_on"><input type="text" name="messagecc" style="width:250px;font-family:Arial;font-size:14px" value="<?php echo $_POST['messagecc']; ?>"></td>
			</tr>
			<tr>
				<td class="ls_on"><strong>BCC</strong>: </td>
				<td class="ls_on"><input type="text" name="messagebcc" style="width:250px;font-family:Arial;font-size:14px" value="<?php echo $_POST['messagebcc']; ?>"></td>
				<td colspan="2" class="ls_on"><font style="font-family:Arial;font-size:14px"><?php echo $addcounterbox; ?></font></td>
			</tr>
			<tr>
				<td colspan="2"><font class="note">Please use a semi-colon ( <strong>;</strong> ) to separate email addresses.</font></td>
				<td colspan="2" align="right"><font class="note">Select a Template:</font>
				<script language="javascript" type="text/javascript" src="js/template_update_email.js"></script>
					<select style="font-family:tahoma;font-size:11px" name="templatename" onchange="showHint(this.value);">
						<option value="">Select One</option>
					<?php 
					$query = "SELECT id, templatename FROM messaging_templates WHERE registrantid=".RID." AND messagetype='Email' AND isactive = 1 ORDER BY templatename ASC";
					$result = mysql_query($query);
					while ($row = mysql_fetch_array($result)) { ?>
						<option value="<?php echo $row['id'] ?>"><?php echo $row['templatename'] ?></option>
					<?php 
					} ?>	
					</select>
				</td>
			</tr>
			</table>
		<?php
		}
		?>
		</td>
		<td rowspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td valign="top" height="100%">
			<textarea style="font-family:Arial;font-size:14px;width:100%;height:200px" name="messagebody" <?php echo $addcounter; ?>><?php echo $templatebody; ?></textarea><br>
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td align="<?php echo $msgalign ?>">
				<?php 
				if ($gval[3] == 2) { ?>
					<input type="submit" name="action" value="Send <?php echo $messagename; ?>" style="font-family:Arial;font-size:14px;width:150px;height:37px"/>
				<?php 
				} else { ?>
					<?php 
					if ($_SESSION['msgremaining'] < 1) { ?>
						<font color="red">You have no more text messages available, please go purchase additional text message credit.</font>
					<?php 
					} else { ?>
					<br /><input type="submit" name="action" value="Send <?php echo $messagename; ?>" style="font-family:Arial;font-size:14px;width:150px;height:37px"/>
				<?php 
					}
				} ?>
					<input type="hidden" name="messagetypesubmit" value="<?php echo $messagename; ?>">
				</td>
				<td align="right">
				<?php 
				if ($gval[3] == 1) { ?>
				<table border="0" cellspacing="2" cellpadding="2" width="250">
				<tr>
					<td align="right">
						Characters Left: <input readonly type="text" name="remLen1" size="3" maxlength="3" value="160" style="font-family:Arial;font-size:14px">
					</td>
				</tr>
				</table>
				<?php 
				} elseif ($gval[3] == 2) {?>
				<table border="0" cellspacing="2" cellpadding="2" width="98%">
				<tr>
					<td width="6%">&nbsp;</td>
					<td>
						&nbsp;<!--Something could go here.-->
					</td>
				</tr>
				</table>
				<?php 
				}?>
				</td>
			</tr>
			</table>
			</form>
		</td>
	</tr>
	</table>
</body>
</html>
