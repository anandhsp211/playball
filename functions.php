<?php 

// to convert a date from a str to a num
function convertDate($conmonth) {	
  	if ($conmonth == "Jan") { $conmonth = '01';
	} elseif ($conmonth == "Feb") { $conmonth = '02'; 
	} elseif ($conmonth == "Mar") { $conmonth = '03'; 
	} elseif ($conmonth == "Apr") { $conmonth = '04'; 
	} elseif ($conmonth == "May") { $conmonth = '05'; 
	} elseif ($conmonth == "Jun") { $conmonth = '06'; 
	} elseif ($conmonth == "Jul") { $conmonth = '07'; 
	} elseif ($conmonth == "Aug") { $conmonth = '08'; 
	} elseif ($conmonth == "Sep") { $conmonth = '09'; 
	} elseif ($conmonth == "Oct") { $conmonth = '10'; 
	} elseif ($conmonth == "Nov") { $conmonth = '11'; 
	} elseif ($conmonth == "Dec") { $conmonth = '12'; 
	} elseif ($conmonth == "01") { $conmonth = 'Jan';
	} elseif ($conmonth == "02") { $conmonth = 'Feb'; 
	} elseif ($conmonth == "03") { $conmonth = 'Mar'; 
	} elseif ($conmonth == "04") { $conmonth = 'Apr'; 
	} elseif ($conmonth == "05") { $conmonth = 'May'; 
	} elseif ($conmonth == "06") { $conmonth = 'Jun'; 
	} elseif ($conmonth == "07") { $conmonth = 'Jul'; 
	} elseif ($conmonth == "08") { $conmonth = 'Aug'; 
	} elseif ($conmonth == "09") { $conmonth = 'Sep'; 
	} elseif ($conmonth == "10") { $conmonth = 'Oct'; 
	} elseif ($conmonth == "11") { $conmonth = 'Nov'; 
	} elseif ($conmonth == "12") { $conmonth = 'Dec'; 
	}
	return $conmonth;
}

function timetosec($value) {
	if ($value == "5 minutes") {
		$remindertime = 300;
	} elseif ($value == "10 minutes") {
		$remindertime = 600;
	} elseif ($value == "30 minutes") {
		$remindertime = 1800;
	} elseif ($value == "1 hour") {
		$remindertime = 3600;
	} elseif ($value == "2 hours") {
		$remindertime = 7200;
	} elseif ($value == "4 hours") {
		$remindertime = 14400;
	} elseif ($value == "8 hours") {
		$remindertime = 28800;
	} elseif ($value == "1 day") {
		$remindertime = 86400;
	} elseif ($value == "2 days") {
		$remindertime = 172800;
	} elseif ($value == "1 week") {
		$remindertime = 432000;
	} elseif ($value == "2 weeks") {
		$remindertime = 864000;
	}
	return $remindertime;
} 

function sectotime($value) {
	if ($value == 300) {
		$selected_time = "5 minutes";
	} elseif ($value == 600) {
		$selected_time = "10 minutes";
	} elseif ($value == 1800) {
		$selected_time = "30 minutes";
	} elseif ($value == 3600) {
		$selected_time = "1 hour";
	} elseif ($value == 7200) {
		$selected_time = "2 hours";
	} elseif ($value == 14400) {
		$selected_time = "4 hours";
	} elseif ($value == 28800) {
		$selected_time = "8 hours";
	} elseif ($value == 86400) {
		$selected_time = "1 day";
	} elseif ($value == 172800) {
		$selected_time = "2 days";
	} elseif ($value == 432000) {
		$selected_time = "1 week";
	} elseif ($value == 864000) {
		$selected_time = "2 weeks";
	}
	return $selected_time;
} 

// Set the Hashid for a particular entry item.... And do ordering
function sethashidandOrder($sethtablename) {
	$sqlsethashid = "SELECT MAX(id) AS HashID FROM ".$sethtablename."";
	$ressethashid = mysql_query($sqlsethashid);
	$rowsethashid = mysql_fetch_array($ressethashid)  Or Die ("Cannot set ID, please notify systems administrator with this code: SetHID: 1");
	$entryid = $rowsethashid['HashID'];
	
	$querysethashid = "UPDATE ".$sethtablename." SET hashid = '".strtoupper(md5($rowsethashid['HashID']))."' WHERE id = ".$rowsethashid['HashID'];
	mysql_query($querysethashid) Or Die ("Cannot set ID, please notify systems administrator with this code: SetHID: 2");
	
	$sqlsethashid = "SELECT MAX(sortorder) AS HashID FROM ".$sethtablename." WHERE registrantid =".RID;
	$ressethashid = mysql_query($sqlsethashid);
	$rowsethashid = mysql_fetch_array($ressethashid);
	
	// If it is their first tab, lets add a 1 value
	if ($rowsethashid['HashID']) {
		$querysethashid = "UPDATE ".$sethtablename." SET sortorder = ".($rowsethashid['HashID']+1)." WHERE id = ".$entryid." AND registrantid =".RID;
		mysql_query($querysethashid);
	} else {
		$querysethashid = "UPDATE ".$sethtablename." SET sortorder = 1 WHERE id = ".$entryid." AND registrantid =".RID;
		mysql_query($querysethashid);
	}
	
	return strtoupper(md5($entryid));
}

// Set the Hashid for a particular entry item....
// if tblname is set to new, then create table
function onlysetHashID($sethtablename,$tblname) {
	$sqlsethashid = "SELECT MAX(id) AS HashID FROM ".$sethtablename."";
	//echo $sqlsethashid."<br>";
	$ressethashid = mysql_query($sqlsethashid);
	$rowsethashid = mysql_fetch_array($ressethashid)  Or Die ("Cannot set ID, please notify systems administrator with this code: SetHID: 1");
	
	$querysethashid = "UPDATE ".$sethtablename." SET hashid = '".strtoupper(md5($rowsethashid['HashID']))."' WHERE id = ".$rowsethashid['HashID'];
	//echo $querysethashid;
	mysql_query($querysethashid) Or Die ("Cannot set ID, please notify systems administrator with this code: SetHID: 2");
	
	if ($tblname == "new") {
		$newtablename = "udf_".strtoupper(md5($rowsethashid['HashID']));
		$sql = "CREATE TABLE ".$newtablename." (`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
				`hashid` VARCHAR( 350 ) NOT NULL ,
				`recordid` INT NOT NULL ,
				`registrantid` VARCHAR( 350 ) NOT NULL ,
				`department` VARCHAR( 150 ) NULL DEFAULT NULL ,
				`createdby` INT NOT NULL ,
				`createddate` DATETIME NOT NULL ,
				`lastupdatedby` INT NULL ,
				`lastupdateddate` DATETIME NULL,
				`issaved` TINYINT NULL,
				`deleteddate` DATETIME NULL,
				`deletedby` TINYINT NULL,
				`tlsprivate` TINYINT NOT NULL DEFAULT  '0'
			) ENGINE = MYISAM ;";
		mysql_query($sql) or die ("Dead") ;
	}
	return strtoupper(md5($rowsethashid['HashID']));
}

function createSubTabs($arr,$sGMTMySqlString,$multi_1,$multi_2,$numberseries_1,$numberseries_2,$multi_singular_1,$multi_singular_2) {
	// Use this if there is more than one category
	$sqlsethashid = "SELECT MAX(id) AS HashID FROM menu_tabs";
	$ressethashid = mysql_query($sqlsethashid);
	$rowsethashid = mysql_fetch_array($ressethashid)  Or Die ("Cannot set ID, please notify systems administrator with this code: SetHID: 1");
	$sortcounter = 1;
	if ($multi_1 != "") {
		for ($number = 0; $number <= 1; $number++) {
			foreach ($arr as &$value) {
				if ($number == 0) {
					if ($value == "new") {
						$numberseries2 = $numberseries_1;
						$newval = $value." ".$multi_singular_1;
					} else {
						$newval = $value." ".$multi_1;
					}
				} else {
					if ($value == "new") {
						$numberseries2 = $numberseries_2;
						$newval = $value." ".$multi_singular_2;
					} else {
						$newval = $value." ".$multi_2;
					}
				}
	   	 		$query = "INSERT INTO menusub_tabs (registrantid, menu_tabhashid, type, multitierlink, label, label_singular, sortorder, numberseries, isactive, datecreated) VALUES (
					".RID.",
					'".strtoupper(md5($rowsethashid['HashID']))."',
	  				'".$value."',
					".$number.",
					'".strtolower($newval)."',
					'".strtolower($newval)."',
	  				".$sortcounter.",
					'".$numberseries2."',
					0,
					'".$sGMTMySqlString."'
	  				)";
				//echo $query;
				mysql_query($query) Or Die ("Cannot submit entry! 2");
				
				// Set Hashid - pass table name
				onlysetHashID("menusub_tabs",$value);
				$sortcounter++;
				unset($numberseries2);
			}
		}
	} else {
		// Use this if there is only one category
		foreach ($arr as &$value) {
			if ($value == "new") {
				$numberseries2 = $numberseries_1;
			} 
	   	 	$query = "INSERT INTO menusub_tabs (registrantid, menu_tabhashid, type, label, sortorder, numberseries, isactive, datecreated) VALUES (
				".RID.",
				'".strtoupper(md5($rowsethashid['HashID']))."',
	  			'".$value."',
				'".strtolower($value)."',
	  			".$sortcounter.",
				'".$numberseries2."',
				0,
				'".$sGMTMySqlString."'
	  			)";
			mysql_query($query) Or Die ("Cannot submit entry! 1");
				
			// Set Hashid - pass table name
			onlysetHashID("menusub_tabs",$value);
			$sortcounter++;
			unset($numberseries2);
		}
	}
	unset($value);
}

// Check if value is an email address
function check_email_address($email) {
    // First, we check that there's one @ symbol, and that the lengths are right
    if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $email)) {
        // Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
        return false;
    }
    // Split it into sections to make life easier
    $email_array = explode("@", $email);
    $local_array = explode(".", $email_array[0]);
    for ($i = 0; $i < sizeof($local_array); $i++) {
         if (!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i])) {
            return false;
        }
    }    
    if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1])) { // Check if domain is IP. If not, it should be valid domain name
        $domain_array = explode(".", $email_array[1]);
        if (sizeof($domain_array) < 2) {
                return false; // Not enough parts to domain
        }
        for ($i = 0; $i < sizeof($domain_array); $i++) {
            if (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i])) {
                return false;
            }
        }
    }
    return true;
}

// Generating column names from labels
function columnnames($label) {
	$specials = array(" ",":",";","�","`","�","!","�","$","%","^","&","*","(",")","_","_","+","=","{","}","[","]","@","'","~","#","|","\\","<",",",">","?","/");
	$columns = strtolower(str_replace(' ',' ',str_replace($specials,'',$label)));
	return $columns;
}

// format data for insert and update 
function insertstructure($udfstring) {
	$udfexplode = explode(":",$udfstring);
	$udflocation = $_POST['udflocation'];
	
	// First we build the insert string
	$num_rows = count($udfexplode);
	$commacounter = 0;
	foreach ($udfexplode as &$udfvalue) {
		$udfexplode2 = explode("&",$udfvalue);
		
		if (($commacounter+1) != $num_rows) {
			$columnstr .= strtolower($udfexplode2[0]).",";
		} else {
			$columnstr .= strtolower($udfexplode2[0]);
		}
		$commacounter++;
	}
	return $columnstr;
}

// format data for insert and update 
function insertdata($udfstring,$udflocation) {
	$udfexplode = explode(":",$udfstring);
	
	// First we build the insert string
	$num_rows = count($udfexplode);
	$commacounter = 0;
	foreach ($udfexplode as &$udfvalue) {
		$udfexplode2 = explode("&",$udfvalue);
		
		// Lets ensure the fields being inserted are validated.
		if ($udfexplode2[1] == "DATE") {
			$datainsert = explode("-",substr($_POST[$udfexplode2[0]],0,11));
			$datainsert = $datainsert[2]."-".convertDate($datainsert[1])."-".$datainsert[0];
		} elseif ($udfexplode2[1] == "DATETIME") {
			$datainsert = explode("-",substr($_POST[$udfexplode2[0]],0,11));
			$datainserttime = substr($_POST[$udfexplode2[0]],-5,5);
			$datainsert = $datainsert[2]."-".convertDate($datainsert[1])."-".$datainsert[0];
			$datainsert = $datainsert." ".$datainserttime.":00";
		} else {
			$datainsert = $_POST[$udfexplode2[0]];
		}
		
		if (($commacounter+1) != $num_rows) {
			$columnvalue .= "'".str_replace("+","++",$datainsert)."',";
		} else {
			$columnvalue .= "'".str_replace("+","++",$datainsert)."'";
		}
		$commacounter++;
	}
	return $columnvalue;
}

function updatestructdata($udfstring) {
	$hiddenhashid = $_POST['hiddenhashid'];
	$udfexplode = explode(":",$udfstring);
	// First we build the insert string
	$num_rows = count($udfexplode);
	$commacounter = 0;
	foreach ($udfexplode as &$udfvalue) {
		$udfexplode2 = explode("&",$udfvalue);
		
		// Lets ensure the fields being inserted are validated.
		if ($udfexplode2[1] == "DATE") {
			$datainsert = explode("-",substr($_POST[$udfexplode2[0]],0,11));
			$datainsert = $datainsert[2]."-".convertDate($datainsert[1])."-".$datainsert[0];
		} elseif ($udfexplode2[1] == "DATETIME") {
			$datainsert = explode("-",substr($_POST[$udfexplode2[0]],0,11));
			$datainserttime = substr($_POST[$udfexplode2[0]],-5,5);
			$datainsert = $datainsert[2]."-".convertDate($datainsert[1])."-".$datainsert[0];
			$datainsert = $datainsert." ".$datainserttime.":00";
		} else {
			$datainsert = $_POST[$udfexplode2[0]];
		}
		
		$datainsert = str_replace("�", "", $datainsert);
		$datainsert = str_replace("�", "&pound;", $datainsert);
		//echo $udfexplode2[0];die();
		if ($udfexplode2[0] == "currency") {
			$datainsert = preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&$-]/s', '', $datainsert);
		}
		
		if (($commacounter+1) != $num_rows) {
			$columnstr .= strtolower($udfexplode2[0])." = '".addslashes($datainsert)."', " ;
		} else {
			$columnstr .= strtolower($udfexplode2[0])." = '".addslashes($datainsert)."' " ;
		}
		$commacounter++;
	}
	$columnstr .= "WHERE hashid = '".$hiddenhashid."' AND registrantid = ".RID." LIMIT 1";
	return $columnstr;
}

function sortGetVals($value) {
	$newvalue = str_replace("&","AaMPERSANdD",$value);
	$newvalue = str_replace("+","PLUSSIGN",$value);
	$newvalue = str_replace("#","HASHHHHHH",$value);
	return $newvalue;
} 

function returnGetnorm($value) {
	$newvalue = str_replace("AaMPERSANdD","&",$value);
	$newvalue = str_replace("PLUSSIGN","+",$value);
	$newvalue = str_replace("HASHHHHHH","#",$value);
	return $newvalue;
} 

function random_string($type,$len) {
	switch($type) {
		case 'alnum'	:
		case 'numeric'	:
		case 'nozero'	:
		case 'alpha'	:
		
		switch ($type) {
			case 'alnum'	:	$pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
				break;
			case 'numeric'	:	$pool = '0123456789';
				break;
			case 'nozero'	:	$pool = '123456789';
				break;
			case 'alpha'	:	$pool = 'bcdfghjklmnpqrstvwxyz';
				break;
		}
		$str = '';
		for ($i=0; $i < $len; $i++) {
			$str .= substr($pool, mt_rand(0, strlen($pool) -1), 1);
		}
		return $str;
		break;
		case 'unique' : return md5(uniqid(mt_rand()));
		break;
	}
}

// insert a value at a specific point in an array
function array_insert(&$array, $insert, $position = -1) {
     $position = ($position == -1) ? (count($array)) : $position ;
     if($position != (count($array))) {
          $ta = $array;
          for($i = $position; $i < (count($array)); $i++) {
               if(!isset($array[$i])) {
                    die(print_r($array, 1)."\r\nInvalid array: All keys must be numerical and in sequence.");
               }
               $tmp[$i+1] = $array[$i];
               unset($ta[$i]);
          }
          $ta[$position] = $insert;
          $array = $ta + $tmp;
          //print_r($array);
     } else {
          $array[$position] = $insert;          
     }
          
     ksort($array);
     return true;
}

function getColumnsettings($fieldname,$table) {
	
	// The sales database
	if (($table == "sales_1") && ($fieldname == "productname")) {
		$columnstruc = "varchar(350) NOT NULL";
	} elseif (($table == "sales_1") && ($fieldname == "productcode")) {
		$columnstruc = "varchar(350) NOT NULL";
	} elseif (($table == "sales_1") && ($fieldname == "salesprice")) {
		$columnstruc = "decimal(10,2) NOT NULL";
	} elseif (($table == "sales_1") && ($fieldname == "costprice")) {
		$columnstruc = "decimal(10,2) NOT NULL";
	} elseif (($table == "sales_1") && ($fieldname == "taxcode")) {
		$columnstruc = "decimal(10,2) NOT NULL";
	} elseif (($table == "sales_1") && ($fieldname == "accountcode")) {
		$columnstruc = "varchar(350) NULL";
	} elseif (($table == "sales_1") && ($fieldname == "discount")) {
		$columnstruc = "decimal(10,2) NULL";
	} elseif (($table == "sales_1") && ($fieldname == "quantity")) {
		$columnstruc = "int(11) NULL";
	} elseif (($table == "sales_1") && ($fieldname == "is_refunded")) {
		$columnstruc = "tinyint(4) NULL";
	} elseif (($table == "sales_1") && ($fieldname == "is_refunded_date")) {
		$columnstruc = "datetime NULL";
	} 
	
	// The purchase database
	if (($table == "purchases_1") && ($fieldname == "productname")) {
		$columnstruc = "varchar(350) NOT NULL";
	} elseif (($table == "purchases_1") && ($fieldname == "productcode")) {
		$columnstruc = "varchar(350) NOT NULL";
	} elseif (($table == "purchases_1") && ($fieldname == "salesprice")) {
		$columnstruc = "decimal(10,2) NOT NULL";
	} elseif (($table == "purchases_1") && ($fieldname == "costprice")) {
		$columnstruc = "decimal(10,2) NOT NULL";
	} elseif (($table == "purchases_1") && ($fieldname == "taxcode")) {
		$columnstruc = "decimal(10,2) NOT NULL";
	} elseif (($table == "purchases_1") && ($fieldname == "accountcode")) {
		$columnstruc = "varchar(350) NULL";
	} elseif (($table == "purchases_1") && ($fieldname == "discount")) {
		$columnstruc = "decimal(10,2) NULL";
	} elseif (($table == "purchases_1") && ($fieldname == "quantity")) {
		$columnstruc = "int(11) NULL";
	} elseif (($table == "purchases_1") && ($fieldname == "received")) {
		$columnstruc = "int(11) NULL";
	} 
	
	// The address database
	if (($table == "address_info") && ($fieldname == "addressline1")) {
		$columnstruc = "text NULL";
	} elseif (($table == "address_info") && ($fieldname == "addressline2")) {
		$columnstruc = "varchar(350) NOT NULL";
	} elseif (($table == "address_info") && ($fieldname == "city")) {
		$columnstruc = "varchar(150) NULL";
	} elseif (($table == "address_info") && ($fieldname == "state")) {
		$columnstruc = "varchar(150) NULL";
	} elseif (($table == "address_info") && ($fieldname == "postcode")) {
		$columnstruc = "varchar(150) NULL";
	} elseif (($table == "address_info") && ($fieldname == "country")) {
		$columnstruc = "varchar(150) NULL";
	} elseif (($table == "address_info") && ($fieldname == "addresstype")) {
		$columnstruc = "varchar(150) NULL";
	} 
	
	// The notes database
	if (($table == "messaging ") && ($fieldname == "messagetype")) {
		$columnstruc = "varchar(150) NULL";
	} elseif (($table == "messaging ") && ($fieldname == "senttoaddress")) {
		$columnstruc = "varchar(250) NULL";
	} elseif (($table == "messaging ") && ($fieldname == "cctoaddress")) {
		$columnstruc = "varchar(250) NULL";
	} elseif (($table == "messaging ") && ($fieldname == "bcctoaddress")) {
		$columnstruc = "varchar(250) NULL";
	} elseif (($table == "messaging ") && ($fieldname == "emailsubject")) {
		$columnstruc = "varchar(250) NULL";
	} elseif (($table == "messaging ") && ($fieldname == "messagebody")) {
		$columnstruc = "text NULL";
	} 
	
	// The notes database
	if (($table == "notes ") && ($fieldname == "note")) {
		$columnstruc = "text NULL";
	} elseif (($table == "notes") && ($fieldname == "userid")) {
		$columnstruc = "int(11) NULL";
	} elseif (($table == "notes") && ($fieldname == "datecreated")) {
		$columnstruc = "datetime NULL";
	} 
	
	// The activities database
	if (($table == "activities ") && ($fieldname == "subject")) {
		$columnstruc = "varchar(250) NULL";
	} elseif (($table == "activities") && ($fieldname == "userid")) {
		$columnstruc = "int(11) NULL";
	} elseif (($table == "activities") && ($fieldname == "datestart")) {
		$columnstruc = "datetime NULL";
	} elseif (($table == "activities") && ($fieldname == "dateend")) {
		$columnstruc = "datetime NULL";
	} elseif (($table == "activities") && ($fieldname == "activitytype")) {
		$columnstruc = "varchar(250) NULL";
	} elseif (($table == "activities") && ($fieldname == "activitystatus")) {
		$columnstruc = "varchar(250) NULL";
	} elseif (($table == "activities") && ($fieldname == "remindertime")) {
		$columnstruc = "varchar(350) NULL";
	} elseif (($table == "activities") && ($fieldname == "remindersnooze")) {
		$columnstruc = "varchar(150) NULL";
	} elseif (($table == "activities") && ($fieldname == "snoozestartpoint")) {
		$columnstruc = "datetime NULL";
	} elseif (($table == "activities") && ($fieldname == "snoozealive")) {
		$columnstruc = "int(11) NULL";
	} elseif (($table == "activities") && ($fieldname == "reminderdisabled")) {
		$columnstruc = "int(11) NULL";
	} elseif (($table == "activities") && ($fieldname == "showtimeas")) {
		$columnstruc = "varchar(150) NULL";
	} elseif (($table == "activities") && ($fieldname == "label")) {
		$columnstruc = "varchar(250) NULL";
	} elseif (($table == "activities") && ($fieldname == "allocatedto")) {
		$columnstruc = "varchar(250) NULL";
	} elseif (($table == "activities") && ($fieldname == "privacy")) {
		$columnstruc = "varchar(100) NULL";
	} elseif (($table == "activities") && ($fieldname == "notes")) {
		$columnstruc = "text NULL";
	} 
	return $columnstruc;
}

?>