<?php
require(dirname(__FILE__) . '/class.damerauLevenshtein.php');

$dl = new DamerauLevenshtein('dk gry', 'dark grey',  1,6,6,1);
echo "Edit distance: "; 
var_dump($dl->getSimilarity());

echo "Similarity, relative: "; 
var_dump($dl->getRelativeDistance());

echo "Maximum possible edit distance: "; 
var_dump($dl->getMaximalDistance());

echo "Matrix: \n"; 
echo $dl->displayMatrix() . "\n";

