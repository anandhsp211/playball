<?php
include '_dbaccess.php';
require(dirname(__FILE__) . '/class.damerauLevenshtein.php');

// Get everything in that country and state
$sql =  "SELECT * FROM venues
            WHERE country = '".mysql_real_escape_string($_POST['country'])."'
            AND state = '".mysql_real_escape_string($_POST['state'])."'
            AND town LIKE '%".mysql_real_escape_string($_POST['town'])."%' ";
//echo $sql."<br />";
$res = mysql_query($sql);
$results = array();
$counter = 0;
while ($row = mysql_fetch_array($res)) {
    $results[$counter]['id'] = $row['id'];
    $results[$counter]['venue'] = $row['venue'];
    $results[$counter]['town'] = $row['town'];
    $results[$counter]['state'] = $row['state'];
    $results[$counter]['country'] = $row['country'];
    $results[$counter]['levscore'] = -1;
    $counter++;
}

$input = $_POST['venue'];
$shortest = -1;

$counter = 0;
foreach ($results as $words) {

    $word = $words['venue'];
    
    $dl = new DamerauLevenshtein($input, $word, 1,6,6,1);
    $lev = $dl->getSimilarity();
    
    // Drop any array item with a lev score greater than 36.
    if ($lev >= 36) {
        unset($results[$counter]);
    } else {
        // Add lev score to the array
        $results[$counter]['levscore'] = $lev;
    }
    
    $counter++;
    
}

// Count the number of array entries
$numSimilarVenues = count($results);
// Reset the index
$results = array_values($results);
// Sort by lev score
usort($results, function($a, $b) {
    return $a['levscore'] - $b['levscore'];
});

function getLatLongFromPostcode($address) {
    $address = urlencode(trim($address));
    $file = "http://maps.googleapis.com/maps/api/geocode/json?address=".$address."&;key=AIzaSyDwYgGY1vNAzuzp6K5K5knvwP3rGlMVuLQ&sensor=false";
    $contents = file_get_contents($file);
    //echo $contents;
    $obj = json_decode($contents, true);
    $latitude = $obj['results'][0]['geometry']['location']['lat'];
    $longitude = $obj['results'][0]['geometry']['location']['lng'];
    
    return array ($latitude,$longitude);   
}

function returnMatch ($distance) {
    
    if ($distance == 0) {
        $message = "<font color='green'>Exact Match Venue</font>";
    } else {
        $message = $distance;
    }
    
    return $message;

} ?>

<!DOCTYPE html>
<html>
  <head>
    <title>Simple Map</title>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
    <style>
      html, body {
        font-family: Arial;
        font-size:16px;
      }
      
      td {
        font-family: Arial;
        font-size:16px;
      }
    </style>
  </head>
  <body>
    <?php
    echo "Checking Venue: <strong>".$_POST['venue']."</strong><br /><br />";
    ?>
    <table width="60%">
    <tr>
        <td colspan="4">
            <?php
            if ($numSimilarVenues > 0) {
                echo "There are <font style='font-weight:bold;font-size:18px;'>".$numSimilarVenues."</font> venues that are similar to the venue you are submitting.
                    To ensure you are not proposing a venue that is already in use by another Franchise, please double check the ones below.  If you are the first franchise
                    to propose this venue, then click the Submit Venue link at the bottom of the page.<br /><br />";
            }
            ?>
        </td>
    </tr>
    <?php
    $counter = 0;
    foreach ($results As $res) {
        $address = $res['venue'].",".$res['town'].",".$res['state'].",".$res['country'];
        //echo $address."<br />";
        $latLong = getLatLongFromPostcode($address);
        //print_r($latLong); 
        ?>
        <tr>
            <td>
                <?php
                echo "<strong>".$res['venue']."</strong><br />";
                echo "Match: ".returnMatch($res['levscore'])."<br />";
                echo "Latitude: ".$latLong[0]."<br />";
                echo "Longitude: ".$latLong[0]."<br />"; ?>
            </td>
            <td>&nbsp;</td>
            <td>
                <div id="map_<?php echo $counter;?>"></div>
            </td>
            <td>
                <textarea style="width:400px;height:150px;"><iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" width="600" height="450" src="https://maps.google.com/maps?hl=en&q=<?php echo $address;?>&ie=UTF8&t=roadmap&z=10&iwloc=B&output=embed"><div></div></iframe></textarea>
            </td>
        </tr>
        <style>
        #map_<?php echo $counter;?> {
            height: 250px;
            width: 350px;
        }
        </style>
    <?php
        $counter++;
    }   
    ?>
    
    <script>
        var map;
        function initMap() {
            <?php
            $counter = 0;
            foreach ($results AS $res) {
                $address = $res['venue'].",".$res['town'].",".$res['state'].",".$res['country'];
                $latLong = getLatLongFromPostcode($address); ?>
                map = new google.maps.Map(document.getElementById('map_<?php echo $counter;?>'), {
                    center: {lat: <?php echo $latLong[0];?>, lng: <?php echo $latLong[1];?>},
                    zoom: 15
                });
                
                var marker = new google.maps.Marker({
                    position: {lat: <?php echo $latLong[0];?>, lng: <?php echo $latLong[1];?>},
                    map: map,
                    title: '<?php echo $res['venue'];?>'
                });
                
                // Add circle overlay and bind to marker
                var circle = new google.maps.Circle({
                    map: map,
                    radius: 400,    // 10 miles in metres
                    strokeColor: '#FF0000',
                    strokeOpacity: 0.4,
                    strokeWeight: 2,
                    fillColor: '#FF0000',
                    fillOpacity: 0.2
                });
                circle.bindTo('center', marker, 'position');
                
            <?php
                $counter++;
            } ?>
        }
    </script>
    <tr>
        <td colspan="3">
            <br /><br />My venue is not listed above, please submit this venue for approval. <a href="#">Submit Venue</a>
        </td>
    </tr>
    </table>
    <br /><br />
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAMRv_ta7Dw0TAdSeKwvOh8F6GiP_5sdQc&callback=initMap" async defer></script>
  </body>
</html>
