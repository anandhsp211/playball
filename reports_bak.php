<?php 
function replacetheText($value) {
	if ($value == "userid") {
		return "Created By";
	} elseif ($value == "datecreated") {
		return "Date Created";
	} elseif ($value == "recordid") {
		return "ID";
	} elseif ($value == "productname") {
		return "Product Name";
	} elseif ($value == "productcode") {
		return "Product Code";
	} elseif ($value == "salesprice") {
		return "Sales Price";
	} elseif ($value == "taxcode") {
		return "Tax Code";
	} elseif ($value == "accountcode") {
		return "Account Code";
	} elseif ($value == "betacolumn") {
		return "Total";
	} else {
		return ucwords($value);
	}
}
 ?>
<br>
<table border="0" width="98%" cellpadding="0" cellspacing="3">
<tr>
	<td rowspan="2" width="10"><img src="/images/spacer.gif" height="3" width="2" border="0"></td>
	<td valign="top">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td width="3"><img src="/images/spacer.gif" height="3" width="5" border="0"></td>
		</tr>
		<tr>
			<td width="3%" align="left"><img src="/images/icons/24_shadow/text_rich_colored.png" border=0></td>
			<td width="80%" align="left">&nbsp;<strong><font style="font-size:140%;font-family:Trebuchet MS">Reports</font></strong> </td>
			<td width="15%" align="right">&nbsp;</td>
			<td width="1%">&nbsp;</td>
		</tr>
		</table><br>
	</td>
</tr>
<tr>
	<td valign="top">
		<table class="note" width="100%" border="0" cellpadding="3" cellspacing="2">
		<tr>
			<td valign="top" width="185" style="background:#ededed;padding:5px"><strong>Report Tree</strong></td>
			<td width="15">&nbsp;</td>
			<td>
				<?php 
				if ($gval[0] == 1) {
					echo "List Existing Reports";
				} elseif ($gval[0] == 2) {
					$query = "SELECT mt.label, mst.hashid FROM menu_tabs mt
								INNER JOIN menusub_tabs mst ON mst.menu_tabhashid = mt.hashid
								WHERE mst.type = 'new'
								AND mt.registrantid = ".RID."
								AND mt.isactive = 1
								AND mt.label != 'Reports'
								ORDER BY mt.label ASC
								";
					// echo $query;
					$result = mysql_query($query); ?>
				<Script language="JavaScript">
				<!-- Script courtesy of http://www.web-source.net - Your Guide to Professional Web Site Design and Development
				function goto(form) { var index=form.select.selectedIndex
					if (form.select.options[index].value != "0") {
					location=form.select.options[index].value;}}
				//-->
				</SCRIPT>
				<form name="newreport" method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?>?id=3::::::::::::::::::::<?php echo $gval[20] ?>">
					<strong>Create New Report:</strong><br><br>
					<?php 
					unset($_SESSION['betacolumn'],$_SESSION['fieldtogroup']); ?>
					Select a Module for which a new Report has to be created:<br><br>
					<select name="select" class="note" onchange="goto(this.form)">
						<option value="">Select One</option>
					<?php 
					while ($row = mysql_fetch_array($result)) { 
						if ($gval[1] == $row['hashid']) {
							$selected_1 = "selected";
						}?>
						<option <?php echo $selected_1; ?> value="<?php echo $_SERVER['PHP_SELF'] ?>?id=<?php echo $gval[0] ?>:<?php echo $row['hashid']; ?>:<?php echo $row['label']; ?>::::::::::::::::::<?php echo $gval[20] ?>"><?php echo $row['label']; ?></option>
					<?php 
						unset($selected_1);
					} ?>
					</select><br><br>
					Select related sub module:<br><br>
					<?php 
					// Lets check what sub tables are associated to top level record set. 
					$getrelated = "SELECT childtable, labelhashid, ispredefined FROM relations_matrix WHERE parenttable = '".$gval[1]."'";
					// echo $getrelated."<br>";
					$resgetrelated = mysql_query($getrelated);  ?>
					<select name="submodule[]" id="submodule" multiple class="note" style="height:200px;width:150px">
						<?php 
						while ($rowgetrelated = mysql_fetch_array($resgetrelated)) { 
								if (is_numeric($rowgetrelated['childtable'])) {
									$predef = "SELECT id, label, location FROM relations_predefined WHERE id = ".$rowgetrelated['childtable'];
									$respredef = mysql_query($predef); 
									$rowpredef = mysql_fetch_array($respredef); ?>
									<option value='<?php echo $rowpredef['label'] ?>:<?php echo $rowpredef['location'] ?>:<?php echo $rowpredef['id'] ?>:1'><?php echo $rowpredef['label'] ?></option>
							<?php 	// Now lets get the fields for these elements
									// because they are pre-defined I will need to store 
									// them in an array somewhere
								} else {
									$getpre = "SELECT id, tablabel,location FROM udf_definitions WHERE hashid = '".$rowgetrelated['labelhashid']."' LIMIT 1";
									//echo $getpre;
									$resgetpre = mysql_query($getpre); 
									$rowgetpre = mysql_fetch_array($resgetpre); ?>
									<option value='<?php echo $rowgetpre['tablabel'] ?>:<?php echo $rowgetrelated['labelhashid'] ?>:<?php echo $rowgetpre['id'] ?>:0'><?php echo $rowgetpre['tablabel'] ?></option>
								<?php
								}
						}?>
					</select><br>
					<br>
				<input type="submit" value="Next Step">
				</form>
				<?php 
				} elseif ($gval[0] == 3) {  ?>
					<script type="text/javascript" language="JavaScript" src="/js/formtool.js"></script>
					<script language="JavaScript">
					function isNetscape(v) {
  						return isBrowser("Netscape", v);
  					}
					function isMicrosoft(v) {
						return isBrowser("Microsoft", v);
					}
					function isBrowser(b,v) {
  						browserOk = false;
						versionOk = false;
						browserOk = (navigator.appName.indexOf(b) != -1);
							if (v == 0) versionOk = true;
							else  versionOk = (v <= parseInt(navigator.appVersion));	
							return browserOk && versionOk;
					}
					var ns = (document.layers)? true: false
					var ie = (document.all)? true: false	
					var browser = (ns)? "Netscape": (ie) ?"Explorer" : " an out-dated browser!?!"
					

					function moveUp() {
						var selectedColumnsObj;
            			var currpos = newreport.selectedColumnsObj.options.selectedIndex;
            			if (currpos > 0) {
			                var prevpos = newreport.selectedColumnsObj.options.selectedIndex-1
							if (browser == 'Explorer') {
			                    temp = newreport.selectedColumnsObj.options[prevpos].innerText
            			        newreport.selectedColumnsObj.options[prevpos].innerText = newreport.selectedColumnsObj.options[currpos].innerText
			                    newreport.selectedColumnsObj.options[currpos].innerText=temp     
            			    } else if (browser == 'Netscape') {
			                    temp = newreport.selectedColumnsObj.options[prevpos].text
			                    newreport.selectedColumnsObj.options[prevpos].text = newreport.selectedColumnsObj.options[currpos].text
            			        newreport.selectedColumnsObj.options[currpos].text = temp
			                }
            			    temp = newreport.selectedColumnsObj.options[prevpos].value
			                newreport.selectedColumnsObj.options[prevpos].value = newreport.selectedColumnsObj.options[currpos].value
            			    newreport.selectedColumnsObj.options[currpos].value = temp
				            newreport.selectedColumnsObj.options[prevpos].selected = true
				            newreport.selectedColumnsObj.options[currpos].selected = false
			            }
        			}
					
					function moveDown() {
			            var currpos = newreport.selectedColumnsObj.options.selectedIndex
            			if (currpos < newreport.selectedColumnsObj.options.length-1) {
			                var nextpos = newreport.selectedColumnsObj.options.selectedIndex+1
							if (browser == 'Explorer') {
            					temp = newreport.selectedColumnsObj.options[nextpos].innerText
								newreport.selectedColumnsObj.options[nextpos].innerText = newreport.selectedColumnsObj.options[currpos].innerText
								newreport.selectedColumnsObj.options[currpos].innerText=temp	
			                } else if (browser == 'Netscape') {
                				temp = newreport.selectedColumnsObj.options[nextpos].text
								newreport.selectedColumnsObj.options[nextpos].text = newreport.selectedColumnsObj.options[currpos].text
								newreport.selectedColumnsObj.options[currpos].text=temp
							}
                			temp = newreport.selectedColumnsObj.options[nextpos].value
               				newreport.selectedColumnsObj.options[nextpos].value = newreport.selectedColumnsObj.options[currpos].value
                			newreport.selectedColumnsObj.options[currpos].value = temp
                    		newreport.selectedColumnsObj.options[nextpos].selected = true
                			newreport.selectedColumnsObj.options[currpos].selected = false
            			}
        			}
					</script>
			<table class="note" border="0" cellpadding="5" cellspacing="3">
			<tr>
				<td valign="top">
				<?php 
					// echo $_POST['select']."<br>";
					//echo $_POST['submodule']."<br>";
					//$submod = explode(":",  $_POST['submodule']);
					$primarymodule = explode(":",  $_POST['select']);
			 		$sql = "SELECT id, label,fieldtype,location FROM udf_definitions WHERE location = '".$primarymodule[1]."' AND registrantid =".RID." AND isactive = 1 AND fieldtype != 11 ORDER BY sortorder ASC";
					// echo $sql."<br>";
					$res = mysql_query($sql);  ?>
					<form method="POST" name="newreport" action="<?php echo $_SERVER['PHP_SELF'] ?>?id=4::::::::::::::::::::<?php echo $gval[20] ?>">
			   		Available Fields(s):<br>
			    	<select name="modtab[]" multiple style="font-family:Tahoma;font-size:11px;width:200px;height:250px"> 
					<optgroup label='<?php echo $primarymodule[2] ?>' style="border:none">
				  <?php 
				  	$thetablabeli = $primarymodule[2];
				  	$prime_identifier =random_string('alpha',3);
					$zerocounter = 1;
				 	while ($row = mysql_fetch_array($res)) { 
						if ($zerocounter == 1) {?>
							<option label="ID" value="<?php echo $row['id'] ?>:<?php echo $row['fieldtype'] ?>:recordid:udf_<?php echo $row['location'] ?>:<?php echo $prime_identifier; ?>:<?php echo $thetablabeli ?>">ID</option>    
					<?php 
							$zerocounter++;
						} ?>
				  		<option label="<?php echo ucwords($row['label']) ?>" value="<?php echo $row['id'] ?>:<?php echo $row['fieldtype'] ?>:<?php echo $row['label'] ?>:udf_<?php echo $row['location'] ?>:<?php echo $prime_identifier; ?>:<?php echo $thetablabeli ?>"><?php echo ucwords($row['label']) ?></option>
					<?php
				  	}
					// Lets check what sub tables are associated to top level record set. 
					$getrelated = "SELECT childtable, labelhashid, ispredefined FROM relations_matrix WHERE parenttable = '".$primarymodule[1]."' AND isactive = 1";
					echo $getrelated."<br>";
					$resgetrelated = mysql_query($getrelated); 
					while ($rowgetrelated = mysql_fetch_array($resgetrelated)) { 
						foreach ($_POST['submodule'] as $newprcess) {
						$submod = explode(":",  $newprcess);
						if (is_numeric($rowgetrelated['childtable'])) {
							$predef = "SELECT id, label, location FROM relations_predefined WHERE id = ".$rowgetrelated['childtable'];
							//echo $predef;
							$respredef = mysql_query($predef); 
							$rowpredef = mysql_fetch_array($respredef); 
							if ($rowpredef['location'] != "") { // Predefined dynamic component 
								$identifier =random_string('alpha',2);?>
								<optgroup label='<?php echo $rowpredef['label'] ?>' style="border:none">
								<?php 
								if ($rowpredef['location'] == "E4DA3B7FBBCE2345D7772B0674A318D5") { //  This is the sales element ?>
									<option label="Product Name" value="predef:62:productname:sales_1:<?php echo $identifier; ?>:Products">Product Name</option>
									<option label="Product Code" value="predef:62:productcode:sales_1:<?php echo $identifier; ?>:Products">Product Code</option>
									<option label="Sales Price" value="predef:13:salesprice:sales_1:<?php echo $identifier; ?>:Products">Sales Price</option>
									<option label="Tax Code" value="predef:62:taxcode:sales_1:<?php echo $identifier; ?>:Products">Tax Code</option>
									<option label="Account Code" value="predef:62:accountcode:sales_1:<?php echo $identifier; ?>:Products">Account Code</option>
									<option label="Discount" value="predef:13:discount:sales_1:<?php echo $identifier; ?>:Products">Discount</option>
									<option label="Quantity" value="predef:13:quantity:sales_1:<?php echo $identifier; ?>:Products">Quantity</option>
							<?php 
								} elseif ($rowpredef['location'] == "182BE0C5CDCD5072BB1864CDEE4D3D6E") { //  This is the resources element ?>
									
							<?php 
								} ?>
						<?php  
							} else {
								if (($rowpredef['id'] == 2) && ($a_set != 1)) { // 2 is for addresses  
									$identifier =random_string('alpha',3); ?>
									<optgroup label='<?php echo $rowpredef['label'] ?>' style="border:none">
									<option label="Address Line 1" value="predef:2:addressline1:address_info:<?php echo $identifier; ?>:Addresses">Address Line 1</option>
									<option label="Address Line 2" value="predef:2:addressline2:address_info:<?php echo $identifier; ?>:Addresses">Address Line 2</option>
									<option label="City" value="predef:2:city:address_info:<?php echo $identifier; ?>:Addresses">City</option>
									<option label="State/Province/County" value="predef:2:state:address_info:<?php echo $identifier; ?>:Addresses">State/Province/County</option>
									<option label="Zip/Post Code" value="predef:2:postcode:address_info:<?php echo $identifier; ?>:Addresses">Zip/Post Code</option>
									<option label="Country" value="predef:2:country:address_info:<?php echo $identifier; ?>:Addresses">Country</option>
									<option label="Address Type" value="predef:2:addresstype:address_info:<?php echo $identifier; ?>:Addresses">Address Type</option>
					<?php 		$a_set = 1;
								} elseif ($rowpredef['id'] == $submod[2]) { 
									if (($submod[2] == 4) && ($m_set != 1)) { // 4 is for messaging 
										$identifier =random_string('alpha',4); ?>
										<optgroup label='<?php echo $rowpredef['label'] ?>' style="border:none">
										<option label="TO" value="predef:4:senttoaddress:messaging:<?php echo $identifier; ?>:Messaging">TO</option>
										<option label="CC" value="predef:4:cctoaddress:messaging:<?php echo $identifier; ?>:Messaging">CC</option>
										<option label="BCC" value="predef:4:bcctoaddress:messaging:<?php echo $identifier; ?>:Messaging">BCC</option>
										<option label="Subject" value="predef:4:emailsubject:messaging:<?php echo $identifier; ?>:Messaging">Subject</option>
					<?php 			$m_set = 1;
									} elseif (($submod[2] == 67) && ($n_set != 1)) { // 67 is for notes
										$identifier =random_string('alpha',5); ?>
										<optgroup label='<?php echo $rowpredef['label'] ?>' style="border:none">
										<option label="Created By" value="predef:67:userid:notes:<?php echo $identifier; ?>:Notes">Added By</option>
										<option label="Created Date" value="predef:67:datecreated:notes:<?php echo $identifier; ?>:Notes">Created Date</option>
										<option label="Note" value="predef:67:note:notes:<?php echo $identifier; ?>:Notes">Note</option>
					<?php  			$n_set = 1;
									} elseif (($submod[2] == 3) && ($d_set != 1)) { // 3 is for documents 
										$identifier =random_string('alpha',6); ?>
										<optgroup label='<?php echo $rowpredef['label'] ?>' style="border:none">
										<option label="File Name" value="predef:3:filename:filemanager:<?php echo $identifier; ?>:Documents">File Name</option>
										<option label="Created By" value="predef:3:userid:filemanager:<?php echo $identifier; ?>:Documents">Added By</option>
										<option label="Created Date" value="predef:3:datecreated:filemanager:<?php echo $identifier; ?>:Documents">Created Date</option>
					<?php  			$d_set = 1;
									} elseif (($submod[2] == 1) && ($do_set != 1)) { // 1 is for activities 
										$identifier =random_string('alpha',7); ?>
										<optgroup label='<?php echo $rowpredef['label'] ?>' style="border:none">
										<option label="Subject" value="predef:1:subject:activities:<?php echo $identifier; ?>:Activities">Subject</option>
										<option label="Start Date" value="predef:1:datestart:activities:<?php echo $identifier; ?>:Activities">Start Date</option>
										<option label="End Date" value="predef:1:dateend:activities:<?php echo $identifier; ?>:Activities">End Date</option>
										<option label="Type" value="predef:1:activitytype:activities:<?php echo $identifier; ?>:Activities">Type</option>
										<option label="Status" value="predef:1:activitystatus:activities:<?php echo $identifier; ?>:Activities">Status</option>
										<option label="Show Time As" value="predef:1:showtimeas:activities:<?php echo $identifier; ?>:Activities">Show Time As</option>
										<option label="Label" value="predef:1:label:activities:<?php echo $identifier; ?>:Activities">Label</option>
										<option label="Privacy" value="predef:1:privacy:activities:<?php echo $identifier; ?>:Activities">Privacy</option>
					<?php  			$do_set = 1;
									}
								}
							}
							// Now that we have the element, we need to get the fields.
						} else {
							$getpre = "SELECT tablabel, hashid FROM udf_definitions WHERE hashid = '".$rowgetrelated['labelhashid']."'  AND isactive = 1 LIMIT 1";
							echo $getpre;
							$resgetpre = mysql_query($getpre); 
							$rowgetpre = mysql_fetch_array($resgetpre); 
							if ($rowgetpre['hashid'] == $submod[1]) { 
								$identifier =random_string('alpha',3); ?>
								<optgroup label='<?php echo $rowgetpre['tablabel'] ?>' style="border:none">
						<?php 	$thetablabel = $rowgetpre['tablabel'];
								// Now lets get the fields for these elements
								$getchild = "SELECT * FROM udf_definitions WHERE location = '".$rowgetrelated['childtable']."'  AND isactive = 1 ORDER BY sortorder ASC";
								// echo $getchild."<br>";
								$resgetchild = mysql_query($getchild); 
								while ($rowgetchild = mysql_fetch_array($resgetchild)) {  ?>
									<option label="<?php echo ucwords($rowgetchild['label']) ?>" value="<?php echo $rowgetchild['id'] ?>:<?php echo $rowgetchild['fieldtype'] ?>:<?php echo $rowgetchild['label'] ?>:udf_<?php echo $rowgetchild['location'] ?>:<?php echo $identifier; ?>:<?php echo $thetablabel ?>"><?php echo ucwords($rowgetchild['label']) ?></option>
						<?php 	}
							}
						}
						}
					}	?>
					</select>
    	  			<input type="hidden" name="modtab_save">      
				</td>
				<td align="center"><br>
					<input type=button value="Add &gt&gt;" style="font-family:Arial;font-size:14px;width:100px" onClick="formtool_move(this.form.elements['modtab[]'],this.form.elements['modtabc[]'],this.form.elements['modtab_save'],this.form.elements['modtabc_save'])"><br><br>
					<input type=button value="&lt&lt; Remove" style="font-family:Arial;font-size:14px;width:100px" onClick="formtool_move(this.form.elements['modtabc[]'],this.form.elements['modtab[]'],this.form.elements['modtab_save'],this.form.elements['modtabc_save'])">
				</td>
				<td valign="top" width="165">Added Field(s):<br>
					<select name="modtabc[]" id="selectedColumnsObj" multiple style="font-family:Tahoma;font-size:11px;width:200px;height:250px">
					</select><input type="hidden" name="modtabc_save">
				</td>
				<td>
				<table>
					<tr> 
                <td> <a id="moveup_link" href="javascript:;" title="Move Up"><img src="/images/icons/16_shadow/navigate_up.png" alt="Move Up" border="0" align="absmiddle" onClick="moveUp()"></a> 
                    <img id="moveup_disabled" src="/images/spacer.gif" width="16" height="16" style="display:none"> 
                </td>
            </tr>
            <tr> 
                <td> <a id="movedown_link" href="javascript:;" title="Move Down"><img src="/images/icons/16_shadow/navigate_down.png"  alt="Move Down" border="0" align="absMiddle" onClick="moveDown()"></a> 
                    <img id="movedown_disabled" src="/crm/images/spacer.gif" width="24" height="24" class="movecol_down_disabled" style="display:none"> 
                </td>
            </tr>
				</table>
				</td>
			</tr>
			<tr>
				<td align="left" colspan="4">
					<input type="submit" name="generator" value="Next Step" onClick="this.value=formtool_selectall('modtabc[]', this.form.elements['modtabc[]'],'Add Module Tabs','Processing')" style="font-family:Arial;font-size:14px;">
				</td>
			</tr>
			</table>
			<input type="hidden" name="prime_identifier" value="<?php echo $prime_identifier; ?>">
			<input type="hidden" name="primary_mod" value="<?php echo $_POST['select'] ?>">
			<input type="hidden" name="select" value="<?php echo $prime_identifier; ?>">
			<input type="hidden" name="submodule" value="<?php echo $_POST['select'] ?>">
			</form>
				<?php 
				} elseif ($gval[0] == 4) { ?>
					<form method="POST" name="newreport" action="<?php echo $_SERVER['PHP_SELF'] ?>?id=5::::::::::::::::::::<?php echo $gval[20] ?>">
					
					<table width="100%">
					<tr align="center">
						<td colspan="5" class="ls_top"><strong>Columns to Total</strong></td>
					</tr>
					<tr>
						<td class="ls_top"><strong>Columns</strong></td>
						<td width="18%" class="ls_top" align="center"><strong>Sum</strong></td>
						<td width="18%" class="ls_top" align="center"><strong>Average</strong></td>
						<td width="18%" class="ls_top" align="center"><strong>Largest Value</strong></td>
						<td width="18%" class="ls_top" align="center"><strong>Smallest Value</strong></td>
					</tr>
					<tr>
						<td class="ls_off">Record Count</td>
						<td class="ls_off" align="center"><input type="checkbox" name="recordcount"></td>
						<td class="ls_off" colspan="3">&nbsp;</td>
					</tr>
					<?php 
					$num_rows = count($_POST['modtabc']);
					$counter = 1;
					$classcounter = 1;
					foreach ($_POST['modtabc'] as &$value) {
						// echo $value."<br>";
						$typvalue = explode(":",$value);
						if ($counter == $num_rows) {
							$storestring .= $value;
						} else {
							$storestring .= $value."||";
						}
						
						// lets pull the fields that can be used for 
						// sum and average
						if (($typvalue[1] == 13) || ($typvalue[1] == 14)) { 
							include("includes/ls.php");?>
							<tr>
								<td class="ls_<?php echo $ls ?>"><?php echo replacetheText($typvalue[2]); ?></td>
								<td class="ls_<?php echo $ls ?>" align="center"><input type="checkbox" name="recordcount"></td>
								<td class="ls_<?php echo $ls ?>" align="center"><input type="checkbox" name="recordcount"></td>
								<td class="ls_<?php echo $ls ?>" align="center"><input type="checkbox" name="recordcount"></td>
								<td class="ls_<?php echo $ls ?>" align="center"><input type="checkbox" name="recordcount"></td>
							</tr>
					<?php 
						}
						$counter++;
					} 
					// echo $storestring; ?>
					</table>
					<br><br>
					<table border="0" width="100%">
					<tr align="center">
						<td colspan="5" class="ls_top"><strong>Grouping</strong></td>
					</tr>
					<tr>
						<td colspan="5" class="ls_off">Summarise by:</td>
					</tr>
					<tr>
						<td class="ls_off" width="15%">
						<select class="note" name="group_field_1">
						<option value="">Select One</option>
						<?php 
						$lablapply = 1;
						foreach ($_POST['modtabc'] as &$value) { 
							$option = explode(":",$value); 
							if ($lablapply == 1) {
								$applylable = $option[5]; ?>
								<optgroup label='<?php echo $applylable; ?> &raquo;' class="select" style="border:none"> 
							<?php 
								$lablapply++;
							}  
							if ($applylable != $option[5]) { 
								$applylable = $option[5]; ?>
								<optgroup label='<?php echo $applylable; ?> &raquo;' class="select" style="border:none">
						<?php 
							} ?> 
							<option value="<?php echo $value; ?>"><?php echo replacetheText($option[2]); ?></option>
					<?php 
						} ?>
						</select>
						</td>
						<td class="ls_off">
							<select class="note" name="sort_order_1">
								<option value="ASC">Ascending
								<option value="DESC">Descending
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="5" class="ls_on">And then by:</td>
					</tr>
					<tr>
						<td class="ls_on" width="15%">
						<select class="note" name="group_field_2">
						<option value="">Select One</option>
						<?php 
						$lablapply = 1;
						foreach ($_POST['modtabc'] as &$value) { 
							$option = explode(":",$value); 
							if ($lablapply == 1) {
								$applylable = $option[5]; ?>
								<optgroup label='<?php echo $applylable; ?> &raquo;' class="select" style="border:none"> 
							<?php 
								$lablapply++;
							}  
							if ($applylable != $option[5]) { 
								$applylable = $option[5]; ?>
								<optgroup label='<?php echo $applylable; ?> &raquo;' class="select" style="border:none">
						<?php 
							} ?> 
							<option><?php echo replacetheText($option[2]); ?></option>
					<?php 
						} ?>
						</select>
						</td>
						<td class="ls_on">
							<select class="note" name="sort_order_2">
								<option value="ASC">Ascending
								<option value="DESC">Descending
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="5" class="ls_off">And finally by:</td>
					</tr>
					<tr>
						<td class="ls_off" width="15%">
						<select class="note" name="group_field_3">
						<option value="">Select One</option>
						<?php 
						$lablapply = 1;
						foreach ($_POST['modtabc'] as &$value) { 
							$option = explode(":",$value); 
							if ($lablapply == 1) {
								$applylable = $option[5]; ?>
								<optgroup label='<?php echo $applylable; ?> &raquo;' class="select" style="border:none"> 
							<?php 
								$lablapply++;
							}  
							if ($applylable != $option[5]) { 
								$applylable = $option[5]; ?>
								<optgroup label='<?php echo $applylable; ?> &raquo;' class="select" style="border:none">
						<?php 
							} ?> 
							<option><?php echo replacetheText($option[2]); ?></option>
					<?php 
						} ?>
						</select>
						</td>
						<td class="ls_off">
							<select class="note" name="sort_order_3">
								<option value="ASC">Ascending
								<option value="DESC">Descending
							</select>
						</td>
					</tr>
					</table>
					<br><br>
					<input type="hidden" name="columnstring" value="<?php echo $storestring; ?>">
					<input type="hidden" name="prime_identifier" value="<?php echo $_POST['prime_identifier']; ?>">
					<input type="hidden" name="primary_mod" value="<?php echo $_POST['primary_mod'] ?>"><br>
					<input type="submit" value="Next">
				<?php 
				} elseif ($gval[0] == 5) { 
					$activatenotes = 0;?>
					<table class="note" width="100%" border="0" cellspacing="2" cellpadding="1">
					<tr>
						<td align="right"><?php include_once("includes/record_gen.php"); ?></td>
					</tr>
					</table>
					<table class="note" width="100%" border="0" cellspacing="2" cellpadding="1">
						<?php 
						// Identify an masrk primary table
						if ($_POST['primary_mod'] != "") {
							$primary_mod = explode(":", $_POST['primary_mod']);
							// we need to get the tab id for the primary module
							$querytabid = "SELECT menu_tabhashid FROM menusub_tabs WHERE hashid = '".$primary_mod[1]."'";
							$resulttabid = mysql_query($querytabid);
							$rowtabid = mysql_fetch_array($resulttabid) or die('Daeda');
							$primary_tabid = $rowtabid['menu_tabhashid'];
							$_SESSION['primary_tabid'] = $primary_tabid;
							
							$primary_mod = $primary_mod[1];
							$_SESSION['prime_mod'] = $primary_mod;
							if (strlen($primary_mod) == 32) {
								$addudf = "udf_";
							}
							$prime_identifier = $_POST['prime_identifier'];
							if ($_POST['prime_identifier'] != "") {
								$_SESSION['prime_identifier'] = $prime_identifier;
							}
						
							$primary_table = $addudf.$primary_mod." ".$_POST['prime_identifier'];
							$_SESSION['primary_table'] = $primary_table;
							$primary_table_compare = $addudf.$primary_mod; // used to check we dont add it to the inner join string
							$_SESSION['primary_table_compare'] = $primary_table_compare;
						} else {
							$primary_mod = $_SESSION['prime_mod'];
							$prime_identifier = $_SESSION['prime_identifier'];
							$primary_table = $_SESSION['primary_table'];
							$primary_table_compare = $_SESSION['primary_table_compare'];
							$primary_tabid = $_SESSION['primary_tabid'];
						}
						
						if ($activatenotes == 1) {
							echo "Primary Module: ".$primary_mod."<br>";
							echo "Primary Identifier: ".$prime_identifier."<br>";
							echo "Primary Table: ".$primary_table."<br>";
							echo "Primary Table Compare: ".$primary_table_compare."<br><br>";
							echo "Section 1 Start: Getting Values both POST and SELECT<br>";
						}
						
						if ($_POST['columnstring']) {
							// If the values exist i a post use those
							$recordval = explode("||",$_POST['columnstring']);
						} else {
							// Lets check if we need to do an insert or update
							$checklistx = "SELECT searchresults FROM reports_list_results_arrays WHERE sessionid = '".session_id()."'";
							$reslistx = mysql_query($checklistx);
							$rowlistx = mysql_fetch_array($reslistx);
							$recordval = explode("||",$rowlistx['searchresults']);
						}
						
						if ($activatenotes == 1) {
							echo "$ recordval<br><br>";
							echo "<pre>Recordval: ";
							 print_r($recordval);
							echo "</pre>";
						}
						
						$num_rows = count($recordval);
						$makkarray = array();
						$tablearray = array();
						$resultarray = array();
						
						// Create the sql fields list
						foreach ($recordval as $value33) {
							$detail = explode(":",$value33);
							array_push($resultarray, $detail[3]);
						}
						
						foreach ($recordval as $value) {
							$indivals = explode(":",$value);
							array_push($makkarray,columnnames($indivals[4].".".$indivals[2]));
							if ((!in_array($indivals[3].":".$indivals[4], $tablearray)) && ($primary_table_compare != $indivals[3])) {
								array_push($tablearray,$indivals[3].":".$indivals[4]);
							}
						}
						
						foreach ($tablearray as $value) {
							$indivals = explode(":",$value);
							$secondarytable .= " LEFT JOIN ".$indivals[0]." ".$indivals[1]." ON ".$prime_identifier.".hashid = ".$indivals[1].".recordid ";
						}
						
						$finalvalsu = count($makkarray);
						$checka = 1;
						
						///////////////////////////////////////////
						/////  Start the restructure for grouping /
						///////////////////////////////////////////
						
						if ($activatenotes == 1) {
							echo "Section 2 Start: Getting GROUP INFO POST and SELECT<br>";
						}
						
						if ($_POST['group_field_1'] != "") {
							// lets create acount sql statement
							$fieldtogroup = explode(":", $_POST['group_field_1']);
							$_SESSION['fieldtogroup'] = $fieldtogroup;
						} else {
							$fieldtogroup = $_SESSION['fieldtogroup'];
						}
						
						if ($activatenotes == 1) {
							echo "$ fieldtogroup<br>";
							echo "<pre>";
							 print_r($fieldtogroup);
							echo "</pre>";
						}
						
						if ($activatenotes == 1) {
							echo "Section 3 Start: Build $ columnstring, $ newcolumnstring, $ tablecolumns<br>";
						}
						
						if ($_POST['columnstring']) {
							
							/// Build a $columnstr start
							
							$makkarray_2 = array();
							foreach ($makkarray AS $newcalues) {
								if ($newcalues != columnnames($fieldtogroup[4].".".$fieldtogroup[2])) {
									array_push($makkarray_2,$newcalues);
								} 
							}
							
							print_r($fieldtogroup);
							if ($fieldtogroup != "") {
								array_insert(&$makkarray_2, $fieldtogroup[4].".".$fieldtogroup[2], $position = 0);
							}
							
							foreach ($makkarray_2 as $value) {
								if ($finalvalsu != $checka) {
									$columnstr .= columnnames($value).",";
								} else {
									$columnstr .= columnnames($value);
								}
								$checka++;
							}
							
							$_SESSION['columnstr'] = $columnstr;
							
							/// Build a $columnstr end
							
							/// Build a $tablecolumns start
							
							$commacounter = 0;
							foreach ($recordval as &$value) {
								$trickyval = explode(":",$value);
								if (($commacounter+1) != $num_rows) {
									$tablecolumns .= $trickyval[2].",";
								} else {
									$tablecolumns .= $trickyval[2];
								}
								$commacounter++;
							}
							$tablecolumns_2 = explode(",", $tablecolumns);
						
							$tablecolumns = array();
							$thisisthekey = 0;
							foreach ($tablecolumns_2 AS $newcalues) {
								if ($newcalues != $fieldtogroup[2]) {
									array_push($tablecolumns,$newcalues);
								} else {
									$newvaluetol = $newcalues;
									$thisisthekey_2 = $thisisthekey;
								}
								$thisisthekey++;
							}
							if ($fieldtogroup != "") {
								array_insert(&$tablecolumns, $newvaluetol, $position = 0);
							}
							
							$_SESSION['tablecolumns'] = $tablecolumns;
							
							/// Build a $tablecolumns end
						
							////////////////////////////////////////////////////
							// now lets reformat the $_POST['columnstring']  ///
							////////////////////////////////////////////////////
						
							$columnstringarray = explode("||",$_POST['columnstring']);
							$thisisthekey_3 = 0;
							$columnstringarray_2 = array();
							foreach ($columnstringarray AS $colitem) {
								if (($thisisthekey_3) != $thisisthekey_2) {
									array_push($columnstringarray_2,$colitem);
								} else {
										$insertthisitem = $colitem;
								}
								$thisisthekey_3++;
							}
							array_insert(&$columnstringarray_2, $insertthisitem, $position = 0);
						
							$num_rows = count($columnstringarray_2);
							$commacounter = 0;
							foreach ($columnstringarray_2 as &$value) {
								if (($commacounter+1) != $num_rows) {
									$newcolumnstring .= $value."||";
								} else {
									$newcolumnstring .= $value;
								}
								$commacounter++;
							}
							
							$_SESSION['newcolumnstring'] = $newcolumnstring;
						
							if ($activatenotes == 1) {
								echo "<pre>Column String: ";
								 print_r($columnstr);
								echo "</pre>";
							
								echo "<pre>New Column String: ";
								 print_r($newcolumnstring);
								echo "</pre>";
							
								echo "<pre>Table Columns: ";
								 print_r($tablecolumns);
								echo "</pre>";
							}
							
						} else {
							// if we are not dealing with a POST we use sessions
							$tablecolumns = $_SESSION['tablecolumns'];
							$columnstr = $_SESSION['columnstr'];
							$newcolumnstring = $_SESSION['newcolumnstring'];
							
							if ($activatenotes == 1) {
								echo "<pre>Column String: ";
								 print_r($columnstr);
								echo "</pre>";
							
								echo "<pre>New Column String: ";
								 print_r($newcolumnstring);
								echo "</pre>";
							
								echo "<pre>Table Columns: ";
								 print_r($tablecolumns);
								echo "</pre>";
							}
						}
						
						////////////////////////////////////////////
						/////  End the restructure for grouping ////
						////////////////////////////////////////////
						
						$trcounter = 1;
						$getprimary = 1;
						
						/////////////////////////
						

		$divideby = ($num_rows-1);
		$colwidth = round(85/$divideby)."%"; 
		$addsearchstr = "reportlock";
		if ($_COOKIE[$addsearchstr] != "on") { $displayslide = 'style="display:none;"';	} else { $displayslide2 = 'style="display:none;"'; } ?>
		<tr>
		<td colspan="6">
	<form method="POST" name="listsearch" action="<?php echo $_SERVER['PHP_SELF'] ?>?id=<?php echo $gval[0].":".$gval[1].":".$gval[2].":".$gval[3].":".$gval[4].":".$gval[5].":".$gval[6]."::".$gval[8]."::".$gval[10].":".$gval[11].":".$gval[12].":".$gval[13].":".$gval[14].":".$gval[15].":".$gval[16].":".$gval[17].":".$gval[18].":".$gval[19].":".$gval[20].":".$gval[21].":".$gval[22].":".$gval[23].":".$gval[24].":".$gval[25]; ?>">
	<table border="0" cellpadding="3" cellspacing="0">
	<tr>
		<td>
			<font class="note">
			<span id="ShowSearchOption" <?php echo $displayslide2; ?>><a href="#" onclick="javascript: slideLockOn('ShowSearchOption','ShowCloseSearch','<?php echo $addsearchstr; ?>'); Effect.SlideDown('slidedown_demo', { duration: 0.5, queue: {position: 'end', scope: 'slidedown_demo', limit: 2} }); return false;">Show Search Fields</a></span><span id="ShowCloseSearch" <?php echo $displayslide ?>><a href="#" onclick="javascript: slideLockOff('ShowCloseSearch','ShowSearchOption','<?php echo $addsearchstr; ?>'); Effect.SlideUp('slidedown_demo', { duration: 0.5, queue: {position: 'end', scope: 'slidedown_demo', limit: 2} }); return false;">Hide Search Fields</a></span>
			| <a <?php echo LFD ?> href="/advanced_query.php?id=1:<?php echo $gval[21] ?>">Advanced Filter</a> | <a href="<?php echo $_SERVER['PHP_SELF'] ?>?id=6<?php echo ":".$gval[1].":".$gval[2].":".$gval[3].":".$gval[4].":".$gval[5].":".$gval[6]."::".$gval[8].":".$gval[9].":".$gval[10].":".$gval[11].":".$gval[12].":".$gval[13].":".$gval[14].":".$gval[15].":".$gval[16].":".$gval[17].":".$gval[18].":".$gval[19].":".$gval[20].":".$gval[21].":".$gval[22].":".$gval[23].":".$gval[24].":".$gval[25]; ?>">Remove Filters</a>
			</font>
		</td>
		<?php 
		if ($_POST['postsearch'] == 1) {
			// We use this to load a session so we know we are running a standard search and notify the customer.
			unset($_SESSION['aq'.$gval[21].''],$_SESSION['aql'.$gval[21].'']);
			$_SESSION['std'.$gval[21].''] = 1;
		} ?>
	</tr>
	</table><br style="line-height:10px">
		<div id="slidedown_demo" <?php echo $displayslide; ?>>
			<div>
    			<?php 
				echo "<pre>Table Columns: ";
								 print_r($tablecolumns);
								echo "</pre>";
				foreach ($tablecolumns AS &$tname) {
					$key1 =  $tname;
					$sqlgitems = "SELECT fieldtype FROM udf_definitions WHERE label = '".$tname."' AND location = '".$primary_mod."'";
					$resgitems = mysql_query($sqlgitems);
					$rowgitems = mysql_fetch_array($resgitems);
					//0echo $sqlgitems."<br>";
					$value1 = $rowgitems['fieldtype'];
					$searchbuildershortarray1[$key1]=$value1;
				}
				include_once("includes/standard_form_searchlist_reports.php"); 
				$trcounter = 1;
				?>
  			</div>
			<div class="clear"></div>
		</div>	
	</form>		
	</td>				
	</tr>
					<?php 	
						/////////////////////////
						
						// Add an extra column is it is a single group count
						$countrecordval = count($recordval);
						if ((($countrecordval == 1) && ($_POST['group_field_1'] != "")) || ($gval[19] == "betacolumn")  || ($_SESSION['betacolumn'] == 1)) {
							array_insert(&$tablecolumns, "betacolumn", $position = 1);
							$_SESSION['betacolumn'] = 1;
						}
						foreach($tablecolumns as &$value) {
							if ($trcounter == 1) { echo "<tr>\n"; }
							if ($getprimary == 1) {	$primarysortlabel = columnnames($value); }
							echo "<td class=\"ls_top\" align='center'>
							<table border=0><tr><td><strong>";
								echo replacetheText($value);
							echo "</strong>&nbsp;</td>";
							if ($gval[18] == "ASC") {
								echo '<td><a href="'.$_SERVER['PHP_SELF'].'?id='.$gval[0].':'.$gval[1].':'.$gval[2].':::::'.$gval[7].':'.$gval[8].':'.$gval[9].':'.$gval[10].':'.$gval[11].':'.$gval[12].':'.$gval[13].':'.$gval[14].':'.$gval[15].':'.$gval[16].':'.$gval[17].':DESC:'.columnnames($value).':'.$gval[20].':'.$gval[21].':'.$gval[22].':'.$gval[23].':'.$gval[24].':'.$gval[25].'">';
								if ($gval[19] == columnnames($value)) {
									echo "<img src=\"/images/icons/sort_asc.gif\" border=\"0\"></a></td>";
								} else {
									echo "<img src=\"/images/icons/sort_asc_g.gif\" border=\"0\"></a></td>";
								}
							} else {
								echo '<td><a href="'.$_SERVER['PHP_SELF'].'?id='.$gval[0].':'.$gval[1].':'.$gval[2].':::::'.$gval[7].':'.$gval[8].':'.$gval[9].':'.$gval[10].':'.$gval[11].':'.$gval[12].':'.$gval[13].':'.$gval[14].':'.$gval[15].':'.$gval[16].':'.$gval[17].':ASC:'.columnnames($value).':'.$gval[20].':'.$gval[21].':'.$gval[22].':'.$gval[23].':'.$gval[24].':'.$gval[25].'">';
								if ($gval[19] == columnnames($value)) {
									echo "<img src=\"/images/icons/sort_desc.gif\" border=\"0\"></a></td>";
								} else {
									echo "<img src=\"/images/icons/sort_asc_g.gif\" border=\"0\"></a></td>";
								}
							}
							echo "</tr></table></td>\n";
							if ($trcounter == ($num_rows+1)) { 
								echo "</tr>\n"; $trcounter = 0; 
							}
							$trcounter++;
							$getprimary++;
						}
						
						$columns = explode(",", $columnstr);
						
						if ($activatenotes == 1) {
							echo "Section 4 Start: Build $ sql, $ grouptable <br>";
						}
						
						if ($_POST['columnstring']) {
							
							// there is a post value so we use this query
							$sql = "SELECT ".$_POST['prime_identifier'].".hashid, ".$columnstr." FROM ".$primary_table." \n";
							$sql .= $secondarytable;
							$grouptable = " FROM ".$primary_table." ";
							$grouptable .= $secondarytable;
							$sql .= " WHERE ".$prime_identifier.".issaved = 1 AND ".$prime_identifier.".registrantid =".RID;
							$grouptable .= " WHERE ".$prime_identifier.".issaved = 1 AND ".$prime_identifier.".registrantid =".RID;
							// echo $grouptable."<br><br>";
							$query_pages = $sql;
							$sqllistarray = $sql;
							
							$_SESSION['sqlquery'] = $sql;
							$_SESSION['grouptable'] = $grouptable;
							
							// echo $sql."<br>";
							$querylra = "DELETE FROM reports_list_results_arrays WHERE sessionid = '".session_id()."'";
							mysql_query($querylra);
								
							$querylra = "DELETE FROM reports_list_arrays WHERE sessionid = '".session_id()."'";
							mysql_query($querylra);
								
							$querylra = "INSERT INTO reports_list_results_arrays (sessionid, searchresults, datecreated) VALUES (
					  						'".session_id()."',
						  					'".addslashes($newcolumnstring)."',
					 						'".$sGMTMySqlString."'
				  						)";
							// echo $querylra."<br><br>";
							mysql_query($querylra) Or Die ("Cannot submit list array!");
								
							$queryaddact = "INSERT INTO reports_list_arrays (sessionid, list_array, datecreated) VALUES (
											  '".session_id()."',
											  '".addslashes($sqllistarray)."',
											  '".$sGMTMySqlString."'
										  )";
							//echo $queryaddact;
							mysql_query($queryaddact) Or Die ("Cannot submit list array!");
						} else {
							$sql = $_SESSION['sqlquery'];
							$grouptable = $_SESSION['grouptable'];
							$query_pages = $sql;
						}
						
						if ($activatenotes == 1) {
							echo "<pre>SQL: ";
							 print_r($sql);
							echo "</pre>";
							
							echo "<pre>Group Table: ";
							 print_r($grouptable);
							echo "</pre>";
						}
						
						//echo $columns[0]."<br>";
						if ($gval[18] == "") {
							if ($columns[0] == ".recordid") {
								$addstr = $_SESSION['prime_identifier'];
							}
							if ($gval[9] == "") {
								$sql .= " ORDER BY ".$addstr.$columns[0]." ASC LIMIT 0, ".$size_of_group;
								$sqltogrrp = " ORDER BY ".$columns[0]." ASC LIMIT 0, ".$size_of_group;
							} else {
								$sql .= " ORDER BY ".$addstr.$columns[0]." ASC LIMIT ".$gval[9].", ".$size_of_group."";
								$sqltogrrp = " ORDER BY ".$columns[0]." ASC LIMIT ".$gval[9].", ".$size_of_group."";
							}
						} else {
							if ($gval[9] == "") {
								$gval[9] = 0;
							} 
							$sql .= " ORDER BY ".$gval[19]." ".$gval[18]." LIMIT ".$gval[9].", ".$size_of_group."";
							$sqltogrrp = " ORDER BY ".$gval[19]." ".$gval[18]." LIMIT ".$gval[9].", ".$size_of_group."";
						}
						
						$res = mysql_query($sql);
						// echo $sql."<br><br>";
						
						if ($activatenotes == 1) {
							echo "Section 5 Start: Build $ grouparray <br>";
						}
						
						////////////////////////////////////////////////
						/// lets get the group count for each product //
						////////////////////////////////////////////////
						
						if ($_POST['group_field_1'] != "") {
							// lets create acount sql statement
							$fieldtogroup = explode(":", $_POST['group_field_1']);
							$grpsql = "SELECT ".columnnames($fieldtogroup[2]).", COUNT(".columnnames($fieldtogroup[2]).") AS ".columnnames($fieldtogroup[2])."_count ".$grouptable." GROUP BY ".columnnames($fieldtogroup[2])." ".$_POST['sort_order_1'];
							$_SESSION['grpsql'] = $grpsql;
						} else {
							$grpsql = $_SESSION['grpsql'];
						}
						
						if (($_POST['group_field_1'] != "") || ($grpsql != "")) {
							$resgrp = mysql_query($grpsql);
							while ($rowgrp = mysql_fetch_array($resgrp)) {
								$key =  $rowgrp[columnnames($fieldtogroup[2])];
								$value = $rowgrp[columnnames($fieldtogroup[2]).'_count'];
								$grouparray[$key]=$value;
							}
						}
						
						if ($activatenotes == 1) {
							echo "<pre>Group SQL Query: ";
							 print_r($grpsql);
							echo "</pre>";
							echo "<pre>Group SQL Array: ";
							 print_r($grouparray);
							echo "</pre>";
						}
						
						// If there is a single column selected and
						// it is grouped we just want to run the group
						// query
						
						$countrecordval = count($recordval);
						$isitagroucount = count($grouparray);
						
						if (($countrecordval == 1) && ($isitagroucount > 0)) {
							$query_pages = $grpsql;
							include("includes/ls.php");
							
							$fieldname = explode(".",$columns[0]);
							
							$ceatetemp = "CREATE TEMPORARY TABLE `groupcount_table_temp` (
											`".$fieldname[1]."` VARCHAR( 100 ) NOT NULL ,
											`betacolumn` INT NULL 
										) ENGINE = MYISAM ; ";
							mysql_query($ceatetemp);
							
							foreach ($grouparray AS $key => $value) {
								$insertstring .= "('".$key."', ".$value."),";
							}
							$insertstring = substr($insertstring, 0, -1); 
							
							// insert the values into the temporary table
							$insertempdata = "INSERT INTO `groupcount_table_temp` ( `".$fieldname[1]."` , `betacolumn` ) 
							  				  VALUES ".$insertstring."";
							mysql_query($insertempdata) or die ('Cannot enter data'); 
							
							// select the data and output
							$sql_grp_temp = "SELECT ".$fieldname[1].", betacolumn FROM groupcount_table_temp ";
							$sqltogrrp = str_replace($fieldname[0].".", "", $sqltogrrp);
							$sql_grp_temp .= $sqltogrrp;
							$res_grp_temp = mysql_query($sql_grp_temp);
							
							while ($row_grp_temp = mysql_fetch_array($res_grp_temp)) { 
							
								echo "<tr>";
								for ($number = 0; $number < ($num_rows); $number++) { ?>
									<td class="ls_<?php echo $ls; ?>">&nbsp;
										<?php echo $row_grp_temp[$fieldname[1]]; ?>
									</td>
									<td class="ls_<?php echo $ls; ?>">
										<?php echo "<strong>".$row_grp_temp['betacolumn']."</strong>"; ?>
									</td>
					<?php 		}
								echo "</tr>";
							}
						} else {
							while ($row = mysql_fetch_array($res)) { 
								include("includes/ls.php"); 
								echo "<tr>";
								for ($number = 0; $number < ($num_rows); $number++) { ?>
									<td class="ls_<?php echo $ls; ?>">
										<?php 
										$usecorrectval = explode(".",$columns[$number]);
										if ($row[$columns[$number]] == "checkboxon") {
											echo "<input type=\"checkbox\" checked disabled>";
										} else {
											if (($currentgroupelement != $row[$usecorrectval[1]]) && ($number == 0) && ($fieldtogroup != "")) {
												echo $row[$usecorrectval[1]]." ( ".$grouparray[$row[$usecorrectval[1]]]." )";
											} elseif (($number != 0) && ($fieldtogroup != "")) {
												if ($number == 1) {
													echo "<a href='".$_SERVER['PHP_SELF']."?id=2:".$row['hashid'].":::::::::::::::::::".$primary_tabid.":".$primary_mod.":2::'>".$row[$usecorrectval[1]]."</a>";
												} else {
													echo $row[$usecorrectval[1]];
												}
											} elseif (($number == 0)  && ($fieldtogroup == "")) {
												echo "<a href='".$_SERVER['PHP_SELF']."?id=2:".$row['hashid'].":::::::::::::::::::".$primary_tabid.":".$primary_mod.":2::'>".$row[$usecorrectval[1]]."</a>";
											} elseif (($number != 0)  && ($fieldtogroup == "")) {
												echo $row[$usecorrectval[1]];
											}
										} 
										if ($number == 0) {
											if (array_key_exists($row[$usecorrectval[1]],$grouparray)) {
												//echo "( ".$grouparray[$row[$usecorrectval[1]]]." )";
												$currentgroupelement = $row[$usecorrectval[1]];
											}
										} ?>&nbsp;
									</td>
					<?php 		}
								echo "</tr>";
							} 
						} ?>
					</table>
					<table style="font-size:14px" width="100%" border="0" cellspacing="2" cellpadding="1">
					<tr>
						<td colspan="6"><?php include("includes/pages_next_prev.php"); ?></td>
					</tr>
					</table>
				<?php 
				} ?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table><br>