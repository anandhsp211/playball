<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<?php 
$serverid = "localhost";
$dbusername = "patrick";
$dbpassword = "cdBHBHNEHwQTr25J";
$databasename = "uk2sales";

$db = mysql_connect($serverid,$dbusername,$dbpassword) or die("Unable to access the database!");
mysql_select_db($databasename);
?>

<html>
<head>
	<title>Revenue Dashboard</title>
</head>

<body>

<?php 

// Need to create an array of usernames
$sql = "SELECT id, username, firstname, lastname, lastyear, startyear FROM users WHERE ismeasured = 1 AND registrantid = 1 ORDER BY firstname ASC";
//echo $sql;
$res = mysql_query($sql);
$arrCounter = 0;
while ($row = mysql_fetch_array($res)) { 
	//echo $row['lastyear']." - ".$currentyear."<br>";
	if ( ($row['lastyear'] == NULL) && ($gval[5] == "") ) {
		$userData[$arrCounter][1] = $row['id'];
		$userData[$arrCounter][2] = $row['firstname'];
		$arrCounter++;
	} elseif (( ($gval[5] != "") && ($row['lastyear'] >= $gval['5'])) || ($row['lastyear'] == NULL) && ($row['startyear'] <= $gval['5']) ) {
		$userData[$arrCounter][1] = $row['id'];
		$userData[$arrCounter][2] = $row['firstname'];
		$arrCounter++;
	}
} 
$num_users = count($userData);
$currentyear = date('Y');
$currentmonth = date('m');
$val_count = 1;
$loop_count = 1;
include_once("..//FusionChartsFree/Code/PHP/Includes/FC_Colors.php");
$graph_data = "<graph animation=\"0\" caption=\"Individual Sales Revenue\" baseFont=\"Verdana\" baseFontSize=\"30\" hovercapbg=\"FFECAA\" hovercapborder=\"F47E00\" formatNumberScale=\"0\" decimalPrecision=\"0\" showvalues=\"0\" numdivlines=\"3\" numVdivlines=\"3\" yaxisminvalue=\"1000\" yaxismaxvalue=\"1800\" rotateNames=\"0\">\n";
//$graph_data = "<graph xaxisname=\"Continent\" yaxisname=\"Export\" hovercapbg=\"DEDEBE\" hovercapborder=\"889E6D\" rotateNames=\"0\" yAxisMaxValue=\"100\" numdivlines="9" divLineColor="CCCCCC" divLineAlpha="80" decimalPrecision="0" showAlternateHGridColor=\"1\" AlternateHGridAlpha=\"30\" AlternateHGridColor=\"CCCCCC\" caption=\"Global Export\" subcaption=\"In Millions Tonnes per annum pr Hectare\">";
$graph_data .= "<categories>\n";
for ($x = 1; $x <= $currentmonth; $x++) { 
	$graph_data .= "<category name='".$x."' />\n";
}
$graph_data .= "</categories>\n";
for ($y = 1; $y <= $num_users; $y++) {
	$get_color = getFCColor();
	for ($x = 1; $x <= $currentmonth; $x++) { 
		$days_in_month = cal_days_in_month(CAL_GREGORIAN, date('m',$x), date('Y'));
		$sql = "SELECT SUM((s1.salesprice*quantity)-(s1.salesprice*quantity) * ((discount/100))) AS DealValue
				FROM udf_9BF31C7FF062936A96D3C8BD1F8F2FF3 udf 
				JOIN users u ON udf.createdby = u.id
				INNER JOIN sales_1 s1 ON udf.hashid = s1.recordid
				WHERE udf.tlsrecidinvoiceddate BETWEEN '".$currentyear."-".$addzero.$x."-01' AND '".$currentyear."-".$addzero.$x."-".$days_in_month."' 
				AND udf.tlsrecidinvoiced = 1
				AND u.id = ".$userData[$y-1][1]."
				AND u.registrantid = 1
				GROUP BY u.id";  
				// echo $sql."<br>";
		$res = mysql_query($sql);
		$row = mysql_fetch_array($res);
		
		if ( ($val_count == 1) && ($loop_count == 1) ) {
			$graph_data .= "<dataset seriesName=\"".$userData[$y-1][2]."\" color=\"".$get_color."\" >\n";
			$revenue = 0;
		} elseif ( ($val_count == 1) && ($loop_count != 1) ) {
			$graph_data .= "</dataset>\n";
			$graph_data .= "<dataset seriesName=\"".$userData[$y-1][2]."\" color=\"".$get_color."\" >\n";
			$revenue = 0;
		} elseif ($val_count == $currentmonth) {
			$val_count = 0;
		}
		
		//echo $userData[$y-1][1]." - ".$row['DealValue']."<=<br>";
		$revenue = $revenue + $row['DealValue'];
		$graph_data .= "<set value=\"".$revenue."\" />\n";
		
		$val_count++;
	}
	$loop_count++;
}
$graph_data .= "</dataset>\n</graph>";
unlink("chart_data_bar.xml");
$filePointerindex = fopen("chart_data_bar.xml","a+");
fputs($filePointerindex,$graph_data);
fclose($filePointerindex);
chmod("chart_data.xml", 0777);
sleep(1);  ?>
<br><br>
<table width="98%" border="0" cellspacing="0" cellpadding="3" align="center">
  <tr> 
    <td valign="top" class="text" align="center"> <div id="chartdiv" align="center"> 
        FusionCharts. </div>
		<SCRIPT LANGUAGE="Javascript" SRC="/FusionChartsFree/JSClass/FusionCharts.js"></SCRIPT>
      	<script type="text/javascript">
		   var chart = new FusionCharts("/FusionChartsFree/Charts/FCF_MSLine.swf", "ChartId", "1200", "650");
		   chart.setDataURL("chart_data.xml");		   
		   chart.render("chartdiv");
		</script> </td>
  </tr>
  <tr>
    <td valign="top" class="text" align="center">&nbsp;</td>
  </tr>
</table>

</body>
</html>
