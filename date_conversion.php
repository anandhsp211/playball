<?php
include_once("_globalconnect.php");
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Mon, 26 Jul 2050 05:00:00 GMT");
$date_Status = array();
if (isset($_POST['fieldtype']) && isset($_POST['value'])) {
    if($_POST['fieldtype'] == 8){
        $newDate = date("Y-m-d -Y:s", strtotime($_POST['value']));
        $date_Status['value'] = $newDate;
        echo json_encode($date_Status);
    }else if($_POST['fieldtype'] == 9){
        $newDate = date("Y-m-d h:i:s", strtotime($_POST['value']));
        $date_Status['value'] = $newDate;
        echo json_encode($date_Status);
    }
}
?>