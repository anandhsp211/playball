<?php 
include_once("_globalconnect.php");

if ($_POST['save'] == "Save Change") {
    
	if ((isset($_POST['excludestart'])) || (isset($_POST['excludeend'])) ) {
		
		// Lets set the cookie values for all the exclude dates
		$fieldvalue = ($_POST['fieldnametype'] == "excludestart") ? "excludestart" : "excludeend";
		foreach ($_SESSION['sessionmanager'] AS $record=>$recvals) {
			$_SESSION['sessionmanager'][$record][$fieldvalue] = date("d-M-Y",strtotime($_POST[$fieldvalue]));
		}
		
	} else {
	
		$rectype1 = ($_GET['recs'] == "hashid") ? "hashid" : "recordid";
		$resType1 = ($_GET['recs'] == "hashid") ? $_SESSION['hashidArr'] : $_SESSION['recstoupdate'];
		
		$addsql1 = "(";
		foreach ($resType1 AS $recordid) {
			$addsql1 .= " ".$rectype1." = '".mysql_real_escape_string($recordid)."' OR ";
		}
		$addsql1 = substr($addsql1,0,-4);
		$addsql1 .= ")";
		
		$column1 = columnnames(mysql_real_escape_string($_GET['col']));
		
		if ($_GET['type'] == 8) { // A date field
			$startingmonth = explode("-",substr($_POST[$column1."_1"],0,11));
			$insertval = $startingmonth[2]."-".convertDate($startingmonth[1])."-".$startingmonth[0];
		} elseif ($rowudf['fieldtype'] == 9) { // A date & time {
			$startingmonth = explode("-",substr($insertval,0,11));
			$startingtime = substr($_POST[$column1."_1"],-5,5);
			$insertval = $startingmonth[2]."-".convertDate($startingmonth[1])."-".$startingmonth[0]." ".$startingtime.":00";
		} else {
			$insertval = $_POST[$column1."_1"];
		}
		
		$sqlzz = "UPDATE `udf_".$_SESSION['locationhash']."`
				  SET ".$column1." = '".mysql_real_escape_string($insertval)."'
				  WHERE ".$addsql1;
		//echo $sql."<br>";
		mysql_query($sqlzz) or die ("Cannot update number series, notify support");
		
	}
    
	$onload = 'onload="window.parent.location = window.parent.location;self.close();return false;"';
	echo "<html>";
	echo "<head></head>";
	echo '<body onload="window.parent.location = window.parent.location;self.close();return false;"></body>';
	echo "</body>";
	echo "</html>";
	exit();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "includes/xhtml1-transitional.dtd">
<html>
<head>
	<title>Bulk Update</title>
	<?php 
	if (ERES > 1024) { ?>
		<style type="text/css" media="all">@import "css/style.css";</style>
	<?php 
	} else { ?>
		<style type="text/css" media="all">@import "css/style_small.css";</style>
	<?php
	}?>
    <style type="text/css" media="all">@import "/css/calendar_pop.css";</style>
    <script type='text/javascript' src='/js/calendar_pop.js'></script>
    <script>
        function DataChange() {
            // Do nothing
        }
    </script>
</head>
<body <?php echo $onload ?>>
	<table cellspacing="0" cellpadding="0" border="0" width="97%">
	<tr>
		<td><img src="images/<?php echo $_SESSION['franchisedata']['logo'];?>" alt="<?php echo $_SESSION['franchisedata']['name'];?> Franchise Management App" / align="center" border="0"></td>
		<td align="right"></td>
	</tr>
	</table><br>
	<table cellspacing="0" cellpadding="1" border="0" width="100%">
	<tr>
		<td height="5" colspan="5"><img src="images/spacer.gif" height="5" width="1"></td>
	</tr>
	<tr>
		<td valign="top" align="right" rowspan="5" width="2%">&nbsp;</td>
		<td valign="top" height="5">
			<strong><font style="font-size:130%;font-family:Trebuchet MS">Bulk Field Update</font></strong>
		</td>
	</tr>
	<tr>
		<td valign="top"><br>
		<?php
		if ($_GET['col'] =="excludestart") {
			$coloutput = "Exclude Range Start";
		} elseif ($_GET['col'] =="excludeend") {
			$coloutput = "Exclude Range End";
		} else {
			$coloutput = $_GET['col'];
		}
		?>
			This tool will update the field value for ALL records for the column <strong><?php echo $coloutput;?></strong> in the selected record set.  You can change these column values as
            many times as you need.<br /><br /><strong>Important:</strong> Changes cannot be undone - however, you can select start Over / Cancel, which will delete the duplicated records and you can start the duplication process
            from the beginning.<br /><br />
		</td>
	</tr>
    <tr>
        <td>
            <table width="50%" cellpadding="15" style="background: #e8e8e8;">
                <tr>
                    <td >
                        <form method="POST">
                        <br />Enter a new value for <strong><?php echo $coloutput;?></strong>:<br />
                        <?php
                        include_once 'display-fields.php';
                        $counter = 1;
                        //echo displayFields($_GET['type'],$_GET['col'],'',columnnames($_GET['col']),$counter,'nohashid','nohashid','nolinkedid','nolocationhash','noobject_master_id');
                        echo displayFields($_GET['type'],$_GET['col'],$row[$c],columnnames($_GET['col']),$counter,$_GET['colhashid'],$row['hashid'],$column['linkedid'],$_SESSION['locationhash'],$column['object_master_id']); ?>
                        <br /><br /><input onclick="return confirm('Are you sure?  These changes cannot be undone.')" type="submit" name="save" value="Save Change" />
                        <input type="hidden" name="fieldname" value="<?php echo $_GET['type'];?>" />
                        </form>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
	</table>
</body>
</html>
