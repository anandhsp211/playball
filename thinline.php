<?php include_once("_globalconnect.php");

if ($_POST['default_icando'] == 'Set I Can Do It as Default') {
    $value = "on";
    setcookie("default_icando", $value, time() + 3600);  /* expire in 1 hour */
    $url = $url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    header("Location:" . $url);
} elseif ($_POST['default_icando'] == 'Disable I Can Do It as Default') {
    $value = "off";
    setcookie("default_icando", $value, time() + 3600);  /* expire in 1 hour */
    $url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    header("Location:" . $url);
}

// Manager to change a venue status
if ($_POST['changevenuestatus'] == "Change Venue Status") {
    $sql = "UPDATE `udf_5737034557EF5B8C02C0E46513B98F90` SET `status` = '" . mysql_real_escape_string($_POST['venuestatus']) . "'
				WHERE `hashid` = '" . mysql_real_escape_string($_POST['venuehashid']) . "' AND issaved =  1 LIMIT 1;";
    //echo $sql."<br />";
    mysql_query($sql);
    header("Location: /thinline.php?id=51::::::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A&venuemsg=success");
}

// This is the venue approval and rejection submission code
if (isset($_POST['approval']) && ($_POST['venuemanager'] == 1)) {

    if ($_POST['approvalnote'] != "") {
        $appNote = "<br /><br /><strong>Note from your Country Manager</strong><br />";
        $appNote .= $_POST['approvalnote'] . "<br />";
    }

    if ($_POST['approval'] == "Approved") {
        // Submission is approved, changed status and email FO
        $sql = "UPDATE `udf_5737034557EF5B8C02C0E46513B98F90` SET status = 'Approved', receivedconfirmation = 'No', approveddate = '" . date("Y-m-d") . "' WHERE hashid = '" . mysql_real_escape_string($_POST['hashid']) . "' AND issaved = 1 LIMIT 1";
        //echo $sql."<br />";
        mysql_query($sql);
        $venueDetails = $_SESSION['venuedetails'];
        //print_r($venueDetails);
        $message = "Your venue <strong>" . $venueDetails['venue'] . " (" . $venueDetails['venueid'] . ")</strong> has been approved.  You can now begin advertising classes at this venue.";
        $message .= "<br /><br />If this new venue is a school, you will need to provide written confirmation in the form of a letter on the school's letterhead paper of them granting your franchise permission to deliver Playball services at this school.";
        $message .= "<br /><br />This confirmation needs to be scanned or photographed and sent to head office within 60 days of today's date.  Failure to send this confirmation could result in your franchises permission to offer Playball services at this venue being revoked. ";
        $message .= "<br /><br />If you have any questions, please contact your Playball Country Manager.";
        $message .= $appNote;
        $message .= "<br /><br /><a href='http://backoffice.playballkids.com'>http://backoffice.playballkids.com</a>";
        $approveDenied = "approved";
        //echo $message;

    }

    if ($_POST['approval'] == "Reject") {

        if ($_POST['rejectionreason'] == "Duplicate Venue") {

            // Submission is approved, changed status and email FO
            $sql = "UPDATE `udf_5737034557EF5B8C02C0E46513B98F90` SET status = 'Not Approved - Duplicate Venue' WHERE hashid = '" . mysql_real_escape_string($_POST['hashid']) . "' AND issaved = 1 LIMIT 1";
            //echo $sql."<br />";
            mysql_query($sql);
            $venueDetails = $_SESSION['venuedetails'];
            //print_r($venueDetails);
            $message = "Your venue <strong>" . $venueDetails['venue'] . " (" . $venueDetails['venueid'] . ")</strong> has not been approved, because it is a duplicate venue already in use by another franchise owner.";
            $message .= "<br><br>If you wish to challenge this decision, please contact your Playball Country Manager.";
            $message .= $appNote;
            $message .= "<br /><br /><a href='http://backoffice.playballkids.com'>http://backoffice.playballkids.com</a>";
            $approveDenied = "denied";
            //echo $message;

        } elseif ($_POST['rejectionreason'] == "Exceeded Maximum Venues") {

            // Submission is approved, changed status and email FO
            $sql = "UPDATE `udf_5737034557EF5B8C02C0E46513B98F90` SET status = 'Not Approved - Exceeded Maximum Venues' WHERE hashid = '" . mysql_real_escape_string($_POST['hashid']) . "' AND issaved = 1 LIMIT 1";
            //echo $sql."<br />";
            mysql_query($sql);
            $venueDetails = $_SESSION['venuedetails'];
            //print_r($venueDetails);
            $message = "Your venue <strong>" . $venueDetails['venue'] . " (" . $venueDetails['venueid'] . ")</strong> has not been approved, because you will exceed the maximum number of venues allowed by your franchise.";
            $message .= "<br><br>If you wish to challenge this decision, please contact your Playball Country Manager.";
            $message .= $appNote;
            $message .= "<br /><br /><a href='http://backoffice.playballkids.com'>http://backoffice.playballkids.com</a>";
            $approveDenied = "denied";
            //echo $message;

        } elseif ($_POST['rejectionreason'] == "Other") {

            // Submission is approved, changed status and email FO
            $sql = "UPDATE `udf_5737034557EF5B8C02C0E46513B98F90` SET status = 'Not Approved - Other' WHERE hashid = '" . mysql_real_escape_string($_POST['hashid']) . "' AND issaved = 1 LIMIT 1";
            //echo $sql."<br />";
            mysql_query($sql);
            $venueDetails = $_SESSION['venuedetails'];
            //print_r($venueDetails);
            $message = "Your venue <strong>" . $venueDetails['venue'] . " (" . $venueDetails['venueid'] . ")</strong> has not been approved, because of the reason set out by the country manager in the notes below.";
            $message .= "<br><br>If you wish to challenge this decision, please contact your Playball Country Manager.";
            $message .= $appNote;
            $message .= "<br /><br /><a href='http://backoffice.playballkids.com'>http://backoffice.playballkids.com</a>";
            $approveDenied = "denied";
            //echo $message;

        }

    }

    require(dirname(__FILE__) . '/includes/email-server-details.php');
    $mail->addAddress($venueDetails['emailaddress']);
    $mail->Subject = 'Your venue submission has been ' . $approveDenied;
    $mail->setFrom("info@playballkids.com", "info@playballkids.com");
    $mail->addReplyTo("donotreply@playballkids.com");
    $mail->Body = $message;

    if (!$mail->Send()) {
        echo "Mailer Error: " . $mail->ErrorInfo;
    } else {
        echo "Message has been sent";
    }


    header('Location: /thinline.php?id=51:9:1::::::::::::::::::C0C7C76D30BD3DCAEFC96F40275BDC0A');
    exit;

}

// This is for the SMS broadcast tool
if ($_POST['action'] == "sendbroadcastmessage") {
    // Send the message
    $textmessage = $_POST['messagebody'];
    $msgremaining = intval($_SESSION['msgremaining']);
    $msgtosend = intval(count($_POST['sessionelement']));

    if ($msgremaining < $msgtosend) {
        //echo "In here<br>";
        $txterror = 1;
    }

    // Need to ckeck if there is a from text mobile number
    $sqltx = "SELECT fromtextmessagemobilenumber FROM `udf_33E8075E9970DE0CFEA955AFD4644BB2` WHERE issaved = 1 AND department = '" . mysql_real_escape_string($_SESSION['franchiseID']) . "' LIMIT 1;";
    //echo $sqltx;
    $restx = mysql_query($sqltx);
    $rowtx = mysql_fetch_array($restx);

    if ($rowtx['fromtextmessagemobilenumber'] != "") {
        $addtxtcode = "&from=" . $rowtx['fromtextmessagemobilenumber'];
    } else {
        $addtxtcode = "";
    }

    // echo "Msg remaining: ".$_SESSION['msgremaining'];
    // echo "Msg to send: ".count($_POST['sessionelement']);
    if ($txterror != 1) {
        foreach ($_POST['sessionelement'] AS $mobilenumber) {
            $detail = explode("||", $mobilenumber);
            $textmessage = str_replace("\r", " ", $textmessage);
            $textmessage = str_replace("\n", " ", $textmessage);
            $textmessage = str_replace(" ", "+", $textmessage);
            $textmessage = str_replace("++++++", "+", $textmessage);
            $textmessage = str_replace("+++++", "+", $textmessage);
            $textmessage = str_replace("++++", "+", $textmessage);
            $textmessage = str_replace("+++", "+", $textmessage);
            $textmessage = str_replace("++", "+", $textmessage);
            $textmessage = str_replace("&", "%26", $textmessage);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "http://api.clickatell.com/http/sendmsg?api_id=" . CTAPIID . "&user=" . CTUSER . "&password=" . CTPASS . "&to=" . $detail[1] . "&text=" . $textmessage . $addtxtcode);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $definition = curl_exec($ch);
            //echo $definition."<br />";
            curl_close($ch);

            //Let's store some view and popularity detail.
            $query = "INSERT INTO messaging (messagetype, registrantid, menutabid, userid, location, recordid, senttoaddress, messagebody, datecreated) VALUES (
		    'text',
		    '1',
		    'A87FF679A2F3E71D9181A67B7542122C',
		    " . UID . ",
		    '45C48CCE2E2D7FBDEA1AFC51C7C6AD26',
		    '" . $detail[0] . "',
		    '" . $detail[1] . "',
		    '" . mysql_real_escape_string(str_replace("+", " ", $textmessage)) . "',
		    '" . $sGMTMySqlString . "'
		    )";
            //echo $query;
            mysql_query($query) Or Die ("Cannot submit entry! 554fkf97");

            $query = "INSERT INTO text_msg_counter (userid, registrantid, department, datecreated) VALUES (
			      '" . UID . "',
			      '1',
			      '" . $_SESSION['securityarrdept'][0] . "',
			      '" . $sGMTMySqlString . "'
			      )";
            mysql_query($query) Or Die ("Cannot submit entry! ddsdsee453w");
        }
    }
}

// Start gval options
// For deleting records

if ($gval[25] == "CLEAR") {
    unset($_SESSION['sorrsaved1'], $_SESSION['sorrsaved2']);
}

if (($_SESSION['sorrsaved1'] != "") && ($_SESSION['sorrsaved2'] != "")) {
    header("Location:thinline.php?id=" . $_SESSION['sorrsaved1'] . ":" . $_SESSION['sorrsaved2'] . ":::::::::::::::::::" . $gval[20] . ":" . $gval[21] . ":" . $gval[22] . ":" . $gval[23] . ":" . $gval[24] . ":CLEAR:::::" . $gval[30] . ":" . $gval[31]);
}

// Lets save default columns
if ($_POST['setdefaultcols'] == 1) {

    if (!$_SESSION['sortcolumn_' . $gval[21]]) {
        $sql = "INSERT INTO userscolsort (`rid` ,`userid` ,`module`, `sortbycolumn` ,`sortcolby` )
				VALUES ('" . RID . "', '" . UID . "', '" . $gval[21] . "', '" . mysql_real_escape_string($_POST['sortcolumname']) . "', '" . mysql_real_escape_string($_POST['sortcolumnamex']) . "');";
        mysql_query($sql);
    } else {
        $sql = "UPDATE `userscolsort`
				SET `sortbycolumn` = '" . mysql_real_escape_string($_POST['sortcolumname']) . "',
				`userscolsort`.`sortcolby` = '" . mysql_real_escape_string($_POST['sortcolumnamex']) . "'
				WHERE `userscolsort`.`module` = '" . $gval['21'] . "'
				AND `userscolsort`.`rid` = '" . RID . "'
				AND `userscolsort`.`userid` = '" . UID . "'
				LIMIT 1 ;";
        //echo $sql;
        mysql_query($sql);
    }

    $_SESSION['sortcolumn_' . $gval[21]] = mysql_real_escape_string($_POST['sortcolumname']);
    $_SESSION['sortcolumnby_' . $gval[21]] = mysql_real_escape_string($_POST['sortcolumnamex']);
    $_SESSION['sortcolumnmod_' . $gval[21]] = $gval[21];
    header("Location:thinline.php?id=::::::::::::::::::" . $_POST['sortcolumnamex'] . ":" . $_POST['sortcolumname'] . ":" . $gval[20] . ":" . $gval[21] . "::::::::::");
}

if ($_POST['pform'] == 1) {
    // Do the table insert
    $tableforinsert = "udf_" . $gval[21];
    $queryudf = "INSERT INTO " . $tableforinsert . " (recordid, registrantid, department, createdby, createddate, " . insertstructure($_POST['udf_string']) . ") VALUES
				(" . $_POST['recordid'] . "," . RID . ",'" . $_SESSION['securityarrdeptstr'] . "'," . UID . ",'" . $sGMTMySqlString . "'," . insertdata($_POST['udf_string']) . ")";
    //echo $queryudf;
    mysql_query($queryudf) Or Die ("Cannot submit entry! 114");

    $querysetnumseries = "UPDATE menusub_tabs SET numberseries = '" . ($_POST['recordid'] + 1) . "' WHERE hashid = '" . $gval[21] . "' AND registrantid =" . RID;
    mysql_query($querysetnumseries) or die ("Cannot update number series, notify support");
//    echo $querysetnumseries;

    if ($_POST['gval30'] != "") {

        $sqlsethashid = "SELECT id FROM relations_matrix WHERE menutabid = '" . $gval[20] . "' AND childtable = '" . $gval[21] . "' AND parenttable = '" . $_POST['gval31'] . "' LIMIT 1";
        //echo $sqlsethashid."<br>";
        $ressethashid = mysql_query($sqlsethashid);
        $rowsethashid = mysql_fetch_array($ressethashid) Or Die ("Cannot set ID, please notify systems administrator with this code: SetHID: 1");

        $queryudf = "INSERT INTO relations_values (registrantid,menutabid,location,recordid,matrix_id,parent_recordid) VALUES (
				 " . RID . ",
				 '" . $gval[20] . "',
				 '" . $gval[21] . "',
				 '" . onlysetHashID($tableforinsert) . "',
				 '" . $rowsethashid['id'] . "',
				 '" . $_POST['gval30'] . "'
				)";
        //echo $queryudf;
        mysql_query($queryudf) Or Die ("Cannot submit entry! 11335");
    }

    header("Location:thinline.php?id=2:" . onlysetHashID($tableforinsert) . ":::::::::::::::::::" . $gval[20] . ":" . $gval[21] . ":" . $gval[22] . ":" . $gval[23] . ":" . $gval[24] . "&notify=new");

}

elseif ($_POST['pform'] == 2) {

    // Want to check if there are sub tabs fields which need to be updated.
    $sql = "SELECT rm.id AS id, rm.childtable AS childtable, udfd.label AS label FROM `relations_matrix` rm
	    INNER JOIN udf_definitions udfd ON udfd.hashid = rm.udf_columnid
	    WHERE `parenttable` = '" . $gval[21] . "'
	    AND ispredefined = 0 AND rm.isactive = 1";
    //echo $sql."<br>";
    $res = mysql_query($sql);
    while ($row = mysql_fetch_array($res)) {
        // Pull the old value before the update to see if it has changed.  If not do nothing
        $sqli = "SELECT " . columnnames($row['label']) . " AS newValue FROM udf_" . mysql_real_escape_string($gval[21]) . " WHERE hashid = '" . mysql_real_escape_string($gval[1]) . "' AND issaved = 1 LIMIT 1; ";
        $resi = mysql_query($sqli);
        $rowi = mysql_fetch_array($resi);
        $checkingValue = $rowi['newValue'];

//        updating fieldtype values

        if ($checkingValue != $_POST[columnnames($row['label'])]) {
            // Go and make the changes that need to be updated
            // First get the affected records
            $sqld = "SELECT recordid FROM relations_values
		     WHERE location = '" . $row['childtable'] . "'
		     AND matrix_id = " . $row['id'] . "
		     AND parent_recordid = '" . mysql_real_escape_string($gval[1]) . "'
		     AND registrantid =" . RID;
            //echo $sqld;
            $resd = mysql_query($sqld);
            while ($rowd = mysql_fetch_array($resd)) {
                // Build the update string
                $sqlAdd .= "hashid = '" . $rowd['recordid'] . "' OR ";
            }
            $sqlp = "UPDATE `udf_2BB232C0B13C774965EF8558F0FBD615`
			SET `" . columnnames($row['label']) . "` = '" . mysql_real_escape_string($_POST[columnnames($row['label'])]) . "'
		    WHERE " . substr($sqlAdd, 0, -3);
            mysql_query($sqlp);
            unset($sqlAdd, $checkingValue);
        }

    }

    // Do the table update
    $tableforinsert = "udf_" . $gval[21];
    $queryudf = "UPDATE " . $tableforinsert . " SET
			    lastupdatedby = " . UID . ",
			    lastupdateddate = '" . $sGMTMySqlString . "',
			    issaved = 1,
			    " . updatestructdata($_POST['udf_string']) . "
			    ";
//    echo $_POST['udf_string']."<br>";
//    echo $queryudf;
//    die();
    mysql_query($queryudf) Or Die ("Cannot submit entry! 115");

    // Do a quick check for priority guardians and update accordingly.
    if ($gval[21] == "45C48CCE2E2D7FBDEA1AFC51C7C6AD26") {
        $delpriority = "DELETE FROM guardian_priority WHERE guardian_id = '" . mysql_real_escape_string($gval[1]) . "' AND department = '" . $deptval . "' LIMIT 1";
        mysql_query($delpriority);
        if ($_POST['priority'] == "Yes") {
            // Then we add the record
            $sqlopt = "INSERT INTO `guardian_priority` (`department`, `guardian_id`) VALUES ('" . $deptval . "', '" . mysql_real_escape_string($gval[1]) . "');";
            mysql_query($sqlopt);
        }
    }

    include_once("audit_log.php");


    if ($_POST['gval30'] != "") {
        $sqlsethashid = "SELECT id FROM relations_matrix WHERE menutabid = '" . $gval[20] . "' AND childtable = '" . $gval[21] . "' AND parenttable = '" . $_POST['gval31'] . "' LIMIT 1";
        //echo $sqlsethashid."<br>";
        $ressethashid = mysql_query($sqlsethashid);
        $rowsethashid = mysql_fetch_array($ressethashid) Or Die ("Cannot set ID, please notify systems administrator with this code: SetHID: 1");

        $queryudf = "INSERT INTO relations_values (registrantid,menutabid,location,recordid,matrix_id,parent_recordid) VALUES (
			     " . RID . ",
			     '" . $gval[20] . "',
			     '" . $gval[21] . "',
			     '" . onlysetHashID($tableforinsert) . "',
			     '" . $rowsethashid['id'] . "',
			     '" . $_POST['gval30'] . "'
			    )";
        // echo $queryudf;
        mysql_query($queryudf) Or Die ("Cannot submit entry! 11335");
    }

    /*
    if (isset($_POST['ppid'])) {
	    // Is a referrer record.  Need to update
	    $ppid = mysql_real_escape_string($_POST['ppid']);

	    if ($_POST['status'] == "Won") {
		    $addsqlwin = " `closedate` = '".date("Y-m-d")."',";
	    } else {
		    $addsqlwin = " `closedate` = '',";
	    }

	    $sql = "UPDATE `viweb`.`p_leads` SET ".$addsqlwin."
	    `status` =  '".$_POST['status']."', `quotevalue` =  '".$_POST['leadvalue']."' WHERE  `p_leads`.`id` = ".$ppid." LIMIT 1 ;";
	    //echo $sql;
	    mysql_query($sql) Or Die ("Cannot update referral information! Please let admin know");
    }*/

    //echo "In here.";

    header("Location: thinline.php?id=2:" . $_POST['hiddenhashid'] . ":" . $gval[2] . ":" . $gval[3] . ":" . $gval[4] . ":" . $gval[5] . ":" . $gval[6] . ":" . $gval[7] . ":" . $gval8 . ":" . $gval9 . ":" . $gval[10] . ":" . $gval[11] . ":" . $gval[12] . ":" . $gval[13] . ":" . $gval[14] . ":" . $gval[15] . ":" . $gval[16] . ":" . $gval[17] . ":" . $gval[18] . ":" . $gval[19] . ":" . $gval[20] . ":" . $gval[21] . ":" . $gval[22] . ":" . $gval[23] . ":" . $gval[24] . "&notify=1");

}

elseif ($_POST['pform'] == 3)
{
    // They are invoicing but we'll save the for
    // Here we are updating any sales/purchase line items
    for ($num = 0; $num < ($_POST['totalsalestabrows'] + 1); $num++) {
        if ($_POST['componentabtype'] == 1) {
            $table_s_name = "sales_";
        } elseif ($_POST['componentabtype'] == 2) {
            $table_s_name = "purchases_";
            $addreceived = "received = '" . $_POST['received_' . $num . ''] . "',";
        }
        $queryudf = "UPDATE " . $table_s_name . RID . " SET
					productcode = '" . $_POST['productcode_' . $num . ''] . "',
					productname = '" . $_POST['productname_' . $num . ''] . "',
					accountcode = '" . $_POST['accountcode_' . $num . ''] . "',
					taxcode = '" . $_POST['taxcode_' . $num . ''] . "',
					" . $addreceived . "
					quantity = '" . $_POST['quantity_' . $num . ''] . "',
					discount = '" . $_POST['discount_' . $num . ''] . "',
					salesprice = '" . $_POST['salesprice_' . $num . ''] . "'
					WHERE md5(id) = '" . $_POST['salestabrowid_' . $num] . "'
					";
        // echo $queryudf."<br><br>";
        mysql_query($queryudf) Or Die ("Cannot submit entry! 119");
    }
    if ($_POST['processorder_2'] == "Order Completed") {
        if ($_POST['componentabtype'] == 2) {
            //$addreceived = "supplierinvoiceamountexvat = '".$_POST['supplierinvoiceamountexvat']."',";
            //$addreceived .= "supplierinvoiceamountincvat = '".$_POST['supplierinvoiceamountincvat']."',";
            // add this to udf below: ".$addreceived."
        }
        //// Lets break down the date into a usable format.
        $invoice = explode("-", substr($_POST['tlsrecidinvoiceddate'], 0, 11));
        $invoicefinal = $invoice[2] . "-" . convertDate($invoice[1]) . "-" . $invoice[0];

        $againstperiod = $invoice[2] . "-" . convertDate($invoice[1]);
        $comparetoday = date("Y-m");
        //echo strtotime($comparetoday)."-".strtotime($invoicefinal) ;

        if ((strtotime($againstperiod) < strtotime($comparetoday)) && (ISMASTER != 1)) {
            $periodclosed = "&period=closed";
        } else {
            $queryudf = "UPDATE udf_" . $gval[21] . " SET
					tlsrecidinvoiced  = 1,
					lastupdatedby = " . UID . ",
					tlsrecidinvoiceddate = '" . $invoicefinal . "',
					tlsinvoiceterms = '" . $_POST['termsdays'] . "',

					lastupdateddate = '" . $sGMTMySqlString . "'
					WHERE hashid = '" . $gval[1] . "'
					";
            //echo $queryudf;
            mysql_query($queryudf) Or Die ("Cannot submit entry! 120");
        }
    } else {
        // Just an update
        // Here we are updating any sales/purchase line items
        for ($num = 0; $num < ($_POST['totalsalestabrows'] + 1); $num++) {
            if ($_POST['componentabtype'] == 1) {
                $table_s_name = "sales_";
            } elseif ($_POST['componentabtype'] == 2) {
                $table_s_name = "purchases_";
                $addreceived = "received = '" . $_POST['received_' . $num . ''] . "',";
            }
            $queryudf = "UPDATE " . $table_s_name . RID . " SET
					productcode = '" . $_POST['productcode_' . $num . ''] . "',
					productname = '" . $_POST['productname_' . $num . ''] . "',
					accountcode = '" . $_POST['accountcode_' . $num . ''] . "',
					taxcode = '" . $_POST['taxcode_' . $num . ''] . "',
					" . $addreceived . "
					quantity = '" . $_POST['quantity_' . $num . ''] . "',
					discount = '" . $_POST['discount_' . $num . ''] . "',
					salesprice = '" . $_POST['salesprice_' . $num . ''] . "'
					WHERE md5(id) = '" . $_POST['salestabrowid_' . $num] . "'
					";
            // echo $queryudf."<br><br>";
            mysql_query($queryudf) Or Die ("Cannot submit entry! 119");
        }
    }
    header("Location:thinline.php?id=" . $gval[0] . ":" . $gval[1] . ":" . $gval[2] . ":" . $gval[3] . ":" . $gval[4] . ":" . $gval[5] . ":" . $gval[6] . ":" . $gval[7] . ":" . $gval8 . ":" . $gval9 . ":" . $gval[10] . ":" . $gval[11] . ":" . $gval[12] . ":" . $gval[13] . ":" . $gval[14] . ":" . $gval[15] . ":" . $gval[16] . ":" . $gval[17] . ":" . $gval[18] . ":" . $gval[19] . ":" . $gval[20] . ":" . $gval[21] . ":" . $gval[22] . ":" . $gval[23] . ":" . $gval[24] . $periodclosed . "");
}

elseif ($_POST['pform'] == 4) {
    // Confirm the resource booking
    $queryconfbooking = "SELECT * FROM resource_allocation WHERE location = '" . $gval[21] . "' AND recordid  = '" . $gval[1] . "'";
    // echo $queryconfbooking."<br>";
    $resconfbooking = mysql_query($queryconfbooking);
    while ($rowconfbooking = mysql_fetch_array($resconfbooking)) {
        // echo $rowconfbooking['id']."<br>";
        $queryudf = "UPDATE resource_allocation SET
					isconfirmed = 1
					WHERE id = " . $rowconfbooking['id'] . "
					";
        // echo $queryudf."<br><br>";
        mysql_query($queryudf) Or Die ("Cannot submit entry! 119");

        // Let's start calcultating some totals
        $diff = strtotime($rowconfbooking['todate']) - strtotime($rowconfbooking['fromdate']);
        $chargeforeach = $rowconfbooking['chargeforeach'];
        if ($chargeforeach == "Hour") {
            $subtotal = (($diff / 60) / 60) * $rowconfbooking['rate'];
        } elseif ($chargeforeach == "Night") {
            $totalnights = round(($diff / 60) / 60 / 24) - 1;
            $subtotal = ($totalnights * $rowconfbooking['rate']);
        } elseif ($chargeforeach == "Night and Person") {
            $totalnights = round(($diff / 60) / 60 / 24) - 1;
            $subtotal = ($totalnights * $rowconfbooking['rate']) * $rowconfbooking['heads'];
        } elseif ($chargeforeach == "Day") {
            $totalnights = round(($diff / 60) / 60 / 24);
            $subtotal = ($totalnights * $rowconfbooking['rate']);
        } elseif ($chargeforeach == "Day and Person") {
            $totalnights = round(($diff / 60) / 60 / 24);
            $subtotal = ($totalnights * $rowconfbooking['rate']) * $rowconfbooking['heads'];
        } elseif ($chargeforeach == "Month") {
            $totalnights = round(($diff / 60) / 60 / 24 / 30);
            if ($totalnights < 1) {
                $totalnights = 1;
            }
            $subtotal = ($totalnights * $rowconfbooking['rate']);
        } elseif ($chargeforeach == "Month and Person") {
            $totalnights = round(($diff / 60) / 60 / 24 / 30);
            if ($totalnights < 1) {
                $totalnights = 1;
            }
            $subtotal = ($totalnights * $rowconfbooking['rate']) * $row['heads'];
        } elseif ($chargeforeach == "Person") {
            $subtotal = $rowconfbooking['rate'] * $rowconfbooking['heads'];
        } elseif ($chargeforeach == "Week") {
            $totalnights = round(($diff / 60) / 60 / 24 / 7);
            $subtotal = ($totalnights * $rowconfbooking['rate']);
        } elseif ($chargeforeach == "Week and Person") {
            $totalnights = round(($diff / 60) / 60 / 24 / 7);
            $subtotal = ($totalnights * $rowconfbooking['rate']) * $rowconfbooking['heads'];
        }

        $grandtotalvalu = $grandtotalvalu + $subtotal;
    }

    $getaccountcode = "SELECT value FROM udf_multi_value WHERE udfdefid = '92CC227532D17E56E07902B254DFAD10' AND isdefault = 1 LIMIT 1";
    $resgetaccountcode = mysql_query($getaccountcode);
    $rowgetaccountcode = mysql_fetch_array($resgetaccountcode);

    $gettaxcode = "SELECT value FROM udf_multi_value WHERE udfdefid = '98DCE83DA57B0395E163467C9DAE521B' AND isdefault = 1 LIMIT 1";
    $resgettaxcode = mysql_query($gettaxcode);
    $rowgettaxcode = mysql_fetch_array($resgettaxcode);

    $queryudf = "INSERT INTO sales_1 (registrantid, location, recordid, productname, salesprice, discount, quantity, accountcode, taxcode, datecreated) VALUES
				(" . RID . ",'" . $gval[21] . "','" . $gval[1] . "','Resource Booking','" . $grandtotalvalu . "','0.00','1','" . $rowgetaccountcode['value'] . "','" . $rowgettaxcode['value'] . "','" . $sGMTMySqlString . "')";
    // echo $queryudf."<br><br>";
    mysql_query($queryudf) Or Die ("Cannot submit entry! 1n14");
    header("Location:thinline.php?id=" . $gval[0] . ":" . $gval[1] . ":" . $gval[2] . ":" . $gval[3] . ":" . $gval[4] . ":" . $gval[5] . ":" . $gval[6] . ":" . $gval[7] . ":" . $gval8 . ":" . $gval9 . ":" . $gval[10] . ":" . $gval[11] . ":" . $gval[12] . ":" . $gval[13] . ":" . $gval[14] . ":" . $gval[15] . ":" . $gval[16] . ":" . $gval[17] . ":" . $gval[18] . ":" . $gval[19] . ":" . $gval[20] . ":" . $gval[21] . ":" . $gval[22] . ":" . $gval[23] . ":" . $gval[24] . "");
}

if ($_POST['recactions'] == "bulkchange") {
    // Lets do duplicating bulk records
    $_SESSION['hashidArr'] = $_POST['recordlist'];
    $_SESSION['locationhash'] = $gval[21];
    $_SESSION['menuTab'] = $gval[20];


    header("Location: bulk-change.php?stage=1");
    exit();
}

if ($_POST['recactions'] == "duplicateselected") {
    // Lets do duplicating bulk records
    $_SESSION['hashidArr'] = $_POST['recordlist'];
    $_SESSION['locationhash'] = $gval[21];
    $_SESSION['menuTab'] = $gval[20];
    $_SESSION['returnURL'] = "thinline.php?id=::" . $gval[2] . ":" . $gval[3] . ":" . $gval[4] . "::" . $gval[6] . ":" . $gval[7] . ":" . $gval[8] . ":" . $gval[9] . ":" . $gval[10] . ":" . $gval[11] . ":" . $gval[12] . ":" . $gval[13] . ":" . $gval[14] . ":" . $gval[15] . ":" . $gval[16] . ":" . $gval[17] . ":" . $gval[18] . ":" . $gval[19] . ":" . $gval[20] . ":" . $gval[21] . ":1:" . $gval[23] . ":" . $gval[24];

    if ($gval[21] == "2B8A61594B1F4C4DB0902A8A395CED93") {
        header("Location: bulk-duplication.php?stage=1");
    } else {
        include(dirname(__FILE__) . '/includes/bulk-duplication.php');
    }

}

//var_dump($gval[0]);

if (($gval[0] == 1) || ($_POST['recactions'] == "deleteselected")) {

    function deleteRec($rec, $loc, $sGMTMySqlString, $rid, $uid)
    {
        $sql = "UPDATE udf_" . mysql_escape_string($loc) . " SET  `issaved` =  '-1',`deleteddate` = '" . $sGMTMySqlString . "', `deletedby` = '" . $uid . "'
			WHERE hashid = '" . mysql_escape_string($rec) . "' AND registrantid =" . mysql_escape_string($rid) . " LIMIT 1";
        //echo $sql;
        mysql_query($sql) Or Die ("Cannot delete entry!111s");

        // Also delete any relationship values
        $sql = "DELETE FROM relations_values WHERE recordid = '" . $rec . "' AND location = '" . $loc . "' AND registrantid =" . $rid;
        //echo $sql;
        mysql_query($sql) Or Die ("Cannot delete entry!111");

        // Also delete any relationship values
        $sql = "DELETE FROM record_history WHERE recordid = '" . $rec . "' AND location = '" . $loc . "'";
        // echo $sql;
        mysql_query($sql) Or Die ("Cannot delete entry!111");
    }

    if ($_POST['recactions'] == "deleteselected") {
        foreach ($_POST['recordlist'] AS $val) {
            deleteRec($val, $gval[21], $sGMTMySqlString, RID, UID);
        }
    } else {
        deleteRec($gval[1], $gval[21], $sGMTMySqlString, RID, UID);

        // IF WE DECIDE THAT DELETING THE RECORD IS BETTER PRACTISE.  ACTIVATE THIS CODE.
        /*// Lets delete a standard form record
		$sql = "DELETE FROM udf_".$gval[21]." WHERE hashid = '".$gval[1]."' AND registrantid =".RID." LIMIT 1" ;
		// echo $sql."<br><br>";
		mysql_query($sql) Or Die ("Cannot delete entry!111");*/
    }

    header("Location:thinline.php?id=::" . $gval[2] . ":" . $gval[3] . ":" . $gval[4] . "::" . $gval[6] . ":" . $gval[7] . ":" . $gval[8] . ":" . $gval[9] . ":" . $gval[10] . ":" . $gval[11] . ":" . $gval[12] . ":" . $gval[13] . ":" . $gval[14] . ":" . $gval[15] . ":" . $gval[16] . ":" . $gval[17] . ":" . $gval[18] . ":" . $gval[19] . ":" . $gval[20] . ":" . $gval[21] . ":1:" . $gval[23] . ":" . $gval[24] . "");

// Gval 2 is reserved fo calendar

}
elseif ($gval[0] == 3) {
    // Lets delete any documents
    $filename = $document_store_path . "/" . $gval[21] . "/" . $gval[1] . "/" . $gval[25];
    unlink($filename);
    $query = "DELETE FROM filemanager WHERE location ='" . $gval[21] . "' AND recordid = '" . $gval[1] . "' AND filename = '" . $gval[25] . "' AND registrantid =" . RID;
    mysql_query($query) Or Die ("Cannot delete entry5!");
    //echo "In here";
    header("Location:thinline.php?id=2:" . $gval[1] . ":" . $gval[2] . ":" . $gval[3] . ":" . $gval[4] . ":" . $gval[5] . "::" . $gval[7] . ":" . $gval8 . ":" . $gval9 . ":" . $gval[10] . ":" . $gval[11] . ":" . $gval[12] . ":" . $gval[13] . ":" . $gval[14] . ":" . $gval[15] . ":" . $gval[16] . ":" . $gval[17] . ":" . $gval[18] . ":" . $gval[19] . ":" . $gval[20] . ":" . $gval[21] . ":" . $gval[22] . ":" . $gval[23] . ":" . $gval[24] . "");
}
elseif ($gval[0] == 4) {
    // Lets process a quick complete for an activity for a company
    $queryaddress = "UPDATE activities SET activitystatus = 'Completed' WHERE hashid = '" . $gval[5] . "' AND registrantid=" . RID;
    // echo $queryaddress."<br>";
    mysql_query($queryaddress) Or Die ("Cannot submit entry!");
    header("Location:" . $_SERVER['PHP_SELF'] . "?id=2:" . $gval[1] . ":" . $gval[2] . ":" . $gval[3] . ":" . $gval[4] . "::" . $gval[6] . ":" . $gval[7] . ":" . $gval8 . ":" . $gval9 . ":" . $gval[10] . ":" . $gval[11] . ":" . $gval[12] . ":" . $gval[13] . ":" . $gval[14] . ":" . $gval[15] . ":" . $gval[16] . ":" . $gval[17] . ":" . $gval[18] . ":" . $gval[19] . ":" . $gval[20] . ":" . $gval[21] . ":" . $gval[22] . ":" . $gval[23] . ":" . $gval[24] . "");
}
elseif ($gval[0] == 5) {
    // Delete an address
    $query = "DELETE FROM address_info WHERE location ='" . $gval[21] . "' AND recordid = '" . $gval[1] . "' AND hashid = '" . $gval[5] . "' AND registrantid =" . RID;
    //echo $query;
    mysql_query($query) Or Die ("Cannot delete entry5!");
    header("Location:" . $_SERVER['PHP_SELF'] . "?id=2:" . $gval[1] . ":" . $gval[2] . ":" . $gval[3] . ":" . $gval[4] . "::" . $gval[6] . ":" . $gval[7] . ":" . $gval8 . ":" . $gval9 . ":" . $gval[10] . ":" . $gval[11] . ":" . $gval[12] . ":" . $gval[13] . ":" . $gval[14] . ":" . $gval[15] . ":" . $gval[16] . ":" . $gval[17] . ":" . $gval[18] . ":" . $gval[19] . ":" . $gval[20] . ":" . $gval[21] . ":" . $gval[22] . ":" . $gval[23] . ":" . $gval[24] . "");
}
elseif ($gval[0] == 6) {
    // Delete stored search values
    $query = "DELETE FROM list_results_arrays WHERE location ='" . $gval[21] . "' AND sessionid = '" . session_id() . "' LIMIT 1";
    //echo $query;
    mysql_query($query) Or Die ("Cannot delete entry 9!");
    // Delete stored search values
    $query = "DELETE FROM list_arrays_advanced WHERE sessionid ='" . session_id() . "' AND location ='" . $gval[21] . "'";
    //echo $query;
    unset($_SESSION['aq' . $gval[21] . ''], $_SESSION['aql' . $gval[21] . ''], $_SESSION['std' . $gval[21] . ''], $_SESSION['aqid' . $gval[21] . '']);
    setcookie('std' . $gval[21] . '', 0, time() - 360000);
    mysql_query($query) Or Die ("Cannot delete entry 11!");
    header("Location:" . $_SERVER['PHP_SELF'] . "?id=:" . $gval[1] . ":" . $gval[2] . ":" . $gval[3] . ":" . $gval[4] . "::" . $gval[6] . ":" . $gval[7] . ":" . $gval8 . ":" . $gval9 . ":" . $gval[10] . ":" . $gval[11] . ":" . $gval[12] . ":" . $gval[13] . ":" . $gval[14] . ":" . $gval[15] . ":" . $gval[16] . ":" . $gval[17] . ":" . $gval[18] . ":" . $gval[19] . ":" . $gval[20] . ":" . $gval[21] . ":" . $gval[22] . ":" . $gval[23] . ":" . $gval[24] . "");
}
elseif ($gval[0] == 7) {
    // Delete stored search values
    $query = "DELETE FROM messaging WHERE MD5(id) ='" . strtolower($gval[2]) . "' AND registrantid = " . RID . " LIMIT 1";
    // echo $query;
    mysql_query($query) Or Die ("Cannot delete entry 10!");
    header("Location:" . $_SERVER['PHP_SELF'] . "?id=2:" . $gval[1] . ":" . $gval[2] . ":" . $gval[3] . ":" . $gval[4] . "::" . $gval[6] . ":" . $gval[7] . ":" . $gval8 . ":" . $gval9 . ":" . $gval[10] . ":" . $gval[11] . ":" . $gval[12] . ":" . $gval[13] . ":" . $gval[14] . ":" . $gval[15] . ":" . $gval[16] . ":" . $gval[17] . ":" . $gval[18] . ":" . $gval[19] . ":" . $gval[20] . ":" . $gval[21] . ":" . $gval[22] . ":" . $gval[23] . ":" . $gval[24] . "");
}
elseif ($gval[0] == 8) {
    // Delete activity note
    $query = "DELETE FROM activities_notes WHERE MD5(id) ='" . strtolower($gval[2]) . "' AND registrantid = " . RID . " LIMIT 1";
    // echo $query;
    mysql_query($query) Or Die ("Cannot delete entry 10!");
    header("Location:" . $_SERVER['PHP_SELF'] . "?id=2:" . $gval[1] . ":" . $gval[2] . ":" . $gval[3] . ":" . $gval[4] . "::" . $gval[6] . ":" . $gval[7] . ":" . $gval8 . ":" . $gval9 . ":" . $gval[10] . ":" . $gval[11] . ":" . $gval[12] . ":" . $gval[13] . ":" . $gval[14] . ":" . $gval[15] . ":" . $gval[16] . ":" . $gval[17] . ":" . $gval[18] . ":" . $gval[19] . ":" . $gval[20] . ":" . $gval[21] . ":" . $gval[22] . ":" . $gval[23] . ":" . $gval[24] . "");
}
elseif ($gval[0] == 9) {
    if ($gval[2] == 1) {
        $table_s_name = "sales_";
    } elseif ($gval[2] == 2) {
        $table_s_name = "purchases_";
    }
    $query = "DELETE FROM " . $table_s_name . RID . " WHERE MD5(id) ='" . strtolower($gval[3]) . "' AND registrantid = " . RID . " LIMIT 1";
    // echo $query;
    mysql_query($query);
    header("Location:thinline.php?id=2:" . $gval[1] . ":" . $gval[2] . ":" . $gval[3] . ":" . $gval[4] . "::" . $gval[6] . ":" . $gval[7] . ":" . $gval8 . ":" . $gval9 . ":" . $gval[10] . ":" . $gval[11] . ":" . $gval[12] . ":" . $gval[13] . ":" . $gval[14] . ":" . $gval[15] . ":" . $gval[16] . ":" . $gval[17] . ":" . $gval[18] . ":" . $gval[19] . ":" . $gval[20] . ":" . $gval[21] . ":" . $gval[22] . ":" . $gval[23] . ":" . $gval[24] . "");
}
elseif ($gval[0] == 10) {
    if ($gval[2] == 1) {
        $table_s_name = "sales_";
    } elseif ($gval[2] == 2) {
        $table_s_name = "purchases_";
    }
    $query = "INSERT INTO " . $table_s_name . RID . " (registrantid, location, recordid, datecreated) VALUES (
						" . RID . ",
						'" . $gval[21] . "',
						'" . $gval[1] . "',
		  				'" . $sGMTMySqlString . "'
		  				)";
    // echo $query."<br><br>";
    mysql_query($query) Or Die ("Cannot submit entry!");
    header("Location:" . $_SERVER['PHP_SELF'] . "?id=2:" . $gval[1] . ":" . $gval[2] . ":" . $gval[3] . ":" . $gval[4] . "::" . $gval[6] . ":" . $gval[7] . ":" . $gval8 . ":" . $gval9 . ":" . $gval[10] . ":" . $gval[11] . ":" . $gval[12] . ":" . $gval[13] . ":" . $gval[14] . ":" . $gval[15] . ":" . $gval[16] . ":" . $gval[17] . ":" . $gval[18] . ":" . $gval[19] . ":" . $gval[20] . ":" . $gval[21] . ":" . $gval[22] . ":" . $gval[23] . ":" . $gval[24] . "");
}
elseif ($gval[0] == 11) {
    // Lets check if there is a crossover on some of the existing dates
    $querymsh = "SELECT validfrom, validto FROM pricing WHERE id ='" . $gval[25] . "' LIMIT 1";
    // echo $querymsh;
    $resmsh = mysql_query($querymsh);
    $rowmsh = mysql_fetch_array($resmsh);
    $validfromdate = $rowmsh['validfrom'];
    $validtodate = $rowmsh['validto'];


    // Lets check if there is a crossover on some of the existing dates
    $querymsh = "SELECT * FROM pricing WHERE location ='" . $gval[21] . "' AND recordid  = '" . $gval[1] . "' AND isactive = 1
				AND id != " . $gval[25] . " ORDER BY validfrom ASC";
    // echo $querymsh;
    $resmsh = mysql_query($querymsh);
    while ($rowmsh = mysql_fetch_array($resmsh)) {
        // echo "<br>Valid from: ".$validfromdate." - ".$rowmsh['validfrom'];
        // echo "<br>Valid to: ".$validtodate." - ".$rowmsh['validto'];
        if ((($validfromdate >= $rowmsh['validfrom']) && ($validtodate <= $rowmsh['validto']))
            || (($validfromdate <= $rowmsh['validto']) && ($validfromdate >= $rowmsh['validfrom']))
            || (($validtodate >= $rowmsh['validfrom']) && ($validtodate <= $rowmsh['validto']))
            || (($validfromdate <= $rowmsh['validfrom']) && ($validtodate >= $rowmsh['validto']))
        ) {
            // echo "<br>Match in range...";
            $activeprice = 1;
            break;
        }
    }

    if ($activeprice != 1) {

        $queryudf = "UPDATE pricing SET
					isactive = 1
					WHERE id = " . $gval[25] . "
					";
        // echo $queryudf;
        mysql_query($queryudf) Or Die ("Cannot submit entry! 1151");
    }

    header("Location:" . $_SERVER['PHP_SELF'] . "?id=2:" . $gval[1] . ":" . $gval[2] . ":" . $gval[3] . ":" . $gval[4] . "::" . $gval[6] . ":" . $gval[7] . ":" . $gval8 . ":" . $gval9 . ":" . $gval[10] . ":" . $gval[11] . ":" . $gval[12] . ":" . $gval[13] . ":" . $gval[14] . ":" . $gval[15] . ":" . $gval[16] . ":" . $gval[17] . ":" . $gval[18] . ":" . $gval[19] . ":" . $gval[20] . ":" . $gval[21] . ":" . $gval[22] . ":" . $gval[23] . ":" . $gval[24] . ":" . $activeprice . "");
}
elseif ($gval[0] == 12) {

    $queryudf = "UPDATE pricing SET
				isactive = 0
				WHERE id = " . $gval[25] . "
				";
    // echo $queryudf;
    mysql_query($queryudf) Or Die ("Cannot submit entry! 1151");
    header("Location:" . $_SERVER['PHP_SELF'] . "?id=2:" . $gval[1] . ":" . $gval[2] . ":" . $gval[3] . ":" . $gval[4] . "::" . $gval[6] . ":" . $gval[7] . ":" . $gval8 . ":" . $gval9 . ":" . $gval[10] . ":" . $gval[11] . ":" . $gval[12] . ":" . $gval[13] . ":" . $gval[14] . ":" . $gval[15] . ":" . $gval[16] . ":" . $gval[17] . ":" . $gval[18] . ":" . $gval[19] . ":" . $gval[20] . ":" . $gval[21] . ":" . $gval[22] . ":" . $gval[23] . ":" . $gval[24] . "#");
}
elseif ($gval[0] == 13) {
    $sql = "DELETE FROM pricing WHERE id = '" . $gval[25] . "' LIMIT 1";
    // echo $sql;
    mysql_query($sql) Or Die ("Cannot delete entry! 1131");
    header("Location:" . $_SERVER['PHP_SELF'] . "?id=2:" . $gval[1] . ":" . $gval[2] . ":" . $gval[3] . ":" . $gval[4] . "::" . $gval[6] . ":" . $gval[7] . ":" . $gval8 . ":" . $gval9 . ":" . $gval[10] . ":" . $gval[11] . ":" . $gval[12] . ":" . $gval[13] . ":" . $gval[14] . ":" . $gval[15] . ":" . $gval[16] . ":" . $gval[17] . ":" . $gval[18] . ":" . $gval[19] . ":" . $gval[20] . ":" . $gval[21] . ":" . $gval[22] . ":" . $gval[23] . ":" . $gval[24] . "#");
}
elseif ($gval[0] == 14) {
    // Lets get the from date and to date for this record:
    $querymsh = "SELECT fromdate, todate FROM udf_" . $gval[21] . " WHERE hashid ='" . $gval[1] . "' AND registrantid  = " . RID;
    //echo $querymsh."<br>";
    $resmsh = mysql_query($querymsh);
    $row = mysql_fetch_array($resmsh);
    $startdate = $row['fromdate'];
    $enddate = $row['todate'];
    // lets create a blank line for this item...
    if ($gval[25] == 0) {
        $chargetype = 0;
    } else {
        $chargetype = 1;
    }
    $queryudf = "INSERT INTO resource_allocation (createdby, resourceid, location, recordid, fromdate, todate, chargetype, datecreated) VALUES
				(" . UID . ",'undefined','" . $gval[21] . "','" . $gval[1] . "','" . $startdate . " 00:00:00','" . $enddate . " 23:59:59'," . $chargetype . ",'" . $sGMTMySqlString . "')";
    // echo $queryudf."<br>";
    mysql_query($queryudf) Or Die ("Cannot submit entry! 11s4");
    header("Location:" . $_SERVER['PHP_SELF'] . "?id=2:" . $gval[1] . ":" . $gval[2] . ":" . $gval[3] . ":" . $gval[4] . "::" . $gval[6] . ":" . $gval[7] . ":" . $gval8 . ":" . $gval9 . ":" . $gval[10] . ":" . $gval[11] . ":" . $gval[12] . ":" . $gval[13] . ":" . $gval[14] . ":" . $gval[15] . ":" . $gval[16] . ":" . $gval[17] . ":" . $gval[18] . ":" . $gval[19] . ":" . $gval[20] . ":" . $gval[21] . ":" . $gval[22] . ":" . $gval[23] . ":" . $gval[24] . "");
}
elseif ($gval[0] == 15) {
    $sql = "DELETE FROM resource_allocation WHERE id = '" . $gval[25] . "' LIMIT 1";
    // echo $sql;
    mysql_query($sql) Or Die ("Cannot delete entry! 1132");
    header("Location:" . $_SERVER['PHP_SELF'] . "?id=2:" . $gval[1] . ":" . $gval[2] . ":" . $gval[3] . ":" . $gval[4] . "::" . $gval[6] . ":" . $gval[7] . ":" . $gval8 . ":" . $gval9 . ":" . $gval[10] . ":" . $gval[11] . ":" . $gval[12] . ":" . $gval[13] . ":" . $gval[14] . ":" . $gval[15] . ":" . $gval[16] . ":" . $gval[17] . ":" . $gval[18] . ":" . $gval[19] . ":" . $gval[20] . ":" . $gval[21] . ":" . $gval[22] . ":" . $gval[23] . ":" . $gval[24] . "");
}
elseif ($gval[0] == 16) {
    // Delete note
    $query = "DELETE FROM notes WHERE MD5(id) ='" . strtolower($gval[2]) . "' AND registrantid = " . RID . " LIMIT 1";
    // echo $query;
    mysql_query($query) Or Die ("Cannot delete entry 10!");
    header("Location:" . $_SERVER['PHP_SELF'] . "?id=2:" . $gval[1] . ":" . $gval[2] . ":" . $gval[3] . ":" . $gval[4] . "::" . $gval[6] . ":" . $gval[7] . ":" . $gval8 . ":" . $gval9 . ":" . $gval[10] . ":" . $gval[11] . ":" . $gval[12] . ":" . $gval[13] . ":" . $gval[14] . ":" . $gval[15] . ":" . $gval[16] . ":" . $gval[17] . ":" . $gval[18] . ":" . $gval[19] . ":" . $gval[20] . ":" . $gval[21] . ":" . $gval[22] . ":" . $gval[23] . ":" . $gval[24] . "");
}
elseif ($gval[0] == 17) {
    // refund an item
    if ($gval[2] == 1) {
        $table_s_name = "sales_";
    } elseif ($gval[2] == 2) {
        $table_s_name = "purchases_";
    }
    $querymsh = "SELECT * FROM " . $table_s_name . RID . " WHERE MD5(id) ='" . strtolower($gval[3]) . "' AND registrantid = " . RID . " LIMIT 1";
    // echo $querymsh."<br>";
    $resmsh = mysql_query($querymsh);
    $row = mysql_fetch_array($resmsh);

    $newsales = -$row['salesprice'];

    $queryudf = "INSERT INTO " . $table_s_name . "1 (registrantid, location, recordid, productname, salesprice, discount, quantity, accountcode, taxcode, datecreated, is_refunded,isrefundeddate,refunded_id) VALUES
				(" . RID . ",'" . $gval[21] . "','" . $gval[1] . "','" . $row['productname'] . "','" . $newsales . "','" . $row['discount'] . "','" . $row['quantity'] . "','" . $row['accountcode'] . "','" . $row['taxcode'] . "','" . $sGMTMySqlString . "',1,'" . date("Y-m-d H:i", time()) . "','" . $row['id'] . "')";

    //echo $queryudf."<br><br>";
    mysql_query($queryudf) Or Die ("Cannot submit entry! 1n1dfgtrg4");

    $query = "UPDATE " . $table_s_name . RID . " SET is_refunded = 1, isrefundeddate = '" . date("Y-m-d H:i", time()) . "'  WHERE id = " . $row['id'] . " AND registrantid = " . RID . " LIMIT 1";
    // echo $query;
    mysql_query($query) Or Die ("Cannot update entry 1453j0!");
    header("Location:" . $_SERVER['PHP_SELF'] . "?id=2:" . $gval[1] . ":" . $gval[2] . ":" . $gval[3] . ":" . $gval[4] . "::" . $gval[6] . ":" . $gval[7] . ":" . $gval8 . ":" . $gval9 . ":" . $gval[10] . ":" . $gval[11] . ":" . $gval[12] . ":" . $gval[13] . ":" . $gval[14] . ":" . $gval[15] . ":" . $gval[16] . ":" . $gval[17] . ":" . $gval[18] . ":" . $gval[19] . ":" . $gval[20] . ":" . $gval[21] . ":" . $gval[22] . ":" . $gval[23] . ":" . $gval[24] . "");
}
elseif (($gval[0] == 18) || ($_POST['recactions'] == "permdeleteselected") || ($_POST['recactions'] == "restoreselected")) {
    function deleteRecPerm($rec, $loc, $sGMTMySqlString, $rid, $uid)
    {
        include("_dbaccessdetails.php");
        // IF WE DECIDE THAT DELETING THE RECORD IS BETTER PROACTISE.  ACTIVATE THIS CODE.
        // Lets delete a standard form record
        $sql = "DELETE FROM udf_" . mysql_escape_string($loc) . " WHERE hashid = '" . mysql_escape_string($rec) . "' AND registrantid =" . $rid . " LIMIT 1";
        // echo $sql."<br><br>";
        mysql_query($sql) Or Die ("Cannot delete entry!14ft11");

        // Also delete any relationship values
        $sqli = "DELETE FROM relations_values WHERE recordid = '" . mysql_escape_string($rec) . "' AND location = '" . mysql_escape_string($loc) . "' AND registrantid =" . $rid;
        // echo $sqli."<br>";
        mysql_query($sqli) Or Die ("Cannot delete entry!19rn11");

        // Also delete any relationship values
        $sqlj = "DELETE FROM record_history WHERE recordid = '" . mysql_escape_string($rec) . "' AND location = '" . mysql_escape_string($loc) . "'";
        // echo $sqlj;
        mysql_query($sqlj) Or Die ("Cannot delete entry!111d41");

        // Also delete any class sessions
        $sqlk = "DELETE FROM class_sessions WHERE parent_hashid = '" . mysql_escape_string($rec) . "' AND location = '" . mysql_escape_string($loc) . "'";
        // echo $sqlk;
        mysql_query($sqlk) Or Die ("Cannot delete entry!111d4dd1");

        // Also delete any multilink
        $sqll = "DELETE FROM multilink WHERE parent_record = '" . mysql_escape_string($rec) . "' AND parent_table = '" . mysql_escape_string($loc) . "'";
        // echo $sqll;
        mysql_query($sqll) Or Die ("Cannot delete entry!111d4es1");
        $sqlm = "DELETE FROM multilink WHERE child_record = '" . mysql_escape_string($rec) . "' AND child_table = '" . mysql_escape_string($loc) . "'";
        // echo $sqlm;
        mysql_query($sqlm) Or Die ("Cannot delete entry!111d4es1");
    }

    function RestoreRec($rec, $loc, $sGMTMySqlString, $rid, $uid)
    {
        include_once("_globalconnect.php");
        $sql = "UPDATE udf_" . mysql_escape_string($loc) . " SET  `issaved` =  '1'
			WHERE hashid = '" . mysql_escape_string($rec) . "' AND registrantid =" . mysql_escape_string($rid) . " LIMIT 1";
        //echo $sql;
        mysql_query($sql) Or Die ("Cannot restore entry! RestoreCode - 1235");
    }

    if ($_POST['recactions'] == "permdeleteselected") {
        foreach ($_POST['recordlist'] AS $val) {
            deleteRecPerm($val, $gval[21], $sGMTMySqlString, RID, UID);
        }
    } elseif ($_POST['recactions'] == "restoreselected") {
        foreach ($_POST['recordlist'] AS $val) {
            RestoreRec($val, $gval[21], $sGMTMySqlString, RID, UID);
        }
    } else {
        deleteRecPerm($gval[1], $gval[21], $sGMTMySqlString, RID, UID);
    }
    //header("Location:thinline.php?id=::".$gval[2].":".$gval[3].":".$gval[4]."::".$gval[6].":".$gval[7].":".$gval[8].":".$gval[9].":".$gval[10].":".$gval[11].":".$gval[12].":".$gval[13].":".$gval[14].":".$gval[15].":".$gval[16].":".$gval[17].":".$gval[18].":".$gval[19].":".$gval[20].":".$gval[21].":4:".$gval[23].":".$gval[24]."");

}
elseif ($gval[0] == 19) {
    // Set a record to private
    $sql = "UPDATE udf_" . mysql_escape_string($gval[21]) . " SET  `tlsprivate` =  '1'
		WHERE hashid = '" . mysql_escape_string($gval[1]) . "' AND registrantid =" . mysql_escape_string(RID) . " LIMIT 1";
    //echo $sql;
    mysql_query($sql) Or Die ("Cannot restore entry! RestoreCode - 1235");
    header("Location:thinline.php?id=2:" . $gval[1] . ":" . $gval[2] . ":" . $gval[3] . ":" . $gval[4] . "::" . $gval[6] . ":" . $gval[7] . ":" . $gval8 . ":" . $gval9 . ":" . $gval[10] . ":" . $gval[11] . ":" . $gval[12] . ":" . $gval[13] . ":" . $gval[14] . ":" . $gval[15] . ":" . $gval[16] . ":" . $gval[17] . ":" . $gval[18] . ":" . $gval[19] . ":" . $gval[20] . ":" . $gval[21] . ":" . $gval[22] . ":" . $gval[23] . ":" . $gval[24] . "");
}
elseif ($gval[0] == 20) {
    // Set a record to private
    $sql = "UPDATE udf_" . mysql_escape_string($gval[21]) . " SET  `tlsprivate` =  '0'
		WHERE hashid = '" . mysql_escape_string($gval[1]) . "' AND registrantid =" . mysql_escape_string(RID) . " LIMIT 1";
    //echo $sql;
    mysql_query($sql) Or Die ("Cannot restore entry! RestoreCode - 1235");
    header("Location:thinline.php?id=2:" . $gval[1] . ":" . $gval[2] . ":" . $gval[3] . ":" . $gval[4] . "::" . $gval[6] . ":" . $gval[7] . ":" . $gval8 . ":" . $gval9 . ":" . $gval[10] . ":" . $gval[11] . ":" . $gval[12] . ":" . $gval[13] . ":" . $gval[14] . ":" . $gval[15] . ":" . $gval[16] . ":" . $gval[17] . ":" . $gval[18] . ":" . $gval[19] . ":" . $gval[20] . ":" . $gval[21] . ":" . $gval[22] . ":" . $gval[23] . ":" . $gval[24] . "");
}
elseif ($gval[0] == 21) {
    // Remove a multilink record association
    $sql = "DELETE FROM multilink
		WHERE `parent_table` = '" . $gval[21] . "' AND `parent_record` = '" . $gval[1] . "'
		AND `child_table` = '" . $gval[26] . "' AND `child_record` = '" . $gval[27] . "' LIMIT 1";
    //echo $sql;
    mysql_query($sql) Or Die ("Cannot multilink entry! ErrorCode - 55877485");

    // Remove mirror link
    $sql = "DELETE FROM multilink
		WHERE `parent_table` = '" . $gval[26] . "' AND `parent_record` = '" . $gval[27] . "'
		AND `child_table` = '" . $gval[21] . "' AND `child_record` = '" . $gval[1] . "' LIMIT 1";
    //echo $sql;
    mysql_query($sql) Or Die ("Cannot multilink entry! ErrorCode - 55877485");
    header("Location:thinline.php?id=2:" . $gval[1] . ":" . $gval[2] . ":" . $gval[3] . ":" . $gval[4] . "::" . $gval[6] . ":" . $gval[7] . ":" . $gval8 . ":" . $gval9 . ":" . $gval[10] . ":" . $gval[11] . ":" . $gval[12] . ":" . $gval[13] . ":" . $gval[14] . ":" . $gval[15] . ":" . $gval[16] . ":" . $gval[17] . ":" . $gval[18] . ":" . $gval[19] . ":" . $gval[20] . ":" . $gval[21] . ":" . $gval[22] . ":" . $gval[23] . ":" . $gval[24] . "");

// 27 is reserved for calendar


}
elseif ($gval[0] == 46) {

    $query = "DELETE FROM activities_reminders WHERE activityid = '" . $gval[1] . "'";
    //echo $query;
    mysql_query($query);

    $query = "DELETE FROM activities_notes WHERE activityhashid = '" . $gval[1] . "' AND registrantid = " . RID;
    //echo $query;
    mysql_query($query);

    $query = "DELETE FROM activity_relations WHERE activityhashid = '" . $gval[1] . "' AND registrantid = " . RID;
    //echo $query;
    mysql_query($query);

    $query = "DELETE FROM activities WHERE hashid  = '" . $gval[1] . "' AND userid = " . UID;
    //echo $query;
    mysql_query($query);

    header("Location: " . $_SERVER['PHP_SELF'] . "?id=8::1");
}
elseif ($gval[0] == 47) {
    $query = "DELETE FROM activity_relations WHERE recordid = '" . $gval[1] . "' AND menutabid = '" . $gval[21] . "' AND location = '" . $gval[22] . "' AND registrantid = " . RID;
    // echo $query;
    mysql_query($query);
    header("Location: " . $_SERVER['PHP_SELF'] . "?id=::1:::" . $gval[5] . "");
}
elseif ($gval[0] == 48) {
    // Delete activity note
    $query = "DELETE FROM activities_notes WHERE MD5(id) ='" . strtolower($gval[2]) . "' AND registrantid = " . RID . " LIMIT 1";
    // echo $query;
    mysql_query($query) Or Die ("Cannot delete entry 10!");
    header("Location:" . $_SERVER['PHP_SELF'] . "?id=9:" . $gval[1] . "::::" . $gval[5] . ":" . $gval[6] . ":" . $gval[7] . ":" . $gval8 . ":" . $gval9 . ":" . $gval[10] . ":" . $gval[11] . ":" . $gval[12] . ":" . $gval[13] . ":" . $gval[14] . ":" . $gval[15] . ":" . $gval[16] . ":" . $gval[17] . ":" . $gval[18] . ":" . $gval[19] . ":" . $gval[20] . ":" . $gval[21] . ":" . $gval[22] . ":" . $gval[23] . ":" . $gval[24] . "");
}
elseif ($gval[0] == 49) {
    // Duplicate a standard record
    $dbHashidEscaped = mysql_real_escape_string($gval[21]);

    $sql = "CREATE TEMPORARY TABLE tmp SELECT * FROM udf_" . $dbHashidEscaped . " WHERE hashid = '" . mysql_real_escape_string($gval[1]) . "' AND issaved = 1 LIMIT 1;";
    mysql_query($sql) Or Die ("Cannot create temp table: 4537!");
//    echo $sql."<br>";

    $sqlsethashid = "SELECT MAX(id) AS HashID FROM udf_" . $dbHashidEscaped . "";
    //echo $sqlsethashid."<br>";
    $ressethashid = mysql_query($sqlsethashid);
    $rowsethashid = mysql_fetch_array($ressethashid) Or Die ("Cannot set ID, please notify systems administrator with this code: SetHID: jdhdyn");

    $sqlnumseries = "SELECT numberseries FROM menusub_tabs WHERE hashid = '" . $dbHashidEscaped . "' AND registrantid =" . RID;
    //echo $sqlnumseries."<br>";
    $resnumseries = mysql_query($sqlnumseries);
    $rownumseriest = mysql_fetch_array($resnumseries);
    $newnumseriest = ($rownumseries['numberseries'] + 1);
    $numberseriesoutputt = $rownumseriest['numberseries'];

    $querysetnumseries = "UPDATE menusub_tabs SET numberseries = '" . ($numberseriesoutputt + 1) . "' WHERE hashid = '" . $gval[21] . "' AND registrantid =" . RID;
    //echo $querysetnumseries."<br>";
    mysql_query($querysetnumseries) or die ("Cannot update number series, notify support");

    $querysethashid = "UPDATE tmp SET id = '" . ($rowsethashid['HashID'] + 1) . "', recordid = '" . $numberseriesoutputt . "', 
			   hashid = '" . strtoupper(md5($rowsethashid['HashID'] + 1)) . "' WHERE hashid = '" . $gval[1] . "'";
    //echo $querysethashid."<br>";

    mysql_query($querysethashid) Or Die ("Cannot update tmp table: fhftte");

    $sqlinst = "INSERT INTO udf_" . $dbHashidEscaped . " SELECT * FROM tmp WHERE id = " . ($rowsethashid['HashID'] + 1) . ";";
    mysql_query($sqlinst) Or Die ("Cannot duplicate record: gfdgdg86fhftte");
    //echo $sqlinst."<br>";

    // Letc change the created date for this record.
    $sqlCreated = "UPDATE udf_" . $dbHashidEscaped . " SET
					createddate = '" . date("Y-m-d H:i:s") . "',
					createdby = '" . UID . "',
					lastupdatedby = '" . UID . "',
					lastupdateddate = '" . date("Y-m-d H:i:s") . "'
					WHERE id = '" . ($rowsethashid['HashID'] + 1) . "' LIMIT 1";
    //echo $sqlCreated."<br>";
    mysql_query($sqlCreated);

    $sqlinst = "CREATE TEMPORARY TABLE tmprelations SELECT * FROM relations_values WHERE location = '" . $dbHashidEscaped . "' AND recordid = '" . $gval[1] . "';";
    mysql_query($sqlinst) Or Die ("Cannot duplicate record: 64545");
    //echo $sqlinst."<br>";

    $querysethashid = "UPDATE tmprelations SET id = NULL, recordid = '" . strtoupper(md5($rowsethashid['HashID'] + 1)) . "'";
    //echo $querysethashid."<br>";
    mysql_query($querysethashid) Or Die ("Cannot update tmp table: fhftte");

    $sqlinst = "INSERT INTO relations_values SELECT * FROM tmprelations WHERE recordid = '" . strtoupper(md5($rowsethashid['HashID'] + 1)) . "'";
    //echo $sqlinst."<br>";
    mysql_query($sqlinst) Or Die ("Cannot duplicate record: ffgt67");

    $sqlrep = "CREATE TEMPORARY TABLE tmpmultilink SELECT * FROM multilink
			WHERE (parent_table = '" . $dbHashidEscaped . "' AND parent_record = '" . $gval[1] . "'
			OR child_table = '" . $dbHashidEscaped . "' AND child_record = '" . $gval[1] . "')";
    //echo $sqlrep."<br>";
    mysql_query($sqlrep) Or Die ("Cannot duplicate record: 5874i");

    $querysethashid = "UPDATE tmpmultilink SET id = NULL, parent_record = '" . strtoupper(md5($rowsethashid['HashID'] + 1)) . "'";
    //echo $querysethashid."<br>";
    mysql_query($querysethashid) Or Die ("Cannot update tmp table: ddft4r");

    $sqlinst = "INSERT INTO multilink SELECT * FROM tmpmultilink;";
    //echo $sqlinst."<br>";
    mysql_query($sqlinst) Or Die ("Cannot duplicate record: ffgdd9t67");

    $sqlrep = "CREATE TEMPORARY TABLE tmpclasssessions SELECT * FROM class_sessions WHERE location = '" . $dbHashidEscaped . "' AND parent_hashid = '" . $gval[1] . "'";
    mysql_query($sqlrep) Or Die ("Cannot duplicate record: 5874i");
    //echo $sqlrep."<br>";

    $querysethashid = "UPDATE tmpclasssessions SET id = NULL, parent_hashid = '" . strtoupper(md5($rowsethashid['HashID'] + 1)) . "'";
    //echo $querysethashid."<br>";
    mysql_query($querysethashid) Or Die ("Cannot update tmp table: ddft4r");

    $sqlinst = "INSERT INTO class_sessions SELECT * FROM tmpclasssessions;";
    //echo $sqlinst."<br>";
    mysql_query($sqlinst) Or Die ("Cannot duplicate record: ffgdd9t67");

    header("Location:" . $_SERVER['PHP_SELF'] . "?id=:::::" . $gval[5] . ":" . $gval[6] . ":" . $gval[7] . ":" . $gval[8] . ":" . $gval[9] . ":" . $gval[10] . ":" . $gval[11] . ":" . $gval[12] . ":" . $gval[13] . ":" . $gval[14] . ":" . $gval[15] . ":" . $gval[16] . ":" . $gval[17] . ":DESC:id:" . $gval[20] . ":" . $gval[21] . ":" . $gval[22] . ":" . $gval[23] . ":" . $gval[24] . "");
} // 50 - 55 range allocated to dashboard
elseif (($gval[0] == 51) && ($gval[1] == 14) && ($gval[2] == 2)) {
    // delete report element
    $sql = "DELETE FROM `reports_groups` WHERE `reports_groups`.`id` = '" . mysql_real_escape_string($gval[3]) . "' LIMIT 1";
    mysql_query($sql) Or Die ("Cannot duplicate record: djstp");
}
elseif ($gval[0] == 60) { // activate coaches to do reports

    $querymsh = "SELECT name, emailaddress FROM udf_81448138F5F163CCDBA4ACC69819F280 WHERE hashid = '" . mysql_real_escape_string($gval[2]) . "' LIMIT 1";
    //echo $querymsh."<br />";
    $resmsh = mysql_query($querymsh);
    $rowmsh = mysql_fetch_array($resmsh);

    $email_address = $rowmsh['emailaddress'];
    $name = $rowmsh['name'];

    $querymsh = "SELECT id FROM users WHERE email LIKE '" . $email_address . "' LIMIT 1 ";
    echo $querymsh . "<br />";
    $resmsh = mysql_query($querymsh);
    $rowmsh = mysql_fetch_array($resmsh);

    if ($rowmsh['id'] != "") {
        //echo $_SESSION['return_url_reports_user'];
        header("Location: thinline.php?id=" . $_SESSION['return_url_reports_user'] . "&msg=email_exists");
    } else {

        $querymsh = "SELECT * FROM users WHERE id = '" . $_SESSION['userid'] . "' LIMIT 1";
        //echo $querymsh;
        $resmsh = mysql_query($querymsh);
        $rowmsh = mysql_fetch_array($resmsh);

        $sql = "INSERT INTO `users` (`registrantid`, `username`, `departments`, `isfranchiseowner`, `email`, `password`, `isactive`,
									`ismaster`, `ismeasured`, `permissions`, `listgroupsize`, `homepage`, `allowmultiplesession`, `security_template`,
									`countryadmin`, `franchisecountry`, `datecreated`, `showrecent`,`fullname`, `coach_set_as_user`) 
					VALUES (
						'1', '" . mysql_real_escape_string($email_address) . "', '" . $rowmsh['departments'] . "', 'No',
						'" . mysql_real_escape_string($email_address) . "','password-to-be-set', '1',
						'0', '0', 'NULL', '20', 'thinline.php', '1','33',
						'NULL', '" . $rowmsh['franchisecountry'] . "', '" . date("Y-m-d H:i:s") . "', '1', '" . mysql_real_escape_string($name) . "', '" . mysql_real_escape_string($gval[2]) . "');";
        //echo $sql."<br />";
        mysql_query($sql);

        $last_recordid = mysql_insert_id();
        $last_recordid_hash = strtoupper(md5($last_recordid));

        $sql = "UPDATE `users` SET `idhash` = '" . mysql_real_escape_string($last_recordid_hash) . "' WHERE `users`.`id` = '" . mysql_real_escape_string($last_recordid) . "';";
        mysql_query($sql);
        //echo $sql."<br />";

        $sql = "INSERT INTO `users_menustring` (`userid`, `registrantid`, `menu_string`)
					VALUES ('" . mysql_real_escape_string($last_recordid) . "', '1', 'C0C7C76D30BD3DCAEFC96F40275BDC0A,8F53295A73878494E9BC8DD6C3C7104F,8F85517967795EEEF66C225F7883BDCB,A87FF679A2F3E71D9181A67B7542122C,96DA2F590CD7246BBDE0051047B0D6F7');";
        mysql_query($sql);

        // set hashid and retrieve

        $sql = "INSERT INTO `franchise_master`.`users` (`id`, `db`, `idhash`, `username`, `email`, `password`, `isactive`, `ismaster`,
										`datecreated`, `departments`, `country`,`coach_set_as_user`) VALUES
									('" . mysql_real_escape_string($last_recordid) . "', 'franchise_playball', '" . mysql_real_escape_string($last_recordid_hash) . "',
										'" . mysql_real_escape_string($email_address) . "', '" . mysql_real_escape_string($email_address) . "',
										'password-to-be-set', '1', '0', '" . date("Y-m-d H:i:s") . "',
										'" . $rowmsh['departments'] . "', '" . $rowmsh['franchisecountry'] . "', '" . mysql_real_escape_string($gval[2]) . "');";
        //echo $sql."<br />";
        mysql_query($sql);
        header("Location: thinline.php?id=" . $_SESSION['return_url_reports_user']);

    }

}
elseif ($gval[0] == 61) { // activate coaches to do reports
    $sql = "DELETE FROM `users` WHERE `coach_set_as_user` = '" . mysql_real_escape_string($gval[2]) . "';";
    mysql_query($sql);
    //echo $sql."<br />";

    $sql = "DELETE FROM `franchise_master`.`users` WHERE `users`.`coach_set_as_user` = '" . mysql_real_escape_string($gval[2]) . "';";
    mysql_query($sql);
    //echo $sql."<br />";

    header("Location: thinline.php?id=" . $_SESSION['return_url_reports_user']);
}
elseif ($gval[0] == 62) { // activate coaches to do reports

    if ($gval[1] == 1) {
        // Make public
        $sql = "UPDATE `reports` SET `public` = 'Yes' WHERE `reports`.`id` = '" . mysql_real_escape_string($gval[2]) . "';";
    } elseif ($gval[1] == 2) {
        // Make private
        $sql = "UPDATE `reports` SET `public` = 'No' WHERE `reports`.`id` = '" . mysql_real_escape_string($gval[2]) . "';";
    }

    mysql_query($sql);
    $url = base64_decode($_GET['r']);
    header("Location: " . $url . "#bottom");
}

// Process activity is processed out of activity_manager.php
if ($_POST['processactivity'] == "1") {

    if ($_POST['subject'] == "") {
        $subjectfail = 1;
    }

    if ($subjectfail != 1) {

        //// Lets break down the date into a usable format.
        $startingmonth = explode("-", substr($_POST['start'], 0, 11));
        $startingtime = substr($_POST['start'], -5, 5);
        $startdatefinal = $startingmonth[2] . "-" . convertDate($startingmonth[1]) . "-" . $startingmonth[0] . " " . $startingtime . ":00";

        $endingmonth = explode("-", substr($_POST['end'], 0, 11));
        $endingtime = substr($_POST['end'], -5, 5);
        $enddatefinal = $endingmonth[2] . "-" . convertDate($endingmonth[1]) . "-" . $endingmonth[0] . " " . $endingtime . ":00";

        if ($_POST['remindertime'] != "") {
            $remindertime = timetosec($_POST['remindertime']);
            // echo strtotime($startdatefinal)." - ".$remindertime."<----<br>";
        }

        $query = "INSERT INTO activities (registrantid, userid, subject, datestart, dateend, activitytype, activitystatus, remindertime, smsreminder, showtimeas, label, allocatedto, privacy, notes, addtocal, datecreated) VALUES (
		  " . RID . ",
		  " . UID . ",
		  '" . $_POST['subject'] . "',
		  '" . $startdatefinal . "',
		  '" . $enddatefinal . "',
		  '" . $_POST['activitytype'] . "',
		  '" . $_POST['activitystatus'] . "',
		  '" . $remindertime . "',
		  '" . $_POST['smsreminder'] . "',
		  '" . $_POST['showtimeas'] . "',
		  '" . $_POST['label'] . "',
	 	  '" . $_POST['allocatedto'] . "',
	 	  '" . $_POST['privacy'] . "',
		  '" . $_POST['notes'] . "',
		  '" . $_POST['addtocal'] . "',
	 	  '" . $sGMTMySqlString . "'
	 	 )";
        // echo $query;
        mysql_query($query) Or Die ("Cannot submit entry! 1256");

        $query = "SELECT MAX(id) AS HashID FROM activities";
        //echo $query;
        $result = mysql_query($query);
        $row = mysql_fetch_array($result);

        $query = "UPDATE activities SET hashid='" . strtoupper(md5($row['HashID'])) . "' WHERE id=" . $row['HashID'];
        mysql_query($query) Or Die ("Cannot submit entry! 1");

        // Now we check if there is an associated record and if so we attach it
        // in the activity relationship table...

        if ($gval[1] != "") {
            $query = "INSERT INTO activity_relations (registrantid, userid, activityhashid, menutabid, location, recordid, datecreated) VALUES (
					" . RID . ",
					" . UID . ",
					'" . strtoupper(md5($row['HashID'])) . "',
		  			'" . $gval[21] . "',
		  			'" . $gval[22] . "',
		  			'" . $gval[1] . "',
	 	  			'" . $sGMTMySqlString . "'
	 			 )";
            // echo $query;
            mysql_query($query) Or Die ("Cannot submit entry!");
        }
        //echo strtoupper(md5($row['HashID']));
        header("Location: " . $_SERVER['PHP_SELF'] . "?id=" . $gval[0] . ":" . $gval[1] . ":" . $gval[2] . ":" . $gval[3] . ":" . $gval[4] . ":" . strtoupper(md5($row['HashID'])) . ":" . $gval[6] . ":" . $gval[7] . ":" . $gval[8] . ":" . $gval[9] . ":" . $gval[10] . ":" . $gval[11] . ":" . $gval[12] . ":" . $gval[13] . ":" . $gval[14] . ":" . $gval[15] . ":" . $gval[16] . ":" . $gval[17] . ":" . $gval[18] . ":" . $gval[19] . ":" . $gval[20] . ":" . $gval[21] . ":" . $gval[22] . ":" . $gval[23] . ":" . $gval[24] . ":" . $gval[25]);
    }
}
elseif ($_POST['processactivity'] == 2) {

    if ($_POST['subject'] == "") {
        $subjectfail = 1;
    }

    $query = "SELECT * FROM activities WHERE hashid ='" . $gval[5] . "'";
    //echo $query."<br>";
    $result = mysql_query($query);
    $row = mysql_fetch_array($result) or die ("Failed to check activity for update 4234sdfsd2");

    //// Lets break down the date into a usable format.
    $startingmonth = explode("-", substr($_POST['start'], 0, 11));
    $startingtime = substr($_POST['start'], -5, 5);
    $startdatefinal = $startingmonth[2] . "-" . convertDate($startingmonth[1]) . "-" . $startingmonth[0] . " " . $startingtime . ":00";

    $endingmonth = explode("-", substr($_POST['end'], 0, 11));
    $endingtime = substr($_POST['end'], -5, 5);
    $enddatefinal = $endingmonth[2] . "-" . convertDate($endingmonth[1]) . "-" . $endingmonth[0] . " " . $endingtime . ":00";

    if ($subjectfail != 1) {
        if ($_POST['remindertime'] != "") {
            $remindertime = timetosec($_POST['remindertime']);
        } else {
            $remindertime = '';
        }
        $query = "UPDATE activities SET 
		  registrantid = " . RID . ",
		  userid = '" . $_SESSION['userid'] . "',
		  subject = '" . $_POST['subject'] . "',
		  datestart = '" . $startdatefinal . "',
		  dateend = '" . $enddatefinal . "',
		  activitytype = '" . $_POST['activitytype'] . "',
		  activitystatus = '" . $_POST['activitystatus'] . "',
		  remindertime = '" . $remindertime . "',
	   	  smsreminder = '" . $_POST['smsreminder'] . "',
		  showtimeas = '" . $_POST['showtimeas'] . "',
		  label = '" . $_POST['label'] . "',
	 	  allocatedto = '" . $_POST['allocatedto'] . "',
	 	  privacy = '" . $_POST['privacy'] . "',
		  notes = '" . $_POST['notes'] . "',
		  addtocal = '" . $_POST['addtocal'] . "'
	 	  WHERE id =" . $row['id'];
        echo $query;
        mysql_query($query) Or Die ("Cannot submit entry2!");
    }
    header("Location: " . $_SERVER['PHP_SELF'] . "?id=" . $gval[0] . ":" . $gval[1] . ":" . $gval[2] . ":" . $gval[3] . ":" . $gval[4] . ":" . $gval[5] . ":" . $gval[6] . ":" . $gval[7] . ":" . $gval[8] . ":" . $gval[9] . ":" . $gval[10] . ":" . $gval[11] . ":" . $gval[12] . ":" . $gval[13] . ":" . $gval[14] . ":" . $gval[15] . ":" . $gval[16] . ":" . $gval[17] . ":" . $gval[18] . ":" . $gval[19] . ":" . $gval[20] . ":" . $gval[21] . ":" . $gval[22] . ":" . $gval[23] . ":" . $gval[24] . ":" . $gval[25]);
} ?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/favicon-<?php echo strtolower($_SESSION['franchisedata']['name']); ?>.ico"
          type="image/x-icon"/>
    <title><?php echo $_SERVER['HTTP_HOST'] ?></title>
    <?php
    if (ERES > 1024) { ?>
        <style type="text/css" media="all">@import "css/style.css";</style>
        <?php
    } else { ?>
        <style type="text/css" media="all">@import "css/style_small.css";</style>
        <?php
    } ?>
    <script type="text/javascript" language="javascript" src="lytebox/lytebox.js"></script>
    <link rel="stylesheet" href="lytebox/lytebox.css" type="text/css" media="screen"/>
    <style type="text/css" media="all">@import "css/calendar_pop.css";</style>
    <script type='text/javascript' src='js/calendar_pop.js'></script>
    <script src="js/prototype.js" type="text/javascript"></script>
    <script src="js/scriptaculous.js?load=effects,builder" type="text/javascript"></script>
    <script src="js/functions.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/dropdown.js"></script>
    <script type="text/javascript" src="js/preLoadingMessage.js"></script>
    <Script language="JavaScript">
        <!--
        function goto(form) {
            var index = form.select.selectedIndex;
            if (form.select.options[index].value != "0") {
                location = form.select.options[index].value;
            }
        }

        //-->
    </SCRIPT>
    <?php
    if ($pop_me_up_in != "") { ?>
        <script language="JavaScript" type="text/javascript">
            setTimeout("window.open('activity_popup.php?id=::2::::::::::::::::::','popup_alert','width=600,height=400,directories=no,location=no,menubar=no,scrollbars=yes,status=yes,toolbar=no,resizable=yes');", <?php echo $pop_me_up_in; ?>);
        </script>
        <?php
    } ?>
</head>
<body>
<a name="#top"></a>
<?php include_once("header_menu.php"); ?>
<br/>

<?php
if ($gval[21] == "5737034557EF5B8C02C0E46513B98F90") {
    include 'includes/venue-count.php';
} ?>

<?php
// for genertating the top level tabs
include_once("tabs_generator.php"); ?>
<div id="main">
    <?php include_once("tabssub_generator.php"); ?>
    <?php
//    echo nl2br("thinline.php\n");
//    var_dump($_SESSION['top_menu_type']);

    if ($_SESSION['top_menu_type'] == "Calendar") {
        include_once("calendar.php");
    } elseif ($_SESSION['top_menu_type'] == "Standard Form") {
        include_once("standard_form.php");
    } elseif ($_SESSION['top_menu_type'] == "Document Store") {
        include_once("document_store.php");
    } elseif ($_SESSION['top_menu_type'] == "Printers") {
        include_once("printers.php");
    } elseif ($_SESSION['top_menu_type'] == "Activities") {
        if (($gval[2] == 1) || ($gval[2] == 2)) {
            include_once("activities.php");
        } else {
            include_once("activity_manager.php");
        }
    } elseif ($_SESSION['top_menu_type'] == "Accounts") {
        include_once("accounts.php");
    } elseif ($_SESSION['top_menu_type'] == "Reports") {
        include_once("reports.php");
    } elseif ($_SESSION['top_menu_type'] == "Dashboard") {
        include_once("dashboard.php");
    }
    ?>
</div>
<?php
include_once("footer.php");
?>
<a name="bottom"></a>
</body>
</html>
