<?php 
include_once("_globalconnect.php"); 
if ($_POST['pform'] == 1) {
	// Lets build the menudisplaystring
	$count = 0;
	for ($x = 0; $x < $_POST['totalmenuitems']; $x++) {
		if ($x != ($_POST['totalmenuitems']-1)) {
			$menustringarray .= $_POST['menuitem_'.$x].",";
		} else {
			$menustringarray .= $_POST['menuitem_'.$x];
		}
		$count++;
	}
	if ($_POST['menuupdate'] == 0) {
		$query = "INSERT INTO sub_tabs_select (location, registrantid, userid, menu_string) VALUES 
				('".$_POST['location']."',".RID.",".UID.",'".$menustringarray."')";
		// echo $query;
		mysql_query($query) Or Die ("Cannot submit entry!");
	} else {
		$query = "UPDATE sub_tabs_select SET menu_string = '".$menustringarray."' WHERE location = '".$_POST['location']."' AND registrantid = ".RID." AND userid = ".UID;
		// echo $query;
		mysql_query($query) Or Die ("Cannot submit entry!");
	}
	
	$onload = 'onload="window.parent.location = window.parent.location;self.close();return false;"';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "includes/xhtml1-transitional.dtd">
<html>
<head>
	<title>Sub Tab Selector</title>
	<?php 
	if (ERES > 1024) { ?>
		<style type="text/css" media="all">@import "css/style.css";</style>
	<?php 
	} else { ?>
		<style type="text/css" media="all">@import "css/style_small.css";</style>
	<?php
	}?>
</head>
<body <?php echo $onload; ?>>
<script language="JavaScript">
function submitform() {
	document.tabform.submit();
}
</script>
<form method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?>?id=<?php echo $gval[0]; ?>" name="tabform">
	<table cellspacing="0" cellpadding="0" border="0" width="93%">
	<tr>
		<td><img src="images/<?php echo $_SESSION['franchisedata']['logo'];?>" alt="" / align="center" border="0"></td>
	</tr>
	</table><br />
	<table cellspacing="0" cellpadding="5" border="0" width="93%">
	<tr>
		<td rowspan="2"><img src="images/spacer.gif" height="1" width="3"></td>
		<td width="35" valign="top" rowspan="2">
			<img src="images/icons/more_tabs_32.png"><img src="images/spacer.gif" height="1" width="10"><br>
		</td>
		<td valign="top">
			<div id="menu_bar">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="85%" align="left"><strong><font style="font-size:130%;font-family:Trebuchet MS">Sub Tab Selector</font></strong></td>
				<td width="70%">&nbsp;</td>
				<td><img src="images/menu/menu_01.gif" width="13" height="25" alt=""></td>
				<td><a href="javascript: submitform()"><img src="images/menu/save.gif" width="23" height="25" alt="Save Tab Selection" border="0"></a></td>
				<td><img src="images/menu/menu_08.gif" width="8" height="25" alt=""></td>
				<td><a href="<?php echo $_SERVER['PHP_SELF']?>?id=<?php echo $fullidvalues ?>"><img src="images/refresh.gif" border="0" alt="Refresh this page"></a></td>
				<td><img src="images/menu/menu_11.gif" width="9" height="25" alt=""></td>
				<td><img src="images/menu/menu_13.gif" width="18" height="25" alt=""></td>
			</tr>
			</table>
			</div>					
		</td>
	</tr>
	</table><br>
	<table cellpadding="3" cellspacing="3" border="0" width="100%">
	<tr align="center">
		<td width="5%" rowspan="102">&nbsp;</td>
		<td class="ls_top"><strong>Sub Tab Name</strong></td>
		<td class="ls_top"><strong>Display</strong></td>
		<td width="5%" rowspan="102">&nbsp;</td>
	</tr>
	<?php 
		// Check if there is a menu manager entry for this user for this particular location
		$sql = "SELECT id, menu_string FROM sub_tabs_select WHERE location = '".$gval[0]."' AND userid =".UID." AND registrantid =".RID;
		$res = mysql_query($sql);
		$row = mysql_fetch_array($res);
		if (!$row['id']) { 
			$menuupdate = 0;
		} else { 
			$menuupdate = 1;
			$menustring = $row['menu_string'];
			$menustring = explode(",",$menustring);
		}
		
		// Now we generate all the labels for this location
		$query = "SELECT childtable, labelhashid, ispredefined FROM relations_matrix WHERE parenttable = '".$gval[0]."' AND isactive = 1 ORDER BY sortorder ASC";
		//echo $query;
		$getsubtabs = mysql_query($query);
		$labelarray = array();
		$tablearray = array();
		while ($row = mysql_fetch_array($getsubtabs)) { 
			if ($_SESSION['securityarrl2'][$row['childtable']] != 4) {
				if (($row['ispredefined'] == 1) || ($row['ispredefined'] == 2) || ($row['ispredefined'] == 3)) {
					$getpre = "SELECT label FROM relations_predefined WHERE id = ".$row['childtable']." LIMIT 1";
					// echo $getpre;
					$preres = mysql_query($getpre);
					$prerow = mysql_fetch_array($preres);
					array_push($labelarray, $prerow['label']);
				} else {
					$getpre = "SELECT tablabel FROM udf_definitions WHERE hashid = '".$row['labelhashid']."' AND isactive = 1 LIMIT 1";
					// echo $getpre."<br>";
					$preres = mysql_query($getpre);
					$prerow = mysql_fetch_array($preres);
					array_push($labelarray, $prerow['tablabel']);
				}
			}
		} 
		$menucounter = 0;
		foreach ($labelarray as $value) { 
			include("includes/ls.php"); 
			// If there is no entry for this lcoation then everything will be displayed...
			if ($menuupdate == 0) { $checkme = "checked"; } 
			if (in_array($value, $menustring)) {
    			$checkme = "checked";
			}?>
		<tr>			
			<td class="ls_<?php echo $ls ?>"><?php echo $value ?></td>
			<td class="ls_<?php echo $ls ?>" width="15%" align="center"><input <?php echo $checkme; ?> type="checkbox" name="menuitem_<?php echo $menucounter; ?>" value="<?php echo $value; ?>"></td>
		</tr>
		<?php
			unset($checkme);
			$menucounter++;
		}?>
	<input type="hidden" name="totalmenuitems" value="<?php echo count($labelarray); ?>">
	<input type="hidden" name="pform" value="1">
	<input type="hidden" name="location" value="<?php echo $gval[0];  ?>">
	<input type="hidden" name="menuupdate" value="<?php echo $menuupdate; ?>">
	</table>
</form>
</body>
</html>